<?php

$file = fopen(dirname(__FILE__)."/pregrepl.csv", "r");
$file2 = fopen(dirname(__FILE__)."/pregrepl-processed.csv", "w+");
$pids = [];
while($line = fgetcsv($file)){
	//print_r($pids);
	$text = $line[4].'-'.$line[4];
	$text = strtolower(preg_replace('#[^0-9a-z]+#i', '-', $text));
	$line[4] = $text;
	unset($line[5]);
	fputcsv($file2, $line);
}
