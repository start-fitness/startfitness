<?php
namespace Raw\VatPopup\Controller\Check\Index;

/**
 * Interceptor class for @see \Raw\VatPopup\Controller\Check\Index
 */
class Interceptor extends \Raw\VatPopup\Controller\Check\Index implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory, \Magento\Framework\Json\Helper\Data $jsonHelper, \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager, \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory, \Magento\Framework\Session\SessionManagerInterface $sessionManager, \Magento\Checkout\Model\Session $checkoutSession, \Magento\Tax\Model\Calculation\Rate $taxModelConfig, \Magento\Framework\App\Request\Http $request)
    {
        $this->___init();
        parent::__construct($context, $resultPageFactory, $jsonHelper, $cookieManager, $cookieMetadataFactory, $sessionManager, $checkoutSession, $taxModelConfig, $request);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        if (!$pluginInfo) {
            return parent::execute();
        } else {
            return $this->___callPlugins('execute', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function taxestoArray()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'taxestoArray');
        if (!$pluginInfo) {
            return parent::taxestoArray();
        } else {
            return $this->___callPlugins('taxestoArray', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRateByCountry($country)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRateByCountry');
        if (!$pluginInfo) {
            return parent::getRateByCountry($country);
        } else {
            return $this->___callPlugins('getRateByCountry', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCurrentTax()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCurrentTax');
        if (!$pluginInfo) {
            return parent::getCurrentTax();
        } else {
            return $this->___callPlugins('getCurrentTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setCurrentTax($data)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setCurrentTax');
        if (!$pluginInfo) {
            return parent::setCurrentTax($data);
        } else {
            return $this->___callPlugins('setCurrentTax', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function jsonResponse($response = '')
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'jsonResponse');
        if (!$pluginInfo) {
            return parent::jsonResponse($response);
        } else {
            return $this->___callPlugins('jsonResponse', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'dispatch');
        if (!$pluginInfo) {
            return parent::dispatch($request);
        } else {
            return $this->___callPlugins('dispatch', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getActionFlag()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getActionFlag');
        if (!$pluginInfo) {
            return parent::getActionFlag();
        } else {
            return $this->___callPlugins('getActionFlag', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getRequest()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getRequest');
        if (!$pluginInfo) {
            return parent::getRequest();
        } else {
            return $this->___callPlugins('getRequest', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getResponse');
        if (!$pluginInfo) {
            return parent::getResponse();
        } else {
            return $this->___callPlugins('getResponse', func_get_args(), $pluginInfo);
        }
    }
}
