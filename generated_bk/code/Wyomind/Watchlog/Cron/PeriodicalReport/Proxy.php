<?php
namespace Wyomind\Watchlog\Cron\PeriodicalReport;

/**
 * Proxy class for @see \Wyomind\Watchlog\Cron\PeriodicalReport
 */
class Proxy extends \Wyomind\Watchlog\Cron\PeriodicalReport implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Wyomind\Watchlog\Cron\PeriodicalReport
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Wyomind\\Watchlog\\Cron\\PeriodicalReport', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Wyomind\Watchlog\Cron\PeriodicalReport
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function getAttemptsCollectionFactory()
    {
        return $this->_getSubject()->getAttemptsCollectionFactory();
    }

    /**
     * {@inheritdoc}
     */
    public function sendPeriodicalReport(?\Magento\Cron\Model\Schedule $schedule = null)
    {
        return $this->_getSubject()->sendPeriodicalReport($schedule);
    }

    /**
     * {@inheritdoc}
     */
    public function getHistory($date)
    {
        return $this->_getSubject()->getHistory($date);
    }

    /**
     * {@inheritdoc}
     */
    public function getTemplate()
    {
        return $this->_getSubject()->getTemplate();
    }

    /**
     * {@inheritdoc}
     */
    public function sendReport($emails)
    {
        return $this->_getSubject()->sendReport($emails);
    }
}
