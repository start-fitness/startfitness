<?php
namespace Wyomind\ElasticsearchBrowser\Helper\Data;

/**
 * Proxy class for @see \Wyomind\ElasticsearchBrowser\Helper\Data
 */
class Proxy extends \Wyomind\ElasticsearchBrowser\Helper\Data implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Wyomind\ElasticsearchBrowser\Helper\Data
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Wyomind\\ElasticsearchBrowser\\Helper\\Data', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Wyomind\ElasticsearchBrowser\Helper\Data
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function getNotificationMessage()
    {
        return $this->_getSubject()->getNotificationMessage();
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstStoreviewId()
    {
        return $this->_getSubject()->getFirstStoreviewId();
    }

    /**
     * {@inheritdoc}
     */
    public function getIndexName($storeId = null)
    {
        return $this->_getSubject()->getIndexName($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getBrowseColumns()
    {
        return $this->_getSubject()->getBrowseColumns();
    }

    /**
     * {@inheritdoc}
     */
    public function addBrowseColumn($type, $field, $title, $sortOrder)
    {
        return $this->_getSubject()->addBrowseColumn($type, $field, $title, $sortOrder);
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        return $this->_getSubject()->isModuleOutputEnabled($moduleName);
    }
}
