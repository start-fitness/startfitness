<?php
namespace Wyomind\ElasticsearchBrowser\Model\Client;

/**
 * Proxy class for @see \Wyomind\ElasticsearchBrowser\Model\Client
 */
class Proxy extends \Wyomind\ElasticsearchBrowser\Model\Client implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Wyomind\ElasticsearchBrowser\Model\Client
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Wyomind\\ElasticsearchBrowser\\Model\\Client', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Wyomind\ElasticsearchBrowser\Model\Client
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        return $this->_getSubject()->init();
    }

    /**
     * {@inheritdoc}
     */
    public function info()
    {
        return $this->_getSubject()->info();
    }

    /**
     * {@inheritdoc}
     */
    public function query($indices, $types, array $params = [])
    {
        return $this->_getSubject()->query($indices, $types, $params);
    }

    /**
     * {@inheritdoc}
     */
    public function getByIds($indices, $type, $ids)
    {
        return $this->_getSubject()->getByIds($indices, $type, $ids);
    }

    /**
     * {@inheritdoc}
     */
    public function getParams()
    {
        return $this->_getSubject()->getParams();
    }
}
