<?php
namespace Wyomind\SimpleGoogleShopping\Helper\Data;

/**
 * Proxy class for @see \Wyomind\SimpleGoogleShopping\Helper\Data
 */
class Proxy extends \Wyomind\SimpleGoogleShopping\Helper\Data implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Wyomind\SimpleGoogleShopping\Helper\Data
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Wyomind\\SimpleGoogleShopping\\Helper\\Data', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Wyomind\SimpleGoogleShopping\Helper\Data
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function generationStats($row)
    {
        return $this->_getSubject()->generationStats($row);
    }

    /**
     * {@inheritdoc}
     */
    public function reportToHtml($data)
    {
        return $this->_getSubject()->reportToHtml($data);
    }

    /**
     * {@inheritdoc}
     */
    public function stripTagsContent($text, $tags = '', $invert = false)
    {
        return $this->_getSubject()->stripTagsContent($text, $tags, $invert);
    }

    /**
     * {@inheritdoc}
     */
    public function strReplaceFirst($search, $replace, $subject)
    {
        return $this->_getSubject()->strReplaceFirst($search, $replace, $subject);
    }

    /**
     * {@inheritdoc}
     */
    public function isMsiEnabled()
    {
        return $this->_getSubject()->isMsiEnabled();
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        return $this->_getSubject()->isModuleOutputEnabled($moduleName);
    }
}
