<?php
namespace Magento\Braintree\Gateway\Config\Config;

/**
 * Interceptor class for @see \Magento\Braintree\Gateway\Config\Config
 */
class Interceptor extends \Magento\Braintree\Gateway\Config\Config implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Braintree\Model\StoreConfigResolver $storeConfigResolver, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, $methodCode = null, $pathPattern = 'payment/%s/%s', ?\Magento\Framework\Serialize\Serializer\Json $serializer = null)
    {
        $this->___init();
        parent::__construct($storeConfigResolver, $scopeConfig, $methodCode, $pathPattern, $serializer);
    }

    /**
     * {@inheritdoc}
     */
    public function getCountrySpecificCardTypeConfig() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountrySpecificCardTypeConfig');
        if (!$pluginInfo) {
            return parent::getCountrySpecificCardTypeConfig();
        } else {
            return $this->___callPlugins('getCountrySpecificCardTypeConfig', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAvailableCardTypes() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAvailableCardTypes');
        if (!$pluginInfo) {
            return parent::getAvailableCardTypes();
        } else {
            return $this->___callPlugins('getAvailableCardTypes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCcTypesMapper() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCcTypesMapper');
        if (!$pluginInfo) {
            return parent::getCcTypesMapper();
        } else {
            return $this->___callPlugins('getCcTypesMapper', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getCountryAvailableCardTypes($country) : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getCountryAvailableCardTypes');
        if (!$pluginInfo) {
            return parent::getCountryAvailableCardTypes($country);
        } else {
            return $this->___callPlugins('getCountryAvailableCardTypes', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isCvvEnabled() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCvvEnabled');
        if (!$pluginInfo) {
            return parent::isCvvEnabled();
        } else {
            return $this->___callPlugins('isCvvEnabled', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isCvvEnabledVault() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isCvvEnabledVault');
        if (!$pluginInfo) {
            return parent::isCvvEnabledVault();
        } else {
            return $this->___callPlugins('isCvvEnabledVault', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isVerify3DSecure() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isVerify3DSecure');
        if (!$pluginInfo) {
            return parent::isVerify3DSecure();
        } else {
            return $this->___callPlugins('isVerify3DSecure', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getThresholdAmount() : float
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getThresholdAmount');
        if (!$pluginInfo) {
            return parent::getThresholdAmount();
        } else {
            return $this->___callPlugins('getThresholdAmount', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get3DSecureSpecificCountries() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'get3DSecureSpecificCountries');
        if (!$pluginInfo) {
            return parent::get3DSecureSpecificCountries();
        } else {
            return $this->___callPlugins('get3DSecureSpecificCountries', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getEnvironment() : string
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getEnvironment');
        if (!$pluginInfo) {
            return parent::getEnvironment();
        } else {
            return $this->___callPlugins('getEnvironment', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getKountMerchantId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getKountMerchantId');
        if (!$pluginInfo) {
            return parent::getKountMerchantId();
        } else {
            return $this->___callPlugins('getKountMerchantId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function canSkipAdminFraudProtection() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'canSkipAdminFraudProtection');
        if (!$pluginInfo) {
            return parent::canSkipAdminFraudProtection();
        } else {
            return $this->___callPlugins('canSkipAdminFraudProtection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMerchantId');
        if (!$pluginInfo) {
            return parent::getMerchantId();
        } else {
            return $this->___callPlugins('getMerchantId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasFraudProtection() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'hasFraudProtection');
        if (!$pluginInfo) {
            return parent::hasFraudProtection();
        } else {
            return $this->___callPlugins('hasFraudProtection', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFraudProtectionThreshold()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFraudProtectionThreshold');
        if (!$pluginInfo) {
            return parent::getFraudProtectionThreshold();
        } else {
            return $this->___callPlugins('getFraudProtectionThreshold', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isActive() : bool
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isActive');
        if (!$pluginInfo) {
            return parent::isActive();
        } else {
            return $this->___callPlugins('isActive', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getDynamicDescriptors() : array
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getDynamicDescriptors');
        if (!$pluginInfo) {
            return parent::getDynamicDescriptors();
        } else {
            return $this->___callPlugins('getDynamicDescriptors', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getMerchantAccountId()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getMerchantAccountId');
        if (!$pluginInfo) {
            return parent::getMerchantAccountId();
        } else {
            return $this->___callPlugins('getMerchantAccountId', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setMethodCode($methodCode)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setMethodCode');
        if (!$pluginInfo) {
            return parent::setMethodCode($methodCode);
        } else {
            return $this->___callPlugins('setMethodCode', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function setPathPattern($pathPattern)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPathPattern');
        if (!$pluginInfo) {
            return parent::setPathPattern($pathPattern);
        } else {
            return $this->___callPlugins('setPathPattern', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getValue($field, $storeId = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getValue');
        if (!$pluginInfo) {
            return parent::getValue($field, $storeId);
        } else {
            return $this->___callPlugins('getValue', func_get_args(), $pluginInfo);
        }
    }
}
