<?php
namespace Magento\Catalog\Model\ResourceModel\Category\Collection;

/**
 * Proxy class for @see \Magento\Catalog\Model\ResourceModel\Category\Collection
 */
class Proxy extends \Magento\Catalog\Model\ResourceModel\Category\Collection implements \Magento\Framework\ObjectManager\NoninterceptableInterface
{
    /**
     * Object Manager instance
     *
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $_objectManager = null;

    /**
     * Proxied instance name
     *
     * @var string
     */
    protected $_instanceName = null;

    /**
     * Proxied instance
     *
     * @var \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    protected $_subject = null;

    /**
     * Instance shareability flag
     *
     * @var bool
     */
    protected $_isShared = null;

    /**
     * Proxy constructor
     *
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param string $instanceName
     * @param bool $shared
     */
    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, $instanceName = '\\Magento\\Catalog\\Model\\ResourceModel\\Category\\Collection', $shared = true)
    {
        $this->_objectManager = $objectManager;
        $this->_instanceName = $instanceName;
        $this->_isShared = $shared;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return ['_subject', '_isShared', '_instanceName'];
    }

    /**
     * Retrieve ObjectManager from global scope
     */
    public function __wakeup()
    {
        $this->_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    }

    /**
     * Clone proxied instance
     */
    public function __clone()
    {
        $this->_subject = clone $this->_getSubject();
    }

    /**
     * Get proxied instance
     *
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     */
    protected function _getSubject()
    {
        if (!$this->_subject) {
            $this->_subject = true === $this->_isShared
                ? $this->_objectManager->get($this->_instanceName)
                : $this->_objectManager->create($this->_instanceName);
        }
        return $this->_subject;
    }

    /**
     * {@inheritdoc}
     */
    public function addIdFilter($categoryIds)
    {
        return $this->_getSubject()->addIdFilter($categoryIds);
    }

    /**
     * {@inheritdoc}
     */
    public function setLoadProductCount($flag)
    {
        return $this->_getSubject()->setLoadProductCount($flag);
    }

    /**
     * {@inheritdoc}
     */
    public function setProductStoreId($storeId)
    {
        return $this->_getSubject()->setProductStoreId($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductStoreId()
    {
        return $this->_getSubject()->getProductStoreId();
    }

    /**
     * {@inheritdoc}
     */
    public function load($printQuery = false, $logQuery = false)
    {
        return $this->_getSubject()->load($printQuery, $logQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function loadProductCount($items, $countRegular = true, $countAnchor = true)
    {
        return $this->_getSubject()->loadProductCount($items, $countRegular, $countAnchor);
    }

    /**
     * {@inheritdoc}
     */
    public function addPathFilter($regexp)
    {
        return $this->_getSubject()->addPathFilter($regexp);
    }

    /**
     * {@inheritdoc}
     */
    public function joinUrlRewrite()
    {
        return $this->_getSubject()->joinUrlRewrite();
    }

    /**
     * {@inheritdoc}
     */
    public function addIsActiveFilter()
    {
        return $this->_getSubject()->addIsActiveFilter();
    }

    /**
     * {@inheritdoc}
     */
    public function addNameToResult()
    {
        return $this->_getSubject()->addNameToResult();
    }

    /**
     * {@inheritdoc}
     */
    public function addUrlRewriteToResult()
    {
        return $this->_getSubject()->addUrlRewriteToResult();
    }

    /**
     * {@inheritdoc}
     */
    public function addPathsFilter($paths)
    {
        return $this->_getSubject()->addPathsFilter($paths);
    }

    /**
     * {@inheritdoc}
     */
    public function addLevelFilter($level)
    {
        return $this->_getSubject()->addLevelFilter($level);
    }

    /**
     * {@inheritdoc}
     */
    public function addRootLevelFilter()
    {
        return $this->_getSubject()->addRootLevelFilter();
    }

    /**
     * {@inheritdoc}
     */
    public function addNavigationMaxDepthFilter()
    {
        return $this->_getSubject()->addNavigationMaxDepthFilter();
    }

    /**
     * {@inheritdoc}
     */
    public function addOrderField($field)
    {
        return $this->_getSubject()->addOrderField($field);
    }

    /**
     * {@inheritdoc}
     */
    public function getProductWebsiteTable()
    {
        return $this->_getSubject()->getProductWebsiteTable();
    }

    /**
     * {@inheritdoc}
     */
    public function getProductTable()
    {
        return $this->_getSubject()->getProductTable();
    }

    /**
     * {@inheritdoc}
     */
    public function setStore($store)
    {
        return $this->_getSubject()->setStore($store);
    }

    /**
     * {@inheritdoc}
     */
    public function setStoreId($storeId)
    {
        return $this->_getSubject()->setStoreId($storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function getStoreId()
    {
        return $this->_getSubject()->getStoreId();
    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultStoreId()
    {
        return $this->_getSubject()->getDefaultStoreId();
    }

    /**
     * {@inheritdoc}
     */
    public function getTable($table)
    {
        return $this->_getSubject()->getTable($table);
    }

    /**
     * {@inheritdoc}
     */
    public function setEntity($entity)
    {
        return $this->_getSubject()->setEntity($entity);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return $this->_getSubject()->getEntity();
    }

    /**
     * {@inheritdoc}
     */
    public function getResource()
    {
        return $this->_getSubject()->getResource();
    }

    /**
     * {@inheritdoc}
     */
    public function setObject($object = null)
    {
        return $this->_getSubject()->setObject($object);
    }

    /**
     * {@inheritdoc}
     */
    public function addItem(\Magento\Framework\DataObject $object)
    {
        return $this->_getSubject()->addItem($object);
    }

    /**
     * {@inheritdoc}
     */
    public function getAttribute($attributeCode)
    {
        return $this->_getSubject()->getAttribute($attributeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function addAttributeToFilter($attribute, $condition = null, $joinType = 'inner')
    {
        return $this->_getSubject()->addAttributeToFilter($attribute, $condition, $joinType);
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldToFilter($attribute, $condition = null)
    {
        return $this->_getSubject()->addFieldToFilter($attribute, $condition);
    }

    /**
     * {@inheritdoc}
     */
    public function addAttributeToSort($attribute, $dir = 'ASC')
    {
        return $this->_getSubject()->addAttributeToSort($attribute, $dir);
    }

    /**
     * {@inheritdoc}
     */
    public function addAttributeToSelect($attribute, $joinType = false)
    {
        return $this->_getSubject()->addAttributeToSelect($attribute, $joinType);
    }

    /**
     * {@inheritdoc}
     */
    public function addEntityTypeToSelect($entityType, $prefix)
    {
        return $this->_getSubject()->addEntityTypeToSelect($entityType, $prefix);
    }

    /**
     * {@inheritdoc}
     */
    public function addStaticField($field)
    {
        return $this->_getSubject()->addStaticField($field);
    }

    /**
     * {@inheritdoc}
     */
    public function addExpressionAttributeToSelect($alias, $expression, $attribute)
    {
        return $this->_getSubject()->addExpressionAttributeToSelect($alias, $expression, $attribute);
    }

    /**
     * {@inheritdoc}
     */
    public function groupByAttribute($attribute)
    {
        return $this->_getSubject()->groupByAttribute($attribute);
    }

    /**
     * {@inheritdoc}
     */
    public function joinAttribute($alias, $attribute, $bind, $filter = null, $joinType = 'inner', $storeId = null)
    {
        return $this->_getSubject()->joinAttribute($alias, $attribute, $bind, $filter, $joinType, $storeId);
    }

    /**
     * {@inheritdoc}
     */
    public function joinField($alias, $table, $field, $bind, $cond = null, $joinType = 'inner')
    {
        return $this->_getSubject()->joinField($alias, $table, $field, $bind, $cond, $joinType);
    }

    /**
     * {@inheritdoc}
     */
    public function joinTable($table, $bind, $fields = null, $cond = null, $joinType = 'inner')
    {
        return $this->_getSubject()->joinTable($table, $bind, $fields, $cond, $joinType);
    }

    /**
     * {@inheritdoc}
     */
    public function removeAttributeToSelect($attribute = null)
    {
        return $this->_getSubject()->removeAttributeToSelect($attribute);
    }

    /**
     * {@inheritdoc}
     */
    public function setPage($pageNum, $pageSize)
    {
        return $this->_getSubject()->setPage($pageNum, $pageSize);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllIds($limit = null, $offset = null)
    {
        return $this->_getSubject()->getAllIds($limit, $offset);
    }

    /**
     * {@inheritdoc}
     */
    public function getAllIdsSql()
    {
        return $this->_getSubject()->getAllIdsSql();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        return $this->_getSubject()->save();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        return $this->_getSubject()->delete();
    }

    /**
     * {@inheritdoc}
     */
    public function importFromArray($arr)
    {
        return $this->_getSubject()->importFromArray($arr);
    }

    /**
     * {@inheritdoc}
     */
    public function exportToArray()
    {
        return $this->_getSubject()->exportToArray();
    }

    /**
     * {@inheritdoc}
     */
    public function getRowIdFieldName()
    {
        return $this->_getSubject()->getRowIdFieldName();
    }

    /**
     * {@inheritdoc}
     */
    public function getIdFieldName()
    {
        return $this->_getSubject()->getIdFieldName();
    }

    /**
     * {@inheritdoc}
     */
    public function setRowIdFieldName($fieldName)
    {
        return $this->_getSubject()->setRowIdFieldName($fieldName);
    }

    /**
     * {@inheritdoc}
     */
    public function _loadEntities($printQuery = false, $logQuery = false)
    {
        return $this->_getSubject()->_loadEntities($printQuery, $logQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function _loadAttributes($printQuery = false, $logQuery = false)
    {
        return $this->_getSubject()->_loadAttributes($printQuery, $logQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function setOrder($attribute, $dir = 'ASC')
    {
        return $this->_getSubject()->setOrder($attribute, $dir);
    }

    /**
     * {@inheritdoc}
     */
    public function toArray($arrAttributes = [])
    {
        return $this->_getSubject()->toArray($arrAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function isAttributeAdded($attributeCode) : bool
    {
        return $this->_getSubject()->isAttributeAdded($attributeCode);
    }

    /**
     * {@inheritdoc}
     */
    public function getLoadedIds()
    {
        return $this->_getSubject()->getLoadedIds();
    }

    /**
     * {@inheritdoc}
     */
    public function clear()
    {
        return $this->_getSubject()->clear();
    }

    /**
     * {@inheritdoc}
     */
    public function removeAllItems()
    {
        return $this->_getSubject()->removeAllItems();
    }

    /**
     * {@inheritdoc}
     */
    public function removeItemByKey($key)
    {
        return $this->_getSubject()->removeItemByKey($key);
    }

    /**
     * {@inheritdoc}
     */
    public function getMainTable()
    {
        return $this->_getSubject()->getMainTable();
    }

    /**
     * {@inheritdoc}
     */
    public function addFieldToSelect($field, $alias = null)
    {
        return $this->_getSubject()->addFieldToSelect($field, $alias);
    }

    /**
     * {@inheritdoc}
     */
    public function removeFieldFromSelect($field)
    {
        return $this->_getSubject()->removeFieldFromSelect($field);
    }

    /**
     * {@inheritdoc}
     */
    public function removeAllFieldsFromSelect()
    {
        return $this->_getSubject()->removeAllFieldsFromSelect();
    }

    /**
     * {@inheritdoc}
     */
    public function addBindParam($name, $value)
    {
        return $this->_getSubject()->addBindParam($name, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function setConnection(\Magento\Framework\DB\Adapter\AdapterInterface $conn)
    {
        return $this->_getSubject()->setConnection($conn);
    }

    /**
     * {@inheritdoc}
     */
    public function getSelect()
    {
        return $this->_getSubject()->getSelect();
    }

    /**
     * {@inheritdoc}
     */
    public function getConnection()
    {
        return $this->_getSubject()->getConnection();
    }

    /**
     * {@inheritdoc}
     */
    public function getSize()
    {
        return $this->_getSubject()->getSize();
    }

    /**
     * {@inheritdoc}
     */
    public function getSelectCountSql()
    {
        return $this->_getSubject()->getSelectCountSql();
    }

    /**
     * {@inheritdoc}
     */
    public function getSelectSql($stringMode = false)
    {
        return $this->_getSubject()->getSelectSql($stringMode);
    }

    /**
     * {@inheritdoc}
     */
    public function addOrder($field, $direction = 'DESC')
    {
        return $this->_getSubject()->addOrder($field, $direction);
    }

    /**
     * {@inheritdoc}
     */
    public function unshiftOrder($field, $direction = 'DESC')
    {
        return $this->_getSubject()->unshiftOrder($field, $direction);
    }

    /**
     * {@inheritdoc}
     */
    public function distinct($flag)
    {
        return $this->_getSubject()->distinct($flag);
    }

    /**
     * {@inheritdoc}
     */
    public function loadWithFilter($printQuery = false, $logQuery = false)
    {
        return $this->_getSubject()->loadWithFilter($printQuery, $logQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function fetchItem()
    {
        return $this->_getSubject()->fetchItem();
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return $this->_getSubject()->getData();
    }

    /**
     * {@inheritdoc}
     */
    public function resetData()
    {
        return $this->_getSubject()->resetData();
    }

    /**
     * {@inheritdoc}
     */
    public function loadData($printQuery = false, $logQuery = false)
    {
        return $this->_getSubject()->loadData($printQuery, $logQuery);
    }

    /**
     * {@inheritdoc}
     */
    public function printLogQuery($printQuery = false, $logQuery = false, $sql = null)
    {
        return $this->_getSubject()->printLogQuery($printQuery, $logQuery, $sql);
    }

    /**
     * {@inheritdoc}
     */
    public function addFilterToMap($filter, $alias, $group = 'fields')
    {
        return $this->_getSubject()->addFilterToMap($filter, $alias, $group);
    }

    /**
     * {@inheritdoc}
     */
    public function joinExtensionAttribute(\Magento\Framework\Api\ExtensionAttribute\JoinDataInterface $join, \Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface $extensionAttributesJoinProcessor)
    {
        return $this->_getSubject()->joinExtensionAttribute($join, $extensionAttributesJoinProcessor);
    }

    /**
     * {@inheritdoc}
     */
    public function getItemObjectClass()
    {
        return $this->_getSubject()->getItemObjectClass();
    }

    /**
     * {@inheritdoc}
     */
    public function addFilter($field, $value, $type = 'and')
    {
        return $this->_getSubject()->addFilter($field, $value, $type);
    }

    /**
     * {@inheritdoc}
     */
    public function getFilter($field)
    {
        return $this->_getSubject()->getFilter($field);
    }

    /**
     * {@inheritdoc}
     */
    public function isLoaded()
    {
        return $this->_getSubject()->isLoaded();
    }

    /**
     * {@inheritdoc}
     */
    public function getCurPage($displacement = 0)
    {
        return $this->_getSubject()->getCurPage($displacement);
    }

    /**
     * {@inheritdoc}
     */
    public function getLastPageNumber()
    {
        return $this->_getSubject()->getLastPageNumber();
    }

    /**
     * {@inheritdoc}
     */
    public function getPageSize()
    {
        return $this->_getSubject()->getPageSize();
    }

    /**
     * {@inheritdoc}
     */
    public function getFirstItem()
    {
        return $this->_getSubject()->getFirstItem();
    }

    /**
     * {@inheritdoc}
     */
    public function getLastItem()
    {
        return $this->_getSubject()->getLastItem();
    }

    /**
     * {@inheritdoc}
     */
    public function getItems()
    {
        return $this->_getSubject()->getItems();
    }

    /**
     * {@inheritdoc}
     */
    public function getColumnValues($colName)
    {
        return $this->_getSubject()->getColumnValues($colName);
    }

    /**
     * {@inheritdoc}
     */
    public function getItemsByColumnValue($column, $value)
    {
        return $this->_getSubject()->getItemsByColumnValue($column, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function getItemByColumnValue($column, $value)
    {
        return $this->_getSubject()->getItemByColumnValue($column, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function walk($callback, array $args = [])
    {
        return $this->_getSubject()->walk($callback, $args);
    }

    /**
     * {@inheritdoc}
     */
    public function each($objMethod, $args = [])
    {
        return $this->_getSubject()->each($objMethod, $args);
    }

    /**
     * {@inheritdoc}
     */
    public function setDataToAll($key, $value = null)
    {
        return $this->_getSubject()->setDataToAll($key, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function setCurPage($page)
    {
        return $this->_getSubject()->setCurPage($page);
    }

    /**
     * {@inheritdoc}
     */
    public function setPageSize($size)
    {
        return $this->_getSubject()->setPageSize($size);
    }

    /**
     * {@inheritdoc}
     */
    public function setItemObjectClass($className)
    {
        return $this->_getSubject()->setItemObjectClass($className);
    }

    /**
     * {@inheritdoc}
     */
    public function getNewEmptyItem()
    {
        return $this->_getSubject()->getNewEmptyItem();
    }

    /**
     * {@inheritdoc}
     */
    public function toXml()
    {
        return $this->_getSubject()->toXml();
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionArray()
    {
        return $this->_getSubject()->toOptionArray();
    }

    /**
     * {@inheritdoc}
     */
    public function toOptionHash()
    {
        return $this->_getSubject()->toOptionHash();
    }

    /**
     * {@inheritdoc}
     */
    public function getItemById($idValue)
    {
        return $this->_getSubject()->getItemById($idValue);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return $this->_getSubject()->getIterator();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        return $this->_getSubject()->count();
    }

    /**
     * {@inheritdoc}
     */
    public function getFlag($flag)
    {
        return $this->_getSubject()->getFlag($flag);
    }

    /**
     * {@inheritdoc}
     */
    public function setFlag($flag, $value = null)
    {
        return $this->_getSubject()->setFlag($flag, $value);
    }

    /**
     * {@inheritdoc}
     */
    public function hasFlag($flag)
    {
        return $this->_getSubject()->hasFlag($flag);
    }
}
