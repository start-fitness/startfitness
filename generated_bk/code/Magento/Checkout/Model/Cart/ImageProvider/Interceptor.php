<?php
namespace Magento\Checkout\Model\Cart\ImageProvider;

/**
 * Interceptor class for @see \Magento\Checkout\Model\Cart\ImageProvider
 */
class Interceptor extends \Magento\Checkout\Model\Cart\ImageProvider implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Quote\Api\CartItemRepositoryInterface $itemRepository, \Magento\Checkout\CustomerData\ItemPoolInterface $itemPool, ?\Magento\Checkout\CustomerData\DefaultItem $customerDataItem = null)
    {
        $this->___init();
        parent::__construct($itemRepository, $itemPool, $customerDataItem);
    }

    /**
     * {@inheritdoc}
     */
    public function getImages($cartId)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getImages');
        if (!$pluginInfo) {
            return parent::getImages($cartId);
        } else {
            return $this->___callPlugins('getImages', func_get_args(), $pluginInfo);
        }
    }
}
