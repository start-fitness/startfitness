<?php
namespace Magento\Sales\Api\Data;

/**
 * Extension class for @see \Magento\Sales\Api\Data\CreditmemoInterface
 */
class CreditmemoExtension extends \Magento\Framework\Api\AbstractSimpleObject implements CreditmemoExtensionInterface
{
    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface|null
     */
    public function getAmGiftcardCreditmemo()
    {
        return $this->_get('am_giftcard_creditmemo');
    }

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface $amGiftcardCreditmemo
     * @return $this
     */
    public function setAmGiftcardCreditmemo(\Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface $amGiftcardCreditmemo)
    {
        $this->setData('am_giftcard_creditmemo', $amGiftcardCreditmemo);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmstorecreditBaseAmount()
    {
        return $this->_get('amstorecredit_base_amount');
    }

    /**
     * @param float $amstorecreditBaseAmount
     * @return $this
     */
    public function setAmstorecreditBaseAmount($amstorecreditBaseAmount)
    {
        $this->setData('amstorecredit_base_amount', $amstorecreditBaseAmount);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmstorecreditAmount()
    {
        return $this->_get('amstorecredit_amount');
    }

    /**
     * @param float $amstorecreditAmount
     * @return $this
     */
    public function setAmstorecreditAmount($amstorecreditAmount)
    {
        $this->setData('amstorecredit_amount', $amstorecreditAmount);
        return $this;
    }
}
