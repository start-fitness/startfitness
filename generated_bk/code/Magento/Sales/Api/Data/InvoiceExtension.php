<?php
namespace Magento\Sales\Api\Data;

/**
 * Extension class for @see \Magento\Sales\Api\Data\InvoiceInterface
 */
class InvoiceExtension extends \Magento\Framework\Api\AbstractSimpleObject implements InvoiceExtensionInterface
{
    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface|null
     */
    public function getAmGiftcardInvoice()
    {
        return $this->_get('am_giftcard_invoice');
    }

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface $amGiftcardInvoice
     * @return $this
     */
    public function setAmGiftcardInvoice(\Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface $amGiftcardInvoice)
    {
        $this->setData('am_giftcard_invoice', $amGiftcardInvoice);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmstorecreditBaseAmount()
    {
        return $this->_get('amstorecredit_base_amount');
    }

    /**
     * @param float $amstorecreditBaseAmount
     * @return $this
     */
    public function setAmstorecreditBaseAmount($amstorecreditBaseAmount)
    {
        $this->setData('amstorecredit_base_amount', $amstorecreditBaseAmount);
        return $this;
    }

    /**
     * @return float|null
     */
    public function getAmstorecreditAmount()
    {
        return $this->_get('amstorecredit_amount');
    }

    /**
     * @param float $amstorecreditAmount
     * @return $this
     */
    public function setAmstorecreditAmount($amstorecreditAmount)
    {
        $this->setData('amstorecredit_amount', $amstorecreditAmount);
        return $this;
    }
}
