<?php
namespace Magento\Sales\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Sales\Api\Data\OrderInterface
 */
interface OrderExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Magento\Sales\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments();

    /**
     * @param \Magento\Sales\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments);

    /**
     * @return \Magento\Payment\Api\Data\PaymentAdditionalInfoInterface[]|null
     */
    public function getPaymentAdditionalInfo();

    /**
     * @param \Magento\Payment\Api\Data\PaymentAdditionalInfoInterface[] $paymentAdditionalInfo
     * @return $this
     */
    public function setPaymentAdditionalInfo($paymentAdditionalInfo);

    /**
     * @return \Magento\GiftMessage\Api\Data\MessageInterface|null
     */
    public function getGiftMessage();

    /**
     * @param \Magento\GiftMessage\Api\Data\MessageInterface $giftMessage
     * @return $this
     */
    public function setGiftMessage(\Magento\GiftMessage\Api\Data\MessageInterface $giftMessage);

    /**
     * @return \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface[]|null
     */
    public function getAppliedTaxes();

    /**
     * @param \Magento\Tax\Api\Data\OrderTaxDetailsAppliedTaxInterface[] $appliedTaxes
     * @return $this
     */
    public function setAppliedTaxes($appliedTaxes);

    /**
     * @return \Magento\Tax\Api\Data\OrderTaxDetailsItemInterface[]|null
     */
    public function getItemAppliedTaxes();

    /**
     * @param \Magento\Tax\Api\Data\OrderTaxDetailsItemInterface[] $itemAppliedTaxes
     * @return $this
     */
    public function setItemAppliedTaxes($itemAppliedTaxes);

    /**
     * @return boolean|null
     */
    public function getConvertingFromQuote();

    /**
     * @param boolean $convertingFromQuote
     * @return $this
     */
    public function setConvertingFromQuote($convertingFromQuote);

    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardOrderInterface|null
     */
    public function getAmGiftcardOrder();

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardOrderInterface $amGiftcardOrder
     * @return $this
     */
    public function setAmGiftcardOrder(\Amasty\GiftCardAccount\Api\Data\GiftCardOrderInterface $amGiftcardOrder);

    /**
     * @return float|null
     */
    public function getAmstorecreditBaseAmount();

    /**
     * @param float $amstorecreditBaseAmount
     * @return $this
     */
    public function setAmstorecreditBaseAmount($amstorecreditBaseAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditAmount();

    /**
     * @param float $amstorecreditAmount
     * @return $this
     */
    public function setAmstorecreditAmount($amstorecreditAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditInvoicedBaseAmount();

    /**
     * @param float $amstorecreditInvoicedBaseAmount
     * @return $this
     */
    public function setAmstorecreditInvoicedBaseAmount($amstorecreditInvoicedBaseAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditInvoicedAmount();

    /**
     * @param float $amstorecreditInvoicedAmount
     * @return $this
     */
    public function setAmstorecreditInvoicedAmount($amstorecreditInvoicedAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditRefundedBaseAmount();

    /**
     * @param float $amstorecreditRefundedBaseAmount
     * @return $this
     */
    public function setAmstorecreditRefundedBaseAmount($amstorecreditRefundedBaseAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditRefundedAmount();

    /**
     * @param float $amstorecreditRefundedAmount
     * @return $this
     */
    public function setAmstorecreditRefundedAmount($amstorecreditRefundedAmount);

    /**
     * @return \Amazon\Payment\Api\Data\OrderLinkInterface|null
     */
    public function getAmazonOrderReferenceId();

    /**
     * @param \Amazon\Payment\Api\Data\OrderLinkInterface $amazonOrderReferenceId
     * @return $this
     */
    public function setAmazonOrderReferenceId(\Amazon\Payment\Api\Data\OrderLinkInterface $amazonOrderReferenceId);
}
