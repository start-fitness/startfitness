<?php
namespace Magento\Sales\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Sales\Api\Data\CreditmemoInterface
 */
interface CreditmemoExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface|null
     */
    public function getAmGiftcardCreditmemo();

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface $amGiftcardCreditmemo
     * @return $this
     */
    public function setAmGiftcardCreditmemo(\Amasty\GiftCardAccount\Api\Data\GiftCardCreditmemoInterface $amGiftcardCreditmemo);

    /**
     * @return float|null
     */
    public function getAmstorecreditBaseAmount();

    /**
     * @param float $amstorecreditBaseAmount
     * @return $this
     */
    public function setAmstorecreditBaseAmount($amstorecreditBaseAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditAmount();

    /**
     * @param float $amstorecreditAmount
     * @return $this
     */
    public function setAmstorecreditAmount($amstorecreditAmount);
}
