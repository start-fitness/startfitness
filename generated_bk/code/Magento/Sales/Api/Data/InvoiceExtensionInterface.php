<?php
namespace Magento\Sales\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Sales\Api\Data\InvoiceInterface
 */
interface InvoiceExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface|null
     */
    public function getAmGiftcardInvoice();

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface $amGiftcardInvoice
     * @return $this
     */
    public function setAmGiftcardInvoice(\Amasty\GiftCardAccount\Api\Data\GiftCardInvoiceInterface $amGiftcardInvoice);

    /**
     * @return float|null
     */
    public function getAmstorecreditBaseAmount();

    /**
     * @param float $amstorecreditBaseAmount
     * @return $this
     */
    public function setAmstorecreditBaseAmount($amstorecreditBaseAmount);

    /**
     * @return float|null
     */
    public function getAmstorecreditAmount();

    /**
     * @param float $amstorecreditAmount
     * @return $this
     */
    public function setAmstorecreditAmount($amstorecreditAmount);
}
