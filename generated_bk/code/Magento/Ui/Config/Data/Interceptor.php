<?php
namespace Magento\Ui\Config\Data;

/**
 * Interceptor class for @see \Magento\Ui\Config\Data
 */
class Interceptor extends \Magento\Ui\Config\Data implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct($componentName, \Magento\Ui\Config\ReaderFactory $readerFactory, \Magento\Framework\Config\CacheInterface $cache, \Magento\Framework\Serialize\SerializerInterface $serializer, \Magento\Framework\Data\Argument\InterpreterInterface $argumentInterpreter)
    {
        $this->___init();
        parent::__construct($componentName, $readerFactory, $cache, $serializer, $argumentInterpreter);
    }

    /**
     * {@inheritdoc}
     */
    public function merge(array $config)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'merge');
        if (!$pluginInfo) {
            return parent::merge($config);
        } else {
            return $this->___callPlugins('merge', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get($path = null, $default = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'get');
        if (!$pluginInfo) {
            return parent::get($path, $default);
        } else {
            return $this->___callPlugins('get', func_get_args(), $pluginInfo);
        }
    }
}
