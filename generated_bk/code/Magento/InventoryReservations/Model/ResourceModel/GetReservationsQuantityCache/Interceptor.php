<?php
namespace Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantityCache;

/**
 * Interceptor class for @see \Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantityCache
 */
class Interceptor extends \Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantityCache implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\InventoryReservations\Model\ResourceModel\GetReservationsQuantity $getReservationsQuantity)
    {
        $this->___init();
        parent::__construct($getReservationsQuantity);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(string $sku, int $stockId) : float
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'execute');
        if (!$pluginInfo) {
            return parent::execute($sku, $stockId);
        } else {
            return $this->___callPlugins('execute', func_get_args(), $pluginInfo);
        }
    }
}
