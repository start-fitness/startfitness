<?php
namespace Magento\Quote\Api\Data;

/**
 * ExtensionInterface class for @see \Magento\Quote\Api\Data\CartInterface
 */
interface CartExtensionInterface extends \Magento\Framework\Api\ExtensionAttributesInterface
{
    /**
     * @return \Magento\Quote\Api\Data\ShippingAssignmentInterface[]|null
     */
    public function getShippingAssignments();

    /**
     * @param \Magento\Quote\Api\Data\ShippingAssignmentInterface[] $shippingAssignments
     * @return $this
     */
    public function setShippingAssignments($shippingAssignments);

    /**
     * @return mixed[]|null
     */
    public function getAmAppliedGiftCards();

    /**
     * @param mixed[] $amAppliedGiftCards
     * @return $this
     */
    public function setAmAppliedGiftCards($amAppliedGiftCards);

    /**
     * @return \Amasty\GiftCardAccount\Api\Data\GiftCardQuoteInterface|null
     */
    public function getAmGiftcardQuote();

    /**
     * @param \Amasty\GiftCardAccount\Api\Data\GiftCardQuoteInterface $amGiftcardQuote
     * @return $this
     */
    public function setAmGiftcardQuote(\Amasty\GiftCardAccount\Api\Data\GiftCardQuoteInterface $amGiftcardQuote);

    /**
     * @return \Amazon\Payment\Api\Data\QuoteLinkInterface|null
     */
    public function getAmazonOrderReferenceId();

    /**
     * @param \Amazon\Payment\Api\Data\QuoteLinkInterface $amazonOrderReferenceId
     * @return $this
     */
    public function setAmazonOrderReferenceId(\Amazon\Payment\Api\Data\QuoteLinkInterface $amazonOrderReferenceId);
}
