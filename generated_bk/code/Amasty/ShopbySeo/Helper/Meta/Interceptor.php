<?php
namespace Amasty\ShopbySeo\Helper\Meta;

/**
 * Interceptor class for @see \Amasty\ShopbySeo\Helper\Meta
 */
class Interceptor extends \Amasty\ShopbySeo\Helper\Meta implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\Helper\Context $context, \Amasty\Shopby\Helper\Data $dataHelper, \Amasty\ShopbySeo\Helper\Data $seoHelper, \Magento\Framework\Registry $registry, \Magento\Framework\App\Request\Http $request, \Amasty\ShopbyBase\Model\Integration\IntegrationFactory $integrationFactory)
    {
        $this->___init();
        parent::__construct($context, $dataHelper, $seoHelper, $registry, $request, $integrationFactory);
    }

    /**
     * {@inheritdoc}
     */
    public function setPageTags(\Magento\Framework\View\Page\Config $pageConfig)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'setPageTags');
        if (!$pluginInfo) {
            return parent::setPageTags($pageConfig);
        } else {
            return $this->___callPlugins('setPageTags', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIndexTagValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIndexTagValue');
        if (!$pluginInfo) {
            return parent::getIndexTagValue();
        } else {
            return $this->___callPlugins('getIndexTagValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFollowTagValue()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFollowTagValue');
        if (!$pluginInfo) {
            return parent::getFollowTagValue();
        } else {
            return $this->___callPlugins('getFollowTagValue', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIndexTagByData($indexTag, \Magento\Framework\DataObject $data)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getIndexTagByData');
        if (!$pluginInfo) {
            return parent::getIndexTagByData($indexTag, $data);
        } else {
            return $this->___callPlugins('getIndexTagByData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFollowTagByData($followTag, \Magento\Framework\DataObject $data)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFollowTagByData');
        if (!$pluginInfo) {
            return parent::getFollowTagByData($followTag, $data);
        } else {
            return $this->___callPlugins('getFollowTagByData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getTagByData($tagKey, $tagValue, $data)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getTagByData');
        if (!$pluginInfo) {
            return parent::getTagByData($tagKey, $tagValue, $data);
        } else {
            return $this->___callPlugins('getTagByData', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isFollowingAllowed()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isFollowingAllowed');
        if (!$pluginInfo) {
            return parent::isFollowingAllowed();
        } else {
            return $this->___callPlugins('isFollowingAllowed', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModifyRobotsEnable()
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModifyRobotsEnable');
        if (!$pluginInfo) {
            return parent::isModifyRobotsEnable();
        } else {
            return $this->___callPlugins('isModifyRobotsEnable', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function isModuleOutputEnabled($moduleName = null)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'isModuleOutputEnabled');
        if (!$pluginInfo) {
            return parent::isModuleOutputEnabled($moduleName);
        } else {
            return $this->___callPlugins('isModuleOutputEnabled', func_get_args(), $pluginInfo);
        }
    }
}
