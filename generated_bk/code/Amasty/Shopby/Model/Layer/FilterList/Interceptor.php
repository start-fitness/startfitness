<?php
namespace Amasty\Shopby\Model\Layer\FilterList;

/**
 * Interceptor class for @see \Amasty\Shopby\Model\Layer\FilterList
 */
class Interceptor extends \Amasty\Shopby\Model\Layer\FilterList implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\ObjectManagerInterface $objectManager, \Magento\Catalog\Model\Layer\FilterableAttributeListInterface $filterableAttributes, \Amasty\Base\Model\MagentoVersion $magentoVersion, \Amasty\Shopby\Helper\FilterSetting $filterSettingHelper, \Magento\Framework\App\Request\Http $request, \Magento\Framework\Registry $registry, \Amasty\ShopbyBase\Model\ResourceModel\FilterSetting\CollectionExtendedFactory $collectionExtendedFactory, \Amasty\Shopby\Model\Request $shopbyRequest, \Amasty\Shopby\Helper\Config $config, \Magento\Framework\View\LayoutInterface $layout, array $filters = [], $place = 'sidebar')
    {
        $this->___init();
        parent::__construct($objectManager, $filterableAttributes, $magentoVersion, $filterSettingHelper, $request, $registry, $collectionExtendedFactory, $shopbyRequest, $config, $layout, $filters, $place);
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters(\Magento\Catalog\Model\Layer $layer)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFilters');
        if (!$pluginInfo) {
            return parent::getFilters($layer);
        } else {
            return $this->___callPlugins('getFilters', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAllFilters(\Magento\Catalog\Model\Layer $layer)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getAllFilters');
        if (!$pluginInfo) {
            return parent::getAllFilters($layer);
        } else {
            return $this->___callPlugins('getAllFilters', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function sortingByPosition($first, $second)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'sortingByPosition');
        if (!$pluginInfo) {
            return parent::sortingByPosition($first, $second);
        } else {
            return $this->___callPlugins('sortingByPosition', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getFilterPosition($filter)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'getFilterPosition');
        if (!$pluginInfo) {
            return parent::getFilterPosition($filter);
        } else {
            return $this->___callPlugins('getFilterPosition', func_get_args(), $pluginInfo);
        }
    }
}
