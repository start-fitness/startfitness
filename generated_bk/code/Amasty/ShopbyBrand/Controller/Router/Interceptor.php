<?php
namespace Amasty\ShopbyBrand\Controller\Router;

/**
 * Interceptor class for @see \Amasty\ShopbyBrand\Controller\Router
 */
class Interceptor extends \Amasty\ShopbyBrand\Controller\Router implements \Magento\Framework\Interception\InterceptorInterface
{
    use \Magento\Framework\Interception\Interceptor;

    public function __construct(\Magento\Framework\App\ActionFactory $actionFactory, \Magento\Framework\App\ResponseInterface $response, \Magento\Framework\Registry $registry, \Magento\Framework\UrlInterface $urlBuilder, \Magento\Framework\Module\Manager $moduleManager, \Amasty\ShopbyBrand\Helper\Data $brandHelper, \Amasty\ShopbyBase\Helper\PermissionHelper $permissionHelper)
    {
        $this->___init();
        parent::__construct($actionFactory, $response, $registry, $urlBuilder, $moduleManager, $brandHelper, $permissionHelper);
    }

    /**
     * {@inheritdoc}
     */
    public function match(\Magento\Framework\App\RequestInterface $request)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'match');
        if (!$pluginInfo) {
            return parent::match($request);
        } else {
            return $this->___callPlugins('match', func_get_args(), $pluginInfo);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function matchBrandParams($identifier)
    {
        $pluginInfo = $this->pluginList->getNext($this->subjectType, 'matchBrandParams');
        if (!$pluginInfo) {
            return parent::matchBrandParams($identifier);
        } else {
            return $this->___callPlugins('matchBrandParams', func_get_args(), $pluginInfo);
        }
    }
}
