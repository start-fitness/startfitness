<?php

$file = fopen(dirname(__FILE__)."/producturlfix.csv", "r");

$pids = [];
while($line = fgetcsv($file)){
	//print_r($pids);
	if(count($pids) >=1000){
		$ids = implode(" ", $pids);
		$pids = [];
		$path = dirname(__FILE__);
		$command = "cd {$path}; php-7.2 -d memory_limit=8G bin/magento regenerate:product:url -s6 ".$ids;
		echo $command ."\n";
		echo shell_exec($command);
	}
	$pids[] = $line[0];
}
if(count($pids) >=1){
	$ids = implode(" ", $pids);
	$pids = [];
	$path = dirname(__FILE__);
	$command = "cd {$path}; php-7.2 -d memory_limit=8G bin/magento regenerate:product:url -s6 ".$ids;
	echo shell_exec($command);
}