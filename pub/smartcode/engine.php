<?php

/**
 * Class Smartcode
 */
class Smartcode
{
	/**
	 * Barcode attribute code in magento store
	 */
	const BARCODE_ATTR_CODE = 'sku';

	/**
	 * @var \Magento\Framework\ObjectManagerInterface
	 */
	protected $_objectManager;

	/**
	 * Smartcode constructor.
	 * @param \Magento\Framework\ObjectManagerInterface $objectManager
	 */
	public function __construct(
		\Magento\Framework\ObjectManagerInterface $objectManager
	)
	{
		$this->_objectManager = $objectManager;
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function getOrder($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('frontend');

			$order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($params['orderId']);

			if (!$order->getId())
			{
				throw new Exception('Order not found');
			}

			$data = $this->getOrderData($order);
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function getOrdersByStatus($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('frontend');

			$data = array(
				"pageNumber" => 0,
				"entriesPerPage" => 500,
				"totalEntries" => 0,
				"totalPages" => 0,
				"orders" => array()
			);

			$ordersCollection = $this->_objectManager->create('\Magento\Sales\Model\ResourceModel\Order\Collection');
			$ordersCollection
				->addFieldToSelect('*')
				->addFieldToFilter('status', ['in' => $params['status']])
				->setPageSize(isset($params['entriesPerPage']) ? $params['entriesPerPage'] : 500)
				->setCurPage(isset($params['pageNumber']) ? $params['pageNumber'] : 1)
				->setOrder('created_at', 'desc')
				;

			$data['pageNumber'] = $ordersCollection->getCurPage();
			$data['entriesPerPage'] = $ordersCollection->getPageSize();
			$data['totalEntries'] = $ordersCollection->getSize();
			$data['totalPages'] = $ordersCollection->getLastPageNumber();

			foreach ($ordersCollection as $order)
			{
				if ($order->getIncrementId() == "6000852073") continue;
				$data['orders'][] = $this->getOrderData($order);
			}
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function setOrder($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('frontend');

			$order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($params['orderId']);

			if (!$order->getId())
			{
				throw new Exception('Order not found');
			}

			$order->setState($params['status'])->setStatus($params['status']);
			$order->save();

			$data = $this->getFailureOrSuccessData();
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function setOrderNote($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('frontend');

			$order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($params['orderId']);

			if (!$order->getId())
			{
				throw new Exception('Order not found');
			}

			$order->addStatusToHistory($order->getStatus(), $params['note'], false);
			$order->save();

			$data = $this->getFailureOrSuccessData();
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function setShipping($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('frontend');

			$order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($params['orderId']);

			if (!$order->getId())
			{
				throw new Exception('Order not found');
			}

			if (!$order->hasShipments())
			{
				throw new Exception('Order has no shipment');
			}

			$shipment = $this->getShipment($order);

			$data = [
				'carrier_code' => $params['courier'],
				'title' => 'Received from Smartcode',
				'number' => $params['tracking'],
			];

			$track = $this->_objectManager->create('Magento\Sales\Model\Order\Shipment\TrackFactory')->create()->addData($data);
			$shipment->addTrack($track)->save();
			$shipment->save();
			$shipment->getOrder()->save();

			$data = $this->getFailureOrSuccessData();
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function getStock($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('adminhtml');
			$product = $this->getProduct($params['barcode'], "sku");
			$stockState = $this->_objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');

			if (!$product)
			{
				throw new Exception('Product not found');
			}

			$qty = $stockState->getStockQty($product->getId(), $product->getStore()->getWebsiteId());
			$productName = $product->getName();
			$stockStatus = $qty > 0 ? 'In Stock' : 'Out of Stock';
			$image_url = $this->getImageUrl($product);

			$data = [
				'barcode' => '',
				'barcodeRecognised' => true,
				'beepOveride' => 0,
				'scanType' => 1,
				'stockInfo' => [
					'barcode' => $params['barcode'],
					'additionalBarcodes' => [],
					'itemName' => $productName,
					'quantity' => $qty,
					'stockStatus' => $stockStatus,
					'stockStatusColour' => '00a800',
					'isEditable' => true,
					'alternativeDisplay' => '',
					'stockData' => [
						[
							'name' => 'Product Name',
							'value' => $productName,
							'displayOnMain' => false,
							'display' => true
						],
						[
							'name' => 'Stock ID',
							'value' => $product->getId(),
							'displayOnMain' => false,
							'display' => true
						],
						[
							'name' => 'SKU',
							'value' => $product->getSku(),
							'displayOnMain' => false,
							'display' => true
						]
					],
					'stockDetails' => [],
					'stockLevels' => [],
					'imageURLS' => [$image_url]
				],
				'assetInfo' => [
					'barcode' => '',
					'additionalBarcodes' => [],
					'name' => '',
					'image' => '',
					'status' => '',
					'location' => '',
					'assetData' => []
				],
				'formFragments' => [],
				'success' => true,
				'detail' => ''
			];
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param $params
	 * @return string
	 */
	public function setStockLevel($params)
	{
		try
		{
			$state = $this->_objectManager->get('\Magento\Framework\App\State');
			$state->setAreaCode('adminhtml');
			$product = $this->getProduct($params['barcode'], "sku");

			if (!$product)
			{
				throw new Exception('Product not found');
			}

			$isInStock = $params['value'] > 0 ? 1 : 0;

			$product->setStockData(['qty' => $params['value'], 'is_in_stock' => $isInStock]);
			$product->setQuantityAndStockStatus(['qty' => $params['value'], 'is_in_stock' => $isInStock]);
			$product->save();

			$data = $this->getFailureOrSuccessData();
		}
		catch (Exception $e)
		{
			$data = $this->getFailureOrSuccessData($e->getMessage());
		}

		return json_encode($data);
	}

	/**
	 * @param \Magento\Sales\Model\Order $order
	 * @return array
	 */
	protected function getOrderData(\Magento\Sales\Model\Order $order)
	{
		$allShippingCourier = $this->_objectManager->get('\Magento\Shipping\Model\Config\Source\Allmethods')->toOptionArray();
		$billingAddress = $order->getBillingAddress();
		$shippingAddress = $order->getShippingAddress();
		$shippingMethod = $order->getShippingMethod();
		$shippingInfo = explode('_', $shippingMethod, 2);
		$courierName = isset($allShippingCourier[$shippingInfo[0]]) ? $allShippingCourier[$shippingInfo[0]]['label'] : '';
		$courierService = '';

		if ($courierName != '')
		{
			foreach ($allShippingCourier[$shippingInfo[0]]['value'] as $rate)
			{
				if ($shippingMethod == $rate['value'])
				{
					$courierService = $rate['label'];
				}
			}
		}

		$data = [
			'orderId' => $order->getIncrementId(),
			'orderAlias' => '',
			'displayPriority' => 0,
			'isPriorityOrder' => false,
			'orderType' => 5,
			'previouslySaved' => false,
			'orderDate' => strtotime($order->getCreatedAt()),
			'firstname' => $shippingAddress && $shippingAddress->getFirstName() ? $shippingAddress->getFirstName() : $billingAddress->getFirstName(),
			'surname' => $shippingAddress && $shippingAddress->getLastname() ? $shippingAddress->getLastname() : $billingAddress->getLastname(),
			'email' => $shippingAddress && $shippingAddress->getEmail() ? $shippingAddress->getEmail() : $billingAddress->getEmail(),
			'telephone' => $shippingAddress && $shippingAddress->getTelephone() ? $shippingAddress->getTelephone() : $billingAddress->getTelephone(),
			'deliveryCompany' => $shippingAddress && $shippingAddress->getCompany() ? $shippingAddress->getCompany() : $billingAddress->getCompany(),
			'deliveryAddressLine1' => $shippingAddress && $shippingAddress->getStreetLine(1) ? $shippingAddress->getStreetLine(1) : $billingAddress->getStreetLine(1),
			'deliveryAddressLine2' => $shippingAddress && $shippingAddress->getStreetLine(2) ? $shippingAddress->getStreetLine(2) : $billingAddress->getStreetLine(2),
			'deliveryAddressLine3' => $shippingAddress && $shippingAddress->getStreetLine(3) ? $shippingAddress->getStreetLine(3) : $billingAddress->getStreetLine(3),
			'deliveryCity' => $shippingAddress && $shippingAddress->getCity() ? $shippingAddress->getCity() : $billingAddress->getCity(),
			'deliveryCounty' => $shippingAddress && $shippingAddress->getRegion() ? $shippingAddress->getRegion() : $billingAddress->getRegion(),
			'deliveryPostcode' => $shippingAddress && $shippingAddress->getPostcode() ? $shippingAddress->getPostcode() : $billingAddress->getPostcode(),
			'deliveryCountry' => $shippingAddress && $shippingAddress->getCountryId() ? $shippingAddress->getCountryId() : $billingAddress->getCountryId(),
			'orderTotal' => $order->getGrandTotal(),
			'shippingTotal' => $order->getShippingAmount(),
			'orderWeight' => $order->getWeight(),
			'orderStatus' => $order->getStatus(),
			'source' => $order->getStoreName(),
			'externalRef' => '',
			'comments' => '',
			'tote' => '',
			'orderLines' => [],
			'labelEnabled' => false,
			'triggerLabelPrint' => false,
			'toteEnabled' => false,
			'userId' => 0,
			'orderLabelInfo' => [
				'courierName' => $courierName,
				'courierService' => $courierService,
				'altCourierServices' => [],
				'selectedCourierService' => '',
				'labelInputRequired' => false,
				'labelInputString' => '',
				'labelInputValue' => '',
				'noOfLabels' => 1,
				'courierTracking' => '',
			],
			'excludeDynamicForms' => [],
			'tag' => '',
		];

		global $orderDataModelNoObject, $orderDataModelNoMethod;
		global $orderDataSkuObject, $orderDataSkuMethod;
		global $orderDataBarcodeValueObject, $orderDataBarcodeValueMethod;

		$counter = 0;
		foreach ($order->getAllVisibleItems() as $item)
		{
			$product = $item->getProduct();

			$data['orderLines'][$counter]['correctCount'] = 0;
			$data['orderLines'][$counter]['productName'] = $product->getName();

			getOrderDataSkuBits($data['orderLines'][$counter], $product, $item);

			$data['orderLines'][$counter]['additionalBarcodes']= [];
			$data['orderLines'][$counter]['correctBarcodes'] = [];
			$data['orderLines'][$counter]['bomLines'] = [];
			$data['orderLines'][$counter]['pickLocation'] = "";
			$data['orderLines'][$counter]['packLocation'] = "";
			$data['orderLines'][$counter]['weight'] = $product->getWeight();
			$data['orderLines'][$counter]['price'] = $item->getPrice();
			$data['orderLines'][$counter]['quantity'] = $item->getQtyOrdered();
			$data['orderLines'][$counter]['status'] = 0;
			$data['orderLines'][$counter]['verified'] = false;
			$data['orderLines'][$counter]['orderId'] = $order->getIncrementId();

			$counter++;
		}

		return $data;
	}

	/**
	 * @param $barcode
	 * @return mixed
	 */
	protected function getProduct($barcode)
	{
		return $this->_objectManager->create('Magento\Catalog\Model\Product')->loadByAttribute("sku", $barcode);
	}

	/**
	 * @param $product
	 * @return mixed
	 */
	protected function getImageUrl($product)
	{
		$appEmulation = $this->_objectManager->get('\Magento\Store\Model\App\Emulation');
		$appEmulation->startEnvironmentEmulation(0, \Magento\Framework\App\Area::AREA_FRONTEND, true);

		$imageUrl = $this->_objectManager->get('\Magento\Catalog\Helper\Image')->init($product, 'product_page_image_large')->getUrl();

		$appEmulation->stopEnvironmentEmulation();

		return $imageUrl;
	}

	/**
	 * @param null $errorMessage
	 * @return array
	 */
	protected function getFailureOrSuccessData($errorMessage = null)
	{
		if ($errorMessage)
		{
			$data = [
				"success" => false,
				"error" => __($errorMessage),
				"value" => ""
			];
		}
		else
		{
			$data = [
				"success" => true,
				"error" => "",
				"value" => ""
			];
		}

		return $data;
	}

	/**
	 * @param $order
	 * @return mixed
	 */
	protected function getShipment($order)
	{
		$shipmentsCollection = array_reverse($order->getShipmentsCollection()->getItems());
		return $shipmentsCollection[0];
	}
}
