<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();
// Login as Admin user
//Mage::getSingleton('admin/session')->login("autoprint","F^r!34zy%@*sWn.e");

// Set Timeouts and reporting
//Mage::setIsDeveloperMode(true); // Turn This off for it to work
ini_set('memory_limit','768M');
ini_set('display_errors',1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);


$sql = 'SELECT alloc.*, orders.increment_id FROM `raw_courriermanager_allocations` alloc 
JOIN sales_order orders on orders.entity_id = alloc.order_id
WHERE increment_id = "'.$_GET['orderid'].'" ORDER BY allocations_id DESC';
$data = $read->fetchAll($sql);
if(count($data) > 0){
	if($_GET['status'] == "success"){
		$data = $data[0];
		$sql = "UPDATE raw_courriermanager_allocations SET number = '".$_GET['trackingno']."', data = '".json_encode($_GET)."' WHERE allocations_id = ".$data['allocations_id'];
		$read->query($sql);
	}
	else{
		$data = $data[0];
		$sql = "UPDATE raw_courriermanager_allocations SET number = 'FAILED', data = '".json_encode($_GET)."' WHERE allocations_id = ".$data['allocations_id'];
		$read->query($sql);
	}
	
}

file_put_contents(dirname(__FILE__)."/logs/".date("Y-m-d His").".txt", implode(",", array_keys($_GET))."\n".implode(",", $_GET));