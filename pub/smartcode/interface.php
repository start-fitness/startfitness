<?php

/* START FITNESS Magento 2 */

header("Content-Type: application/json");
ini_set("display_errors", 0);
ini_set("display_startup_errors", 0);
ini_set("max_execution_time", 300);
error_reporting(E_NONE);


// config
$ipsAllowed = array(
	"82.42.115.199", // ryan
	"82.43.105.212", // chris
	"217.155.197.149", // chris
	"35.166.75.240", // smartcode
	"35.176.177.120", // smartcode
	"159.65.209.69", // smartcode
	"161.35.164.100", // smartcode
	"161.35.173.177", // smartcode
	"68.183.38.106",
	"209.97.179.120"
);


$validAuth = array(
	"smartcode" => "p842svLCusVpuFtD"
);
$validAuthUsernames = array_keys($validAuth);


function getOrderDataSkuBits(&$data, &$product, &$item) // used in engine.php:getOrderData(
{
	$data["modelNo"] = "";
	$data["sku"] = $product->getActualModel();
	$data["barcodeValue"] = $item->getData('sku');
}
// /config


$userIp = "";
if (!empty($_SERVER["HTTP_CLIENT_IP"])) $userIp = $_SERVER["HTTP_CLIENT_IP"];
else if (!empty($_SERVER["HTTP_X_FORWARDED_FOR"])) $userIp = $_SERVER["HTTP_X_FORWARDED_FOR"];
else $userIp = $_SERVER["REMOTE_ADDR"];

if (in_array($userIp, $ipsAllowed) === false)
{
	http_response_code(403);

	$response["success"] = false;
	$response["error"] = $_SERVER["REMOTE_ADDR"] . ", you are not permitted to access this file";

	echo json_encode($response);

	exit(0);
}


/*if (isset($_SERVER["PHP_AUTH_USER"]) == false OR isset($_SERVER["PHP_AUTH_PW"]) == false OR
	$_SERVER["PHP_AUTH_USER"] === "" OR $_SERVER["PHP_AUTH_PW"] === "" OR
	in_array($_SERVER["PHP_AUTH_USER"], $validAuthUsernames, true) === false OR ($_SERVER["PHP_AUTH_PW"] !== $validAuth[$_SERVER["PHP_AUTH_USER"]]))
{
	http_response_code(401);

	$response = array();
	$response["success"] = false;
	$response["error"] = "Invalid auth";
	
	echo json_encode($response);
	
	exit(0);
}*/


use Magento\Framework\App\Bootstrap;

include("../../app/bootstrap.php");
include("engine.php");

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();


$action = isset($_GET["action"]) ? $_GET["action"] : null;

if ($action !== null) {
	$smartcode = new Smartcode($objectManager);
	$response = "{}";

	switch ($action)
	{
		case "getOrder":
			$response = $smartcode->getOrder($_GET);
			break;
		case "getOrdersByStatus":
			$response = $smartcode->getOrdersByStatus($_GET);
			break;
		case "setOrder":
			$response = $smartcode->setOrder($_GET);
			break;
		case "setOrderNote":
			$response = $smartcode->setOrderNote($_GET);
			break;
		case "setShipping":
			$response = $smartcode->setShipping($_GET);
			break;
		case "getStock":
			$response = $smartcode->getStock($_GET);
			break;
		case "setStockLevel":
			$response = $smartcode->setStockLevel($_GET);
			break;
	}

	echo $response;
}
else
{
	echo "<strong>action is missing.</strong>";
}
