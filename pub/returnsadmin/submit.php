<?php
header('Content-Type: application/json');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//require dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
//require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs_new/app/Mage.php'); //Path to Magento
use Magento\Framework\App\Bootstrap;

include("../../app/bootstrap.php");

$bootstrap = Bootstrap::create(BP, $_SERVER);
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('global');
$storeManager = $objectManager->create('\Magento\Store\Model\StoreManagerInterface');
$register = $objectManager->create('\Magento\Framework\Registry');
$transportManager = $objectManager->create('\Magento\Framework\Mail\Template\TransportBuilder');
$store_id = 6;
$storeManager->setCurrentStore($store_id);
$config = $objectManager->create('\Magento\Framework\App\Config\ScopeConfigInterface');

$code = $_POST['barcode'];

$order = $objectManager->create('Magento\Sales\Model\Order')->loadByIncrementId($code);
$orderdata = ($order->getData());
if(!$order || count($orderdata) == 0){
	echo json_encode(array("status" => "nok"));
}
else{
	$register->unregister("current_order");
	$register->register("current_order", $order);
	//$order->setState(Mage_Sales_Model_Order::STATE_COMPLETE, true)->save();
	$order->setStatus("processing_return");
	$order->addStatusToHistory($order->getStatus(), 'Package scanned in for return processing.', false);
	$order->save();
	
	$sender  = array(
        'name' => $config->getValue('trans_email/ident_support/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, 6),
        'email' => $config->getValue('trans_email/ident_support/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, 6)
    );
     $vars = array(
    //'item_html' => $item_html,
    "customername" => $order->getCustomerName()
    );
    $templateId = 46;
	
	
	try {
		$store_id = $order->getStoreId();
		if($store_id == 28) { // Sports Size
			$templateId = 55;
			$sender  = array(
				'name' => $config->getValue('trans_email/ident_support/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE, 28),
				'email' => $config->getValue('trans_email/ident_support/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, 28)
			);
		}		
	} catch(Exception $e) {
	
	}
	
    $transport = $transportManager->setTemplateIdentifier($templateId)
            ->setTemplateOptions(['area' => Magento\Framework\App\Area::AREA_FRONTEND, 'store' => $store_id])
            ->setTemplateVars([])
            ->setFrom($sender)
            ->addTo($order->getCustomerEmail() , $order->getCustomerName())

            ->getTransport();
    $transport->sendMessage();
	
	echo  json_encode(array("status" => "ok"));
}
