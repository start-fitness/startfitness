<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Startfitness return panel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

    <!-- Custom styles for this template -->

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body style="margin-top: 10px;">
	
    <div class="container">


      <div class="jumbotron">
        <h1>Startfitness Returns Panel</h1>
      </div>

      <div class="row">
      	<div class="col-md-1">
      		<div class="alert alert-success" id="successalert" role="alert">Return processed successfully!</div>
      		<div class="alert alert-danger" id="failedalert" role="alert">Error while processing the return.</div>
      	</div>
        <div class="col-md-1">
        	<form id="returnform">
        		<div class="form-group">
        			<label for="exampleInputEmail1">Return barcode:</label>
    				<input type="text" class="form-control" id="barcode" placeholder="xxxxxxxxxxxx">
        		</div>
        		<button class="btn btn-default" type="submit">Scan</button>	
        	</form>
        </div>
      </div>

      <footer class="footer">
        
      </footer>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    
  </body>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
  <script>
  	jQuery(document).ready(function($){
  		$("#successalert").hide();
  		$("#failedalert").hide();
  	});
  	jQuery("#returnform").submit(function(e){
  		e.preventDefault();
  		//alert("submit");
  		$.post( "/returnsadmin/submit.php", {barcode : jQuery("#barcode").val()}, function( data ) {
		  if(data.status != "ok"){
		  	$("#failedalert").show();
		  	setTimeout(function(){$("#failedalert").hide();}, 3000);
		  }
		  else{
		  	$("#successalert").show();
		  	setTimeout(function(){$("#successalert").hide();}, 3000);
		  }
		  jQuery("#barcode").val("");jQuery("#barcode").focus();
		});
  	});
  	jQuery("#barcode").keypress(function(e){
  		if(e.which == 13){
  			e.preventDefault();
  			jQuery("#returnform").submit();
  		}
  	});
  </script>
</html>
