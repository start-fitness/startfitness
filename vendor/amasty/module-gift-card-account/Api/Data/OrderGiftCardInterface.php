<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_GiftCardAccount
 */


declare(strict_types=1);

namespace Amasty\GiftCardAccount\Api\Data;

interface OrderGiftCardInterface
{
    /**
     * @return int
     */
    public function getGiftCardId(): int;

    /**
     * @param int $giftCardId
     *
     * @return \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface
     */
    public function setGiftCardId(int $giftCardId): \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface;

    /**
     * @return string
     */
    public function getGiftCardCode(): string;

    /**
     * @param string $giftCardCode
     *
     * @return \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface
     */
    public function setGiftCardCode(string $giftCardCode): \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface;

    /**
     * @return float
     */
    public function getGiftCardAmount(): float;

    /**
     * @param float $giftAmount
     *
     * @return \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface
     */
    public function setGiftCardAmount(float $giftAmount): \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface;

    /**
     * @return float
     */
    public function getGiftCardBaseAmount(): float;

    /**
     * @param float $baseGiftAmount
     *
     * @return \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface
     */
    public function setGiftCardBaseAmount(
        float $baseGiftAmount
    ): \Amasty\GiftCardAccount\Api\Data\OrderGiftCardInterface;
}
