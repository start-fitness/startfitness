<?php

namespace Amasty\StoreCredit\Observer;

use Amasty\StoreCredit\Api\Data\SalesFieldInterface;
use Magento\Paypal\Model\Cart as PayPalCart;

class RemoveStoreCreditFromPayment implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Amasty\StoreCredit\Model\ConfigProvider
     */
    private $configProvider;

    public function __construct(\Amasty\StoreCredit\Model\ConfigProvider $configProvider)
    {
        $this->configProvider = $configProvider;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->configProvider->isEnabled()) {
            $cart = $observer->getData('cart');
            $salesEntity = $cart->getSalesModel();
            $usingMethod = $salesEntity->getDataUsingMethod('entity_type');

            if ($usingMethod === 'order'
                || $salesEntity->getDataUsingMethod(SalesFieldInterface::AMSC_USE)
                || ($cart instanceof PayPalCart && $usingMethod === 'invoice')
            ) {
                $value = abs($salesEntity->getDataUsingMethod(SalesFieldInterface::AMSC_BASE_AMOUNT));
                if ($value > 0.0001) {
                    $cart->addDiscount((double)$value);
                }
            }
        }
    }
}
