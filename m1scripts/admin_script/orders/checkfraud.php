<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

echo "Releasing stocks on  orders order than 7 days ...\n";
$timetocheck = date("Y-m-d H:i:s", strtotime("-7 days"));
$orders = Mage::getModel('sales/order')->getCollection()
    ->addFieldToFilter('status', array('in'=> array('fraud_giftcard', 'pending_auth')))
	->addFieldToFilter('created_at', array(
            'to' => $timetocheck,
            ))
	->addAttributeToSelect('*');
//var_dump($orders->getSelect()."");exit;
foreach ($orders as $order) {
	
	var_dump($order->canCreditmemo(), $order->getIncrementId());
	if ($order->canCreditmemo()) {
		try {
			foreach ($order->getInvoiceCollection() as $invoice) {
			    $invoices[] = $invoice;
			}
			$service = Mage::getModel('sales/service_order', $order);
			foreach ($invoices as $invoice) {
			    $creditmemo = $service->prepareInvoiceCreditmemo($invoice);
				$creditmemo->setPaymentRefundDisallowed(true)->register();
			    /*$creditmemo->register();*/ //$creditmemo->refund();
			    
                $creditmemo->save();
			}
		 
            
            // send email notification
            $order->addStatusToHistory($order->getStatus(), 'STOCK RELEASED FOR SALE - OFFLINE REFUND COMPLETE - order placed on hold as suspected fraud for more than 7 days', false);
			$order->hold()->save();
        } catch (Exception $e) {
            var_dump($e->getMessage(), $e->getTraceAsString());
			$order->addStatusToHistory($order->getStatus(), 'STOCK /!\NOT/!\ RELEASED FOR SALE - OFFLINE REFUND /!\NOT/!\ COMPLETE - order placed on hold as suspected fraud for more than 7 days; '.$e->getMessage(), false);
			$order->hold()->save();
        }
		
	}
	
}


$timetocheck = date("Y-m-d H:i:s", strtotime("-1 hours"));
$orders = Mage::getModel('sales/order')->getCollection()
    ->addFieldToFilter('status', 'pending_auth')
	->addFieldToFilter('created_at', array(
            'to' => $timetocheck,
            ))
    ->addAttributeToSelect('*');
//var_dump($orders->getSelect()."");exit;
echo "Sending emails to customers...\n";
	
foreach ($orders as $order) {
	
	$historys = $order->getStatusHistoryCollection();
	$hassent = false;
	foreach($historys as $history){
		//var_dump($history->getComment());
		if($history->getComment() == "Sent fraud email to customer")
		{
			$hassent = true;
			break;
		}
	}
	if(!$hassent){
		
		try{
			
			$order->addStatusToHistory($order->getStatus(), 'Sent fraud email to customer', false);
			$order->save();
			//var_dump();
			$sender  = array(
		        'name' => Mage::getStoreConfig('trans_email/ident_support/name', $order->getStoreId()),
		        'email' => Mage::getStoreConfig('trans_email/ident_support/email', $order->getStoreId())
		    );
		     $vars = array(
		    'orderid' => $order->getIncrementId(),
		    );
		    $templateId = 47;
			if($order->getStoreId() == 28) $templateId = 56;
		    Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $order->getCustomerEmail(), $order->getCustomerName(), $vars, $order->getStoreId());
			var_dump($order->getIncrementId());
		}
		catch(Exception $e){
			var_dump($order->getIncrementId()." ".$e->getMessage());
		}
	}
}


