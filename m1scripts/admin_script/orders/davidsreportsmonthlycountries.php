<?php
require_once(dirname(__FILE__).'/../phpmailer/class.phpmailer.php');
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4096M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$it = 1;
$dateto = new DateTime("2018-11-31 23:59:59");

$datefrom = new DateTime("2016-01-01 00:00:00");

$email = new PHPMailer();
$email->SetFrom('cron@startfitness.co.uk', 'Start Fitness Cron'); //Name is optional
$email->Subject   = 'IRELAND sales report - '.$dateto->format("Y-m-d");
$email->Body      = "Reports Attached.";
//$email->AddAddress( 'geoffrey@startfitness.co.uk' );
$email->AddAddress( 'geoffrey@rawtechnologies.co.uk' );
$files =[];

while($it > 0){
$it--;	

$sql = [];



$sql["detailed"] = "
SELECT orders.increment_id, orders.entity_id, orders.created_at, orders.order_currency_code, 
orders.base_grand_total + IF(orders.base_customer_balance_amount IS NULL, 0, orders.base_customer_balance_amount)
+ (IF(orders.base_gift_cards_amount IS NULL, 0 , orders.base_gift_cards_amount )) as base_grand_total, 
orders.grand_total + IF(orders.customer_balance_amount IS NULL, 0, orders.customer_balance_amount)
+ (IF(orders.gift_cards_amount IS NULL, 0 , orders.gift_cards_amount )) as grand_total, 
/*orders.base_tax_amount - SUM( oi.base_discount_amount - ((oi.base_discount_amount / ((oi.tax_percent  / 100)+1)))) as base_tax_amount, */
corr.base_tax_after_discount as base_corrected_vat,
corr.tax_after_discount as corrected_vat,
orders.base_shipping_amount, orders.shipping_amount, base_shipping_incl_tax, shipping_incl_tax ,  (pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region,
`customer_group`.customer_group_code,
vatcode.value as vatcode
FROM  sales_flat_order orders
JOIN `dimasoft_order_vat_corrected` corr ON corr.order_id = orders.entity_id
JOIN sales_flat_order_item oi ON oi.order_id = orders.entity_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
LEFT JOIN `customer_group` on `customer_group`.customer_group_id = orders.customer_group_id
LEFT JOIN  `customer_entity_varchar` vatcode ON vatcode.`attribute_id` = 15 AND vatcode.entity_id = orders.customer_id
WHERE shipping.country_id IN ('IE') AND orders.created_at >= '".$datefrom->format("Y-m-d")." 00:00:00' AND orders.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free'
GROUP BY orders.entity_id";

$sql["detailed_returns"] = "
SELECT credit.increment_id, credit.entity_id, credit.created_at, orders.order_currency_code, 
credit.base_grand_total + IF(credit.base_customer_balance_amount IS NULL, 0, credit.base_customer_balance_amount)
+ (IF(credit.base_gift_cards_amount IS NULL, 0 , credit.base_gift_cards_amount )) as base_grand_total, 
credit.grand_total + IF(credit.customer_balance_amount IS NULL, 0, credit.customer_balance_amount)
+ (IF(credit.gift_cards_amount IS NULL, 0 , credit.gift_cards_amount )) as grand_total, 
credit.base_tax_amount as bas_vat,
credit.tax_amount as vat,
credit.base_shipping_amount,credit.shipping_amount, credit.base_shipping_incl_tax ,credit.shipping_incl_tax ,  (pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region,
`customer_group`.customer_group_code,
vatcode.value as vatcode
FROM `sales_flat_creditmemo` credit
JOIN sales_flat_order orders ON orders.entity_id = credit.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
LEFT JOIN `customer_group` on `customer_group`.customer_group_id = orders.customer_group_id
LEFT JOIN  `customer_entity_varchar` vatcode ON vatcode.`attribute_id` = 15 AND vatcode.entity_id = orders.customer_id
WHERE /*prod.attribute_set_id IN (9,34) AND*/ shipping.country_id IN ('IE') AND credit.created_at >= '".$datefrom->format("Y-m-d")." 00:00:00' AND credit.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free'";





foreach($sql as $index => $sqll):

	$lines = $read->fetchAll($sqll);
	$headers = false;
	$filename = "ie/".$index."-".$datefrom->format("Y-m").".csv";
	$f = fopen($filename, "w+");
	foreach($lines as $line){
		if(!$headers){
			fputcsv($f, array_keys($line));
			$headers = true;
		}
		fputcsv($f, array_values($line));
	}
	fclose($filename);
	
	
	
	
	
	$file_to_attach = $filename;
	
	$email->AddAttachment( $file_to_attach , 'IRELAND-'.$dateto->format("Y-m-d").'.csv' );
	$files [] = $file_to_attach;

endforeach;

$dateto->modify("- 35 days");
$dateto->modify("last day of this month");
$datefrom = clone $dateto;
$datefrom->modify("first day of this month");

}

return $email->Send();

foreach($files as $filename){
	unlink($filename);
}

