<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '16048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
//require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');


$currencies = array(
	//"GBP" => "pounds",
	//"EUR" => "euros",
	//"USD" => "dollars",
	"CAD" => "cdollars",
	"AUD" => "adollars",
	"NOK" => "nkrone",
	"SEK" => "skrone",
	"JPY" => "yen",
	"RUB" => "ruble",
	"MXN" => "pesos",
	"DKK" => "corona",
);

$datei = new DateTime();
$datei->sub(new DateInterval("P1D"));
$dateo = new DateTime();
$dateo->sub(new DateInterval("P1D"));

//$datei = new DateTime("2017-01-11");

//$dateo = new DateTime("2017-01-19");


//echo getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP");exit;


	


	//$collection = Mage::getModel('reportsalesorders/reportsalesorders')->getCollection();
	//$collection->setDateRange($datei, $dateo);
	$collection = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "GBP"));
	
	
	//exit;
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$collectioneur = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$collectionrow = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "USD"));
	
	$SHIPPING = array();
	$UKINT = array();
	$EURINT = array();
	$ROWINT = array();
	$EBYINT = array();
	
	
	
	foreach($collection as $line){
		
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//var_dump($countrycode, $ineurope);
		//$ineurope = true;
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat'] - $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		
		if($line["store_id"]== "25"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_no_vat'=>0,  'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0,  'total_vat'=>0));
			$SHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$EBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$EBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			$EBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0,  'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0,  'total_vat'=>0));			
			$SHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$UKINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));;
			if(!$ineurope) 
				$UKINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$UKINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$UKINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			$UKINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$UKINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$UKINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
			//$UKINT[$line['increment_id']]['created_at'] = $line["created_at"];
		}

		if($line['increment_id'] == "1900124034"){
			//var_dump((float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'],$UKINT[$line['increment_id']]);exit;
		}
		
	}
	unset($collection);
	
	foreach($collectioneur as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
			$SHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$EBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$EBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			$EBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));			
			$SHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$EURINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$EURINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EURINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$EURINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			$EURINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EURINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$EURINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
			
			if($line['increment_id'] == "600475553"){
			//var_dump($EURINT[$line['increment_id']]);exit;
			}
		}
		
	}
	unset($collectioneur);
	foreach($collectionrow as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
			$SHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += $line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$EBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$EBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			$EBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));			
			$SHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$ROWINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if($ineurope) 
				$ROWINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$ROWINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$ROWINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal_invoiced"] - (float)$line['total_zerorated'];
			//var_dump($line['increment_id'], $line["base_subtotal_invoiced"] - $line['total_zerorated']);
			$ROWINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$ROWINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$ROWINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	foreach($collectionrow as $line){
		if($line['tax_after_discount_noshipping'] > 0){
			//var_dump($line);exit;
		}
		if(($line['base_subtotal_invoiced'] - $line['total_zerorated']) < 0){
			//var_dump($line);exit;
		}
	}
	
	unset($collectionrow);
	
	
	//$rcollection = Mage::getModel('reportcreditmemos/reportcreditmemos')->getCollection();
	//$rcollection->setDateRange($datei, $dateo);
	//var_dump($rcollection->getSelect()->__toString());exit;
	$rcollection = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "GBP"));
	
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$rcollectioneur = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$rcollectionrow = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $dateo->format("Y-m-d 23:59:59"), "USD"));
	
	
	
	$rSHIPPING = array();
	$rUKINT = array();
	$rEURINT = array();
	$rROWINT = array();
	$rEBYINT = array();
	
	foreach($rcollection as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$rSHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
		if($line["store_id"]== "25"){
			$rEBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			$rSHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$rEBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rEBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$rSHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$rUKINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$rUKINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rUKINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rUKINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rUKINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$rUKINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rUKINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	unset($rcollection);
	foreach($rcollectioneur as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$rSHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
		if($line["store_id"]== "25"){
			$rEBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			$rSHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$rEBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rEBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$rSHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$rEURINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if(!$ineurope) 
				$rEURINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEURINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rEURINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rEURINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEURINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	unset($rcollectioneur);
	foreach($rcollectionrow as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$rSHIPPING[$line['increment_id']] = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
		if($line["store_id"]== "25"){
			$rEBYINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			$rSHIPPING[$line['increment_id']]['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if($ineurope) 
				$rEBYINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rEBYINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$rSHIPPING[$line['increment_id']]['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING[$line['increment_id']]['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			
			$rROWINT[$line['increment_id']] = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
			if($ineurope) 
				$rROWINT[$line['increment_id']]['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rROWINT[$line['increment_id']]['total_zero_vat'] += $line["total_zerorated"];
			$rROWINT[$line['increment_id']]['total_exc_vat'] += (float)$line["base_subtotal"] - (float)$line['total_zerorated'];
			$rROWINT[$line['increment_id']]['total_vat'] += $line["tax_after_discount_noshipping"];
			$rROWINT[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rROWINT[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	unset($rcollectionrow);
	//var_dump($UKINT);exit;
	$xmlarr = array();
	foreach($UKINT as $oid => $UKINTD){
	//foreach($rUKINT as $oid => $UKINTD){
		$xmlarrtemp = array(
		"ITEM_01" => array("NAME"=>"UK - INT", "TAXCODE" => "1", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($UKINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $UKINTD['total_exc_vat'], "TAX_WOREFUNDS" => $UKINTD['total_vat']),
		"ITEM_02" => array("NAME"=>"UK - INT ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_zero_vat'], 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['total_zero_vat'] - $UKINTD['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
		"ITEM_02BIS" => array("NAME"=>"UK - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_zero_vat_bis'] - 0,2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		"ITEM_03" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>roundfix($UKINTD['gift']['voucher_spent'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
		"ITEM_04" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>roundfix($UKINTD['gift']['voucher_in'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit"),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	foreach($EURINT as $oid => $EURINTD){
	//foreach($rEURINT as $oid => $EURINTD){
		$xmlarrtemp = array(
		"ITEM_05" => array("NAME"=>"EUR - INT", "TAXCODE" => "1", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($EURINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $EURINTD['total_exc_vat'], "TAX_WOREFUNDS" => $EURINTD['total_vat']),
		"ITEM_06" => array("NAME"=>"EUR - INT ZERO RATED",   "TAXCODE" => "2", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EURINTD['total_zero_vat'], "TAX_WOREFUNDS" => 0),
		"ITEM_06BIS" => array("NAME"=>"EUR - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_zero_vat_bis'] - 0, 2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EURINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	foreach($ROWINT as $oid => $ROWINTD){
	//foreach($rROWINT as $oid => $ROWINTD){
		$xmlarrtemp = array(
		"ITEM_07" => array("NAME"=>"ROW - INT", "TAXCODE" => "1", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($ROWINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $ROWINTD['total_exc_vat'], "TAX_WOREFUNDS" => $ROWINTD['total_vat']),
		"ITEM_08" => array("NAME"=>"ROW - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $ROWINTD['total_zero_vat'], "TAX_WOREFUNDS" => 0),
		"ITEM_08BIS" => array("NAME"=>"ROW - ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_zero_vat_bis'] - 0, 2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $ROWINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	
	foreach($SHIPPING as $oid => $SHIPPINGD){
	//foreach($rSHIPPING as $oid => $SHIPPINGD){
		$xmlarrtemp = array(
		"ITEM_12" => array("NAME"=>"Postage - INT VAT", "TAXCODE" => "1", "NOMINAL" => "4040--INT", "TOTAL"=>roundfix($SHIPPINGD['int']['total_exc_vat'] - 0, 2), "TAX"=>roundfix($SHIPPINGD['int']['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $SHIPPINGD['int']['total_exc_vat'], "TAX_WOREFUNDS" => $SHIPPINGD['int']['total_vat']),
		"ITEM_13" => array("NAME"=>"Postage - EBY VAT", "TAXCODE" => "1", "NOMINAL" => "4040--EBY", "TOTAL"=>roundfix($SHIPPINGD['eby']['total_exc_vat'] - 0, 2), "TAX"=>roundfix($SHIPPINGD['eby']['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $SHIPPINGD['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['eby']['total_vat']),
		"ITEM_14" => array("NAME"=>"Postage - INT NOVAT", "TAXCODE" => "2", "NOMINAL" => "4040--INT", "TOTAL"=>roundfix($SHIPPINGD['int']['total_no_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $SHIPPINGD['int']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['int']['total_no_vat']),
		"ITEM_15" => array("NAME"=>"Postage - EBY NOVAT", "TAXCODE" => "2", "NOMINAL" => "4040--EBY", "TOTAL"=>roundfix($SHIPPINGD['eby']['total_no_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $SHIPPINGD['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['eby']['total_no_vat']),
		);
		$xmlarr["SHIP-".$oid] = $xmlarrtemp;
	}
	
	foreach($EBYINT as $oid => $EBYINTD){
	//foreach($rEBYINT as $oid => $EBYINTD){
		$xmlarrtemp = array(
		"ITEM_09" => array("NAME"=>"GEN - EBY", "TAXCODE" => "1", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($EBYINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $EBYINTD['total_exc_vat'], "TAX_WOREFUNDS" => $EBYINTD['total_vat']),
		"ITEM_10" => array("NAME"=>"GEN - EBY ZERO RATED", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EBYINTD['total_zero_vat'],  "TAXCODE" => "2", "TAX_WOREFUNDS" => 0),
		"ITEM_10BIS" => array("NAME"=>"GEN - EBY EXEMPT",   "TAXCODE" => "0", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_zero_vat_bis'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EBYINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		
		
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	$nominals = array();
	
	//var_dump($xmlarr);exit;
	//$i = 1;
	$f = fopen("export.csv", "w+");
	//$f = fopen("25janexport-credit.csv", "w+");
	$headers = true;
	foreach($xmlarr as $index => $item){
		foreach($item as $subitem){
			unset($subitem['TOTAL_WOREFUNDS']);
			unset($subitem['TAX_WOREFUNDS']);
			if($subitem['TOTAL'] == 0) continue;
			if($subitem['TAXCODE'] == "1") $subitem['TAX'] = $subitem['TOTAL'] * 0.2;
			else $subitem['TAX'] = 0;
			if($headers){
				$headers = false;
				fputcsv($f, array_merge(array("ORDER ID"),array_keys($subitem)));
			}
			//if($subitem['total_exc_vat'] == 0)continue;			
			fputcsv($f, array_merge(array($index),array_values($subitem)));
		}
	}
	
	$xmlarr = array();
	//foreach($UKINT as $oid => $UKINTD){
	foreach($rUKINT as $oid => $UKINTD){
		$xmlarrtemp = array(
		"ITEM_01" => array("NAME"=>"UK - INT", "TAXCODE" => "1", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($UKINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $UKINTD['total_exc_vat'], "TAX_WOREFUNDS" => $UKINTD['total_vat']),
		"ITEM_02" => array("NAME"=>"UK - INT ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_zero_vat'], 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['total_zero_vat'] - $UKINTD['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
		"ITEM_02BIS" => array("NAME"=>"UK - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-UK-INT", "TOTAL"=>roundfix($UKINTD['total_zero_vat_bis'] - 0,2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		"ITEM_03" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>roundfix($UKINTD['gift']['voucher_spent'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
		"ITEM_04" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>roundfix($UKINTD['gift']['voucher_in'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $UKINTD['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit"),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	//foreach($EURINT as $oid => $EURINTD){
	foreach($rEURINT as $oid => $EURINTD){
		$xmlarrtemp = array(
		"ITEM_05" => array("NAME"=>"EUR - INT", "TAXCODE" => "1", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($EURINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $EURINTD['total_exc_vat'], "TAX_WOREFUNDS" => $EURINTD['total_vat']),
		"ITEM_06" => array("NAME"=>"EUR - INT ZERO RATED",   "TAXCODE" => "2", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EURINTD['total_zero_vat'], "TAX_WOREFUNDS" => 0),
		"ITEM_06BIS" => array("NAME"=>"EUR - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>roundfix($EURINTD['total_zero_vat_bis'] - 0, 2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EURINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	//foreach($ROWINT as $oid => $ROWINTD){
	foreach($rROWINT as $oid => $ROWINTD){
		$xmlarrtemp = array(
		"ITEM_07" => array("NAME"=>"ROW - INT", "TAXCODE" => "1", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($ROWINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $ROWINTD['total_exc_vat'], "TAX_WOREFUNDS" => $ROWINTD['total_vat']),
		"ITEM_08" => array("NAME"=>"ROW - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $ROWINTD['total_zero_vat'], "TAX_WOREFUNDS" => 0),
		"ITEM_08BIS" => array("NAME"=>"ROW - ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>roundfix($ROWINTD['total_zero_vat_bis'] - 0, 2) , "TAX"=>0,
		"TOTAL_WOREFUNDS" => $ROWINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	
	//foreach($SHIPPING as $oid => $SHIPPINGD){
	foreach($rSHIPPING as $oid => $SHIPPINGD){
		$xmlarrtemp = array(
		"ITEM_12" => array("NAME"=>"Postage - INT VAT", "TAXCODE" => "1", "NOMINAL" => "4040--INT", "TOTAL"=>roundfix($SHIPPINGD['int']['total_exc_vat'] - 0, 2), "TAX"=>roundfix($SHIPPINGD['int']['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $SHIPPINGD['int']['total_exc_vat'], "TAX_WOREFUNDS" => $SHIPPINGD['int']['total_vat']),
		"ITEM_13" => array("NAME"=>"Postage - EBY VAT", "TAXCODE" => "1", "NOMINAL" => "4040--EBY", "TOTAL"=>roundfix($SHIPPINGD['eby']['total_exc_vat'] - 0, 2), "TAX"=>roundfix($SHIPPINGD['eby']['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $SHIPPINGD['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['eby']['total_vat']),
		"ITEM_14" => array("NAME"=>"Postage - INT NOVAT", "TAXCODE" => "2", "NOMINAL" => "4040--INT", "TOTAL"=>roundfix($SHIPPINGD['int']['total_no_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $SHIPPINGD['int']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['int']['total_no_vat']),
		"ITEM_15" => array("NAME"=>"Postage - EBY NOVAT", "TAXCODE" => "2", "NOMINAL" => "4040--EBY", "TOTAL"=>roundfix($SHIPPINGD['eby']['total_no_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $SHIPPINGD['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPINGD['eby']['total_no_vat']),
		);
		$xmlarr["SHIP-".$oid] = $xmlarrtemp;
	}
	
	//foreach($EBYINT as $oid => $EBYINTD){
	foreach($rEBYINT as $oid => $EBYINTD){
		$xmlarrtemp = array(
		"ITEM_09" => array("NAME"=>"GEN - EBY", "TAXCODE" => "1", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_exc_vat'] - 0, 2), "TAX"=>roundfix($EBYINTD['total_vat'] - 0, 2),
		"TOTAL_WOREFUNDS" => $EBYINTD['total_exc_vat'], "TAX_WOREFUNDS" => $EBYINTD['total_vat']),
		"ITEM_10" => array("NAME"=>"GEN - EBY ZERO RATED", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_zero_vat'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EBYINTD['total_zero_vat'],  "TAXCODE" => "2", "TAX_WOREFUNDS" => 0),
		"ITEM_10BIS" => array("NAME"=>"GEN - EBY EXEMPT",   "TAXCODE" => "0", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>roundfix($EBYINTD['total_zero_vat_bis'] - 0, 2), "TAX"=>0,
		"TOTAL_WOREFUNDS" => $EBYINTD['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
		
		
		);
		$xmlarr[$oid] = $xmlarrtemp;
	}
	$nominals = array();
	
	//var_dump($xmlarr);exit;
	//$i = 1;
	$f = fopen("exportreturns.csv", "w+");
	//$f = fopen("25janexport-credit.csv", "w+");
	$headers = true;
	foreach($xmlarr as $index => $item){
		foreach($item as $subitem){
			unset($subitem['TOTAL_WOREFUNDS']);
			unset($subitem['TAX_WOREFUNDS']);
			if($subitem['TOTAL'] == 0) continue;
			if($subitem['TAXCODE'] == "1") $subitem['TAX'] = $subitem['TOTAL'] * 0.2;
			else $subitem['TAX'] = 0;
			if($headers){
				$headers = false;
				fputcsv($f, array_merge(array("ORDER ID"),array_keys($subitem)));
			}
			//if($subitem['total_exc_vat'] == 0)continue;			
			fputcsv($f, array_merge(array($index),array_values($subitem)));
		}
	}
	


	
fclose($f);


$mail = new PHPMailer;
$email ="geoffrey@rawtechnologies.co.uk"; 
//$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'localhost';  // Specify main and backup server
//$mail->SMTPAuth = false;                               // Enable SMTP authentication
//$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
//$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'cron@startfitness.co.uk';
$mail->FromName = 'Accounts Breakdown';
$mail->addAddress($email);  // Add a recipient
$mail->addAddress("melissaburton@startfitness.co.uk");  // Add a recipient
$mail->addAddress("richard@rawtechnologies.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML
$data['comment'] = nl2br($data['comment']);
$mail->Subject = 'Accounts Breakdown Fitness/Ebay';
$mail->Body    = 
"
Hello,


Reports attached.

Thanks
"
;
$mail->AddAttachment(dirname(__FILE__)."/export.csv",
                         'export.csv');
$mail->AddAttachment(dirname(__FILE__)."/exportreturns.csv",
                         'exportreturns.csv');

if(!$mail->send()) {
   echo json_encode(array("status"=>"0"));
   exit;
}
//$xml = new SimpleXMLElement('<root/>');
//array_to_xml($xmlarr, $xml);




function array_to_orderarray($item, $cust, $name, $datei)
{
	$xml = "";
		//var_dump($data);
		
		$orderdata = array();
		$orderdata['ORDER_TYPE'] = 1;
		$fullpay = true;
		
		$orderdata['ORDER_NUMBER'] = $name;
		//$orderdata['WAREHOUSE'] = "ASHINGTON";
		
		$orderdata['SET_ACKNOWLEDGED_FLAG'] = "YES";
		
		$orderdata['CUST_ACC_REF'] = $cust;
		
		
		//$orderdata['CUST_ACC_REF'] = str_pad($data->CUSTOMER->CUSTOMER_ID, 8, "0", STR_PAD_LEFT);
		$orderdata['ORDER_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_REQUESTED_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_PROMISED_DATE'] = $datei->format("Y/m/d");
		$orderdata['CUSTOMER_ORDER_NUMBER'] = $name;
		
		$orderdata['INV_POSTAL_NAME'] = $cust;
		$orderdata['INV_ADDRESS_LINE_1'] = "";
		$orderdata['INV_ADDRESS_LINE_2'] = "";
		$orderdata['INV_ADDRESS_LINE_3'] = "";
		$orderdata['INV_ADDRESS_LINE_4'] = "";
		$orderdata['INV_POST_CODE'] = "";
		$orderdata['INV_CITY'] = "";
		$orderdata['INV_COUNTY'] = "";
		$orderdata['INV_CONTACT'] = "";
		$orderdata['INV_TELEPHONE'] = "";
		$orderdata['INV_FAX'] = "";//$billingAddress->getData('');
		$orderdata['INV_EMAIL'] = "";
		$orderdata['INV_TAX_NUMBER'] = "";
		$orderdata['INV_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['INV_COUNTRY_CODE'] = "";
		$orderdata['INV_COUNTRY'] = "";
		
		$orderdata['DEL_POSTAL_NAME'] = "";
		$orderdata['DEL_ADDRESS_LINE_1'] = "";
		$orderdata['DEL_ADDRESS_LINE_2'] = "";
		$orderdata['DEL_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
		$orderdata['DEL_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
		$orderdata['DEL_POST_CODE'] = "";
		$orderdata['DEL_CITY'] = "";
		$orderdata['DEL_COUNTY'] = "";
		$orderdata['DEL_CONTACT'] = "";
		$orderdata['DEL_TELEPHONE'] = "";
		$orderdata['DEL_FAX'] = "";//$billingAddress->getData('');
		$orderdata['DEL_EMAIL'] = "";
		$orderdata['DEL_TAX_NUMBER'] = "";
		$orderdata['DEL_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['DEL_COUNTRY_CODE'] ="";
		$orderdata['DEL_COUNTRY'] = "";
		
		
		$orderdata['PAYMENT_IN_FULL'] = "NO";
		
		
		$del_meth = "";
		
		
		//$orderdata['PAYMENT_VALUE'] = $order->getData('');
		//$orderdata['PAYMENT_REF'] = $order->getData('');
		//$orderdata['PAYMENT_METHOD'] = $order->getData('');
		
		
		
		$orderdata['EXCHANGE_RATE'] = "";
		$orderdata['ARE_PRICES_TAX_INCLUSIVE'] = "YES";
		
		
		$i = 0;
			if((isset($item['TYPE']) && $item['TYPE'] = "credit")){
				//return null;
			}
			if((float)$item["TOTAL"] < 0 || (isset($item['TYPE']) && $item['TYPE'] = "credit")){
				
				$orderdata['CREDIT'] = 1;
				if(strpos($orderdata['CUSTOMER_ORDER_NUMBER'], "UK - INT ZERO R") !== null){
					$orderdata['CUSTOMER_ORDER_NUMBER'] = "GIFTCARD ".$datei->format("Ymd");
				}
			}
			else{
				
				$orderdata['CREDIT'] = 0;
			}
			if((float)$item["TOTAL"] == 0) return null;
			$i++;
			$nominal = explode("-", $item["NOMINAL"]);
			//var_dump($item);exit;
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 2;
			$orderdata['ITEM_'.$i.'_LINE_EXC_TAX'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = implode("-", $nominal); //"S1"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = $item["NAME"];
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = 1;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $item["TOTAL"];
			$orderdata['ITEM_'.$i.'_UNITTAX'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = "0";
			$orderdata['ITEM_'.$i.'_TAXCODE'] = $item["TAXCODE"];
			$orderdata['ITEM_'.$i.'_NOMINAL'] = $nominal[0];
			$orderdata['ITEM_'.$i.'_CC'] = $nominal[1];
			$orderdata['ITEM_'.$i.'_DEPT'] = $nominal[2];
			
		
			
		
		

		$orderdata['DISCOUNT_EX'] = 0;
		$orderdata['DISCOUNT_INC'] = 0;
		$orderdata['DISCOUNT_TAX'] = 0;
		$orderdata['ITEMS_COUNT'] = $i;
		$orderdata['DO_INVOICE'] = "0";
		$orderdata['DO_SHIPPED'] = "0";
		$orderdata['DO_POSTED'] = "0";
		$orderdata['TAKEN_BY'] = "DM SCRIPT";
		
		//var_dump($orderdata);
		
		$xml = array("data"=> $orderdata);
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><form></form>');
		arrayToXml($xml,$xml_data);
		
		
		return $xml_data;
}


function arrayToXml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            arrayToXml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
    	if(strpos($key, "ITEM_")!== false) $key = "ITEM";
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function getRows($datein, $dateout, $currency = "GBP"){
	$sql = "SELECT  
	(SELECT SUM(IF(sales_flat_order_item.base_tax_amount = 0, sales_flat_order_item.base_discount_amount, (sales_flat_order_item.base_discount_amount/1.2))) 
	FROM sales_flat_order_item WHERE sales_flat_order_item.order_id = main_table.entity_id) *-1
	AS base_discount_excl_vat,
	`main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       IF(`a`.`country_id` IS NULL, `b`.`country_id`, `a`.`country_id`) AS
       `region_code`,
       `fix`.`tax_after_discount`,
       `fix`.`tax_after_discount_noshipping`,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       `main_table`.`created_at`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0)) AS total_zerorated
FROM `sales_flat_invoice` as invoices
INNER JOIN   `sales_flat_order` AS `main_table` ON `main_table`.entity_id = `invoices`.`order_id`
       LEFT JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
       JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  ( payments.method <> \"c3v12finance\" )
       AND ( invoices.created_at BETWEEN
             '{$datein}' AND '{$dateout}'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR main_table.customer_id IS NULL )
       AND ( main_table.order_currency_code = '{$currency}' ) 
       
       
       GROUP BY main_table.entity_id
	";
	//echo $sql;exit;
	return $sql;
	
}

function getRowsReturn($datein, $dateout, $currency = "GBP"){
	$sql = "
	SELECT   
((SELECT SUM(IF(sales_flat_creditmemo_item.base_discount_amount IS NULL, 0,IF(sales_flat_creditmemo_item.base_tax_amount = 0, sales_flat_creditmemo_item.base_discount_amount, (sales_flat_creditmemo_item.base_discount_amount/1.2))))                                                           
        FROM sales_flat_creditmemo_item WHERE sales_flat_creditmemo_item.parent_id = main_table.entity_id) *-1) as discountnovat,	
	  `main_table`.*,
           `a`.`store_name`,
           `a`.`created_at`                                                            AS `purchased_on`,
           (`tax_after_discount` * -1)                                                 AS `tax_after_discount`,
           (`tax_after_discount_noshipping`)                                           AS `tax_after_discount_noshipping`,
                      Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS `shipto_name`,
           `b`.`city`                                                                  AS `shipto_city`,
           `b`.`region`                                                                AS `shipto_state`,
           `b`.`country_id`,
           IF(`b`.`country_id` IS NULL, `c`.`country_id`, `b`.`country_id`) AS
       		`region_code`,
                      Concat(Coalesce(c.firstname, ''), ' ', Coalesce(c.lastname, '')) AS `billto_name`,
           `main_table`.`increment_id`                                                 AS `order_increment_id`,
           `main_table`.`created_at`                                                   AS `purchased_on`,
           `main_table`.`billing_address_id`,
           `main_table`.`shipping_address_id`,
           `main_table`.`subtotal`,
           (main_table.subtotal - IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)) AS `goods_net`,
           IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)                         AS `discount_amount`,
           `main_table`.`tax_amount`,
           `main_table`.`shipping_amount`,
           `main_table`.`grand_total`,
           `main_table`.`shipping_tax_amount`,
           `main_table`.`subtotal_incl_tax`,
           (main_table.subtotal_incl_tax - main_table.subtotal)                                                AS `item_tax`,
           (IF(main_table.customer_bal_total_refunded IS NULL, 0.00, main_table.customer_bal_total_refunded) ) AS `giftcard_invoice`,
           (
                  SELECT IF(Sum(row_invoiced) IS NULL, 0.00,Sum(row_invoiced) )
                  FROM   `sales_flat_creditmemo_item` ci
                  JOIN   sales_flat_order_item sfoi
                  ON     sfoi.item_id = ci.parent_id
                  WHERE  sfoi.product_type = \"giftcard\"
                  AND    sfoi.order_id = main_table.order_id)          AS `giftcard_bought`,
           Sum(IF(items.base_tax_amount = 0, items.base_row_total, 0)) AS total_zerorated
FROM       `sales_flat_creditmemo`                                     AS `main_table`
INNER JOIN `sales_flat_order`                                          AS `a`
ON         main_table.order_id = a.entity_id
JOIN       `sales_flat_creditmemo_item` items
ON         items.parent_id = main_table.entity_id
LEFT JOIN  `dimasoft_credit_vat_corrected` AS `fix`
ON         fix.credit_id = main_table.entity_id
LEFT JOIN `sales_flat_order_address` AS `b`
ON         main_table.shipping_address_id = b.entity_id
INNER JOIN `sales_flat_order_address` AS `c`
ON         main_table.billing_address_id = c.entity_id
WHERE      (
                      main_table.created_at BETWEEN '{$datein}' AND        '{$dateout}')
AND        (
                      main_table.grand_total >= 0)
AND        (
                      main_table.order_currency_code = '{$currency}')
AND ( a.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR a.customer_id IS NULL )
GROUP BY   main_table.entity_id";
	return $sql;
}

function roundfix($data, $dec){
	//return round($data, $dec);
	return $data;
}
