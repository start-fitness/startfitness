<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$datei = "2016-01-01 00:00:00";
$dateo = "2019-10-30 23:59:59";

$headers = "ord.entity_id, ord.increment_id, ord.created_at, ord.base_grand_total, ord.grand_total, ord.base_to_order_rate, pay.method";
$date = new DateTime();
$date->modify("- 360 days");
$sql = "
SELECT  {$headers}, pay.additional_data FROM sales_flat_order ord
JOIN sales_flat_order_payment pay on pay.parent_id = ord.entity_id
JOIN core_store store on store.store_id = ord.store_id
WHERE store.name LIKE 'amazon%' AND created_at >= '".$date->format("Y-m-d H:i:s")."'
ORDER BY ord.created_at ASC
";
var_dump($sql);exit;
$f = fopen(dirname(__file__)."/reconcile/amazonexport-ongoing.csv", "a+");
$headers = explode(",", $headers);
foreach($headers as $ind=> $header){
	$tmp = (explode(".",trim($header)));
	$headers[$ind] = $tmp[count($tmp)-1];
}
$headers[]="trans_sum";
$headers[]="trans_fee";
$headers[]="base_trans_sum";
$headers[]="base_trans_fee";
$headers[]="trans_tx";
$headers[]="diff";
fputcsv($f, $headers);
foreach($read->fetchAll($sql) as $row){
	
	$add = unserialize($row['additional_data']);
	$orderid = $row['entity_id'];
	unset($row['additional_data']);
	//unset($row['entity_id']);
	$sum = $add['transactions'][0]['sum'];
	$fee = $add['transactions'][0]['fee'];
	$sum2 = $add['transactions'][0]['base_sum'] / $row['base_to_order_rate'];
	$fee2 = $add['transactions'][0]['base_fee'] / $row['base_to_order_rate'];
	$tid = $add['transactions'][0]['transaction_id'];
	if($tid == "") continue;
	$row[]=$sum;
	$row[]=$fee;
	$row[]=$sum2;
	$row[]=$fee2;
	$row[]=$tid;
	$row[]=$sum - $row['grand_total'];
	$diff =$sum - $row['grand_total'];
	if(( $diff!= 0 AND ($diff >= 0.02) )){
		fputcsv($f, $row);
		$fixed = ($row['grand_total'] * 1.2) - $sum;
		if($fixed >= 0.02){
			// split across items
			$diff = $sum - $row['grand_total'];
			$diff_base = $diff / $row['base_to_order_rate'];
			
			$select = "SELECT COUNT(*) as count FROM sales_flat_order_item WHERE order_id = {$orderid} AND base_row_total > 0";
			$counts = $read->fetchAll($select);
			$numitems = 1;
			if(count($counts) > 0) $numitems = $counts[0]['count'];
			
			$itemsplit = $diff / $numitems;
			$itemsplit_base = $diff_base / $numitems;
			
			$sql = "
			UPDATE sales_flat_order
			SET 
		
			base_total_invoiced = base_total_invoiced +  $diff_base,
			total_invoiced = total_invoiced +  $diff,
			base_total_paid = base_total_paid +  $diff_base,
			total_paid = total_paid +  $diff,
			base_grand_total = base_grand_total +  $diff_base,
			grand_total = grand_total +  $diff,
			base_subtotal_incl_tax = base_subtotal_incl_tax +  $diff_base,
			subtotal_incl_tax = subtotal_incl_tax +  $diff,
			base_subtotal = base_subtotal +  $diff_base,
			subtotal = subtotal +  $diff,
			subtotal_invoiced = subtotal_invoiced +  $diff,
			base_subtotal_invoiced = base_subtotal_invoiced+  $diff_base
			
			WHERE sales_flat_order.entity_id = {$orderid}
			";
			$sql2 = "
			UPDATE sales_flat_invoice
			SET 
			base_subtotal_incl_tax = base_subtotal_incl_tax +  $diff_base,
			subtotal_incl_tax = subtotal_incl_tax +  $diff,
			base_subtotal = base_subtotal +  $diff_base,
			subtotal = subtotal +  $diff,
			base_grand_total = base_grand_total +  $diff_base,
			grand_total = grand_total +  $diff
			
			WHERE sales_flat_invoice.order_id = {$orderid}
			";
			$sql3 = "
			UPDATE sales_flat_order_item 
			SET
			price = (price + $itemsplit) / qty_ordered,
			base_price = (base_price + $itemsplit_base) / qty_ordered,
			price_incl_tax = (price_incl_tax + $itemsplit) / qty_ordered,
			base_price_incl_tax = (base_price_incl_tax + $itemsplit_base) / qty_ordered,
			row_total = row_total + $itemsplit,
			base_row_total = base_row_total + $itemsplit_base,
			row_total_incl_tax = row_total_incl_tax + $itemsplit,
			base_row_total_incl_tax = base_row_total_incl_tax + $itemsplit_base,
			row_invoiced = row_invoiced  + $itemsplit,
			base_row_invoiced = base_row_invoiced + $itemsplit_base
			
			WHERE order_id = {$orderid} AND base_row_total > 0
			";
			$sql4 = "
			UPDATE sales_flat_order_grid
			JOIN sales_flat_order ON sales_flat_order_grid.entity_id = sales_flat_order.entity_id 
			SET
			sales_flat_order_grid.base_grand_total = sales_flat_order.base_grand_total,
			sales_flat_order_grid.base_total_paid = sales_flat_order.base_total_paid,
			sales_flat_order_grid.grand_total = sales_flat_order.grand_total,
			sales_flat_order_grid.total_paid = sales_flat_order.total_paid
			
			WHERE sales_flat_order_grid.entity_id = {$orderid}
			";
			$sql5 = "
			UPDATE sales_flat_invoice_grid
			JOIN sales_flat_invoice ON sales_flat_invoice_grid.entity_id = sales_flat_invoice.entity_id 
			SET
			sales_flat_invoice_grid.base_grand_total = sales_flat_invoice.base_grand_total,
			sales_flat_invoice_grid.grand_total = sales_flat_invoice.grand_total
			
			WHERE sales_flat_invoice_grid.order_id = {$orderid}
			";
			//$read->query($sql);$read->query($sql2);$read->query($sql3);$read->query($sql4);$read->query($sql5);
		}
		else{
			// ADD VAT TO ALL TOTALS
			$sql = "
			UPDATE sales_flat_order
			SET 
			
			base_shipping_amount = base_shipping_amount * 1.2,
			shipping_amount = shipping_amount * 1.2,
			base_shipping_invoiced = base_shipping_invoiced * 1.2,
			shipping_invoiced = shipping_invoiced * 1.2,
			base_shipping_incl_tax = base_shipping_incl_tax * 1.2,
			shipping_incl_tax = shipping_incl_tax * 1.2,
			
			base_total_invoiced = base_total_invoiced * 1.2,
			total_invoiced = total_invoiced * 1.2,
			base_total_paid = base_total_paid * 1.2,
			total_paid = total_paid * 1.2,
			base_grand_total = base_grand_total * 1.2,
			grand_total = grand_total * 1.2,
			base_subtotal_incl_tax = base_subtotal_incl_tax * 1.2,
			subtotal_incl_tax = subtotal_incl_tax * 1.2,
			base_subtotal = base_subtotal * 1.2,
			subtotal = subtotal * 1.2,
			subtotal_invoiced = subtotal_invoiced * 1.2,
			base_subtotal_invoiced = base_subtotal_invoiced* 1.2
			
			WHERE sales_flat_order.entity_id = {$orderid}
			";
			$sql2 = "
			UPDATE sales_flat_invoice
			SET 
			base_shipping_amount = base_shipping_amount * 1.2,
			shipping_amount = shipping_amount * 1.2,
			base_shipping_incl_tax = base_shipping_incl_tax * 1.2,
			shipping_incl_tax = shipping_incl_tax * 1.2,
			
			base_subtotal_incl_tax = base_subtotal_incl_tax * 1.2,
			subtotal_incl_tax = subtotal_incl_tax * 1.2,
			base_subtotal = base_subtotal * 1.2,
			subtotal = subtotal * 1.2,
			base_grand_total = base_grand_total * 1.2,
			grand_total = grand_total * 1.2
			
			WHERE sales_flat_invoice.order_id = {$orderid}
			";
			$sql3 = "
			UPDATE sales_flat_order_item 
			SET
			price = (price * 1.2) ,
			base_price = (base_price * 1.2),
			price_incl_tax = (price_incl_tax * 1.2) ,
			base_price_incl_tax = (base_price_incl_tax * 1.2) ,
			row_total = row_total * 1.2,
			base_row_total = base_row_total * 1.2,
			row_total_incl_tax = row_total_incl_tax * 1.2,
			base_row_total_incl_tax = base_row_total_incl_tax * 1.2,
			row_invoiced = row_invoiced * 1.2,
			base_row_invoiced = base_row_invoiced * 1.2
			
			WHERE order_id = {$orderid} AND base_row_total > 0
			";
			
			$sql4 = "
			UPDATE sales_flat_order_grid
			JOIN sales_flat_order ON sales_flat_order_grid.entity_id = sales_flat_order.entity_id 
			SET
			sales_flat_order_grid.base_grand_total = sales_flat_order.base_grand_total,
			sales_flat_order_grid.base_total_paid = sales_flat_order.base_total_paid,
			sales_flat_order_grid.grand_total = sales_flat_order.grand_total,
			sales_flat_order_grid.total_paid = sales_flat_order.total_paid
			
			WHERE sales_flat_order_grid.entity_id = {$orderid}
			";
			$sql5 = "
			UPDATE sales_flat_invoice_grid
			JOIN sales_flat_invoice ON sales_flat_invoice_grid.entity_id = sales_flat_invoice.entity_id 
			SET
			sales_flat_invoice_grid.base_grand_total = sales_flat_invoice.base_grand_total,
			sales_flat_invoice_grid.grand_total = sales_flat_invoice.grand_total
			
			WHERE sales_flat_invoice_grid.order_id = {$orderid}
			";
			//$read->query($sql);$read->query($sql2);$read->query($sql3);$read->query($sql4);$read->query($sql5);
			//var_dump($sql,$sql2, $sql3);exit;
		}
	}
	
}
fclose($f);
