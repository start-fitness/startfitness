<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '16048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');


$currencies = array(
	//"GBP" => "pounds",
	//"EUR" => "euros",
	//"USD" => "dollars",
	"CAD" => "cdollars",
	"AUD" => "adollars",
	"NOK" => "nkrone",
	"SEK" => "skrone",
	"JPY" => "yen",
	"RUB" => "ruble",
	"MXN" => "pesos",
	"DKK" => "corona",
);

$datei = new DateTime();
$datei->sub(new DateInterval("P1D"));
$dateo = new DateTime();
$dateo->sub(new DateInterval("P1D"));

//$datei = new DateTime("2018-03-07");

//$dateo = new DateTime("2018-03-07");


//echo getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP");exit;

do{
	


	//$collection = Mage::getModel('reportsalesorders/reportsalesorders')->getCollection();
	//$collection->setDateRange($datei, $dateo);
	$collection = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP"));
	
	
	//exit;
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$collectioneur = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$collectionrow = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "USD"));
	
	foreach($currencies as $code=>$label){
		$varname = "collection".$code;
		$$varname = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), $code));
		$varname = $code."INT";
		$$varname = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	}
	
	
	$SHIPPING = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0));
	$UKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$EURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$ROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$EBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	
	
	
	
	foreach($collection as $line){
		
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//var_dump($countrycode, $ineurope);
		//$ineurope = true;
		//var_dump($line['total_zerorated'] . " - " . $line["giftcard_bought"]);
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat'] - $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"|| $line["store_id"]== "36"){
			$SHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$EBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$UKINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$UKINT['total_zero_vat'] += $line["total_zerorated"];
			$UKINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			$UKINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$UKINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$UKINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	unset($collection);
	
	foreach($collectioneur as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"|| $line["store_id"]== "36"){
			$SHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$EBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$EURINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EURINT['total_zero_vat'] += $line["total_zerorated"];
			$EURINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			$EURINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EURINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}
	unset($collectioneur);
	foreach($collectionrow as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"|| $line["store_id"]== "36"){
			$SHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if($ineurope) 
				$EBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$EBYINT['total_zero_vat'] += $line["total_zerorated"];
			$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			$SHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
			$SHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if($ineurope) 
				$ROWINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$ROWINT['total_zero_vat'] += $line["total_zerorated"];
			$ROWINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
			//var_dump($line['increment_id'], $line["base_subtotal_invoiced"] - $line['total_zerorated']);
			$ROWINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$ROWINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$ROWINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		
	}



	
	foreach($collectionrow as $line){
		if($line['tax_after_discount_noshipping'] > 0){
			//var_dump($line);exit;
		}
		if(($line['base_subtotal_invoiced'] - $line['total_zerorated']) < 0){
			//var_dump($line);exit;
		}
	}
	
	unset($collectionrow);
	
	
	foreach($currencies as $currcode => $currval){
		$collectionvar = "collection".$currcode;
		$arrvar = $currcode."INT";
		foreach($$collectionvar as $line){
			$countrycode = ($line['region_code']);
			$ineurope = false;
			$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
			$eu_countries_array = explode(',',$eu_countries);
			if(in_array($countrycode, $eu_countries_array))
				$ineurope = true;
			//var_dump($currcode ." => ". $countrycode ,$ineurope);
			//$ineurope = false;
			
			
			
			$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat']- $line["giftcard_bought"];;
			$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
			if($line["store_id"]== "25"|| $line["store_id"]== "36"){
				$SHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
				$SHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($ineurope) 
					$EBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					$EBYINT['total_zero_vat'] += $line["total_zerorated"];
				$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
				$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
				$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
				$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
				$SHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
				$SHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($ineurope) 
					${$arrvar}['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					${$arrvar}['total_zero_vat'] += $line["total_zerorated"];
				${$arrvar}['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
				//var_dump($line['increment_id'], $line["base_subtotal_invoiced"] - $line['total_zerorated']);
				${$arrvar}['total_vat'] += $line["tax_after_discount_noshipping"];
				${$arrvar}['gift']['voucher_in'] += $line["giftcard_invoice"];
				${$arrvar}['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			
			//var_dump( $$arrvar);
			//exit;
			
		}
	
	
		
		unset($collectionvar);
	}
	
	
	//$rcollection = Mage::getModel('reportcreditmemos/reportcreditmemos')->getCollection();
	//$rcollection->setDateRange($datei, $dateo);
	//var_dump($rcollection->getSelect()->__toString());exit;
	$rcollection =  $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP"));
	
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$rcollectioneur =  $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$rcollectionrow = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "USD"));
	
	
	foreach($currencies as $code=>$label){
		$varname = "rcollection".$code;
		$$varname = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), $code));
		$varname = "r".$code."INT";
		$$varname = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
		$varname = "r".$code."INTf";
		$$varname = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	}
	
	
	$rSHIPPING = array(	"eby"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0), 
						"int"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0),
						"fraud"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0));
	$rUKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rEURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rEBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	// Fraud related
	$rUKINTf = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rEURINTf = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rROWINTf = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	
	
	foreach($rcollection as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"|| $line["store_id"]== "36"){
			$rSHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$rEBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
			$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			if(isOrderFraud($line['order_id'])){
				$rSHIPPING['fraud']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['fraud']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['fraud']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if(!$ineurope) 
					$rUKINTf['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					$rUKINTf['total_zero_vat'] += $line["total_zerorated"];
				$rUKINTf['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
				$rUKINTf['total_vat'] += $line["tax_after_discount_noshipping"];
				$rUKINTf['gift']['voucher_in'] += $line["giftcard_invoice"];
				$rUKINTf['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			else{
				$rSHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($line['is_nonitemised']){
					$resnoni = workOutItemlessCredit($line, $line['order_id']);
					
					if($ineurope) 
						$rUKINT[$line['increment_id']]['total_zero_vat_bis'] += $resnoni['zero'];
					else 
						$rUKINT[$line['increment_id']]['total_zero_vat'] += $resnoni['zero'];
					$rUKINT[$line['increment_id']]['total_exc_vat'] += $resnoni['taxable'];
					$rUKINT[$line['increment_id']]['total_vat'] += $resnoni['taxable']*0.2;
					
					//var_dump($rUKINT[$line['increment_id']]);
					//${$arrvar}[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
					//${$arrvar}[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
				else{
					if(!$ineurope) 
						$rUKINT['total_zero_vat_bis'] += $line["total_zerorated"];
					else 
						$rUKINT['total_zero_vat'] += $line["total_zerorated"];
					$rUKINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
					$rUKINT['total_vat'] += $line["tax_after_discount_noshipping"];
					$rUKINT['gift']['voucher_in'] += $line["giftcard_invoice"];
					$rUKINT['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
			}
			
		}
		
	}
	unset($rcollection);
	foreach($rcollectioneur as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25"|| $line["store_id"]== "36"){
			$rSHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if(!$ineurope) 
				$rEBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
			$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		if(isOrderFraud($line['order_id'])){
				$rSHIPPING['fraud']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['fraud']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['fraud']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if(!$ineurope) 
					$rEURINTf['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					$rEURINTf['total_zero_vat'] += $line["total_zerorated"];
				$rEURINTf['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
				$rEURINTf['gift']['voucher_in'] += $line["giftcard_invoice"];
				$rEURINTf['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			else{
				$rSHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($line['is_nonitemised']){
					$resnoni = workOutItemlessCredit($line, $line['order_id']);
					
					if($ineurope) 
						$rEURINT[$line['increment_id']]['total_zero_vat_bis'] += $resnoni['zero'];
					else 
						$rEURINT[$line['increment_id']]['total_zero_vat'] += $resnoni['zero'];
					$rEURINT[$line['increment_id']]['total_exc_vat'] += $resnoni['taxable'];
					$rEURINT[$line['increment_id']]['total_vat'] += $resnoni['taxable']*0.2;
					
					//var_dump($rUKINT[$line['increment_id']]);
					//${$arrvar}[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
					//${$arrvar}[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
				else{
					if(!$ineurope) 
						$rEURINT['total_zero_vat_bis'] += $line["total_zerorated"];
					else 
						$rEURINT['total_zero_vat'] += $line["total_zerorated"];
					$rEURINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
					$rEURINT['gift']['voucher_in'] += $line["giftcard_invoice"];
					$rEURINT['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
			}
		}
		
	}
	unset($rcollectioneur);
	foreach($rcollectionrow as $line){
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		if($line["store_id"]== "25" || $line["store_id"]== "36"){
			$rSHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
			$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
			$rSHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
			if($ineurope) 
				$rEBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
			else 
				$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
			$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
			$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
			$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
			$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
		}
		elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
			if(isOrderFraud($line['order_id'])){
				$rSHIPPING['fraud']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['fraud']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['fraud']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($ineurope) 
					$rROWINTf['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					$rROWINTf['total_zero_vat'] += $line["total_zerorated"];
				$rROWINTf['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
				$rROWINTf['total_vat'] += $line["tax_after_discount_noshipping"];
				$rROWINTf['gift']['voucher_in'] += $line["giftcard_invoice"];
				$rROWINTf['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			else{
				$rSHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($line['is_nonitemised']){
					$resnoni = workOutItemlessCredit($line, $line['order_id']);
					
					if($ineurope) 
						$rROWINT[$line['increment_id']]['total_zero_vat_bis'] += $resnoni['zero'];
					else 
						$rROWINT[$line['increment_id']]['total_zero_vat'] += $resnoni['zero'];
					$rROWINT[$line['increment_id']]['total_exc_vat'] += $resnoni['taxable'];
					$rROWINT[$line['increment_id']]['total_vat'] += $resnoni['taxable']*0.2;
					
					//var_dump($rUKINT[$line['increment_id']]);
					//${$arrvar}[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
					//${$arrvar}[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
				else{
					if($ineurope) 
						$rROWINT['total_zero_vat_bis'] += $line["total_zerorated"];
					else 
						$rROWINT['total_zero_vat'] += $line["total_zerorated"];
					$rROWINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
					$rROWINT['total_vat'] += $line["tax_after_discount_noshipping"];
					$rROWINT['gift']['voucher_in'] += $line["giftcard_invoice"];
					$rROWINT['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
			}
		}
		
	}
	unset($rcollectionrow);
	
	
	foreach($currencies as $currcode => $currval){
		$collectionvar = "rcollection".$currcode;
		$collectionvar = "rcollection".$currcode;
		$arrvar = "r".$currcode."INT";
		$arrvarf = "r".$currcode."INTf";
		foreach($$collectionvar as $line){
			$countrycode = ($line['region_code']);
			$ineurope = false;
			$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
			$eu_countries_array = explode(',',$eu_countries);
			if(in_array($countrycode, $eu_countries_array))
				$ineurope = true;
			//$ineurope = false;
			
			$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
			$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
			if($line["store_id"]== "25"|| $line["store_id"]== "36"){
				$rSHIPPING['eby']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
				$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
				$rSHIPPING['eby']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
				if($ineurope) 
					$rEBYINT['total_zero_vat_bis'] += $line["total_zerorated"];
				else 
					$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
				$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
				$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
				$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
				$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
			}
			elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
				if(isOrderFraud($line['order_id'])){
					$rSHIPPING['fraud']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
					$rSHIPPING['fraud']['total_vat'] += $line["base_shipping_tax_amount"];
					$rSHIPPING['fraud']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
					if($ineurope) 
						${$arrvarf}['total_zero_vat_bis'] += $line["total_zerorated"];
					else 
						${$arrvarf}['total_zero_vat'] += $line["total_zerorated"];
					${$arrvarf}['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
					${$arrvarf}['total_vat'] += $line["tax_after_discount_noshipping"];
					${$arrvarf}['gift']['voucher_in'] += $line["giftcard_invoice"];
					${$arrvarf}['gift']['voucher_spent'] += $line["giftcard_bought"];
				}
				else{
					$rSHIPPING['int']['total_exc_vat'] += ($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"];
					$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
					$rSHIPPING['int']['total_no_vat'] += ($line["base_shipping_tax_amount"] > 0) ? 0 :$line["base_shipping_amount"];
					if($line['is_nonitemised']){
						$resnoni = workOutItemlessCredit($line, $line['order_id']);
						
						if($ineurope) 
							${$arrvar}[$line['increment_id']]['total_zero_vat_bis'] += $resnoni['zero'];
						else 
							${$arrvar}[$line['increment_id']]['total_zero_vat'] += $resnoni['zero'];
						${$arrvar}[$line['increment_id']]['total_exc_vat'] += $resnoni['taxable'];
						${$arrvar}[$line['increment_id']]['total_vat'] += $resnoni['taxable']*0.2;
						
						//var_dump($rUKINT[$line['increment_id']]);
						//${$arrvar}[$line['increment_id']]['gift']['voucher_in'] += $line["giftcard_invoice"];
						//${$arrvar}[$line['increment_id']]['gift']['voucher_spent'] += $line["giftcard_bought"];
					}
					else{
						if($ineurope) 
							${$arrvar}['total_zero_vat_bis'] += $line["total_zerorated"];
						else 
							${$arrvar}['total_zero_vat'] += $line["total_zerorated"];
						${$arrvar}['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
						${$arrvar}['total_vat'] += $line["tax_after_discount_noshipping"];
						${$arrvar}['gift']['voucher_in'] += $line["giftcard_invoice"];
						${$arrvar}['gift']['voucher_spent'] += $line["giftcard_bought"];
					}
				}
			}
			
		}
		unset($$collectionvar);
		}
	
	//exit;
	//var_dump($JPYINT);exit;
	
	$xmlarr = array(
	"ITEM_01" => array("NAME"=>"UK - INT", "TAXCODE" => "1", "NOMINAL" => "4000-UK-INT", "TOTAL"=>$UKINT['total_exc_vat'] - $rUKINT['total_exc_vat'], "TAX"=>$UKINT['total_vat'] - $rUKINT['total_vat'],
	"TOTAL_WOREFUNDS" => $UKINT['total_exc_vat'], "TAX_WOREFUNDS" => $UKINT['total_vat']),
	"ITEM_02" => array("NAME"=>"UK - INT ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-UK-INT", "TOTAL"=>$UKINT['total_zero_vat'] - $rUKINT['total_zero_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $UKINT['total_zero_vat'] - $UKINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
	"ITEM_02BIS" => array("NAME"=>"UK - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-UK-INT", "TOTAL"=>$UKINT['total_zero_vat_bis'] - $rUKINT['total_zero_vat_bis'] , "TAX"=>0,
	"TOTAL_WOREFUNDS" => $UKINT['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
	"ITEM_03" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$UKINT['gift']['voucher_spent'] - $rUKINT['gift']['voucher_spent'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $UKINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
	"ITEM_04" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$UKINT['gift']['voucher_in'] - $rUKINT['gift']['voucher_in'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $UKINT['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit"),
	/*);
	$xmlarreur = array(*/
	"ITEM_05" => array("NAME"=>"EUR - INT", "TAXCODE" => "1", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>$EURINT['total_exc_vat'] - $rEURINT['total_exc_vat'], "TAX"=>$EURINT['total_vat'] - $rEURINT['total_vat'],
	"TOTAL_WOREFUNDS" => $EURINT['total_exc_vat'], "TAX_WOREFUNDS" => $EURINT['total_vat']),
	"ITEM_06" => array("NAME"=>"EUR - INT ZERO RATED",   "TAXCODE" => "2", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>$EURINT['total_zero_vat'] - $rEURINT['total_zero_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EURINT['total_zero_vat'], "TAX_WOREFUNDS" => 0),
	"ITEM_06BIS" => array("NAME"=>"EUR - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>$EURINT['total_zero_vat_bis'] - $rEURINT['total_zero_vat_bis'] , "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EURINT['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
	"ITEM_06BIS2" => array("NAME"=>"EUR - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$EURINT['gift']['voucher_spent'] - $rEURINT['gift']['voucher_spent'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EURINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
	"ITEM_06BIS2" => array("NAME"=>"EUR - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$EURINT['gift']['voucher_in'] - $rEURINT['gift']['voucher_in'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EURINT['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit"),
	/*);
	$xmlarrrow = array(*/
	"ITEM_07" => array("NAME"=>"ROW - INT", "TAXCODE" => "1", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>$ROWINT['total_exc_vat'] - $rROWINT['total_exc_vat'], "TAX"=>$ROWINT['total_vat'] - $rROWINT['total_vat'],
	"TOTAL_WOREFUNDS" => $ROWINT['total_exc_vat'], "TAX_WOREFUNDS" => $ROWINT['total_vat']),
	"ITEM_08" => array("NAME"=>"ROW - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>$ROWINT['total_zero_vat'] - $rROWINT['total_zero_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $ROWINT['total_zero_vat'], "TAX_WOREFUNDS" => 0),
	"ITEM_08BIS" => array("NAME"=>"ROW - ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>$ROWINT['total_zero_vat_bis'] - $rROWINT['total_zero_vat_bis'] , "TAX"=>0,
	"TOTAL_WOREFUNDS" => $ROWINT['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0),
	"ITEM_08BIS2" => array("NAME"=>"ROW - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$ROWINT['gift']['voucher_spent'] - $rROWINT['gift']['voucher_spent'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $ROWINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
	"ITEM_08BIS2" => array("NAME"=>"ROW - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$ROWINT['gift']['voucher_in'] - $rROWINT['gift']['voucher_in'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $ROWINT['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit"),
	
	/////////////////////////
	// FRAUD REFUND POSTINGS
	/////////////////////////
	"ITEM_01_FRAUD" => array("NAME"=>"UK - INT FD", "TAXCODE" => "1", "NOMINAL" => "2120--", "TOTAL"=> $rUKINTf['total_exc_vat'], "TAX"=>$rUKINTf['total_vat'],
	"TYPE"=>"credit"),
	"ITEM_02_FRAUD" => array("NAME"=>"UK - INT ZERO RATED FD",  "TAXCODE" => "2", "NOMINAL" => "2120--", "TOTAL"=>$rUKINTf['total_zero_vat'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_02BIS_FRAUD" => array("NAME"=>"UK - INT EXEMPT FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>$rUKINTf['total_zero_vat_bis'] , "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_03_FRAUD" => array("NAME"=>"UK - INT 0 RATED GCB FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>  $rUKINTf['gift']['voucher_spent'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_04_FRAUD" => array("NAME"=>"UK - INT 0 RATED GCP FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>$rUKINTf['gift']['voucher_in'], "TAX"=>0,
	"TYPE"=>"credit"),

	"ITEM_05_FRAUD" => array("NAME"=>"EUR - INT FD", "TAXCODE" => "1", "NOMINAL" => "2120--", "TOTAL"=> $rEURINTf['total_exc_vat'], "TAX"=> $rEURINTf['total_vat'],
	"TYPE"=>"credit"),
	"ITEM_06_FRAUD" => array("NAME"=>"EUR - INT ZERO RATED FD",   "TAXCODE" => "2", "NOMINAL" => "2120--", "TOTAL"=>$rEURINTf['total_zero_vat'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_06BIS_FRAUD" => array("NAME"=>"EUR - INT EXEMPT FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>$rEURINTf['total_zero_vat_bis'] , "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_06BIS2_FRAUD" => array("NAME"=>"EUR - INT 0 RATED GCB FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> $rEURINTf['gift']['voucher_spent'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_06BIS2_FRAUD" => array("NAME"=>"EUR - INT 0 RATED GCP FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> $rEURINTf['gift']['voucher_in'], "TAX"=>0,
	"TYPE"=>"credit"),

	"ITEM_07_FRAUD" => array("NAME"=>"ROW - INT FD", "TAXCODE" => "1", "NOMINAL" => "2120--", "TOTAL"=> $rROWINTf['total_exc_vat'], "TAX"=>$rROWINTf['total_vat'],
	"TYPE"=>"credit"),
	"ITEM_08_FRAUD" => array("NAME"=>"ROW - INT EXEMPT FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>$rROWINTf['total_zero_vat'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_08BIS_FRAUD" => array("NAME"=>"ROW - ZERO RATED FD",  "TAXCODE" => "2", "NOMINAL" => "2120--", "TOTAL"=>$rROWINTf['total_zero_vat_bis'] , "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_08BIS2_FRAUD" => array("NAME"=>"ROW - 0 RATED GCB FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> $rROWINTf['gift']['voucher_spent'], "TAX"=>0,
	"TYPE"=>"credit"),
	"ITEM_08BIS2_FRAUD" => array("NAME"=>"ROW - 0 RATED GCP FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> $rROWINTf['gift']['voucher_in'], "TAX"=>0,
	"TYPE"=>"credit"),
	
	/////////////////////////
	// END OF FRAUD REFUND POSTINGS
	/////////////////////////
	
	/*);
	
	$xmlarrgen = array(*/
	
	/*);
	$xmlarrpost = array(*/
	"ITEM_12" => array("NAME"=>"Postage - INT", "TAXCODE" => "1", "NOMINAL" => "4040--INT", "TOTAL"=>$SHIPPING['int']['total_exc_vat'] - $rSHIPPING['int']['total_exc_vat'], "TAX"=>$SHIPPING['int']['total_vat'] - $rSHIPPING['int']['total_vat'],
	"TOTAL_WOREFUNDS" => $SHIPPING['int']['total_exc_vat'], "TAX_WOREFUNDS" => $SHIPPING['int']['total_vat']),
	"ITEM_12_2" => array("NAME"=>"Postage - INT NOVAT", "TAXCODE" => "2", "NOMINAL" => "4040--INT", "TOTAL"=>$SHIPPING['int']['total_no_vat'] - $rSHIPPING['int']['total_no_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $SHIPPING['int']['total_no_vat'], "TAX_WOREFUNDS" => 0),
	
	///////////////////////////
	// Fraud  refund shipping
	///////////////////////////
	
	"ITEM_12_bis" => array("NAME"=>"Postage - INT FD", "TAXCODE" => "1", "NOMINAL" => "2120--", "TOTAL"=> $rSHIPPING['fraud']['total_exc_vat'], "TAX"=> $rSHIPPING['fraud']['total_vat'],
	"TYPE"=>"credit"),
	"ITEM_12_2_bis" => array("NAME"=>"Postage - INT NOVAT FD", "TAXCODE" => "2", "NOMINAL" => "2120--", "TOTAL"=>$rSHIPPING['fraud']['total_no_vat'], "TAX"=>0,
	"TYPE"=>"credit"),
	);
	
	$xmlarrebay = array(
	"ITEM_09" => array("NAME"=>"GEN - EBY", "TAXCODE" => "1", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>$EBYINT['total_exc_vat'] - $rEBYINT['total_exc_vat'], "TAX"=>$EBYINT['total_vat'] - $rEBYINT['total_vat'],
	"TOTAL_WOREFUNDS" => $EBYINT['total_exc_vat'], "TAX_WOREFUNDS" => $EBYINT['total_vat']),
	"ITEM_10" => array("NAME"=>"GEN - EBY ZERO RATED", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>$EBYINT['total_zero_vat'] - $rEBYINT['total_zero_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EBYINT['total_zero_vat'],  "TAXCODE" => "2", "TAX_WOREFUNDS" => 0),
	"ITEM_10BIS" => array("NAME"=>"GEN - EBY EXEMPT", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>$EBYINT['total_zero_vat_bis'] - $rEBYINT['total_zero_vat_bis'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $EBYINT['total_zero_vat_bis'],  "TAXCODE" => "0", "TAX_WOREFUNDS" => 0),
	"ITEM_11" => array("NAME"=>"Postage - EBY", "TAXCODE" => "1", "NOMINAL" => "4040--EBY", "TOTAL"=>$SHIPPING['eby']['total_exc_vat'] - $rSHIPPING['eby']['total_exc_vat'], "TAX"=>$SHIPPING['eby']['total_vat'] - $rSHIPPING['eby']['total_vat'],
	"TOTAL_WOREFUNDS" => $SHIPPING['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPING['eby']['total_vat']),
	"ITEM_11_2" => array("NAME"=>"Postage - EBY", "TAXCODE" => "2", "NOMINAL" => "4040--EBY", "TOTAL"=>$SHIPPING['eby']['total_no_vat'] - $rSHIPPING['eby']['total_no_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => $SHIPPING['eby']['total_no_vat'] , "TAX_WOREFUNDS" => 0),
	
	);
	
	
	foreach($currencies as $currcode => $currval){
		$iicur = 1;
		$arrvar = "".$currcode."INT";
		$rarrvar = "r".$currcode."INT";
		$rarrvarf = "r".$currcode."INTf";
		$xmlarr["ITEM_{$iicur}_$currcode"] = array("NAME"=>"$currcode - INT", "TAXCODE" => "1", "NOMINAL" => "4000-$currcode-INT", "TOTAL"=>${$arrvar}['total_exc_vat'] - ${$rarrvar}['total_exc_vat'], "TAX"=>${$arrvar}['total_vat'] - ${$rarrvar}['total_vat'],
	"TOTAL_WOREFUNDS" => ${$arrvar}['total_exc_vat'], "TAX_WOREFUNDS" => ${$arrvar}['total_vat']);
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode"] = array("NAME"=>"$currcode - INT EXEMPT",  "TAXCODE" => "0", "NOMINAL" => "4000-$currcode-INT", "TOTAL"=>${$arrvar}['total_zero_vat'] - ${$rarrvar}['total_zero_vat'], "TAX"=>0,
	"TOTAL_WOREFUNDS" => ${$arrvar}['total_zero_vat'], "TAX_WOREFUNDS" => 0);
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode"] = array("NAME"=>"$currcode - ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-$currcode-INT", "TOTAL"=>${$arrvar}['total_zero_vat_bis'] - ${$rarrvar}['total_zero_vat_bis'] , "TAX"=>0,
	"TOTAL_WOREFUNDS" => ${$arrvar}['total_zero_vat_bis'], "TAX_WOREFUNDS" => 0);
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode"] = array("NAME"=>"$currcode - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>${$arrvar}['gift']['voucher_spent'] - ${$rarrvar}['voucher_spent'], "TAX"=>0,
		"TOTAL_WOREFUNDS" => ${$arrvar}['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0);
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode"] = array("NAME"=>"$currcode - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>${$arrvar}['gift']['voucher_in'] - ${$rarrvar}['voucher_in'], "TAX"=>0,
		"TOTAL_WOREFUNDS" => ${$arrvar}['gift']['voucher_in'], "TAX_WOREFUNDS" => 0, "TYPE"=>"credit");
		$iicur++;
		
		$xmlarr["ITEM_{$iicur}_$currcode_FRAUD"] = array("NAME"=>"ROW - INT FD", "TAXCODE" => "1", "NOMINAL" => "2120--", "TOTAL"=> ${$arrvarf}['total_exc_vat'], "TAX"=> ${$arrvarf}['total_vat'],
		"TYPE"=>"credit");
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode_FRAUD"] = array("NAME"=>"ROW - INT EXEMPT FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=>${$arrvarf}['total_zero_vat'], "TAX"=>0,
		"TYPE"=>"credit");
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode_FRAUD"] = array("NAME"=>"ROW - ZERO RATED FD",  "TAXCODE" => "2", "NOMINAL" => "2120--", "TOTAL"=>${$arrvarf}['total_zero_vat_bis'] , "TAX"=>0,
		"TYPE"=>"credit");
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode_FRAUD"] = array("NAME"=>"ROW - 0 RATED GCB FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> ${$arrvarf}['gift']['voucher_spent'], "TAX"=>0,
		"TYPE"=>"credit");
		$iicur++;
		$xmlarr["ITEM_{$iicur}_$currcode_FRAUD"] = array("NAME"=>"ROW - 0 RATED GCP FD",  "TAXCODE" => "0", "NOMINAL" => "2120--", "TOTAL"=> ${$arrvarf}['gift']['voucher_in'], "TAX"=>0,
		"TYPE"=>"credit");
		$iicur++;
	}
	
	//var_dump($xmlarr); exit;
	
	$nominals = array();
	
	
	/*$f = fopen("sum23febexport.csv", "w+");
	//$f = fopen("25janexport-credit.csv", "w+");
	$headers = true;
		foreach($xmlarr as $subitem){
			//unset($subitem['TOTAL_WOREFUNDS']);
			//unset($subitem['TAX_WOREFUNDS']);
			if($subitem['TOTAL'] == 0) continue;
			if($headers){
				$headers = false;
				fputcsv($f, (array_keys($subitem)));
			}
			//if($subitem['total_exc_vat'] == 0)continue;			
			fputcsv($f, (array_values($subitem)));
		}
		foreach($xmlarrebay as $subitem){
			//unset($subitem['TOTAL_WOREFUNDS']);
			//unset($subitem['TAX_WOREFUNDS']);
			if($subitem['TOTAL'] == 0) continue;
			if($headers){
				$headers = false;
				fputcsv($f, (array_keys($subitem)));
			}
			//if($subitem['total_exc_vat'] == 0)continue;			
			fputcsv($f, (array_values($subitem)));
		}
	exit;*/
	
	foreach($xmlarr as $temp)
		$nominals[$temp['NOMINAL']]= $temp['NOMINAL'];
	foreach($xmlarrebay as $temp)
		$nominals[$temp['NOMINAL']]= $temp['NOMINAL'];
	//var_dump($nominals);exit;
	//$i = 1;
	foreach($xmlarr as $index => $item){
		//var_dump(($item));
		if(round($item["TOTAL"],2) <= 0) continue;
		$iname = str_pad($i, 2, '0', STR_PAD_LEFT);
		$xml = array_to_orderarray($item, "INTFIT", substr($item['NAME'], 0, 15)."_".$datei->format("Ymd"), $datei);
		if($xml == null) continue;
		
		$dir = dirname(__FILE__)."/exportsdm/";
		if(!is_dir($dir)) mkdir($dir);
		$dom = dom_import_simplexml($xml)->ownerDocument;
		$dom->formatOutput = true;
		//header("Content-type: text/xml");
		file_put_contents($dir."$iname-{$item['NAME']}_{$item['NOMINAL']}_".$datei->format("Ymd").".xml", $dom->saveXML());
		$i++;
	}
	foreach($xmlarrebay as $index => $item){
		//var_dump(($item["TOTAL"]));
		if(round($item["TOTAL"],2) <= 0) continue;
		$iname = str_pad($i, 2, '0', STR_PAD_LEFT);
		$xml = array_to_orderarray($item, "EBAYFIT", substr($item['NAME'], 0, 15)."_".$datei->format("Ymd"), $datei);
		if($xml == null) continue;
	
		$dir = dirname(__FILE__)."/exportsdm/";
		if(!is_dir($dir)) mkdir($dir);
		$dom = dom_import_simplexml($xml)->ownerDocument;
		$dom->formatOutput = true;
		//header("Content-type: text/xml");
		file_put_contents($dir."$iname-{$item['NAME']}_{$item['NOMINAL']}_".$datei->format("Ymd").".xml", $dom->saveXML());
		$i++;
	}
	
	var_dump($datei->format("d-m-Y"));
	$datei->add(new DateInterval('P1D'));

}
while($datei <= $dateo);

//$xml = new SimpleXMLElement('<root/>');
//array_to_xml($xmlarr, $xml);




function array_to_orderarray($item, $cust, $name, $datei)
{
	$xml = "";
		//var_dump($data);
		
		$orderdata = array();
		$orderdata['ORDER_TYPE'] = 1;
		$fullpay = true;
		
		$orderdata['ORDER_NUMBER'] = $name;
		//$orderdata['WAREHOUSE'] = "ASHINGTON";
		
		$orderdata['SET_ACKNOWLEDGED_FLAG'] = "YES";
		
		$orderdata['CUST_ACC_REF'] = $cust;
		
		
		//$orderdata['CUST_ACC_REF'] = str_pad($data->CUSTOMER->CUSTOMER_ID, 8, "0", STR_PAD_LEFT);
		$orderdata['ORDER_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_REQUESTED_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_PROMISED_DATE'] = $datei->format("Y/m/d");
		$orderdata['CUSTOMER_ORDER_NUMBER'] = $name;
		
		$orderdata['INV_POSTAL_NAME'] = $cust;
		$orderdata['INV_ADDRESS_LINE_1'] = "";
		$orderdata['INV_ADDRESS_LINE_2'] = "";
		$orderdata['INV_ADDRESS_LINE_3'] = "";
		$orderdata['INV_ADDRESS_LINE_4'] = "";
		$orderdata['INV_POST_CODE'] = "";
		$orderdata['INV_CITY'] = "";
		$orderdata['INV_COUNTY'] = "";
		$orderdata['INV_CONTACT'] = "";
		$orderdata['INV_TELEPHONE'] = "";
		$orderdata['INV_FAX'] = "";//$billingAddress->getData('');
		$orderdata['INV_EMAIL'] = "";
		$orderdata['INV_TAX_NUMBER'] = "";
		$orderdata['INV_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['INV_COUNTRY_CODE'] = "";
		$orderdata['INV_COUNTRY'] = "";
		
		$orderdata['DEL_POSTAL_NAME'] = "";
		$orderdata['DEL_ADDRESS_LINE_1'] = "";
		$orderdata['DEL_ADDRESS_LINE_2'] = "";
		$orderdata['DEL_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
		$orderdata['DEL_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
		$orderdata['DEL_POST_CODE'] = "";
		$orderdata['DEL_CITY'] = "";
		$orderdata['DEL_COUNTY'] = "";
		$orderdata['DEL_CONTACT'] = "";
		$orderdata['DEL_TELEPHONE'] = "";
		$orderdata['DEL_FAX'] = "";//$billingAddress->getData('');
		$orderdata['DEL_EMAIL'] = "";
		$orderdata['DEL_TAX_NUMBER'] = "";
		$orderdata['DEL_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['DEL_COUNTRY_CODE'] ="";
		$orderdata['DEL_COUNTRY'] = "";
		
		
		$orderdata['PAYMENT_IN_FULL'] = "NO";
		
		
		$del_meth = "";
		
		
		//$orderdata['PAYMENT_VALUE'] = $order->getData('');
		//$orderdata['PAYMENT_REF'] = $order->getData('');
		//$orderdata['PAYMENT_METHOD'] = $order->getData('');
		
		
		
		$orderdata['EXCHANGE_RATE'] = "";
		$orderdata['ARE_PRICES_TAX_INCLUSIVE'] = "YES";
		
		
		$i = 0;
			if((isset($item['TYPE']) && $item['TYPE'] = "credit")){
				//return null;
			}
			if((float)$item["TOTAL"] < 0 || (isset($item['TYPE']) && $item['TYPE'] = "credit")){
				
				$orderdata['CREDIT'] = 1;
				if(strpos($orderdata['CUSTOMER_ORDER_NUMBER'], "UK - INT ZERO R") !== null){
					$orderdata['CUSTOMER_ORDER_NUMBER'] = "GIFTCARD ".$datei->format("Ymd");
				}
			}
			else{
				
				$orderdata['CREDIT'] = 0;
			}
			if((float)$item["TOTAL"] == 0) return null;
			$i++;
			$nominal = explode("-", $item["NOMINAL"]);
			//var_dump($item);exit;
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 2;
			$orderdata['ITEM_'.$i.'_LINE_EXC_TAX'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = implode("-", $nominal); //"S1"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = $item["NAME"];
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = 1;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $item["TOTAL"];
			$orderdata['ITEM_'.$i.'_UNITTAX'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = "0";
			$orderdata['ITEM_'.$i.'_TAXCODE'] = $item["TAXCODE"];
			$orderdata['ITEM_'.$i.'_NOMINAL'] = $nominal[0];
			$orderdata['ITEM_'.$i.'_CC'] = $nominal[1];
			$orderdata['ITEM_'.$i.'_DEPT'] = $nominal[2];
			
		
			
		
		

		$orderdata['DISCOUNT_EX'] = 0;
		$orderdata['DISCOUNT_INC'] = 0;
		$orderdata['DISCOUNT_TAX'] = 0;
		$orderdata['ITEMS_COUNT'] = $i;
		$orderdata['DO_INVOICE'] = "0";
		$orderdata['DO_SHIPPED'] = "0";
		$orderdata['DO_POSTED'] = "0";
		$orderdata['TAKEN_BY'] = "DM SCRIPT";
		
		//var_dump($orderdata);
		
		$xml = array("data"=> $orderdata);
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><form></form>');
		arrayToXml($xml,$xml_data);
		
		
		return $xml_data;
}


function arrayToXml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            arrayToXml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
    	if(strpos($key, "ITEM_")!== false) $key = "ITEM";
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function getRows($datein, $dateout, $currency = "GBP"){
	$sql = "SELECT  
	(SELECT SUM(IF(sales_flat_order_item.base_tax_amount = 0, sales_flat_order_item.base_discount_amount, (sales_flat_order_item.base_discount_amount/1.2))) 
	FROM sales_flat_order_item WHERE sales_flat_order_item.order_id = main_table.entity_id) *-1
	AS base_discount_excl_vat,
	`main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       IF(`a`.`country_id` IS NULL, `b`.`country_id`, `a`.`country_id`) AS
       `region_code`,
       `fix`.`base_tax_after_discount` as tax_after_discount,
       `fix`.`base_tax_after_discount_noshipping` as tax_after_discount_noshipping,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       `main_table`.`created_at`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.item_id IS NOT NULL,IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0),0)) AS total_zerorated
FROM `sales_flat_invoice` as invoices
INNER JOIN   `sales_flat_order` AS `main_table` ON `main_table`.entity_id = `invoices`.`order_id`
       LEFT JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
        JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  ( payments.method <> \"c3v12finance\" ) AND ( payments.method <> \"purchaseorder\" ) AND ( payments.method <> \"raw_financebackend\" ) AND ( payments.method <> \"free\" ) AND ( payments.method <> \"cyclescheme2\" )
       AND ( invoices.created_at BETWEEN
             '{$datein}' AND '{$dateout}'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR main_table.customer_id IS NULL )
       AND ( main_table.order_currency_code = '{$currency}' ) 
       
       
       GROUP BY main_table.entity_id
	";
	//var_dump($sql);exit;
	return $sql;
	
}

function getRowsReturn($datein, $dateout, $currency = "GBP"){
	$sql = "
	SELECT   
((SELECT SUM(IF(sales_flat_creditmemo_item.base_discount_amount IS NULL, 0,IF(sales_flat_creditmemo_item.base_tax_amount = 0, sales_flat_creditmemo_item.base_discount_amount, (sales_flat_creditmemo_item.base_discount_amount/1.2))))                                                           
        FROM sales_flat_creditmemo_item WHERE sales_flat_creditmemo_item.parent_id = main_table.entity_id) *-1) as discountnovat,	
	  `main_table`.*,
           `a`.`store_name`,
           `a`.`created_at`                                                            AS `purchased_on`,
           (`base_tax_after_discount` * -1)                                                 AS `tax_after_discount`,
           (`base_tax_after_discount_noshipping`)                                           AS `tax_after_discount_noshipping`,
                      Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS `shipto_name`,
           `b`.`city`                                                                  AS `shipto_city`,
           `b`.`region`                                                                AS `shipto_state`,
           `b`.`country_id`,
           IF(`b`.`country_id` IS NULL, `c`.`country_id`, `b`.`country_id`) AS
       		`region_code`,
                      Concat(Coalesce(c.firstname, ''), ' ', Coalesce(c.lastname, '')) AS `billto_name`,
           `main_table`.`increment_id`                                                 AS `order_increment_id`,
           `main_table`.`created_at`                                                   AS `purchased_on`,
           `main_table`.`billing_address_id`,
           `main_table`.`shipping_address_id`,
           `main_table`.`subtotal`,
           (main_table.subtotal - IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)) AS `goods_net`,
           IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)                         AS `discount_amount`,
           `main_table`.`tax_amount`,
           `main_table`.`shipping_amount`,
           `main_table`.`grand_total`,
           `main_table`.`shipping_tax_amount`,
           `main_table`.`subtotal_incl_tax`,
           (main_table.subtotal_incl_tax - main_table.subtotal)                                                AS `item_tax`,
           (IF(main_table.customer_bal_total_refunded IS NULL, 0.00, main_table.customer_bal_total_refunded) ) AS `giftcard_invoice`,
           (
                  SELECT IF(Sum(row_invoiced) IS NULL, 0.00,Sum(row_invoiced) )
                  FROM   `sales_flat_creditmemo_item` ci
                  JOIN   sales_flat_order_item sfoi
                  ON     sfoi.item_id = ci.parent_id
                  WHERE  sfoi.product_type = \"giftcard\"
                  AND    sfoi.order_id = main_table.order_id)          AS `giftcard_bought`,
           Sum(IF(items.entity_id IS NOT NULL,IF(items.base_tax_amount = 0, items.base_row_total, 0), 0)) AS total_zerorated,
	    IF(items.entity_id IS NULL, 1, 0) as is_nonitemised,
	    payments.method as payment_method
FROM       `sales_flat_creditmemo`                                     AS `main_table`
INNER JOIN `sales_flat_order`                                          AS `a`
ON         main_table.order_id = a.entity_id
LEFT JOIN       `sales_flat_creditmemo_item` items
ON         items.parent_id = main_table.entity_id
LEFT JOIN  `dimasoft_credit_vat_corrected` AS `fix`
ON         fix.credit_id = main_table.entity_id
LEFT JOIN `sales_flat_order_payment` AS `payments`
ON payments.parent_id = a.entity_id
LEFT JOIN `sales_flat_order_address` AS `b`
ON         main_table.shipping_address_id = b.entity_id
INNER JOIN `sales_flat_order_address` AS `c`
ON         main_table.billing_address_id = c.entity_id
WHERE      (
                      main_table.created_at BETWEEN '{$datein}' AND        '{$dateout}')
AND        (
                      main_table.grand_total >= 0)
AND        (
                      main_table.order_currency_code = '{$currency}')
AND ( a.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR a.customer_id IS NULL )
AND ( payments.method <> \"c3v12finance\" ) AND ( payments.method <> \"purchaseorder\" ) AND ( payments.method <> \"raw_financebackend\" ) AND ( payments.method <> \"free\" ) AND ( payments.method <> \"cyclescheme2\" )
GROUP BY   main_table.entity_id";
//var_dump($sql);exit;
	return $sql;
}

function isOrderFraud($id){
	$read = Mage::getSingleton('core/resource')->getConnection('core_read');
	$sql = "SELECT *  FROM `sales_flat_order_status_history` 
	WHERE `comment` = 'STOCK RELEASED FOR SALE - OFFLINE REFUND COMPLETE - order placed on hold as suspected fraud for more than 7 days'
	AND parent_id = $id";
	$res = $read->fetchAll($sql);
	return (count($res) > 0);
}

function workOutItemlessCredit($credit, $oid){
	//var_dump($oid);
	$read = Mage::getSingleton('core/resource')->getConnection('core_read');
	
	$sql = "SELECT sales_flat_order.*, corr.base_tax_after_discount_noshipping as corrtax FROM sales_flat_order 
	JOIN dimasoft_order_vat_corrected corr on corr.order_id = sales_flat_order.entity_id WHERE entity_id = $oid";
	$order = $read->fetchAll($sql);
	if(count($order) == 0) return false;
	$order = $order[0];
	$sql = "SELECT credit.* FROM sales_flat_creditmemo credit 
	LEFT JOIN sales_flat_creditmemo_item items on items.parent_id = credit.entity_id
	WHERE credit.order_id = $oid AND items.entity_id IS NULL AND credit.entity_id < ".$credit['entity_id'];
	
	$credits = $read->fetchAll($sql);
	$niacredited = 0;
	if(count($credits) > 0){
		foreach($credits as $credit_found){
			$niacredited += $credit_found['base_grand_total'];
		}
	}
	
	$sql = "SELECT credit.*, corr.base_tax_after_discount_noshipping as corrtax FROM sales_flat_creditmemo credit 
	JOIN dimasoft_credit_vat_corrected corr on corr.credit_id = credit.entity_id
	LEFT JOIN sales_flat_creditmemo_item items on items.parent_id = credit.entity_id
	WHERE credit.order_id = $oid AND items.entity_id IS NOT NULL AND credit.entity_id < ".$credit['entity_id']." GROUP BY credit.entity_id";
	
	$credits = $read->fetchAll($sql);
	
	$maxvatrefund = $order['corrtax'];
	$vatrefunded =0;
	
	if(count($credits) > 0){
		foreach($credits as $credit_found){
			$vatrefunded += $credit_found['corrtax'];
			
		}
	}
	$actualvatrefunded = $maxvatrefund - $vatrefunded - $niacredited;
	
	$credittotal = $credit['base_grand_total'] - $credit['base_shipping_incl_tax'] / 1.2;
	if($actualvatrefunded <= 0){
		return ["taxable"=> 0, "zero"=> $credittotal];
	}
	elseif($actualvatrefunded > 0 && $actualvatrefunded >= $credit['base_grand_total']){
		return ["taxable"=> $credittotal / 1.2, "zero"=> 0];
	}
	else{
		$vatableamountleft = $actualvatrefunded * 0.2;
		return ["taxable"=> $vatableamountleft/ 1.2 , "zero"=> $credittotal-$vatableamountleft];
	}
	
	

}
