<?php
//$timezop = date_default_timezone_set ( "Europe/London" );
//$script_tz = date_default_timezone_get();
(ini_set('date.timezone', 'Europe/London'));
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../../htdocs_new/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$timetocheck = date("Y-m-d H:i:s", strtotime("-20 minutes"));

$timestampchecl = strtotime("-10 minutes");
$dt = new DateTime("@$timestampchecl", new DateTimeZone('UTC')); 
//echo date_format($dt, 'Y-m-d H:i:s T')."\n";
$dt->setTimezone(new DateTimeZone('Europe/London'));
$timetocheck =  date_format($dt, 'Y-m-d H:i:s');

//echo $timetocheck;exit;

//echo $timetocheck ."\n";
// Gathering orders
$orders = Mage::getModel('sales/order')->getCollection()
    ->addFieldToFilter('status', 'preprocessing_bt')
	->addFieldToFilter('created_at', array(
            'to' => $timetocheck,
            ))
    ->addAttributeToSelect('*');

// Gathering order IDS for mass "select" query of sagepay details
$orderids = array();
foreach ($orders as $order) {
    $data = (object)$order->getData();
	$orderids[] = $data->entity_id;
}

// Fetching sagepay details for all orders
$saypayinfo = array();
if(count($orderids)>0){
	$result=$write->fetchAll("SELECT * FROM `sales_flat_order_payment` WHERE `parent_id` IN (".implode(",", $orderids).")");
	foreach($result as $line){
		$saypayinfo[$line['parent_id']] = $line;
	}
}

// Looping through all orders
foreach ($orders as $order) {
	$skip = false;
    $data = (object)$order->getData();
	$mismatchedamt = false;
	$cardcountry = "";
	$thirdman_score = 100;
	
	try{
		if(isset($saypayinfo[$data->entity_id])){
			$row = $saypayinfo[$data->entity_id];	
		}
		else{
			throw new Exception("no sage info for this order");
		}
	}
	catch(exception $e){
		echo $e->getMessage();
		$row = null;
	}
	// If we did find a matching sagepay entry, carry on
	if($row != null){
		
		$btinfo = unserialize($saypayinfo[$data->entity_id]['additional_information']);
		//var_dump($btinfo);exit;
		$due = (float)$data->grand_total;
		$paid = (float)$saypayinfo[$data->entity_id]['amount_paid'];
		//var_dump($paid-$due
		$fraud = $saypayinfo[$data->entity_id]['adyen_total_fraud_score']; 
		//if($fraud != 0 &&empty( $fraud)) return '';
		$resultsf = json_decode($saypayinfo[$data->entity_id]['cc_avs_status'], true);
		$avs = (isset($resultsf['rawavc'])) ? $resultsf['rawavc'] : "n/a";
		$avsc = ($avs == "M" || $avs == "D" || $avs == "F"|| $avs == "X") ? "green" : "red";
		$cvc = (isset($resultsf['rawcvc'])) ? $resultsf['rawcvc'] : "n/a";
		$cvcc = ($cvc == "M" ) ? "green" : "red";
		$raw3ds = (isset($resultsf['rawcvc'])) ? "Yes" : "No";
		$raw3dsc = (isset($resultsf['rawcvc'])) ? "green":"red";
		if($due != $paid && (($paid-$due > 0.015) || ($paid-$due < -0.015))){
                $mismatchedamt = true;
        }
		if($mismatchedamt){
			$order->setState("processing");
			$order->setStatus("mismatched_payment");
			$order->addStatusToHistory($order->getStatus(), 
			"Order was detected as having mismatched amounts from payment provider.", false); // Setting this to true will send email to customer
		}
		else{
			$passall = true;
			if($avsc != "green")
				$passall = false;
			if($cvcc != "green")
				$passall = false;
			if($raw3dsc != "green")
				$passall = false;
			if($fraud > 10)
				$passall = false;
			if($passall){
				// ALL OK
				$order->setState("processing");
				$order->setStatus("processing");
				$order->addStatusToHistory($order->getStatus(), 'Fraud checks passed.', false); // Setting this to true will send email to customer
			}
			else{
				// Possible fraud
				$order->setState("processing");
				$order->setStatus("fraud");
				$order->addStatusToHistory($order->getStatus(), 
				"Order was detected as potential fraud. Manual check required.", false); // Setting this to true will send email to customer
			}
		}
		
		try{
			// Saving the order, DUH
			$order->save();
			var_dump($order->getIncrementId()." - ".$order->getStatus());
		}
		catch(exception $e){
			echo $e->getMessage();
			
		}
	}
}


#### CANCEL OFF ORDERS

$sql = '
SELECT updated_at, sales_flat_order.entity_id, increment_id  FROM `sales_flat_order` 
JOIN sales_flat_order_payment pay on pay.parent_id = sales_flat_order.entity_id
WHERE sales_flat_order.`status` = \'pending\' AND sales_flat_order.created_at < (NOW() - INTERVAL 100 MINUTE)  AND pay.adyen_refusal_reason_raw IS NULL
ORDER BY `sales_flat_order`.`created_at`  ASC
';
$result=$write->fetchAll($sql);
foreach($result as $row){
	$order = Mage::getModel('sales/order')->load($row['entity_id']);
	if ($order->canCancel()) {
		try {
			$order->cancel();

			// remove status history set in _setState
			$order->getStatusHistoryCollection(true);

			// do some more stuff here
			// ...

			$order->save();
		} catch (Exception $e) {
			//Mage::logException($e);
		}
	}
	
}

function pretty_json($json, $ret= "\n", $ind="\t") {

    $beauty_json = '';
    $quote_state = FALSE;
    $level = 0; 

    $json_length = strlen($json);

    for ($i = 0; $i < $json_length; $i++)
    {                               

        $pre = '';
        $suf = '';

        switch ($json[$i])
        {
            case '"':                               
                $quote_state = !$quote_state;                                                           
                break;

            case '[':                                                           
                $level++;               
                break;

            case ']':
                $level--;                   
                $pre = $ret;
                $pre .= str_repeat($ind, $level);       
                break;

            case '{':

                if ($i - 1 >= 0 && $json[$i - 1] != ',')
                {
                    $pre = $ret;
                    $pre .= str_repeat($ind, $level);                       
                }   

                $level++;   
                $suf = $ret;                                                                                                                        
                $suf .= str_repeat($ind, $level);                                                                                                   
                break;

            case ':':
                $suf = ' ';
                break;

            case ',':

                if (!$quote_state)
                {  
                    $suf = $ret;                                                                                                
                    $suf .= str_repeat($ind, $level);
                }
                break;

            case '}':
                $level--;   

            case ']':
                $pre = $ret;
                $pre .= str_repeat($ind, $level);
                break;

        }

        $beauty_json .= $pre.$json[$i].$suf;

    }

    return $beauty_json;

}  