<?php
//$timezop = date_default_timezone_set ( "Europe/London" );
//$script_tz = date_default_timezone_get();
(ini_set('date.timezone', 'Europe/London'));
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
require_once('../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$order = Mage::getModel('sales/order')->load(927568);

$data = (object)$order->getData();

$result=$write->fetchAll("SELECT * FROM `sagepaysuite_transaction` WHERE `order_id` IN (". $order->getEntityId().")");
	foreach($result as $line){
		$saypayinfo[$line['order_id']] = $line;
	}

$row = $saypayinfo[$order->getEntityId()];
//var_dump($row[]);
$_configuration = Mage::app()->getStore($data->store_id)->getConfig("sagepayreporting/account");
$vendor = strtolower($_configuration['vendor']);
$username = ($_configuration['username']);
$password = (Mage::helper('core')->decrypt($_configuration['password']));

// Is it a "test" order?
$env = ($_configuration['enviroment'] == 1)?true:false;

// Initialasing the API connection
//var_dump($data->store_id);
$adminapi = new SagePayAdminApi("$vendor", "$username", "$password", $env);

// Looking for the transaction
$transaction = $adminapi->getTransactionDetail(array('vendortxcode' => $row['vendor_tx_code']));

// If sagepay raises an error, we raise an exception
if((string)$transaction->errorcode != "0000"){
	var_dump($transaction);
}

// Getting necessary info from sagepay's response
$paymentsystemdetails = ((string)$transaction->paymentsystemdetails);

var_dump($transaction->amount);
