<?php
require_once(dirname(__FILE__).'/../phpmailer/class.phpmailer.php');
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4096M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$it = 1;
$dateto = new DateTime("2018-08-31 23:59:59");

$datefrom = new DateTime("2018-08-01 00:00:00");

$email = new PHPMailer();
$email->SetFrom('cron@startfitness.co.uk', 'Start Fitness Cron'); //Name is optional
$email->Subject   = 'Rounding and po/finance report - '.$datefrom->format("Y-m");
$email->Body      = "Reports Attached.";
//$email->AddAddress( 'geoffrey@startfitness.co.uk' );
$email->AddAddress( 'geoffrey@rawtechnologies.co.uk' );
$files =[];

while($it > 0){
$it--;	

$sql = [];

$sql["orders"] = "SELECT  
	(SELECT SUM(IF(sales_flat_order_item.base_tax_amount = 0, sales_flat_order_item.base_discount_amount, (sales_flat_order_item.base_discount_amount/1.2))) 
	FROM sales_flat_order_item WHERE sales_flat_order_item.order_id = main_table.entity_id) *-1
	AS base_discount_excl_vat,
	`main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       IF(`a`.`country_id` IS NULL, `b`.`country_id`, `a`.`country_id`) AS
       `region_code`,
       `fix`.`base_tax_after_discount` as tax_after_discount,
       `fix`.`base_tax_after_discount_noshipping` as tax_after_discount_noshipping,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       `main_table`.`created_at`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.item_id IS NOT NULL,IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0),0)) AS total_zerorated,
	    IF(items.item_id IS NULL, 1, 0) as is_nonitemised
FROM `sales_flat_invoice` as invoices
INNER JOIN   `sales_flat_order` AS `main_table` ON `main_table`.entity_id = `invoices`.`order_id`
       LEFT JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
         JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  (( payments.method = \"c3v12financeNOPE\" ) OR ( payments.method = \"purchaseorder\" ) OR ( payments.method = \"raw_financebackend\" ) OR ( payments.method = \"freeNOPE\" ) OR ( payments.method = \"cyclescheme2\" ))
       AND ( invoices.created_at BETWEEN
             '".$datefrom->format("Y-m-d 00:00:00")."' AND '".$dateto->format("Y-m-d 23:59:59")."'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR main_table.customer_id IS NULL ) 
       
       
       GROUP BY main_table.entity_id";

$sql["credits"] = "SELECT   
((SELECT SUM(IF(sales_flat_creditmemo_item.base_discount_amount IS NULL, 0,IF(sales_flat_creditmemo_item.base_tax_amount = 0, sales_flat_creditmemo_item.base_discount_amount, (sales_flat_creditmemo_item.base_discount_amount/1.2))))                                                           
        FROM sales_flat_creditmemo_item WHERE sales_flat_creditmemo_item.parent_id = main_table.entity_id) *-1) as discountnovat,	
	  `main_table`.*,
           `a`.`store_name`,
           `a`.`created_at`                                                            AS `purchased_on`,
           (`base_tax_after_discount` * -1)                                                 AS `tax_after_discount`,
           (`base_tax_after_discount_noshipping`)                                           AS `tax_after_discount_noshipping`,
                      Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS `shipto_name`,
           `b`.`city`                                                                  AS `shipto_city`,
           `b`.`region`                                                                AS `shipto_state`,
           `b`.`country_id`,
           IF(`b`.`country_id` IS NULL, `c`.`country_id`, `b`.`country_id`) AS
       		`region_code`,
                      Concat(Coalesce(c.firstname, ''), ' ', Coalesce(c.lastname, '')) AS `billto_name`,
           `main_table`.`increment_id`                                                 AS `order_increment_id`,
           `main_table`.`created_at`                                                   AS `purchased_on`,
           `main_table`.`billing_address_id`,
           `main_table`.`shipping_address_id`,
           `main_table`.`subtotal`,
           (main_table.subtotal - IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)) AS `goods_net`,
           IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)                         AS `discount_amount`,
           `main_table`.`tax_amount`,
           `main_table`.`shipping_amount`,
           `main_table`.`grand_total`,
           `main_table`.`shipping_tax_amount`,
           `main_table`.`subtotal_incl_tax`,
           (main_table.subtotal_incl_tax - main_table.subtotal)                                                AS `item_tax`,
           (IF(main_table.customer_bal_total_refunded IS NULL, 0.00, main_table.customer_bal_total_refunded) ) AS `giftcard_invoice`,
           (
                  SELECT IF(Sum(row_invoiced) IS NULL, 0.00,Sum(row_invoiced) )
                  FROM   `sales_flat_creditmemo_item` ci
                  JOIN   sales_flat_order_item sfoi
                  ON     sfoi.item_id = ci.parent_id
                  WHERE  sfoi.product_type = \"giftcard\"
                  AND    sfoi.order_id = main_table.order_id)          AS `giftcard_bought`,
           Sum(IF(items.entity_id IS NOT NULL,IF(items.base_tax_amount = 0, items.base_row_total, 0), 0)) AS total_zerorated,
	    IF(items.entity_id IS NULL, 1, 0) as is_nonitemised,
	    payments.method as payment_method
FROM       `sales_flat_creditmemo`                                     AS `main_table`
INNER JOIN `sales_flat_order`                                          AS `a`
ON         main_table.order_id = a.entity_id
LEFT JOIN       `sales_flat_creditmemo_item` items
ON         items.parent_id = main_table.entity_id
LEFT JOIN  `dimasoft_credit_vat_corrected` AS `fix`
ON         fix.credit_id = main_table.entity_id
LEFT JOIN `sales_flat_order_payment` AS `payments`
ON payments.parent_id = a.entity_id
LEFT JOIN `sales_flat_order_address` AS `b`
ON         main_table.shipping_address_id = b.entity_id
INNER JOIN `sales_flat_order_address` AS `c`
ON         main_table.billing_address_id = c.entity_id
WHERE      
                      main_table.created_at BETWEEN '".$datefrom->format("Y-m-d 00:00:00")."' AND '".$dateto->format("Y-m-d 23:59:59")."'
AND        (
                      main_table.grand_total >= 0)

AND ( a.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR a.customer_id IS NULL )
AND (( payments.method = \"c3v12financeNOPE\" ) OR ( payments.method = \"purchaseorder\" ) OR ( payments.method = \"raw_financebackend\" ) OR ( payments.method = \"freeNOPE\" ) OR ( payments.method = \"cyclescheme2\" ))

GROUP BY   main_table.entity_id";


$sql["orders-r"] = "SELECT  
	(SELECT SUM(IF(sales_flat_order_item.base_tax_amount = 0, sales_flat_order_item.base_discount_amount, (sales_flat_order_item.base_discount_amount/1.2))) 
	FROM sales_flat_order_item WHERE sales_flat_order_item.order_id = main_table.entity_id) *-1
	AS base_discount_excl_vat,
	`main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       IF(`a`.`country_id` IS NULL, `b`.`country_id`, `a`.`country_id`) AS
       `region_code`,
       `fix`.`base_tax_after_discount` as tax_after_discount,
       `fix`.`base_tax_after_discount_noshipping` as tax_after_discount_noshipping,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       `main_table`.`created_at`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.item_id IS NOT NULL,IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0),0)) AS total_zerorated,
	    IF(items.item_id IS NULL, 1, 0) as is_nonitemised
FROM `sales_flat_invoice` as invoices
INNER JOIN   `sales_flat_order` AS `main_table` ON `main_table`.entity_id = `invoices`.`order_id`
       LEFT JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
         JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  ( payments.method <> \"c3v12finance\" ) AND ( payments.method <> \"purchaseorder\" ) AND ( payments.method <> \"raw_financebackend\" ) AND ( payments.method <> \"free\" ) AND ( payments.method <> \"cyclescheme2\" )
       AND ( invoices.created_at BETWEEN
             '".$datefrom->format("Y-m-d 00:00:00")."' AND '".$dateto->format("Y-m-d 23:59:59")."'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR main_table.customer_id IS NULL )
       
       AND main_table.base_grand_total <> main_table.base_total_paid AND (main_table.base_grand_total - main_table.base_total_paid) between -0.05 AND 0.05
       
       GROUP BY main_table.entity_id";



$ordersnotimported = [];

$res = $read->fetchAll($sql['orders']);
foreach($res as $index => $sqll):

	$ordersnotimported[] = $sqll['increment_id']." - ".$sqll['base_grand_total']." - ".$sqll['payment_method']." - ".$sqll['created_at'];

endforeach;

$creditsnotimported = [];
$res = $read->fetchAll($sql['credits']);
foreach($res as $index => $sqll):

	$creditsnotimported[] = $sqll['increment_id']." - ".$sqll['base_grand_total']." - ".$sqll['payment_method']." - ".$sqll['created_at'];

endforeach;
$roundingissue = [];
$totalrounding = 0;
$res = $read->fetchAll($sql['orders-r']);
foreach($res as $index => $sqll):

	$roundingissue[] = $sqll['increment_id']." - ".$sqll['base_grand_total']." - ".$sqll['base_total_paid']." = ".($sqll['base_grand_total'] - $sqll['base_total_paid']);
	$totalrounding+= ($sqll['base_grand_total'] - $sqll['base_total_paid']);
endforeach;

//var_dump($ordersnotimported, $creditsnotimported);

}


$email->Body      = "<h1>Orders not imported to sage:</h1><br />".implode("<br />\n",$ordersnotimported);
$email->Body      .= "<br/><br/><h1>Credits not imported to sage:</h1><br />".implode("<br />\n",$creditsnotimported);
$email->Body      .= "<br/><br/><h1>Rounding issue on following orders (total is &pound;".number_format($totalrounding, 2)."):</h1><br />".implode("<br />\n",$roundingissue);

$email->IsHTML(true); 
return $email->Send();

foreach($files as $filename){
	unlink($filename);
}

