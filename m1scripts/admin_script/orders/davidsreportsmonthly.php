<?php
require_once(dirname(__FILE__).'/../phpmailer/class.phpmailer.php');
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4096M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$it = 1;
$dateto = new DateTime("2018-11-31 23:59:59");

$datefrom = new DateTime("2018-11-01 00:00:00");

$email = new PHPMailer();
$email->SetFrom('cron@startfitness.co.uk', 'Start Fitness Cron'); //Name is optional
$email->Subject   = 'Daily sales report - '.$dateto->format("Y-m-d");
$email->Body      = "Bikes and itemised Reports Attached.";
//$email->AddAddress( 'geoffrey@startfitness.co.uk' );
$email->AddAddress( 'geoffrey@rawtechnologies.co.uk' );
$files =[];

while($it > 0){
$it--;	

$sql = [];

$sql["bikes"] = "SELECT orders.increment_id, orders.entity_id, orders.created_at, orders.base_grand_total, SUM(oi.base_row_total_incl_tax) as total_bikes, GROUP_CONCAT(brand.name) as brands, max(pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_order_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_order orders ON orders.entity_id = oi.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
WHERE prod.attribute_set_id IN (9,34) AND oi.created_at >= '".$datefrom->format("Y-m-d")." 00:00:00' AND oi.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0
GROUP BY orders.entity_id";

$sql["detailed"] = "SELECT orders.increment_id, orders.entity_id, orders.created_at, orders.base_grand_total as parent_order_total, prod.created_at as product_created_at, brand.name as brands, model.value as model, sport.value as sport, cost.value as cost_price, oi.base_row_total as row_total_excl_vat, oi.base_row_total_incl_tax as row_total_incl_vat, oi.base_discount_amount as row_discount_incl_vat,  (oi.base_row_total_incl_tax - oi.base_discount_amount) as row_total_incl_vat_incl_discount,  (pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_order_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_order orders ON orders.entity_id = oi.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
LEFT JOIN catalog_product_entity_varchar model on model.attribute_id = 134 AND model.store_id = 0 AND model.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_int as sportint ON sportint.attribute_id = 389 AND sportint.store_id = 0 AND sportint.entity_id = oi.product_id
LEFT JOIN eav_attribute_option_value sport on sport.option_id = sportint.value
LEFT JOIN catalog_product_entity_decimal cost on cost.attribute_id = 293 and cost.store_id = 0 and cost.entity_id = oi.product_id
WHERE /*prod.attribute_set_id IN (9,34) AND*/ oi.created_at >= '".$datefrom->format("Y-m-d")." 00:00:00' AND oi.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0";




foreach($sql as $index => $sqll):

	$lines = $read->fetchAll($sqll);
	$headers = false;
	$filename = $index."-".$datefrom->format("Y-m").".csv";
	$f = fopen($filename, "w+");
	foreach($lines as $line){
		if(!$headers){
			fputcsv($f, array_keys($line));
			$headers = true;
		}
		fputcsv($f, array_values($line));
	}
	fclose($filename);
	
	
	
	
	
	$file_to_attach = $filename;
	
	$email->AddAttachment( $file_to_attach , 'report'.md5(time()).$dateto->format("Y-m-d").'.csv' );
	$files [] = $file_to_attach;

endforeach;

$dateto->modify("- 35 days");
$dateto->modify("last day of this month");
$datefrom = clone $dateto;
$datefrom->modify("first day of this month");

}

return $email->Send();

foreach($files as $filename){
	unlink($filename);
}

