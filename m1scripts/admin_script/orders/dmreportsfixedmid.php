<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '16048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');


$currencies = array(
	//"GBP" => "pounds",
	//"EUR" => "euros",
	//"USD" => "dollars",
	"CAD" => "cdollars",
	"AUD" => "adollars",
	"NOK" => "nkrone",
	"SEK" => "skrone",
	"JPY" => "yen",
	"RUB" => "ruble",
	"MXN" => "pesos",
	"DKK" => "corona",
);

$datei = new DateTime();
$datei->sub(new DateInterval("P15M"));
$dateo = new DateTime();
$dateo->sub(new DateInterval("P1D"));

$datei = new DateTime("2017-03-01");

$dateo = new DateTime("2018-12-11");

$datei = new DateTime("2018-09-03");

$dateo = new DateTime("2018-09-03");


//echo getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP");exit;
$totalsall = array();
$totalsallr = array();
$totalsallh = array();
do{
	$totals = array();
	$totalsr = array();


	//$collection = Mage::getModel('reportsalesorders/reportsalesorders')->getCollection();
	//$collection->setDateRange($datei, $dateo);
	$collection = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP"));
	
	
	//exit;
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$collectioneur = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$collectionrow = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "USD"));
	
	foreach($currencies as $code=>$label){
		$varname = "collection".$code;
		$$varname = $read->fetchAll(getRows($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), $code));
		$varname = $code."INT";
		$$varname = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	}
	
	
	$SHIPPING = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0));
	$UKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$EURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$ROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$EBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	
	
	
	
	foreach($collection as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//var_dump($countrycode, $ineurope);
		//$ineurope = true;
		//var_dump($line['total_zerorated'] . " - " . $line["giftcard_bought"]);
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totals[$indextotal]))
			$totals[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat'] - $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totals[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totals[$indextotal]["total"]+= $line['total_invoiced'];
		
		
		
	}
	unset($collection);
	
	foreach($collectioneur as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totals[$indextotal]))
			$totals[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal_invoiced'] += $line['base_discount_excl_vat'] - $line["giftcard_bought"];
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totals[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totals[$indextotal]["total"]+= $line['total_invoiced'];
		
	}
	unset($collectioneur);
	foreach($collectionrow as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totals[$indextotal]))
			$totals[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal_invoiced'] += 
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totals[$indextotal]["base_total"]+= (($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"])
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totals[$indextotal]["total"]+= $line['total_invoiced'];
		
	}



	
	unset($collectionrow);
	
	
	foreach($currencies as $currcode => $currval){
		
		$collectionvar = "collection".$currcode;
		$arrvar = $currcode."INT";
		foreach($$collectionvar as $line){
			if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
			
			$countrycode = ($line['region_code']);
			$ineurope = false;
			$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
			$eu_countries_array = explode(',',$eu_countries);
			if(in_array($countrycode, $eu_countries_array))
				$ineurope = true;
			//var_dump($currcode ." => ". $countrycode ,$ineurope);
			//$ineurope = false;
			
			
			
			$indextotal = $line['payment_method']."-".$line['order_currency_code'];
			if(!isset($totals[$indextotal]))
				$totals[$indextotal] = array("base_total"=>0, "total"=>0);
			
			$line['base_subtotal_invoiced'] += 
			$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
			$totals[$indextotal]["base_total"]+= (($line["base_shipping_tax_amount"] == 0) ? 0 :$line["base_shipping_amount"])
			+$line["base_shipping_tax_amount"]
			+($line["base_shipping_amount"])
			+$line["total_zerorated"]
			+$line["base_subtotal_invoiced"] - $line['total_zerorated']
			+$line["tax_after_discount_noshipping"]
			+$line["giftcard_bought"];
			$totals[$indextotal]["total"]+= $line['total_invoiced'];
			
		}
	
	
		
		unset($collectionvar);
	}
	
	
	//$rcollection = Mage::getModel('reportcreditmemos/reportcreditmemos')->getCollection();
	//$rcollection->setDateRange($datei, $dateo);
	//var_dump($rcollection->getSelect()->__toString());exit;
	$rcollection =  $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "GBP"));
	
	//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
	//$collectioneur->setDateRange($datei, $dateo);
	$rcollectioneur =  $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "EUR"));
	
	//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
	//$collectionrow->setDateRange($datei, $dateo);
	$rcollectionrow = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), "USD"));
	
	
	foreach($currencies as $code=>$label){
		$varname = "rcollection".$code;
		$$varname = $read->fetchAll(getRowsReturn($datei->format("Y-m-d 00:00:00"), $datei->format("Y-m-d 23:59:59"), $code));
		$varname = "r".$code."INT";
		$$varname = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	}
	
	
	$rSHIPPING = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0, 'total_no_vat'=>0));
	$rUKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rEURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	$rEBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_zero_vat_bis'=>0,  'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
	
	foreach($rcollection as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totalsr[$indextotal]))
			$totalsr[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totalsr[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totalsr[$indextotal]["total"]+= $line['grand_total'];
		
	}
	unset($rcollection);
	foreach($rcollectioneur as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = true;
		
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totalsr[$indextotal]))
			$totalsr[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totalsr[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totalsr[$indextotal]["total"]+= $line['grand_total'];
		
	}
	unset($rcollectioneur);
	foreach($rcollectionrow as $line){
		if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
		$countrycode = ($line['region_code']);
		$ineurope = false;
		$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array($countrycode, $eu_countries_array))
			$ineurope = true;
		//$ineurope = false;
		
		$indextotal = $line['payment_method']."-".$line['order_currency_code'];
		if(!isset($totalsr[$indextotal]))
			$totalsr[$indextotal] = array("base_total"=>0, "total"=>0);
		
		$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
		$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
		$totalsr[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+($line["base_shipping_amount"])
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
		$totalsr[$indextotal]["total"]+= $line['grand_total'];
		
	}
	unset($rcollectionrow);

	
	
	foreach($currencies as $currcode => $currval){
		
		$collectionvar = "rcollection".$currcode;
		$arrvar = "r".$currcode."INT";
		foreach($$collectionvar as $line){
			if(!in_array($line['store_id'], array(25,36,6,19,8))) continue;
			$countrycode = ($line['region_code']);
			$ineurope = false;
			$eu_countries = Mage::getStoreConfig('general/country/eu_countries');
			$eu_countries_array = explode(',',$eu_countries);
			if(in_array($countrycode, $eu_countries_array))
				$ineurope = true;
			//$ineurope = false;
			
			$indextotal = $line['payment_method']."-".$line['order_currency_code'];
			if(!isset($totalsr[$indextotal]))
				$totalsr[$indextotal] = array("base_total"=>0, "total"=>0);
			
			$line['base_subtotal'] += $line['discountnovat']- $line["giftcard_bought"];;
			$line['total_zerorated'] = $line['total_zerorated'] - $line["giftcard_bought"];
			$totalsr[$indextotal]["base_total"]+= 
		+$line["base_shipping_tax_amount"]
		+$line["base_shipping_amount"]
		+$line["total_zerorated"]
		+$line["base_subtotal_invoiced"] - $line['total_zerorated']
		+$line["tax_after_discount_noshipping"]
		+$line["giftcard_bought"];
			$totalsr[$indextotal]["total"]+= $line['grand_total'];
			
		}
		unset($$collectionvar);
		}
	
	//exit;
	//var_dump($JPYINT);exit;
	
	
		//$totalsallh = array_merge( $totalsallh,array_keys($totals));
		foreach($totals as $key=>  $dumn){
			//foreach($dumn as $key=> $dumn2)
				if(!isset($totalsallh[$key])) $totalsallh[$key] = $key; 
		}
		foreach($totalsr as $key=>  $dumn){
			//foreach($dumn as $key=> $dumn2)
				if(!isset($totalsallh[$key])) $totalsallh[$key] = $key; 
		}
		$totalsall[$datei->format("Y-m-d")] = $totals;
		$totalsallr[$datei->format("Y-m-d")] = $totalsr;
		$datei->add(new DateInterval('P1D'));
	}
	while($datei <= $dateo);

	
//var_dump($totalsallh);exit;
$f = fopen("reconcile/reconcilemid.csv", "w+");
$fr = fopen("reconcile/reconcilemid-r.csv", "w+");
sort(($totalsallh));
$totalsallh = array_merge(array("Date"=>"Date"), $totalsallh);

fputcsv($f, $totalsallh);
fputcsv($fr, $totalsallh);


foreach($totalsall as $date=>$data){
	$row=array();
	foreach($totalsallh as $header){
		if($header == "Date"){
			$row[] = $date;
			continue;
		}
		$row[] = (isset($data[$header])) ? $data[$header]['total'] : "0";
	}
	fputcsv($f, $row);
}
foreach($totalsallr as $date=>$data){
	$row=array();
	foreach($totalsallh as $header){
		if($header == "Date"){
			$row[] = $date;
			continue;
		}
		$row[] = (isset($data[$header])) ? $data[$header]['total'] : ""; 
	}
	fputcsv($fr, $row);
}

fclose($f);
fclose($fr);
//$xml = new SimpleXMLElement('<root/>');
//array_to_xml($xmlarr, $xml);




function array_to_orderarray($item, $cust, $name, $datei)
{
	$xml = "";
		//var_dump($data);
		
		$orderdata = array();
		$orderdata['ORDER_TYPE'] = 1;
		$fullpay = true;
		
		$orderdata['ORDER_NUMBER'] = $name;
		//$orderdata['WAREHOUSE'] = "ASHINGTON";
		
		$orderdata['SET_ACKNOWLEDGED_FLAG'] = "YES";
		
		$orderdata['CUST_ACC_REF'] = $cust;
		
		
		//$orderdata['CUST_ACC_REF'] = str_pad($data->CUSTOMER->CUSTOMER_ID, 8, "0", STR_PAD_LEFT);
		$orderdata['ORDER_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_REQUESTED_DATE'] = $datei->format("Y/m/d");
		$orderdata['ORDER_PROMISED_DATE'] = $datei->format("Y/m/d");
		$orderdata['CUSTOMER_ORDER_NUMBER'] = $name;
		
		$orderdata['INV_POSTAL_NAME'] = $cust;
		$orderdata['INV_ADDRESS_LINE_1'] = "";
		$orderdata['INV_ADDRESS_LINE_2'] = "";
		$orderdata['INV_ADDRESS_LINE_3'] = "";
		$orderdata['INV_ADDRESS_LINE_4'] = "";
		$orderdata['INV_POST_CODE'] = "";
		$orderdata['INV_CITY'] = "";
		$orderdata['INV_COUNTY'] = "";
		$orderdata['INV_CONTACT'] = "";
		$orderdata['INV_TELEPHONE'] = "";
		$orderdata['INV_FAX'] = "";//$billingAddress->getData('');
		$orderdata['INV_EMAIL'] = "";
		$orderdata['INV_TAX_NUMBER'] = "";
		$orderdata['INV_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['INV_COUNTRY_CODE'] = "";
		$orderdata['INV_COUNTRY'] = "";
		
		$orderdata['DEL_POSTAL_NAME'] = "";
		$orderdata['DEL_ADDRESS_LINE_1'] = "";
		$orderdata['DEL_ADDRESS_LINE_2'] = "";
		$orderdata['DEL_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
		$orderdata['DEL_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
		$orderdata['DEL_POST_CODE'] = "";
		$orderdata['DEL_CITY'] = "";
		$orderdata['DEL_COUNTY'] = "";
		$orderdata['DEL_CONTACT'] = "";
		$orderdata['DEL_TELEPHONE'] = "";
		$orderdata['DEL_FAX'] = "";//$billingAddress->getData('');
		$orderdata['DEL_EMAIL'] = "";
		$orderdata['DEL_TAX_NUMBER'] = "";
		$orderdata['DEL_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['DEL_COUNTRY_CODE'] ="";
		$orderdata['DEL_COUNTRY'] = "";
		
		
		$orderdata['PAYMENT_IN_FULL'] = "NO";
		
		
		$del_meth = "";
		
		
		//$orderdata['PAYMENT_VALUE'] = $order->getData('');
		//$orderdata['PAYMENT_REF'] = $order->getData('');
		//$orderdata['PAYMENT_METHOD'] = $order->getData('');
		
		
		
		$orderdata['EXCHANGE_RATE'] = "";
		$orderdata['ARE_PRICES_TAX_INCLUSIVE'] = "YES";
		
		
		$i = 0;
			if((isset($item['TYPE']) && $item['TYPE'] = "credit")){
				//return null;
			}
			if((float)$item["TOTAL"] < 0 || (isset($item['TYPE']) && $item['TYPE'] = "credit")){
				
				$orderdata['CREDIT'] = 1;
				if(strpos($orderdata['CUSTOMER_ORDER_NUMBER'], "UK - INT ZERO R") !== null){
					$orderdata['CUSTOMER_ORDER_NUMBER'] = "GIFTCARD ".$datei->format("Ymd");
				}
			}
			else{
				
				$orderdata['CREDIT'] = 0;
			}
			if((float)$item["TOTAL"] == 0) return null;
			$i++;
			$nominal = explode("-", $item["NOMINAL"]);
			//var_dump($item);exit;
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 2;
			$orderdata['ITEM_'.$i.'_LINE_EXC_TAX'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = implode("-", $nominal); //"S1"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = $item["NAME"];
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = 1;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $item["TOTAL"];
			$orderdata['ITEM_'.$i.'_UNITTAX'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = "0";
			$orderdata['ITEM_'.$i.'_TAXCODE'] = $item["TAXCODE"];
			$orderdata['ITEM_'.$i.'_NOMINAL'] = $nominal[0];
			$orderdata['ITEM_'.$i.'_CC'] = $nominal[1];
			$orderdata['ITEM_'.$i.'_DEPT'] = $nominal[2];
			
		
			
		
		

		$orderdata['DISCOUNT_EX'] = 0;
		$orderdata['DISCOUNT_INC'] = 0;
		$orderdata['DISCOUNT_TAX'] = 0;
		$orderdata['ITEMS_COUNT'] = $i;
		$orderdata['DO_INVOICE'] = "0";
		$orderdata['DO_SHIPPED'] = "0";
		$orderdata['DO_POSTED'] = "0";
		$orderdata['TAKEN_BY'] = "DM SCRIPT";
		
		//var_dump($orderdata);
		
		$xml = array("data"=> $orderdata);
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><form></form>');
		arrayToXml($xml,$xml_data);
		
		
		return $xml_data;
}


function arrayToXml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            arrayToXml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
    	if(strpos($key, "ITEM_")!== false) $key = "ITEM";
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function getRows($datein, $dateout, $currency = "GBP"){
	$sql = "SELECT  
	(SELECT SUM(IF(sales_flat_order_item.base_tax_amount = 0, sales_flat_order_item.base_discount_amount, (sales_flat_order_item.base_discount_amount/1.2))) 
	FROM sales_flat_order_item WHERE sales_flat_order_item.order_id = main_table.entity_id) *-1
	AS base_discount_excl_vat,
	`main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       IF(`a`.`country_id` IS NULL, `b`.`country_id`, `a`.`country_id`) AS
       `region_code`,
       `fix`.`base_tax_after_discount` as tax_after_discount,
       `fix`.`base_tax_after_discount_noshipping` as tax_after_discount_noshipping,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       `main_table`.`created_at`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0)) AS total_zerorated
FROM `sales_flat_invoice` as invoices
INNER JOIN   `sales_flat_order` AS `main_table` ON `main_table`.entity_id = `invoices`.`order_id`
       LEFT JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
       JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  ( payments.method <> \"c3v12finance\" )
       AND ( invoices.created_at BETWEEN
             '{$datein}' AND '{$dateout}'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR main_table.customer_id IS NULL )
       AND ( main_table.order_currency_code = '{$currency}' ) 
       
       
       GROUP BY main_table.entity_id
	";

	return $sql;
	
}

function getRowsReturn($datein, $dateout, $currency = "GBP"){
	$sql = "
	SELECT   
((SELECT SUM(IF(sales_flat_creditmemo_item.base_discount_amount IS NULL, 0,IF(sales_flat_creditmemo_item.base_tax_amount = 0, sales_flat_creditmemo_item.base_discount_amount, (sales_flat_creditmemo_item.base_discount_amount/1.2))))                                                           
        FROM sales_flat_creditmemo_item WHERE sales_flat_creditmemo_item.parent_id = main_table.entity_id) *-1) as discountnovat,	
	  `main_table`.*,
           `a`.`store_name`,
           `a`.`created_at`                                                            AS `purchased_on`,
           (`base_tax_after_discount` * -1)                                                 AS `tax_after_discount`,
           (`base_tax_after_discount_noshipping`)                                           AS `tax_after_discount_noshipping`,
                      Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS `shipto_name`,
           `b`.`city`                                                                  AS `shipto_city`,
           `b`.`region`                                                                AS `shipto_state`,
           `b`.`country_id`,
           IF(`b`.`country_id` IS NULL, `c`.`country_id`, `b`.`country_id`) AS
       		`region_code`,
                      Concat(Coalesce(c.firstname, ''), ' ', Coalesce(c.lastname, '')) AS `billto_name`,
           `main_table`.`increment_id`                                                 AS `order_increment_id`,
           `main_table`.`created_at`                                                   AS `purchased_on`,
           `main_table`.`billing_address_id`,
           `main_table`.`shipping_address_id`,
           `main_table`.`subtotal`,
           (main_table.subtotal - IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)) AS `goods_net`,
           IF(main_table.discount_amount < 0 , Abs(main_table.discount_amount), main_table.discount_amount)                         AS `discount_amount`,
           `main_table`.`tax_amount`,
           `main_table`.`shipping_amount`,
           `main_table`.`grand_total`,
           `main_table`.`shipping_tax_amount`,
           `main_table`.`subtotal_incl_tax`,
           (main_table.subtotal_incl_tax - main_table.subtotal)                                                AS `item_tax`,
           (IF(main_table.customer_bal_total_refunded IS NULL, 0.00, main_table.customer_bal_total_refunded) ) AS `giftcard_invoice`,
           (
                  SELECT IF(Sum(row_invoiced) IS NULL, 0.00,Sum(row_invoiced) )
                  FROM   `sales_flat_creditmemo_item` ci
                  JOIN   sales_flat_order_item sfoi
                  ON     sfoi.item_id = ci.parent_id
                  WHERE  sfoi.product_type = \"giftcard\"
                  AND    sfoi.order_id = main_table.order_id)          AS `giftcard_bought`,
           Sum(IF(items.base_tax_amount = 0, items.base_row_total, 0)) AS total_zerorated,
`payments`.`method`                                              AS
`payment_method`
FROM       `sales_flat_creditmemo`                                     AS `main_table`
INNER JOIN `sales_flat_order`                                          AS `a`
ON         main_table.order_id = a.entity_id
JOIN       `sales_flat_creditmemo_item` items
ON         items.parent_id = main_table.entity_id
LEFT JOIN  `dimasoft_credit_vat_corrected` AS `fix`
ON         fix.credit_id = main_table.entity_id
LEFT JOIN `sales_flat_order_address` AS `b`
ON         main_table.shipping_address_id = b.entity_id
INNER JOIN `sales_flat_order_address` AS `c`
ON         main_table.billing_address_id = c.entity_id
LEFT JOIN `sales_flat_order_payment` AS `payments`
ON payments.parent_id = main_table.order_id
WHERE      (
                      main_table.created_at BETWEEN '{$datein}' AND        '{$dateout}')
AND        (
                      main_table.grand_total >= 0)
AND        (
                      main_table.order_currency_code = '{$currency}')
AND ( a.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187, 590691 )
              OR a.customer_id IS NULL )
GROUP BY   main_table.entity_id";
//var_dump($sql);exit;
	return $sql;
}

