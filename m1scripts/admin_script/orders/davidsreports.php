<?php
require_once(dirname(__FILE__).'/../phpmailer/class.phpmailer.php');
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '400096M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$dateto = new DateTime();
$dateto->modify("-1 days");

$datefrom = new DateTime("2017-08-01 00:00:00");
//$dateto->modify("-1 days");

$sql = [];

$sql["bikes"] = "SELECT orders.increment_id, orders.entity_id, orders.created_at, orders.order_currency_code,orders.base_grand_total, orders.grand_total, 
SUM(oi.base_row_total_incl_tax) as base_total_bikes, SUM(oi.row_total_incl_tax) as total_bikes, GROUP_CONCAT(brand.name) as brands, max(pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_order_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_order orders ON orders.entity_id = oi.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
WHERE prod.attribute_set_id IN (9,34) AND oi.created_at >= '".$dateto->format("Y-m-d")." 00:00:00' AND oi.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0 AND orders.status != 'canceled'
GROUP BY orders.entity_id";

$sql["detailed"] = "SELECT orders.increment_id, orders.entity_id, orders.created_at,  orders.order_currency_code, orders.base_grand_total as base_parent_order_total, orders.grand_total as parent_order_total,prod.created_at as product_created_at, brand.name as brands, model.value as model, sport.value as sport, cost.value as cost_price, oi.base_row_total as base_row_total_excl_vat, oi.row_total as row_total_excl_vat, oi.base_row_total_incl_tax as base_row_total_incl_vat,oi.row_total_incl_tax as row_total_incl_vat, oi.base_discount_amount as base_row_discount_incl_vat,oi.discount_amount as row_discount_incl_vat,  (oi.base_row_total_incl_tax - oi.base_discount_amount) as base_row_total_incl_vat_incl_discount,  (oi.row_total_incl_tax - oi.discount_amount) as row_total_incl_vat_incl_discount,  (pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_order_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_order orders ON orders.entity_id = oi.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
LEFT JOIN catalog_product_entity_varchar model on model.attribute_id = 134 AND model.store_id = 0 AND model.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_int as sportint ON sportint.attribute_id = 389 AND sportint.store_id = 0 AND sportint.entity_id = oi.product_id
LEFT JOIN eav_attribute_option_value sport on sport.option_id = sportint.value
LEFT JOIN catalog_product_entity_decimal cost on cost.attribute_id = 293 and cost.store_id = 0 and cost.entity_id = oi.product_id
WHERE  oi.created_at >= '".$dateto->format("Y-m-d")." 00:00:00' AND oi.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0 AND orders.status != 'canceled'";


$sql["bikes_returns"] = "SELECT credit.increment_id, credit.entity_id, credit.created_at, orders.order_currency_code, credit.base_grand_total, credit.grand_total, 
SUM(oi.base_row_total_incl_tax) as base_total_bikes,SUM(oi.row_total_incl_tax) as total_bikes, GROUP_CONCAT(brand.name) as brands, max(pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_creditmemo_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_creditmemo credit ON credit.entity_id = oi.parent_id
JOIN sales_flat_order orders ON orders.entity_id = credit.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
WHERE prod.attribute_set_id IN (9,34) AND credit.created_at >= '".$dateto->format("Y-m-d")." 00:00:00' AND credit.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0
GROUP BY orders.entity_id";

$sql["detailed_returns"] = "SELECT credit.increment_id, credit.entity_id, credit.created_at, orders.order_currency_code, 
credit.base_grand_total as base_parent_credit_total, 
credit.grand_total as parent_credit_total, 
prod.created_at as product_created_at, brand.name as brands, model.value as model, sport.value as sport, cost.value as cost_price, 
oi.base_row_total as base_row_total_excl_vat, 
oi.row_total as row_total_excl_vat, 
oi.base_row_total_incl_tax as base_row_total_incl_vat, 
oi.row_total_incl_tax as row_total_incl_vat, 
oi.base_discount_amount as base_row_discount_incl_vat,  
oi.discount_amount as row_discount_incl_vat,  
(oi.base_row_total_incl_tax - oi.base_discount_amount) as base_row_total_incl_vat_incl_discount,  
(oi.row_total_incl_tax - oi.discount_amount) as row_total_incl_vat_incl_discount,  
(pay.method) as method,
orders.customer_email, orders.customer_firstname, orders.customer_lastname, shipping.country_id, shipping.region
FROM `sales_flat_creditmemo_item` oi
JOIN catalog_product_entity prod ON prod.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_varchar brandid ON brandid.attribute_id = 337 AND brandid.store_id = 0 and brandid.entity_id = prod.entity_id
LEFT JOIN smashingmagazine_branddirectory_brand brand on brand.entity_id = brandid.value
JOIN sales_flat_creditmemo credit ON credit.entity_id = oi.parent_id
JOIN sales_flat_order orders ON orders.entity_id = credit.order_id
JOIN sales_flat_order_payment pay on pay.parent_id = orders.entity_id
JOIN sales_flat_order_address shipping on shipping.entity_id = orders.shipping_address_id
LEFT JOIN catalog_product_entity_varchar model on model.attribute_id = 134 AND model.store_id = 0 AND model.entity_id = oi.product_id
LEFT JOIN catalog_product_entity_int as sportint ON sportint.attribute_id = 389 AND sportint.store_id = 0 AND sportint.entity_id = oi.product_id
LEFT JOIN eav_attribute_option_value sport on sport.option_id = sportint.value
LEFT JOIN catalog_product_entity_decimal cost on cost.attribute_id = 293 and cost.store_id = 0 and cost.entity_id = oi.product_id
WHERE  credit.created_at >= '".$dateto->format("Y-m-d")." 00:00:00' AND credit.created_at <= '".$dateto->format("Y-m-d")." 23:59:59' AND pay.method != 'free' AND oi.base_row_total_incl_tax > 0";



$email = new PHPMailer();
$email->SetFrom('cron@startfitness.co.uk', 'Start Fitness Cron'); //Name is optional
$email->Subject   = 'Daily sales report - '.$dateto->format("Y-m-d");
$email->Body      = "Bikes and itemised Reports Attached. ".$dateto->format("Y-m-d");
$email->AddAddress( 'davidquinn@startfitness.co.uk' );
$email->AddAddress( 'geoffrey@rawtechnologies.co.uk' );

$files =[];
foreach($sql as $index => $sqll):

	$lines = $read->fetchAll($sqll);
	$headers = false;
	$filename = "".$index."-".md5(time()).".csv";
	$f = fopen($filename, "w+");
	foreach($lines as $line){
		if(!$headers){
			fputcsv($f, array_keys($line));
			$headers = true;
		}
		fputcsv($f, array_values($line));
	}
	fclose($filename);
	
	
	
	
	
	$file_to_attach = $filename;
	
	$email->AddAttachment( $file_to_attach , $filename );
	$files [] = $file_to_attach;

endforeach;

$email->Send();

foreach($files as $filename){
	unlink($filename);
}

