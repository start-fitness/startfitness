<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
require_once('../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$timetocheck = date("Y-m-d H:i:s", strtotime("-10 minutes"));
// Gathering orders
$orders = Mage::getModel('sales/order')->getCollection()
    ->addFieldToFilter('status', 'fraud')
    ->addFieldToFilter('state', 'processing')
	->addFieldToFilter('created_at', array(
            'to' => $timetocheck,
            ))
    ->addAttributeToSelect('*');

foreach ($orders as $order) {
	echo "here";
	$check = false;
	$data = (object)$order->getData();
	/*$_history = $order->getAllStatusHistory(); 
	if(count($_history)>0){
		//$historyItem = $_history[count($_history)-1];
		foreach($_history as $index => $historyItem)
		{
			//echo $index . " -- " .$historyItem->getData('comment');
			//echo "\n";
			if(strpos($historyItem->getData('comment'), "sagepay third man Checks failed NORESULT") !==false){
				echo "FOUND !\n";
				$check = true;
				break;
			}
		}
	}*/
	$sql = "SELECT * FROM sagepayreporting_fraud WHERE order_id = ".$data->entity_id;
	$row = $read->fetchRow($sql);
	if($row != null){
		//print_r($row);
		if($row['thirdman_action']== "NORESULT"){
			$check = true;
		}
	}
	if ($row == null){
		$check = true;
	}
	if($check){
		$_order = Mage::getModel('sales/order')->load($order->getId());
		Mage::unregister('reporting_store_id');
		Mage::register('reporting_store_id', $_order->getStoreId());

        if(is_object($_order->getSagepayInfo())) {
        	echo "checking score ...\n";
		    $rs = getFraud()->getTransactionDetails($_order->getSagepayInfo()->getVendorTxCode());
			
			$status = ($rs->getT3maction());//exit;
        	if($status == "OK"){
        		try{
				getPersistentFraud()->updateThirdMan($rs, $_order->getId());
				$_order->setStatus("preprocessing");
				$_order->save();
				}catch(Exception $e){
					echo "Could not do order $orderId... \n\n";	
				}
        	}
			
			
			echo "\norder {$_order->getId()} Third man updated\n\n";
			//exit;
		}
	}
}
function _getCanRank()
{
	return Mage::getStoreConfigFlag('payment/sagepaysuite/auto_fulfill_low_risk_trn');
}

function _getRank()
{
	return (int)Mage::getStoreConfig('payment/sagepaysuite/auto_fulfill_low_risk_trn_value');
}
function getPersistentFraud()
{
	return Mage::getModel('sagepayreporting/fraud');
}

function getFraud()
{
	return Mage::getModel('sagepayreporting/sagepayreporting');
}
