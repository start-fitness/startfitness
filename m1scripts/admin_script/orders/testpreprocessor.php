<?php
//$timezop = date_default_timezone_set ( "Europe/London" );
//$script_tz = date_default_timezone_get();
(ini_set('date.timezone', 'Europe/London'));
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
require_once('../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$timetocheck = date("Y-m-d H:i:s", strtotime("-20 minutes"));

$timestampchecl = strtotime("-20 minutes");
$dt = new DateTime("@$timestampchecl", new DateTimeZone('UTC')); 
//echo date_format($dt, 'Y-m-d H:i:s T')."\n";
$dt->setTimezone(new DateTimeZone('Europe/London'));
$timetocheck =  date_format($dt, 'Y-m-d H:i:s');

//echo $timetocheck;exit;

//echo $timetocheck ."\n";
// Gathering orders
$orders = Mage::getModel('sales/order')->getCollection()
    ->addFieldToFilter('status', 'preprocessing')
	->addFieldToFilter('entity_id', '1231592')
	//->addFieldToFilter('created_at', array(
            //'to' => $timetocheck,
    //        ))
    ->addAttributeToSelect('*');

// Gathering order IDS for mass "select" query of sagepay details
$orderids = array();
foreach ($orders as $order) {
    $data = (object)$order->getData();
	$orderids[] = $data->entity_id;
}

// Fetching sagepay details for all orders
$saypayinfo = array();
if(count($orderids)>0){
	$result=$write->fetchAll("SELECT * FROM `sagepaysuite_transaction` WHERE `order_id` IN (".implode(",", $orderids).")");
	foreach($result as $line){
		$saypayinfo[$line['order_id']] = $line;
	}
}

// Looping through all orders
foreach ($orders as $order) {
	$skip = false;
    $data = (object)$order->getData();
	$mismatchedamt = false;
	$cardcountry = "";
	$thirdman_score = 100;
	
	// Checking to see if that order has a matching sagepay entry (no reason why it souldn't have but better check)
	try{
		if(isset($saypayinfo[$data->entity_id])){
			$row = $saypayinfo[$data->entity_id];	
		}
		else{
			throw new Exception("no sage info for this order");
		}
	}
	catch(exception $e){
		echo $e->getMessage();
		$row = null;
	}
	// If we did find a matching sagepay entry, carry on
	if($row != null){
		$haserror = false;
		$errors = array();
		
		// Fields to check (key is database field, value is the value to check against)
		$tocheck = array(
		"address_result" => "MATCHED",
		"postcode_result" => "MATCHED",
		"cv2result" => "MATCHED",
		);
		foreach($tocheck as $key => $tomatch){
			if(!isset($row[$key]) || $row[$key] != $tomatch){
				$haserror = true;
				$errors[] = "$key NOT '$tomatch'";
			}
		}
		
		// Querying Sagepay Admin API
		try{
			
			// Fetching appropriate sagepay admin logins from the backend, depending on the order's store ID
			$_configuration = Mage::app()->getStore($data->store_id)->getConfig("sagepayreporting/account");
	        $vendor = strtolower($_configuration['vendor']);
	        $username = ($_configuration['username']);
	        $password = (Mage::helper('core')->decrypt($_configuration['password']));
	        
			// Is it a "test" order?
	        $env = ($_configuration['enviroment'] == 1)?true:false;
			
			// Initialasing the API connection
			$adminapi = new SagePayAdminApi("$vendor", "$username", "$password", $env);
			
			// Looking for the transaction
			$transaction = $adminapi->getTransactionDetail(array('vendortxcode' => $row['vendor_tx_code']));
			
			// If sagepay raises an error, we raise an exception
			if((string)$transaction->errorcode != "0000"){
				throw new Exception("sagepayerror");
			}
			
			$sagepayamt = $transaction->amount;
			
			if($data->grand_total != $sagepayamt){
				$mismatchedamt = true;
			}
			// Getting necessary info from sagepay's response
			$paymentsystemdetails = ((string)$transaction->paymentsystemdetails);
			//var_dump($transaction);
			//echo "HERE";
			$shipping_country = ((string)$transaction->billingcountry);
			$paymentsystemdetailssplit = explode(", ", $paymentsystemdetails);
			//var_dump($shipping_country);
			// Checking if the card's country matching shipping country
			if(isset($transaction->cardcountry)){
				$issue_country = (string)$transaction->cardcountry;
				if($shipping_country != $issue_country){
					$haserror = true;
					$errors[] = "card country (".$issue_country.") does not match billing country (".$shipping_country.")";
				}
				else{
					//has same country so add the country to a variable
					$cardcountry = $issue_country;
				}
			}
			else{
				$haserror = true;
				$errors[] = "card country could not be defined";
			}
			
			// save or update sage registered country to custom table
			unset($model);
			$model = Mage::getModel('customadmin/sageregisteredcountry');
			$sage_country = $model->load($order->getId(),'sales_order_id');
			if($sage_country->getId()){
				// update existing record
				$model->setSageRegisteredCountry($issue_country);
				try {
				        $model->save();
				        echo "Data updated successfully.";
				        
				    } catch (Exception $e){
				        echo $e->getMessage();
				}
			}
			else{
				// insert new record
				$model->setSalesOrderId($order->getId())
	              ->setSageRegisteredCountry($issue_country);
				  try {
				        $insertId = $model->save()->getId();
				        echo "Data successfully inserted. Insert ID: ".$insertId;
				    } catch (Exception $e){
				     echo $e->getMessage();  
				}
			}
		}
		catch(exception $e){
			$haserror = true;
			if($e->getMessage() == "sagepayerror")
				$errors[] = "sagepay could not retrieve this order";
			else
				$errors[] = "sagepay API unreachable for fraud checks";
		}
		
		// GET THIRD MAN SCORE
		$sql = "SELECT * FROM sagepayreporting_fraud WHERE order_id  = {$data->entity_id}";
		$row = $read->fetchRow($sql);
		//var_dump($row);
		if($row != null){
			//print_r($row);
			if($row['thirdman_action']== "NORESULT"){
				$haserror = true;
				$errors[] = "sagepay third man Checks failed NORESULT";
			}
			else{
				$thirdman_score = $row['thirdman_score'];
				if(is_numeric($row['thirdman_score']) && $row['thirdman_score'] > 0){
					$haserror = true;
					$errors[] = "sagepay third man Checks failed (score > 0)";
				}
			}
		}
		else{
			if(count($errors) < 1)
				$skip = true;
			//echo "no entries";
		}
		//print_r($errors);
		//exit;
		
		// CHECKING PAYMENT TABLE ENTRY
		$sql = "SELECT sfo.increment_id, st.status_detail, sfo.status, sfo.base_grand_total, sfo.created_at FROM `sagepaysuite_transaction` st LEFT JOIN `sales_flat_order` sfo ON st.order_id = sfo.entity_id WHERE st.status_detail LIKE '%CANCELLED%' AND sfo.increment_id IS NOT NULL AND order_id = {$data->entity_id} ORDER BY sfo.created_at desc";
		$row = $read->fetchRow($sql);
		if($row != null){
			$haserror == true;
			$errors[] = "Payment entry not found in the database";
		}
		
		//Check Customer Group - if wholesale approved customer or shop order etc... - All to be accepted
		//Check done at end as often trade orders do fail for reasons of different order / delivery addresses
 		$sql = "SELECT customer_group_id, customer_id FROM `sales_flat_order` where entity_id = {$data->entity_id}";
                $row = $read->fetchRow($sql);
		if ($row['customer_group_id'] == 2 || $row['customer_group_id'] == 6 || $row['customer_group_id'] == 7 || $row['customer_group_id'] == 9)
		{
			$haserror = false;
			$errors = array();
		}
		//Check if customer whitelisted with customer setting
		if($row['customer_id'] != null){
			$customer = Mage::getModel('customer/customer')->load($row['customer_id']);
			if($customer){
				if($customer->getData("bypass_fraud") == "1"){
					$haserror = false;
					$errors = array();
				}
			}
		}
		//var_dump($haserror, $errors);exit;

		//Check if card country in a preferred list and countries match.  If country none match wont progress.
		//But with check score 0 or more
		if ($cardcountry == "SE" || $cardcountry == "AU" || $cardcountry == "NZ" || $cardcountry == "DK" || $cardcountry == "FI")
		{
			//is a preferred none fraud country
			if ($thirdman_score < 1)
			{
				//is 0 or less in score
				$haserror == false;
	                        $errors[] = "";
			}
		}
		
		
		
		// If at least one check failed, we set the order as suspected fraud and raise an order comment.
		if($skip === false){
			if($mismatchedamt){
				$order->setState("processing");
				$order->setStatus("mismatched_payment");
				$order->addStatusToHistory($order->getStatus(), "Order was detected as having mismatched amounts from payment provider.", false); // Setting this to true will send email to customer
			}	
			else if($haserror){
				$order->setState("processing");
				$order->setStatus("fraud");
				$order->addStatusToHistory($order->getStatus(), "Order was detected as potential fraud. Manual check required. More info: { - ".implode(", - ", $errors)."}", false); // Setting this to true will send email to customer
			}
			
			// Else we set the order back to a pending status
			else{
				$order->setState("processing");
				$order->setStatus("processing");
				$order->addStatusToHistory($order->getStatus(), 'Fraud checks passed.', false); // Setting this to true will send email to customer
			}
			try{
				// Saving the order, DUH
				$order->save();
			}
			catch(exception $e){
				echo $e->getMessage();
				
			}
		}
		
	}
}
