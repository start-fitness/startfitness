<?php
//$timezop = date_default_timezone_set ( "Europe/London" );
//$script_tz = date_default_timezone_get();
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4096M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$timetocheck = date("Y-m-d H:i:s", strtotime("-90 minutes"));

$timestampchecl = strtotime("-2000 minutes");
$dt = new DateTime("@$timestampchecl", new DateTimeZone('UTC')); 
//echo date_format($dt, 'Y-m-d H:i:s T')."\n";
$dt->setTimezone(new DateTimeZone('Europe/London'));
$timetocheck =  date_format($dt, 'Y-m-d H:i:s');

//echo $timetocheck;exit;

//echo $timetocheck ."\n";
// Gathering orders
// DELETE DUPLICATE FROM AMAZON

$sqldup = "DELETE FROM `m2epro_order` 
WHERE m2epro_order.id IN 
	(SELECT id FROM (SELECT MAX(m2eo.id) as id 
	FROM `m2epro_amazon_order` 
	LEFT JOIN `m2epro_order` m2eo ON m2eo.id=m2epro_amazon_order.`order_id` 
	LEFT JOIN sales_flat_order ON sales_flat_order.entity_id = m2eo.magento_order_id 
	WHERE m2eo.create_date >= '{$timetocheck}'
	GROUP BY m2epro_amazon_order.amazon_order_id 
	HAVING COUNT(m2epro_amazon_order.amazon_order_id) > 1) 
	as innerq)";

$write->query($sqldup);
	
$sql = "SELECT sfo.`created_at`, sfo.`entity_id`, sfo.`increment_id`, sfo.`base_total_invoiced`, sfo.`customer_firstname`, sfo.`customer_lastname`, sfo.`state`,sfo.`status`, m2eo.`magento_order_id` FROM `sales_flat_order` sfo
LEFT JOIN `m2epro_order` m2eo ON m2eo.`magento_order_id`=sfo.`entity_id`
WHERE sfo.`store_id` IN (24,25,26) AND sfo.`created_at` >= '{$timetocheck}' AND m2eo.`magento_order_id` IS NULL AND sfo.`coupon_code` IS NULL AND sfo.status <> 'duplicatem2e' AND state NOT IN('complete','closed')
ORDER BY sfo.`entity_id`";

$oorders = $read->fetchAll($sql);


$ids = array();
foreach($oorders as $order){
	echo "\n".$order['increment_id']."\t - \t {$order['state']}/{$order['status']}  -- {$order['created_at']} {$order['customer_firstname']} {$order['customer_lastname']}";
	$ids[] = $order['entity_id'];
	
	try{
		$OldOrder = Mage::getModel('sales/order')->load($order['entity_id']);
	    $invoices = array();
		foreach ($OldOrder->getInvoiceCollection() as $invoice) {
			    $invoices[] = $invoice;
		}
	  	$service = Mage::getModel('sales/service_order', $OldOrder);
	    foreach ($invoices as $invoice) {
		    $creditmemo = $service->prepareInvoiceCreditmemo($invoice);
			$creditmemo->setPaymentRefundDisallowed(true)->register();
		    /*$creditmemo->register();*/ //creditmemo->refund();
		    
            $creditmemo->save();
		}
		$OldOrder->addStatusToHistory("duplicatem2e", 'STOCK RELEASED FOR SALE - OFFLINE REFUND COMPLETE - order placed on hold as suspected m2e duplicate', false);
		$OldOrder->save();
    }
	catch(exception $e){
		$OldOrder->addStatusToHistory("duplicatem2e", 'STOCK /!\NOT/!\ RELEASED FOR SALE - OFFLINE REFUND /!\NOT/!\ COMPLETE - order placed on hold as suspected m2e duplicate; '.$e->getMessage(), false);
		$OldOrder->save();
	}
	
}
$idsstr = implode(",", $ids);

if(count($ids) > 0){
	$updatesql = "UPDATE sales_flat_order SET `state` = 'closed', `status` = 'closed' WHERE entity_id IN ({$idsstr});
	UPDATE sales_flat_order_grid SET `status` = 'closed' WHERE entity_id IN ({$idsstr});";
	
	$write->query($updatesql);
}
