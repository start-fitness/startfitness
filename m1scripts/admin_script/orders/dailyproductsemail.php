<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
require_once dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
umask(0);
Mage::app();
$date = new DateTime();
$date->add(DateInterval::createFromDateString('yesterday'));

$datetd = new DateTime();
$datetd->add(DateInterval::createFromDateString('yesterday'));
//echo $date->format('F j, Y') . "\n";
// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


$sql ="
SELECT pe.entity_id, name.value as name, actual_model.value as model, pe.sku, pe.type_id, visibility.value as visibility
FROM `catalog_product_entity` pe 
JOIN catalog_product_entity_varchar name ON name.attribute_id = 71 AND name.store_id = 0 AND name.entity_id = pe.entity_id 
JOIN catalog_product_entity_varchar actual_model ON actual_model.attribute_id = 134 AND actual_model.store_id = 0 AND actual_model.entity_id = pe.entity_id 
JOIN catalog_product_entity_int visibility ON visibility.attribute_id = 102 AND visibility.store_id = 0 AND visibility.entity_id = pe.entity_id 
WHERE DATE(pe.created_at) =DATE('".$datetd->format("Y-m-d 09:00:00")."') 
";
//echo $sql;exit;
$products = $read->fetchAll($sql);

$sqllogs = "SELECT * FROM `enterprise_logging_event`
WHERE event_code = 'catalog_products' AND date(time) = date('".$datetd->format("Y-m-d 09:00:00")."')
order by time ASC";
$logs = $read->fetchAll($sqllogs);




$getusercount = array();
foreach($products as $product){
	if($product['visibility'] == 4) {
		
		$getusercount[$product['entity_id']] = array();
	}	
}
//var_dump($getusercount);
$usercount = array();
$producttouser = array();
foreach($logs as $log){
	$data = (unserialize($log['info']));
	$ids = explode(", ", $data['general']);
	//var_dump($ids);
	foreach($ids as $id){
		if(isset($getusercount[$id])){
			if(!isset($usercount[$log['user']])) $usercount[$log['user']] = 0;
			if(!isset($producttouser[$id])) $producttouser[$id] = 0;
			$usercount[$log['user']]++;
			$producttouser[$id] = $log['user'];
			unset($getusercount[$id]);
		}
	}
}
//var_dump($producttouser);
//exit;
$totalnewproducts = count($products);
$totalvisible = 0;
$totalnonvisisble = 0;
$getusercount = array();
$list = "";
foreach($products as $product){
	if($product['visibility'] == 4) {
		$totalvisible++;
		$list .= "<tr><td>".$product['entity_id']."</td><td>".$product['model']."</td><td>".$product['name']."</td><td>".$product['sku']."</td><td>".$product['type_id']."</td><td>".$producttouser[$product['entity_id']]."</td></tr>\n";
		$getusercount[$product['entity_id']] = array();
	}
	else{
		$totalnonvisisble++;
		
	}
	
}

$usercountstr = "<table>";
foreach ($usercount as $user=>$count){
	$usercountstr .="<tr>
	<td>{$user}</td><td>{$count}</td>
	</tr>
	";
}
$usercountstr.="</table>";
//var_dump($usercountstr);exit;


$mail = new PHPMailer;
$email ="geoffrey@startfitness.co.uk";
/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';     */                       // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
$mail->addAddress("richard@startfitness.co.uk");  // Add a recipient
$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
$mail->addAddress($email);  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Daily product creation report';
$mail->Body    = "
There has been {$totalvisible} products created on the ".$datetd->format("d/m/Y")." ({$totalnonvisisble} product sizes, {$totalnewproducts} overall added )<br />
<br />
User breakdown (only visible products):
<br />
{$usercountstr}<br />
See a full list of products below:<br />
<br />
<table>
<tr>
	<th>entity id</th><th>model</th><th>name</th><th>sku</th><th>type</th><th>user</th>
</tr>
{$list}
</table>
";

//exit;
if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';
