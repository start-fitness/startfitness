<?php
//$timezop = date_default_timezone_set ( "Europe/London" );
//$script_tz = date_default_timezone_get();
ini_set('date.timezone', 'Europe/London');
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4096M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
$timetocheck = date("Y-m-d H:i:s", strtotime("-24 hours"));



	
$sql = "SELECT sfo.`created_at`, sfo.`entity_id`, sfo.`increment_id`, sfo.`base_total_invoiced`, sfo.`customer_firstname`, sfo.`customer_lastname`, sfo.`state`,sfo.`status`, m2eo.`magento_order_id` FROM `sales_flat_order` sfo
LEFT JOIN `m2epro_order` m2eo ON m2eo.`magento_order_id`=sfo.`entity_id`
WHERE sfo.`store_id` IN (24,25,26) AND sfo.`created_at` <= '{$timetocheck}' AND m2eo.`magento_order_id` IS NULL AND sfo.`coupon_code` IS NULL AND sfo.status = 'duplicatem2e' AND state <> 'closed'
ORDER BY sfo.`entity_id`";

$oorders = $read->fetchAll($sql);


$ids = array();
foreach($oorders as $order){
	echo "\n".$order['increment_id']."\t - \t {$order['state']}/{$order['status']}  -- {$order['created_at']} {$order['customer_firstname']} {$order['customer_lastname']}\n";
	$ids[] = $order['entity_id'];
	
	try{
		$OldOrder = Mage::getModel('sales/order')->load($order['entity_id']);
	    $invoices = array();
		foreach ($OldOrder->getInvoiceCollection() as $invoice) {
			    $invoices[] = $invoice;
		}
	  	$service = Mage::getModel('sales/service_order', $OldOrder);
	    foreach ($invoices as $invoice) {
		    $creditmemo = $service->prepareInvoiceCreditmemo($invoice);
			$creditmemo->setPaymentRefundDisallowed(true)->register();
		    /*$creditmemo->register();*/ //$creditmemo->refund();
		    
            $creditmemo->save();
		}
	    
    }
	catch(exception $e){
		var_dump($e->getTraceAsString());
	}
	$updatesql = "UPDATE sales_flat_order SET `state` = 'closed' WHERE entity_id = ".$order['entity_id'].";";
	
	//$write->query($updatesql);
	//exit;
}

