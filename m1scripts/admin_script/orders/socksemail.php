<?php
require_once('/home/sfadmin/public_html/app/Mage.php'); //Path to Magento Live
require '/home/sfadmin/public_html/script/phpmailer/class.phpmailer.php';
umask(0);
Mage::app();
$date = new DateTime();
$date->add(DateInterval::createFromDateString('yesterday'));

$datetd = new DateTime();
//echo $date->format('F j, Y') . "\n";
// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

$sql = "SELECT count(*) total, flat.sku, item.name, flat.gender_value FROM `sales_flat_order_item` item JOIN catalog_product_flat_6 flat ON flat.entity_id = item.product_id WHERE item.`sku` IN ('MM2342','MM2341','MM2472','5055604343850','5055604343867','5055604349487','5055604349494','5055604349500','5055604349517','YFORX-MM2472','5055604343874','5055604343881','5055604346172') AND 
item.created_at BETWEEN '".$date->format("Y-m-d 09:00:00")."' AND '".$datetd->format("Y-m-d 09:00:00")."' AND item.discount_amount > 0 AND applied_rule_ids = 4261 group by flat.sku";


$sql ="
SELECT SUM(qty_ordered) total, flat.sku, item.name, flat.gender_value, link.*
FROM `sales_flat_order_item` item 
LEFT JOIN `catalog_product_super_link` link ON link.`product_id` = item.product_id
JOIN catalog_product_flat_6 flat ON flat.entity_id = IF(link.parent_id IS NULL, item.product_id, link.parent_id) 
WHERE item.`sku` IN ('MM2342','MM2341','MM2472','5055604343850','5055604343867','5055604349487','5055604349494','5055604349500','5055604349517','YFORX-MM2472','5055604343874','5055604343881','5055604346172') AND
item.created_at BETWEEN '".$date->format("Y-m-d 09:00:00")."' AND '".$datetd->format("Y-m-d 09:00:00")."' AND item.discount_amount > 0 AND applied_rule_ids = 4261 group by flat.sku
";
//echo $sql;exit;
$products = $read->fetchAll($sql);

$promostring = "<p>Break down of socks offer from ".$date->format("d/m/Y 09:00")." to ".$datetd->format("d/m/Y 09:00")."</p>\n<ul>";

foreach($products as $product){
	$promostring .= "<li>{$product['total']} x {$product['name']} ({$product['sku']}) - {$product['gender_value']}<br /></li>\n";
}
$promostring .= "</ul>";

$mail = new PHPMailer;
$email ="richard@dimasoft.co.uk";
/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';     */                       // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
$mail->addAddress($email);  // Add a recipient
$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
//$mail->addAddress("speedking34@gmail.com");  // Add a recipient

$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Sock promotion report';
$mail->Body    = $promostring;

//exit;
if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';
