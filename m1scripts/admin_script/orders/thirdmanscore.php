<?php

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');
//$timetocheck = date("Y-m-d H:i:s", strtotime("-7 days"));
//$timenottocheck = date("Y-m-d H:i:s", strtotime("-15 minutes"));

$timestamp1 = strtotime("-7 days");
$dt = new DateTime("@$timestamp1", new DateTimeZone('UTC')); 
//echo date_format($dt, 'Y-m-d H:i:s T')."\n";
$dt->setTimezone(new DateTimeZone('Europe/London'));
$timetocheck =  date_format($dt, 'Y-m-d H:i:s');

$timestamp2 = strtotime("-15 minutes");
$dt = new DateTime("@$timestamp2", new DateTimeZone('UTC')); 
echo date_format($dt, 'Y-m-d H:i:s T')."\n";
$dt->setTimezone(new DateTimeZone('Europe/London'));
$timenottocheck =  date_format($dt, 'Y-m-d H:i:s');

// Gathering orders
$fraudTblName = Mage::getSingleton('core/resource')->getTableName('sagepayreporting_fraud');
		$transactions = Mage::getResourceModel('sagepaysuite2/sagepaysuite_transaction_collection');
		$transactions->addFieldToSelect(array('order_id', 'vendor_tx_code', 'vps_tx_id', 'trndate'));

		$transactions
		->getSelect()
		->where("`main_table`.`order_id` IS NOT NULL AND (`main_table`.`order_id` NOT IN (SELECT `order_id` FROM ". $fraudTblName .")) AND `main_table`.trndate >= '$timetocheck' AND `main_table`.trndate <= '$timenottocheck'");
		
		//echo $transactions->getSelect()."\n";exit;

// Looping through all orders
foreach ($transactions as $_trn) {

		echo "doing ".$_trn->getData('order_id')." ".$_trn->getData('vendor_tx_code')." ".$_trn->getData('trndate')."\n";
		$orderId = $_trn->getData('order_id');
		$_order = Mage::getModel('sales/order')->load($orderId);
		
		Mage::unregister('reporting_store_id');
		Mage::register('reporting_store_id', $_order->getStoreId());

        if(is_object($_order->getSagepayInfo())) {
		    $rs = getFraud()->getTransactionDetails($_order->getSagepayInfo()->getVendorTxCode());
			
			//print_r($rs);exit;
        
			try{
				getPersistentFraud()->updateThirdMan( $rs, $orderId);
				
			}catch(Exception $e){
				echo "Could not do order $orderId... \n\n".var_dump($e);	
			}
			echo "order $orderId Third man updated\n\n";
			//exit;
		}
		// Querying Sagepay Admin API
	/*$update = $_trn->updateFromApi();
	
	if (!$update->getFraud()) {
		var_dump($update->getData());exit;
		echo "Could not do ... \n\n";	
		continue;
		
	}
	
	try {
	
		$rs             = $update->getFraud();
		$noresult       = ((string)$rs->getThirdmanAction() == 'NORESULT');
		$orderPusOneDay = strtotime("+1 day", strtotime($_trn->getCreatedAt()));
	
		if(!$noresult || ($now > $orderPusOneDay)) {
	
			/**
			 *  Automatic fulfill
			 */
			 /*
			$canAuthorise = ($_trn->getTxType() == 'AUTHENTICATE' && !$_trn->getAuthorised());
			$canRelease   = ($_trn->getTxType() == 'DEFERRED' && !$_trn->getReleased());
			$rank         = ( _getCanRank() && (_getRank() <= (int)$rs->getThirdmanScore()) );
	
			if(($canAuthorise || $canRelease) && $rank){
				Mage::getModel('sagepaysuite/api_payment')->invoiceOrder($_trn->getOrderId(), Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
			}
			
			echo "order ".$_trn->getData('order_id')." Third man updated\n\n";
			/**
			 *  Automatic fulfill
			 *//*
	
		}
	
	} catch (Exception $e) {
		echo "order could not be updated due to ".$e->getMessage()."\n\n\n";
		Sage_Log::logException($e);
			  *
			  *//*
		
	}*/
		
		
		
		
}

function _getCanRank()
{
	return Mage::getStoreConfigFlag('payment/sagepaysuite/auto_fulfill_low_risk_trn');
}

function _getRank()
{
	return (int)Mage::getStoreConfig('payment/sagepaysuite/auto_fulfill_low_risk_trn_value');
}
function getPersistentFraud()
{
	return Mage::getModel('sagepayreporting/fraud');
}

function getFraud()
{
	return Mage::getModel('sagepayreporting/sagepayreporting');
}
