<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$datei = "2016-10-01 00:00:00";
$dateo = "2016-10-30 23:59:59";

//$collection = Mage::getModel('reportsalesorders/reportsalesorders')->getCollection();
//$collection->setDateRange($datei, $dateo);
$collection = $read->fetchAll(getRows($datei, $dateo, "GBP"));

//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
//$collectioneur->setDateRange($datei, $dateo);
$collectioneur = $read->fetchAll(getRows($datei, $dateo, "EUR"));

//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
//$collectionrow->setDateRange($datei, $dateo);
$collectionrow = $read->fetchAll(getRows($datei, $dateo, "USD"));

$SHIPPING = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
$UKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$EURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$ROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$EBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));




foreach($collection as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$SHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$EBYINT['total_zero_vat'] += $line["total_zerorated"];
		$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$SHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$UKINT['total_zero_vat'] += $line["total_zerorated"];
		$UKINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		$UKINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$UKINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$UKINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
unset($collection);

foreach($collectioneur as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$SHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$EBYINT['total_zero_vat'] += $line["total_zerorated"];
		$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$SHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$EURINT['total_zero_vat'] += $line["total_zerorated"];
		$EURINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		$EURINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$EURINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
unset($collectioneur);
foreach($collectionrow as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$SHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$EBYINT['total_zero_vat'] += $line["total_zerorated"];
		$EBYINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		$EBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$EBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$EBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$SHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$SHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$ROWINT['total_zero_vat'] += $line["total_zerorated"];
		$ROWINT['total_exc_vat'] += $line["base_subtotal_invoiced"] - $line['total_zerorated'];
		//var_dump($line['increment_id'], $line["base_subtotal_invoiced"] - $line['total_zerorated']);
		$ROWINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$ROWINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$ROWINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
foreach($collectionrow as $line){
	if($line['tax_after_discount_noshipping'] > 0){
		//var_dump($line);exit;
	}
	if(($line['base_subtotal_invoiced'] - $line['total_zerorated']) < 0){
		//var_dump($line);exit;
	}
}

unset($collectionrow);


//$rcollection = Mage::getModel('reportcreditmemos/reportcreditmemos')->getCollection();
//$rcollection->setDateRange($datei, $dateo);
//var_dump($rcollection->getSelect()->__toString());exit;
$rcollection = $read->fetchAll(getRowsReturn($datei, $dateo, "GBP"));

//$collectioneur = Mage::getModel('reportsalesorderseuro/reportsalesorderseuro')->getCollection();
//$collectioneur->setDateRange($datei, $dateo);
$rcollectioneur = $read->fetchAll(getRowsReturn($datei, $dateo, "EUR"));

//$collectionrow = Mage::getModel('reportsalesordersdollars/reportsalesordersdollars')->getCollection();
//$collectionrow->setDateRange($datei, $dateo);
$rcollectionrow = $read->fetchAll(getRowsReturn($datei, $dateo, "USD"));



$rSHIPPING = array("eby"=>array('total_exc_vat'=>0, 'total_vat'=>0), "int"=>array('total_exc_vat'=>0, 'total_vat'=>0));
$rUKINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$rEURINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$rROWINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));
$rEBYINT = array('total_exc_vat'=>0, 'total_zero_vat'=>0, 'total_vat'=>0, 'gift'=>array('voucher_in'=>0, 'voucher_spent'=>0));

foreach($rcollection as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$rSHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
		$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$rSHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$rUKINT['total_zero_vat'] += $line["total_zerorated"];
		$rUKINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rUKINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$rUKINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rUKINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
unset($rcollection);
foreach($rcollectioneur as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$rSHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
		$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$rSHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$rEURINT['total_zero_vat'] += $line["total_zerorated"];
		$rEURINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rEURINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rEURINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
unset($rcollectioneur);
foreach($rcollectionrow as $line){
	
	$line['base_subtotal_invoiced'] += $line['base_discount_invoiced'];
	if($line["store_id"]== "25"){
		$rSHIPPING['eby']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['eby']['total_vat'] += $line["base_shipping_tax_amount"];
		$rEBYINT['total_zero_vat'] += $line["total_zerorated"];
		$rEBYINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rEBYINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$rEBYINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rEBYINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	elseif($line["store_id"]== "6" || $line["store_id"]== "19" || $line["store_id"]== "8"){
		$rSHIPPING['int']['total_exc_vat'] += $line["base_shipping_amount"];
		$rSHIPPING['int']['total_vat'] += $line["base_shipping_tax_amount"];
		$rROWINT['total_zero_vat'] += $line["total_zerorated"];
		$rROWINT['total_exc_vat'] += $line["base_subtotal"] - $line['total_zerorated'];
		$rROWINT['total_vat'] += $line["tax_after_discount_noshipping"];
		$rROWINT['gift']['voucher_in'] += $line["giftcard_invoice"];
		$rROWINT['gift']['voucher_spent'] += $line["giftcard_bought"];
	}
	
}
unset($rcollectionrow);
//var_dump($rEBYINT);exit;

$xmlarr = array(
"ITEM_01" => array("NAME"=>"UK - INT", "TAXCODE" => "1", "NOMINAL" => "4000-UK-INT", "TOTAL"=>$UKINT['total_exc_vat'] - $rUKINT['total_exc_vat'], "TAX"=>$UKINT['total_vat'] - $rUKINT['total_vat'],
"TOTAL_WOREFUNDS" => $UKINT['total_exc_vat'], "TAX_WOREFUNDS" => $UKINT['total_vat']),
"ITEM_02" => array("NAME"=>"UK - INT ZERO RATED",  "TAXCODE" => "2", "NOMINAL" => "4000-UK-INT", "TOTAL"=>$UKINT['total_zero_vat'] - $rUKINT['total_zero_vat'] - ($UKINT['voucher_spent'] - $rUKINT['gift']['voucher_spent']), "TAX"=>0,
"TOTAL_WOREFUNDS" => $UKINT['total_zero_vat'] - $UKINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
"ITEM_03" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD BOUGHT",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$UKINT['gift']['voucher_spent'] - $rUKINT['gift']['voucher_spent'], "TAX"=>0,
"TOTAL_WOREFUNDS" => $UKINT['gift']['voucher_spent'], "TAX_WOREFUNDS" => 0),
"ITEM_04" => array("NAME"=>"UK - INT ZERO RATED GIFTCARD PAID",  "TAXCODE" => "0", "NOMINAL" => "2130--", "TOTAL"=>$UKINT['gift']['voucher_in'] - $rUKINT['gift']['voucher_in'], "TAX"=>0,
"TOTAL_WOREFUNDS" => $UKINT['gift']['voucher_in'], "TAX_WOREFUNDS" => 0),
/*);
$xmlarreur = array(*/
"ITEM_05" => array("NAME"=>"EUR - INT", "TAXCODE" => "1", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>$EURINT['total_exc_vat'] - $rEURINT['total_exc_vat'], "TAX"=>$EURINT['total_vat'] - $rEURINT['total_vat'],
"TOTAL_WOREFUNDS" => $EURINT['total_exc_vat'], "TAX_WOREFUNDS" => $EURINT['total_vat']),
"ITEM_06" => array("NAME"=>"EUR - INT ZERO RATED",   "TAXCODE" => "2", "NOMINAL" => "4000-EUR-INT", "TOTAL"=>$EURINT['total_zero_vat'] - $rEURINT['total_zero_vat'], "TAX"=>0,
"TOTAL_WOREFUNDS" => $EURINT['total_zero_vat'], "TAX_WOREFUNDS" => 0),
/*);
$xmlarrrow = array(*/
"ITEM_07" => array("NAME"=>"ROW - INT", "TAXCODE" => "1", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>$ROWINT['total_exc_vat'] - $rROWINT['total_exc_vat'], "TAX"=>$ROWINT['total_vat'] - $rROWINT['total_vat'],
"TOTAL_WOREFUNDS" => $ROWINT['total_exc_vat'], "TAX_WOREFUNDS" => $ROWINT['total_vat']),
"ITEM_08" => array("NAME"=>"ROW - INT ZERO RATED",  "TAXCODE" => "0", "NOMINAL" => "4000-ROW-INT", "TOTAL"=>$ROWINT['total_zero_vat'] - $rROWINT['total_zero_vat'], "TAX"=>0,
"TOTAL_WOREFUNDS" => $ROWINT['total_zero_vat'], "TAX_WOREFUNDS" => 0),
/*);

$xmlarrgen = array(*/

/*);
$xmlarrpost = array(*/
"ITEM_12" => array("NAME"=>"Postage - INT", "TAXCODE" => "1", "NOMINAL" => "4040--INT", "TOTAL"=>$SHIPPING['int']['total_exc_vat'] - $rSHIPPING['int']['total_exc_vat'], "TAX"=>$SHIPPING['int']['total_vat'] - $rSHIPPING['int']['total_vat'],
"TOTAL_WOREFUNDS" => $SHIPPING['int']['total_exc_vat'], "TAX_WOREFUNDS" => $SHIPPING['int']['total_vat']),
);

$xmlarrebay = array(
"ITEM_09" => array("NAME"=>"GEN - EBY", "TAXCODE" => "1", "NOMINAL" => "4000-GEN-EBY", "TOTAL"=>$EBYINT['total_exc_vat'] - $rEBYINT['total_exc_vat'], "TAX"=>$EBYINT['total_vat'] - $rEBYINT['total_vat'],
"TOTAL_WOREFUNDS" => $EBYINT['total_exc_vat'], "TAX_WOREFUNDS" => $EBYINT['total_vat']),
"ITEM_10" => array("NAME"=>"GEN - INT ZERO RATED", "NOMINAL" => "4000-GEN-INT", "TOTAL"=>$EBYINT['total_zero_vat'] - $rEBYINT['total_zero_vat'], "TAX"=>0,
"TOTAL_WOREFUNDS" => $EBYINT['total_zero_vat'],  "TAXCODE" => "2", "TAX_WOREFUNDS" => 0),
"ITEM_11" => array("NAME"=>"Postage - EBY", "TAXCODE" => "1", "NOMINAL" => "4040--EBY", "TOTAL"=>$SHIPPING['eby']['total_exc_vat'] - $rSHIPPING['eby']['total_exc_vat'], "TAX"=>$SHIPPING['eby']['total_vat'] - $rSHIPPING['eby']['total_vat'],
"TOTAL_WOREFUNDS" => $SHIPPING['eby']['total_exc_vat'] , "TAX_WOREFUNDS" => $SHIPPING['eby']['total_vat']),

);
$nominals = array();

foreach($xmlarr as $temp)
	$nominals[$temp['NOMINAL']]= $temp['NOMINAL'];
foreach($xmlarrebay as $temp)
	$nominals[$temp['NOMINAL']]= $temp['NOMINAL'];
//var_dump($nominals);exit;
//$i = 1;
foreach($xmlarr as $index => $item){
	$iname = str_pad($i, 2, '0', STR_PAD_LEFT);
	$xml = array_to_orderarray($item, "INTFIT", "$iname-{$item['NOMINAL']}".date("Ymd"));
	if($xml == null) continue;
	
	$dir = dirname(__FILE__)."/exportsdm/";
	if(!is_dir($dir)) mkdir($dir);
	$dom = dom_import_simplexml($xml)->ownerDocument;
	$dom->formatOutput = true;
	//header("Content-type: text/xml");
	file_put_contents($dir."$iname-{$item['NOMINAL']}".date("Ymd").".xml", $dom->saveXML());
	$i++;
}
foreach($xmlarrebay as $index => $item){
	$iname = str_pad($i, 2, '0', STR_PAD_LEFT);
	$xml = array_to_orderarray($item, "EBAYFIT", "$iname-{$item['NOMINAL']}".date("Ymd"));
	if($xml == null) continue;

	$dir = dirname(__FILE__)."/exportsdm/";
	if(!is_dir($dir)) mkdir($dir);
	$dom = dom_import_simplexml($xml)->ownerDocument;
	$dom->formatOutput = true;
	//header("Content-type: text/xml");
	file_put_contents($dir."$iname-{$item['NOMINAL']}".date("Ymd").".xml", $dom->saveXML());
	$i++;
}



//$xml = new SimpleXMLElement('<root/>');
//array_to_xml($xmlarr, $xml);




function array_to_orderarray($item, $cust, $name)
{
	$xml = "";
		//var_dump($data);
		
		$orderdata = array();
		$orderdata['ORDER_TYPE'] = 1;
		$fullpay = true;
		
		$orderdata['ORDER_NUMBER'] = $name;
		//$orderdata['WAREHOUSE'] = "ASHINGTON";
		
		$orderdata['SET_ACKNOWLEDGED_FLAG'] = "YES";
		
		$orderdata['CUST_ACC_REF'] = $cust;
		
		
		//$orderdata['CUST_ACC_REF'] = str_pad($data->CUSTOMER->CUSTOMER_ID, 8, "0", STR_PAD_LEFT);
		$orderdata['ORDER_DATE'] = date("Y/m/d");
		$orderdata['ORDER_REQUESTED_DATE'] = date("Y/m/d");
		$orderdata['ORDER_PROMISED_DATE'] = date("Y/m/d");
		$orderdata['CUSTOMER_ORDER_NUMBER'] = $name;
		
		$orderdata['INV_POSTAL_NAME'] = $cust;
		$orderdata['INV_ADDRESS_LINE_1'] = "";
		$orderdata['INV_ADDRESS_LINE_2'] = "";
		$orderdata['INV_ADDRESS_LINE_3'] = "";
		$orderdata['INV_ADDRESS_LINE_4'] = "";
		$orderdata['INV_POST_CODE'] = "";
		$orderdata['INV_CITY'] = "";
		$orderdata['INV_COUNTY'] = "";
		$orderdata['INV_CONTACT'] = "";
		$orderdata['INV_TELEPHONE'] = "";
		$orderdata['INV_FAX'] = "";//$billingAddress->getData('');
		$orderdata['INV_EMAIL'] = "";
		$orderdata['INV_TAX_NUMBER'] = "";
		$orderdata['INV_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['INV_COUNTRY_CODE'] = "";
		$orderdata['INV_COUNTRY'] = "";
		
		$orderdata['DEL_POSTAL_NAME'] = "";
		$orderdata['DEL_ADDRESS_LINE_1'] = "";
		$orderdata['DEL_ADDRESS_LINE_2'] = "";
		$orderdata['DEL_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
		$orderdata['DEL_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
		$orderdata['DEL_POST_CODE'] = "";
		$orderdata['DEL_CITY'] = "";
		$orderdata['DEL_COUNTY'] = "";
		$orderdata['DEL_CONTACT'] = "";
		$orderdata['DEL_TELEPHONE'] = "";
		$orderdata['DEL_FAX'] = "";//$billingAddress->getData('');
		$orderdata['DEL_EMAIL'] = "";
		$orderdata['DEL_TAX_NUMBER'] = "";
		$orderdata['DEL_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['DEL_COUNTRY_CODE'] ="";
		$orderdata['DEL_COUNTRY'] = "";
		
		
		$orderdata['PAYMENT_IN_FULL'] = "NO";
		
		
		$del_meth = "";
		
		
		//$orderdata['PAYMENT_VALUE'] = $order->getData('');
		//$orderdata['PAYMENT_REF'] = $order->getData('');
		//$orderdata['PAYMENT_METHOD'] = $order->getData('');
		
		
		
		$orderdata['EXCHANGE_RATE'] = "";
		$orderdata['ARE_PRICES_TAX_INCLUSIVE'] = "YES";
		
		
		$i = 0;
		
			if($item["TOTAL"] == 0) return null;
			$i++;
			$nominal = explode("-", $item["NOMINAL"]);
			//var_dump($item);exit;
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 2;
			$orderdata['ITEM_'.$i.'_LINE_EXC_TAX'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = implode("-", $nominal); //"S1"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = $item["NAME"];
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = 1;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $item["TOTAL"];
			$orderdata['ITEM_'.$i.'_UNITTAX'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = $item["TAX"];
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = "0";
			$orderdata['ITEM_'.$i.'_TAXCODE'] = $item["TAXCODE"];
			$orderdata['ITEM_'.$i.'_NOMINAL'] = $nominal[0];
			$orderdata['ITEM_'.$i.'_CC'] = $nominal[1];
			$orderdata['ITEM_'.$i.'_DEPT'] = $nominal[2];
			
		
			
		
		

		$orderdata['DISCOUNT_EX'] = 0;
		$orderdata['DISCOUNT_INC'] = 0;
		$orderdata['DISCOUNT_TAX'] = 0;
		$orderdata['ITEMS_COUNT'] = $i;
		$orderdata['DO_INVOICE'] = "0";
		$orderdata['DO_SHIPPED'] = "0";
		$orderdata['DO_POSTED'] = "0";
		$orderdata['TAKEN_BY'] = "DM SCRIPT";
		
		//var_dump($orderdata);
		
		$xml = array("data"=> $orderdata);
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><form></form>');
		arrayToXml($xml,$xml_data);
		
		
		return $xml_data;
}


function arrayToXml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            arrayToXml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function array_to_xml( $data, &$xml_data ) {
    foreach( $data as $key => $value ) {
    	if(strpos($key, "ITEM_")!== false) $key = "ITEM";
        if( is_array($value) ) {
            if( is_numeric($key) ){
                $key = 'item'.$key; //dealing with <0/>..<n/> issues
            }
            $subnode = $xml_data->addChild($key);
            array_to_xml($value, $subnode);
        } else {
            $xml_data->addChild("$key",htmlspecialchars("$value"));
        }
     }
}

function getRows($datein, $dateout, $currency = "GBP"){
	$sql = "SELECT `main_table`.*,
       Concat(Coalesce(a.firstname, ''), ' ', Coalesce(a.lastname, '')) AS
       `shipto_name`,
       `a`.`city`                                                       AS
       `shipto_city`,
       `a`.`region`                                                     AS
       `shipto_state`,
       `a`.`country_id`,
       `a`.`country_id`                                                 AS
       `region_code`,
       `fix`.`tax_after_discount`,
       `fix`.`tax_after_discount_noshipping`,
       `payments`.`method`                                              AS
       `payment_method`,
       Concat(Coalesce(b.firstname, ''), ' ', Coalesce(b.lastname, '')) AS
       `billto_name`,
       `main_table`.`increment_id`                                      AS
       `order_increment_id`,
       `main_table`.`store_name`,
       `main_table`.`created_at`                                        AS
       `purchased_on`,
       `main_table`.`billing_address_id`,
       `main_table`.`shipping_address_id`,
       `main_table`.`subtotal`,
       ( main_table.subtotal
         + main_table.discount_amount )                                 AS
       `goods_net`,
       `main_table`.`discount_amount`,
       `main_table`.`tax_amount`,
       `main_table`.`shipping_amount`,
       `main_table`.`grand_total`,
       `main_table`.`status`,
       `main_table`.`shipping_description`,
       `main_table`.`shipping_tax_amount`,
       `main_table`.`subtotal_incl_tax`,
       ( main_table.subtotal_incl_tax - main_table.subtotal )           AS
       `item_tax`,
       ( IF(main_table.gift_cards_invoiced IS NULL, 0.00,
         main_table.gift_cards_invoiced)
         + IF(main_table.customer_balance_invoiced IS NULL, 0.00,
         main_table.customer_balance_invoiced) )                        AS
       `giftcard_invoice`,
       (SELECT IF(Sum(row_invoiced) IS NULL, 0.00, Sum(row_invoiced))
        FROM   sales_flat_order_item sfoi
        WHERE  product_type = \"giftcard\"
               AND sfoi.order_id = main_table.entity_id)                AS
       `giftcard_bought`,

	    SUM(IF(items.base_tax_invoiced = 0, (items.base_row_invoiced - items.base_discount_invoiced), 0)) AS total_zerorated
FROM   `sales_flat_order` AS `main_table`
       INNER JOIN `sales_flat_order_address` AS `a`
       
               ON main_table.shipping_address_id = a.entity_id
       JOIN `sales_flat_order_item` items ON items.order_id = main_table.entity_id
       LEFT JOIN `dimasoft_order_vat_corrected` AS `fix`
              ON fix.order_id = main_table.entity_id
       LEFT JOIN `sales_flat_order_payment` AS `payments`
              ON payments.parent_id = main_table.entity_id
       INNER JOIN `sales_flat_order_address` AS `b`
               ON main_table.billing_address_id = b.entity_id
WHERE  ( payments.method <> \"c3v12finance\" )
       AND ( main_table.created_at BETWEEN
             '{$datein}' AND '{$dateout}'
           )
       AND ( main_table.customer_id NOT IN ( 94599, 379555, 55337, 55619,
                                             460385, 421839, 277970, 92030,
                                             54760, 54187 )
              OR main_table.customer_id IS NULL )
       AND ( main_table.order_currency_code = '{$currency}' ) 
       
       GROUP BY main_table.entity_id
	";
	return $sql;
	
}

function getRowsReturn($datein, $dateout, $currency = "GBP"){
	$sql = "
	SELECT `main_table`.*, `a`.`store_name`, `a`.`created_at` AS `purchased_on`, (`tax_after_discount` * -1) AS `tax_after_discount`, (`tax_after_discount_noshipping`) AS `tax_after_discount_noshipping`, CONCAT(COALESCE(b.firstname, ''), ' ', COALESCE(b.lastname, '')) AS `shipto_name`, `b`.`city` AS `shipto_city`, `b`.`region` AS `shipto_state`, `b`.`country_id`, `b`.`country_id` AS `region_code`, CONCAT(COALESCE(c.firstname, ''), ' ', COALESCE(c.lastname, '')) AS `billto_name`, `main_table`.`increment_id` AS `order_increment_id`, `main_table`.`created_at` AS `purchased_on`, `main_table`.`billing_address_id`, `main_table`.`shipping_address_id`, `main_table`.`subtotal`, (main_table.subtotal - IF(main_table.discount_amount < 0 , ABS(main_table.discount_amount), main_table.discount_amount)) AS `goods_net`, IF(main_table.discount_amount < 0 , ABS(main_table.discount_amount), main_table.discount_amount) AS `discount_amount`, `main_table`.`tax_amount`, `main_table`.`shipping_amount`, `main_table`.`grand_total`, `main_table`.`shipping_tax_amount`, `main_table`.`subtotal_incl_tax`, (main_table.subtotal_incl_tax - main_table.subtotal) AS `item_tax`, (IF(main_table.customer_bal_total_refunded IS NULL, 0.00, main_table.customer_bal_total_refunded) ) AS `giftcard_invoice`, (SELECT IF(SUM(row_invoiced) IS NULL, 0.00,SUM(row_invoiced) ) FROM `sales_flat_creditmemo_item` ci JOIN sales_flat_order_item sfoi ON sfoi.item_id = ci.parent_id WHERE sfoi.product_type = \"giftcard\" AND sfoi.order_id = main_table.order_id) AS `giftcard_bought`, SUM(IF(items.base_tax_amount = 0, items.base_row_total, 0)) AS total_zerorated FROM `sales_flat_creditmemo` AS `main_table`
 INNER JOIN `sales_flat_order` AS `a` ON main_table.order_id = a.entity_id
 JOIN `sales_flat_creditmemo_item` items ON items.parent_id = main_table.entity_id
 LEFT JOIN `dimasoft_credit_vat_corrected` AS `fix` ON fix.credit_id = main_table.entity_id
 INNER JOIN `sales_flat_order_address` AS `b` ON main_table.shipping_address_id = b.entity_id
 INNER JOIN `sales_flat_order_address` AS `c` ON main_table.billing_address_id = c.entity_id WHERE (main_table.created_at BETWEEN '{$datein}' AND '{$dateout}') AND (main_table.grand_total >= 0) AND (main_table.order_currency_code = '{$currency}')
	GROUP BY main_table.entity_id";
	return $sql;
}
