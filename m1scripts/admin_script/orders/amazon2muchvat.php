<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '4048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$datei = "2016-01-01 00:00:00";
$dateo = "2019-10-30 23:59:59";

$headers = "ord.entity_id, ord.increment_id, ord.created_at, ord.base_grand_total, ord.grand_total, ord.base_to_order_rate, pay.method";

$sql = "
SELECT order_id, group_concat(sales_flat_order_item.item_id) as items  FROM `sales_flat_order_item` 
JOIN catalog_product_entity_int tax on tax.attribute_id = 122 AND tax.store_id = 0 AND tax.entity_id = sales_flat_order_item.product_id
WHERE sales_flat_order_item.`store_id` = 24 AND sales_flat_order_item.`created_at` >= '2018-01-01 00:00:00' AND base_price > 0 AND base_tax_amount > 0 AND tax.value = 5
GROUP BY order_id
";
$res = $read->fetchAll($sql);
$ordids = [];
$f = fopen(dirname(__FILE__)."/reconcile/amazon2muchvat-".date("Y-m-d H-i")."-orders.csv", "w+");fputcsv($f, ["order_id", "items"]);
foreach($res as $row) { $ordids[] = $row; fputcsv($f, $row); }
fclose($f);

foreach($ordids as $row){
	//continue;
	//var_dump($row);
	$base_tax = 0;
	$tax = 0;
	$sqlitems = "SELECT * FROM sales_flat_order_item WHERE sales_flat_order_item.item_id IN (".$row['items'].")";
	$resitems = $read->fetchAll($sqlitems);
	foreach($resitems as $rowi){
		$base_tax += $rowi['base_tax_amount'];
		$tax += $rowi['tax_amount'];
		
		$sqlupdate = "UPDATE sales_flat_order_item
		SET 
		tax_percent = 0,
		base_tax_amount = 0, tax_amount = 0,
		base_tax_invoiced = 0, tax_invoiced = 0,
		price = price_incl_tax, base_price = base_price_incl_tax,
		row_total = row_total_incl_tax, base_row_total = base_row_total_incl_tax,
		row_invoiced = row_total_incl_tax, base_row_invoiced = base_row_total_incl_tax
		WHERE sales_flat_order_item.item_id = ".$rowi['item_id'];
		$read->query($sqlupdate);
	}
	$sqlupdateord = "UPDATE sales_flat_order SET
		
		base_subtotal = base_subtotal + ".$base_tax." ,
		base_subtotal_invoiced = base_subtotal_invoiced + ".$base_tax." ,
		subtotal = subtotal + ".$tax." ,
		subtotal_invoiced = subtotal_invoiced + ".$tax." ,
		base_tax_amount = base_tax_amount - ".$base_tax." ,
		base_tax_invoiced = base_tax_invoiced - ".$base_tax." ,
		tax_amount = tax_amount - ".$tax." ,
		tax_invoiced = tax_invoiced - ".$tax." 
		WHERE sales_flat_order.entity_id = ".$row['order_id'];
	$read->query($sqlupdateord);
	//exit;
}

$sql = "
SELECT parent_id as order_id, group_concat(sales_flat_invoice_item.entity_id) as items  FROM `sales_flat_invoice_item` 
JOIN sales_flat_invoice ON sales_flat_invoice.entity_id = sales_flat_invoice_item.parent_id
JOIN catalog_product_entity_int tax on tax.attribute_id = 122 AND tax.store_id = 0 AND tax.entity_id = sales_flat_invoice_item.product_id
WHERE sales_flat_invoice.`store_id` = 25 AND sales_flat_invoice.`created_at` >= '2018-01-01 00:00:00' AND sales_flat_invoice_item.base_price > 0 AND sales_flat_invoice_item.base_tax_amount > 0 AND tax.value = 5
GROUP BY parent_id
";
$res = $read->fetchAll($sql);
$ordids = [];
$f = fopen(dirname(__FILE__)."/reconcile/amazon2muchvat-".date("Y-m-d H-i")."-invoices.csv", "w+");fputcsv($f, ["invoice_id", "items"]);
foreach($res as $row) { $ordids[] = $row; fputcsv($f, $row); }
fclose($f);

foreach($ordids as $row){
	//continue;
	//var_dump($row);
	$base_tax = 0;
	$tax = 0;
	$sqlitems = "SELECT * FROM sales_flat_invoice_item WHERE sales_flat_invoice_item.entity_id IN (".$row['items'].")";
	$resitems = $read->fetchAll($sqlitems);
	foreach($resitems as $rowi){
		$base_tax += $rowi['base_tax_amount'];
		$tax += $rowi['tax_amount'];
		
		$sqlupdate = "UPDATE sales_flat_invoice_item
		SET 
		base_tax_amount = 0, tax_amount = 0,
		price = price_incl_tax, base_price = base_price_incl_tax,
		row_total = row_total_incl_tax, base_row_total = base_row_total_incl_tax
		WHERE sales_flat_invoice_item.entity_id = ".$rowi['entity_id'];
		$read->query($sqlupdate);
	}
	$sqlupdateord = "UPDATE sales_flat_invoice SET
		
		base_subtotal = base_subtotal + ".$base_tax." ,
		subtotal = subtotal + ".$tax." ,
		base_tax_amount = base_tax_amount - ".$base_tax." ,
		tax_amount = tax_amount - ".$tax." 
		WHERE sales_flat_invoice.entity_id = ".$row['order_id'];
	$read->query($sqlupdateord);
	//exit;
}

$sql = "
SELECT parent_id as order_id, group_concat(sales_flat_creditmemo_item.entity_id) as items  FROM `sales_flat_creditmemo_item` 
JOIN sales_flat_creditmemo ON sales_flat_creditmemo.entity_id = sales_flat_creditmemo_item.parent_id
JOIN catalog_product_entity_int tax on tax.attribute_id = 122 AND tax.store_id = 0 AND tax.entity_id = sales_flat_creditmemo_item.product_id
WHERE sales_flat_creditmemo.`store_id` = 24 AND sales_flat_creditmemo.`created_at` >= '2018-01-01 00:00:00' AND sales_flat_creditmemo_item.base_price > 0 AND sales_flat_creditmemo_item.base_tax_amount > 0 AND tax.value = 5
GROUP BY parent_id
";
$res = $read->fetchAll($sql);
$ordids = [];
//var_dump("/reconcile/ebay2muchvat-".date("Y-m-d H-i")."-credit.csv");exit;
$f = fopen(dirname(__FILE__)."/reconcile/amazon2muchvat-".date("Y-m-d H-i")."-credit.csv", "w+");fputcsv($f, ["credit_id", "items"]);
foreach($res as $row) { $ordids[] = $row; fputcsv($f, $row); }
fclose($f);

foreach($ordids as $row){
	//var_dump($row);
	$base_tax = 0;
	$tax = 0;
	$sqlitems = "SELECT * FROM sales_flat_creditmemo_item WHERE sales_flat_creditmemo_item.entity_id IN (".$row['items'].")";
	$resitems = $read->fetchAll($sqlitems);
	foreach($resitems as $rowi){
		$base_tax += $rowi['base_tax_amount'];
		$tax += $rowi['tax_amount'];
		
		$sqlupdate = "UPDATE sales_flat_creditmemo_item
		SET 
		base_tax_amount = 0, tax_amount = 0,
		price = price_incl_tax, base_price = base_price_incl_tax,
		row_total = row_total_incl_tax, base_row_total = base_row_total_incl_tax
		WHERE sales_flat_creditmemo_item.entity_id = ".$rowi['entity_id'];
		$read->query($sqlupdate);
	}
	$sqlupdateord = "UPDATE sales_flat_creditmemo SET
		
		base_subtotal = base_subtotal + ".$base_tax." ,
		subtotal = subtotal + ".$tax." ,
		base_tax_amount = base_tax_amount - ".$base_tax." ,
		tax_amount = tax_amount - ".$tax." 
		WHERE sales_flat_creditmemo.entity_id = ".$row['order_id'];
	$read->query($sqlupdateord);
	//exit;
}

