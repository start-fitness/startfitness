<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$query ="
UPDATE `sales_flat_order`
JOIN (
	SELECT sales_flat_invoice.order_id, SUM(sales_flat_invoice.base_grand_total) as base_grand_total, SUM(sales_flat_invoice.grand_total) as grand_total, SUM(sales_flat_invoice.subtotal) as subtotal, SUM(sales_flat_invoice.base_subtotal) as base_subtotal  from sales_flat_invoice GROUP BY sales_flat_invoice.order_id
) inv on inv.order_id = sales_flat_order.entity_id
JOIN sales_flat_order_payment ON sales_flat_order_payment.parent_id = sales_flat_order.entity_id

SET 
sales_flat_order.base_subtotal_invoiced = inv.base_subtotal, 
sales_flat_order.subtotal_invoiced = inv.subtotal, 
sales_flat_order.total_paid =  inv.grand_total, 
sales_flat_order.base_total_paid = inv.base_grand_total, 
sales_flat_order.base_total_invoiced = inv.base_grand_total, 
sales_flat_order.total_invoiced = inv.grand_total, 
sales_flat_order.base_shipping_invoiced = sales_flat_order.base_shipping_incl_tax, 
sales_flat_order.shipping_invoiced =  sales_flat_order.shipping_incl_tax 

WHERE (total_paid = 0 || total_paid IS NULL) AND inv.base_grand_total > 0 AND state = \"processing\" AND sales_flat_order_payment.base_amount_paid > 0;
";

$write->query($query);