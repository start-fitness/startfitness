<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
require_once dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
require_once dirname(__FILE__).'/../phpmailer/class.pop3.php';
require_once dirname(__FILE__).'/../phpmailer/class.smtp.php';

umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$sql ='

SELECT sales_flat_order.store_id, count(sales_flat_order.entity_id) total_orders, AVG(sales_flat_order.base_grand_total) average_order_value, 
group_concat(sales_flat_order.increment_id) order_ids,  sales_flat_order_address.street, sales_flat_order_address.postcode, 
group_concat(sales_flat_order.customer_id) customer_ids, CONCAT(sales_flat_order_address.firstname, " ", sales_flat_order_address.lastname) customer_name 
FROM `sales_flat_order`
JOIN sales_flat_order_address ON sales_flat_order_address.parent_id = sales_flat_order.entity_id AND sales_flat_order_address.address_type = "shipping"
WHERE sales_flat_order.created_at  BETWEEN DATE_SUB(NOW(), INTERVAL 30 DAY) AND NOW() AND sales_flat_order.status != "closed"
GROUP BY sales_flat_order_address.street, sales_flat_order_address.postcode
HAVING count(sales_flat_order.entity_id) > 3 AND AVG(sales_flat_order.base_grand_total) <= 25 AND AVG(sales_flat_order.base_grand_total) > 0
ORDER BY count(sales_flat_order.entity_id) DESC

';

$results = $read->fetchAll($sql);

$amazon = array();
$ebay = array();
$other = array();
$headers = array();
foreach($results as $row){
	if(count($headers) == 0){
		foreach($row as $index => $balur){
			$headers[] = $index;
		}
	}
	$row["order_ids"] = str_replace(",", ", ", $row["order_ids"]);
	if($row['store_id']  == 25){
		$ebay[] = $row;
	}
	elseif($row['store_id']  == 24){
		$amazon[] = $row;
	}
	else{
		$other[] = $row;
	}
}
$headerhtml ="";
foreach($headers as $header){
	$headerhtml.="<th style=\"border-bottom: 1px solid #000;\"><strong>$header</strong></th>";
}
$body="<style>
".getcss()."
</style>";
$body .="<h1>Potential fraud orders over the last 30 days</h1>";
if(count($amazon) > 0){
$amazonhtml = "";
foreach($amazon as $amazonrow){
	$currrow = "";
	foreach($amazonrow as $subrow){
		$currrow.="<td>$subrow</td>";
	}
	
	$amazonhtml.="<tr>{$currrow}</tr>";
	
}
//var_dump($amazonhtml);
$body .= "
<h2>Potential fraud - AMAZON</h2>
<table style=\"width: 100%;\">
<thead>
<tr>
{$headerhtml}
</tr>
</thead>
<tbody>
{$amazonhtml}
</tbody>
</table>
";
}

if(count($ebay) > 0){
$amazonhtml = "";
foreach($ebay as $amazonrow){
	$currrow = "";
	foreach($amazonrow as $subrow){
		$currrow.="<td>$subrow</td>";
	}
	$amazonhtml.="<tr>{$currrow}</tr>";
}
$body .= "
<h2>Potential fraud - EBAY</h2>
<table style=\"width: 100%;\">

<thead>
<tr>
{$headerhtml}
</tr>
</thead>
<tbody>
{$amazonhtml}
</tbody>
</table>
";
}

if(count($other) > 0){
$amazonhtml = "";
foreach($other as $amazonrow){
	$currrow = "";
	foreach($amazonrow as $subrow){
		$currrow.="<td>$subrow</td>";
	}
	$amazonhtml.="<tr>{$currrow}</tr>";
}
$body .= "
<h2>Potential fraud - Other stores</h2>
<table style=\"width: 100%;\">

<thead>
<tr>
{$headerhtml}
</tr>
</thead>
<tbody>
{$amazonhtml}
</tbody>
</table>
";
}

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'localhost';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = false;                               // Enable SMTP authentication
    //$mail->Username = 'user@example.com';                 // SMTP username
    //$mail->Password = 'secret';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //$mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('cron@startfitness.co.uk');
	$mail->addAddress('richard@startfitness.co.uk');   
    $mail->addAddress('geoffrey@startfitness.co.uk');
    $mail->addAddress('michaelamillican@startfitness.co.uk');
	$mail->addAddress('gina@startfitness.co.uk');
    
	     // Add a recipient
                // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    //$mail->addAttachment(dirname(__FILE__)."/emails.csv");         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Potential order fraud';
    $mail->Body    = $body;
    $mail->AltBody = '';

    $mail->send();
    echo 'Message has been sent';
	//file_put_contents(dirname(__FILE__)."/lastsend.txt", $thistime);
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}


function getcss(){
	return "
	table th{
		border-bottom: 1px solid #000;
	}
	";

}

