<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');
require_once('../../app/Mage.php'); //Path to Magento
require_once('../../lib/vendors/sagepayAdminApi.class.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');



// Fetching sagepay details for all orders
$saypayinfo = array();

$result=$write->fetchAll("SELECT sagepaysuite_transaction.vendor_tx_code, orders.* FROM `sagepaysuite_transaction`
LEFT JOIN sales_flat_order orders on orders.entity_id = sagepaysuite_transaction.order_id
WHERE sagepaysuite_transaction.created_at BETWEEN '2018-11-21 00:00:00' AND '2019-01-28 23:59:9' AND sagepaysuite_transaction.card_type = \"VISA\"");


$file = fopen("transactions.csv", "w+");
$first = true;
$count = 0;
// Looping through all orders
foreach ($result as $order) {
    $data = (object)$order;
	$count++;
	// Checking to see if that order has a matching sagepay entry (no reason why it souldn't have but better check)
	
	// If we did find a matching sagepay entry, carry on
	if(true){
		
		// Querying Sagepay Admin API
		try{
			echo "doing $count of ".count($result)." \n";
			// Fetching appropriate sagepay admin logins from the backend, depending on the order's store ID
			$_configuration = Mage::app()->getStore($data->store_id)->getConfig("sagepayreporting/account");
	        $vendor = strtolower($_configuration['vendor']);
	        $username = ($_configuration['username']);
	        $password = (Mage::helper('core')->decrypt($_configuration['password']));
	        
			// Is it a "test" order?
	        $env = ($_configuration['enviroment'] == 1)?true:false;
			
			// Initialasing the API connection
			$adminapi = new SagePayAdminApi("$vendor", "$username", "$password", $env);
			
			// Looking for the transaction
			$transaction = $adminapi->getTransactionDetail(array('vendortxcode' => $data->vendor_tx_code));
			$csvline = [];
			if($first){
				$first = false;
				//var_dump(array_keys((array)$transaction));
				fputcsv($file,array_keys((array)$transaction));
			}
			foreach($transaction as $header => $cell){
				$csvline[] = (string)$cell;
			}
			fputcsv($file,($csvline));
			//var_dump($transaction);exit;
			
		}
		catch(exception $e){
			$haserror = true;
			if($e->getMessage() == "sagepayerror")
				$errors[] = "sagepay could not retrieve this order";
			else
				$errors[] = "sagepay API unreachable for fraud checks";
		}
		
		
		
	}
}
fclose($file);