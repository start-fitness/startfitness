<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$readConnection = $resource->getConnection();
error_reporting(E_ALL & ~E_NOTICE);
/**
 * Retrieve the write connection
 */
$writeConnection = $readConnection;

$fromdate = date("Y-m-d H:i:s",strtotime("- 20 days"));


$it = 30;

if(isset($argv[1])){
	$it = $argv[1];
}


	

for($ii = 1; $ii <= $it; $ii++){
	echo date("Y-m-d H:i:s",strtotime("- $ii months"))."\n";
	$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
	$orderCollection = $OrderFactory->create();
	$salesCollection = $orderCollection->addAttributeToFilter('created_at', array('from'=>  date("Y-m-d H:i:s",strtotime("- $ii days"))  ));
	//$salesCollection->addAttributeToFilter('status', array('in' => array('complete')));
	$salesCollection->getSelect()->joinLeft(array("fix"=>"dimasoft_order_vat_corrected"), "entity_id = order_id" )->where(("`fix`.order_id IS NULL"));
	
	$x = 1;
	foreach($salesCollection as $order){
		$res = getTaxAfterDiscount($order, $objectManager);
		$sql = "INSERT INTO dimasoft_order_vat_corrected (order_id, tax_after_discount, base_tax_after_discount, tax_after_discount_noshipping, base_tax_after_discount_noshipping) VALUES(
		".$order->getData("entity_id").", {$res['discount_after_tax']},{$res['base_discount_after_tax']},{$res['discount_after_tax_noshipping']},{$res['base_discount_after_tax_noshipping']}
		)";
		$writeConnection->query($sql);
		echo "$x insert !";
		$x++;
	}
}


function getTaxAfterDiscount($source, $objectManager){
	$_source = $source;
	
	//$_source = Mage::getModel('sales/order')->loadByIncrementId($_source->getData("increment_id"));
	//var_dump($_source->getData());exit;
	$zero_tax = array(0,5);
	$totals = array();
	$totals['tax_after_discount'] = 0;
	$totals['base_tax_after_discount'] = 0;
	$totals['shipping_tax'] = ($_source->getShippingInclTax() - $_source->getShippingAmount());
	$totals['base_shipping_tax'] = ($_source->getBaseShippingInclTax() - $_source->getBaseShippingAmount());
	
	foreach($_source->getAllItems() as $item){
		unset($tax_rate);
		unset($pre_tax);
		unset($pre_tax_subtotal);
		unset($tax_after_discount);
		if ($item->getParentItem() == null && $item->getQtyOrdered() > 0) {				
			// only taxable items
			
			// base prices
			if($item->getProduct() && !in_array($item->getProduct()->getTaxClassId(), $zero_tax) && $item->getBaseTaxAmount() > 0){						
				$base_tax_rate = $item->getBaseTaxPercent()/100;
				$base_pre_tax = ($item->getBaseRowTotalInclTax() - $item->getBaseTaxAmount());
				$base_pre_tax_subtotal = $base_pre_tax - $item->getBaseDiscountAmount();
				$base_tax_after_discount = $item->getBaseTaxAmount() - (($item->getBaseDiscountAmount()/($item->getTaxPercent()+100))*$item->getTaxPercent());
				$totals['base_tax_after_discount'] += $base_tax_after_discount;
			}
			
			// order prices				
			if($item->getProduct() && !in_array($item->getProduct()->getTaxClassId(), $zero_tax) && $item->getTaxAmount() > 0){						
				$tax_rate = $item->getTaxPercent()/100;
				$pre_tax = ($item->getRowTotalInclTax() - $item->getTaxAmount());
				$pre_tax_subtotal = $pre_tax - $item->getDiscountAmount();
				$tax_after_discount = $item->getTaxAmount() - (($item->getDiscountAmount()/($item->getTaxPercent()+100))*$item->getTaxPercent());
				$totals['tax_after_discount'] += $tax_after_discount;
			}
		}
	}
	$totals['tax_after_discount_noshipping'] = $totals['tax_after_discount'];
	$totals['base_tax_after_discount_noshipping'] = $totals['base_tax_after_discount'];
	$totals['tax_after_discount'] += $totals['shipping_tax'];
	$totals['base_tax_after_discount'] += $totals['base_shipping_tax'];
	
	return array('discount_after_tax' => round($totals['tax_after_discount'],2), 
	'base_discount_after_tax' => round($totals['base_tax_after_discount'],2),
	'discount_after_tax_noshipping' => round($totals['tax_after_discount_noshipping'],2),
	'base_discount_after_tax_noshipping' => round($totals['base_tax_after_discount_noshipping'],2)
	
	);
}
