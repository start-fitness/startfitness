<?php
//SCRIPT NEEDS TO BE CRON'D

//echo "Welcome To Auto Print<br>";

// Set this so nothing is sent to print or invoiced
//$testing = true;

// TO Do
// Prioritise express orders
// After weekend, printing needs to be faster - work out around 400 sheets per printer before 7am

// To Set up each printer - search for $printerid
//session_save_path ("/home/sfadmin/public_html/script/J5HUH/print/tmp");
// First authenticate Google Cloud Print Session
//session_start();

	require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento Live

	umask(0);
	Mage::app();

	// Get the SQL Read/Write Setup
	$resource = Mage::getSingleton('core/resource');
	$read  = $resource->getConnection('core_read');

	// Login as Admin user
	Mage::getSingleton('admin/session')->login("autoprint","F^r!34zy%@*sWn.e");

	// Set Timeouts and reporting
	//Mage::setIsDeveloperMode(true); // Turn This off for it to work
	ini_set('memory_limit','768M');
	ini_set('display_errors',1);
	ini_set('max_execution_time', 0);
	error_reporting(E_ALL);

	// Function to send email upon error
	function sendErrorEmail($message){

        list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('autoprint',true);

        $mail = Mage::getModel('core/email');
        $mail->setToName('Start Fitness');
        $mail->setSubject('Auto Print Ebay Amazon Error '.date("D d-M-Y"));
        $mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
        $mail->setFromName('Auto Print Error');
        $mail->setType('text');
        $mail->setBodyText($message);

        //Easiest method to send multiple emails, instead of setting up a transactional
        try {
            foreach($toEmailAddresses as $toEmail) {
                $mail->setToEmail($toEmail);
                $mail->send();
            }
        } catch (Exception $e) {
            Mage::log('Autoprint Ebay Amazon Script: Could not send error report email(s).');
        }
	}

	// Need to check if we can print yet - here we use date and time
	// We will only print between Monday-Sat 
	$today = date('N'); // Number Format Days
	$time = date('Gi'); // 24 Hour

	// If its Mon-Saturday
        if($today == 1 || $today == 2 || $today == 3 || $today == 4 || $today == 5 || $today == 6){
                // Remember the server time is a hour out
                if($time >= 0400 && $time <= 2345){
                        $canprint = true;
                }else{
                        $canprint = false; // change this to false
                }
        }else{
                // Remember the server time is a hour out
                if(($time >= 0400 && $time <= 1600) || ($time >= 1900 && $time <= 2345)){
                        $canprint = true;
                }else{
                        $canprint = false; // change this to false
                }

        }
	//test override
	//$canprint = true;
	if($canprint == true){

		// Need to find all the orders this script has marked as printed
		// and make sure that Google actually has this printed
		// To Do here - limit the output from the sql to only say the last hours orders to check
		// better yet, what ever was printed in the last batch to check against - save time

		// First get the picknote directory
		/*$filename = "orders/";

		$sql_checkprinted = "SELECT distinct(sfo.`entity_id`),sfo.`increment_id` FROM `sales_flat_order` sfo
					RIGHT JOIN `sales_flat_order_status_history` sfosh ON sfo.`entity_id`=sfosh.`parent_id`
					WHERE sfosh.`comment` like '%autoprint'
					AND sfo.`status` = 'pick_note_printed'";
		$checkorders = $read->fetchAll($sql_checkprinted);

		foreach($checkorders as $checked){

			$filename = "PrintFolder/Express" . $checked['increment_id'].".pdf"; // this will be increment_id.pdf

			if (file_exists($filename)) {
    				echo "The file $filename exists<br>"; // do nothing - success print
			} else {
   			 	echo "The file $filename does not exist<br>"; // needs to be reprinted
			}
		}*/

		// First we priorities the Express shipments
		// These will always be printed out no matter what
/*
		$sql_getexpress = "SELECT sfo.`entity_id`,LOWER(sfo.`shipping_description`)
					FROM `sales_flat_order` sfo 
					RIGHT JOIN `metapack_consignment` mc ON sfo.`entity_id`=mc.`order_entity_id`
					WHERE sfo.`status` = 'processing' 
					AND sfo.`customer_group_id` NOT IN ('6') 
					AND sfo.`base_total_invoiced` > 0.00 
					AND mc.`consignment_status` REGEXP 'Allocated|Unallocated'
					AND sfo.`shipping_description` REGEXP 'dhl|next|exp|saturday|special|werktag|prime'
                                        AND sfo.`created_at` < date_sub(UTC_TIMESTAMP(), interval 25 minute)
					AND sfo.`store_id` NOT IN ('23')
					LIMIT 50";
		// limits set to 100 to protect against double printing from script overlap...  Please DO NOT CHANGE limits unless very sure of reasons.
		// cycles exclude code - AND sfo.`store_id` NOT IN ('21','10','1')

		$expressorders = $read->fetchAll($sql_getexpress);

		foreach($expressorders as $express){
			echo '<p>'.$express['entity_id'];
			//$loadorder = Mage::getModel('sales/order')->load(378506);
			$loadorder = Mage::getModel('sales/order')->load($express['entity_id']);
			// Try and get the invoice collection
			$prefix = "";
			if ($loadorder->hasInvoices()) {
				$shipping_type = "Express";
				$product_type = "All";
				
				
				$orderedItems = $loadorder->getAllItems();
				$orderedProductIds = array();
			    foreach ($orderedItems as $item) {
					if ($item->getProductType() == "simple")
					{
				    		$orderedProductIds[] = $item->getData('product_id');
					}
				}
				
				if(count($orderedProductIds) > 0){
					$shelves = array();
									
					
					foreach($orderedProductIds as $product){
						
						$bin = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product, 'bin_location', 0);
						
						
						if(!empty($bin))$shelves[] = $bin;
					}
					if(count($shelves) > 0){
						asort($shelves);
						//var_dump($shelves);
						$prefix1 = str_replace('-', '', $shelves[0]);
						$prefix = str_replace(' ', '', $prefix1);
						if(count($shelves) > 1)
						$prefix = "Z".$prefix;
					}
					
				}
				
				// A little bodge to get the invoices to print - as moogento expects more than one id in an array
				// We pass the single id in an array for it to print out without an error
				$orderIDs = explode(',',$loadorder->getID());
				//var_dump($prefix);exit;
				if($testing == false){
					try {
						echo "here express";
						// Get MOOGENTO INVOUICE CODE
						
						$pdf = Mage::getModel('sales/order_pdf_invoices')->getPickSeparated2($orderIDs);
						// MOOGENTO CODE END

						$pdf_filename = $prefix."-".$loadorder->getIncrementId().".pdf";
						$pdfFile = $pdf->render();
						$pdf->save("PrintFolder/Express/".$pdf_filename); // dont really want to save?
						//UPLOAD TO EXPRESS QUEUE & LOG

					} catch (Zend_Pdf_Exception $e) {
						sendErrorEmail('PDF error: ' . $e->getMessage());
						die ('PDF error: ' . $e->getMessage()); // Will need to send email if error to fix this message
					} catch (Exception $e) {
						sendErrorEmail('Application error: Order (' . $express['entity_id'] . ') - ' . $e->getMessage());
						die ('Application error: Order (' . $express['entity_id'] . ') - ' . $e->getMessage()); // Will need to send an email if error
					}
				}
			}
		}
		*/
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		// Get all the processing orders
		// Have to do it by sql as mage function is protected
		// We will use this process to select oldest first
		// Cameron asked to skip Cycles store view
		$sql_getorders = "SELECT sfo.`entity_id`,sfo.`shipping_description`
					FROM `sales_flat_order` sfo 
					LEFT JOIN `metapack_consignment` mc ON sfo.`entity_id`=mc.`order_entity_id`
					WHERE sfo.`status` = 'processing' 
					AND sfo.`customer_group_id` NOT IN ('6') 
					AND sfo.`base_total_invoiced` > 0.00 
					/*AND mc.`consignment_status` REGEXP 'Allocated|Unallocated'*/
					AND ((mc.`consignment_status` REGEXP 'Allocated|Unallocated' ) OR (mc.`consignment_status` is NULL AND sfo.`shipping_description` REGEXP 'Is Prime'))
					/*AND ( sfo.`shipping_description` NOT REGEXP 'Is Prime')*/  
					AND sfo.`store_id` IN (24,25,26,35)
					AND sfo.`created_at` < date_sub(UTC_TIMESTAMP(), interval 25 minute)
					ORDER BY sfo.`created_at` 
					ASC LIMIT 20";
		$orders = $read->fetchAll($sql_getorders);
		// limits set to 100 to protect against double printing from script overlap...  Please DO NOT CHANGE limits unless very sure of reasons.
		// cycles exclude code AND sfo.`store_id` NOT IN ('23')

		// Loop thorough each to make sure an invoice has been created
		$orderIDs = array();
		foreach ($orders as $order){
			echo '<p>aa'.$order['entity_id'];
			//$loadorder = Mage::getModel('sales/order')->load(474143);
			$loadorder = Mage::getModel('sales/order')->load($order['entity_id']);
			$hasbike = false;
			$hasbike_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
					LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
                    LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
					WHERE sfoi.`order_id` = ".$loadorder->getID()." 
					AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` IN (9,34) OR (sfoi.base_price >= 250 AND cpebrand.value IN (206,607)))
					GROUP BY cpe.`attribute_set_id`";
		
			// Get the output of the above SQL query
			$hasbikeres = $read->fetchCol($hasbike_getitems);
			if(count($hasbikeres) > 0) $hasbike = true;
			
			
			// Check if order has bike helmet or shoes items - UPDATE only items not in bins starting with "A"
			$hasbikeitems = false;
			$hasbikeitems_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
					LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
                    LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
					LEFT JOIN `catalog_product_entity_varchar` cpebin ON cpe.`entity_id`=cpebin.`entity_id` AND cpebin.attribute_id = 136 AND cpebin.store_id = 0
					WHERE sfoi.`order_id` = ".$loadorder->getID()." 
					AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` IN (10,12)) AND (cpebin.value NOT LIKE 'A%')
					GROUP BY cpe.`attribute_set_id`";
			$hasbikeitemsres = $read->fetchCol($hasbikeitems_getitems);
			if(count($hasbikeitemsres) > 0) $hasbikeitems = true;
			
			// Check if order has any non bikes related items
			$hasnonbikeitems = false;
			$hasnonbikeitems_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
					LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
                    LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
					WHERE sfoi.`order_id` = ".$loadorder->getID()." 
					AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` NOT IN (10,12,9,34) )
					GROUP BY cpe.`attribute_set_id`";
			$hasnonbikeitemsres = $read->fetchCol($hasnonbikeitems_getitems);
			if(count($hasnonbikeitemsres) > 0) $hasnonbikeitems = true;
			
			// Check if order has any running footwear items
			$hasfootwearitems = false;
			$hasfootwearitems_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
					LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
					WHERE sfoi.`order_id` = ".$loadorder->getID()." 
					AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` IN (45) )
					GROUP BY cpe.`attribute_set_id`";
			$hasfootwearitemsres = $read->fetchCol($hasfootwearitems_getitems);
			if(count($hasfootwearitemsres) > 0) $hasfootwearitems = true;
			
			// If the order has any non bike related item, cancel out the $hasbikeitems flag
			if($hasnonbikeitems) $hasbikeitems = false;
			
			// Check to see if order contains any scott items - UPDATE by scott we mean only bin locations beginning with "T" (cpebrand IN (22))
			$hasscott = false;
			$hasscott_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
					LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
                    LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
					LEFT JOIN `catalog_product_entity_varchar` cpebin ON cpe.`entity_id`=cpebin.`entity_id` AND cpebin.attribute_id = 136 AND cpebin.store_id = 0
					WHERE sfoi.`order_id` = ".$loadorder->getID()." 
					AND sfoi.`parent_item_id` IS NULL AND (cpebin.value LIKE 'T%')
					";
			$hasscottres = $read->fetchAll($hasscott_getitems);
			if(count($hasscottres) > 0) $hasscott = true;
			
			// Try and get the invoice collection
			if ($loadorder->hasInvoices()) {
				echo "here";
				// First we will need to see what the delivery type is - Express just goes straight to the printer
				// Include the EXPRESS SHIPPING terms in an array
				// $express_terms = array('Next Working','Express','Exp','Next Day');
				if(preg_match('/exp/', strtolower($order['shipping_description'])) || preg_match('/dhl/', strtolower($order['shipping_description'])) || preg_match('/next/', strtolower($order['shipping_description'])) || preg_match('/second/', strtolower($order['shipping_description'])) || 'ebay shipping - other 24 hour courier' == strtolower($order['shipping_description']) || 'ebay shipping - royal mail tracked 24' == strtolower($order['shipping_description']) || preg_match('/special/', strtolower($order['shipping_description'])) || preg_match('/prime/', strtolower($order['shipping_description'])) || preg_match('/saturday/', strtolower($order['shipping_description']))){
					$shipping_type = "Express";
					$product_type = "Express";
				}else{
					$shipping_type = "Standard";
					//$product_type = "Amazon";

                                        $sql_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
                                                        LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
                                                        WHERE sfoi.`order_id` = ".$loadorder->getID()."
                                                        AND sfoi.`parent_item_id` IS NULL
                                                        GROUP BY cpe.`attribute_set_id`";

                                        // Get the output of the above SQL query
                                        $attributes = $read->fetchCol($sql_getitems);

                                        //print_r($attributes);

                                        // We now need to know what the attribute set is
                                        // All footwear to one printer - Clothing and everything else to another printer - And mixed

                                        if (count($attributes) > 1 && (in_array("45", $attributes, true) || in_array("10", $attributes, true))){
                                                // As the array contains more than one item, and includes footwear, this is a mixed order
                                                $product_type = "Mixed";
						//$product_type = "temp";
                                        }else if($attributes[0]== 45 || $attributes[0]== 10){
                                                // This is footwear (fitness footwear)
						//$product_type = "temp";
                                                $product_type = "Footwear";
                                        }else{
                                                // This is clothing or other
                                                $product_type = "Other";
                                        }

				}
				
				$prefix = "";
				$orderedItems = $loadorder->getAllItems();
				$orderedProductIds = array();
			    foreach ($orderedItems as $item) {
					if ($item->getProductType() == "simple")
                                        {
					    $orderedProductIds[] = $item->getData('product_id');
					}
				}
				
				if(count($orderedProductIds) > 0){
					$shelves = array();
									
					
					foreach($orderedProductIds as $product){
						
						$bin = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product, 'bin_location', 0);
						
						
						if(!empty($bin))$shelves[] = $bin;
					}
					if(count($shelves) > 0){
						asort($shelves);
						//var_dump($shelves);
						$prefix1 = str_replace('-', '', $shelves[0]);
						$prefix = str_replace(' ', '', $prefix1);
						if(count($shelves) > 1)
						$prefix = "Z".$prefix;
					}
					
				}

				// A little bodge to get the invoices to print - as moogento expects more than one id in an array
				// We pass the single id in an array for it to print out without an error
				$orderIDs = explode(',',$loadorder->getID());
				echo "here2";
				if ($testing == false){
					try {
						echo "here NOT express";
						// Get MOOGENTO INVOUICE CODE
						$pdf = Mage::getModel('sales/order_pdf_invoices')->getPickSeparated2($orderIDs);
						// MOOGENTO CODE END

						$pdf_filename = $prefix."-".$loadorder->getIncrementId().".pdf";
						$pdfFile = $pdf->render();
                                                if($shipping_type=="Express" && ($hasscott || $hasbike || $hasbikeitems ))
                                                        $pdf->save("NCPrintFolder/bikesexp/".$pdf_filename); // dont really want to save?
						else if($hasscott)
							$pdf->save("NCPrintFolder/bikes/".$pdf_filename); // dont really want to save?
						else if($hasbike || $hasbikeitems)
							$pdf->save("NCPrintFolder/bikes/".$pdf_filename); // dont really want to save?
//						else if($hasfootwearitems)
//							$pdf->save("PrintFolder/ss-Other/".$pdf_filename); // Using Old SS Printer for footwear free gift promo
						else
							$pdf->save("PrintFolder/".$product_type."/".$pdf_filename); // dont really want to save?
						//UPLOAD TO CORRECT QUEUE & LOG

					} catch (Zend_Pdf_Exception $e) {
						sendErrorEmail('PDF error: ' . $e->getMessage());
						die ('PDF error: ' . $e->getMessage()); // Will need to send email if error to fix this message
					} catch (Exception $e) {
						sendErrorEmail('Application error: Order (' . $order['entity_id'] . ') - ' . $e->getMessage());
						die ('Application error: Order (' . $order['entity_id'] . ') - ' . $e->getMessage()); // Will need to send an email if error
					}
				}
			}
		}
	}else{
		echo "Currently Out Of Hours - Nothing to print till later - just refreshing (" . date('D jS F - G:s').")";
	}

// Finally, destroy the session.
//session_destroy();

echo "<br>This script has completed successfully";

?>
