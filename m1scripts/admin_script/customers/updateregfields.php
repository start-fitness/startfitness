<?php

require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$sql = "
INSERT INTO customer_entity_int (entity_type_id, attribute_id, entity_id, `value`)
SELECT 1, ac.attribute_id, ce.entity_id, 1 FROM customer_entity ce
JOIN `eav_attribute` ac ON attribute_code IN ('reg_extra'
,'reg_sms'
,'reg_email'
,'reg_fax'
,'reg_paper')
LEFT JOIN customer_entity_int ei on ei.entity_id = ce.entity_id AND ac.attribute_id = ei.attribute_id
WHERE ei.value IS NULL
";
$write->query($sql);
