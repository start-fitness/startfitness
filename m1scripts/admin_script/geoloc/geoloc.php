<?php
###################################################
# Country Selector Gateway
###################################################
# By: Geoffrey@dimasoft.co.uk
# 19/12/2013
# Uses the geoip data bin to locate IPs
# script in JSON format
###################################################
# Returns:
#		- CODE (country Code)
#		- NAME (country Name)
#		- IP (user ip)
##################################################

// Uncomment to enable PHP errors
/*ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);*/

// Including geoip lib
require ("geoloc/geoip.inc");
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Init. of geoip object, takes location of bin file as parameter
$gi = geoip_open("geoloc/GeoIP.dat",GEOIP_STANDARD);


// NOTHING TO CHANGE PAST THIS POINT

$currenturl = null;
$ip = $_SERVER['REMOTE_ADDR'];

// Checking if we got any parameters sent by the calling script, if so use them over the default values
if(isset($_GET['IP'])) $ip = $_GET['IP'];


// Generate return object
$resp = array(
"CODE"	=>  geoip_country_code_by_addr($gi, $ip),
"NAME"	=>  geoip_country_name_by_addr($gi, $ip),
"IP"	=>	$ip,
);

// serving up that JSON goodness !
header('Content-type: application/json');
//echo json_encode();
echo Mage::helper('core')->jsonEncode($resp);
##################################################
# Quick and efficient =)
##################################################
