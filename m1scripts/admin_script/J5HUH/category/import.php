<?php

error_reporting(E_ALL | E_STRICT);
ini_set('display_errors', 1);

require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');

ini_set('display_errors', 1);
Mage::app();

$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
$products->addAttributeToSelect('category_ids');

$row = 1;
if (($handle = fopen("import.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $num = count($data);
        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        for ($c=0; $c < $num; $c++) {
            echo $data[$c] . "<br />\n";
        }
    }
    fclose($handle);
}
?>