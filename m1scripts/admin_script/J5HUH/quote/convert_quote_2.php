<?php
require('/home/sfadmin/public_html/app/Mage.php');
Mage::app();
$quote = Mage::getModel('sales/quote')->load(3900302);

$convert = Mage::getModel('sales/convert_quote');
$order = $convert->toOrder($quote);

$order->addressToOrder($quote->getAddress(),$order);

foreach($quote->getAllItems() as $item){
    $orderItem = $convert->itemToOrderItem($item);
    if ($item->getParentItem()) {
       $orderItem->setParentItem($order->getItemByQuoteItemId($item->getParentItem()->getId()));
    }
    $order->addItem($orderItem);
}


$quote->getShippingAddress()->setPaymentMethod('checkmo');
$quote->getShippingAddress()->setCollectShippingRates(true);

$payment = $quote->getPayment();
$payment->importData($data);
$quote->save();

$payment = $convert->paymentToOrderPayment($quote->getPayment());

//$order->setPayment($quote->getPayment());

//$message = '[Notice] - Order converted from quote manually';
//$order->addStatusToHistory($order->getStatus(), $message);
//$order->place();
//$order->save();

$quote->setIsActive(false)->save();