<?php
	require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
	Mage::app();
	$read = Mage::getSingleton('core/resource')->getConnection('core_read');

if(isset($_GET['get_order_number'])){
	
	$order_number = $_GET['get_order_number'];

	if(strlen($order_number) >= "9" && strlen($order_number) <= "11"){
	
		//$sql = "SELECT entity_id,increment_id,sku,name,product_id,qty_invoiced,qty_refunded FROM `sales_flat_order` INNER JOIN `sales_flat_order_item` on sales_flat_order.entity_id=sales_flat_order_item.order_id WHERE sales_flat_order.increment_id=".$order_number." AND sales_flat_order_item.product_type = 'simple'";

		
 		// G.Hall - Added extra join sfoi2 to pull back refunded qty from configurable, if no configurable use simples refunded qty instead
		$sql = "SELECT sfo.`entity_id`,sfo.`increment_id`,sfoi.`sku`,sfoi.`name`,sfoi.`product_id`,sum(sfoi.`qty_invoiced`) qty_invoiced, if(sfoi2.`qty_refunded` is not NULL,sum(sfoi2.`qty_refunded`), sum(sfoi.`qty_refunded`)) qty_refunded FROM `sales_flat_order` sfo
				INNER JOIN `sales_flat_order_item` sfoi on sfoi.`order_id`=sfo.`entity_id`
				LEFT JOIN `sales_flat_order_item` sfoi2 on sfoi2.`item_id`=sfoi.`parent_item_id`
				WHERE sfo.`increment_id`=".$order_number." AND sfoi.product_type = 'simple'
				GROUP BY sfoi.`sku`";

//echo $sql;
	
		$readresult=$read->query($sql);

		while ($row = $readresult->fetch() ) {
			$model_number = "SELECT value FROM `catalog_product_entity_varchar` WHERE entity_id = ".$row['product_id']." AND attribute_id = '134' AND store_id = '0' ";
			$model = $read->fetchOne($model_number);	

			// If item has been refunded
			if(isset($row['qty_refunded'])){
				$quantity = round($row['qty_invoiced']) - round($row['qty_refunded']) ;

			}else{		
				// Nothing Refunded - Get the actual count
				$quantity = round($row['qty_invoiced']);
			}
			echo $row['sku']."~".$model."~".$row['name']."~".$quantity.",\r\n";
		}
	$read->closeConnection();
	}else{
		echo "Invalid Order Number";
	}
}

if(isset($_GET['barcode_scan'])){
	$barcode_num  = $_GET['barcode_scan'];

	if(strlen($_GET['barcode_num']) >= "12"){
		$sql = "SELECT ent1.sku,var1.value as model,var2.value as title FROM `catalog_product_entity` AS ent1 INNER JOIN `catalog_product_entity_varchar` AS var1  ON ent1.entity_id=var1.entity_id INNER JOIN `catalog_product_entity_varchar` AS var2 ON ent1.entity_id=var2.entity_id where var1.attribute_id = '134' and ent1.sku = '".$barcode_num."' and var2.attribute_id = '71'";

		$readresult1 = $read->query($sql);
		while ($row = $readresult1->fetch() ) {
			echo $row['sku']."~".$row['model']."~".$row['title'];
		}
		$read->closeConnection();
	}else{
		echo "Invalid Barcode";
	}

}

if(isset($_GET['get_status'])){
	$ordernum = $_GET['get_status'];

	$collection = Mage::getModel('sales/order')->getCollection()
    	->addAttributeToFilter('increment_id', $ordernum);
	$order = $collection->getFirstItem();

	$collection_credit = Mage::getResourceModel('sales/order_creditmemo_collection')  // G.Hall - Added to check if credit memo is attached to an order
        ->addAttributeToFilter('order_id', $order->getId());
    $credit = $collection_credit->getFirstItem();
	
	$status='';
	if ($order->getId()) { 
    	$status = $order->getStatusLabel();
	if ($status == 'paypal_reversed')
	{
		$status = 'Cancelled';
	}
}
	if($order->getStatus() == "holded"){
		$status = 'Closed';
	}
	if ($status != 'Cancelled' && $status != 'Closed'){  // G.Hall - Added to mimic a "Refunded" status - Dan's script will refresh order data if an order has this status
		if ($credit->getId()) { 
	    	$status = 'Refunded';
		}
	}
	echo $status;
}

//header("HTTP/1.0 404 Not Found");


?>
