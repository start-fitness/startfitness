<?php

// Get The URL Data
$order = $_GET["order"];
$comment = $_GET["comment"];

// Turn On Error Reporting
error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit', '10048M');
ini_set('display_errors', 1);

// Require Mage
require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
Mage::app();

if (isset($order)) {
	if (isset($comment)) {
	$magorder = Mage::getModel('sales/order')->loadByIncrementId($order);
		if ($magorder->getId()) {
			// First See if the order has been scanned correct
			// If correct, update the status to packed
			if (stripos($comment, ": Correct") !== false) {
				// See what the current status is - if picknote printed then update the status
				if ($magorder->getStatus() == 'pick_note_printed'){
					$magorder->addStatusToHistory('packed', $comment, false);
					$magorder->save();
					echo 'Added comment - Order Status Updated "'.$comment.'" to order number '.$order;
				// Else keep the current status
				}else{
					$magorder->addStatusToHistory($magorder->getStatus(), $comment, false);
					$magorder->save();
					echo 'Added comment "'.$comment.'" to order number '.$order;
				}
			// Now see if there is a fauly barcode - chances are this will still be packed
			} else if(stripos($comment, ": More Mile clear") !== false) {
				if ($magorder->getStatus() == 'pick_note_printed'){
					$magorder->addStatusToHistory('packed', $comment, false);
					$magorder->save();
					echo 'Added comment - Order Status Updated "'.$comment.'" to order number '.$order;
				}else{
					$magorder->addStatusToHistory($magorder->getStatus(), $comment, false);
					$magorder->save();
					echo 'Added comment "'.$comment.'" to order number '.$order;
				}


			} else {
				$magorder->addStatusToHistory($magorder->getStatus(), $comment, false);
				$magorder->save();
				echo 'Added comment "'.$comment.'" to order number '.$order;
			}
		} else {
			echo 'Order Number '.$order.' Not Found!';
		}
	}
}

?>