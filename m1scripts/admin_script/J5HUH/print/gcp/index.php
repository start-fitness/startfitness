<?php

// To add printers to your account follow the following link 
// https://support.google.com/cloudprint/answer/1686197

session_start();

if (!isset($_SESSION['accessToken'])) {
    
    header('Location: oAuthRedirect.php?op=getauth');
}
?>