<?php
// Use a meta refresh to sort the orders
echo '<head>';
echo '<meta http-equiv="refresh" content="300">';
echo '<title>JS- Google Cloud Print</title>';
echo '</head>';

echo "<h2>DO NOT CLOSE THIS WINDOW</h2>";
echo "Welcome To Google Cloud Print<br>";

/////////////////////////////////////////////////////////////////////////////////
// 											  //
//   Google Cloud Print Service For Magento - by James Simpson - V0.01 	  //
//											  //
//   The plan here is to have 4 printers all together, one printer will hold   //
//   all standard footwear, one will be all standard clothing, one will be mix //
//   of all products, and there will also be an express printer		  //
// 											  //
/////////////////////////////////////////////////////////////////////////////////

// To Set up each printer - search for $printerid
session_save_path ("/home/sfadmin/public_html/script/J5HUH/print/tmp");
// First authenticate Google Cloud Print Session
session_start();
if (!isset($_SESSION['accessToken'])) {

	if (!is_writable(session_save_path())) {
    		echo 'Session path "'.session_save_path().'" is not writable for PHP!<br>'; 
	}
	echo $_SESSION['accessToken'];
	var_dump($_SESSION['accessToken']);


    header('Location: gcp/oAuthRedirect.php?op=getauth');
}else{

	// Connect to Mage & GCP
	//require_once('/home/startdev/domains/startdev.startcycles.co.uk/public_html/script/J5HUH/print/gcp/GoogleCloudPrint.php'); //Path to GCP Dev
	//require_once('/home/startdev/domains/startdev.startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento Dev

	require_once('/home/sfadmin/public_html/script/J5HUH/print/gcp/GoogleCloudPrint.php'); //Path to GCP Live
	require_once('/home/sfadmin/public_html/app/Mage.php'); //Path to Magento Live

	umask(0);
	Mage::app();

	// Get the SQL Read/Write Setup
	$resource = Mage::getSingleton('core/resource');
	$read  = $resource->getConnection('core_read');

	// Create the Google Clound Print Object
	$gcp = new GoogleCloudPrint();
	$gcp->setAuthToken($_SESSION['accessToken']);
	//$gcp->setAuthToken(file_get_contents('refreshtoken.conf'));
	$printers = $gcp->getPrinters();
	$printerid = '';
	// Use below to print out the printer list
	//echo"<pre>";
	//print_r($printers);
	//echo"</pre>";

	// Login as Admin user
	Mage::getSingleton('admin/session')->login("autoprint","QWV$2774gw");

	// Set Timeouts and reporting
	//Mage::setIsDeveloperMode(true); // Turn This off for it to work
	ini_set('memory_limit','768M');
	ini_set('display_errors',1);
	ini_set('max_execution_time', 0);
	error_reporting(E_ALL);
	
	// Need to check if we can print yet - here we use date and time
	// We will only print between Monday-Sat 
	$today = date('N'); // Number Format Days
	$time = date('Gi'); // 24 Hour

	// If its Mon-Friday
	if($today == 1 || $today == 2 || $today == 3 || $today == 4 || $today == 5){
		// Remember the server time is a hour out
		if($time >= 0500 && $time <= 1430){
			$canprint = true;
		}else{
			$canprint = false;
		}
	}else{
		$canprint = false;
	}

	if($canprint == true){

		// Get all the processing orders
		// Have to do it by sql as mage function is protected
		// We will use this process to select oldest first
		// Cameron asked to skip Cycles store view
		$sql_getorders = "SELECT sfo.`entity_id`,sfo.`shipping_description`
					FROM `sales_flat_order` sfo 
					RIGHT JOIN `metapack_consignment` mc ON sfo.`entity_id`=mc.`order_entity_id`
					WHERE sfo.`status` = 'processing' 
					AND sfo.`store_id` NOT IN ('21','10','1') 
					AND sfo.`customer_group_id` NOT IN ('6') 
					AND sfo.`base_total_invoiced` > 0.00 
					AND mc.`consignment_status` = 'Allocated'
					ORDER BY sfo.`created_at` 
					ASC LIMIT 20";
		//$sql_getorders = "SELECT `entity_id`,`shipping_description` FROM `sales_flat_order` WHERE `status` = 'processing' AND `customer_group_id` NOT IN ('6') ORDER BY `created_at` ASC LIMIT 3";
		$orders = $read->fetchAll($sql_getorders);

		// Loop thorough each to make sure an invoice has been created
		$orderIDs = array();
		foreach ($orders as $order){
			$loadorder = Mage::getModel('sales/order')->load($order['entity_id']);
			// Try and get the invoice collection
			if ($loadorder->hasInvoices()) {
				
				// First we will need to see what the delivery type is - Express just goes straight to the printer
				// Include the EXPRESS SHIPPING terms in an array
				//$express_terms = array('Next Working','Express','Exp','Next Day');
				if(preg_match('/exp/', strtolower($order['shipping_description'])) || preg_match('/next/', strtolower($order['shipping_description'])) || preg_match('/saturday/', strtolower($order['shipping_description']))){
					$shipping_type = "Express";
					$product_type = "All";
					$printerid = 'f53a7d2a-a1b8-1b25-8b9f-6a1274d81c64'; // Express Printer
				}else{
					$shipping_type = "Standard";
					// Select the printer
				
					// We will need to see what products are on the order to decide what printer to send this to
					// We will select the order items table for this part
					$sql_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_flat_order_item` sfoi
							LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
							WHERE sfoi.`order_id` = ".$loadorder->getID()." 
							AND sfoi.`parent_item_id` IS NULL
							GROUP BY cpe.`attribute_set_id`";
				
					// Get the output of the above SQL query
					$attributes = $read->fetchCol($sql_getitems);

					//print_r($attributes);
				
					// We now need to know what the attribute set is
					// All footwear to one printer - Clothing and everything else to another printer - And mixed 
					
					if (count($attributes) > 1 && in_array("45", $attributes, true)){
						// As the array contains more than one item, and includes footwear, this is a mixed order
						$product_type = "Mixed";
						$printerid = "7e910653-093d-9ffd-f568-63b518dee6e5"; // Mixed Order Printer
						//$printerid = '__google__docs'; // Google Docs For Testing
					}else if($attributes[0]['attribute_set_id']== 45){
						// This is footwear (fitness footwear)
						$product_type = "Footwear";
						$printerid = "e46c7868-04b6-0785-7024-f36aef63d027"; // Footwear Printer
						//$printerid = '__google__docs'; // Google Docs For Testing
					}else{
						// This is clothing or other
						$product_type = "Clothing / Other";
						$printerid = "418672e1-dae9-8560-111c-e8dbf044f6bb"; // Everything Else Printer
						//$printerid = '__google__docs'; // Google Docs For Testing
					}

				}


				// A little bodge to get the invoices to print - as moogento expects more than one id in an array
				// We pass the single id in an array for it to print out without an error
				$orderIDs = explode(',',$loadorder->getID());

				try {
					// Get MOOGENTO INVOUICE CODE
					$pdf = Mage::getModel('sales/order_pdf_invoices')->getPickSeparated2($orderIDs);
					// MOOGENTO CODE END

					$pdf_filename = $loadorder->getIncrementId().".pdf";
					$pdfFile = $pdf->render();
					$pdf->save("orders/".$pdf_filename); // dont really want to save?

					// Send over Google Cloud Print
					if(count($printers)==0) {
						echo "Could not get printers";
						exit;
					} else {
						$print_title = $loadorder->getIncrementId() . " " . $shipping_type . " (".$product_type.")"; // Used to display in GCP
						//$print_title = implode(",", $orderIDs) . " - Sent To Print";
						//$printerid = $printers[0]['id']; // Pass id of any printer to be used for print
						// Send document to the printer using GCP
						$resarray = $gcp->sendPrintToPrinter($printerid, $print_title, "orders/".$pdf_filename, "application/pdf");
						//echo $printerid . ' '; // Echo printer for testing 
						if($resarray['status']==true) {
							echo $print_title . " - Sent To Print<br>";
						} else {
							echo "An error occurred while printing the doc. Error code:".$resarray['errorcode']." Message:".$resarray['errormessage'];
						}
					}


				} catch (Zend_Pdf_Exception $e) {
					die ('PDF error: ' . $e->getMessage()); // Will need to send email if error to fix this message
				} catch (Exception $e) {
					die ('Application error: Order (' . $order['entity_id'] . ') - ' . $e->getMessage()); // Will need to send an email if error
				}
			}
		}
	}else{
		echo "Currently Out Of Hours - Nothing to print till later - just refreshing (" . date('D jS F - G:s').")";
	}
}

// Finally, destroy the session.
session_destroy();

echo "<br>This script has completed successfully";

// TO DO - DELETE OLD PICKNOTES AFTER X DAYS

?>