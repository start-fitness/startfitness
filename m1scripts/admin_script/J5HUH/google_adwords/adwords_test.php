<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();


// The SQL query
$sql_visible = "SELECT cpe.entity_id,cpe.attribute_set_id,cpe.type_id,cpe.sku,cped.value as 'percent',cped1.value as 'price',cpei1.value as 'brand',cpei2.value as 'freeship',cpei3.value as 'bike_type'
		FROM `catalog_product_entity` cpe 
		/*LEFT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id`
		LEFT JOIN `catalog_product_entity_int` cpei1 ON cpe.`entity_id`=cpei1.`entity_id`
                LEFT JOIN `catalog_product_entity_int` cpei2 ON cpe.`entity_id`=cpei2.`entity_id`
		LEFT JOIN `catalog_product_entity_int` cpei3 ON cpe.`entity_id`=cpei3.`entity_id`
		LEFT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id`
		LEFT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id`
		WHERE cpei.`attribute_id` = 102 
		AND cpei.`value` = 4 
		AND cpei.`store_id` = 0
		AND cpei1.`attribute_id` = 81
		AND cpei1.`store_id` = 0
                AND cpei2.`attribute_id` = 396
                AND cpei2.`store_id` = 0
                AND cpei3.`attribute_id` = 198
                AND cpei3.`store_id` = 0
		AND cped.`attribute_id` = 338
		AND cped.`store_id` = 0
                AND cped1.`attribute_id` = 75
                AND cped1.`store_id` = 0*/
LEFT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` AND cpei.`attribute_id` = 102 AND cpei.`store_id` = 0
LEFT JOIN `catalog_product_entity_int` cpei1 ON cpe.`entity_id`=cpei1.`entity_id` AND cpei1.`attribute_id` = 81 AND cpei1.`store_id` = 0
LEFT JOIN `catalog_product_entity_int` cpei2 ON cpe.`entity_id`=cpei2.`entity_id` AND cpei2.`attribute_id` = 396 AND cpei2.`store_id` = 0
LEFT JOIN `catalog_product_entity_int` cpei3 ON cpe.`entity_id`=cpei3.`entity_id` AND cpei3.`attribute_id` = 198 AND cpei3.`store_id` = 0
LEFT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id` AND cped.`attribute_id` = 338 AND cped.`store_id` = 0
LEFT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id` AND cped1.`attribute_id` = 75 AND cped1.`store_id` = 0
WHERE cpei.`value` = 4
AND cpe.entity_id = 356496";


// Fetch all reqults
$results_visible = $read->fetchAll($sql_visible);

//print_r($results_visible);



foreach ($results_visible as $line) {
	$percentage = 0;

	// Now find the stock
	if ($line['type_id'] == 'configurable'){
		$sql_stock = "SELECT COUNT(`product_id`) as 'total_simples', SUM(csi.`qty`) as 'total_stock', SUM(csi.`is_in_stock`)  as 'some_child_in_stock'
		FROM `cataloginventory_stock_item` csi
		WHERE product_id IN (SELECT config.`product_id`
		FROM `catalog_product_super_link` config
		WHERE config.`parent_id`='".$line['entity_id']."')";

		$results_stock = $read->fetchRow($sql_stock);
		
	}else{
		$sql_stock = "SELECT 0 as 'total_simples', qty as 'total_stock', is_in_stock as 'some_child_in_stock'  FROM `cataloginventory_stock_item` WHERE `product_id` = '".$line['entity_id']."'";
		$results_stock = $read->fetchRow($sql_stock);
	}

	if($results_stock['some_child_in_stock'] == '0' || $results_stock['total_simples'] == 0){
		$percentage = '0';
	}else{	
		$percentage = round($results_stock['some_child_in_stock']/$results_stock['total_simples']*100);
	}
	

	if (($line['attribute_set_id'] == '9' || $line['attribute_set_id'] == '34') && ($line['bike_type'] == '4300' || $line['bike_type'] == '4302' || $line['bike_type'] == '4301' || $line['bike_type'] == '490' || $line['bike_type'] == '986' || $line['bike_type'] == '1234'))  //ROAD
	{
		if ($line['price'] < '1000')
		{
			$group = '4831';
		}
		else if ($line['price'] < '3000')
		{
			$group = '4832';
		}
		else if ($line['price'] < '5000')
		{
			$group = '4833';
		}
		else
		{
			$group = '4834';
		}
	}
	else if (($line['attribute_set_id'] == '9' || $line['attribute_set_id'] == '34') && ($line['bike_type'] == '1155' || $line['bike_type'] == '4788')) //Mountain
	{
        	if ($line['price'] < '1000')
	        {
			$group = '4835';
	        }
        	else if ($line['price'] < '3000')
	        {
			$group = '4836';
	        }
        	else if ($line['price'] < '5000')
	        {
			$group = '4837';
	        }
        	else
	        {
			$group = '4838';
	        }
	}
	else if (($line['attribute_set_id'] == '9' || $line['attribute_set_id'] == '34') && ($line['bike_type'] == '4785' || $line['bike_type'] == '4787' || $line['bike_type'] == '4786')) //Ebike
	{
	        if ($line['price'] < '1000')
        	{
			$group = '4839';
        	}
	        else if ($line['price'] < '3000')
        	{
			$group = '4840';
        	}
	        else if ($line['price'] < '5000')
        	{
			$group = '4841';
        	}
	        else
        	{
			$group = '4842';
	        }
	}
	else if (($line['attribute_set_id'] == '9' || $line['attribute_set_id'] == '34') && ($line['bike_type'] == '1498')) //Hybrid
	{
        	if ($line['price'] < '1000')
        	{
			$group = '4843';
        	}
	        else if ($line['price'] < '3000')
        	{
			$group = '4844';
        	}
        	else if ($line['price'] < '5000')
        	{
			$group = '4845';
        	}
        	else
        	{
			$group = '4846';
        	}
	}
	else if (($line['attribute_set_id'] == '9' || $line['attribute_set_id'] == '34')) //Other Bike
	{
		$group = '4850';
	}
	else if ($line['attribute_set_id'] == '10') //Bike Footwear
	{
		$group = '4848';
	}
	else if ($line['attribute_set_id'] == '11') //Bike Clothing
	{
		$group = '4847';
	}
	else if ($line['attribute_set_id'] >= '12' && $line['attribute_set_id'] <= '42') //Bike Other / Accessories
	{
		$group = '4849';
	}
	//Free Shipping Products
        //else if($line['freeship'] == 1 && $line['brand'] !== '679'){
        //      $group = '4827'; // Free Delivery
	//New Products (3 Types)
	else if(round($line['percent']) <= '15' && $line['brand'] !== '679' && $line['attribute_set_id'] == '45'){ // If there is less than 10% off price and brand is not more mile FW
              $group = '4828'; // New Product (Footwear)
	}else if(round($line['percent']) <= '15' && $line['brand'] !== '679' && $line['attribute_set_id'] == '44'){ // If there is less than 10% off price and brand is not more mile CL
              $group = '4829'; // New Product (Clothing)
        }else if(round($line['percent']) <= '15' && $line['brand'] !== '679' && $line['attribute_set_id'] == '43'){ // If there is less than 10% off price and brand is not more mile AC
              $group = '4830'; // New Product (Accessories)
	//Reduced (4 Types)
        }else if(round($line['percent']) >= '30' && $line['brand'] !== '679' && $line['attribute_set_id'] == '45' && $results_stock['total_stock'] >= '100' && $percentage >= '70'){ // 40% off
              $group = '4820'; // Reduced HS (Footwear)
        }else if(round($line['percent']) >= '30' && $line['brand'] !== '679' && $line['attribute_set_id'] == '44' && $results_stock['total_stock'] >= '75' && $percentage >= '60'){ // 40% off
              $group = '4822'; // Reduced HS (Clothing)
        }else if(round($line['percent']) >= '30' && $line['brand'] !== '679' && $line['attribute_set_id'] == '45' && $results_stock['total_stock'] <= '100' && $percentage >= '70'){ // 40% off
              $group = '4821'; // Reduced LS (Footwear)
        }else if(round($line['percent']) >= '30' && $line['brand'] !== '679' && $line['attribute_set_id'] == '44' && $results_stock['total_stock'] <= '75' && $percentage >= '60'){ // 40% off
              $group = '4823'; // Reduced LS (Clothing)
	//High Stock (3 Types)
	}else if(round($results_stock['total_stock']) >= '100' && $line['brand'] !== '679' && $line['attribute_set_id'] == '45'){ // >100
              $group = '4824'; // HS Footwear
        }else if(round($results_stock['total_stock']) >= '100' && $line['brand'] !== '679' && $line['attribute_set_id'] == '44'){ // >100
              $group = '4825'; // HS Clothing
        }else if(round($results_stock['total_stock']) >= '100' && $line['brand'] !== '679' && $line['attribute_set_id'] == '43'){ // >100
              $group = '4826'; // HS Accessories
	//if(round($results_stock['total_stock']) >= '100' && $percentage >= '75' && round($line['percent']) >= '30' && $line['brand'] !== '679'){
	//	$group = '3748'; // Best Return
	//}else if(round($line['percent']) <= '10' && $line['brand'] !== '679'){ // If there is less than 10% off price and brand is not more mile
	//	$group = '3750'; // New Product
	}else if($line['attribute_set_id'] == '44' &&  $results_stock['some_child_in_stock'] == 1){
		$group = '4117'; // Single Size Clothing
	}else if($line['attribute_set_id'] == '45' &&  $results_stock['some_child_in_stock'] == 1){
		$group = '4118'; // Single Size Footwear
	}else if($line['attribute_set_id'] == '44' &&  $results_stock['some_child_in_stock'] >= 2 && $results_stock['some_child_in_stock'] <= 3){
		$group = '4115'; // Little Size Clothing (2 - 3 Stock Lines)
	}else if($line['attribute_set_id'] == '45' &&  $results_stock['some_child_in_stock'] >= 2 && $results_stock['some_child_in_stock'] <= 4){
		$group = '4116'; // Little Size Footwear (2 - 4 Stock Lines)
	}else{
		$group = 'NULL'; // Worst Return
	}
//	if ($line['entity_id'] == 382468)
//	{
	echo $line['sku'] . ' - ' . $line['type_id'] . ' - <strong>Total Simples: </strong>' . $results_stock['total_simples'] . ' - Total Simples In Stock: ' . $results_stock['some_child_in_stock'] . ' - Total Stock: '  . round($results_stock['total_stock']) . ' - Percentage In Stock: ' . $percentage . ' - Percetage Saving: ' . round($line['percent']) . ' ' . $group;
	echo "\r\n";
//	}
	// Need to see if the row exists
	$sql_ifexists = "SELECT count(*) FROM `catalog_product_entity_int` WHERE `attribute_id` = '345' AND `entity_id` = '".$line['entity_id']."' AND `store_id` = 0";
	$results_exists = $read->fetchOne($sql_ifexists);

	if($results_exists == 0){
		// Row doesnt exist
		$sql_insert = "INSERT INTO `catalog_product_entity_int` ( attribute_id, store_id, entity_id, value) VALUES ( '345', '0', '".$line['entity_id']."', '".$group."')";
		//echo $sql_insert  . '<br>';
		$writeConnection->query($sql_insert);
	}else{
		// Update the database
		$sql_query = "UPDATE `catalog_product_entity_int` SET `value` = ".$group." WHERE `attribute_id` = '345' AND `entity_id` = '".$line['entity_id']."' AND `store_id` = 0 ";
		//echo $sql_query . '<br>';
		$writeConnection->query($sql_query);
	}
}

echo "Google Updated Successfully";

?>
