<?php
error_reporting(E_ALL);

// First connect up to Mage
require_once('/var/www/html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

date_default_timezone_set("GMT");

$html .= '<style>';
$html .= '.CSSTableGenerator table { font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { color: #FFF; background: #A0A0A0; font-weight: bold; font-size: 11px; }';
$html .= '.CSSTableGenerator td { padding-left: 5px; font-size: 10px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';

$max_option_columns=4;
$store_id = 0;
$entity_type_id = 4;
$build_html='<div class="CSSTableGenerator">';

$product_attribute_names = array("name","price","cost","manufacturer");

// Get Parent Product ID's
$sql_products = "SELECT cpe.`entity_id`, cpe.`attribute_set_id`, cpe.`has_options`, IF(cpe.`type_id`='configurable' OR (cpe.`type_id`='simple' AND cpe.`has_options` = 0 AND cpsl.`parent_id` is NULL),'true','false') isProduct, cpsl.`parent_id` FROM `catalog_product_entity` cpe
				LEFT JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpe.`entity_id` 
				HAVING isProduct = 'true'";
$results_sql_products = $read->fetchAll($sql_products);

// Get Child ID's
foreach ($results_sql_products as $row) {
	$option_attribute_names = array();

	// Add Option Attribute Names to Array
	if ($row['has_options']) {
		$sql_product_attributes = "SELECT ea.`attribute_id`, ea.`attribute_code` FROM `catalog_product_super_attribute` cpsa 
									INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpsa.`attribute_id`
									WHERE cpsa.`product_id` = ".$row['entity_id'];
		$results_sql_product_attributes = $read->fetchAll($sql_product_attributes);
		foreach ($results_sql_product_attributes as $option_attributes) {
			$option_attribute_names[]=array($option_attributes['attribute_id'],$option_attributes['attribute_code']);
		}
		$option_count = count($option_attribute_names);
	}

	$build_html.='<table border="1">';
	$build_html.='<tr><th>Entity ID</th><th>Manufacturer</th><th>Name</th><th>Price</th><th>Cost</th><th>Total Qty</th>';
	$a=get_attribute($row['entity_id'], $product_attribute_names);
	//echo $row['entity_id']."<br />";
	if ($row['has_options']) {
		if ($option_count == 1) {
			$column_attribute_id = $option_attribute_names[0][0];
			$sql_product_options = "SELECT cpsl.`product_id`, eaov.`value`, cisi.`qty` FROM `catalog_product_super_link` cpsl
									INNER JOIN `catalog_product_entity_int` cpei ON cpei.`entity_id` = cpsl.`product_id` AND cpei.`attribute_id` = ".$column_attribute_id."
									INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = cpei.`attribute_id` AND eao.`option_id` = cpei.`value`
									INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = ".$store_id."
									LEFT OUTER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id` = cpsl.`product_id`
									WHERE cpsl.`parent_id` = ".$row['entity_id']."
									ORDER BY eao.`attribute_id`, eao.`sort_order`, eaov.`value`";

			$results_sql_product_options = $read->fetchAll($sql_product_options);
			//print_r($results_sql_product_options);
			//exit;
			$build_html_header='';
			$total_qty = 0;
			$i=1;
			foreach ($results_sql_product_options as $option_data_row) {
				$total_qty += $option_data_row['qty'];
				//$b = get_attribute($option_data_row['product_id'], $option_attribute_names);
				$build_html_header.='<th>'.$option_data_row['value'].'</th>';
				if ($i % $max_option_columns == 0) {
					echo '<pre>';
echo htmlspecialchars($build_html_header.'</tr>');
echo '</pre>';
					$th[] = $build_html_header.'</tr>';
					$build_html_header='<tr><th colspan="6"></th>';
				}
				$i++;
			}
					echo '<pre>';
			print_r(htmlspecialchars($th));
					echo '</pre>';
			//exit;
			$build_html.='</tr>';
			$build_html.='<tr><td width="50">'.$a['entity_id'].'</td><td width="50">'.$a['manufacturer'].'</td><td width="200">'.$a['name'].'</td><td width="50">'.number_format($a['price'],2).'</td><td width="50">'.$a['cost'].'</td><td width="50">'.$total_qty.'</td>';
			foreach ($results_sql_product_options as $option_data_row) {
				$build_html.='<td width="20">'.number_format($option_data_row['qty'],0).'</td>';
			}

		}
			//print_r($b);

		$build_html.='</tr>';
	}
	$build_html.='</table><p>';
}
$build_html.='</div>';
echo $html;
echo $build_html;
exit;

foreach ($results_sql_products as $row) {
	$a[$row['entity_id']]=get_attribute($row['entity_id'], $attribute_names);
	$a[$row['entity_id']]['qty']=$row['qty'];
}

$writeTurnaround .= '<div class="CSSTableGenerator" >';
$writeTurnaround .= '<table width="200" border="0" cellspacing="0" cellpadding="0">';
$writeTurnaround .= '<tr><th colspan="2" align="center"><strong>All Products</strong></th></tr>';
$writeTurnaround .= '<tr>';
foreach ($a as $row) {
	foreach (array_keys($row) as $key) {
		$writeTurnaround .= '<th>'.$key.'</th>';
	}
	break;
}
$writeTurnaround .= '</tr>';

foreach ($a as $row) {
	$writeTurnaround .= '<tr>';
	//print_r(array_keys($a));
	foreach ($row as $key => $value) {
		$writeTurnaround .= '<td>'.$value.'</td>';
	}
	$writeTurnaround .= '</tr>';
}
$writeTurnaround .= '</table></div>';

function get_attribute($entity_id, $attribute) {
	global $read, $store_id, $entity_type_id;
	$results_array = array();
	$results_array['entity_id']=$entity_id;
	foreach ($attribute as $attribute_name) {
		$sql_get_attribute = "SELECT `attribute_id`, `backend_type` FROM `eav_attribute` WHERE attribute_code = '".$attribute_name."' AND `entity_type_id` = ".$entity_type_id;
		$results_sql_get_attribute = $read->fetchAll($sql_get_attribute);

		if ($results_sql_get_attribute) {
			$attribute_id = $results_sql_get_attribute[0]['attribute_id'];
			$attribute_type = $results_sql_get_attribute[0]['backend_type'];
			switch ($attribute_type) {
			    case "datetime":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_datetime` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "decimal":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_decimal` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "int":
			    	$sql_attribute = "SELECT eaov.`value` FROM `catalog_product_entity_int` cpei INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = cpei.`value` WHERE cpei.`attribute_id` = ".$attribute_id." AND cpei.`entity_id` = ".$entity_id." AND cpei.`entity_type_id` = ".$entity_type_id." AND cpei.`store_id` = ".$store_id." AND eaov.`store_id` = ".$store_id;
			        break;
			    case "media_gallery":
			    	$sql_attribute = "SELECT `value` FROM  `catalog_product_entity_media_gallery` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id;
			        break;
			    case "varchar":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_varchar` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "text":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_text` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			}
			$results_sql_attribute = $read->fetchAll($sql_attribute);
			$results_array[$attribute_name]=$results_sql_attribute[0]['value'];
		} else {
			$results_array[$attribute_name]="NULL";
		}
	}
	return $results_array;
}



// css styling
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #F33; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #0303F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr>';
$html .= '<td align="center" valign="top">'.$writeTurnaround.'</td>';
$html .= '</tr>';
$html .= '<tr><td colspan="3" align="center"><div class="line"></div><h3>ISSUE ORDERS</h3><p>Orders below need updating/chasing/dispatching ASAP</p></td></tr>';
$html .= '<tr><td colspan="3">'.$writeUpdate.'</td></tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';


// Closing tables
$html .= '</td></tr></table>';
echo $html;
exit;
// Send the email
//Receiver Details
$sendToEmailAddress = array('hallgary@sky.com');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Daily Customer Services');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Customer Services Report '.date("D d-M-Y"));
			$mail->send();

//end
echo $html;

?>