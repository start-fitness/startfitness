<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

//$start_date = date("Y-m-d", strtotime("-1 days")) . ' 09:00:00';
//$end_date = date("Y-m-d") . ' 08:59:59';

$num_days = (date('w')==1) ? 2 : 1; //if monday need to look back 2 days, otherwise 1 day
$start_date = date("Y-m-d", strtotime("-$num_days days")) . ' 09:00:00';
$end_date = date("Y-m-d") . ' 08:59:59';


$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';

// Get Data
$sql_getdata = "SELECT DISTINCT sfo.`created_at`, sfo.`increment_id`, sfo.`shipping_description`, mc.`consignment_status`, sfosh.`comment` FROM `sales_flat_order_status_history` sfosh
				INNER JOIN `sales_flat_order` sfo ON sfo.`entity_id`=sfosh.`parent_id`
				LEFT JOIN `metapack_consignment` mc ON mc.`order_entity_id`=sfo.`entity_id`
				WHERE sfosh.`status`='pick_note_printed' AND sfosh.`comment` LIKE '%AUTO PRINT -- JOB ID:  -- STATUS: %' AND sfo.`created_at` >= '".$start_date."' AND sfo.`created_at` <= '" .$end_date. "'
				ORDER BY sfo.`created_at` DESC";

//echo $sql_getdata; exit;				
// Fetch all reqults
$results = $read->fetchAll($sql_getdata);

if(empty($results)) exit; // nothing to report

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><td colspan="5" align="center">Possible Print Errors - From '.$start_date.' to '.$end_date.'</td></tr>';
$html .= '<tr><th align="left">Created Date</th><th align="left">Order #</th><th align="left">Shipping</th><th>Consignment Status</th><th>Comment</th></tr>';
foreach($results as $row){
	$html .= '<tr><td>'.$row['created_at'].'</td><td>'.$row['increment_id'].'</td><td>'.$row['shipping_description'].'</td><td align="center">'.$row['consignment_status'].'</td><td>'.$row['comment'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');

$sendToEmailAddress = array('gary@startfitness.co.uk','pat@startfitness.co.uk','michaeltaggart@startfitness.co.uk','michaelamillican@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Possible Print Errors '.date("D d-M-Y"));
			$mail->send();


echo $html;
?>
