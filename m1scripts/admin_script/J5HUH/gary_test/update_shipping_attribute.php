<?php

/*
G. Hall 12/07/2019
Always set the simples Shipping Group to be the same value as the parent
*/

error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php');

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$write = $read = $resource->getConnection();

$debug = false;

umask(0);

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');
//$write = $resource->getConnection('core_write');

// SQL Queries
$sql_getprod = "SELECT cpei_shipgroup.`value_id`, cpe.`entity_id`, cpei_shipgroup.`value` ship_group, cpsl.`product_id`, cpsl.`parent_id`, cpei_shipgroup_parent.`value` parent_ship_group FROM `catalog_product_entity` cpe
				INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`= cpe.`entity_id`
				INNER JOIN `catalog_product_entity` cpe_exists ON cpe_exists.`entity_id`=cpsl.`parent_id`
				LEFT JOIN `catalog_product_entity_int` cpei_shipgroup ON cpei_shipgroup.`entity_id` = cpe.`entity_id` AND cpei_shipgroup.`attribute_id` = 199 AND cpei_shipgroup.`store_id` = 0
				LEFT JOIN `catalog_product_entity_int` cpei_shipgroup_parent ON cpei_shipgroup_parent.`entity_id` = cpsl.`parent_id` AND cpei_shipgroup_parent.`attribute_id` = 199 AND cpei_shipgroup_parent.`store_id` = 0
				WHERE (IFNULL(cpei_shipgroup.`value`,0) <> IFNULL(cpei_shipgroup_parent.`value`,0))";

$results = $read->fetchAll($sql_getprod);
//echo $sql_getprod; exit;
//echo sizeof($results); exit;
foreach($results as $row) {
	$parent_val=($row['parent_ship_group']===NULL)?"NULL":$row['parent_ship_group'];

	if ($row['value_id'] !==NULL) {
		$sql_update="UPDATE `catalog_product_entity_int`
				SET `value`=".$parent_val." 
				WHERE `value_id`=".$row['value_id'];

		if ($debug) {
			echo PHP_EOL . '   *U*   '. $row['entity_id'] . ' - ' . $sql_update;
		} else {
			$write->query($sql_update);
 		}

	} else {

		$sql_insert="INSERT INTO `catalog_product_entity_int` (`attribute_id`, `store_id`, `entity_id`, `value`)
		 VALUES (199, 0, " . $row['entity_id']. ", " . $parent_val . ")";

		if ($debug) {
	 		echo PHP_EOL . '   *I*   '. $row['entity_id'] . ' - ' . $sql_insert;
		} else {
			$write->query($sql_insert);
		}
	}
	//$write->query($sql_update);
}
echo PHP_EOL .  'Processed: ' . sizeof($results);

?>
