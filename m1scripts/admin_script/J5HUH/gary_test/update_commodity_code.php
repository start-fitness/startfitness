<?php

/*
G. Hall 11/12/2020
If default or empty commodity code then set commodity code to value in gh_commodity_code table based on attribute_set_id, sub_type & option_id 
*/

error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php');

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$write = $read = $resource->getConnection();

$debug = false;

umask(0);

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');
//$write = $resource->getConnection('core_write');

//********************* ASSIGN COMMODITY CODES TO PARENT/VISIBLE PRODUCTS ************************


$sql_get_commodity_codes = "SELECT cpev_commodity_code.`value_id`, cpe.`entity_id`, cpev_model.`value` `model`, cpev_name.`value` `name`, tmp.`qty`, cpe.`attribute_set_id`, IF(cpei_type.`attribute_id` IS NOT NULL, cpei_type.`attribute_id`, cpev_type.`attribute_id`) `attribute_id`, IF(cpei_type.`attribute_id` IS NOT NULL, cpei_type.`value`, cpev_type.`value`) `option_id`, cpev_commodity_code.`value` `cc_before`, cc.`commodity_code` `cc_after`,  cc.`derived_value` `cc_class_after` FROM `catalog_product_entity` cpe 
INNER JOIN `catalog_product_entity_int` cpei_visibility ON cpei_visibility.`entity_id`=cpe.`entity_id` AND cpei_visibility.`attribute_id` = 102 AND cpei_visibility.`value`=4
LEFT JOIN `catalog_product_entity_varchar` cpev_commodity_code ON cpev_commodity_code.`entity_id` = cpe.`entity_id` AND cpev_commodity_code.`attribute_id` = 428 AND cpev_commodity_code.`store_id` = 0
LEFT JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id` = cpe.`entity_id`
LEFT JOIN `catalog_product_entity_int` cpei_type ON cpei_type.`entity_id` = IF(cpsl.`parent_id` IS NOT NULL, cpsl.`parent_id`, cpe.`entity_id`) AND cpei_type.`store_id` = 0 AND cpei_type.`attribute_id` =
(CASE
     WHEN cpe.`attribute_set_id` = 9 THEN 198                                        
     WHEN cpe.`attribute_set_id` IN (11,44,47) THEN 329     
     WHEN cpe.`attribute_set_id` = 10 THEN 189                                      
     WHEN cpe.`attribute_set_id` IN (43,46) THEN 340           
     WHEN cpe.`attribute_set_id` = 48 THEN 264                                      
     WHEN cpe.`attribute_set_id` = 12 THEN 193                                      
     WHEN cpe.`attribute_set_id` = 13 THEN 194                                      
     WHEN cpe.`attribute_set_id` = 14 THEN 195                                      
     WHEN cpe.`attribute_set_id` = 15 THEN 196                                      
     WHEN cpe.`attribute_set_id` = 16 THEN 198                                      
     WHEN cpe.`attribute_set_id` = 17 THEN 201                                      
     WHEN cpe.`attribute_set_id` = 18 THEN 203                                      
     WHEN cpe.`attribute_set_id` = 19 THEN 206                                      
     WHEN cpe.`attribute_set_id` = 20 THEN 208                                      
     WHEN cpe.`attribute_set_id` = 21 THEN 210                                      
     WHEN cpe.`attribute_set_id` = 22 THEN 211                                      
     WHEN cpe.`attribute_set_id` = 23 THEN 221                                      
     WHEN cpe.`attribute_set_id` = 24 THEN 213                                      
     WHEN cpe.`attribute_set_id` = 25 THEN 214                                      
     WHEN cpe.`attribute_set_id` = 26 THEN 216                                      
     WHEN cpe.`attribute_set_id` = 27 THEN 217                                      
     WHEN cpe.`attribute_set_id` = 28 THEN 219                                      
     WHEN cpe.`attribute_set_id` = 29 THEN 221                                      
     WHEN cpe.`attribute_set_id` = 31 THEN 264                                      
     WHEN cpe.`attribute_set_id` = 34 THEN 198                                      
     WHEN cpe.`attribute_set_id` = 35 THEN 198                                      
     WHEN cpe.`attribute_set_id` = 36 THEN 272                                      
     WHEN cpe.`attribute_set_id` = 37 THEN 221                                      
     WHEN cpe.`attribute_set_id` = 38 THEN 273                                      
     WHEN cpe.`attribute_set_id` = 39 THEN 274                                      
     WHEN cpe.`attribute_set_id` = 40 THEN 221                                      
     WHEN cpe.`attribute_set_id` = 41 THEN 198                                      
     WHEN cpe.`attribute_set_id` = 42 THEN 221                                      
END)
LEFT JOIN `catalog_product_entity_varchar` cpev_type ON cpev_type.`entity_id` = IF(cpsl.`parent_id` IS NOT NULL, cpsl.`parent_id`, cpe.`entity_id`) AND cpev_type.`store_id` = 0 AND cpev_type.`attribute_id` =
(CASE
     WHEN cpe.`attribute_set_id` = 45 THEN 289                                      
END)
LEFT JOIN `gh_commodity_codes` cc ON cc.`attribute_set_id` = cpe.`attribute_set_id` AND cc.`attribute_id` = IF(cpei_type.`attribute_id` IS NOT NULL, cpei_type.`attribute_id`, cpev_type.`attribute_id`)
 AND cc.`option_id` = IF(cpei_type.`attribute_id` IS NOT NULL, cpei_type.`value`, cpev_type.`value`)
LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = cpe.`entity_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0
LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = cpe.`entity_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0
LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id` INNER JOIN `catalog_product_entity` cpe2 ON cpe2.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp ON tmp.`product_id`=cpe.`entity_id`
WHERE (cpev_commodity_code.`value` IS NULL) AND tmp.`qty` >= 0
ORDER BY cpe.`entity_id` ASC";

$results = $read->fetchAll($sql_get_commodity_codes);

$update_count = $insert_count = $ignore_count = $nomatch_count = 0;

foreach($results as $row) {
	//echo $row['cc_before'] . ' - ' . $row['cc_after'] . ' - ' .  $row['value_id']. ' - ';
	$to_update = $row['cc_before'] != $row['cc_after'] && $row['cc_after'] != NULL && $row['value_id'] != NULL;
	$to_insert = $row['cc_before'] != $row['cc_after'] && $row['cc_after'] != NULL && $row['value_id'] == NULL;
	$to_ignore = $row['cc_before'] == $row['cc_after'];
	
	//echo ($to_update ? 'UPDATE' : '') . ($to_insert ? 'INSERT' : '') . ($to_ignore ? 'IGNORE' : '') . ' - ' . $row['entity_id'] . ' - ' . $row['model'] . ' - ';

	if($to_update) {
		echo $sql_update_query = "UPDATE `catalog_product_entity_varchar`
						SET `value` = " . $row['cc_after'] . "
						WHERE `value_id` = " . $row['value_id'];
		$write->query($sql_update_query);
		$update_count++;
	} elseif ($to_insert) {
		echo $sql_insert_query = "INSERT INTO `catalog_product_entity_varchar` (`attribute_id`, `store_id`, `entity_id`, `value`)
						VALUES (428, 0, " .$row['entity_id'] . ", " . $row['cc_after'] . ")";
		$write->query($sql_insert_query);
		$insert_count++;
	} elseif ($to_ignore) {
		$ignore_count++;
	} else {
		echo 'NOMATCH - ' . $row['entity_id'] . ' - ' . $row['model'] . ' - ' .  $row['name']. ' - ' . number_format($row['qty'],0) . ' - ' . $row['attribute_set_id'] . ' - ' .  $row['attribute_id'] . ' - ' .  $row['option_id'] . ' - ' . $row['cc_before'];
		$nomatch_count ++;
	}
	echo '<p>';
}

echo "<p>Updates = $update_count";
echo "<p>Inserts = $insert_count";
echo "<p>Ignores = $ignore_count";
echo "<p>NoMatches = $nomatch_count";


// ***************** SYNC SIMPLES TO CONFIGURABLES ********************

$update_simples_count = $insert_simples_count = 0;

// SQL Queries
$sql_getprod = "SELECT cpei_shipgroup.`value_id`, cpe.`entity_id`, cpei_shipgroup.`value` ship_group, cpsl.`product_id`, cpsl.`parent_id`, cpei_shipgroup_parent.`value` parent_ship_group FROM `catalog_product_entity` cpe
				INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`= cpe.`entity_id`
				INNER JOIN `catalog_product_entity` cpe_exists ON cpe_exists.`entity_id`=cpsl.`parent_id`
				LEFT JOIN `catalog_product_entity_varchar` cpei_shipgroup ON cpei_shipgroup.`entity_id` = cpe.`entity_id` AND cpei_shipgroup.`attribute_id` = 428 AND cpei_shipgroup.`store_id` = 0
				LEFT JOIN `catalog_product_entity_varchar` cpei_shipgroup_parent ON cpei_shipgroup_parent.`entity_id` = cpsl.`parent_id` AND cpei_shipgroup_parent.`attribute_id` = 428 AND cpei_shipgroup_parent.`store_id` = 0
				WHERE (IFNULL(cpei_shipgroup.`value`,0) <> IFNULL(cpei_shipgroup_parent.`value`,0))";

$results = $read->fetchAll($sql_getprod);
//echo $sql_getprod; exit;
//echo sizeof($results); exit;
foreach($results as $row) {
	$parent_val=($row['parent_ship_group']===NULL)?"NULL":$row['parent_ship_group'];

	if ($row['value_id'] !==NULL) {

		$sql_update="UPDATE `catalog_product_entity_varchar`
				SET `value`=".$parent_val." 
				WHERE `value_id`=".$row['value_id'];

		if ($debug) {
			echo '<br />' . '   *U*   '. $row['parent_id'] . ' - ' . $row['entity_id'] . ' - ' . $sql_update;
		} else {
			$write->query($sql_update);
 		}
		$update_simples_count++;

	} else {
		
		$sql_insert="INSERT INTO `catalog_product_entity_varchar` (`attribute_id`, `store_id`, `entity_id`, `value`)
		 VALUES (428, 0, " . $row['entity_id']. ", " . $parent_val . ")";

		if ($debug) {
	 		echo '<br />' . '   *I*   '. $row['entity_id'] . ' - ' . $sql_insert;
		} else {
			$write->query($sql_insert);
		}
		$insert_simples_count++;
	}

}
echo "<p>Simple Updates = $update_simples_count";
echo "<p>Simple Inserts = $insert_simples_count";


?>
