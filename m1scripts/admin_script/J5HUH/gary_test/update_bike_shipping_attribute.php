<?php

/*
G. Hall 05/08/2019
Always set the simples Shipping Group to be "BIKE" for bike attribute_set
*/

error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php');

$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$write = $read = $resource->getConnection();


umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');
//$write = $resource->getConnection('core_write');

// SQL Queries
$sql_getprod = "SELECT cpe.`entity_id` FROM `catalog_product_entity` cpe
				INNER JOIN `catalog_product_entity_int` cpei_shipping ON cpei_shipping.`entity_id`=cpe.`entity_id` AND cpei_shipping.`attribute_id`=199 AND cpei_shipping.`store_id`=0
				WHERE cpe.`attribute_set_id` IN (9) AND (cpei_shipping.`value` <> 1553 or cpei_shipping.`value` IS NULL)";

$results = $read->fetchAll($sql_getprod);

foreach($results as $row) {
	$product_id=$row['entity_id'];
	$sql_update="UPDATE `catalog_product_entity_int`
				SET `value`= 1553 
				WHERE `entity_id`=".$product_id." AND `attribute_id`=199 AND `store_id`=0";
	//echo $sql_update.'<br>';
	$write->query($sql_update);
}

echo "DONE!!"
?>


