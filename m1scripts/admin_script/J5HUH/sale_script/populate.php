<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.1


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
//require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');  //Path to Magento (WEB)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

// Get webstie ID's
//fitness = 3
//football = 2
//cycles = 1
//wildtrak = 4
$websites = array("3","2","1","4"); // Live website

$count = 0;
$count1 = 0;

// Get categorie ID's
$football_cat = array("420","422","423","424","425","426","427","2022","878","469","428","429","430","431");
$fitness_cat = array("1211","1212","1213","1214","1215","1216","1217","1218","1219","1220","1221","1222","1223");
$cycles_cat = array ("300","301","302","303","304","305","306","2035","2036","848","307","308","309","310");
$wildtrak_cat = array("1844","1853","1856","1854","1855","2033","2034","2037","2038","2039","2040","2041","2042");
$all_cat = array_merge($football_cat,$fitness_cat,$cycles_cat,$wildtrak_cat);

//print_r($all_cat);

// First we need to delete items in the category
foreach($all_cat as $cat){
	$sql_delete = "DELETE FROM `catalog_category_product` WHERE `category_id` = '".$cat."'";
	echo $sql_delete;
	$write->query($sql_delete);
	echo "<br>";
}

// The SQL Queries
foreach($websites as $website){
$sql_getprod = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,cped.value as 'price',cped1.value as 'percent',cpei.value as 'visability', cpw.website_id FROM `catalog_product_entity` cpe 
		RIGHT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id` RIGHT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id` 
		RIGHT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` RIGHT JOIN `catalog_product_website` cpw ON cpe.`entity_id`=cpw.`product_id` WHERE cped.`attribute_id` = 75 
		AND cped1.`attribute_id` = 338 AND cped1.`store_id`=0 AND cped.`store_id` = 0 AND cpei.`attribute_id` = 102 AND cpei.`store_id` = 0 AND cpei.`value` = 4 
		AND cpw.`website_id` = '".$website."' AND cped1.`value` >= '50' AND cped1.`value` <= '95' ORDER BY cped1.`value` ASC";

$sql_getprod_price = "SELECT cpe.entity_id,cpe.attribute_set_id,cpe.sku,cped.value as 'price',cped1.value as 'percent',cpei.value as 'visability', cpw.website_id FROM `catalog_product_entity` cpe 
		RIGHT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id` RIGHT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id` 
		RIGHT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` RIGHT JOIN `catalog_product_website` cpw ON cpe.`entity_id`=cpw.`product_id` WHERE cped.`attribute_id` = 75 
		AND cped1.`attribute_id` = 338 AND cped1.`store_id`=0 AND cped.`store_id` = 0 AND cpei.`attribute_id` = 102 AND cpei.`store_id` = 0 AND cpei.`value` = 4 
		AND cpw.`website_id` = '".$website."' AND cped.`value` >= '1' AND cped.`value` <= '25' ORDER BY cped.`value` ASC";

// Fetch all reqults
$results = $read->fetchAll($sql_getprod);
$results_price = $read->fetchAll($sql_getprod_price);

// Print out all results
//print_r($results);
//print_r($results_price);

// Defaults
$number = 0;
echo "<h1>Add Price Picker - Website(".$website.")</h1>";
foreach($results_price as $price){
	$cat = NULL;
	
	// find the website
	if($price['website_id']=='2'){
		//football website
		if($price['price'] == '5'){
			$cat = '2022'; // Live
		}else if($price['price'] == '10'){
			$cat = '878'; 
		}else if($price['price'] > '0' AND $price['price'] <= '5'){
			$cat = '469'; 
		}else if($price['price'] > '5' AND $price['price'] <= '10'){
			$cat = '428'; 
		}else if($price['price'] > '10' AND $price['price'] <= '20'){
			$cat = '429'; 
		}else if($price['price'] > '20' AND $price['price'] <= '30'){
			$cat = '430'; 
		}else if($price['price'] > '30' AND $price['price'] <= '40'){
			$cat = '431'; 
		}

	}else if($price['website_id']=='3'){
		//fitness website
		if($price['price'] == '5'){
			$cat = '1212';
		}else if($price['price'] == '10'){
			$cat = '1213'; 
		}else if($price['price'] >= '0' AND $price['price'] < '5'){
			$cat = '1214'; 
		}else if($price['price'] >= '5' AND $price['price'] < '10'){
			$cat = '1215'; 
		}else if($price['price'] >= '10' AND $price['price'] < '15'){
			$cat = '1216'; 
		}else if($price['price'] >= '15' AND $price['price'] < '20'){
			$cat = '1217'; 
		}else if($price['price'] >= '20' AND $price['price'] <= '25'){
			$cat = '1218'; 
		}

	}else if($price['website_id']=='1'){
		//cycles website
		if($price['price'] == '5'){
			$cat = '2035';
		}else if($price['price'] == '10'){
			$cat = '2036'; 
		}else if($price['price'] >= '0' AND $price['price'] < '5'){
			$cat = '848'; 
		}else if($price['price'] >= '5' AND $price['price'] < '10'){
			$cat = '307'; 
		}else if($price['price'] >= '10' AND $price['price'] < '15'){
			$cat = '308'; 
		}else if($price['price'] >= '15' AND $price['price'] < '20'){
			$cat = '309'; 
		}else if($price['price'] >= '20' AND $price['price'] <= '25'){
			$cat = '310'; 
		}

	}else if($price['website_id']=='4'){
		//wildtrak website
		if($price['price'] == '5'){
			$cat = '2034';
		}else if($price['price'] == '10'){
			$cat = '2037'; 
		}else if($price['price'] >= '0' AND $price['price'] < '5'){
			$cat = '2038'; 
		}else if($price['price'] >= '5' AND $price['price'] < '10'){
			$cat = '2039'; 
		}else if($price['price'] >= '10' AND $price['price'] < '15'){
			$cat = '2040'; 
		}else if($price['price'] >= '15' AND $price['price'] < '20'){
			$cat = '2041'; 
		}else if($price['price'] >= '20' AND $price['price'] <= '25'){
			$cat = '2042'; 
		}

	}
	
	// Insert Price Categories
	$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat."','".$price['entity_id']."','".$number."')";
	echo $sql_insert;exit;
	echo " - Price: " . $price['price'];
	echo " - Percent: " . $price['percent'];
	echo " - Website: " . $price['website_id'];
	echo " - Attribute: " .$price['attribute_set_id'];
	try {
		$write->query($sql_insert);
		$count1++ ;
	} catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
	echo "<br>";
	
}
echo "<h1>Add Percentage Savings - Website(".$website.")</h1>";
// Now Insert the data
foreach($results as $value){
$cat_id = NULL;
	
	// find the website
	if($value['website_id']=='2'){
		//football website
		if($value['percent'] == '50'){
			$cat_id = '422';
		}else if($value['percent'] >= '50' AND $value['percent'] < '60'){
			$cat_id = '423'; 
		}else if($value['percent'] >= '60' AND $value['percent'] < '70'){
			$cat_id = '424'; 
		}else if($value['percent'] >= '70' AND $value['percent'] < '80'){
			$cat_id = '425'; 
		}else if($value['percent'] >= '80' AND $value['percent'] < '95'){
			$cat_id = '426'; 
		}

	}else if($value['website_id']=='3'){
		//fitness website
		if($value['percent'] == '50'){
			$cat_id = '1223';
		}else if($value['percent'] >= '50' AND $value['percent'] < '60'){
			$cat_id = '1219'; 
		}else if($value['percent'] >= '60' AND $value['percent'] < '70'){
			$cat_id = '1220'; 
		}else if($value['percent'] >= '70' AND $value['percent'] < '80'){
			$cat_id = '1221'; 
		}else if($value['percent'] >= '80' AND $value['percent'] < '95'){
			$cat_id = '1222'; 
		}

	}else if($value['website_id']=='1'){
		//cycles website
		if($value['percent'] == '50'){
			$cat_id = '301';
		}else if($value['percent'] >= '50' AND $value['percent'] < '60'){
			$cat_id = '302'; 
		}else if($value['percent'] >= '60' AND $value['percent'] < '70'){
			$cat_id = '303'; 
		}else if($value['percent'] >= '70' AND $value['percent'] < '80'){
			$cat_id = '304'; 
		}else if($value['percent'] >= '80' AND $value['percent'] < '95'){
			$cat_id = '305'; 
		}

	}else if($value['website_id']=='4'){
		//wildtrak website
		if($value['percent'] == '50'){
			$cat_id = '1853';
		}else if($value['percent'] >= '50' AND $value['percent'] < '60'){
			$cat_id = '1856'; 
		}else if($value['percent'] >= '60' AND $value['percent'] < '70'){
			$cat_id = '1854'; 
		}else if($value['percent'] >= '70' AND $value['percent'] < '80'){
			$cat_id = '1855'; 
		}else if($value['percent'] >= '80' AND $value['percent'] < '95'){
			$cat_id = '2033'; 
		}
	}

	// Insert Percentage Savings
	$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat_id."','".$value['entity_id']."','".$number."')";
	echo $sql_insert;exit;
	echo " - Price: " . $value['price'];
	echo " - Percent: " . $value['percent'];
	echo " - Website: " . $value['website_id'];
	try {
		$write->query($sql_insert);
		echo "<br>";
	}catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}

	$count++ ;
}

}

$html = "Completed successfully <br>";
$html .= "Moved Into Price Cat (".$count1.") - Moved Into Percentage Cat (".$count.")"; 

echo $html;

//Receiver Details
$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToName = 'Magento Server';

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), Mage::getStoreConfig('trans_email/ident_general/name'));
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Sales Script Ran '.date("F j, Y, g:i a"));
			$mail->send();


?>
