<?php
// Start Group Script To Auto Populate New Sale Pages
// Designed and built by RW
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
//require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');  //Path to Magento (WEB)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');
$websites = array("3"); // Live website
$source = "Start Fitness";

$sourcec = "Start Cycles";
$sourcef = "Start Football";

// !!! TEST IDS !!!
/*$categories = array(
	"ladiesclothing" 	=> "1695",
	"ladiesfootwear" 	=> "1694",
	"mensclothing" 		=> "1693",
	"mensfootwear" 		=> "1692",
	"junior" 			=> "1697",
	"acessories" 		=> "1696",
);*/

// SELECTING CATEGORIES
$categories = array(
	"ladiesclothing" 	=> "2631",
	"ladiesfootwear" 	=> "2632",
	"mensclothing" 		=> "2633",
	"mensfootwear" 		=> "2634",
	"junior" 			=> "2635",
	"acessories" 		=> "2636",
	"cycle"				=> "315",
	"cycleclothing"		=> "1705",
	"boots"				=> "436",
	"footballclothing"	=> "433",
);
$categoriesstr = ""; foreach($categories as $cats){$categoriesstr.="'{$cats}',";} $categoriesstr = rtrim($categoriesstr, ",");

// DEFINING ATTRIBUTE SETS TO FILTER DOWN TO 
$attributesets = array("'Accessories'","'Clothing'", "'Footwear'", "'Bras'", "'StartBikes'", "'CycleClothing'" );
$attributesetsstr = implode(",", $attributesets);
$count = 0;
$success_count = 0;
$sql_insert="";

// DELETE OLD DATA
$sql_delete = "DELETE FROM `catalog_category_product` WHERE `category_id` IN ({$categoriesstr})\n";
echo $sql_delete;
try{
	if($categoriesstr!="") $write->query($sql_delete);
}
catch(Exception $e){
	
}
//exit;

// LOOP THROUGH ALL WEBSITES
foreach($websites as $website){
	// GET ALL PRODUCTS THAT ARE VISIBLE, TURNED ON, THAT ARE PART OF THE ABOVE ATTRIBUTE SETS AND >= 50% SAVINGS
	$sql = "SELECT pe.*, aov.value as gender, aovs.value as sourcename, eas.attribute_set_name as setname, pev.value as saving FROM `catalog_product_entity` pe
JOIN `catalog_product_website` cpw ON cpw.product_id = pe.entity_id AND cpw.website_id = {$website}
JOIN catalog_product_entity_decimal pev ON pev.attribute_id = 338 AND pev.entity_id = pe.entity_id AND pev.store_id = 0 AND pev.value is not null AND pev.value >= 50
JOIN catalog_product_entity_int peen ON peen.attribute_id = 96 AND peen.entity_id = pe.entity_id AND peen.store_id = 0 AND peen.value = 1
JOIN catalog_product_entity_int vis ON vis.attribute_id = 102 AND vis.entity_id = pe.entity_id AND vis.store_id = 0
LEFT JOIN catalog_product_entity_varchar pei ON pei.attribute_id = 141 AND pei.entity_id = pe.entity_id AND pei.store_id = 0
LEFT JOIN `eav_attribute_option_value` aov ON aov.`option_id` = pei.value AND aov.store_id = 0
LEFT JOIN catalog_product_entity_int peis ON peis.attribute_id = 343 AND peis.entity_id = pe.entity_id AND peis.store_id = 0
LEFT JOIN `eav_attribute_option_value` aovs ON aovs.`option_id` = peis.value AND aovs.store_id = 0
LEFT JOIN `eav_attribute_set`eas ON eas.attribute_set_id = pe.attribute_set_id
LEFT JOIN catalog_product_entity_int pevi ON pevi.attribute_id = 102 AND pevi.entity_id = pe.entity_id AND pevi.store_id = 0 AND pevi.value != 1
WHERE eas.attribute_set_name IN ({$attributesetsstr}) AND vis.value = 4";

	$results = $read->fetchAll($sql);
	
	// LOOP THROUGH ALL PRODUCTS
	foreach($results as $product){
		$attribute_set = $product['setname'];
		$gender = $product['gender'];
		$thissource = $product['sourcename'];
		$cat2add = null;
		
		if($source != $thissource) continue;
		// LOGIC TO DECIDE WHICH CATEGORY THE PRODUCT WILL FALL UNDER
		if($gender == "Womens" && ($attribute_set == "Clothing" || $attribute_set == "Bras")){
			$cat2add = $categories['ladiesclothing'];
		}
		elseif($gender == "Womens" && $attribute_set == "Footwear"){
			$cat2add = $categories['ladiesfootwear'];
		}
		elseif($gender == "Mens" && $attribute_set == "Clothing"){
			$cat2add = $categories['mensclothing'];
		}
		elseif($gender == "Mens" && $attribute_set == "Footwear"){
			$cat2add = $categories['mensfootwear'];
		}
		elseif($gender == "Unisex" && $attribute_set == "Footwear"){
			$cat2add = array($categories['mensfootwear'],$categories['ladiesfootwear']);
		}
		elseif($gender == "Junior" && $attribute_set != "Accessories"){
			$cat2add = $categories['junior'];
		}
		elseif( $attribute_set == "Accessories"){
			$cat2add = $categories['acessories'];
		}
		
		// DO THE INSERT (ONLY IF A CAT WAS FOUND)
		if($cat2add != null && !is_array($cat2add)){
			$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."');\n";
			try {
				echo "$sql_insert";
				$write->query($sql_insert);
				$success_count++ ;
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			$count++;
		}
		elseif($cat2add != null && is_array($cat2add)){
			foreach($cat2add as $subcat2add){
				$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$subcat2add."','".$product['entity_id']."','".$count."');\n";
				try {
					echo "$sql_insert";
					$write->query($sql_insert);
					$success_count++ ;
				} catch (Exception $e) {
					echo 'Caught exception: ',  $e->getMessage(), "\n";
				}
			}
			$count++;
		}
		else{
			$count++;
		}
		
	}
	
	// FOOTBAll sSTUFF
	$sql_delete3 = "DELETE FROM `catalog_category_product_index` 
	WHERE `category_id` IN ({$categories['footballclothing']},{$categories['boots']})
	AND store_id IN (6, 19)\n";
	$write->query($sql_delete3);//var_dump($sql_delete2);exit;
	$count = 0;
	foreach($results as $product){
		$attribute_set = $product['setname'];
		$gender = $product['gender'];
		$thissource = $product['sourcename'];
		$cat2add = null;
		
		if($sourcef != $thissource) continue;
		// LOGIC TO DECIDE WHICH CATEGORY THE PRODUCT WILL FALL UNDER
		if($attribute_set == "Clothing"){
			$cat2add = $categories['footballclothing'];
		}
		
		elseif($attribute_set == "Footwear"){
			$cat2add = $categories['boots'];
		}
		
		
		// DO THE INSERT (ONLY IF A CAT WAS FOUND)
		if($cat2add != null && !is_array($cat2add)){
			$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."');\n";
			$sql_insert2 = "INSERT INTO `catalog_category_product_index` (`category_id`,`product_id`,`position`, `store_id`, `visibility`, is_parent) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."', 6, 4, 1);\n";
			$sql_insert3 = "INSERT INTO `catalog_category_product_index` (`category_id`,`product_id`,`position`, `store_id`, `visibility`, is_parent) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."', 19, 4, 1);\n";
			try {
				echo "$sql_insert";
				$write->query($sql_insert);
				$write->query($sql_insert2);
				$write->query($sql_insert3);
				$success_count++ ;
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			$count++;
		}
		
		else{
			$count++;
		}
		
	}
	
	// CYCLE sSTUFF
	// DELETE OLD DATA
	
	$sql = "SELECT pe.*, aov.value as gender, aovs.value as sourcename, eas.attribute_set_name as setname, pev.value as saving FROM `catalog_product_entity` pe
JOIN `catalog_product_website` cpw ON cpw.product_id = pe.entity_id AND cpw.website_id = {$website}
JOIN catalog_product_entity_decimal pev ON pev.attribute_id = 338 AND pev.entity_id = pe.entity_id AND pev.store_id = 0 AND pev.value is not null AND pev.value >= 30
JOIN catalog_product_entity_int peen ON peen.attribute_id = 96 AND peen.entity_id = pe.entity_id AND peen.store_id = 0 AND peen.value = 1
JOIN catalog_product_entity_int vis ON vis.attribute_id = 102 AND vis.entity_id = pe.entity_id AND vis.store_id = 0
LEFT JOIN catalog_product_entity_int pei ON pei.attribute_id = 141 AND pei.entity_id = pe.entity_id AND pei.store_id = 0
LEFT JOIN `eav_attribute_option_value` aov ON aov.`option_id` = pei.value AND aov.store_id = 0
LEFT JOIN catalog_product_entity_int peis ON peis.attribute_id = 343 AND peis.entity_id = pe.entity_id AND peis.store_id = 0
LEFT JOIN `eav_attribute_option_value` aovs ON aovs.`option_id` = peis.value AND aovs.store_id = 0
LEFT JOIN `eav_attribute_set`eas ON eas.attribute_set_id = pe.attribute_set_id
LEFT JOIN catalog_product_entity_int pevi ON pevi.attribute_id = 102 AND pevi.entity_id = pe.entity_id AND pevi.store_id = 0 AND pevi.value != 1
WHERE eas.attribute_set_name IN ({$attributesetsstr}) AND vis.value = 4";

	$results = $read->fetchAll($sql);
	$sql_delete2 = "DELETE FROM `catalog_category_product_index` 
	WHERE `category_id` IN ({$categories['cycleclothing']},{$categories['cycle']})
	AND store_id IN (6, 19)\n";
	$write->query($sql_delete2);//var_dump($sql_delete2);exit;
	$count = 0;
	foreach($results as $product){
		$attribute_set = $product['setname'];
		$gender = $product['gender'];
		$thissource = $product['sourcename'];
		$cat2add = null;
		
		if($sourcec != $thissource) continue;
		// LOGIC TO DECIDE WHICH CATEGORY THE PRODUCT WILL FALL UNDER
		if($attribute_set == "Clothing" || $attribute_set == "CycleClothing"){
			$cat2add = $categories['cycleclothing'];
		}
		
		elseif($attribute_set == "StartBikes"){
			$cat2add = $categories['cycle'];
		}
		
		
		// DO THE INSERT (ONLY IF A CAT WAS FOUND)
		if($cat2add != null && !is_array($cat2add)){
			$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."');\n";
			$sql_insert2 = "INSERT INTO `catalog_category_product_index` (`category_id`,`product_id`,`position`, `store_id`, `visibility`, is_parent) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."', 6, 4, 1);\n";
			$sql_insert3 = "INSERT INTO `catalog_category_product_index` (`category_id`,`product_id`,`position`, `store_id`, `visibility`, is_parent) VALUES ('".$cat2add."','".$product['entity_id']."','".$count."', 19, 4, 1);\n";
			try {
				echo "$sql_insert";
				$write->query($sql_insert);
				$write->query($sql_insert2);
				$write->query($sql_insert3);
				$success_count++ ;
			} catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";
			}
			$count++;
		}
		
		else{
			$count++;
		}
		
	}
	
}
