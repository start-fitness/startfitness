<?php
require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
umask(0);
Mage::app();

$storeId = 5;
Mage::app()->setCurrentStore($storeId);

ini_set('memory_limit','1500M');
ini_set('display_errors',1);
error_reporting(E_ALL);

$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', array('eq' => 'configurable'));

$store = Mage::app()->getStore();

foreach ($collectionConfigurable as $_configurableproduct) {

    // Load product by product id

	$model = "N/A";
    
    $product = Mage::getModel('catalog/product')->load($_configurableproduct->getId());
	//$model = Mage::getModel('catalog/product')->load($_configurableproduct->getId())->getAttributeText('actual_model');
	$model = $product->getResource()->getAttribute('actual_model')->getFrontend()->getValue($product);

    
    // Only process product if it is available (saleable)
    


        // Get children products (all associated children products data)

        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);

        $stockItem = $product->getStockItem();
        
           // All configurable products, which are out of stock

            $outofstock_childrenisinstock = false;

		$current_count = 0;

            foreach ($childProducts as $childProduct) {
                $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($childProduct)->getQty();
			$current_count += $qty;

                if ($qty > 0) {
                    $outofstock_childrenisinstock = true;
                }
            }

            if ($current_count >= '1' && $current_count <= '5') {
                echo $product->getName()."~".$model."~".$current_count."</br>\r\n";
            }
   
}
?>