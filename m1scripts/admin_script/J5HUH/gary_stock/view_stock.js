$(document).ready(function(){
 $("#model").bind("change", function() {
     $.ajax({
         type: "GET", 
         url: "results.php",
         data: "model="+$("#model").val(),
         success: function(html) {
             $("#results").html(html);
         }
     });
 });

 $("#website").bind("change", function() {
     $.ajax({
         type: "GET", 
         url: "store_menu.php",
         data: "website="+$("#website").val(),
         success: function(html) {
             $("#store").html(html);
         }
     });
     $.ajax({
         type: "GET", 
         url: "results.php",
         data: "model="+$("#model").val(),
         success: function(html) {
             $("#results").html(html);
         }
     });

	 $("#store1").show();
 });
 $("#toggle_entity_id").click(function() {
	if ($(this).html()=="Entity OFF") {
		$(this).html("Entity ON");
	} else {
		$(this).html("Entity OFF");
	}
	$(".header_entity_id").toggle();
	$(".data_entity_id").toggle();
 });

 $("#toggle_name").click(function() {
	if ($(this).html()=="Name OFF") {
		$(this).html("Name ON");
	} else {
		$(this).html("Name OFF");
	}
	$(".header_name").toggle();
	$(".data_name").toggle();
 });

 $("#toggle_bin").click(function() {
	if ($(this).html()=="Bin OFF") {
		$(this).html("Bin ON");
	} else {
		$(this).html("Bin OFF");
	}
	$(".header_bin").toggle();
	$(".data_bin").toggle();
 });

 $("#toggle_rrp").click(function() {
	if ($(this).html()=="RRP OFF") {
		$(this).html("RRP ON");
	} else {
		$(this).html("RRP OFF");
	}
	$(".header_rrp").toggle();
	$(".data_rrp").toggle();
 });

 $("#toggle_price").click(function() {
	if ($(this).html()=="Price OFF") {
		$(this).html("Price ON");
	} else {
		$(this).html("Price OFF");
	}
	$(".header_price").toggle();
	$(".data_price").toggle();
 });

 $("#toggle_model").click(function() {
	if ($(this).html()=="Model OFF") {
		$(this).html("Model ON");
	} else {
		$(this).html("Model OFF");
	}
	$(".header_model").toggle();
	$(".data_model").toggle();
 });

 $("#toggle_qty").click(function() {
	if ($(this).html()=="Qty OFF") {
		$(this).html("Qty ON");
		$(".header_qty").fadeOut(3000);
		$(".data_qty").fadeOut(3000);
	} else {
		$(this).html("Qty OFF");
		$(".header_qty").fadeIn();
		$(".data_qty").fadeIn();
	}
 });
});
