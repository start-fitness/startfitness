<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

// Get webstie ID's
//fitness = 3
//football = 2
//cycles = 1
//wildtrak = 4
$websites = array("3","2","1","4"); // Live website

$count = 0;
$count_correct = 0;
$count1 = 0;

// The SQL Queries
$sql_getprod = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,cped.value as 'price',cped2.value as 'RRP',cped3.value AS 'special', 
			cped3df.value AS 'special_from', cped3dt.value AS 'special_to', cped1.value as 'percent',cpei.value as 'visability', csi.is_in_stock 
			FROM `catalog_product_entity` cpe 
            LEFT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id` AND cped.`attribute_id` = 75 AND cped.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id` AND cped1.`attribute_id` = 338 AND cped1.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_decimal` cped2 ON cpe.`entity_id`=cped2.`entity_id` AND cped2.`attribute_id`= 120 AND cped2.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_decimal` cped3 ON cpe.`entity_id`=cped3.`entity_id` AND cped3.`attribute_id`= 76 AND cped3.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_datetime` cped3df ON cpe.`entity_id`=cped3df.`entity_id` AND cped3df.`attribute_id`= 77 AND cped3df.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_datetime` cped3dt ON cpe.`entity_id`=cped3dt.`entity_id` AND cped3dt.`attribute_id`= 78  AND cped3dt.`store_id`= 0 
			LEFT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` AND cpei.`attribute_id` = 102 AND cpei.`store_id`= 0 
			LEFT JOIN `cataloginventory_stock_item` csi ON cpe.`entity_id`=csi.`product_id`
			WHERE 
			 csi.`is_in_stock` = 1
            /*AND cpe.entity_id = 385933*/
             AND cped.value IS NOT NULL
ORDER BY `cpe`.`entity_id`  DESC";

// Fetch all reqults
$results = $read->fetchAll($sql_getprod);

// Print out all results
//print_r($results);

// Defaults
$number = 0;
$rrpset = 0;
$html = '';
// Set the table up to display issue products - if any
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<center><h2>Product Issue Table</h2></center>';
$html .= '<table style="border:1px #000 solid;" width="600" border="1" cellspacing="2" cellpadding="2"><tr><th>ID</th><th>Model #</th><th>Price</th><th>RRP</th></tr>';

// First delete any none default values
$sql_delete = "DELETE FROM `catalog_product_entity_decimal` WHERE `attribute_id` = 338 AND store_id != 0";
$read->query($sql_delete);

// Now Insert the data
foreach($results as $value){
	if($value['special'] != null){
		if($value['special_from']==null && $value['special_to']==null){
			if(is_numeric($value['special'])) $value['price'] = $value['special'];
		}
		else{
			$datenow = strtotime(date("Y-m-d 00:00:00"));
			$datefrom = strtotime($value['special_from']);
			$dateto = strtotime($value['special_to']);
			$iswithintime = true;
			if($datefrom != false && $datefrom >$datenow) $iswithintime = false;
			if($dateto != false && $dateto < $datenow) $iswithintime = false;
			
			if($iswithintime){
				if(is_numeric($value['special'])) $value['price'] = $value['special'];
			}
		}
		
	}
	$count++;
	
	if($value['RRP'] == NULL){ // No RRP Set
		$html .= '<tr><td>'.$value['entity_id'].'</td><td>'.$value['sku'].'</td><td>'.number_format($value['price'],2).'</td><td>-</td></tr>';
		$rrpset++;
	}else{ // Else work out the RRP/Percentage Savings
		$savings_percentagenonrounded = (($value['RRP'] - $value['price'])/$value['RRP'])*100;
		$savings_percentage = round((($value['RRP'] - $value['price'])/$value['RRP'])*100, 0 );
		$current_percentage = round($value['percent'], 0);
		if($savings_percentage == 0 && $savings_percentagenonrounded > 0 && $savings_percentagenonrounded < 1) $savings_percentage = 1;
	}
	
	//var_dump($savings_percentage,$current_percentage );

	if(round($savings_percentage,0) == number_format($current_percentage,2)){ // If the current percent is fine, skip it
		$count_correct++;
		continue;
	}else if(round($savings_percentage) <= 0){ // Product Price bigger than RRP
		$savings_percentage = 0.00;
		$html .= '<tr><td>'.$value['entity_id'].'</td><td>'.$value['sku'].'</td><td>'.number_format($value['price'],2).'</td><td>�'.number_format($value['RRP'],2).'</td></tr>';
		$rrpset++;
		$count1--;
	}else if($savings_percentage != $current_percentage){ // These Percentages need updating
		$savings_percentage = $savings_percentage;
	}
	// Insert Percentage Savings
	$sql_insert2 = "INSERT IGNORE INTO `catalog_product_entity_decimal` VALUES( null, 338,0,{$value['entity_id']}, '".$savings_percentage."' ); ";
	$sql_insert = "UPDATE `catalog_product_entity_decimal` SET `value` = '".$savings_percentage."' WHERE `attribute_id` = 338 AND `store_id` = 0 AND `entity_id` = ".$value['entity_id']." ";
	echo $sql_insert . "\n";//exit;
	
	

	$read->query($sql_insert2); // write updates to database
	$read->query($sql_insert); // write updates to database
	$count1++ ;
}

$read->query(
"INSERT IGNORE INTO catalog_product_entity_decimal
SELECT null, 338, 0, innerq.parent_id , innerq.percsave FROM
(
	SELECT parent_id, MAX(perc.value) as percsave, MAX(perc1.value) as percsave2 FROM `catalog_product_relation`
    JOIN catalog_product_entity_decimal perc on perc.attribute_id = 338 AND perc.entity_id = child_id AND perc.store_id = 0
    LEFT JOIN catalog_product_entity_decimal perc1 on perc1.attribute_id = 338 AND perc1.entity_id = parent_id AND perc1.store_id = 0
    GROUP BY parent_id
    HAVING  NOT MAX(perc.value) <=> MAX(perc1.value)
) as innerq"

);

$read->query(
"UPDATE catalog_product_entity_decimal
JOIN
(
	SELECT parent_id, MAX(perc.value) as percsave, MAX(perc1.value) as percsave2 FROM `catalog_product_relation`
    JOIN catalog_product_entity_decimal perc on perc.attribute_id = 338 AND perc.entity_id = child_id AND perc.store_id = 0
    LEFT JOIN catalog_product_entity_decimal perc1 on perc1.attribute_id = 338 AND perc1.entity_id = parent_id AND perc1.store_id = 0
    GROUP BY parent_id
    HAVING  NOT MAX(perc.value) <=> MAX(perc1.value)
) as innerq on innerq.parent_id = catalog_product_entity_decimal.entity_id AND catalog_product_entity_decimal.attribute_id = 338 AND catalog_product_entity_decimal.store_id = 0

SET catalog_product_entity_decimal.value = innerq.percsave");
// If all RRPs are fine - show this cell
if($rrpset == '0'){ $html .= '<tr><td colspan="4" align="center">No Problem Products</td></tr>'; }

$html .= '</table>';

$html .= "Total Active Products (".$count.") - No Update Required (".$count_correct.") - Updated Percentages (".$count1.")"; 
$html .= "</table>";
echo $html;

/*//Receiver Details
$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gina@startfitness.co.uk','lori@startfitness.co.uk');
$sendToName = 'Magento Server';

$mail = new \Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Percentage Savings Cron');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Percentage Savings '.date("F j, Y, g:i a"));
			$mail->send();

echo "Completed successfully <br>";
*/
?>
