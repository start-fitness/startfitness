<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


// Get webstie ID's
$fitness = '6,19,8,27';
//$sportssize = '28';
//$football = '5,20';
//$cycles = '1,21';
//$wildtrak = '7';
//$moremile = '8';
//$amazon_uk = '24';
//$amazon_us = '26';
//$ebay = '25';
$websites = array("$fitness");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$weekend = date("Y-m-d",strtotime('-3 days')); // Date before the weekend

if($day=='1'){
	// Day is monday so get weekend sales
	$title = "Weekend Sales Report - By Quantity";
	$date_from = $weekend;
	$date_to = $today;
}else{
	// Else every other day of the working week
	$title = "30 Day Sales Report - By Quantity";
	$date_from = $yesterday;
	$date_to = $today;
}

// override dates
        $title = "30 Day Sales Report - By Quantity";
        $date_from = date("Y-m-d",strtotime('-31 days'));
        $date_to =  date("Y-m-d",strtotime('-1 days'));


$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';
$html .= '<center><p>'.$title.' Report run from '.$date_from.' 0:00am till '.$date_to.' 23:59pm</p></center>';
// The SQL Queries
foreach($websites as $website){

// The website names
if($website == $fitness){ $web_name = "Start Fitness"; }
//if($website == $sportssize){ $web_name = "Sports Size"; }
//if($website == $football){$web_name = "Start Football"; }
//if($website == $cycles){ $web_name = "Start Cycles"; }
//if($website == $wildtrak){ $web_name = "WildTrak"; }
if($website == $moremile){ $web_name = "More Mile"; }
if($website == $amazon_uk){ $web_name = "Amazon UK"; }
if($website == $amazon_us){ $web_name = "Amazon US"; }
if($website == $ebay){ $web_name = "eBay"; }

// Get Data
$sql_getsales = "SELECT `cpev_model`.`value` as 'Model',`cpev_name`.`value` as 'Name',`tmp`.`Qty_Sold` 
		  FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(
		  SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` 
		  ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold 
		  FROM `sales_flat_order_item` sfoi LEFT JOIN `sales_flat_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` WHERE `sfo`.`store_id` IN (".$website.") AND sfo.`status` NOT IN ('pending_payment','canceled')
		  AND `sfoi`.`product_type` = 'simple' AND (`sfo`.`created_at` >= '".$date_from." 00:00:00' AND `sfo`.`created_at` <= '".$date_to." 23:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) 
		  GROUP BY `parent_id` ORDER BY Qty_Sold Desc) tmp LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 
		  LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 WHERE `tmp`.`Qty_Sold` >0 LIMIT 100";

$sql_getnumorders = "SELECT count(entity_id) as 'total_orders' FROM `sales_flat_order` WHERE `created_at` >= '".$date_from." 00:00:00' AND `created_at` <= '".$date_to." 23:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending_payment','canceled')";
$sql_getammount = "SELECT sum(base_grand_total) FROM `sales_flat_order` WHERE `created_at` >= '".$date_from." 00:00:00' AND `created_at` <= '".$date_to." 23:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending_payment','canceled')";
// echo $sql_getsales; exit;
// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);
$results_orders = $read->fetchOne($sql_getnumorders);
$results_ammount = $read->fetchOne($sql_getammount);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$web_name.' '.$title.'</h1>';
$html .= '<p><b>Total Orders</b>: ('.$results_orders.') - <b>Total Takings</b>: (�'.number_format($results_ammount,2).')</p>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('30 Day Sales Report '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";






// Get webstie ID's
$fitness = '6,19,8,27';
//$sportssize = '28';
//$football = '5,20';
//$cycles = '1,21';
//$wildtrak = '7';
//$moremile = '8';
//$amazon_uk = '24';
//$amazon_us = '26';
//$ebay = '25';
$websites = array("$fitness");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$weekend = date("Y-m-d",strtotime('-3 days')); // Date before the weekend

if($day=='1'){
	// Day is monday so get weekend sales
	$title = "Weekend Sales Report - By Value";
	$date_from = $weekend;
	$date_to = $today;
}else{
	// Else every other day of the working week
	$title = "30 Day Sales Report - By Value";
	$date_from = $yesterday;
	$date_to = $today;
}

// override dates
        $title = "30 Day Sales Report - By Value";
        $date_from = date("Y-m-d",strtotime('-31 days'));
        $date_to =  date("Y-m-d",strtotime('-1 days'));


$count = 0;
$html = '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';
$html .= '<center><p>'.$title.' Report run from '.$date_from.' 0:00am till '.$date_to.' 23:59pm</p></center>';
// The SQL Queries
foreach($websites as $website){

// The website names
if($website == $fitness){ $web_name = "Start Fitness"; }
//if($website == $sportssize){ $web_name = "Sports Size"; }
//if($website == $football){$web_name = "Start Football"; }
//if($website == $cycles){ $web_name = "Start Cycles"; }
//if($website == $wildtrak){ $web_name = "WildTrak"; }
if($website == $moremile){ $web_name = "More Mile"; }
if($website == $amazon_uk){ $web_name = "Amazon UK"; }
if($website == $amazon_us){ $web_name = "Amazon US"; }
if($website == $ebay){ $web_name = "eBay"; }

// Get Data
$sql_getsales = "SELECT tmp.`Qty_Sold`, tmp.`Qty_Sold`*tmp.`Value` as 'Value', cpev_name.`value` as 'Name', cpev_model.`value` as 'Model' FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,( SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold, cped_price.`value` as 'Value' FROM `sales_flat_order_item` sfoi LEFT JOIN `sales_flat_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` LEFT JOIN `catalog_product_entity_decimal` cped_price ON `cped_price`.`entity_id` = `sfoi`.`product_id` and `cped_price`.`attribute_id`=75 AND `cped_price`.`store_id`=0 WHERE `sfo`.`store_id` IN (".$website.") AND `sfoi`.`product_type` = 'simple' AND (`sfo`.`created_at` >= '".$date_from." 00:00:00' AND `sfo`.`created_at` <= '".$date_to." 23:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) AND sfo.`status` NOT IN ('pending_payment','canceled') GROUP BY `parent_id`  ORDER BY `Value` DESC) tmp
	LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 ORDER BY `Value` DESC LIMIT 100";

//echo $sql_getsales; exit;

$sql_getnumorders = "SELECT count(entity_id) as 'total_orders' FROM `sales_flat_order` WHERE `created_at` >= '".$date_from." 00:00:00' AND `created_at` <= '".$date_to." 23:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending_payment','canceled')";
$sql_getammount = "SELECT sum(base_grand_total) FROM `sales_flat_order` WHERE `created_at` >= '".$date_from." 00:00:00' AND `created_at` <= '".$date_to." 23:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending_payment','canceled')";

// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);
$results_orders = $read->fetchOne($sql_getnumorders);
$results_ammount = $read->fetchOne($sql_getammount);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$web_name.' '.$title.'</h1>';
$html .= '<p><b>Total Orders</b>: ('.$results_orders.') - <b>Total Takings</b>: (�'.number_format($results_ammount,2).')</p>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th><th>Value</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td><td align="center">'.number_format($home['Value'],2).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('30 Day Sales Report  - By Value '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>
