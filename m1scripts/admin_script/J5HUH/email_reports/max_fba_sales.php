<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 0);
//error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
//require_once('/microcloud/domains/fitnes/domains/admin.startfitness.co.uk/http/app/Mage.php'); //Path to Magento

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format

$title = "Amazon FBA Sales for $yesterday";

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}
table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }</style>';
// The SQL Queries

// Get Data
$sql_getsales = "SELECT sii.`product_id`, cpev_model.`value` `model`, cpev_name.`value` `name`, sum(`qty`) `qty_sold` FROM `sales_invoice_item` sii
INNER JOIN `sales_invoice` si ON si.`entity_id` = sii.`parent_id`
INNER JOIN `sales_order` so ON so.`entity_id` = si.`order_id`
LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = sii.`product_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 
LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = sii.`product_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 
WHERE DATE(si.`created_at`) = '".$yesterday."' AND si.`store_id` = 24 AND so.`increment_id` LIKE 'AMZFBA%' 
GROUP BY sii.`product_id`";

//echo $sql_getsales; exit;
// Fetch all reqults
$results = $read->fetchAll($sql_getsales);
print_r($results); 

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$title.'</h1>';

$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Product ID</th><th align="left">Model</th><th>Name</th><th>Qty</th></tr>';
foreach($results as $home){
	$html .= '<tr><td><a href="https://admin.startfitness.co.uk/admin/catalog/product/edit/id/'.$home['product_id'].'/">'.$home['product_id'].'</a></td><td>'.$home['model'].'</td><td>'.$home['name'].'</td><td align="center">'.number_format($home['qty_sold'],0).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk','max@startfitness.co.uk');
//$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk','kieranreay@startfitness.co.uk','danielmeggison@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Magento '.$title);
			//$mail->setFrom("customerservices@startfitness.co.uk");
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily FBA Sales Report '.$yesterday);
			$mail->send();

echo $html;
echo "Success";

?>
