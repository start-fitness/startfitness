<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 0);
//error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
//require_once('/microcloud/domains/fitnes/domains/admin.startfitness.co.uk/http/app/Mage.php'); //Path to Magento

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$today = date("Y-m-d");

$title = "Store Credit Report - Since: ".$yesterday;

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}
table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }</style>';
// The SQL Queries

// Get Data
$sql_getsales = "select so.`entity_id`, so.`created_at`, so.`increment_id`, so.`customer_email`, so.`grand_total`, so.`order_currency_code`, so.`amstorecredit_base_amount`, acs.`store_credit` from `sales_order` so 
INNER JOIN `sales_invoice` si ON si.`order_id` = so.`entity_id`
LEFT JOIN `amasty_store_credit` acs ON acs.`customer_id` = so.`customer_id`
WHERE so.`amstorecredit_base_amount` IS NOT NULL AND DATE(so.`created_at`) BETWEEN '".$yesterday."' AND '".$today."'
ORDER BY so.`created_at` DESC";

//echo $sql_getsales; exit;
// Fetch all reqults
$results = $read->fetchAll($sql_getsales);
//if(empty($results)) exit;
//print_r($results); 

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">Sales - '.$title.'</h1>';

$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Order Date</th><th>Order #</th><th>Email</th><th>Order Total</th><th>Currency Code</th><th>Store Credit Used</th><th>Store Credit Remaining</th></tr>';
foreach($results as $home){
	$html .= '<tr><td>'.$home['created_at'].'</td><td><a href="https://admin.startfitness.co.uk/admin/sales/order/view/order_id/'.$home['entity_id'].'/">'.$home['increment_id'].'</a></td><td>'.$home['customer_email'].'</td><td>'.$home['grand_total'].'</td><td>'.$home['order_currency_code'].'</td><td>'.$home['amstorecredit_base_amount'].'</td><td>'.$home['store_credit'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';

// START

$sql_getcredits = "select so.`entity_id`, scm.`created_at`, so.`increment_id`, so.`customer_email`, so.`grand_total`, so.`order_currency_code`, scm.`amstorecredit_base_amount`, acs.`store_credit` from `sales_order` so 
INNER JOIN `sales_creditmemo` scm ON scm.`order_id` = so.`entity_id`
LEFT JOIN `amasty_store_credit` acs ON acs.`customer_id` = so.`customer_id`
WHERE scm.`amstorecredit_base_amount` IS NOT NULL AND scm.`amstorecredit_base_amount` !=0 AND DATE(scm.`created_at`) BETWEEN '".$yesterday."' AND '".$today."'
ORDER BY scm.`created_at` DESC";

//echo $sql_getsales; exit;
// Fetch all reqults
$results_credits = $read->fetchAll($sql_getcredits);
if(empty($results_credits) && empty($results)) exit;
//print_r($results); 

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">Refunds - '.$title.'</h1>';

$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Order Date</th><th>Order #</th><th>Email</th><th>Order Total</th><th>Currency Code</th><th>Store Credit Used</th><th>Store Credit Remaining</th></tr>';
foreach($results_credits as $home){
	$html .= '<tr><td>'.$home['created_at'].'</td><td><a href="https://admin.startfitness.co.uk/admin/sales/order/view/order_id/'.$home['entity_id'].'/">'.$home['increment_id'].'</a></td><td>'.$home['customer_email'].'</td><td>'.$home['grand_total'].'</td><td>'.$home['order_currency_code'].'</td><td>'.$home['amstorecredit_base_amount'].'</td><td>'.$home['store_credit'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';

// END





//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk','carlsmith@startfitness.co.uk');
//$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk','kieranreay@startfitness.co.uk','danielmeggison@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Magento '.$title);
			//$mail->setFrom("customerservices@startfitness.co.uk");
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Store Credit Report - Since: '.$yesterday);
			$mail->send();

echo $html;
echo "Success";

?>
