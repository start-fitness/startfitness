<?php
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../../app/bootstrap.php';
$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();
error_reporting(E_ALL & ~E_NOTICE);

$today = date("Y-m-d"); // Todays Date mysql format
$last_week = date("Y-m-d", strtotime("-2 weeks")); // Last Week Date mysql format
$count = 0;
$dateovverride = date("Y-m-d", strtotime("-1 days"));
//$dateovverride = "2020-11-09";

// Get Data
$sql_getdata = "SELECT 

####Header
date(sfo.created_at) as 'Order Date', date(sfi.created_at) as 'Invoice Date', m2ao.amazon_order_id , sfo.increment_id as 'Order Number', sfi.increment_id as 'Invoice Number', 
sfo.store_name as 'Store Name', SUBSTRING_INDEX(sfo.customer_email, '@', -1) as 'Substore', sfo.customer_email, sfo.order_currency_code, sfoa.country_id,

####Goods Info
round(sfi.base_subtotal_incl_tax-dv.base_tax_after_discount_noshipping+sfi.base_discount_amount,2) as '4000: Net Goods',
round(dv.base_tax_after_discount_noshipping,2) as '2200: Goods VAT',
round(sfi.base_subtotal_incl_tax+sfi.base_discount_amount,2) as 'Gross Goods', 

####Shipping info
round(sfi.base_shipping_incl_tax-(dv.base_tax_after_discount-dv.base_tax_after_discount_noshipping),2) as '4040: Net Shipping', 
round(dv.base_tax_after_discount-dv.base_tax_after_discount_noshipping,2) as '2200: Shipping VAT', 
round(sfi.base_shipping_incl_tax,2) as 'Shipping Amount',

####Total
round(sfi.base_grand_total-dv.base_tax_after_discount,2) as 'Net Total Sale', 
round(dv.base_tax_after_discount,2) as 'Total VAT', 
round(sfi.base_grand_total,2) as 'Grand Total Sales',



#### Cost Centre
(CASE 
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.au' 	then '1969: Australia'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.de'		then '1025: Germany'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.fr' 		then '1024: France'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com' or SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.ca'	then '1028: USA\\Canada'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.mx' 	then '1984: Mexico'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.co.uk' 	then '1023: UK'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.it' 		then '1026: Italy'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.es' 		then '1027: Spain'
      
   ELSE 'Other Amazon Store'
      
END) as 'Amazon region',

'AMZ' as 'Cost Centre',

#### Department    
(CASE 
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.au' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.de'		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.fr' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.mx' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.co.uk' 	then 'UK'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.it' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.es' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.ca'		then 'ROW'
   ELSE 'Other Amazon Department'
      
END) as 'Amazon Department'

FROM sales_invoice sfi

left join sales_order sfo on sfo.entity_id = sfi.order_id
left join dimasoft_order_vat_corrected dv on dv.order_id = sfo.entity_id
left join m2epro_order m2o on sfo.entity_id = m2o.magento_order_id
left join m2epro_amazon_order m2ao on  m2o.id = m2ao.order_id
left join sales_order_address sfoa on sfoa.parent_id = sfo.entity_id and sfoa.address_type = 'Shipping'

where DATE(sfi.created_at) = '".$dateovverride."' and sfo.store_id IN ('24','26','35')  and sfo.customer_group_id NOT IN ('6') and sfo.customer_email<>'wholesale@startfitness.co.uk'  

union ALL

###################
### Amazon daily credits
###################

SELECT 

####header data
date(sfo.created_at) as 'Order Date', date(sfcm.created_at) as 'Credit Date', m2ao.amazon_order_id, sfo.increment_id as 'Order Number', sfcm.increment_id as 'Credit Number', 
sfo.store_name as 'Store Name', SUBSTRING_INDEX(sfo.customer_email, '@', -1) as 'Sub-store' , sfo.customer_email, sfo.order_currency_code, sfoa.country_id,

####Goods totals
-(sfcm.base_subtotal_incl_tax-dv.base_tax_after_discount_noshipping+sfcm.base_discount_amount) as 'Net Goods',
-(dv.base_tax_after_discount_noshipping) as 'Goods VAT',
-(sfcm.base_subtotal_incl_tax+sfcm.base_discount_amount) as 'Gross Goods', 


####Shipping totals
-(sfcm.base_shipping_incl_tax-(dv.base_tax_after_discount-dv.base_tax_after_discount_noshipping)) as 'Net Shipping', 
-(dv.base_tax_after_discount-dv.base_tax_after_discount_noshipping) as 'Shipping VAT', 
-(sfcm.base_shipping_incl_tax) as 'Shipping Amount',

#####Credit totals
-(sfcm.base_grand_total-dv.base_tax_after_discount) as 'Net Sale', 
-(dv.base_tax_after_discount) as 'VAT', 
-(sfcm.base_grand_total) as 'Grand Total',


#### Cost Centre for Sage posting
(CASE 
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.au' 	then '1969: Australia'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.de'		then '1025: Germany'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.fr' 		then '1024: France'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com' or SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.ca'	then '1028: USA\\Canada'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.mx' 	then '1984: Mexico'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.co.uk' 	then '1023: UK'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.it' 		then '1026: Italy'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.es' 		then '1027: Spain'
      
   ELSE 'Other Amazon Store'
      
END) as 'Amazon region',

'AMZ' as 'Cost Centre',

#### Department for Sage posting
(CASE 
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.au' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.de'		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.fr' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.com.mx' 	then 'ROW'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.co.uk' 	then 'UK'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.it' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.es' 		then 'EUR'
   when SUBSTRING_INDEX(sfo.customer_email, '@', -1) = 'marketplace.amazon.ca'		then 'ROW'
   ELSE 'Other Amazon Department'
      
END) as 'Amazon Department'

FROM sales_creditmemo sfcm

left join sales_order sfo on sfo.entity_id = sfcm.order_id
left join dimasoft_credit_vat_corrected dv on dv.credit_id = sfcm.entity_id
left join m2epro_order m2o on sfo.entity_id = m2o.magento_order_id
left join m2epro_amazon_order m2ao on  m2o.id = m2ao.order_id
left join sales_order_address sfoa on sfoa.parent_id = sfcm.order_id and sfoa.address_type = 'Shipping'

where DATE(sfcm.created_at) = '".$dateovverride."' and sfo.store_id IN ('24','26','35')  and sfo.customer_group_id NOT IN ('6') and sfo.customer_email<>'wholesale@startfitness.co.uk'";

//echo $sql_getdata;exit;
// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);

// Output data To CSV
if(is_file(dirname(__FILE__)."/amazon_daily_sales.csv")) unlink(dirname(__FILE__)."/amazon_daily_sales.csv");
$fp = fopen(dirname(__FILE__).'/amazon_daily_sales.csv', 'w');
$csvHeader = array("Order Date", "Invoice/Credit Date", "Amazon Order ID","Order Number","Invoice Number","Store Name","Substore","Customer email","Order Currency","Country ID","4000: Net Goods","2200: Goods VAT",
"Gross Goods","4040: Net Shipping","2200: Shipping VAT","Gross Shipping","Net Total Sale", "Total VAT", "Grand Total Sale","Amazon Region","Cost Centre", "Amazon Department");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getdata as $data){
    fputcsv($fp, array($data['Order Date'], $data['Invoice Date'], $data['amazon_order_id'], $data['Order Number'], $data['Invoice Number'], $data['Store Name'], $data['Substore'], $data['customer_email'], 
	$data['order_currency_code'], $data['country_id'], $data['4000: Net Goods'], $data['2200: Goods VAT'], $data['Gross Goods'], $data['4040: Net Shipping'], $data['2200: Shipping VAT'], $data['Shipping Amount'], $data['Net Total Sale'], $data['Total VAT'], $data['Grand Total Sales'], $data['Amazon region'], $data['Cost Centre'], $data['Amazon Department'] ), ",");
}
fclose($fp);


//Receiver Details
$sendToEmailAddress = array('melissaburton@startfitness.co.uk','jillfawcett@startfitness.co.uk','geoffrey@rawtechnologies.co.uk');

$sendToName = 'Start Fitness';
$message = "Amazon Daily Sales Report";
$html = "Hi Accounts, <br><br>Daily Amazon Sales Report";
$mail = new \Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom("customerservice@startfitness.co.uk", 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Amazon Sales report  '.$dateovverride); //yesterdays date

	$dir = dirname(__FILE__)."/";
	$path = $dir.'amazon_daily_sales.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'amazon_daily_sales.csv';


		try {
    			$mail->send();
				echo "Success";
    			//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
		}
		catch (Exception $e) {
				echo "Failed to Send Email";
    			//Mage::getSingleton('core/session')->addError('Unable to send.');
		}
		
	


?>