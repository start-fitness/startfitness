<?php
// Start Group Script To Get In Stock Items SET OOS and Vice Versa
// Designed and built by James Simpson
// V1.0

error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #000;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#FF6717;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#FFC6B8;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000;}</style>';

// The SQL Queries
$sql_get_items = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,cpei.value as 'visability', csi.is_in_stock, cpe.has_options 
			FROM `catalog_product_entity` cpe 
			RIGHT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` 
			RIGHT JOIN `cataloginventory_stock_item` csi ON cpe.`entity_id`=csi.`product_id`
			WHERE cpei.`attribute_id` = 102 
			AND cpei.`value` = 4 
			AND cpei.`store_id` = 0
			AND cpe.type_id NOT LIKE '%giftcard%'";

// Fetch all reqults
$results_items = $read->fetchAll($sql_get_items);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">Out Of Stock - Set In Stock</h1>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th>Entity</th><th align="left">Model #</th><th>Type</th><th align="left">Total QTY</th><th>Status</th></tr>';
foreach($results_items as $items){
	// Default
	$totalstock = 0;
	// if is configurable - get simple stock count
	if($items['has_options']=='1'){
		$sql_options_stock = "SELECT cpsl.product_id,cpsl.parent_id,csi.qty,csi.is_in_stock FROM `catalog_product_super_link` cpsl RIGHT JOIN `cataloginventory_stock_item` csi ON cpsl.`product_id`=csi.`product_id` WHERE `parent_id` = ".$items['entity_id']." ";
		$results_child = $read->fetchAll($sql_options_stock);
		
		foreach($results_child as $child){
			$totalstock+= $child['qty'];
		}
		If($totalstock > 0 and $items['is_in_stock'] == '1'){
			continue;
		}else if($totalstock == '0' and $items['is_in_stock'] == '0'){ // Configurables should be set to in stock really
			continue;
		}else if($totalstock == '0' and $items['is_in_stock'] == '1'){ // Configurable can be set to in stock and not display
			continue;
		}
	}else{ // else get simple stock count
		$sql_stock = "SELECT qty,is_in_stock FROM `cataloginventory_stock_item` WHERE `product_id` = ".$items['entity_id']." ";
		$totalstock = $read->fetchOne($sql_stock);
		If($totalstock > 0 and $items['is_in_stock'] == '1'){
			continue;
		}else if($totalstock == '0' and $items['is_in_stock'] == '0'){
			continue;
		}
	}


	if($items['is_in_stock'] == 1){
		$stockstatus = "In Stock";
	}else{
		$stockstatus = "Out Of Stock";
	}
	$count++;
	$html .= '<tr><td><a href="https://www.startfitness.co.uk/admin/catalog/product/edit/id/'.$items['entity_id'].'/" target="_blank">'.$items['entity_id'].'</a></td><td>'.$items['sku'].'</td><td>'.$items['type_id'].'</td><td>'.$totalstock.'</td><td align="center">'.$stockstatus.'</td></tr>';
}
$html .= '</table>';
$html .= 'Total Products Failed: '.$count;
$html .= '</td></tr></table>';




//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk');
//$sendToEmailAddress = array('gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','lindsayhedley@startfitness.co.uk','carlsmith@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');

$sendToName = 'Start Fitness';

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Problem Products');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Problem Product Report '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>