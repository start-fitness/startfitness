<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$start_date = date('Y-m-d', strtotime('first day of last month'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "stock-report - ".$end_date.".csv";

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Stock Report - ".$end_date;
$html = "Here is your monthly Stock report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Stock Report - '.$end_date);


// Get Data
$sql_getdata = "SELECT tmp.`parent_id`, tmp.`source`, tmp.`attribute_set_name` `attribute set`, tmp.`brand`, tmp.`gender`, tmp.`weight`, tmp.`tax`, tmp.`bin location`, tmp.`bin overstock`,
(SELECT eaov_brand.`value`  FROM `catalog_product_entity` cpe
LEFT OUTER JOIN `catalog_product_entity_int` cpei_brand ON cpei_brand.`entity_id` = cpe.`entity_id`
INNER JOIN `eav_attribute` ea_brand ON ea_brand.`attribute_id` = cpei_brand.`attribute_id` AND ea_brand.`attribute_id` = 
CASE
	WHEN cpe.`attribute_set_id` = 9 THEN 198		 	/* bikes */
	WHEN cpe.`attribute_set_id` IN (11,44,47) THEN 329		/* cycleclothing / clothing / bras */

	WHEN cpe.`attribute_set_id` = 45 THEN 289			/* footwear */
	WHEN cpe.`attribute_set_id` IN (43,46) THEN 340			/* accessories / socks */
	WHEN cpe.`attribute_set_id` = 48 THEN 264			/* nutrition */
	ELSE 0
END
INNER JOIN `eav_attribute_option` eao_brand ON eao_brand.`attribute_id` = ea_brand.`attribute_id` AND eao_brand.`option_id` = cpei_brand.`value` INNER JOIN `eav_attribute_option_value` eaov_brand ON eaov_brand.`option_id` = eao_brand.`option_id` AND eaov_brand.`store_id` = 0 WHERE cpe.`entity_id`=tmp.`parent_id`) `attribute_type`,
(SELECT GROUP_CONCAT(eaov.`value` SEPARATOR '|') `type` FROM `catalog_product_entity` cpe
LEFT JOIN `catalog_product_entity_varchar` cpev_footwear_type ON cpev_footwear_type.`entity_id`=cpe.`entity_id` AND cpev_footwear_type.`attribute_id`=289 AND cpev_footwear_type.`store_id`=0
INNER JOIN `eav_attribute_option_value` eaov ON FIND_IN_SET(eaov.`option_id`,cpev_footwear_type.`value`) AND eaov.`store_id` = 0
WHERE cpev_footwear_type.`entity_id`=tmp.`parent_id`
GROUP BY cpev_footwear_type.`entity_id`) `footwear_type`,
(SELECT DISTINCT `created_at` FROM `catalog_product_entity` cpe WHERE cpe.`entity_id` = tmp.`parent_id` ) created_date, (SELECT DISTINCT `sku` FROM `catalog_product_entity` cpe WHERE cpe.`entity_id` = tmp.`parent_id` ) sku, tmp.`parent model`, tmp.`parent name`, sum(tmp.`qty`) `total qty`, tmp.`price`, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = tmp.`parent_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) cost_price, tmp.`rrp`, sum(tmp.`qty`) * (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = tmp.`parent_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) `total cost`, (((tmp.`rrp` - tmp.`price`)/tmp.`rrp`)*100) discount FROM (SELECT cpes.`entity_id`, cpes.`sku`, eas.`attribute_set_name`, IFNULL((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` AND cped.`attribute_id`=120 AND cped.`store_id`=0), 0) rrp, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0) price, (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) qty, 
(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0) * (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) total_value, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=80 AND cped.`store_id`=0) weight,
(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) model, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=136 AND cpev.`store_id`=0) `bin location`, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=137 AND cpev.`store_id`=0) `bin overstock`, (select eaov.`value` from `catalog_product_entity_int` cpei INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id` AND ea.`attribute_id` = 81 INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = ea.`attribute_id` AND eao.`option_id` = cpei.`value` INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = 0 WHERE cpei.`entity_id` = cpes.`entity_id`) brand, (select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0) tax, (select eaov.`value` from `catalog_product_entity_int` cpei INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id` AND ea.`attribute_id` = 343 INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = ea.`attribute_id` AND eao.`option_id` = cpei.`value` INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = 0 WHERE cpei.`entity_id` = cpes.`entity_id`) `source`,(select eaov.`value` from `catalog_product_entity_varchar` cpei INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id` AND ea.`attribute_id` = 141 INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = ea.`attribute_id` AND eao.`option_id` = cpei.`value` INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = 0 WHERE cpei.`entity_id` = cpes.`entity_id`) gender, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) name,
IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE cpsl.`product_id` = cpes.`entity_id` ORDER BY `parent_id` DESC LIMIT 1) >1,(SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE cpsl.`product_id` = cpes.`entity_id` ORDER BY `parent_id` DESC LIMIT 1),cpes.`entity_id`) parent_id,
IF ((SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) <>'NULL',(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0),(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0)) `parent name`, IF ((SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) <>'NULL',(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0),(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0)) `parent model`
FROM `catalog_product_entity` cpes INNER JOIN `eav_attribute_set` eas ON eas.`attribute_set_id` = cpes.`attribute_set_id` WHERE cpes.`has_options` = 0) tmp GROUP BY tmp.`parent_id` ORDER BY `total qty` DESC";

//echo '<p>'.$sql_getdata; exit;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/".$filename)) unlink(dirname(__FILE__)."/".$filename);
$fp = fopen(dirname(__FILE__)."/".$filename, 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,"\t"); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, chr(9));
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/'.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;



try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>
