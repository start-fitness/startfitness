<?php 
/**
 * General sales report script.
 *
 *
 * 20/5/2014: Added totals row (DR)
 * 22/5/2014: Added better email subject (DR)
 *
 */
	require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
	Mage::app();
	
	//Receiver Details
	$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','dan@robins.co.uk');
	$sendToName = 'Test Name';

	//Define Store Code
	$storeCode = $_GET['storecode'];	
		
	//Set category
	$cat1 = '1221';
			
	//Set Attribute ID
	$attr_id = '285';
	
	//Set Store Arrays
	//$start_cycles = array("1","10","11","13"); //This is homepage only, exclude ebay, amazon...
	$start_cycles = array("1","10");
	//$start_football = array("5","16","14"); //This is homepage only, exclude ebay, amazon...
	$start_football = array("5","16");
	//$start_fitness = array ("6","15","17"); //This is homepage only, exclude ebay, amazon...
	$start_fitness = array ("6"); 
	$start_outdoors = array();
	$moremile = array();
	
	//Set the current report view
	if(isset($storeCode)){
		if($storeCode=="cycles"){
			//Cycles Selected
			$current_web = implode(',',$start_cycles);
		}elseif($storeCode=="football"){
			//Football Selected
			$current_web = implode(',',$start_football);
		}elseif($storeCode=="fitness"){
			//Fitness Selected
			$current_web = implode(',',$start_fitness);
		}
	}else{
		echo"No Store Selected";
	}
		
	//Check Store Code
	if(isset($storeCode))
	{
		//Check Date - not send on Sat, Sun
		if(date("l") != "Sunday" || date("l") != "Saturday")
		{
			//Tue - Fri report sending
			if(date("l") != "Monday")
			{
				//Set dates
				$dateStart = mktime(4, 0, 0, date('m'), date('d')-1, date('Y'));
				$dateEnd = mktime(4, 0, 0, date('m'), date('d'), date('Y'));
				$csv_result = '';
			
			} elseif(date("l") == "Monday")
			{
				//Set dates
				$dateStart = mktime(9, 0, 0, date('m'), date('d')-3, date('Y'));
				$dateEnd = mktime(6, 0, 0, date('m'), date('d'), date('Y'));
				$csv_result = '';
			
			}	
			//Retreive domain name
			
			$store = Mage::app()->getStore()->getName();
			$shopUrl = str_replace(array('http://', '/'), '', Mage::app()->getStore()->getBaseUrl());
			$write = Mage::getSingleton('core/resource')->getConnection('core_write');
			
			$sql = "SELECT order_id
					    FROM sales_flat_order_item
					    WHERE
					    order_id IN (
					        SELECT entity_id
					        FROM sales_flat_order
					        WHERE created_at BETWEEN '".date('Y-m-d', $dateStart)."' AND '".date('Y-m-d', $dateEnd)."' AND state != 'canceled'
					    )
					    GROUP BY order_id";
			
			$sql_body = "SELECT order_id
					    FROM sales_flat_order_item
					    WHERE
					    order_id IN (
					        SELECT entity_id
					        FROM sales_flat_order
					        WHERE created_at BETWEEN '".date('Y-m-d', $dateStart)."' AND '".date('Y-m-d', $dateEnd)."' AND state != 'canceled'
					    )
					    GROUP BY order_id";
			
			$sql_total = "SELECT SUM(row_total_incl_tax) AS totals, SUM(qty_ordered) AS cnt FROM sales_flat_order_item 
					WHERE
				    order_id IN (
				        SELECT entity_id
				        FROM sales_flat_order
				        WHERE created_at BETWEEN '".date('Y-m-d', $dateStart)."' AND '".date('Y-m-d', $dateEnd)."' AND state != 'canceled'
				    )";
			
			$readresult=$write->query($sql);
			$readresult_body=$write->query($sql_body);
			$readresult_total=$write->query($sql_total)->fetch();
			
			$csv_result .= '"Product","Code","Quantity","Price","Total"'."\n";
			while ($row = $readresult->fetch() ) {
				//Count items = 10
				if($i<11)
				{
					$orderItems = Mage::getModel('sales/order')->load($row['order_id'])->getAllItems();
			
					foreach ($orderItems as $item){
			
						$i++;
						//var_dump($item->getData());
			
						if($item->getParent_item_id()==null)
						{
							if ($item->getProduct_type()=='configurable')
							{
								$x['name']=$item->getName();
								$x['qty']=$x['qty'] + $item->getQty_ordered();
								$x['sum']=$x['sum'] + $item->getPrice();
							}
							if($x['name'] && $item->getName() != $x['name'])
							{
								//Write Totals
								$csv_result .= '"'.$x['name'].'","","'.number_format($x['qty'],0).'","","'.number_format($x['sum'],2).'"'."\n";
								$x['name']="";
								$x['qty']=0;
								$x['sum']=0;
							}
							$csv_result .= '"'.$item->getName().'","'.$item->getSku().'","'.number_format($item->getQty_ordered(),0).'","'.number_format($item->getPrice(),2).'","'.number_format($item->getRow_total_incl_tax(),2).'"'."\n";
						}
					}
				}
			}
			
			$html = "<h1>General sales report for ".ucwords(strtolower($storeCode))." ".$store."</h1>";
			$html .="<h2>Date Range:" . date('d-m-Y h:i', $dateStart) . " - " . date('d-m-Y h:i', $dateEnd)."</h2>";
			$html .= "<h2>Total sales: ".number_format($readresult_total['cnt'],0)."</h2>";
			$html .= "<h2>Total revenue: ".number_format($readresult_total['totals'],2)."</h2>";
			$html .= "<h2>Top-10 Best Selling Products</h2>";
			$html .= "<table border=\"1\" cellpadding=\"10\" cellspacing=\"0\">";
			$html .= "<thead><tr><th>Product</th><th>Code</th><th>Quantity</th><th>Price</th><th>Total</th></tr></thead>";
			
			$i=0;
			$html .= "<tbody>";
			
			//Configurable totals
			$x = array(
					"name" => "",
					"qty" => 0,
					"sum" => 0
			);
				
			while ($row_body = $readresult_body->fetch() ) {
				//Count items = 10
				if($i<11)
				{
					$orderItems = Mage::getModel('sales/order')->load($row_body['order_id'])->getAllItems();
				
					foreach ($orderItems as $item){
	
						$i++;
						//var_dump($item->getData());
						
						if($item->getParent_item_id()==null)
						{
							if ($item->getProduct_type()=='configurable')
							{
								$x['name']=$item->getName();
								$x['qty']=$x['qty'] + $item->getQty_ordered();
								$x['sum']=$x['sum'] + $item->getPrice();
							}
							if($x['name'] && $item->getName() != $x['name'])
							{
								//Write Totals
								$html .= "<tr><td>".$x['name']."</td><td>-</td><td>".number_format($x['qty'],0)."</td><td>-</td><td>".number_format($x['sum'],2)."</td></tr>";
								$x['name']="";
								$x['qty']=0;
								$x['sum']=0;
							}
							$html .= "<tr><td>".$item->getName()."</td><td>".$item->getSku()."</td><td>".number_format($item->getQty_ordered(),0)."</td><td>".number_format($item->getPrice(),2)."</td><td>".number_format($item->getRow_total_incl_tax(),2)."</td></tr>";
						}
					}
				}
			}
			$html .= "</tbody>";
			$html .= "</table>";
			
			echo $html;
			
			$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), Mage::getStoreConfig('trans_email/ident_general/name'));
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject(ucwords(strtolower($storeCode)).' General Sales Report '.date('d/m/Y', $dateEnd));
			
			//create attachment from content variable
			$at = $mail->createAttachment($csv_result);
			$at->type = 'text/csv';//
			$at->filename = $shopUrl.'_general_sales_report_'.date('Y-m-d').'.csv';
			
			$mail->send();
		}
	}
		
?>