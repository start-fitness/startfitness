<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

$today = date("Y-m-d"); // Todays Date mysql format
$last_week = date("Y-m-d", strtotime("-2 weeks")); // Last Week Date mysql format
$count = 0;

// Get Data
$sql_getdata = "SELECT 
eaov_1.value as `Brand`, cpe.`sku` as `Barcode`,

cpev_parentmodel.value as `Parent_Model`, cpev_parentname.value as 'Parent Name', 

cpev_model.value as 'Simple Model', cpev_name.value as 'Simple name',

round((csi.qty),0) as 'Qty', round((csi.qty*if(cpei_tax.value=2, cped_sellprice.value/1.2,cped_sellprice.value)),2) as 'Total Sales', 

(round(csi.qty * cped_costprice.value,2)) as 'Total Cost', 

round((csi.qty*if(cpei_tax.value=2, cped_sellprice.value/1.2,cped_sellprice.value)),2)-round(csi.qty * cped_costprice.value,2) as 'Profit',

round((round((csi.qty*if(cpei_tax.value=2, cped_sellprice.value/1.2,cped_sellprice.value)),2)-round(csi.qty * cped_costprice.value,2))/(round((csi.qty*if(cpei_tax.value=2, cped_sellprice.value/1.2,cped_sellprice.value)),2))*100,2) AS `Margin`, cped_RRP.value as 'RRP', cped_sellprice.value as 'Sell Price', round(((cped_RRP.value -cped_sellprice.value)/cped_RRP.value)*100,2) as 'Saving on RRP',

t2.`pqty`


FROM cataloginventory_stock_item csi


#####attribute set

left join catalog_product_entity cpe on cpe.entity_id = csi.product_id

####brand
left JOIN catalog_product_entity_int cpei_1 on csi.product_id = cpei_1.entity_id and cpei_1.attribute_id='81' and cpei_1.store_id='0'
left join eav_attribute_option_value eaov_1 on cpei_1.value = eaov_1.option_id




####name
left JOIN catalog_product_entity_varchar cpev_name on csi.product_id = cpev_name.entity_id and cpev_name.attribute_id='71' and cpev_name.store_id='0'




####sell price

Left join catalog_product_entity_decimal cped_sellprice on csi.product_id = cped_sellprice.entity_id and cped_sellprice.attribute_id='75' and cped_sellprice.store_id='0'

#####cost price
Left join catalog_product_entity_decimal cped_costprice on csi.product_id = cped_costprice.entity_id and cped_costprice.attribute_id='293' and cped_costprice.store_id='0'



######model
left JOIN catalog_product_entity_varchar cpev_model on csi.product_id = cpev_model.entity_id and cpev_model.attribute_id='134' and cpev_model.store_id='0'


######parent id-simple id
left join catalog_product_super_link cpsl on cpsl.product_id=csi.product_id


#####parent name
left JOIN catalog_product_entity_varchar cpev_parentname on if(cpsl.parent_id is null,csi.product_id, cpsl.parent_id) = cpev_parentname.entity_id and cpev_parentname.attribute_id='71' and cpev_parentname.store_id='0'

######PArent model
left JOIN catalog_product_entity_varchar cpev_parentmodel on if(cpsl.parent_id is null,csi.product_id, cpsl.parent_id)  = cpev_parentmodel.entity_id and cpev_parentmodel.attribute_id='134' and cpev_parentmodel.store_id='0'

######vat rate
left join catalog_product_entity_int cpei_tax on csi.product_id = cpei_tax.entity_id and cpei_tax.attribute_id = 122 AND cpei_tax.store_id=0 


####RRP price
Left join catalog_product_entity_decimal cped_RRP on csi.product_id = cped_RRP.entity_id and cped_RRP.attribute_id='120' and cped_RRP.store_id='0'

left join (SELECT cpev_parentmodel.entity_id, cpev_parentmodel.value parent_model, sum(csi.qty) pqty
            
            From cataloginventory_stock_item csi
            
            left join catalog_product_entity cpe on cpe.entity_id = csi.product_id
            
            
            left JOIN catalog_product_entity_varchar cpev_name on csi.product_id = cpev_name.entity_id and cpev_name.attribute_id='71' and cpev_name.store_id='0'
            
            
            left JOIN catalog_product_entity_varchar cpev_model on csi.product_id = cpev_model.entity_id and cpev_model.attribute_id='134' and cpev_model.store_id='0'
            
            
            left join catalog_product_super_link cpsl on cpsl.product_id=csi.product_id
            
            
            left JOIN catalog_product_entity_varchar cpev_parentname on if(cpsl.parent_id is null,csi.product_id, cpsl.parent_id) = cpev_parentname.entity_id and cpev_parentname.attribute_id='71' and cpev_parentname.store_id='0'
            
            
            left JOIN catalog_product_entity_varchar cpev_parentmodel on if(cpsl.parent_id is null,csi.product_id, cpsl.parent_id)  = cpev_parentmodel.entity_id and cpev_parentmodel.attribute_id='134' and cpev_parentmodel.store_id='0'

            group by cpev_parentmodel.value  ) t2 on t2.entity_id = if(cpsl.parent_id is null,csi.product_id, cpsl.parent_id)

WHERE t2.`pqty` >= 150

HAVING `Margin` >= 30 and `brand` NOT IN ('More Mile')

ORDER BY t2.`pqty`  DESC";

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);

// Output data To CSV
if(is_file(dirname(__FILE__)."/vervaunt.csv")) unlink(dirname(__FILE__)."/vervaunt.csv");
$fp = fopen(dirname(__FILE__).'/vervaunt.csv', 'w');
$csvHeader = array("brand", "Barcode", "Parent_Model","Parent Name","Simple Model","Simple Name","Simple Qty","Total Sales","Total Cost","Profit","Margin","RRP","Sell Price","Saving on RRP","Parent Qty");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getdata as $data){
    fputcsv($fp, array($data['Brand'], $data['Barcode'], $data['Parent_Model'], $data['Parent Name'], $data['Simple Model'], $data['Simple name'], $data['Qty'], $data['Total Sales'], $data['Total Cost'], $data['Profit'], $data['Margin'], $data['RRP'], $data['Sell Price'], $data['Saving on RRP'], $data['pqty'] ), ",");
}
fclose($fp);


//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','richardtate@startfitness.co.uk','mark@startfitness.co.uk','josh@vervaunt.com');

$sendToName = 'Start Fitness';
$message = "Start Fitness Product Report";
$html = "Hi Vervaunt, <br><br>Here is your latest product report from Start Fitness.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Product Report '.date("D d-M-Y"));

	$dir = Mage::getBaseDir();
	$path = $dir.DS.'script'.DS.'J5HUH'.DS.'email_reports'.DS.'vervaunt.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'vervaunt.csv';


		try {
    			$mail->send();
				echo "Success";
    			//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
		}
		catch (Exception $e) {
				echo "Failed to Send Email";
    			//Mage::getSingleton('core/session')->addError('Unable to send.');
		}
		
		finally {
			echo "finally";
		}


?>