<?php

ini_set('date.timezone', 'Europe/London');

//$start_date = '2020-10-01';
//$end_date = '2020-10-31';
//$countries = array('CZ','HU','NL','PL','PT','SI');

$start_date = date('Y-m-d', strtotime('first day of last month'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$countries = array('AT','BE','CZ','DE','DK','ES','FI','FR','GR','HU','IE','IT','NL','PL','PT','SE','SI');

?>
