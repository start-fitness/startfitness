<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$start_date = date('Y-m-d', strtotime('this day last year'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "previous-years-sales-report - ".$start_date." - ".$end_date.".csv";

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Previous Years Sales Report - ".$end_date;
$html = "Here is your previous years sales report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Previous Years Sales Report  - '.$end_date);


// Get Data
$sql_getdata = "SELECT `cpev_model`.`value` `model`, `cpev_name`.`value` `name`,`tmp`.`Qty_Sold` FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold FROM `sales_order_item` sfoi LEFT JOIN `sales_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` WHERE `sfoi`.`product_type` = 'simple' AND (DATE(`sfo`.`created_at`) >= '".$start_date."' AND DATE(`sfo`.`created_at`) <= '".$end_date."') AND `sfo`.`customer_group_id` NOT IN (2,6,7,9) AND sfo.`store_id` NOT IN (23,29,30,31,32,33,34) AND sfo.`status` <> 'canceled' GROUP BY `parent_id` ORDER BY Qty_Sold Desc) tmp LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/".$filename)) unlink(dirname(__FILE__)."/".$filename);
$fp = fopen(dirname(__FILE__)."/".$filename, 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/'.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;



try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>