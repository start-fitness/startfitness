<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');


umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$start_date = date('Y-m-d', strtotime('first day of last month'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "eu-only-sales-vat-invoices-report - ".$end_date.".csv";
$eu_countries = "'AT','BE','BG','HR','CY','CZ','DK','EE','FI','FR','DE','GR','HU','IE','IT','LV','LT','LU','MT','MT','NL','PL','PT','RO','SK','SI','ES','SE'";

//echo date('Y-m-d') . ' - '.$start_date . ' - ' . $end_date; exit;

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Store Credit Report - ".$end_date;
$html = "Here is your monthly EU Only Sales VAT Invoices report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('EU Only Sales VAT Invoices Report - '.$end_date);


// Get Data
$sql_getdata = "SELECT sfi.entity_id as 'Invoice Entity ID', sfo.`status`, sfo.`customer_id` as 'Customer Number', sfo.increment_id as 'Order number', sfi.increment_id as 'Invoice Number', date(sfi.created_at) as 'Invoice date', sfo.`customer_group_id`, IF(sfo.`customer_taxvat` IS NOT NULL,sfo.`customer_taxvat`,sfoa.`vat_id`) `customer_taxvat`,
#sfi.base_subtotal as 'GBP Base sub total', 
#sfi.base_discount_amount as 'GBP discount amount', 
sfi.base_subtotal_incl_tax+sfi.base_discount_amount-dsvc.base_tax_after_discount_noshipping as 'GBP Net goods after discount', 
dsvc.base_tax_after_discount_noshipping as 'GBP VAT on Goods', 
sfi.base_subtotal_incl_tax+sfi.base_discount_amount  as 'GBP Gross goods after discount', sfoa.country_id, sfo.amstorecredit_base_amount as 'Store Credit Used', sfo.store_name

FROM sales_invoice sfi

LEFT JOIN sales_order as sfo on sfi.order_id = sfo.entity_id
left join dimasoft_order_vat_corrected dsvc on sfi.order_id = dsvc.order_id
JOIN sales_order_address sfoa ON sfoa.parent_id = sfo.entity_id AND sfoa.address_type = 'shipping'

WHERE date(sfi.created_at) >= '".$start_date."' and date(sfi.created_at) <= '".$end_date."' AND sfo.`status` <> 'canceled' AND sfo.`customer_group_id`=4 AND sfoa.`country_id` IN (".$eu_countries.")
ORDER BY date(sfi.created_at)

AND sfi.`entity_id` NOT IN (SELECT si.`entity_id` FROM `sales_invoice` si INNER JOIN `sales_order` so ON so.`entity_id` = si.`order_id` LEFT JOIN `sales_shipment` ss ON ss.`order_id` = so.`entity_id` WHERE DATE(si.`created_at`) BETWEEN '2020-11-01' AND '2020-11-30' AND si.`store_id` IN (24,26,35,25) AND so.`created_at` BETWEEN DATE_SUB(ss.`created_at`, INTERVAL 5 MINUTE) AND ss.`created_at` )
";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/".$filename)) unlink(dirname(__FILE__)."/".$filename);
$fp = fopen(dirname(__FILE__)."/".$filename, 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/'.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;



try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>