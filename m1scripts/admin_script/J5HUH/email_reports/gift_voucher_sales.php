<?php
// Start Group Script To Get In Stock Items SET OOS and Vice Versa
// Designed and built by James Simpson
// V1.0

error_reporting(E_ALL);

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);

$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("-1 days"));
$html = "";

$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #000;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#FF6717;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#FFC6B8;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000;}</style>';

// The SQL Queries
$sql_get_items = "SELECT so.`entity_id`, so.`created_at`, so.`increment_id`, GROUP_CONCAT(concat(format(soi.`qty_ordered`,0),' x ',soi.`name`) SEPARATOR '\r\n') `gift_vouchers`, so.`status`  FROM `sales_order_item` soi
				INNER JOIN `sales_order` so ON so.`entity_id` = soi.`order_id`
				WHERE `product_type` LIKE '%gift%' AND DATE(so.`created_at`) BETWEEN '".$yesterday."' AND '".$today."'
				GROUP BY so.`increment_id`";

// Fetch all reqults
$results_items = $read->fetchAll($sql_get_items);

if (empty($results_items)) echo 'No Output'; exit;

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">Gift Voucher Sales - From Yesterday</h1>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th>Date</th><th align="left">Order #</th><th>Gift Vouchers</th><th align="left">Status</th></tr>';
foreach($results_items as $items){
	$html .= '<tr><td>'.$items['created_at'].'</td><td><a href="https://www.startfitness.co.uk/admin/sales/order/view/order_id/'.$items['entity_id'].'/" target="_blank">'.$items['increment_id'].'</a></td><td>'.nl2br(htmlentities($items['gift_vouchers'])).'</td><td>'.$items['status'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';




//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','carlsmith@startfitness.co.uk','michaeltaggart@startfitness.co.uk','gina@startfitness.co.uk','finnbrodie@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');

$sendToName = 'Start Fitness';

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Gift Voucher Sales');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Gift Voucher Sales '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>