<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

require 'monthly_config.php';

//$start_date = date('Y-m-d', strtotime('first day of last month'));
//$end_date = date('Y-m-d', strtotime('last day of last month'));
//$countries = array('AT','BE','DE','DK','ES','FI','FR','GR','IE','IT','SE');

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','melissaburton@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Shipping Credits VAT Reports - ".$end_date;
$html = "Here are your monthly Shipping Credits VAT reports.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Monthly Shipping Credits VAT Reports - '.$end_date);


foreach ($countries as $country_id) {
echo $country_id;	
// Get Data
$sql_getdata = "SELECT sfoa.`country_id`, DATE(sfcm.`created_at`) `created_at`, sfcm.`increment_id`, sfcm.`entity_id`, sfcm.`base_shipping_amount`, sfcm.`base_shipping_tax_amount`, sfcm.`base_shipping_incl_tax` FROM sales_creditmemo sfcm
LEFT JOIN sales_order sfo ON sfo.`entity_id` = sfcm.`order_id`
left JOIN sales_order_address sfoa ON sfoa.`entity_id` = sfo.shipping_address_id
WHERE date(sfcm.created_at) >= '".$start_date."' AND date(sfcm.created_at) <= '".$end_date."' and sfoa.country_id = '".$country_id."' ORDER BY `created_at`";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) continue;

// Output data To CSV
if(is_file(dirname(__FILE__)."/shipping-credits-vat-report - ".$country_id.' - '.$end_date.".csv")) unlink(dirname(__FILE__)."/shipping-credits-vat-report - ".$country_id.' - '.$end_date.".csv");
$fp = fopen(dirname(__FILE__)."/shipping-credits-vat-report - ".$country_id.' - '.$end_date.".csv", 'w');
$csvHeader = array("country_id", "created_at", "increment_id","entity_id","base_shipping_amount","base_shipping_tax_amount","base_shipping_incl_tax");

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,",");
foreach ($results_getdata as $data){
    fputcsv($fp, array($data['country_id'], $data['created_at'], $data['increment_id'], $data['entity_id'], $data['base_shipping_amount'], $data['base_shipping_tax_amount'], $data['base_shipping_incl_tax']), ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/shipping-credits-vat-report - '.$country_id.' - '.$end_date.'.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'shipping-credits-vat-report - '.$country_id.' - '.$end_date.'.csv';

}

try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>