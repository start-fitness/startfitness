<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 0);
//error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
//require_once('/microcloud/domains/fitnes/domains/admin.startfitness.co.uk/http/app/Mage.php'); //Path to Magento

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');


// Get webstie ID's
$fitness = '6,19';
//$sportssize = '28';
//$football = '5,20';
//$cycles = '1,21';
//$wildtrak = '7';
//$moremile = '8';
$amazon_uk = '24';
$amazon_us = '26';
$ebay = '25';
$websites = array("$fitness","$amazon_uk","$amazon_us","$ebay");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$weekend = date("Y-m-d",strtotime('-3 days')); // Date before the weekend

if($day=='1'){
	// Day is monday so get weekend sales
	$title = "Weekend Sales Report - By Quantity";
	$date_from = $weekend;
	$date_to = $today;
}else{
	// Else every other day of the working week
	$title = "Daily Sales Report - By Quantity";
	$date_from = $yesterday;
	$date_to = $today;
}

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}
table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }</style>';
$html .= '<center><p>'.$title.' Report run from '.$date_from.' 9:00am till '.$date_to.' 8:59am</p></center>';
// The SQL Queries
foreach($websites as $website){

// The website names
if($website == $fitness){ $web_name = "Start Fitness"; }
//if($website == $sportssize){ $web_name = "Sports Size"; }
//if($website == $football){$web_name = "Start Football"; }
//if($website == $cycles){ $web_name = "Start Cycles"; }
//if($website == $wildtrak){ $web_name = "WildTrak"; }
if($website == $moremile){ $web_name = "More Mile"; }
if($website == $amazon_uk){ $web_name = "Amazon UK"; }
if($website == $amazon_us){ $web_name = "Amazon US"; }
if($website == $ebay){ $web_name = "eBay"; }

// Get Data
$sql_getsales = "SELECT `cpev_model`.`value` as 'Model',`cpev_name`.`value` as 'Name',`tmp`.`Qty_Sold` 
		  FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(
		  SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` 
		  ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold 
		  FROM `sales_order_item` sfoi LEFT JOIN `sales_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` WHERE `sfo`.`store_id` IN (".$website.") AND sfo.`status` NOT IN ('pending','pending_payment','canceled')
		  AND `sfoi`.`product_type` IN ('simple','giftcard') AND (`sfo`.`created_at` >= '".$date_from." 09:00:00' AND `sfo`.`created_at` <= '".$date_to." 08:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) 
		  GROUP BY `parent_id` ORDER BY Qty_Sold Desc) tmp LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 
		  LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 WHERE `tmp`.`Qty_Sold` >0 LIMIT 25";

$sql_getnumorders = "SELECT count(entity_id) as 'total_orders' FROM `sales_order` WHERE `created_at` >= '".$date_from." 09:00:00' AND `created_at` <= '".$date_to." 08:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending','pending_payment','canceled')";
$sql_getammount = "SELECT sum(base_grand_total) FROM `sales_order` WHERE `created_at` >= '".$date_from." 09:00:00' AND `created_at` <= '".$date_to." 08:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending','pending_payment','canceled')";
//echo $sql_getsales; exit;
// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);
$results_orders = $read->fetchOne($sql_getnumorders);
$results_ammount = $read->fetchOne($sql_getammount);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$web_name.' '.$title.'</h1>';
$html .= '<p><b>Total Orders</b>: ('.$results_orders.') - <b>Total Takings</b>: (�'.number_format($results_ammount,2).')</p>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk','kieranreay@startfitness.co.uk','danielmeggison@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Magento '.$title);
			//$mail->setFrom("customerservices@startfitness.co.uk");
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Sales Report '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";






// Get webstie ID's
$fitness = '6,19';
//$sportssize = '28';
//$football = '5,20';
//$cycles = '1,21';
//$wildtrak = '7';
//$moremile = '8';
$amazon_uk = '24';
$amazon_us = '26';
$ebay = '25';
$websites = array("$fitness","$amazon_uk","$amazon_us","$ebay");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$weekend = date("Y-m-d",strtotime('-3 days')); // Date before the weekend

if($day=='1'){
	// Day is monday so get weekend sales
	$title = "Weekend Sales Report - By Value";
	$date_from = $weekend;
	$date_to = $today;
}else{
	// Else every other day of the working week
	$title = "Daily Sales Report - By Value";
	$date_from = $yesterday;
	$date_to = $today;
}

$count = 0;
$html = '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}
table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }</style>';
$html .= '<center><p>'.$title.' Report run from '.$date_from.' 9:00am till '.$date_to.' 8:59am</p></center>';
// The SQL Queries
foreach($websites as $website){

// The website names
if($website == $fitness){ $web_name = "Start Fitness"; }
//if($website == $sportssize){ $web_name = "Sports Size"; }
//if($website == $football){$web_name = "Start Football"; }
//if($website == $cycles){ $web_name = "Start Cycles"; }
//if($website == $wildtrak){ $web_name = "WildTrak"; }
if($website == $moremile){ $web_name = "More Mile"; }
if($website == $amazon_uk){ $web_name = "Amazon UK"; }
if($website == $amazon_us){ $web_name = "Amazon US"; }
if($website == $ebay){ $web_name = "eBay"; }

// Get Data
$sql_getsales = "SELECT tmp.`Qty_Sold`, tmp.`Qty_Sold`*tmp.`Value` as 'Value', cpev_name.`value` as 'Name', cpev_model.`value` as 'Model' FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,( SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold, cped_price.`value` as 'Value' FROM `sales_order_item` sfoi LEFT JOIN `sales_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` LEFT JOIN `catalog_product_entity_decimal` cped_price ON `cped_price`.`entity_id` = `sfoi`.`product_id` and `cped_price`.`attribute_id`=75 AND `cped_price`.`store_id`=0 WHERE `sfo`.`store_id` IN (".$website.") AND `sfoi`.`product_type` IN ('simple','giftcard') AND (`sfo`.`created_at` >= '".$date_from." 09:00:00' AND `sfo`.`created_at` <= '".$date_to." 08:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) AND sfo.`status` NOT IN ('pending','pending_payment','canceled') GROUP BY `parent_id`  ORDER BY `Value` DESC) tmp
	LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 ORDER BY `Value` DESC LIMIT 25";

//echo $sql_getsales; exit;

$sql_getnumorders = "SELECT count(entity_id) as 'total_orders' FROM `sales_order` WHERE `created_at` >= '".$date_from." 09:00:00' AND `created_at` <= '".$date_to." 08:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending','pending_payment','canceled')";
$sql_getammount = "SELECT sum(base_grand_total) FROM `sales_order` WHERE `created_at` >= '".$date_from." 09:00:00' AND `created_at` <= '".$date_to." 08:59:59' AND `store_id` IN (".$website.") AND `customer_group_id` NOT IN (2,6,7) AND `status` NOT IN ('pending','pending_payment','canceled')";

//echo $sql_getnumorders; echo $sql_getammount; exit;

// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);
$results_orders = $read->fetchOne($sql_getnumorders);
$results_ammount = $read->fetchOne($sql_getammount);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$web_name.' '.$title.'</h1>';
$html .= '<p><b>Total Orders</b>: ('.$results_orders.') - <b>Total Takings</b>: (�'.number_format($results_ammount,2).')</p>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th><th>Value</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td><td align="center">'.number_format($home['Value'],2).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk','kieranreay@startfitness.co.uk','danielmeggison@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Sales Report  - By Value '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>
