<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('memory_limit', '2048M');
ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');


umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$start_date = date('Y-m-d', strtotime('first day of last month'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "nrv-stock-report - ".$end_date.".csv";

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','melissaburton@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "NRV Stock Report - ".$end_date;
$html = "Here is your monthly NRV Stock report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('NRV Stock Report - '.$end_date);


// Get Data
$sql_getdata = "SELECT cpei_source.`value` `source`, cpes.`entity_id`, cpes.`sku`, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=120 AND cped.`store_id`=0) rrp_inc_vat, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0) price_inc_vat,
IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,'Y','N') vat, (select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0) vat_code,
FORMAT(IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)),2) price_ex_vat,
(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) cost_price_ex_vat,
IF (IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0)<0,IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0),0) less_than_cost,
IF (IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0)<0,IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0),0) * (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) cost_vs_NRV,
(SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) qty, 
(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) * (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) `total_value`,
(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) model, (select eaov.`value` from `catalog_product_entity_int` cpei INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id` AND ea.`attribute_id` = 81 INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = ea.`attribute_id` AND eao.`option_id` = cpei.`value` INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = 0 WHERE cpei.`entity_id` = cpes.`entity_id`) brand, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) `name`,
(SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE cpsl.`product_id` = cpes.`entity_id` LIMIT 1) parent_id,
(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) parent_name, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) parent_model
FROM `catalog_product_entity` cpes
LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`entity_id` = cpes.`entity_id` AND cpei_source.`attribute_id` = 343
WHERE cpes.`has_options` = 0";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/".$filename)) unlink(dirname(__FILE__)."/".$filename);
$fp = fopen(dirname(__FILE__)."/".$filename, 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/'.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;



try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>
