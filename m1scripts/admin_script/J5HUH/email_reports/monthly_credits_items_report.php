<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

require 'monthly_config.php';

//$start_date = date('Y-m-d', strtotime('first day of last month'));
//$end_date = date('Y-m-d', strtotime('last day of last month'));
//$countries = array('AT','BE','DE','DK','ES','FI','FR','GR','IE','IT','SE');

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','melissaburton@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Credits Items Reports - ".$end_date;
$html = "Here are your monthly Credits Items reports.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Monthly Credits Items Reports - '.$end_date);


foreach ($countries as $country_id) {
echo $country_id;	
// Get Data
$sql_getdata = "SELECT sfoi.item_id, sfci.order_item_id, sfcm.entity_id `Invoice ID`, sfo.increment_id `Order Number`, concat(sfo.customer_firstname, \" \", sfo.customer_lastname) as `Customer name`, sfo.customer_email, sfcm.increment_id as `Credit Note No`,  sfcm.created_at as `Credit note Date`,  sfoa.country_id, sfci.base_row_total_incl_tax, sfo.store_name, replace(sfci.name,'\"','') `name`, sfci.base_discount_amount, sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount) `Value less discount`, sfoi.tax_percent, (sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount)) / (1+(sfoi.tax_percent/100)) `Net Discounted Amount`, 

concat(IF(month(sfcm.created_at) % 2 <> 0, month(sfcm.created_at)+1, month(sfcm.created_at)),year(sfcm.created_at)) as `Return Date`, concat(month(sfcm.created_at),year(sfcm.created_at)) `month_year`, sfci.order_item_id, cpei_tax.value,

csi.product_id, sfo.customer_group_id,

round(if(cpei_tax.value=5,(sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount)),(sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount))/1.2),2) `net based on catalog vat`,


if(sfo.customer_group_id=4, (sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount)),(round(if(cpei_tax.value=5,(sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount)),(sfci.base_row_total_incl_tax-if(sfci.base_discount_amount is null, 0, sfci.base_discount_amount))/1.2),2))) as `Net sales based on customer group`


FROM sales_creditmemo_item sfci 

left join sales_order_item sfoi on sfci.order_item_id = sfoi.item_id

left join sales_creditmemo sfcm on sfci.parent_id = sfcm.entity_id

left join sales_order sfo on sfcm.order_id = sfo.entity_id

left join sales_order_address sfoa on sfoa.parent_id = sfo.entity_id and sfoa.address_type = 'Shipping'

left join cataloginventory_stock_item csi on sfoi.product_id = csi.product_id

left join catalog_product_entity_int cpei_tax on csi.product_id = cpei_tax.entity_id and cpei_tax.attribute_id = 122 AND cpei_tax.store_id=0


where date(sfcm.created_at) >= '".$start_date."' AND date(sfcm.created_at) <= '".$end_date."' AND sfoa.country_id = '".$country_id."' and sfci.base_row_total<>'NULL'
ORDER BY `cpei_tax`.`value`  ASC";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) continue;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/credits-items-report - ".$country_id.' - '.$end_date.".csv")) unlink(dirname(__FILE__)."/credits-items-report - ".$country_id.' - '.$end_date.".csv");
$fp = fopen(dirname(__FILE__)."/credits-items-report - ".$country_id.' - '.$end_date.".csv", 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	//$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/credits-items-report - '.$country_id.' - '.$end_date.'.csv';
	//$file = $mail->createAttachment(file_get_contents($path));
	//$file ->type        = 'text/csv';
	//$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	//$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	//$file ->filename    = 'credits-items-report - '.$country_id.' - '.$end_date.'.csv';

}


try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>