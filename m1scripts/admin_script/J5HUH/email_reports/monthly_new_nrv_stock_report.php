<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('memory_limit', '4096M');
ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');


umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$start_date = date('Y-m-d', strtotime('first day of last month'));
$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "new-nrv-stock-report - ".$end_date.".csv";

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','melissaburton@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "NEW NRV Stock Report - ".$end_date;
$html = "Here is your monthly NEW NRV Stock report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('NEW NRV Stock Report - '.$end_date);


// Get Data
$sql_getdata = "SELECT eaov_source.`value` `source`, eaov_brand.`value` `brand`, cpe.`entity_id`, cpev_model.`value` `model`, cpev_name.`value` `name`, 

IF((SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1) IS NULL, cpe.`entity_id`, (SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1)) AS `parent_id`,

cpev_parent_model.`value` `parent_model`,
cpev_parent_name.`value` `parent_name`,

eas.`attribute_set_name`, eaov_type.`value` `product_sub_type`, cpev_bin.`value` `bin_location`, cpev_overstock.`value` `overstock_location`, cpev_origin.`value` `country_of_origin`, cpev_commodity.`value` `commodity_code`, cpe.`sku`, cpe.`created_at`, cpe.`updated_at`, FORMAT(cped_rrp.`value`,2) `rrp_inc_vat`, FORMAT(cped_price.`value`,2) `price_inc_vat`,  
tc.`class_name` `vat_class_name`, cpei_vat.`value` `vat_code`, 
FORMAT(IF(cpei_vat.`value` IN(2), cped_price.`value`/(1+((SELECT `rate` FROM `tax_calculation_rate` WHERE `tax_country_id` = 'GB' AND `code` = 'Standard Rate VAT')/100)), cped_price.`value`),2) `price ex vat`,
FORMAT(cped_cost.`value`,2) `cost_price_ex_vat`,
FORMAT(IF(IF(cpei_vat.`value` IN(2), cped_price.`value`/(1+((SELECT `rate` FROM `tax_calculation_rate` WHERE `tax_country_id` = 'GB' AND `code` = 'Standard Rate VAT')/100)), cped_price.`value`) - cped_cost.`value` < 0, IF(cpei_vat.`value` IN(2), cped_price.`value`/(1+((SELECT `rate` FROM `tax_calculation_rate` WHERE `tax_country_id` = 'GB' AND `code` = 'Standard Rate VAT')/100)), cped_price.`value`) - cped_cost.`value`, 0),2) `less_than_cost`, 

FORMAT(IF(IF(cpei_vat.`value` IN(2), cped_price.`value`/(1+((SELECT `rate` FROM `tax_calculation_rate` WHERE `tax_country_id` = 'GB' AND `code` = 'Standard Rate VAT')/100)), cped_price.`value`) - cped_cost.`value` < 0, IF(cpei_vat.`value` IN(2), cped_price.`value`/(1+((SELECT `rate` FROM `tax_calculation_rate` WHERE `tax_country_id` = 'GB' AND `code` = 'Standard Rate VAT')/100)), cped_price.`value`) - cped_cost.`value`, 0) * cisi.`qty`,2) `cost_vs_NRV`, 

FORMAT(cisi.`qty`,0) `qty`,

FORMAT(cped_cost.`value` * cisi.`qty`,2) `total_value`,

FORMAT(IFNULL(monthly_sales.`qty_sold`,0),0) `monthly_sales`,
FORMAT(IFNULL(part_1_yearly_sales.`qty_sold`,0) + IFNULL(part_2_yearly_sales.`qty_sold`,0),0) `yearly_sales`

FROM `catalog_product_entity` cpe
LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`entity_id` = cpe.`entity_id` AND cpei_source.`attribute_id` = 343 AND cpei_source.`store_id` = 0
LEFT JOIN `eav_attribute_option_value` eaov_source ON eaov_source.`option_id` = cpei_source.`value` AND eaov_source.`store_id` = 0 
LEFT JOIN `catalog_product_entity_decimal` cped_rrp ON cped_rrp.`entity_id` = cpe.`entity_id` and cped_rrp.`attribute_id` = 120 AND cped_rrp.`store_id` = 0
LEFT JOIN `catalog_product_entity_decimal` cped_price ON cped_price.`entity_id` = cpe.`entity_id` and cped_price.`attribute_id` = 75 AND cped_price.`store_id` = 0
LEFT JOIN `catalog_product_entity_decimal` cped_cost ON cped_cost.`entity_id` = cpe.`entity_id` and cped_cost.`attribute_id` = 293 AND cped_cost.`store_id` = 0
LEFT JOIN `catalog_product_entity_int` cpei_vat ON cpei_vat.`entity_id` = cpe.`entity_id` AND cpei_vat.`attribute_id` = 122 AND cpei_vat.`store_id`=0
LEFT JOIN `tax_class` tc ON tc.`class_id` = cpei_vat.`value`
LEFT JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id` = cpe.`entity_id`
LEFT JOIN `eav_attribute_set` eas ON eas.`attribute_set_id`=cpe.`attribute_set_id`
LEFT JOIN `catalog_product_entity_varchar` cpev_bin ON cpev_bin.`entity_id` = cpe.`entity_id` AND cpev_bin.`attribute_id`=136 AND cpev_bin.`store_id`=0
LEFT JOIN `catalog_product_entity_varchar` cpev_overstock ON cpev_overstock.`entity_id` = cpe.`entity_id` AND cpev_overstock.`attribute_id`=137 AND cpev_overstock.`store_id`=0
LEFT JOIN `catalog_product_entity_varchar` cpev_origin ON cpev_origin.`entity_id` = cpe.`entity_id` AND cpev_origin.`attribute_id`=427 AND cpev_origin.`store_id`=0
LEFT JOIN `catalog_product_entity_varchar` cpev_commodity ON cpev_commodity.`entity_id` = cpe.`entity_id` AND cpev_commodity.`attribute_id`=428 AND cpev_commodity.`store_id`=0
LEFT JOIN `catalog_product_entity_int` cpei_brand ON cpei_brand.`entity_id` = cpe.`entity_id` AND cpei_brand.`attribute_id` = 81 AND cpei_brand.`store_id` = 0
LEFT JOIN `eav_attribute_option_value` eaov_brand ON eaov_brand.`option_id` = cpei_brand.`value` AND eaov_brand.`store_id` = 0 
LEFT JOIN `catalog_product_entity_varchar` cpev_model ON cpev_model.`entity_id` = cpe.`entity_id` AND cpev_model.`attribute_id` = 134 AND cpev_model.`store_id`=0
LEFT JOIN `catalog_product_entity_varchar` cpev_name ON cpev_name.`entity_id` = cpe.`entity_id` AND cpev_name.`attribute_id` = 71 AND cpev_name.`store_id`=0

LEFT JOIN `catalog_product_entity_int` cpei_type ON cpei_type.`entity_id` = cpe.`entity_id` AND cpei_type.`store_id` = 0 AND cpei_type.`attribute_id`=
(CASE
     WHEN cpe.`attribute_set_id` = 9 THEN 198                                        /* bikes */
     WHEN cpe.`attribute_set_id` IN (11,44,47) THEN 329     /* cycleclothing / clothing / bras */
     WHEN cpe.`attribute_set_id` = 10 THEN 189                                      /* cycle footwear */
     WHEN cpe.`attribute_set_id` IN (43,46) THEN 340           /* accessories / socks */
     WHEN cpe.`attribute_set_id` = 48 THEN 264                                      /* nutrition */
     WHEN cpe.`attribute_set_id` = 12 THEN 193                                      /* cycle helmets */
     WHEN cpe.`attribute_set_id` = 13 THEN 194                                      /* cycle pumps */
     WHEN cpe.`attribute_set_id` = 14 THEN 195                                      /* cycle computers */
     WHEN cpe.`attribute_set_id` = 15 THEN 196                                      /* cycle pedals */
     WHEN cpe.`attribute_set_id` = 16 THEN 198                                      /* cycle mudguards */
     WHEN cpe.`attribute_set_id` = 17 THEN 201                                      /* cycle lights */
     WHEN cpe.`attribute_set_id` = 18 THEN 203                                      /* cycle tyres */
     WHEN cpe.`attribute_set_id` = 19 THEN 206                                      /* cycle locks */
     WHEN cpe.`attribute_set_id` = 20 THEN 208                                      /* cycle saddles */
     WHEN cpe.`attribute_set_id` = 21 THEN 210                                      /* cycle bottles */
     WHEN cpe.`attribute_set_id` = 22 THEN 211                                      /* cycle bags */
     WHEN cpe.`attribute_set_id` = 23 THEN 221                                      /* cycle generic */
     WHEN cpe.`attribute_set_id` = 24 THEN 213                                      /* cycle storage */
     WHEN cpe.`attribute_set_id` = 25 THEN 214                                      /* cycle tubes */
     WHEN cpe.`attribute_set_id` = 26 THEN 216                                      /* cycle wheels */
     WHEN cpe.`attribute_set_id` = 27 THEN 217                                      /* cycle bike racks */
     WHEN cpe.`attribute_set_id` = 28 THEN 219                                      /* cycle bar type */
     WHEN cpe.`attribute_set_id` = 29 THEN 221                                      /* cycle product filterable */
     WHEN cpe.`attribute_set_id` = 31 THEN 264                                      /* cycle nutrition */
     WHEN cpe.`attribute_set_id` = 34 THEN 198                                      /* cycle frameset */
     WHEN cpe.`attribute_set_id` = 35 THEN 198                                      /* cycle stems */
     WHEN cpe.`attribute_set_id` = 36 THEN 272                                      /* cycle global first aid */
     WHEN cpe.`attribute_set_id` = 37 THEN 221                                      /* cycle child seat */
     WHEN cpe.`attribute_set_id` = 38 THEN 273                                      /* cycle maintainance */
     WHEN cpe.`attribute_set_id` = 39 THEN 274                                      /* cycle trailers */
     WHEN cpe.`attribute_set_id` = 40 THEN 221                                      /* cycle bar tape */
     WHEN cpe.`attribute_set_id` = 41 THEN 198                                      /* cycle derailleurs */
     WHEN cpe.`attribute_set_id` = 42 THEN 221                                      /* cycle seatposts */
END)

LEFT JOIN `catalog_product_entity_varchar` cpev_type ON cpev_type.`entity_id` = cpe.`entity_id` AND cpev_type.`store_id` = 0 AND cpev_type.`attribute_id` =
(CASE
     WHEN cpe.`attribute_set_id` = 45 THEN 289                                      /* footwear */
END)

LEFT JOIN `eav_attribute_option_value` eaov_type ON eaov_type.`option_id` = IF(cpei_type.`attribute_id` IS NOT NULL, cpei_type.`value`, cpev_type.`value`) AND eaov_type.`store_id` = 0

LEFT JOIN `catalog_product_entity_varchar` cpev_parent_model ON cpev_parent_model.`entity_id` = IF((SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1) IS NULL, cpe.`entity_id`, (SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1)) AND cpev_parent_model.`attribute_id` = 134 AND cpev_parent_model.`store_id`=0

LEFT JOIN `catalog_product_entity_varchar` cpev_parent_name ON cpev_parent_name.`entity_id` = IF((SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1) IS NULL, cpe.`entity_id`, (SELECT `parent_id` FROM `catalog_product_super_link` WHERE `product_id` = cpe.`entity_id` ORDER BY `parent_id` DESC LIMIT 1)) AND cpev_parent_name.`attribute_id` = 71 AND cpev_parent_name.`store_id`=0

LEFT JOIN (SELECT sii.`product_id`, SUM(sii.`qty`) `qty_sold` FROM `sales_invoice_item` sii
INNER JOIN `sales_invoice` si ON si.`entity_id` = sii.`parent_id` 
INNER JOIN `sales_order` so ON so.`entity_id` = si.`order_id`
WHERE DATE(si.`created_at`) BETWEEN '2021-01-01' AND '2021-01-31' AND `so`.`customer_group_id` NOT IN (2,6,7,9) AND so.`store_id` NOT IN (23,29,30,31,32,33,34)
GROUP BY sii.`product_id`) `monthly_sales` ON monthly_sales.`product_id`= cpe.`entity_id`

LEFT JOIN (SELECT sii.`product_id`, SUM(sii.`qty`) `qty_sold` FROM `fitness`.`sales_flat_invoice_item` sii
INNER JOIN `fitness`.`sales_flat_invoice` si ON si.`entity_id` = sii.`parent_id` 
INNER JOIN `fitness`.`sales_flat_order` so ON so.`entity_id` = si.`order_id`
WHERE DATE(si.`created_at`) BETWEEN '2020-02-01' AND '2020-10-28' AND `so`.`customer_group_id` NOT IN (2,6,7,9) AND so.`store_id` NOT IN (23,29,30,31,32,33,34)
GROUP BY sii.`product_id`) `part_1_yearly_sales` ON part_1_yearly_sales.`product_id`= cpe.`entity_id`

LEFT JOIN (SELECT sii.`product_id`, SUM(sii.`qty`) `qty_sold` FROM `sales_invoice_item` sii
INNER JOIN `sales_invoice` si ON si.`entity_id` = sii.`parent_id` 
INNER JOIN `sales_order` so ON so.`entity_id` = si.`order_id`
WHERE DATE(si.`created_at`) BETWEEN '2020-10-29' AND '2021-01-31' AND `so`.`customer_group_id` NOT IN (2,6,7,9) AND so.`store_id` NOT IN (23,29,30,31,32,33,34)
GROUP BY sii.`product_id`) `part_2_yearly_sales` ON part_2_yearly_sales.`product_id`= cpe.`entity_id`

WHERE cpe.`sku` IS NOT NULL AND cpe.`type_id` = 'simple'";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/".$filename)) unlink(dirname(__FILE__)."/".$filename);
$fp = fopen(dirname(__FILE__)."/".$filename, 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/'.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;



try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>
