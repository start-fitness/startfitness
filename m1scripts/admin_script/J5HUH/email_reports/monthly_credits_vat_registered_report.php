<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

require 'monthly_config.php';

//$start_date = date('Y-m-d', strtotime('first day of last month'));
//$end_date = date('Y-m-d', strtotime('last day of last month'));
//$countries = array('AT','BE','DE','DK','ES','FI','FR','GR','IE','IT','SE');

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','melissaburton@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Credits VAT Registered Reports - ".$end_date;
$html = "Here are your monthly Credits VAT Registered reports.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Monthly Credits VAT Registered Reports - '.$end_date);


foreach ($countries as $country_id) {
echo $country_id;	
// Get Data
$sql_getdata = "SELECT sfcm.*
FROM sales_creditmemo sfcm
INNER JOIN sales_order as sfo on sfcm.order_id = sfo.entity_id
INNER join dimasoft_credit_vat_corrected dcvc on dcvc.credit_id = sfcm.entity_id
INNER JOIN sales_order_address sfoa ON sfoa.parent_id = sfo.entity_id AND sfoa.address_type = 'shipping'
WHERE  sfo.`customer_group_id`=4 AND date(sfcm.`created_at`)>='".$start_date."' AND date(sfcm.`created_at`)<='".$end_date."' AND sfoa.`country_id` = '".$country_id."' AND sfo.`status` <> 'canceled'";

//echo '<p>'.$sql_getdata;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);

if(empty($results_getdata)) continue;
//print_r(array_keys($results_getdata[0]));
//exit;


// Output data To CSV
if(is_file(dirname(__FILE__)."/credits-vat-registered-report - ".$country_id.' - '.$end_date.".csv")) unlink(dirname(__FILE__)."/credits-vat-registered-report - ".$country_id.' - '.$end_date.".csv");
$fp = fopen(dirname(__FILE__)."/credits-vat-registered-report - ".$country_id.' - '.$end_date.".csv", 'w');
$csvHeader = array();

foreach (array_keys($results_getdata[0]) as $keyname) {
	array_push($csvHeader,$keyname);	
}

//print_r($csvHeader);

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,","); 
foreach ($results_getdata as $data){
    fputcsv($fp, $data, ",");
}
fclose($fp);

//exit;

	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/credits-vat-registered-report - '.$country_id.' - '.$end_date.'.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'credits-vat-registered-report - '.$country_id.' - '.$end_date.'.csv';

}

try {
		//$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>