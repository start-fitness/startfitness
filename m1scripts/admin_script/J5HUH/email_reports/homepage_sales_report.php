<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


// Get webstie ID's
$fitness = '6,19';
//$football = '5,20';
//$cycles = '1,21';
//$wildtrak = '7';
//$moremile = '8';
$websites = array("$fitness");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$weekend = date("Y-m-d",strtotime('-3 days')); // Date before the weekend

if($day=='1'){
	// Day is monday so get weekend sales
	$date_from = $weekend;
	$date_to = $today;
}else{
	// Else every other day of the working week
	$date_from = $yesterday;
	$date_to = $today;
}

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#d4e3e5;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}</style>';
$html .= '<center><p>This homepage sales reports run from '.$date_from.' 9:00am till '.$date_to.' 8:59am</p></center>';
// The SQL Queries
foreach($websites as $website){

// The categorie ID's for each website homepage
if($website == $fitness){ $category_id = "1224"; $web_name = "Start Fitness"; }
if($website == $football){ $category_id = "410"; $web_name = "Start Football"; }
if($website == $cycles){ $category_id = "907"; $web_name = "Start Cycles"; }
if($website == $wildtrak){ $category_id = "1337"; $web_name = "WildTrak"; }
if($website == $moremile){ $category_id = "1481"; $web_name = "More Mile"; }


// Get Website Homepage
/*$sql_gethome = "SELECT `cpev_model`.`value` as 'Model', `cpev_name`.`value` as 'Name',`tmp`.`Qty_Sold` 
		  FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl 
		  WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(SELECT DISTINCT `parent_id` 
		  FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold 
		  FROM `sales_flat_order_item` sfoi LEFT JOIN `sales_flat_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` 
		  WHERE `sfo`.`store_id` IN (".$website.") AND `sfoi`.`product_type` = 'simple' AND (`sfoi`.`created_at` >= '".$date_from." 09:00:00' AND `sfoi`.`created_at` <= '".$date_to." 08:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) 
		  GROUP BY `parent_id` ORDER BY Qty_Sold Desc) tmp LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 
		  LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0 
		  INNER JOIN `catalog_category_product` ccp ON `ccp`.`product_id`=`tmp`.`parent_id` AND `ccp`.`category_id`=".$category_id." ORDER BY `tmp`.`Qty_Sold` Desc";
*/

$sql_gethome = "SELECT `cpev_model`.`value` as 'Model', `cpev_name`.`value` as 'Name', IFNULL(`tmp`.`Qty_Sold`,0) Qty_Sold FROM `catalog_category_product` ccp
		LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `ccp`.`product_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0
		LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `ccp`.`product_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0
		LEFT JOIN
		(
		SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold FROM `sales_flat_order_item` sfoi LEFT JOIN `sales_flat_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` WHERE `sfo`.`store_id` IN (".$fitness.") AND `sfoi`.`product_type` = 'simple' AND (`sfo`.`created_at` >= '".$date_from." 09:00:00' AND `sfo`.`created_at` <= '".$date_to." 08:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) AND sfo.`status` NOT IN ('pending_payment','canceled') GROUP BY `parent_id` ORDER BY Qty_Sold Desc
		) tmp ON `tmp`.`parent_id`=`ccp`.`product_id`
		WHERE `ccp`.`category_id`=".$category_id."
		ORDER BY `tmp`.`Qty_Sold` Desc";
		
// Fetch all reqults
$results_home = $read->fetchAll($sql_gethome);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$web_name.' Homepage Sales Report</h1>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento Homepage Report');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Homepage Report '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>
