<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::setIsDeveloperMode(true);
ini_set('memory_limit', '8096M');
ini_set('display_errors', 1);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


// Get Data
$sql_getdata = "SELECT cpei_source.`value` `source`, cpes.`entity_id`, cpes.`sku`, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=120 AND cped.`store_id`=0) rrp_inc_vat, (SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0) price_inc_vat,
IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,'Y','N') vat,
FORMAT(IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)),2) price_ex_vat,
(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) cost_price_ex_vat,
IF (IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0)<0,IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0),0) less_than_cost,
IF (IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0)<0,IF ((select cpei.`value` from `catalog_product_entity_int` cpei WHERE cpei.`attribute_id` = 122 AND cpei.`entity_id` = cpes.`entity_id` AND cpei.`store_id`=0)=2,(((SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0)/6)*5),(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=75 AND cped.`store_id`=0))-(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0),0) * (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) cost_vs_NRV,
(SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) qty, 
(SELECT DISTINCT `value` FROM `catalog_product_entity_decimal` cped WHERE cped.`entity_id` = cpes.`entity_id` and cped.`attribute_id`=293 AND cped.`store_id`=0) * (SELECT DISTINCT `qty` FROM `cataloginventory_stock_item` csi WHERE csi.`product_id`=cpes.`entity_id`) `total_value`,
(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) model, (select eaov.`value` from `catalog_product_entity_int` cpei INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id` AND ea.`attribute_id` = 81 INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = ea.`attribute_id` AND eao.`option_id` = cpei.`value` INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = 0 WHERE cpei.`entity_id` = cpes.`entity_id`) brand, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = cpes.`entity_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) `name`,
(SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE cpsl.`product_id` = cpes.`entity_id` LIMIT 1) parent_id,
(SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=71 AND cpev.`store_id`=0) parent_name, (SELECT DISTINCT `value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = `parent_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) parent_model
FROM `catalog_product_entity` cpes
LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`entity_id` = cpes.`entity_id` AND cpei_source.`attribute_id` = 343
WHERE cpes.`has_options` = 0";

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);

// Output data To CSV
if(is_file(dirname(__FILE__)."/tca-startfitness-inventory.csv")) unlink(dirname(__FILE__)."/tca-startfitness-inventory.csv");
$fp = fopen(dirname(__FILE__).'/tca-startfitness-inventory.csv', 'w');
$csvHeader = array("source", "entity_id", "sku","rrp_inc_vat","price_inc_vat","price_ex_vat","cost_price_ex_vat","qty","total_value","model","brand","name","parent_id","parent_name","parent_model");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getdata as $data){
    fputcsv($fp, array($data['source'], $data['entity_id'], $data['sku'], $data['rrp_inc_vat'], $data['price_inc_vat'], $data['price_ex_vat'], $data['cost_price_ex_vat'], $data['qty'], $data['total_value'], $data['model'], $data['brand'], $data['name'], $data['parent_id'], $data['parent_name'], $data['parent_model'] ), ",");
}
fclose($fp);

?>
