<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);


// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');


umask(0);
//Mage::setIsDeveloperMode(true);

//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$year = date("Y", strtotime('last day of last month'));
$month = date("m", strtotime('last day of last month'));
$day = date("d", strtotime('last day of last month'));

//$year = 2020;
//$month = 10;
//$day = 31;

//echo $year.' - '.$month;

// Get Data
$sql_getdata = "SELECT
sales_order.created_at, 
sales_order.customer_email, 
sales_order.entity_id, 
sales_order.status, 
sales_order.store_id, 
sales_order.base_grand_total, 
sales_order.base_subtotal_incl_tax, 
sales_order.total_item_count,
sales_order.base_subtotal, 
sales_order.increment_id, 
sales_order.base_shipping_incl_tax, 
if(dimasoft_order_vat_corrected.order_id IS NOT NULL, dimasoft_order_vat_corrected.base_tax_after_discount, sales_order.base_tax_amount) tax_after_discount,
sales_order_address.country_id, 
sales_order_address.city, 
sales_order_address.postcode,
sales_order.status, 
ifnull(sales_order.base_total_refunded,0) as 'gross_refunded',
ifnull((sales_order.base_total_refunded),0)-sum(ifnull(if(dimasoft_credit_vat_corrected.credit_id IS NOT NULL, dimasoft_credit_vat_corrected.base_tax_after_discount, sales_creditmemo.base_tax_amount),0)) net_refunded, 
sum(ifnull(if(dimasoft_credit_vat_corrected.credit_id IS NOT NULL, dimasoft_credit_vat_corrected.base_tax_after_discount, sales_creditmemo.base_tax_amount),0)) AS 'VAT refunded', 
sales_order.base_grand_total+ ifnull(sales_order.amstorecredit_base_amount,0)-if(dimasoft_order_vat_corrected.order_id IS NOT NULL, dimasoft_order_vat_corrected.base_tax_after_discount, sales_order.base_tax_amount) net_sale_before_refund 
FROM sales_order
LEFT JOIN sales_creditmemo ON sales_order.entity_id = sales_creditmemo.order_id 
LEFT JOIN dimasoft_order_vat_corrected ON sales_order.entity_id = dimasoft_order_vat_corrected.order_id 
LEFT JOIN sales_order_address ON sales_order_address.parent_id = sales_order.entity_id AND sales_order_address.address_type = 'shipping' 
LEFT JOIN dimasoft_credit_vat_corrected ON sales_creditmemo.entity_id = dimasoft_credit_vat_corrected.credit_id
WHERE YEAR(DATE(sales_order.created_at))=".$year." AND MONTH(DATE(sales_order.created_at)) =".$month." AND sales_order.`status` <> 'canceled'

AND sales_order.`entity_id` NOT IN (SELECT so.`entity_id` FROM `sales_invoice` si INNER JOIN `sales_order` so ON so.`entity_id` = si.`order_id` LEFT JOIN `sales_shipment` ss ON ss.`order_id` = so.`entity_id` WHERE DATE(si.`created_at`) BETWEEN '2020-11-01' AND '2020-11-30' AND si.`store_id` IN (24,26,35,25) AND so.`created_at` BETWEEN DATE_SUB(ss.`created_at`, INTERVAL 5 MINUTE) AND ss.`created_at` )

GROUP BY sales_order.entity_id";

//echo '<p>'.$sql_getdata; exit;

// Fetch all reqults
$results_getdata = $read->fetchAll($sql_getdata);
if(empty($results_getdata)) exit;

// Output data To CSV
if(is_file(dirname(__FILE__)."/intrastat-".$year."-".$month."-".$day.".csv")) unlink(dirname(__FILE__)."/intrastat-".$year."-".$month."-".$day.".csv");
$fp = fopen(dirname(__FILE__)."/intrastat-".$year."-".$month."-".$day.".csv", 'w');
$csvHeader = array("created_at", "customer_email", "entity_id","status","store_id","base_grand_total","base_subtotal_incl_tax","total_item_count","base_subtotal","increment_id","base_shipping_incl_tax","tax_after_discount","country_id","city","postcode","status","gross_refunded","net_refunded","VAT refunded","net_sale_before_refund");

//echo '<p>';
//var_dump($results_getdata);

fputcsv( $fp, $csvHeader,",");
foreach ($results_getdata as $data){
    fputcsv($fp, array($data['created_at'], $data['customer_email'], $data['entity_id'], $data['status'], $data['store_id'], $data['base_grand_total'], $data['base_subtotal_incl_tax'], $data['total_item_count'], $data['base_subtotal'], $data['increment_id'], $data['base_shipping_incl_tax'], $data['tax_after_discount'], $data['country_id'], $data['city'], $data['postcode'], $data['status'], $data['gross_refunded'], $data['net_refunded'], $data['VAT refunded'], $data['net_sale_before_refund'] ), ",");
}
fclose($fp);

//exit;
//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Intrastat Report - ".$year." - ".$month." - ".$day;
$html = "Here is your monthly Intrastat report.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Monthly Intrastat Report '.date("D d-M-Y"));


	$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/intrastat-'.$year.'-'.$month.'-'.$day.'.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'intrastat-'.$year.'-'.$month.'-'.$day.'.csv';


		try {
    			//$mail->send();
				echo "Success";
    			//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
		}
		catch (Exception $e) {
				echo "Failed to Send Email";
    			//Mage::getSingleton('core/session')->addError('Unable to send.');
		}
		

?>