<?php
// Start Group Script To Get In Stock Items SET OOS and Vice Versa
// Designed and built by James Simpson
// V1.0

error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #000;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#FF6717;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}.tftable tr {background-color:#FFC6B8;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #000;}</style>';

// The SQL Queries
$sql_get_items = "SELECT sfo.`entity_id`, sfo.`created_at`, sfo.`increment_id`, sfo.`status`, sfoi.`name`, sfoi.`qty_ordered` FROM `sales_flat_order_item` sfoi
		INNER JOIN `sales_flat_order` sfo ON sfo.`entity_id` = sfoi.`order_id`
		WHERE sfo.`created_at` >= '2019-01-28 00:00:00' AND sfoi.`name` REGEXP '^(Mavic|Fox|RockShox|Hope).*$' AND sfoi.`product_type` = 'simple' AND sfo.`status` NOT IN ('complete','canceled','closed','processing_return')";

// Fetch all reqults
$results_items = $read->fetchAll($sql_get_items);

if (!sizeof($results_items)) { echo 'No Results'; exit; }

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">Fox - Mavic - RockShox Orders</h1>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th>Date</th><th>Order #</th><th>Status</th><th>Name</th><th>Qty</th></tr>';
foreach($results_items as $items){

$html .= '<tr><td>'.$items['created_at'].'</td><td><a href="https://admin.startfitness.co.uk/index.php/startfitnessadmin/sales_order/view/order_id/'.$items['entity_id'].'/" target="_blank">'.$items['increment_id'].'</a></td><td>'.$items['status'].'</td><td>'.$items['name'].'</td><td>'.$items['qty_ordered'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';




//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk','danielmeggison@startfitness.co.uk','braddobbing@startfitness.co.uk');
//$sendToEmailAddress = array('gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','lindsayhedley@startfitness.co.uk','carlsmith@startfitness.co.uk');

$sendToName = 'Start Fitness';

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Order Watch');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Order Watch - '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>
