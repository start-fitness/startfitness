<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 1);
error_reporting(E_ALL);

// First connect up to Mage
//require_once(dirname(__FILE__).'/../../../app/Mage.php'); //Path to Magento (CRON)

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::setIsDeveloperMode(true);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$count = 0;

$end_date = date('Y-m-d', strtotime('last day of last month'));
$filename = "monthly_files_".$end_date.".zip";


$path = $dir.'/m1scripts/admin_script/J5HUH/email_reports/';

$zip = new ZipArchive();
$ret = $zip->open($path.$filename, ZipArchive::CREATE);
if ($ret !== TRUE) {
    printf('Failed with code %d', $ret);
} else {
    $directory = realpath('.');
    $options = array('add_path' => 'files/', 'remove_path' => $directory);
    $zip->addPattern('/.*'.$end_date.'\.csv$/', $directory, $options);
    $zip->close();
}

//exit;

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$message = "Monthly Files - ".$end_date;
$html = "Here is the link to your monthly files.<p>https://admin.startfitness.co.uk/script/J5HUH/email_reports/".$filename;
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom($sendFromEmailAddress, 'Start Fitness Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Monthly Files - '.$end_date);
	
	/*$dir = Mage::getBaseDir();
	$path = $dir.DS.'script'.DS.'J5HUH'.DS.'email_reports'.DS.$filename;
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = $filename;
*/

try {
		$mail->send();
		echo "Success";
		//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
}
catch (Exception $e) {
		echo "Failed to Send Email";
		//Mage::getSingleton('core/session')->addError('Unable to send.');
}

?>