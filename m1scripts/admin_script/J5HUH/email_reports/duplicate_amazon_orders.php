<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0

ini_set('display_errors', 0);
//error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/vhosts/admin.startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
//require_once('/microcloud/domains/fitnes/domains/admin.startfitness.co.uk/http/app/Mage.php'); //Path to Magento

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');

umask(0);
//Mage::app();

// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

$yesterday = date("Y-m-d", strtotime("yesterday")); // Yesterdays Date mysql format
$today = date("Y-m-d");

$title = "Duplicate Amazon Orders - Since: ".$yesterday;

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}
table,
  td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
  }</style>';
// The SQL Queries

// Get Data
$sql_getsales = "SELECT * FROM `m2epro_amazon_order`
WHERE DATE(purchase_create_date) BETWEEN '".$yesterday."' AND '".$today."' AND amazon_order_id IN (
SELECT amazon_order_id FROM `m2epro_amazon_order`
GROUP BY `amazon_order_id`
HAVING COUNT(amazon_order_id)>1)";

//echo $sql_getsales; exit;
// Fetch all reqults
$results = $read->fetchAll($sql_getsales);
if(empty($results)) exit;
//print_r($results); 

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$title.'</h1>';

$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Amazon Order ID</th><th>Selling Manager ID</th><th>Buyer Name</th></tr>';
foreach($results as $home){
	$html .= '<tr><td>'.$home['amazon_order_id'].'</td><td>'.$home['selling_manager_id'].'</td><td>'.$home['buyer_name'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk','michaeltaggart@startfitness.co.uk');
//$sendToEmailAddress = array('michaeltaggart@startfitness.co.uk','braddobbing@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk','kieranreay@startfitness.co.uk','danielmeggison@startfitness.co.uk');
$sendFromEmailAddress = $objectManager->get('Magento\Framework\App\Config\ScopeConfigInterface')->getValue('trans_email/ident_general/email');
$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom($sendFromEmailAddress, 'Magento '.$title);
			//$mail->setFrom("customerservices@startfitness.co.uk");
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Duplicate Amazon Orders - Since: '.$yesterday);
			$mail->send();

echo $html;
echo "Success";

?>
