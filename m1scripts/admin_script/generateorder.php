<?php
ini_set('display_errors',1); 
 error_reporting(E_ALL);
require_once('../app/Mage.php'); //Path to Magento
$store_ids = array();
$allStores = Mage::app()->getStores();

foreach ($allStores as $_eachStoreId => $val)
{
	$store_ids[] = $_eachStoreId;
}

$passcode = 'klsh7890ehgiudfjlbcx76t345dsjvkjkhdf78954hb';
if($_GET['passcode'] != $passcode){
	echo "<h1>Access Denied</h1>";
	die;
}

$argument_errors = array();
if($_GET['storeid'] == '' || $_GET['storeid'] == null){
	$argument_errors[] = 'No Store ID Provided';
	
}
elseif(!in_array($_GET['storeid'], $store_ids)){
	$argument_errors[] = 'Store ID is not valid';
}
else{
	Mage::app()->setCurrentStore($_GET['storeid']);	
}

if($_GET['quoteid'] == '' || $_GET['quoteid'] == null){
	$argument_errors[] = 'No Quote ID Provided';
}
elseif(!$quoteObj=Mage::getModel('sales/quote')->load($_GET['quoteid'])){
	$argument_errors[] = 'Quote ID is not valid';
}

?>


<?php if(!empty($argument_errors)): ?>
	<h2>There is a problem with the arguments passed</h2>
	<br/>
	<ul>
	<?php foreach($argument_errors as $error): ?>
		<li><?php echo $error; ?></li>
	<?php endforeach; ?>
	</ul>
	<?php die; ?>
<?php else: ?>
	<?php convert_to_sales_order($quoteObj); ?>
<?php endif; ?>


<?php
function convert_to_sales_order($quoteObj){
		// get all grouped products

	    $items=$quoteObj->getAllItems();
	    $quoteObj->collectTotals();
	    $quoteObj->reserveOrderId();
	
	    $quotePaymentObj=$quoteObj->getPayment();
		//print_r($quoteObj->getData()); die;
	    //methods: authorizenet, paypal_express, googlecheckout, purchaseorder
	    $quotePaymentObj->setMethod('purchaseorder');
	    $quoteObj->setPayment($quotePaymentObj);
	    $convertQuoteObj=Mage::getSingleton('sales/convert_quote');
	    $orderObj = $convertQuoteObj->addressToOrder($quoteObj->getShippingAddress());
	    $orderPaymentObj=$convertQuoteObj->paymentToOrderPayment($quotePaymentObj);
	
	    $orderObj->setBillingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getBillingAddress()));
	    $orderObj->setShippingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getShippingAddress()));
	    $orderObj->setPayment($convertQuoteObj->paymentToOrderPayment($quoteObj->getPayment()));
	
	    
	    foreach ($items as $item) {
	        //@var $item Mage_Sales_Model_Quote_Item
	        $orderItem = $convertQuoteObj->itemToOrderItem($item);
	        if ($item->getParentItem()) {
	            $orderItem->setParentItem($orderObj->getItemByQuoteItemId($item->getParentItem()->getId()));
	        }
	        $orderObj->addItem($orderItem);
	    }
	    
	    $orderObj->setCanShipPartiallyItem(false);
	    $totalDue=$orderObj->getTotalDue();
	    $orderObj->place(); //calls _placePayment
	    
	      try {
		      $orderObj->save(); 
			  $orderId=$orderObj->getId();
			  echo "<br/>Order successfully created: " .  $orderId;
		    } catch (Exception $e){
		      echo "<br/>" . $e->getMessage();  
			}
	}

?>