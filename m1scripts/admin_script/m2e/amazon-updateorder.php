<?php 
include(dirname(__FILE__)."/.config.inc.php");
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
umask(0);
$store_id = 0;
Mage::app()->setCurrentStore($store_id);


$serviceUrl = "https://mws-eu.amazonservices.com/Orders/2013-09-01";

 $config = array (
   'ServiceURL' => $serviceUrl,
   'ProxyHost' => null,
   'ProxyPort' => -1,
   'ProxyUsername' => null,
   'ProxyPassword' => null,
   'MaxErrorRetry' => 3,
 );
 $service = new MarketplaceWebServiceOrders_Client(
        AWS_ACCESS_KEY_ID,
        AWS_SECRET_ACCESS_KEY,
        APPLICATION_NAME,
        APPLICATION_VERSION,
        $config);
$count = 0;
$missingorders = array();
$missingorderstxt = array();
foreach($marketplaces as $index=>$marketplace){
//if($index != "ES") continue;
 echo "doing marketplace $index \n";
 $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
 $datetime = (new DateTime())->modify("- 1 days");
 $datetime2 = (new DateTime())->modify("-  3 hours");
 $request->setSellerId(MERCHANT_ID);
 $request->setMWSAuthToken(MWS_AUTH_TOKEN);
 $request->setLastUpdatedAfter($datetime->format(DateTime::ATOM));
 $request->setLastUpdatedBefore($datetime2->format(DateTime::ATOM));
 $request->setMarketplaceId($marketplace); 
 // object or array of parameters
 $orders = invokeListOrders($service, $request);
 

 //var_dump($orders);exit;
 
 if(isset($orders->ListOrdersResult->Orders->Order)){
	 foreach($orders->ListOrdersResult->Orders->Order as $order){
	 	//var_dump($order);exit;
	 	$checkdb = checkInMagento($order->AmazonOrderId);
	 	if(!$checkdb){
	 		//echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
			$missingorderstxt [] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
			$missingorders[] = $order->AmazonOrderId;
			//var_dump($order);
	 	}
		else{
			if($checkdb['magento_order_id'] == NULL && $checkdb['status'] == 0 && ($order->OrderStatus != "Pending" && $order->OrderStatus != "PendingAvailability") ){
				//echo "ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
				$missingorders[] = $order->AmazonOrderId;
				$missingorderstxt[] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
				
				buildUpOrder($order->AmazonOrderId, $order, $service, $marketplace);
				//var_dump($order);exit;
			}
		}
		$count++;
	 }
 }
  //echo "Sleeping for 30sec to avoid API throttling\n";
  sleep(65); 
 $token = isset($orders->ListOrdersResult->NextToken)? $orders->ListOrdersResult->NextToken : null;
 //var_dump($token);exit;
 while($token != null){
 	$request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
	$request->setSellerId(MERCHANT_ID);
	$request->setMWSAuthToken(MWS_AUTH_TOKEN);
 	$request->setNextToken($token);
	 // object or array of parameters
	$orders = invokeListOrderByNextToken($service, $request);
	//var_dump($orders);exit;
	if(isset($orders->ListOrdersByNextTokenResult->Orders->Order)){
		foreach($orders->ListOrdersByNextTokenResult->Orders->Order as $order){
		 	//var_dump($order->SalesChannel);
		 	$checkdb = checkInMagento($order->AmazonOrderId);
		 	if(!$checkdb){
		 		//echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
				$missingorderstxt [] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
				$missingorders[] = $order->AmazonOrderId;
				//var_dump($order);
		 	}
			else{
				if($checkdb['magento_order_id'] == NULL && $checkdb['status'] == 0 && ($order->OrderStatus != "Pending" && $order->OrderStatus != "PendingAvailability") ){
					//echo "ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
					$missingorders[] = $order->AmazonOrderId;
					$missingorderstxt[] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
					
					buildUpOrder($order->AmazonOrderId, $order, $service, $marketplace);
					//var_dump($order);exit;
				}
			}
			$count++;
	 	}
	}
	$token = isset($orders->ListOrdersByNextTokenResult->NextToken)? $orders->ListOrdersByNextTokenResult->NextToken : null;
	//echo "Sleeping for 30sec to avoid API throttling\n";
	sleep(65); 
 }
}
if(count($missingorders) == 0)
exit;

$mail = new PHPMailer;
$email ="richard@rawtechnologies.co.uk";
/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
*/
$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
$mail->addAddress($email);  // Add a recipient
//$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
$mail->addAddress("geoffrey@rawtechnologies.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Missing Amazon Orders';
$mail->Body    = 

'
<p>The following amazon orders are currently missing from Magento: (Script checked '.$count.' orders)</p>
<p>'.implode("<br />", $missingorders).'<p>
<p>'.implode("<br />", $missingorderstxt).'<p>
';

if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';
 var_dump("Number of orders found: ".$count);
 
 
 function checkInMagento($id){
 	
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	
	$sql = "SELECT m2epro_amazon_order.amazon_order_id, m2epro_amazon_order.status, m2epro_amazon_order.paid_amount, m2epro_order.magento_order_id FROM `m2epro_amazon_order` 
	JOIN `m2epro_order` ON m2epro_order.id = m2epro_amazon_order.order_id
	WHERE m2epro_amazon_order.amazon_order_id = '{$id}'";
	
	$results = $readConnection->fetchAll($sql);
	
	if(count($results)> 0)
		return $results[0];
	else {
		return false;
	}
 }
 
 function buildUpOrder($amazon_id, $order, $service, $marketplace){
 	
	$request = new MarketplaceWebServiceOrders_Model_GetOrderRequest();
 	$request->setSellerId(MERCHANT_ID);
	$request->setMWSAuthToken(MWS_AUTH_TOKEN);
	$request->setAmazonOrderId($amazon_id);
	$order = invokeGetOrder($service, $request);
	
	$request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest();
 	$request->setSellerId(MERCHANT_ID);
	$request->setMWSAuthToken(MWS_AUTH_TOKEN);
	$request->setAmazonOrderId($amazon_id);
	$order_items = invokeListOrderItems($service, $request);
	
	updateOrderData($amazon_id, $order->GetOrderResult->Orders->Order, $order_items->ListOrderItemsResult->OrderItems->OrderItem);
	//var_dump();exit;
	
 }
 
function updateOrderData($amazon_id, $order, $items){
	if(!is_array($items)) $items = array($items);	
	
	//var_dump($items);exit;
	$shipping = 0;
	$tax = array("product" => 0, "shipping"=>0, "gift"=>0);		
	$discount = array("shipping" => 0, "promotion"=>0);		
	
	$amazon_order = Mage::getModel('M2ePro/amazon_order')->getCollection()
          ->addFieldToFilter('amazon_order_id', $amazon_id)
          ->getFirstItem();
	
	
	//$itemsql = "SELECT * FROM "
	foreach($items as $item){
		
		$tax['product'] += $item->ItemTax->Amount;
		$tax['shipping'] += $item->ShippingTax->Amount;
		//$tax['gift'] += $item->ShippingTax->Amount;
		$discount['shipping'] += $item->ShippingDiscount->Amount;
		$discount['promotion'] += $item->PromotionDiscount->Amount;
		$shipping += $item->ShippingPrice->Amount;
		
		$amazon_order_item = Mage::getModel('M2ePro/amazon_order_item')->getCollection()
          ->addFieldToFilter('amazon_order_item_id', $item->OrderItemId)
          ->getFirstItem();
		  //var_dump($amazon_order_item->getData());
		if($amazon_order_item && ($amazon_order_item->getData("amazon_order_item_id"))){
			
			$amazon_order_item->setData("price",$item->ItemPrice->Amount - $item->ItemTax->Amount );
			try{
				$amazon_order_item->save();
			}
			catch(exception $e){
				var_dump($e->getMessage());
			}
			//var_dump($amazon_order_item->getData());
		}
		
	}

	//exit;
	//var_dump($order->ShippingAddress);		
	$shippingaddress = array(
	"country" => "",
	"country_code" => $order->ShippingAddress->CountryCode,
	"state" => $order->ShippingAddress->StateOrRegion,
	"city" => $order->ShippingAddress->City,
	"postal_code" => $order->ShippingAddress->PostalCode,
	"recipient_name" => $order->ShippingAddress->Name,
	"phone" => $order->ShippingAddress->Phone,
	"company" => "",
	"street" => array()
	);
	for($i =1; $i <= 3; $i++){
		$addrline = "AddressLine".$i;
		if(isset($order->ShippingAddress->$addrline)) $shippingaddress['street'][] = $order->ShippingAddress->$addrline;
	}
	$orderdata = array(
	"shipping_address" => json_encode($shippingaddress),
	"buyer_name" => $order->BuyerName,
	"buyer_email" => $order->BuyerEmail,
	"shipping_service" => $order->ShipServiceLevel,
	"shipping_price"=> $shipping,
	"shipping_dates"=> '{"ship":{"from":"'.$order->EarliestShipDate.'","to":"'.$order->LatestShipDate.'"},"delivery":{"from":"'.$order->EarliestDeliveryDate.'","to":"'.$order->LatestDeliveryDate.'"}}',
	"paid_amount"=> $order->OrderTotal->Amount,
	"tax_details"=> json_encode($tax),
	"discount_details"=> json_encode($discount),
	"currency"=> $order->OrderTotal->CurrencyCode,
	"qty_unshipped"=> $order->NumberOfItemsUnshipped,
	"status" => 1
	);
	foreach($orderdata as $key=>$val){
		$amazon_order->setData($key, $val);
	}
	try{
		$amazon_order->save();
	}
	catch(exception $e){
		
	}
	//var_dump($amazon_order->getData());//exit;
	
}
 
 
 function invokeGetOrder(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
      	if(!$service) throw new MarketplaceWebServiceOrders_Exception("issue with service", 1);
		  
        $response = $service->GetOrder($request);

        //echo ("Service Response\n");
        //echo ("=============================================================================\n");

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
		$xml = simplexml_load_string($dom->saveXML());
        return json_decode(json_encode($xml));

     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
  
  function invokeListOrderItems(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
        $response = $service->ListOrderItems($request);

        //echo ("Service Response\n");
        //echo ("=============================================================================\n");

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = simplexml_load_string($dom->saveXML());
        return json_decode(json_encode($xml));

     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
/**
  * Get List Orders Action Sample
  * Gets competitive pricing and related information for a product identified by
  * the MarketplaceId and ASIN.
  *
  * @param MarketplaceWebServiceOrders_Interface $service instance of MarketplaceWebServiceOrders_Interface
  * @param mixed $request MarketplaceWebServiceOrders_Model_ListOrders or array of parameters
  */
  function invokeListOrders(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
        $response = $service->ListOrders($request);
        //echo ("Service Response\n");
        //echo ("=============================================================================\n");
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = simplexml_load_string($dom->saveXML());
		return json_decode(json_encode($xml));
        //echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

  function invokeListOrderByNextToken(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
        $response = $service->ListOrdersByNextToken($request);

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = simplexml_load_string($dom->saveXML());
		return json_decode(json_encode($xml));

     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
