<?php 
use Magento\Framework\App\Bootstrap;
include(dirname(__FILE__)."/.config.inc.php");
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');
require __DIR__ . '/../../../app/bootstrap.php';
require dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();


$serviceUrl = "https://mws-eu.amazonservices.com/Orders/2013-09-01";
$serviceUrlUS = "https://mws.amazonservices.com/Orders/2013-09-01";

 $config = array (
   'ServiceURL' => $serviceUrl,
   'ProxyHost' => null,
   'ProxyPort' => -1,
   'ProxyUsername' => null,
   'ProxyPassword' => null,
   'MaxErrorRetry' => 3,
 );
 
  $configUS = array (
   'ServiceURL' => $serviceUrlUS,
   'ProxyHost' => null,
   'ProxyPort' => -1,
   'ProxyUsername' => null,
   'ProxyPassword' => null,
   'MaxErrorRetry' => 3,
 );
 $serviceEU = new MarketplaceWebServiceOrders_Client(
        AWS_ACCESS_KEY_ID,
        AWS_SECRET_ACCESS_KEY,
        APPLICATION_NAME,
        APPLICATION_VERSION,
        $config);
		
 $serviceUS = new MarketplaceWebServiceOrders_Client(
        AWS_ACCESS_KEY_ID_US,
        AWS_SECRET_ACCESS_KEY_US,
        APPLICATION_NAME,
        APPLICATION_VERSION,
        $configUS);
$count = 0;
$missingorders = array();
$missingorderstxt = array();


foreach($marketplaces as $index=>$marketplace){
	$merchanid = ($index == "CA" || $index == "US")? MERCHANT_ID_US: MERCHANT_ID;
$service = ($index == "CA" || $index == "US")? $serviceUS: $serviceEU;


//var_dump($merchanid);exit;
 echo "doing marketplace $index \n";
 $request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
 $datetime = (new DateTime())->modify("- 2 days");
 $datetime2 = (new DateTime())->modify("-  3 hours");
 $request->setSellerId($merchanid);
 if($index != "CA" && $index != "US") $request->setMWSAuthToken(MWS_AUTH_TOKEN);
 $request->setLastUpdatedAfter($datetime->format(DateTime::ATOM));
 $request->setLastUpdatedBefore($datetime2->format(DateTime::ATOM));
 $request->setMarketplaceId($marketplace); 
 // object or array of parameters
 $orders = invokeListOrders($service, $request);
 

 //var_dump($orders);exit;
 
 if(isset($orders->ListOrdersResult->Orders->Order)){
	 foreach($orders->ListOrdersResult->Orders->Order as $order){
	 	echo $order->AmazonOrderId." processing ...\n";
	 	if($order->OrderStatus == "Shipped") continue;
	 	$checkdb = checkInMagento($read, $order->AmazonOrderId);
	 	if(!$checkdb){
	 		echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
			$missingorderstxt [] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
			$missingorders[] = $order->AmazonOrderId;
			//var_dump($order);
	 	}
		else{
			if($checkdb['magento_order_id'] == NULL && $checkdb['status'] == 0 && ($order->OrderStatus != "Pending" && $order->OrderStatus != "PendingAvailability") ){
				echo "ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
				$missingorders[] = $order->AmazonOrderId;
				$missingorderstxt[] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
				
			}
		}
		$count++;
	 }
 }
  echo "Sleeping for 30sec to avoid API throttling\n";
  sleep(35); 
 $token = isset($orders->ListOrdersResult->NextToken)? $orders->ListOrdersResult->NextToken : null;
 //var_dump($token);exit;
 while($token != null){
 	$request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest();
	$request->setSellerId($merchanid);
	if($index != "CA" && $index != "US") $request->setMWSAuthToken(MWS_AUTH_TOKEN);
 	$request->setNextToken($token);
	 // object or array of parameters
	$orders = invokeListOrderByNextToken($service, $request);
	//var_dump($orders);exit;
	if(isset($orders->ListOrdersByNextTokenResult->Orders->Order)){
		foreach($orders->ListOrdersByNextTokenResult->Orders->Order as $order){
			echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
		 	//var_dump($order->SalesChannel);
		 	$checkdb = checkInMagento($read,$order->AmazonOrderId);
		 	if(!$checkdb){
		 		echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
				$missingorderstxt [] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
				$missingorders[] = $order->AmazonOrderId;
				//var_dump($order);
		 	}
			else{
				if($checkdb['magento_order_id'] == NULL && $checkdb['status'] == 0 && ($order->OrderStatus != "Pending" && $order->OrderStatus != "PendingAvailability") ){
					echo "ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
					$missingorders[] = $order->AmazonOrderId;
					$missingorderstxt[] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
					
				}
			}
			$count++;
	 	}
	}
	$token = isset($orders->ListOrdersByNextTokenResult->NextToken)? $orders->ListOrdersByNextTokenResult->NextToken : null;
	//echo "Sleeping for 30sec to avoid API throttling\n";
	sleep(65); 
 }
}
if(count($missingorders) == 0)
	exit;

$mail = new \PHPMailer;
$email ="richard@rawtechnologies.co.uk";
/*$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
*/
$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
$mail->addAddress($email);  // Add a recipient
//$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
$mail->addAddress("geoffrey@rawtechnologies.co.uk");  // Add a recipient
$mail->addAddress("gary@startfitness.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Missing Amazon Orders';
$mail->Body    = 

'
<p>The following amazon orders are currently missing from Magento: (Script checked '.$count.' orders)</p>
<p>'.implode("<br />", $missingorders).'<p>
<p>'.implode("<br />", $missingorderstxt).'<p>
';

if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';
 var_dump("Number of orders found: ".$count);
 
 
 function checkInMagento($read, $id){
 	
	//$resource = Mage::getSingleton('core/resource');
	$readConnection = $read;
	
	$sql = "SELECT m2epro_amazon_order.amazon_order_id, m2epro_amazon_order.status, m2epro_amazon_order.paid_amount, m2epro_order.magento_order_id FROM `m2epro_amazon_order` 
	JOIN `m2epro_order` ON m2epro_order.id = m2epro_amazon_order.order_id
	WHERE m2epro_amazon_order.amazon_order_id = '{$id}'";
	
	$results = $readConnection->fetchAll($sql);
	
	if(count($results)> 0)
		return $results[0];
	else {
		return false;
	}
 }
/**
  * Get List Orders Action Sample
  * Gets competitive pricing and related information for a product identified by
  * the MarketplaceId and ASIN.
  *
  * @param MarketplaceWebServiceOrders_Interface $service instance of MarketplaceWebServiceOrders_Interface
  * @param mixed $request MarketplaceWebServiceOrders_Model_ListOrders or array of parameters
  */
  function invokeListOrders(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
        $response = $service->ListOrders($request);
        //echo ("Service Response\n");
        //echo ("=============================================================================\n");
        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = simplexml_load_string($dom->saveXML());
		return json_decode(json_encode($xml));
        //echo("ResponseHeaderMetadata: " . $response->getResponseHeaderMetadata() . "\n");
     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }

  function invokeListOrderByNextToken(MarketplaceWebServiceOrders_Interface $service, $request)
  {
      try {
        $response = $service->ListOrdersByNextToken($request);

        $dom = new DOMDocument();
        $dom->loadXML($response->toXML());
        $dom->preserveWhiteSpace = false;
        $dom->formatOutput = true;
        $xml = simplexml_load_string($dom->saveXML());
		return json_decode(json_encode($xml));

     } catch (MarketplaceWebServiceOrders_Exception $ex) {
        echo("Caught Exception: " . $ex->getMessage() . "\n");
        echo("Response Status Code: " . $ex->getStatusCode() . "\n");
        echo("Error Code: " . $ex->getErrorCode() . "\n");
        echo("Error Type: " . $ex->getErrorType() . "\n");
        echo("Request ID: " . $ex->getRequestId() . "\n");
        echo("XML: " . $ex->getXML() . "\n");
        echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
     }
 }
