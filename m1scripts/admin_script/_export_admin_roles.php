<?php
/*
 * File to control access to scripts via IP
 * IPs set in site system configuration, Pinpoint_General module.
 */
require_once('../app/Mage.php');
Mage::setIsDeveloperMode(true);
umask(0);
Mage::app();
ini_set('max_execution_time', 3600);
ini_set('memory_limit', '512M');
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

try{
    echo 'Admin Role,Admin Name';
    echo PHP_EOL;

    $roles = Mage::getModel('admin/roles')->getCollection();
    foreach ($roles as $role):
        $roles_users = Mage::getModel('admin/roles')->load($role->getId())->getRoleUsers();
        foreach ($roles_users as $roleuser):
            $user = Mage::getModel('admin/user')->load($roleuser);
            echo $role->getRoleName() . ',' . $user->getFirstname() . ' ' . $user->getLastname();
            echo PHP_EOL;
        endforeach;
    endforeach;

}catch(Exception $e){
    echo $e->getMessage();
}

exit;

