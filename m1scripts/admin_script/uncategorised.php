<?php

ini_set('display_errors', 1);

ini_set('display_startup_errors', 1);

error_reporting(E_ALL);



ini_set('memory_limit', '3024M');

require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento

require dirname(__FILE__).'/phpmailer/class.phpmailer.php';

umask(0);

Mage::app();



$stores = array(

//	array("bikes_web", 1, 1),

//	array("football_en", 5, 2),

	array("fitness_en", 6, 3),

//	array("outdoors_en", 7, 4),

	array("moremile_en", 8, 5),

//	array("sportssize_store", 28, 9)


);



foreach ($stores as $store) {
	call_user_func_array('generate',$store);
}







function generate($store_code, $store_id, $website_id) {

	/**

	 * Get the resource model

	 */

	$resource = Mage::getSingleton('core/resource');

	

	/**

	 * Retrieve the read connection

	 */

	$readConnection = $resource->getConnection('core_read');

	

	$query = "SELECT DISTINCT(cpe.entity_id), cpe.type_id, cpe.sku, cpev.value, stock.is_in_stock FROM `catalog_product_entity` as cpe
INNER JOIN `catalog_product_entity_int` AS `visibility`
ON `visibility`.`entity_id` = `cpe`.`entity_id`
INNER JOIN catalog_product_entity_varchar as cpev
ON cpev.`entity_id` = cpe.entity_id and cpev.store_id = 0
INNER JOIN catalog_product_website as cpw
on cpw.product_id = cpe.entity_id

INNER JOIN cataloginventory_stock_item as stock
on stock.product_id = cpe.entity_id

WHERE (`visibility`.`attribute_id` = '102')
    AND (`visibility`.`store_id` = ".$store_id." OR `visibility`.`store_id` = 0)
	AND (`visibility`.`value` = 4)
	AND cpe.entity_id NOT IN (select product_id from catalog_category_product as ccp
	WHERE `ccp`.category_id IN (
		SELECT entity_id FROM catalog_category_flat_store_".$store_id." WHERE children_count = 0
	)
)
AND cpw.website_id = ".$website_id."
AND cpev.attribute_id = 71";

	

	/**

	 * Execute the query and store the results in $results

	 */

	$results = $readConnection->fetchAll($query);

	

	/**

	 * Print out the results

	 */

	ob_start();

	?>

	<html>

		<head>

			  <!-- Compiled and minified CSS -->

			  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">

			

			  <!-- Compiled and minified JavaScript -->

			  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>

		</head>

		<body> 

			<?php echo "<pre>Total: ".count($results)."</pre>"; ?>

			<?php if(count($results) > 500) {

				echo "<pre>Max 500 rows in preview. ".(count($results) - 500)." items hidden.</pre>";

			} ?>

			

			<br />

			<table>

				<tr>

					<th>Entity ID</th>

					<th>Type</th>

					<th>Sku</th>

					<th>Value</th>

					<th>In Stock?</th>

				</tr>

			<?php 

			foreach ($results as $row) {

				?>

				<tr>

					<?php foreach ($row as $key => $value) {

						if($count <= 500) {

							echo "<td>".$value."</td>";

						} else {

							break;

						}

						$count++;

					} ?>

				</tr>

				<?php

			}

			?>

			</table>

	

		</body>

	</html>

	

	<?php

	$body = ob_get_contents();

	ob_end_clean();

	

	

	$file = fopen($store_code."_uncategorised.csv","w");

	

	$result = array("Entity ID", "Type", "Sku", "Value", "Is in Stock?");

	 fputcsv($file, $result);

	foreach ($results as $row) {

	    $result = array();

	    array_walk_recursive($row, function($item) use (&$result) {

	        $result[] = $item;

	    });

	    fputcsv($file, $result);

	}

	fclose($file);

	

	

	try{
    
    list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('productuncategorised',true);

		$mail = new PHPMailer(); // defaults to using php "mail()"


		//$mail->isSMTP();                                      // Set mailer to use SMTP

		//$mail->Host = 'dimasoft-development.co.uk';  // Specify main and backup server

		//$mail->SMTPAuth = true;                               // Enable SMTP authentication

		//$mail->Username = 'smtp@charlie.dimasoft-development.co.uk';                            // SMTP username

		//$mail->Password = 'ISpNlbTctT';     

		$mail->CharSet = 'utf-8';

		$mail->SetFrom('noreply@startfitness.co.uk', $store_code.'_cron');

		// $mail->AddReplyTo("name@yourdomain.com","First Last");

        foreach($toEmailAddresses as $toEmailAddress){
            $mail->AddAddress($toEmailAddress,$toEmailAddress);
        }

        foreach($ccEmailAddresses as $ccEmailAddress){
            $mail->AddCC($ccEmailAddress,$ccEmailAddress);
        }

		$mail->Subject    = "Uncategorised Products";
		$mail->isHTML(true);

		$mail->MsgHTML($body);

		if(file_exists($store_code."_uncategorised.csv")) {

			$mail->AddAttachment($store_code."_uncategorised.csv");

		}
    
		if(!$mail->Send()) {

		  throw new Exception("Mailer Error: " . $mail->ErrorInfo);

		} else {

		  echo "Message sent!";

		}

	} catch(exception $e) {

		echo $e->getMessage();

	}

}

