<?php

ini_set('display_startup_errors',1);

ini_set('display_errors',1);

error_reporting(-1);

ini_set('memory_limit', '8096M');

require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento

umask(0);

Mage::app()->setCurrentStore(0);

$observer = Mage::getSingleton('mblconnector/cron');
$observer->_isCLI = true;
$observer->sync("products");

