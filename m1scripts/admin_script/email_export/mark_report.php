<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0


use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();

$today = date("Y-m-d  00:00:00"); // Todays Date mysql format
$last_week = date("Y-m-d 00:00:00", strtotime("-1 weeks")); // Last Week Date mysql format
$count = 0;

// Get Data
$sql_getnewsletter = "SELECT ns.`subscriber_id` AS 'id' ,ns.`subscriber_email` AS 'email',csg.`name` AS 'website',caev.`value` AS 'firstname',caev1.`value` AS 'lastname' 
			 FROM `newsletter_subscriber` ns
			 LEFT JOIN `store` cs ON ns.`store_id`=cs.`store_id`
			 LEFT JOIN `store_group` csg ON cs.`group_id`=csg.`group_id`
			 LEFT JOIN `customer_entity_varchar` caev ON ns.`customer_id`=caev.`entity_id` AND caev.`attribute_id` = '5'
			 LEFT JOIN `customer_entity_varchar` caev1 ON ns.`customer_id`=caev1.`entity_id` AND caev1.`attribute_id` = '7' WHERE ns.subscriber_status = 1
			 AND change_status_at >= '".$last_week."' AND (caev.`value` IS NULL OR caev.`value` NOT LIKE '%http%')";
			 
$sql_getnewsletter2 = "SELECT ns.`id` AS 'id' ,ns.`email` AS 'email',csg.`name` AS 'website','' AS 'firstname','' AS 'lastname' 
			 FROM `dimasoft_coupon_sent` ns
			 LEFT JOIN `store_group` csg ON ns.`website_id`=csg.`website_id`
			 WHERE timestamp >= '".$last_week."' and ns.status = 1";
	

$sql_getcustomer = "SELECT DISTINCT(o.customer_email),o.customer_firstname, o.customer_lastname, csg.name, a.country_id 
		      FROM `sales_order` o 
		      LEFT JOIN `store` cs ON o.`store_id`=cs.`store_id`
		      LEFT JOIN `store_group` csg ON cs.`group_id`=csg.`group_id`
		      RIGHT JOIN `sales_order_address` a on o.`entity_id`=a.`parent_id` 
		      JOIN `customer_entity_int` gdrp1 ON o.`customer_id`=gdrp1.`entity_id` AND gdrp1.`attribute_id` = '410'
		      JOIN `customer_entity_int` gdrp2 ON o.`customer_id`=gdrp2.`entity_id` AND gdrp2.`attribute_id` = '411'
		      WHERE o.`customer_email` NOT LIKE '%marketplace%' AND o.`customer_email` NOT LIKE  '%@startcycles.co.uk' 
		      AND o.`customer_email` NOT LIKE '%@startfitness.co.uk' AND o.`customer_email` NOT LIKE '%@startfootball.co.uk' AND o.`customer_email` NOT LIKE '%@amazon' 
		      AND gdrp1.value = 1 AND gdrp2.value = 2
		      AND a.address_type= 'billing' AND o.`created_at` >= '".$last_week."' GROUP BY(customer_email)";

		      
$sql_getcustomeroptout = "SELECT '' as id, (c.email) as email,caev.`value` AS 'firstname',caev1.`value` AS 'lastname', csg.name as website
		      FROM `customer_entity` c 
		      LEFT JOIN `store` cs ON c.`store_id`=cs.`store_id`
		      LEFT JOIN `store_group` csg ON cs.`group_id`=csg.`group_id`
		      
		      JOIN `customer_entity_varchar` caev ON c.`entity_id`=caev.`entity_id` AND caev.`attribute_id` = '5'
			  JOIN `customer_entity_varchar` caev1 ON c.`entity_id`=caev1.`entity_id` AND caev1.`attribute_id` = '7'
		      JOIN `customer_entity_int` gdrp1 ON c.`entity_id`=gdrp1.`entity_id` AND gdrp1.`attribute_id` = '410'
		      JOIN `customer_entity_int` gdrp2 ON c.`entity_id`=gdrp2.`entity_id` AND gdrp2.`attribute_id` = '411'
		      JOIN `customer_entity_datetime` gdrp3 ON c.`entity_id`=gdrp3.`entity_id` AND gdrp3.`attribute_id` = '416'
		      WHERE c.`email` NOT LIKE '%marketplace%' AND c.`email` NOT LIKE  '%@startcycles.co.uk' 
		      AND c.`email` NOT LIKE '%@startfitness.co.uk' AND c.`email` NOT LIKE '%@startfootball.co.uk' AND c.`email` NOT LIKE '%@amazon' 
		      AND gdrp1.value = 0 AND gdrp2.value = 3 AND gdrp3.value >= '".$last_week."'
		      ";
$sql_getcustomeroptin = "SELECT '' as id,  (c.email) as email,caev.`value` AS 'firstname',caev1.`value` AS 'lastname', csg.name as website
		      FROM `customer_entity` c 
		      LEFT JOIN `store` cs ON c.`store_id`=cs.`store_id`
		      LEFT JOIN `store_group` csg ON cs.`group_id`=csg.`group_id`
		      JOIN `customer_entity_varchar` caev ON c.`entity_id`=caev.`entity_id` AND caev.`attribute_id` = '5'
			  JOIN `customer_entity_varchar` caev1 ON c.`entity_id`=caev1.`entity_id` AND caev1.`attribute_id` = '7'
		      JOIN `customer_entity_int` gdrp1 ON c.`entity_id`=gdrp1.`entity_id` AND gdrp1.`attribute_id` = '410'
		      JOIN `customer_entity_int` gdrp2 ON c.`entity_id`=gdrp2.`entity_id` AND gdrp2.`attribute_id` = '411'
		      WHERE c.`email` NOT LIKE '%marketplace%' AND c.`email` NOT LIKE  '%@startcycles.co.uk' 
		      AND c.`email` NOT LIKE '%@startfitness.co.uk' AND c.`email` NOT LIKE '%@startfootball.co.uk' AND c.`email` NOT LIKE '%@amazon' 
		      AND gdrp1.value = 1 AND gdrp2.value = 2
		      ";
		      
// Fetch all reqults
$results_getnewsletter = $read->fetchAll($sql_getnewsletter);
$results_getnewsletter2 = []; //$read->fetchAll($sql_getnewsletter2);
$results_getcustomer = $read->fetchAll($sql_getcustomer);
$results_getcustomeroptout = $read->fetchAll($sql_getcustomeroptout);
$results_getcustomeroptin = $read->fetchAll($sql_getcustomeroptin);

foreach ($results_getnewsletter as $index => $newsletter){
		unset($results_getnewsletter[$index]);
		$results_getnewsletter[$newsletter['email']] = $newsletter;
}
foreach ($results_getnewsletter2 as $index => $newsletter){
		unset($results_getnewsletter2[$index]);
		$results_getnewsletter2[$newsletter['email']] = $newsletter;
}
foreach ($results_getcustomeroptin as $index => $newsletter){
	if(!isset($results_getnewsletter[$newsletter['email']])){
		$results_getnewsletter[$newsletter['email']] = $newsletter;
	}
}
foreach ($results_getcustomeroptout as $index => $newsletter){
	if(isset($results_getnewsletter[$newsletter['email']])){
		unset($results_getnewsletter[$newsletter['email']]);
	}
}

// Output newsletter People To CSV
if(is_file(dirname(__FILE__)."/newsletter.csv")) unlink(dirname(__FILE__)."/newsletter.csv");
$fp = fopen(dirname(__FILE__).'/newsletter.csv', 'w');
$csvHeader = array("id", "email","website","firstname","lastname");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getnewsletter as $newsletter){
    fputcsv($fp, array($newsletter['id'], $newsletter['email'], $newsletter['website'], $newsletter['firstname'], $newsletter['lastname']), ",");
}
fclose($fp);

// Output newsletter2 People To CSV
if(is_file(dirname(__FILE__)."/newsletter-coupon.csv")) unlink(dirname(__FILE__)."/newsletter-coupon.csv");
$fp = fopen(dirname(__FILE__).'/newsletter-coupon.csv', 'w');
$csvHeader = array("id", "email","website","firstname","lastname");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getnewsletter2 as $newsletter){
    fputcsv($fp, array($newsletter['id'], $newsletter['email'], $newsletter['website'], $newsletter['firstname'], $newsletter['lastname']), ",");
}
fclose($fp);

// Output Customers To CSV
if(is_file(dirname(__FILE__)."/customers.csv")) unlink(dirname(__FILE__)."/customers.csv");
$fp = fopen(dirname(__FILE__).'/customers.csv', 'w');
$csvHeader = array("email", "firstname","lastname","website","country");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getcustomer as $customer){
    fputcsv($fp, array($customer['customer_email'], $customer['customer_firstname'], $customer['customer_lastname'], $customer['name'], $customer['country_id']), ",");
}
fclose($fp);

// Output recent optout To CSV
if(is_file(dirname(__FILE__)."/optout.csv")) unlink(dirname(__FILE__)."/optout.csv");
$fp = fopen(dirname(__FILE__).'/optout.csv', 'w');
$csvHeader = array("", "email", "firstname","lastname","website");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getcustomeroptout as $customer){
    fputcsv($fp, array($newsletter['id'], $newsletter['email'],  $newsletter['firstname'], $newsletter['lastname'],$newsletter['website']),  ",");
}
fclose($fp);


//Receiver Details
//$sendToEmailAddress = array('geoffrey@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','mark@startfitness.co.uk', 'geoffrey@rawtechnologies.co.uk');

$sendToName = 'Start Fitness';
$message = "Bi-Weekly Email Lists";
$html = "Hi Mark, <br><br> This is your latest emails from Magento.<br><br>These are (GRDP compliant) newsletter subscribers, customers who have placed orders on any of the websites in the past 2 weeks, and customers who have opted out of receiving emails.<br><br>See you again in 1 week time.";
$mail = new \Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom("info@startfitness.co.uk", 'Magento Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Weekly Email Export '.date("D d-M-Y"));

	$dir =  __DIR__ . '/../../../';
	$path = dirname(__FILE__)."/newsletter.csv";
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'newsletter.csv';
	
	$path = dirname(__FILE__)."/newsletter-coupon.csv";
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'newsletter-coupon.csv';

	$path = dirname(__FILE__)."/customers.csv";
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'customers.csv';
	
	$path = dirname(__FILE__)."/optout.csv";
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'optout.csv';

		try {
    			$mail->send();
    			//Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
		}
		catch (Exception $e) {
    			//Mage::getSingleton('core/session')->addError('Unable to send.');
		}

echo "Success";
?>