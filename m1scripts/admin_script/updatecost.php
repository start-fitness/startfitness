<?php
ini_set('memory_limit', '2G');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$log = fopen("costpricelog.txt", "w+");
$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$i = 0;
if (($handle = fopen("costprice.csv", "r")) !== FALSE) {
    while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
    	
        $sku = $data[0];
		$price = ($data[2] != "")? $data[2] : $data[1];
		//var_dump($sku);
		if($price == "") continue;
		$price = str_replace(",", "", $price);
		$price = str_replace(" ", "", $price);
		//var_dump($sku, $price); 
		$_catalog = Mage::getModel('catalog/product');
		$_productId = $_catalog->getIdBySku($sku);
		if(!is_numeric($_productId)) continue;
		$product = Mage::getModel('catalog/product')->load($_productId);
		//$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);
		if($product == null) continue;
		$productids = array();
		if($product->getTypeId() == "configurable")
		{
			$conf = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
		    $simple_collection = $conf->getUsedProductCollection()->addAttributeToSelect('*')->addFilterByRequiredOptions();
		    foreach($simple_collection as $simple_product){
		        $productids[] = $simple_product->getEntityId();
		    }
			$productids[] = $product->getEntityId();
		}
		else{
			$productids[] = $product->getEntityId();
		}
		//var_dump($productids);
		$query = "";
		foreach($productids as $pid){
			$query .= "
			INSERT INTO catalog_product_entity_decimal (entity_type_id, attribute_id, store_id, entity_id, `value`)
			VALUES(4, 293, 0, {$pid}, {$price})
			ON DUPLICATE KEY UPDATE `value`={$price};
			"; 
		}
		try{
			$writeConnection->query($query);
		}
		catch(exception $e){
			fwrite($log, $sku."\t ERROR ! ".$e->getMessage()."\t (row $i - {$data[2]})\t ".$query ."\n");
		}
		$productidsstr = implode(",", $productids);
		$query = "UPDATE catalog_product_entity_decimal SET `value` = {$price} WHERE `entity_type_id` = 4 AND `attribute_id` = 293 AND `entity_id` IN ({$productidsstr})";
		var_dump($sku."\t (row $i - {$data[2]})\t ".$query);
		fwrite($log, $sku."\t (row $i - {$data[2]})\t ".$query ."\n");
		try{
			$writeConnection->query($query);
		}
		catch(exception $e){
			fwrite($log, $sku."\t ERROR ! ".$e->getMessage()."\t (row $i - {$data[2]})\t ".$query ."\n");
		}
		$i++;
		//if($i > 100) exit;
		
   	}
	fclose($log);
    fclose($handle);
}