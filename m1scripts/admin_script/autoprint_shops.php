<?php
//SCRIPT NEEDS TO BE CRON'D

//echo "Welcome To Auto Print<br>";

// Set this so nothing is sent to print or invoiced
//$testing = true;

// TO Do
// Prioritise express orders
// After weekend, printing needs to be faster - work out around 400 sheets per printer before 7am

// To Set up each printer - search for $printerid
//session_save_path ("/home/sfadmin/public_html/script/J5HUH/print/tmp");
// First authenticate Google Cloud Print Session
//session_start();

	require_once(dirname(__FILE__).'/../app/Mage.php');
	
	umask(0);
	Mage::app();

	// Get the SQL Read/Write Setup
	$resource = Mage::getSingleton('core/resource');
	$read  = $resource->getConnection('core_read');

	// Login as Admin user
	Mage::getSingleton('admin/session')->login("autoprint","F^r!34zy%@*sWn.e");

	// Set Timeouts and reporting
	//Mage::setIsDeveloperMode(true); // Turn This off for it to work
	ini_set('memory_limit','768M');
	ini_set('display_errors',1);
	ini_set('max_execution_time', 0);
	error_reporting(E_ALL);

	// Function to send email upon error
	// called by using sendErrorEmail();
	function sendErrorEmail($message){
		//$sendToEmailAddress = array('support@granulr.uk','gary@startfitness.co.uk');
		$sendToEmailAddress = array('richard@dimasoft.co.uk','gary@startfitness.co.uk','michaeltaggart@startfitness.co.uk');//testing
		$sendToName = 'Start Fitness';

		$mail = new Zend_Mail();
		$mail->setBodyText($message);
		$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Auto Print Error');
		$mail->addTo($sendToEmailAddress, $sendToName);
		$mail->setSubject('Auto Print Error '.date("D d-M-Y"));
		$mail->send();
	}

	// Need to check if we can print yet - here we use date and time
	// We will only print between Monday-Sat 
	$today = date('N'); // Number Format Days
	$time = date('Gi'); // 24 Hour

	// If its Mon-Saturday
        if($today == 1 || $today == 2 || $today == 3 || $today == 4 || $today == 5 || $today == 6){
                // Remember the server time is a hour out
                if($time >= 0400 && $time <= 1700){
                        $canprint = true;
                }else{
                        $canprint = false; // change this to false
                }
        }else{
                // Remember the server time is a hour out
                if($time >= 1500 && $time <= 1600){
                        $canprint = true;
                }else{
                        $canprint = false; // change this to false
                }

        }
	//test override
	//$canprint = true;
	if($canprint == true){

		// Need to find all the orders this script has marked as printed
		// and make sure that Google actually has this printed
		// To Do here - limit the output from the sql to only say the last hours orders to check
		// better yet, what ever was printed in the last batch to check against - save time

		// First get the picknote directory
		/*$filename = "orders/";

		$sql_checkprinted = "SELECT distinct(sfo.`entity_id`),sfo.`increment_id` FROM `sales_flat_order` sfo
					RIGHT JOIN `sales_flat_order_status_history` sfosh ON sfo.`entity_id`=sfosh.`parent_id`
					WHERE sfosh.`comment` like '%autoprint'
					AND sfo.`status` = 'pick_note_printed'";
		$checkorders = $read->fetchAll($sql_checkprinted);

		foreach($checkorders as $checked){

			$filename = "PrintFolder/Express" . $checked['increment_id'].".pdf"; // this will be increment_id.pdf

			if (file_exists($filename)) {
    				echo "The file $filename exists<br>"; // do nothing - success print
			} else {
   			 	echo "The file $filename does not exist<br>"; // needs to be reprinted
			}
		}*/

		// First we priorities the Express shipments
		// These will always be printed out no matter what

		
		
		///////////////////////////////////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		// Get all the processing orders
		// Have to do it by sql as mage function is protected
		// We will use this process to select oldest first
		// Cameron asked to skip Cycles store view
		$sql_getorders = "SELECT sfo.`entity_id`,sfo.`shipping_description`, status
					FROM `sales_flat_order` sfo 
                    JOIN sales_flat_order_payment pay on pay.parent_id = sfo.entity_id
					LEFT JOIN `metapack_consignment` mc ON sfo.`entity_id`=mc.`order_entity_id`
					WHERE sfo.`status` = 'processing' 
					
					AND (sfo.customer_email = 'wholesale@startfitness.co.uk' OR sfo.customer_email = 'startfitnessshop@startfitness.co.uk')
					AND sfo.`base_total_invoiced` = 0.00 
					/*AND sfo.`store_id` IN (33)
                    AND pay.po_number = 'reorderauto'*/
					AND sfo.`created_at` < date_sub(UTC_TIMESTAMP(), interval 20 minute)
					ORDER BY sfo.`created_at` 
					ASC LIMIT 50";
		$orders = $read->fetchAll($sql_getorders);

		// cycles exclude code AND sfo.`store_id` NOT IN ('23')

		// Loop thorough each to make sure an invoice has been created
		$orderIDs = array();
		foreach ($orders as $order){
			echo '<p>aa'.$order['entity_id'];
			//$loadorder = Mage::getModel('sales/order')->load(474143);
			$loadorder = Mage::getModel('sales/order')->load($order['entity_id']);
			// Try and get the invoice collection
			if ($loadorder->hasInvoices()) {
				echo "here";
				
                $product_type = "Footwear"; // This is for shop orders. Type was kept due to printer already set up to work with this folder name
                    

				
				
				$prefix = "";
				$orderedItems = $loadorder->getAllItems();
				$orderedProductIds = array();
			    foreach ($orderedItems as $item) {
					if ($item->getProductType() == "simple")
                                        {
					    $orderedProductIds[] = $item->getData('product_id');
					}
				}
				
				if(count($orderedProductIds) > 0){
					$shelves = array();
									
					
					foreach($orderedProductIds as $product){
						
						$bin = Mage::getResourceModel('catalog/product')->getAttributeRawValue($product, 'bin_location', 0);
						
						
						if(!empty($bin))$shelves[] = $bin;
					}
					if(count($shelves) > 0){
						asort($shelves);
						//var_dump($shelves);
						$prefix1 = str_replace('-', '', $shelves[0]);
						$prefix = str_replace(' ', '', $prefix1);
						if(count($shelves) > 1)
						$prefix = "Z".$prefix;
					}
					
				}

				// A little bodge to get the invoices to print - as moogento expects more than one id in an array
				// We pass the single id in an array for it to print out without an error
				$orderIDs = explode(',',$loadorder->getID());
				echo "here2";
				if ($testing == false){
					try {
						echo "here NOT express";
						// Get MOOGENTO INVOUICE CODE
						$pdf = Mage::getModel('sales/order_pdf_invoices')->getPickSeparated2($orderIDs);
						// MOOGENTO CODE END

						$pdf_filename = $prefix."-".$loadorder->getIncrementId().".pdf";
						$pdfFile = $pdf->render();
						
						$pdf->save("PrintFolder/ss-".$product_type."/".$pdf_filename); // dont really want to save?
						
						//UPLOAD TO CORRECT QUEUE & LOG

					} catch (Zend_Pdf_Exception $e) {
						sendErrorEmail('PDF error: ' . $e->getMessage());
						die ('PDF error: ' . $e->getMessage()); // Will need to send email if error to fix this message
					} catch (Exception $e) {
						sendErrorEmail('Application error: Order (' . $order['entity_id'] . ') - ' . $e->getMessage());
						die ('Application error: Order (' . $order['entity_id'] . ') - ' . $e->getMessage()); // Will need to send an email if error
					}
				}
			}
		}
	}else{
		echo "Currently Out Of Hours - Nothing to print till later - just refreshing (" . date('D jS F - G:s').")";
	}

// Finally, destroy the session.
//session_destroy();

echo "<br>This script has completed successfully";

?>
