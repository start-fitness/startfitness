<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once(dirname(__FILE__).'/../../app/Mage.php'); 
Mage::app()->setCurrentStore(0);
$writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
 
/* Split these into multi array (98)Mens / (13)Ladies / (113)Junior */
$cat2subcats = array(
	/* DEV */
	// "2988" => array(98 => "2998", 13 => "2995"), // CYCLES
	//"2638" => array(98 => "2999", 13 => "2996"), // FOOTBALL
	//"2639" => array(98 => "3000", 13 => "2997") // OUTDOORS
	
	/* LIVE */
	//"2995" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//CYCLES SUBS
//"2906" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//"44" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//"2863" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//"2859" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//"1712" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//"73" => array( 98 => "2999", 13 => "2996", 113 => "3002", 97 => "2999,2996" ), // CYCLES
//END SUBS
//	"2638" => array( 98 => "3000", 13 => "2997", 113 => "3003", 97 => "3000,2997" ), // FOOTBALL
"2639" => array( 98 => "3001", 13 => "2998", "BOTH|98,13" => "3001,2998" ), // OUTDOORS

// SPIKES Cross country
"2807" => array( 98 => "2552", 13 => "2511", "BOTH|98,13" => "2552,2511"),

// SPIKES Track
"2926" => array( 98 => "2551", 13 => "2510", "BOTH|98,13" => "2551,2510" ),
"2808" => array( 98 => "2551", 13 => "2510", "BOTH|98,13" => "2551,2510" ),
"2809" => array( 98 => "2551", 13 => "2510", "BOTH|98,13" => "2551,2510" ),
"2810" => array( 98 => "2551", 13 => "2510", "BOTH|98,13" => "2551,2510" ),

// SPIKES Field
"2812" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2814" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2813" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2815" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2816" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2817" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
"2818" => array( 98 => "2553", 13 => "2512", "BOTH|98,13" => "2553,2512" ),
);

/* Start n.b. Gender Attribute is 141 */
$genders = array();
$sql = "SELECT * FROM `eav_attribute_option_value` 
WHERE `option_id` IN 
	(SELECT option_id FROM `eav_attribute_option` WHERE `attribute_id` = 141) 
AND `store_id` = 0 
ORDER BY `option_id` DESC";
$results = $readConnection->fetchAll($sql);

/* Generate an array containing gender options */
foreach($results as $row) {
	$genders[$row['option_id']] = $row['value'];
}
/* End n.b. */

foreach($cat2subcats as $from => $to){

	/* Remove all products from this category */
	$sqlpurge = "
	DELETE FROM catalog_category_product WHERE category_id IN (".implode(',', $to).");
	DELETE FROM catalog_category_product_index WHERE category_id IN (".implode(',', $to).") and store_id = 6;
	DELETE FROM catalog_category_product_index WHERE category_id IN (".implode(',', $to).") and store_id = 19;
	";
	
	/* Reinsert products from the desired category ID */
	$sqlinsert = "";
	$writeConnection->query($sqlpurge);
	//echo $sqlpurge."\n";
}

foreach($cat2subcats as $from => $to){
$sqlinsert = "";

	foreach($to as  $gender => $toidarr){
		
		$toidsplit = explode(",", $toidarr);
		$filtervar = "";
		foreach($toidsplit as $toid){
			$both = explode("|", $gender);
			if(count($both) > 1){
				$genders = explode(",", $both[1]);
				foreach($genders as $inngender){
					$filtervar .= " FIND_IN_SET({$inngender}, cpei.value) AND";
				}
				$filtervar = rtrim($filtervar, "AND");
				//var_dump($filtervar);exit;
			}
			else{
				$filtervar = "FIND_IN_SET({$gender}, cpei.value)";
			}
			$sqlinsert.= "
			  INSERT IGNORE INTO catalog_category_product (category_id, product_id, position)
			  SELECT {$toid}, innerj.product_id, innerj.position FROM catalog_category_product_index innerj 
			  INNER JOIN catalog_product_entity_varchar as cpei ON innerj.product_id = cpei.entity_id
			  WHERE innerj.category_id = {$from} AND innerj.store_id = 6 AND $filtervar  AND cpei.attribute_id = 141;
			  ";
			  $sqlinsert.= "
			  INSERT IGNORE INTO catalog_category_product_index (category_id, product_id, position, is_parent, store_id, visibility)
			  SELECT {$toid}, innerj.product_id, innerj.position, innerj.is_parent, innerj.store_id, innerj.visibility FROM catalog_category_product_index innerj
			  INNER JOIN catalog_product_entity_varchar as cpei ON innerj.product_id = cpei.entity_id
			  WHERE innerj.category_id = {$from} AND innerj.store_id = 6 AND $filtervar AND cpei.attribute_id = 141;
			  ";
			  $sqlinsert.= "
			  UPDATE catalog_category_flat_store_6 AS toupdate 
			  INNER JOIN catalog_category_flat_store_6 AS jn ON jn.entity_id = {$from}
			  SET toupdate.children_count = jn.children_count, toupdate.is_anchor = jn.is_anchor
			  WHERE toupdate.entity_id = {$toid};
			  UPDATE catalog_category_flat_store_19 AS toupdate 
			  INNER JOIN catalog_category_flat_store_6 AS jn ON jn.entity_id = {$from}
			  SET toupdate.children_count = jn.children_count, toupdate.is_anchor = jn.is_anchor
			  WHERE toupdate.entity_id = {$toid};
			  ";
			  
			  
		  }

   }
	//var_dump($sqlinsert);
//echo $sqlinsert."\n";
   $writeConnection->query($sqlinsert);
}

