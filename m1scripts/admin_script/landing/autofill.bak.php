<?php
require_once(dirname(__FILE__).'/../../app/Mage.php'); 
Mage::app()->setCurrentStore(0);
$writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');

$cat2subcats = array(
// "2988" => "2995", // DEV
"2995" => "2996,2999,3002", // CYCLES
"2638" => "2997,3000,3003", // FOOTBALL
"2639" => "2998,3001", // OUTDOORS
);

foreach($cat2subcats as $from => $to){
	$sqlpurge = "
	DELETE FROM catalog_category_product WHERE category_id IN ($to);
	DELETE FROM catalog_category_product_index WHERE category_id IN ($to) and store_id = 6;
	";
	// DO PURGE
	$sqlinsert  ="";
	foreach((explode(",", $to)) as $toid){
		$sqlinsert.= "
		INSERT INTO catalog_category_product (category_id, product_id, position)
		SELECT {$toid}, innerj.product_id, innerj.position FROM catalog_category_product_index innerj
		WHERE innerj.category_id = {$from} AND innerj.store_id = 6;
		";
		$sqlinsert.= "
		INSERT INTO catalog_category_product_index (category_id, product_id, position, is_parent, store_id, visibility)
		SELECT {$toid}, innerj.product_id, innerj.position, innerj.is_parent, innerj.store_id, innerj.visibility FROM catalog_category_product_index innerj
		WHERE innerj.category_id = {$from} AND innerj.store_id = 6;
		";
		$sqlinsert.= "
		UPDATE catalog_category_flat_store_6 AS toupdate 
		INNER JOIN catalog_category_flat_store_6 AS jn ON jn.entity_id = {$from}
		SET toupdate.children_count = jn.children_count, toupdate.is_anchor = jn.is_anchor
		WHERE toupdate.entity_id = {$toid};
		";
		
	}
	//echo "<pre>{$sqlpurge}{$sqlinsert}</pre>";
	$writeConnection->query($sqlpurge.$sqlinsert);
}
