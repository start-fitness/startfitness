<?php
ini_set('memory_limit', '2048M');
$start = microtime(true);
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
if(isset($argv) && isset($argv[1]) && is_numeric($argv[1])) $storeid = $argv[1];
else $storeid = 6;
Mage::app()->setCurrentStore($storeid);
//var_dump($storeid);exit;
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');
$website_id = Mage::app()->getStore()->getWebsiteId();

//exit;

$catalog_rule = Mage::getModel('salesrule/rule')->getCollection()->addFieldToSelect("*")->addFieldToFilter("simple_action", "group_n")->addFieldToFilter("website_ids", "$website_id")->setOrder('sort_order'); // Rule ID

$operators = array(
"=="	=>	"eq",
"!="	=>	"neq",
"{}"	=>	"like",
"!{}"	=>	"nlike",
"()"	=>	"in",
"!()"	=>	"nin",
">"		=>	"gt",
"<"		=>	"lt",
">="	=>	"gteq",
"<="	=>	"lteq",
);
$query = 'SELECT * FROM stickers_cache_table WHERE store_id = '.$storeid;
$cachetabletmp = $readConnection->fetchAll($query);
$cachetable = array();
foreach($cachetabletmp as $tmpline){
	$cachetable[$tmpline['pid']] = $tmpline;
}
unset($cachetabletmp);

$attropts = array();
$attrids = array();
$name='current_deal';
$attropts[$name] = array();
$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
$attributeId = $attributeInfo->getAttributeId();
$attrids[$name] = $attributeId;
$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
$attributeOptionstmp = $attribute ->getSource()->getAllOptions(false); 
foreach($attributeOptionstmp as $linetmp){
	$attropts[$name][$linetmp['label']] = $linetmp['value'];
}
unset($attributeOptionstmp);
$name='current_free';
$attropts[$name] = array();
$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
$attributeId = $attributeInfo->getAttributeId();
$attrids[$name] = $attributeId;
$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
$attributeOptionstmp = $attribute ->getSource()->getAllOptions(false); 
foreach($attributeOptionstmp as $linetmp){
	$attropts[$name][$linetmp['label']] = $linetmp['value'];
}
unset($attributeOptionstmp);

if (isset($argv[2]) && $argv[2] == "debug") {var_dump($attrids);exit;}

$products2update = array();

foreach($catalog_rule as $rule){
	//var_dump($rule->getData("sort_order"));exit;
	if($rule->getData("simple_action") == "group_n"){
		$sticker = $rule->getData("discount_step")."_for_".round($rule->getData("discount_amount"))."_pound";
		$sticker_type = "current_deal";
		//echo $attrids[$sticker_type]; exit;
	}
	if(isset($attrids[$sticker_type]) && isset($attropts[$sticker_type][$sticker])){
		$actions = unserialize($rule->getActionsSerialized());
		$conditions = ($actions['conditions']);
		$aggregator = $actions["aggregator"];
		$filters = array();
		foreach($conditions as $condition){
			$attribute = $condition["attribute"];
			if (0 === strpos($attribute, 'quote_')) {
			   // Do nothing, this is not a product's attribute
			   
			}
			else{
				$operator = $operators[$condition["operator"]];
				$value = $condition["value"];
				$filters[] = array("attribute"=>$attribute, "operator"=>$operator, "value" => $value);
			}
		}
		//var_dump($collection->count());
		$catfilter = "";
		//var_dump($filters);
		foreach($filters as $keyf =>$filter){
			
			if($filter["attribute"]== "category_ids"){
				if($catfilter != "") $catfilter = $catfilter.",".$filter["value"]; else $catfilter = $filter["value"];
				unset($filters[$keyf]);
			}
		}
		
		$catids = explode(",",$catfilter);
		//var_dump($catfilter);
		echo $rule->getName()."\n";
		
		foreach($catids as $catid){
			
			if(!is_numeric($catid)) continue;
			
			$category = Mage::getModel('catalog/category')->load($catid);
			$docat = true;
			if($category->getData("entity_id") == NULL || !is_numeric($category->getData("entity_id"))) $docat = false;
			
			if($docat){
				$collection = Mage::getResourceModel('catalog/product_collection');
				$collection->clear();
				$collection->addCategoryFilter($category);
				foreach($filters as $filter){
					//echo "where {$filter["attribute"]} is {$filter["operator"]} {$filter["value"]}\n";
					$collection->addAttributeToFilter($filter["attribute"], array($filter["operator"] => $filter["value"]));
				}
				echo "category $catid (".$category->getName().") - products found: ".$collection->count()."\n";
				foreach($collection as $_product){
					
					$prodtmp = array();
					$prodtmp['pid'] = $_product->getId();
					$prodtmp['ruleid'] = $rule->getId();
					$prodtmp['priority'] = $rule->getData("sort_order");
					$prodtmp['stickertype'] = $sticker_type;
					$prodtmp['sticker'] = $sticker;
					if(isset($products2update[$prodtmp['pid']]) && $products2update[$prodtmp['pid']]['priority'] >=$prodtmp['priority'] ){
						// DO NOTHING, THE PREVIOUS RULE TAKES PRECEDENT
					}
					else{
						if(isset($products2update[$prodtmp['pid']]) && $products2update[$prodtmp['pid']]['priority'] > $prodtmp['priority'])
							$products2update[$prodtmp['pid']] = $products2update[$prodtmp['pid']];
						else
							$products2update[$prodtmp['pid']] = $prodtmp;
					}
				}
			}
		}
	}
}
//exit;
//$products2update = array();
$todelete = array();
foreach($cachetable as $cacheline){
	$pid = $cacheline['pid'];
	if(!isset($products2update[$pid])){
		$todelete[] = $cacheline;
	}
}
//var_dump(count($todelete));exit;

if(count($products2update) > 0):
// ADDING TO EAV TABLE
$sql = "INSERT INTO catalog_product_entity_int (entity_type_id,attribute_id,store_id,entity_id,`value`) VALUES {{VALUES}}
ON DUPLICATE KEY UPDATE `value`=VALUES(`value`),attribute_id=VALUES(attribute_id) ;";
$values = array();
foreach($products2update as $stickerline){
	$values[] = "4,{$attrids[$stickerline['stickertype']]},$storeid,{$stickerline['pid']},{$attropts[$stickerline['stickertype']][$stickerline['sticker']]}";
}
$sql = str_replace("{{VALUES}}", "(".implode("),(", $values).")", $sql);
$writeConnection->query($sql);

//echo ($sql);

// ADDING TO FLAT TABLE

$values = array();
foreach($products2update as $stickerline){
	
	$sql = "UPDATE catalog_product_flat_$storeid SET {$stickerline['stickertype']} = {$attropts[$stickerline['stickertype']][$stickerline['sticker']]},
	{$stickerline['stickertype']}_value = '{$stickerline['sticker']}' WHERE entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
	//echo ($sql);
}
endif;
if(count($todelete) > 0):

// DELETING FROM EAV TABLE
foreach($todelete as $stickerline){
	$sql = "DELETE FROM catalog_product_entity_int WHERE entity_type_id = 4 AND store_id = $storeid AND attribute_id = {$attrids[$stickerline['stickertype']]} AND entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
}

// DELETING FROM CACHE TABLE
foreach($todelete as $stickerline){
	$sql = "UPDATE catalog_product_flat_$storeid SET {$stickerline['stickertype']} = NULL,
	{$stickerline['stickertype']}_value = NULL WHERE entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
	//echo ($sql);
}
endif;
//var_dump($todelete);exit;


// ADDING TO CACHE TABLE
$writeConnection->query("DELETE FROM stickers_cache_table WHERE store_id = $storeid");
//exit;
if(count($products2update) > 0):
$sql = "INSERT INTO stickers_cache_table (pid,ruleid,priority,stickertype,`sticker`,`store_id`) VALUES {{VALUES}}";
$values = array();
foreach($products2update as $stickerline){
	$values[] = "{$stickerline['pid']}, {$stickerline['ruleid']}, {$stickerline['priority']},'{$stickerline['stickertype']}', '{$stickerline['sticker']}', {$storeid}";
}
$sql = str_replace("{{VALUES}}", "(".implode("),(", $values).")", $sql);
$writeConnection->query($sql);
endif;

$time_taken = microtime(true) - $start;
echo "\nScript executed in {$time_taken} seconds\n";
?>