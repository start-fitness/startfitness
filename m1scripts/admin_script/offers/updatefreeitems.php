<?php
ini_set('memory_limit', '2048M');
$start = microtime(true);
require_once '../../app/Mage.php';
umask(0);
if(isset($argv) && isset($argv[1]) && is_numeric($argv[1])) $storeid = $argv[1];
else $storeid = 6;
Mage::app()->setCurrentStore($storeid);

$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

$catalog_rule = Mage::getModel('awafptc/rule')->getCollection(); // Rule ID

$operators = array(
"=="	=>	"eq",
"!="	=>	"neq",
"{}"	=>	"like",
"!{}"	=>	"nlike",
"()"	=>	"in",
"!()"	=>	"nin",
">"		=>	"gt",
"<"		=>	"lt",
">="	=>	"gteq",
"<="	=>	"lteq",
);
$query = 'SELECT * FROM freeitems_cache_table WHERE store_id = '.$storeid;
$cachetabletmp = $readConnection->fetchAll($query);
$cachetable = array();
foreach($cachetabletmp as $tmpline){
	$cachetable[$tmpline['pid']] = $tmpline;
}
unset($cachetabletmp);

$attropts = array();
$attrids = array();
$name='current_free';
$attropts[$name] = array();
$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')->setCodeFilter($name)->getFirstItem();
$attributeId = $attributeInfo->getAttributeId();
$attrids[$name] = $attributeId;
$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
$attributeOptionstmp = $attribute ->getSource()->getAllOptions(false); 
foreach($attributeOptionstmp as $linetmp){
	$attropts[$name][$linetmp['label']] = $linetmp['value'];
}
unset($attributeOptionstmp);

$products2update = array();


foreach($catalog_rule as $rule){
	//var_dump($rule->getData());exit;
	if($rule->getData("status") == 1 && in_array($storeid, explode(",",$rule->getData("store_ids")))){
		if(true /*$rule->getData("simple_action") == "group_n"*/){
			$sticker = $rule->getData("name");
			$sticker_type = "current_free";
			//echo $attrids[$sticker_type]; exit;
		}
		if(isset($attrids[$sticker_type]) && isset($attropts[$sticker_type][$sticker])){
			//echo "{$attropts[$sticker_type][$sticker]}\n";exit;
			$actions = unserialize($rule->getData("conditions_serialized"));
			
			$conditions = ($actions['conditions'][0]['conditions']);
			//print_r($conditions);exit;
			$aggregator = $actions["aggregator"];
			$filters = array();
			foreach($conditions as $condition){
				$attribute = $condition["attribute"];
				if (0 === strpos($attribute, 'quote_')) {
				   // Do nothing, this is not a product's attribute
				   
				}
				else{
					$operator = $operators[$condition["operator"]];
					$value = $condition["value"];
					$filters[] = array("attribute"=>$attribute, "operator"=>$operator, "value" => $value);
				}
			}
			
			//var_dump($collection->count());
			$catfilter = "";
			foreach($filters as $keyf =>$filter){
				
				if($filter["attribute"]== "category_ids"){
					if($catfilter != "") $catfilter.",".$filter["value"]; else $catfilter = $filter["value"];
					unset($filters[$keyf]);
				}
			}
			
			$catids = explode(",",$catfilter);
			
			echo $rule->getName()."\n";
			if($catfilter == ""){
				$collection = Mage::getResourceModel('catalog/product_collection');
				$collection->clear();
				foreach($filters as $filter){
					//echo "where {$filter["attribute"]} is {$filter["operator"]} {$filter["value"]}\n";
					$collection->addAttributeToFilter($filter["attribute"], array($filter["operator"] => $filter["value"]));
				}
				echo " - products found: ".$collection->count()."\n";
				foreach($collection as $_product){
					$prodtmp = array();
					$prodtmp['pid'] = $_product->getId();
					$prodtmp['ruleid'] = $rule->getId();
					$prodtmp['priority'] = $rule->getData("priority");
					$prodtmp['stickertype'] = $sticker_type;
					$prodtmp['sticker'] = $sticker;
					//print_r($prodtmp);exit;
					if(isset($products2update[$prodtmp['pid']]) && $products2update[$prodtmp['pid']]['priority'] >=$prodtmp['priority'] ){
						// DO NOTHING, THE PREVIOUS RULE TAKES PRECEDENT
					}
					else{
						if(isset($cachetable[$prodtmp['pid']]) && $cachetable[$prodtmp['pid']]['priority'] > $prodtmp['priority'])
							$products2update[$prodtmp['pid']] = $cachetable[$prodtmp['pid']];
						else
							$products2update[$prodtmp['pid']] = $prodtmp;
					}
				}
			}
			else{
				foreach($catids as $catid){
					if(!is_numeric($catid)) break;
					$collection = Mage::getResourceModel('catalog/product_collection');
					$category = Mage::getModel('catalog/category')->load($catid);
					$collection->clear();
					$collection->addCategoryFilter($category);
					foreach($filters as $filter){
						//echo "where {$filter["attribute"]} is {$filter["operator"]} {$filter["value"]}\n";
						$collection->addAttributeToFilter($filter["attribute"], array($filter["operator"] => $filter["value"]));
					}
					echo "category $catid (".$category->getName().") - products found: ".$collection->count()."\n";
					
					foreach($collection as $_product){
						$prodtmp = array();
						$prodtmp['pid'] = $_product->getId();
						$prodtmp['ruleid'] = $rule->getId();
						$prodtmp['priority'] = $rule->getData("priority");
						$prodtmp['stickertype'] = $sticker_type;
						$prodtmp['sticker'] = $sticker;
						//print_r($prodtmp);exit;
						if(isset($products2update[$prodtmp['pid']]) && $products2update[$prodtmp['pid']]['priority'] >=$prodtmp['priority'] ){
							// DO NOTHING, THE PREVIOUS RULE TAKES PRECEDENT
						}
						else{
							if(isset($cachetable[$prodtmp['pid']]) && $cachetable[$prodtmp['pid']]['priority'] > $prodtmp['priority'])
								$products2update[$prodtmp['pid']] = $cachetable[$prodtmp['pid']];
							else
								$products2update[$prodtmp['pid']] = $prodtmp;
						}
					}
				}
			}
		}
	}
	else{
		echo "this rule does not apply\n";
	}
}
//$products2update = array();
$todelete = array();
foreach($cachetable as $cacheline){
	$pid = $cacheline['pid'];
	if(!isset($products2update[$pid])){
		$todelete[] = $cacheline;
	}
}
//var_dump(count($todelete));exit;

if(count($products2update) > 0):
// ADDING TO EAV TABLE
$sql = "INSERT INTO catalog_product_entity_int (entity_type_id,attribute_id,store_id,entity_id,`value`) VALUES {{VALUES}}
ON DUPLICATE KEY UPDATE `value`=VALUES(`value`),attribute_id=VALUES(attribute_id) ;";
$values = array();
foreach($products2update as $stickerline){
	$values[] = "4,{$attrids[$stickerline['stickertype']]},$storeid,{$stickerline['pid']},{$attropts[$stickerline['stickertype']][$stickerline['sticker']]}";
}
$sql = str_replace("{{VALUES}}", "(".implode("),(", $values).")", $sql);
$writeConnection->query($sql);

//echo ($sql);

// ADDING TO FLAT TABLE
$values = array();
foreach($products2update as $stickerline){
	$sql = "UPDATE catalog_product_flat_$storeid SET {$stickerline['stickertype']} = {$attropts[$stickerline['stickertype']][$stickerline['sticker']]},
	{$stickerline['stickertype']}_value = '{$stickerline['sticker']}' WHERE entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
	//echo ($sql);
}
endif;
if(count($todelete) > 0):

// DELETING FROM EAV TABLE
foreach($todelete as $stickerline){
	$sql = "DELETE FROM catalog_product_entity_int WHERE entity_type_id = 4 AND store_id = $storeid AND attribute_id = {$attrids[$stickerline['stickertype']]} AND entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
}

// DELETING FROM CACHE TABLE
foreach($todelete as $stickerline){
	$sql = "UPDATE catalog_product_flat_$storeid SET {$stickerline['stickertype']} = NULL,
	{$stickerline['stickertype']}_value = NULL WHERE entity_id = {$stickerline['pid']}";
	$writeConnection->query($sql);
	//echo ($sql);
}
endif;
//var_dump($todelete);exit;


// ADDING TO CACHE TABLE
$writeConnection->query("DELETE FROM freeitems_cache_table WHERE store_id = $storeid");
//exit;
if(count($products2update) > 0):
$sql = "INSERT INTO freeitems_cache_table (pid,ruleid,priority,stickertype,`sticker`,`store_id`) VALUES {{VALUES}}";
$values = array();
foreach($products2update as $stickerline){
	$values[] = "{$stickerline['pid']}, {$stickerline['ruleid']}, {$stickerline['priority']},'{$stickerline['stickertype']}', '{$stickerline['sticker']}', {$storeid}";
}
$sql = str_replace("{{VALUES}}", "(".implode("),(", $values).")", $sql);
$writeConnection->query($sql);
endif;

$time_taken = microtime(true) - $start;
echo "\nScript executed in {$time_taken} seconds\n";
?>