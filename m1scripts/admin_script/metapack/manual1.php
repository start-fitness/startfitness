<?php
//Load Magento API
require_once dirname(dirname(dirname(__FILE__))).'/app/Mage.php';
Mage::app();

Mage::app('admin')->setUseSessionInUrl(false);

umask(0);

//var_dump();exit;
$pid = getmypid();
$pidfile = dirname(__FILE__)."/manual1.pid";
$canrun = true;
if(is_file($pidfile)){
	$oldpid = file_get_contents($pidfile);
	if(file_exists( "/proc/$oldpid" )){
		$canrun = false;
	}
	else{
		$canrun = true;
		unlink($pidfile);
	}
}
else{
	$canrun = true;
}
if(!$canrun) { echo "script currently running! exiting ...\n"; exit;}

file_put_contents($pidfile, $pid);

echo "Starting script pid: $pid...";

 Mage::getConfig()->init()->loadEventObservers('crontab');


//First we load the model
$model = Mage::getModel('cargo_metapack/observers_cron');

//Then execute the task
$model->checkForConsignmentUpdates();


//sleep(10);
unlink($pidfile);

?>
