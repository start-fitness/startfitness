<?php
$stores = array(
1 => array("name"=>"Start Cycles", "emails"=>"test@geoffrey2.dimasoft-development.co.uk"),
5 => array("name"=>"Start Football", "emails"=>"test@geoffrey2.dimasoft-development.co.uk"),
6 => array("name"=>"Start Fitness", "emails"=>"test@geoffrey2.dimasoft-development.co.uk"),
7 => array("name"=>"Start Outdoors", "emails"=>"test@geoffrey2.dimasoft-development.co.uk"),
8 => array("name"=>"More Miles", "emails"=>"test@geoffrey2.dimasoft-development.co.uk")
);
if(count($argv) == 1){
	foreach($stores as $sid => $sinfo){
		exec("/usr/local/bin/php productsinstock2.php $sid");
	}
	
}
elseif(isset($argv[1]) && isset($stores[$argv[1]])){
	require_once 'phpmailer/class.phpmailer.php';
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
	ini_set('memory_limit', '2048M');
	require_once '../app/Mage.php';
	//Mage::init();
	
	
	
	//foreach($stores as $sid => $sinfo){
		$sid = $argv[1];
		$sinfo = $stores[$argv[1]];
		$emails = explode(",", $sinfo['emails']);
		$storename = $sinfo['name'];
		$storeofs = array();
		
		//Mage::unsetapp();
		$app = Mage::app();
		$app->setCurrentStore($sid);
		//$currentstore = $app->reinitStores();
		echo "SETTING STORE $sid - $currentstore\n";
		$collection = Mage::getModel('catalog/product')->getCollection()
			->addAttributeToSelect('*')
			->addAttributeToFilter('stock_status', 0)
			->addAttributeToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED)
			->setPageSize(500);
		for ($i = 1; ($i <= $collection->getLastPageNumber()); $i++) {
			$collection->setCurPage($i)->load();
			foreach ($collection as $product) {
				$qtyStock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
				//var_dump(Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getData());exit;
				$qtyStock =  round($qtyStock);
				if($qtyStock > 0) continue;
				if( $product->getTypeId() != 'simple' ) continue;
				//var_dump($qtyStock);
				$currentline = "<tr><td>SKU: ".$product->getData("sku")."</td><td>NAME: ".$product->getData("name")."</td><td>BIN: ".$product->getData("bin_location")."</td><td>OVER: ".$product->getData("bin_overstock_location")."</td></tr>";
				$storeofs[] = $currentline;
				//echo "$currentline\n";
			}
			//$collection->clear();
		}
		//echo "found ".count($storeofs)."\n";
		//exit;
		$mail = new PHPMailer;
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'localhost';  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
		$mail->Password = 'QvEF4Clp';                           // SMTP password
		//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
		
		$mail->From = 'customerservices@startcycles.co.uk';
		$mail->FromName = 'Stock Manager';
		foreach($emails as $email){
			$mail->addAddress($email);  // Add a recipient
		}
		
		//$mail->addAddress("speedking34@gmail.com");  // Add a recipient
		
		//$mail->addBCC("speedking34@gmail.com");  // Add a recipient
		
		$mail->isHTML(true);                                  // Set email format to HTML
		
		$mail->Subject = ''.$storename.' - Out of stock notice for product options';
		$message = "THE FOLLOWING PRODUCTS ARE OUT OF STOCK: <br /><table>".implode("\n", $storeofs)."</table>";
		$mail->Body    = $message;
		
		if(!$mail->send()) {
		   echo 'Message could not be sent.';
		   echo 'Mailer Error: ' . $mail->ErrorInfo;
		   exit;
		}
		$collection->clear();
		//exit;
	//}
}
