<?php

ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

$sql = "SELECT f1.entity_id, f1.actual_model, f1.name, f1.sku, f1.manufacturer_value FROM `catalogsearch_fulltext` ft RIGHT JOIN catalog_product_flat_6 f1 on f1.entity_id = ft.product_id AND ft.store_id = 6 WHERE ft.product_id IS NULL AND f1.visibility IN (4, 3)";

$sqlm = "SELECT f1.entity_id, f1.actual_model, f1.name, f1.sku, f1.manufacturer_value FROM `catalogsearch_fulltext` ft RIGHT JOIN catalog_product_flat_19 f1 on f1.entity_id = ft.product_id AND ft.store_id = 19 WHERE ft.product_id IS NULL AND f1.visibility IN (4, 3)";

$sqlmm = "SELECT f1.entity_id, f1.actual_model, f1.name, f1.sku, f1.manufacturer_value FROM `catalogsearch_fulltext` ft RIGHT JOIN catalog_product_flat_8 f1 on f1.entity_id = ft.product_id AND ft.store_id = 8 WHERE ft.product_id IS NULL AND f1.visibility IN (4, 3)";

$insert = "";
$results = $readConnection->fetchAll($sql);
$resultsm = $readConnection->fetchAll($sqlm);
$resultsmm = $readConnection->fetchAll($sqlmm);
foreach($results as $row){
	$data = str_replace('"', '\"', "{$row['actual_model']}|{$row['name']}|{$row['sku']}|{$row['manufacturer_value']}");
	$insert .= 'INSERT INTO catalogsearch_fulltext (product_id, store_id, data_index, updated) VALUES('.$row['entity_id'].', 6, "'.$data.'", 0);'."\n";
}
foreach($resultsm as $row){
	$data = str_replace('"', '\"',"{$row['actual_model']}|{$row['name']}|{$row['sku']}|{$row['manufacturer_value']}");
	$insert .= 'INSERT INTO catalogsearch_fulltext (product_id, store_id, data_index, updated) VALUES('.$row['entity_id'].', 19, "'.$data.'", 0);'."\n";
}
foreach($resultsmm as $row){
        $data = str_replace('"', '\"',"{$row['actual_model']}|{$row['name']}|{$row['sku']}|{$row['manufacturer_value']}");
        $insert .= 'INSERT INTO catalogsearch_fulltext (product_id, store_id, data_index, updated) VALUES('.$row['entity_id'].', 8, "'.$data.'", 0);'."\n";
}
//var_dump($insert);exit;
$writeConnection->query($insert);
