<?php
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);


$couponcsv = fopen("coupons.csv", "r");
$headerskipped = false;
$count = 0;
while($line = fgetcsv($couponcsv)){
	if($headerskipped==false){
		$headerskipped = true;
	}
	else{
		$data = array(
		    'product_ids' => null,
		    'name' => $line[1]."-SF",
		    'description' => "Imported from old startfitness site",
		    'is_active' => 1,
		    'website_ids' => array(3),
		    'customer_group_ids' => array(1),
		    'coupon_type' => 2,
		    'coupon_code' => $line[1],
		    'uses_per_coupon' => ($line[2] == 'n')?1:999999,
		    'uses_per_customer' => 1,
		    'from_date' => null,
		    'to_date' => date("Y-m-d", strtotime(str_replace("/", "-",$line[6]))),
		    'sort_order' => null,
		    'is_rss' => 1,
		    'rule' => array(
		        'conditions' => array(
		            array(
		                'type' => 'salesrule/rule_condition_combine',
		                'aggregator' => 'all',
		                'value' => 1,
		                'new_child' => null
		            )
		        )
		    ),
		    'simple_action' => ($line[8] == "p"? "by_percent":"by_fixed"),
		    'discount_amount' => (float)$line[7],
		    'discount_qty' => 0,
		    'discount_step' => null,
		    'apply_to_shipping' => 0,
		    'simple_free_shipping' => 0,
		    'stop_rules_processing' => 0,
		    'rule' => array(
		        'actions' => array(
		            array(
		                'type' => 'salesrule/rule_condition_product_combine',
		                'aggregator' => 'all',
		                'value' => 1,
		                'new_child' => null
		            )
		        )
		    ),
		    'store_labels' => array($line[1])
		);
		//var_dump($data);exit;
		$model = Mage::getModel('salesrule/rule');
		//$data = $this->_filterDates($data, array('from_date', 'to_date'));
		$validateResult = $model->validateData(new Varien_Object($data));
		if ($validateResult == true) {
		    if (isset($data['simple_action']) && $data['simple_action'] == 'by_percent'
		            && isset($data['discount_amount'])) {
		        $data['discount_amount'] = min(100, $data['discount_amount']);
		    }
		    if (isset($data['rule']['conditions'])) {
		        $data['conditions'] = $data['rule']['conditions'];
		    }
		    if (isset($data['rule']['actions'])) {
		        $data['actions'] = $data['rule']['actions'];
		    }
		    unset($data['rule']);
		    try{
		    	$model->loadPost($data);
		    	$model->save();
				echo "Added - {$data['coupon_code']}\n";
			}
			catch(exception $e){
				echo "!!!exception - {$e->getMessage()}-{$data['coupon_code']}!!!\n";
			}
		}
		else{
			echo "\t!!! failed at $count - {$data['coupon_code']}\n";
			//exit();
		}
		$count++;
		//if($count > 2) exit();
	}
}
