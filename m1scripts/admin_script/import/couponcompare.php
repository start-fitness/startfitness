<?php
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);


$couponcsv = fopen("coupon.csv", "r");
$headerskipped = false;
$count = 0;

$sql = "SELECT * FROM `salesrule_coupon` WHERE rule_id IN (( SELECT DISTINCT rule_id FROM `salesrule_website` WHERE website_id <> 3 )) GROUP BY rule_id ORDER BY rule_id ASC, created_at DESC";
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$result=$write->fetchAll($sql);
$existing = array();
foreach($result as $coupon){
	$existing[strtoupper($coupon['code'])] = $coupon;
	
}
//var_dump($existing['HANSEN15']);
while($line = fgetcsv($couponcsv)){
	if($headerskipped==false){
		$headerskipped = true;
	}
	else{
		$code = strtoupper($line[1]);
		if(isset($existing[$code])){
			echo "".$code." Exists on the site already!\n";
		}
		//var_dump($existing[$code]);
		$count++;
		//if($count > 2) exit();
	}
}
