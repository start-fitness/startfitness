\<?php
/*	
 *  Author: Charlie Stelling
 *	Company: Dimasoft Ltd.
 *	Date: 29th November 2013
 *	
 *	About: Run in Cron to send out shipping emails to completed
 *		   orders.
 *
*/

//require_once($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php'); 
require_once('/microcloud/domains/fitnes/domains/admin.startfitness.co.uk/admincode/htdocs_new/app/Mage.php'); //Path to Magento

umask(0);

Mage::app();

// Log File Location
$log_file = 'Shipping_Completed_Emails.log';

// Memory Limits and Error Reporting	
ini_set('memory_limit','5048M');

ini_set('display_errors',1);

error_reporting(E_ALL);

// Filter by date 
$today = date('Y-m-d H:i:s', time());

$date = new DateTime($today);
$date->modify('-14 day');
$date = $date->format('Y-m-d H:i:s');


send_email($date);



// Log errors
function log_this($log_content) {
	$today = date('Y-m-d H:i:s', time());
	
	if(file_exists($_SERVER['DOCUMENT_ROOT'].'/var/log/'.$log_file)) {

		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/var/log/'.$log_file, $url." - ".$today."\n \r ", FILE_APPEND | LOCK_EX);
	}
}

// Get order collection
function get_collection($date) {
	$orders =  Mage::getModel('sales/order')->getCollection()
        ->addFieldToFilter('status', 'complete') // Where status is complete
        ->addAttributeToFilter('created_at', array('gteq' => $date));
		
		return $orders;
}

// Is allowed to send email?
function can_email($order) {
	$storeId = $order->getStore()->getId();
	
	if (!Mage::helper('sales')->canSendNewShipmentEmail($storeId)) {
		return FALSE;
	} 
	else {
		return TRUE; // Can send Emails
	}
}

// Send Emails for orders that are completed
function send_email($date) {
		
	$orders = get_collection($date);

	foreach($orders as $order) {

		foreach($order->getShipmentsCollection() as $shipment)
		{
			if($shipment){
	 	
       	 		if(!$shipment->getEmailSent()){ // If email hasn't been sent.
			
     			 	if(!$shipment->sendEmail(true)) { // If error in sending email - log it.
						log_this(
							"Email Not Sent \n \r".
							"Order: ".$order['increment_id']."\n \r".
							"Customer: ".$order['customer_email']."\n \r"
						);
         		  	} else {
		            	$shipment->setEmailSent(true);
		            	$shipment->save();   
						echo $order['increment_id']." Sent!<br />";
					}  
				} 
			}
		}
	}	
}
