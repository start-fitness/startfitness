<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$query ="
UPDATE `catalog_product_entity_int` cpei
INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpei.`entity_id`
INNER JOIN (SELECT cpe.`entity_id`, cpei_free_shipping.`value` FROM `catalog_product_entity` cpe
INNER JOIN `catalog_product_entity_int` cpei_visibility ON cpei_visibility.`entity_id`=cpe.`entity_id` AND cpei_visibility.`attribute_id` = 102 AND cpei_visibility.`value`=4
INNER JOIN `catalog_product_entity_int` cpei_free_shipping ON cpei_free_shipping.`entity_id`=cpe.`entity_id` AND cpei_free_shipping.`attribute_id`=396
WHERE cpe.`type_id`='configurable') tmp_parent ON tmp_parent.`entity_id`=cpsl.`parent_id`
SET cpei.`value`=tmp_parent.`value`
WHERE cpei.`attribute_id`=396 AND cpei.`store_id`=0 AND cpei.`value`<>tmp_parent.`value`;
INSERT IGNORE INTO `catalog_product_entity_int` (entity_type_id, attribute_id , store_id, entity_id, value)
(SELECT 4,396,0,cpei.`entity_id`, tmp_parent.`value` FROM `catalog_product_entity_int` cpei
INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpei.`entity_id`
INNER JOIN (SELECT cpe.`entity_id`, cpei_free_shipping.`value` FROM `catalog_product_entity` cpe
INNER JOIN `catalog_product_entity_int` cpei_visibility ON cpei_visibility.`entity_id`=cpe.`entity_id` AND cpei_visibility.`attribute_id` = 102 AND cpei_visibility.`value`=4
INNER JOIN `catalog_product_entity_int` cpei_free_shipping ON cpei_free_shipping.`entity_id`=cpe.`entity_id` AND cpei_free_shipping.`attribute_id`=396
WHERE cpe.`type_id`='configurable') tmp_parent ON tmp_parent.`entity_id`=cpsl.`parent_id`
);
UPDATE `catalog_product_entity_int` cpei
INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpei.`entity_id`
INNER JOIN (SELECT cpe.`entity_id`, cpei_free_shipping.`value` FROM `catalog_product_entity` cpe
INNER JOIN `catalog_product_entity_int` cpei_visibility ON cpei_visibility.`entity_id`=cpe.`entity_id` AND cpei_visibility.`attribute_id` = 102 AND cpei_visibility.`value`=4
INNER JOIN `catalog_product_entity_int` cpei_free_shipping ON cpei_free_shipping.`entity_id`=cpe.`entity_id` AND cpei_free_shipping.`attribute_id`=199
WHERE cpe.`type_id`='configurable') tmp_parent ON tmp_parent.`entity_id`=cpsl.`parent_id`
SET cpei.`value`=tmp_parent.`value`
WHERE cpei.`attribute_id`=199 AND cpei.`store_id`=0 AND (cpei.`value` != tmp_parent.`value` 
                                                         OR (cpei.`value` IS NULL AND tmp_parent.`value` IS NOT NULL)
                                                         OR (cpei.`value` IS NOT NULL AND tmp_parent.`value` IS NULL)) ;
INSERT IGNORE INTO `catalog_product_entity_int` (entity_type_id, attribute_id , store_id, entity_id, value)
(SELECT 4,199,0,cpei.`entity_id`, tmp_parent.`value` FROM `catalog_product_entity_int` cpei
INNER JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpei.`entity_id`
INNER JOIN (SELECT cpe.`entity_id`, cpei_free_shipping.`value` FROM `catalog_product_entity` cpe
INNER JOIN `catalog_product_entity_int` cpei_visibility ON cpei_visibility.`entity_id`=cpe.`entity_id` AND cpei_visibility.`attribute_id` = 102 AND cpei_visibility.`value`=4
INNER JOIN `catalog_product_entity_int` cpei_free_shipping ON cpei_free_shipping.`entity_id`=cpe.`entity_id` AND cpei_free_shipping.`attribute_id`=199
WHERE cpe.`type_id`='configurable') tmp_parent ON tmp_parent.`entity_id`=cpsl.`parent_id`
);
";

$write->query($query);
