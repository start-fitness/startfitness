<?php
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting(-1);
	ini_set('memory_limit', '24192M');
	require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
	umask(0);
	Mage::app();
	$store_id = 0;
	Mage::app()->setCurrentStore($store_id);

	updateReportTable();
	 function updateReportTable(){
	 		deleteReportData();
			
			$counter = 0;
			$allStores = Mage::app()->getStores();
			foreach ($allStores as $_eachStoreId => $val){
				//$_eachStoreId = 6;
				//echo "\r\nStore ID: " . $_eachStoreId;
				$collection = Mage::getModel('catalog/product')->getCollection()
			       ->addAttributeToSelect('entity_id')
			       ->addAttributeToSelect('sku')
				   ->addAttributeToSelect('actual_model')
				   ->addAttributeToSelect('manufacturer')
				   ->addAttributeToSelect('gender')
				   ->addAttributeToSelect('name')
			       ->addAttributeToSelect('type_id')
			       ->addAttributeToSelect('msrp')
			       ->addAttributeToSelect('price')
				   ->addAttributeToFilter('type_id', 'configurable' )
				   ->addStoreFilter($_eachStoreId)
			       ->addAttributeToSelect('percentage_saving')
			       //->addAttributeToFilter('visibility', array('neq' => 1))
			       ->addAttributeToFilter('status', 1);
				   //->setCurPage(1)
				   //->setPageSize(10);
				   
				$counter = 0;
				// get all visible products
				$products_error = array();
				$insert_data = array();
				foreach($collection as $product){
					 $parent_data = array();
					 // set parent values
					 $parent_data['store_id'] = $_eachStoreId;
					 $parent_data['attribute_set'] = $product->getAttributeSetId();
					 $parent_data['product_id'] = $product->getId();
					 $parent_data['name'] = $product->getName();
					 $parent_data['sku'] = $product->getSku();
					 $parent_data['actual_model'] = $product->getActualModel();
					 $parent_data['manufacturer'] = $product->getManufacturer();
					 $parent_data['gender'] = $product->getGender();
					 $parent_data['type_id'] = $product->getTypeId();
					 $parent_data['rrp'] = $product->getMsrp();
					 $parent_data['price'] = $product->getFinalPrice();
					 $parent_data['percentage_saving'] = $product->getPercentageSaving();
					 $total_qty = 0;
				     $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
					 foreach($childProducts as $child){
						 $total_qty += Mage::getModel('cataloginventory/stock_item')->loadByProduct($child)->getQty();				 
					 }
					 $parent_data['qty'] = $total_qty;
					 $parent_data['parent_id'] = 0;
					 $parent_data['is_orphan'] = false;
					 //print_r($parent_data);
					 //$record = checkProductExists($parent_data);
					 //print_r($record);
					 //echo $record['product_id'];
					 $counter++;
					 /*if(!empty($record)){
					 	
					 	//$parent_data['id'] = $record[0]['id'] . " :";
					 	// check data against current data
						if(productChanged($product, $record[0], $parent_data['qty'])){
							$parent_data['id'] = $record[0]['id'];
						 	echo "\r\n ". $counter . ") Updating: " . $parent_data['product_id'];
							//print_r($parent_data);
						 	updateReportData($parent_data);
						}
						else{
								echo "\r\n ". $counter . ") Skipping parent: " . $parent_data['product_id'];
							}					 	
					 }
					 else{
					 	echo "\r\n ". $counter . ") Inserting: " . $parent_data['product_id'];
					 	insertReportData($parent_data);
					 }*/
					 
					 echo "\r\n ". $counter . ") Inserting: " . $parent_data['product_id'];
					 //deleteReportRecord($product->getId(), $_eachStoreId);
					 insertReportData($parent_data);

					 //set child values
					 foreach($childProducts as $child){
					 	 $child_data = array();
						 $child_data['store_id'] = $_eachStoreId;
						 $child_data['attribute_set'] = $child->getAttributeSetId();
						 $child_data['product_id'] = $child->getId();
						 $child_data['name'] = $child->getName();
						 $child_data['sku'] = $child->getSku();
					 	 $child_data['actual_model'] = $child->getActualModel();
						 $child_data['manufacturer'] = $child->getManufacturer();
					     $child_data['gender'] = $child->getGender();
						 $child_data['type_id'] = $child->getTypeId();
						 $child_data['rrp'] = $child->getMsrp();
						 $child_data['price'] = $child->getFinalPrice();
						 $child_data['percentage_saving'] = $child->getPercentageSaving();
						 $child_data['qty'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($child)->getQty();
						 $child_data['parent_id'] = $product->getId();
						 $child_data['is_orphan'] = false;
						 						 
						 //$record = checkProductExists($child_data);
						 $counter++;
						 /*if(!empty($record)){
						 	if(productChanged($child, $record[0], $child_data['qty'])){
							 	echo "\r\n ". $counter . ") Updating: " . $child_data['product_id'];
							 	$child_data['id'] = $record[0]['id'];
							 	updateReportData($child_data);
							}
							else{
								echo "\r\n ". $counter . ") Skipping child: " . $child_data['product_id'];
							}
						 }
						 else{
						 	echo "\r\n ". $counter . ") Inserting: " . $child_data['product_id'];
						 	insertReportData($child_data);
						 }*/
						 echo "\r\n ". $counter . ") Inserting: " . $child_data['product_id'];
						 //deleteReportRecord($child->getId(), $_eachStoreId);
						 insertReportData($child_data);		 
					 }
				}

				// orphan products
				$orphan_collection = Mage::getModel('catalog/product')->getCollection()
			       ->addAttributeToSelect('entity_id')
			       ->addAttributeToSelect('sku')
				   ->addAttributeToSelect('actual_model')
				   ->addAttributeToSelect('name')
				   ->addAttributeToSelect('manufacturer')
				   ->addAttributeToSelect('gender')
			       ->addAttributeToSelect('type_id')
			       ->addAttributeToSelect('msrp')
			       ->addAttributeToSelect('price')
				   ->addAttributeToFilter('type_id', 'simple' )
				   ->addAttributeToFilter('visibility', array('eq' => 4))
				   ->addStoreFilter($_eachStoreId)
			       ->addAttributeToSelect('percentage_saving')
			       //->addAttributeToFilter('visibility', array('neq' => 1))
			       ->addAttributeToFilter('status', 1);
				   //->setCurPage(1)
				   //->setPageSize(10);
				   foreach($orphan_collection as $orphan){
					 	 $orphan_data = array();
						 $orphan_data['store_id'] = $_eachStoreId;
						 $orphan_data['attribute_set'] = $orphan->getAttributeSetId();
						 $orphan_data['product_id'] = $orphan->getId();
						 $orphan_data['name'] = $orphan->getName();
						 $orphan_data['sku'] = $orphan->getSku();
					 	 $orphan_data['actual_model'] = $orphan->getActualModel();
						 $orphan_data['manufacturer'] = $orphan->getManufacturer();
					     $orphan_data['gender'] = $orphan->getGender();
						 $orphan_data['type_id'] = $orphan->getTypeId();
						 $orphan_data['rrp'] = $orphan->getMsrp();
						 $orphan_data['price'] = $orphan->getFinalPrice();
						 $orphan_data['percentage_saving'] = $orphan->getPercentageSaving();
						 $orphan_data['qty'] = Mage::getModel('cataloginventory/stock_item')->loadByProduct($orphan)->getQty();
						 $orphan_data['parent_id'] = 0;
						 $orphan_data['is_orphan'] = true;
						 						 
						 //$record = checkProductExists($orphan_data);
						 $counter++;
						 /*if(!empty($record)){
						 	if(productChanged($orphan, $record[0], $orphan_data['qty'])){
							 	echo "\r\n ". $counter . ") Updating: " . $orphan_data['product_id'];
							 	$orphan_data['id'] = $record[0]['id'];
							 	updateReportData($orphan_data);
							}
							else{
								echo "\r\n ". $counter . ") Skipping orphan: " . $orphan_data['product_id'];
							}

						 }
						 else{
						 	echo "\r\n ". $counter . ") Inserting: " . $orphan_data['product_id'];
						 	insertReportData($orphan_data);
						 }*/
						 echo "\r\n ". $counter . ") Inserting: " . $orphan_data['product_id'];
						 //deleteReportRecord($orphan->getId(), $_eachStoreId);
						 insertReportData($orphan_data);		 
				   }
//die;
			}

		echo "\r\nTotal Products Processed : " . $counter;			
	}

	function productChanged($_product, $data, $actual_qty){
		//echo "\r\n RESULTS";
		//$_product = Mage::getModel('catalog/product')->load($data['product_id']);
		$changed = false;
		if($_product->getName() != $data['name']){
			echo "\r\n1";
			$changed = true;
		}
		if($_product->getSku() != $data['sku']){
			echo "\r\n2";
			$changed = true;
		}
		if($_product->getActualModel() != $data['actual_model']){
			echo "\r\n3";	
			$changed = true;
		}
		if($_product->getManufacturer() != $data['manufacturer']){
			echo "\r\n4";
			$changed = true;
		}
		if($_product->getGender() != $data['gender']){
			echo "\r\n" . $_product->getGender();
			echo "\r\n" . $data['gender'];
			echo "\r\n5";
			$changed = true;
		}
		if($_product->getFinalPrice() == null && $data['price'] == 0){
			
		}
		elseif($_product->getFinalPrice() != $data['price']){
				echo "\r\n6";
				$changed = true;
		}
		if($_product->getMsrp() == null && $data['rrp'] == 0){
			
		}
		elseif($_product->getMsrp() != $data['rrp']){
				echo "\r\n7";
				$changed = true;
		}
		
		if($_product->getPercentageSaving() != $data['percentage_saving']){
		    echo "\r\n8";
			$changed = true;
		}
		if($actual_qty != $data['qty']){
		    echo "\r\n9";
			$changed = true;
		}
		
		//echo "\r\n FINISHED RESULTS";
		if($changed){
			//echo "\r\ntrue";
			echo $_product->getId();
			//die;
		}
		else{
			//echo "\r\nfalse";
		}
		return $changed;
	}

	function checkProductExists($data){
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$sql = "SELECT *          
				FROM dimasoft_decended_stock 
				WHERE product_id = " . $data['product_id'] . " 
				AND store_id = " . $data['store_id']; 		  											  
		$result = $readConnection->fetchAll($sql);
		return $result;
	}

	function updateReportData($binds){		
		//print_r($binds); die;
		$resource = Mage::getSingleton('core/resource');
		$write = $resource->getConnection("core_write");
		$query = "UPDATE dimasoft_decended_stock 
				  SET 	 store_id = :store_id,
						 attribute_set = :attribute_set,
						 product_id = :product_id,
						 name = :name,
						 sku = :sku,
						 actual_model = :actual_model,
						 manufacturer = :manufacturer,
						 gender = :gender,
						 qty = :qty,
						 type_id = :type_id,
						 rrp = :rrp,
						 price = :price,
						 percentage_saving = :percentage_saving,
						 parent_id = :parent_id,
						 is_orphan = :is_orphan
				  WHERE id = :id";
		try {
	        $write->query($query, $binds);
	        //echo "Data successfully inserted. Insert ID: ".$insertId;
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
			
			/* 
			 * SELECT dcs_parent.qty, dcs_parent.percentage_saving, dcs_parent.type_id, dcs_parent.name, dcs_child.* FROM `dimasoft_decended_stock` dcs_parent
				JOIN `dimasoft_decended_stock` dcs_child ON dcs_parent.product_id = dcs_child.parent_id
				WHERE dcs_parent.type_id = 0
				ORDER BY dcs_parent.qty DESC  
			 * */
	}

	function insertReportData($binds){		
		
		//print_r($binds); die;
		$resource = Mage::getSingleton('core/resource');
		$write = $resource->getConnection("core_write");
		$query = "INSERT INTO dimasoft_decended_stock (
														 store_id,
														 attribute_set,
														 product_id,
														 name,
														 sku,
														 actual_model,
														 manufacturer,
														 gender,
														 qty,
														 type_id,
														 rrp,
														 price,
														 percentage_saving,
														 parent_id,
														 is_orphan
														 ) values (
														 :store_id,
														 :attribute_set,
														 :product_id,
														 :name,
														 :sku,
														 :actual_model,
														 :manufacturer,
														 :gender,
														 :qty,
														 :type_id,
														 :rrp,
														 :price,
														 :percentage_saving,
														 :parent_id,
														 :is_orphan
														 )";
		try {
	        $write->query($query, $binds);
	        //echo "Data successfully inserted. Insert ID: ".$insertId;
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
			
			/* 
			 * SELECT dcs_parent.qty, dcs_parent.percentage_saving, dcs_parent.type_id, dcs_parent.name, dcs_child.* FROM `dimasoft_decended_stock` dcs_parent
				JOIN `dimasoft_decended_stock` dcs_child ON dcs_parent.product_id = dcs_child.parent_id
				WHERE dcs_parent.type_id = 0
				ORDER BY dcs_parent.qty DESC  
			 * */
	}
	
	
	function deleteReportData($date){
		$resource = Mage::getSingleton('core/resource');
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$query = "DELETE FROM dimasoft_decended_stock";
		$write->query($query);
		try {
	        $write->query($query);
	        //echo "Data deleted successfully";
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
	}
	
	function deleteReportRecord($product_id, $store_id){
		$resource = Mage::getSingleton('core/resource');
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$query = "DELETE FROM dimasoft_decended_stock WHERE product_id = " . $product_id . " AND store_id = " . $store_id;
		$write->query($query);
		try {
	        $write->query($query);
	        //echo "Data deleted successfully";
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
	}
	
	
	function checkForParent($items, $sku){
		foreach ($items as $itemId => $item){
			if($item->getSku() == $sku && $item->getProduct()->getTypeId() != 'simple'){
				return $item;
			}                	
		}
		return false;
	}
	
	