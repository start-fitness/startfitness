<?php
// Start Group Script To Populate Percentage Savings
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$ebay_attr = "369";
$amazon_attr = "370";

$sql = "SELECT j1.*, j2.value_id as ebay_exists, j2.value as ebay_name, j3.value_id as amazon_exists, j3.value as amazon_name 
FROM `catalog_product_entity_varchar` j1 
LEFT JOIN `catalog_product_entity_varchar` j2 ON j2.entity_id = j1.entity_id AND j2.attribute_id = {$ebay_attr} AND j2.store_id = 0 
LEFT JOIN `catalog_product_entity_varchar` j3 ON j3.entity_id = j1.entity_id AND j3.attribute_id = {$amazon_attr} AND j3.store_id = 0 
WHERE j1.`attribute_id` = 71 and j1.store_id = 0 AND (j2.value IS NULL OR j3.value IS NULL)";
//var_dump($sql);
//exit;
$res = $read->fetchAll($sql);

//var_dump($res);
$sqlinsertTpl = "INSERT INTO catalog_product_entity_varchar (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) VALUES ";
$sqlinsert = "";
$insertcounter = 0;
$currinsertcounter = 0;
$totalcounter = 0;
$totalintotal = count($res);
$updatecounter = 0;
foreach($res as $row){
	$updatesql = "";
	$totalcounter++;$totalcounter++;
	$row['value'] = str_replace("'", "''", $row['value']);
	if($sqlinsert == "") $sqlinsert = $sqlinsertTpl;
	if($row["ebay_exists"] == null){
		$valuesinsert = "\n({$row['entity_type_id']}, {$ebay_attr}, {$row['store_id']}, {$row['entity_id']}, '{$row['value']}'),";
		$sqlinsert .= $valuesinsert;
		$currinsertcounter++;$insertcounter++;
	}
	elseif($row["ebay_name"] == NULL){
		$updatesql = "UPDATE `catalog_product_entity_varchar` SET value = '{$row['value']}' WHERE value_id = {$row['ebay_exists']}";
		try{
			$write->query($updatesql);
			$updatecounter++;
		}
		catch(exception $e){
			var_dump($e->getMessage());
		}
		//echo $updatesql;exit;
		// UPDATE?
	}
	
	$updatesql = "";
	if($row["amazon_exists"] == null){
		$valuesinsert = "\n({$row['entity_type_id']}, {$amazon_attr}, {$row['store_id']}, {$row['entity_id']}, '{$row['value']}'),";
		$sqlinsert .= $valuesinsert;
		$currinsertcounter++;$insertcounter++;
	}
	elseif($row["amazon_name"] == NULL){
		// UPDATE?
		$updatesql = "UPDATE `catalog_product_entity_varchar` SET value = '{$row['value']}' WHERE value_id = {$row['amazon_exists']}";
		try{
			$write->query($updatesql);
			$updatecounter++;
		}
		catch(exception $e){
			var_dump($e->getMessage());
		}
	}
	
	if($currinsertcounter > 50){
		$sqlinsert = rtrim($sqlinsert, ",");
		try{
			$write->query($sqlinsert);
		}
		catch(exception $e){
			var_dump($e->getMessage());
		}
		echo "BATCH INSERTED ... processed: $totalcounter of $res\n";
		$sqlinsert = "";
		$currinsertcounter = 0;
		
	}
}
if($currinsertcounter > 0){
		$sqlinsert = rtrim($sqlinsert, ",");
		try{
			$write->query($sqlinsert);
		}
		catch(exception $e){
			var_dump($e->getMessage());
		}
		echo "BATCH INSERTED ... processed: $totalcounter of $res\n";
		$sqlinsert = "";
		$currinsertcounter = 0;
		
	}
echo "\n\nTOTAL INSERTED: $insertcounter of $totalcounter -- TOTAL UPDATED: $updatecounter\n";
