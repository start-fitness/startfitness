#<?php
require dirname(__FILE__).'/phpmailer/class.phpmailer.php';
require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento (CRON)
umask(0);
Mage::app();

$store_id = 0;
Mage::app()->setCurrentStore($store_id);


ini_set('memory_limit','19768M');
ini_set('display_errors',1);
error_reporting(E_ALL);
 
$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', array('eq' => 'configurable'))->addAttributeToFilter('entity_id', array('gt' => 342000));


$parentOOS =  array();
$message= "";
$x = 0;
$a = 0;
foreach ($collectionConfigurable as $_configurableproduct) {
$a++;
echo "A".$a."\r\n";	
    /**
    * Load product by product id
    */
    
    $product = Mage::getModel('catalog/product')->load($_configurableproduct->getId());
    /**
    * only process product if it is available (saleable)
    */
    
   // if ($product->isSaleable()) {
        /**
        * Get children products (all associated children products data)
        */
        
        $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);

        //$stockItem = $product->getStockItem();
        /**
        * For all configurable products which are out of stock
        */
          
        $outofstock_childrenisinstock = false; // If all simple products are out of stock - keep as false.

        foreach ($childProducts as $childProduct) {
            $qty = Mage::getModel('cataloginventory/stock_item')->loadByProduct($childProduct)->getQty();
            if ($qty > 0) { // If asscoiated product is in stock.
                  $outofstock_childrenisinstock = true;
            } 
        }
		
	
		// If all simple products are out of stock set parent as OOS.
        if (!$outofstock_childrenisinstock) {
                $stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
                if($stockItem->getBackorders() != 1){
                	$stockItem->setData('is_in_stock', 0);
					$stockItem->save();
					echo $x.$product->getSku();
                	$message .= "<p>".$product->getName()." (".$product->getSku().") is <span style='font-weight: bold;color: red'>OUT OF STOCK</span></p>";
					$x++;
				}
				elseif($stockItem->getBackorders() == 1){
					$stockItem->setData('is_in_stock', 1);
					$stockItem->save();
					echo $x.$product->getSku();
                	$message .= "<p>".$product->getName()." (".$product->getSku().") is <span style='font-weight: bold;color: green'>IN STOCK</span></p>";
				}
	        unset($stockItem);

                
        	
	}
//	else
//	{
//                $stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($product->getId());
//                $stockItem->setData('is_in_stock', 1);
//                $stockItem->save();
//                unset($stockItem);
//
//                echo $x.$product->getSku();
//                $message .= "<p>".$product->getName()." (".$product->getSku().") is <span style='font-weight: bold;color: red'>IN STOCK</span></p>";
//                $x++;
//	}
}
//}


	/* foreach($parentOOS as $indParentOOS) {
		print_r($indParentOOS)."<br />";
	} */

list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('productoptionsoos',true);

$mail = new PHPMailer;
//$mail->isSMTP();                                      // Set mailer to use SMTP
//$mail->Host = 'localhost';  // Specify main and backup server
//$mail->SMTPAuth = false;                               // Enable SMTP authentication
//$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
//$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startfitness.co.uk';
$mail->FromName = 'Stock Manager';

foreach($toEmailAddresses as $toEmailAddress){
    $mail->AddAddress($toEmailAddress,$toEmailAddress);
}

foreach($ccEmailAddresses as $ccEmailAddress){
    $mail->AddCC($ccEmailAddress,$ccEmailAddress);
}
$mail->addAddress("gary@startfitness.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Out of stock notice for product options';
$mail->Body    = $message ."<br /><br /> Count: ".$x;

if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';

