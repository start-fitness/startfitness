<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '2096M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
//$store_id = 0;
//Mage::app()->setCurrentStore($store_id);

$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')
				->addAttributeToSelect('*')
                ->addAttributeToFilter('type_id', array('eq' => 'configurable'));
				//->addAttributeToFilter('sku',  'MM1566');
				
$in_stock_counter = 0;
$out_of_stock_counter = 0;
$product_counter = 0;
foreach ($collectionConfigurable as $_product) {
	$stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product->getId());
		
	if($_product->isSaleable()) {
		$stockItem->setData('is_in_stock', 1);
		$in_stock_counter++;
	}else{
		$stockItem->setData('is_in_stock', 0);
		$out_of_stock_counter++;
	}
	$stockItem->save();
	$product_counter++;
	echo "\r\n" . $product_counter . ")";
	unset($_product);
	unset($stockItem);
}

echo "\r\n Number of products set to in stock: " . $in_stock_counter;
echo "\r\n Number of products set to out of stock: " . $out_of_stock_counter;
echo "\r\n Total number of products: " . $product_counter;

list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('configstockupdate');
mail($toEmailAddresses, 'Cron Complete - http://www.startfitness.co.uk/script/update_configurable_stock.php', date('Y-M-D H:i:s'));