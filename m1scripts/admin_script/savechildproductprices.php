<?php

ini_set('memory_limit', '3024M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$store_id = 0;
ini_set('display_errors',1);

Mage::app()->setCurrentStore($store_id);
get_product_type();
//echo "hello"; die;

function get_product_type(){
		//echo "hello2"; die;
		// read the csv file, line by line
		$file = "more-mile-prices.csv";
		$handle = fopen($file, "r") or die("can't open file");
		$store_id = 8;
		
		if ($handle !== FALSE) {
			$headers = true;
			//echo "<table>";	
			$cycles = 0;
			$counter = 0;
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				//if(!$headers){
					$cycle++;
					unset($_product);	
					$_product = Mage::getModel('catalog/product')->setStoreId($store_id)->loadByAttribute('sku', $data[0]);
					if($_product){
						echo "\r\n" . $cycle . ") Parent Product: " . $_product->getSku();	
						echo "\r\n Price: " . $price = $data[1];
						$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $_product);
						if($childProducts){
							foreach($childProducts as $child){
								if($child->getPrice() != $price){
									unset($_childproduct);
									$_childproduct = Mage::getModel('catalog/product')->setStoreId($store_id)->load($child->getId());
									$_childproduct->setStoreId($store_id)->setPrice($price);
							    	try{
										$_childproduct->save();
										$counter++;
										echo "\r\n" . $cycle . ") Product Updated: " . $_childproduct->getSku();	
									}
									catch (Exception $e) {
										$products_error[] = $_childproduct->getId();
										echo "\r\n" . $cycle . ") Product Update Error: " . $e . " | " . $_childproduct->getSku();
									}
								}
								else{
									echo "\r\n" . $cycle . ") Product Skipped: " . $child->getSku(); 
								}
							}
							
						}
					}

				/*if($cycle > 1){
					break;
				}*/
				//$headers = false;   
			}
			//echo "</table>";

			fclose($handle);
		}

		if(!empty($products_error)){
			echo "The following products were not updated: " . implode(',', $products_error);
			}
			echo "\r\nNumber products updated: " . $counter;
	}

?>