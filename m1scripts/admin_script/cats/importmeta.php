<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$f = fopen(dirname(__FILE__)."/catsmeta2.csv", "r");
$headers  = fgetcsv($f);

//$headers = array_flip($headers);

//var_dump($headers);
$cats = array();
//fgetcsv($f);
while($row = fgetcsv($f)){
	
	$data = array();
	foreach($row as $index=>$datav){
		
		$data[trim($headers[$index])] = $datav;
		
	}
	$cats[] = $data;
	
	
	
}

fclose($f);
foreach($cats as $cat){
	
	$cid = updateCat($cat);
	//var_dump($cid);exit;
	var_dump($cid);
}

function updateCat($data, $model, $type, $links = array()){
	
	$cat = Mage::getModel('catalog/category')->load($data['entity_id']);
	if(!$cat){
	 return null;
	}
	else{
		var_dump($data['entity_id'] . " - " . $data['meta_description'] . " - " .$data['meta_title']. " - " .$data['meta_keywords'] );
		if($data["meta_description"] == "NULL" ) $data["meta_description"]= "";
		if($data["meta_title"] == "NULL") $data["meta_title"]= "";
		if($data["meta_keywords"] == "NULL") $data["meta_keywords"]= "";
		$cat->setData("meta_description", $data["meta_description"]);
		$cat->setData("meta_title", $data["meta_title"]);
		$cat->setData("meta_keywords", $data["meta_keywords"]);
		try{
			$cat->save();
		}
		catch(exception $e){
			
		}
		//var_dump($product->getData());
		return $cat->getEntityId();
	}
	
}



function addAttributeValue($attributeCode, $attValue) {
	$value = attributeValueExists($attributeCode, $attValue);
    if (!$value) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attributeCode);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['option_name'][0] = $attValue;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }
	return attributeValueExists($attributeCode, $attValue);
	
}

function attributeValueExists($attribute, $value) {
    $attribute_model = Mage::getModel('eav/entity_attribute');
    $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
    $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute);
    $attribute = $attribute_model->load($attribute_code);
    $attribute_options_model->setAttribute($attribute);
    $options = $attribute_options_model->getAllOptions(false);

    foreach ($options as $option) {
        if ($option['label'] == $value) {
            return $option['value'];
        }
    }
    return false;
}