<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$cats = array();
$allcats = array();
$sql = array();

$cats = array();
getCatIds(2473, $cats);
$allfitnesscats = implode(",", $cats);
$cats = array();

getCatIds(2995, $cats);

$cats = implode(",", $cats);
$allcats[] = $cats;
$sql[] = '
SELECT "cycle" as name,  SUM(sales_flat_order_item.base_row_invoiced + sales_flat_order_item.base_tax_invoiced - sales_flat_order_item.base_discount_invoiced) total FROM sales_flat_order_item
JOIN sales_flat_order ON sales_flat_order.entity_id  = sales_flat_order_item.order_id
WHERE sales_flat_order_item.created_at BETWEEN "2018-03-19 00:00:00" AND "2018-03-25 23:59:59" AND sales_flat_order_item.product_id IN (
    SELECT product_id  FROM `catalog_category_product` 
    WHERE category_id IN ('.$allfitnesscats.')
    GROUP BY product_id HAVING MAX(`category_id`) IN ('.$cats.')
	) 
AND sales_flat_order.store_id IN (6, 19)
';

$cats = array();
getCatIds(2638, $cats);

$cats = implode(",", $cats);
$allcats[] = $cats;
$sql[] = '
SELECT "football" as name, SUM(sales_flat_order_item.base_row_invoiced + sales_flat_order_item.base_tax_invoiced - sales_flat_order_item.base_discount_invoiced) total FROM sales_flat_order_item
JOIN sales_flat_order ON sales_flat_order.entity_id  = sales_flat_order_item.order_id
WHERE sales_flat_order_item.created_at BETWEEN "2018-03-19 00:00:00" AND "2018-03-25 23:59:59" AND sales_flat_order_item.product_id IN (
    SELECT product_id  FROM `catalog_category_product` 
    WHERE category_id IN ('.$allfitnesscats.')
    GROUP BY product_id HAVING MAX(`category_id`) IN ('.$cats.')
	) 
AND sales_flat_order.store_id IN (6, 19)
';

$cats = array();
getCatIds(3388, $cats);

$cats = implode(",", $cats);
$allcats[] = $cats;
$sql[] = '
SELECT "running" as name, SUM(sales_flat_order_item.base_row_invoiced + sales_flat_order_item.base_tax_invoiced - sales_flat_order_item.base_discount_invoiced) total FROM sales_flat_order_item
JOIN sales_flat_order ON sales_flat_order.entity_id  = sales_flat_order_item.order_id
WHERE sales_flat_order_item.created_at BETWEEN "2018-03-19 00:00:00" AND "2018-03-25 23:59:59" AND sales_flat_order_item.product_id IN (
    SELECT product_id  FROM `catalog_category_product` 
    WHERE category_id IN ('.$allfitnesscats.')
    GROUP BY product_id HAVING MAX(`category_id`) IN ('.$cats.')
	) 
AND sales_flat_order.store_id IN (6, 19)
';

$cats = array();
getCatIds(3409, $cats);

$cats = implode(",", $cats);
$allcats[] = $cats;
$sql[] = '
SELECT "gym" as name, SUM(sales_flat_order_item.base_row_invoiced + sales_flat_order_item.base_tax_invoiced - sales_flat_order_item.base_discount_invoiced) total FROM sales_flat_order_item
JOIN sales_flat_order ON sales_flat_order.entity_id  = sales_flat_order_item.order_id
WHERE sales_flat_order_item.created_at BETWEEN "2018-03-19 00:00:00" AND "2018-03-25 23:59:59" AND sales_flat_order_item.product_id IN (
    SELECT product_id  FROM `catalog_category_product` 
    WHERE category_id IN ('.$allfitnesscats.')
    GROUP BY product_id HAVING MAX(`category_id`) IN ('.$cats.')
	) 
AND sales_flat_order.store_id IN (6, 19)
';


$allcats = implode(",", $allcats);

$sql[] = '
SELECT "OTHERS", SUM(sales_flat_order_item.base_row_invoiced + sales_flat_order_item.base_tax_invoiced - sales_flat_order_item.base_discount_invoiced) total FROM sales_flat_order_item
JOIN sales_flat_order ON sales_flat_order.entity_id  = sales_flat_order_item.order_id
WHERE sales_flat_order_item.created_at BETWEEN "2018-03-19 00:00:00" AND "2018-03-25 23:59:59" AND sales_flat_order_item.product_id IN (
    SELECT product_id  FROM `catalog_category_product`  
    WHERE category_id IN ('.$allfitnesscats.')
    GROUP BY product_id HAVING MAX(`category_id`) NOT IN ('.$allcats.')
	) 
AND sales_flat_order.store_id IN (6, 19)
';

echo implode("\nUNION\n",$sql) ."\n";

function getCatIds($id, &$cats){
	$cats[] = $id;
	$_category = Mage::getModel('catalog/category')->load($id);
	$catID = $_category->getId(); //or any specific category id, e.g. 5
	$children = Mage::getModel('catalog/category')->getCategories($catID);
	foreach ($children as $category) {
			getCatIds($category->getId(), $cats);
	}
}
