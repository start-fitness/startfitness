<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$quote_id = 1471065;
Mage::app()->setCurrentStore(5);
convert_to_sales_order($quote_id);

function convert_to_sales_order($quote_id){
		//echo "hello"; die;
		//echo "hello"; die;
		$quoteObj=Mage::getModel('sales/quote')->load($quote_id);
		if(!$quoteObj){
			echo "Quote doesnt exist!";
			die;
		}
		//print_r($quoteObj); die;
	    $items=$quoteObj->getAllItems();
	    $quoteObj->collectTotals();
	    $quoteObj->reserveOrderId();
	
	    $quotePaymentObj=$quoteObj->getPayment();
		//print_r($quotePaymentObj->getData()); die;
	    //methods: authorizenet, paypal_express, googlecheckout, purchaseorder
	    $quotePaymentObj->setMethod('purchaseorder');
	    $quoteObj->setPayment($quotePaymentObj);
	    $convertQuoteObj=Mage::getSingleton('sales/convert_quote');
	    $orderObj = $convertQuoteObj->addressToOrder($quoteObj->getShippingAddress());
	    $orderPaymentObj=$convertQuoteObj->paymentToOrderPayment($quotePaymentObj);
	
	    $orderObj->setBillingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getBillingAddress()));
	    $orderObj->setShippingAddress($convertQuoteObj->addressToOrderAddress($quoteObj->getShippingAddress()));
	    $orderObj->setPayment($convertQuoteObj->paymentToOrderPayment($quoteObj->getPayment()));
	    
	    foreach ($items as $item) {
	        //@var $item Mage_Sales_Model_Quote_Item
	        $orderItem = $convertQuoteObj->itemToOrderItem($item);
	        if ($item->getParentItem()) {
	            $orderItem->setParentItem($orderObj->getItemByQuoteItemId($item->getParentItem()->getId()));
	        }
	        $orderObj->addItem($orderItem);
	    }
	    
	    $orderObj->setCanShipPartiallyItem(false);
	    
	    $totalDue=$orderObj->getTotalDue();
	    $orderObj->place(); //calls _placePayment
	    $orderObj->save(); 
	    $orderId=$orderObj->getId();
		echo $orderId;
}