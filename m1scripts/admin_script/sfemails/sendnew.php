<?php
//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

require_once dirname(__FILE__).'/../../app/Mage.php';
require_once dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
require_once dirname(__FILE__).'/../phpmailer/class.pop3.php';
require_once dirname(__FILE__).'/../phpmailer/class.smtp.php';

umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$lastsent = file_get_contents(dirname(__FILE__)."/lastsend.txt");
if(empty($lastsent)) 
{
	$lastsent = time();
	file_put_contents(dirname(__FILE__)."/lastsend.txt", $lastsent);
	exit;
}
$thistime = time();
$lastsent = date("Y-m-d H:i:s", $lastsent);
$sql = "SELECT * FROM dimasoft_coupon_sent WHERE timestamp > '".$lastsent."' AND website_id = 3";

$lines = $write->fetchAll($sql);
if(count($lines) < 1){
	exit;
}
$f = fopen(dirname(__FILE__)."/emails.csv", "w+");
foreach($lines as $line){
	fputcsv($f, array($line['email']));
}
fclose($f);

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'localhost';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = false;                               // Enable SMTP authentication
    //$mail->Username = 'user@example.com';                 // SMTP username
    //$mail->Password = 'secret';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //$mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('cron@startfitness.co.uk');
	$mail->addAddress('mark@startfitness.co.uk');   
    $mail->addBCC('geoffrey@startfitness.co.uk');     // Add a recipient
                // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    $mail->addAttachment(dirname(__FILE__)."/emails.csv");         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'StartFitness 10% off mailout list';
    $mail->Body    = 'CSV file is attached';
    $mail->AltBody = 'CSV file is attached';

    $mail->send();
    echo 'Message has been sent';
	file_put_contents(dirname(__FILE__)."/lastsend.txt", $thistime);
} catch (Exception $e) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
