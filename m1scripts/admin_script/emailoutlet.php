<?php
ini_set('memory_limit', '2048M');
require dirname(__FILE__).'/phpmailer/class.phpmailer.php';
require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');
$message = '';

$query1 = '
SELECT DISTINCT catalog_category_product.product_id, catalog_product_flat_6.sku, catalog_product_flat_6.actual_model, catalog_product_flat_6.name, catalog_product_flat_6.percentage_saving, catalog_product_flat_6.price, catalog_product_flat_6.special_price, SUM(IF(stock2.qty IS NULL, 0, stock2.qty)) as total_qty FROM catalog_category_flat_store_6
JOIN catalog_category_product ON catalog_category_product.category_id = catalog_category_flat_store_6.entity_id
JOIN catalog_product_flat_6 ON catalog_category_product.product_id = catalog_product_flat_6.entity_id
JOIN cataloginventory_stock_item stock1 ON stock1.product_id = catalog_category_product.product_id
JOIN catalog_product_super_link link ON link.parent_id = catalog_category_product.product_id
JOIN cataloginventory_stock_item stock2 ON stock2.product_id = link.product_id
WHERE catalog_category_flat_store_6.path LIKE \'1/2473/3270%\'
AND catalog_category_product.product_id NOT IN (
	SELECT DISTINCT catalog_category_product.product_id FROM catalog_category_flat_store_6
    JOIN catalog_category_product ON catalog_category_product.category_id = catalog_category_flat_store_6.entity_id
    WHERE catalog_category_flat_store_6.path  NOT LIKE \'1/2473/3270%\' AND catalog_category_flat_store_6.path NOT LIKE \'1/2473/2630%\' AND catalog_category_flat_store_6.is_active = 1
	
)
/*AND stock1.is_in_stock = 1*/
GROUP BY catalog_category_product.product_id
ORDER BY IF(SUM(IF(stock2.qty IS NULL, 0, stock2.qty)) > 0, 1, 0) desc, `catalog_product_flat_6`.`name` asc
';


$res1 = $readConnection->fetchAll($query1);

$t1 = "";


$cachetable = array();
$x=0;
foreach($res1 as $tmpline){
	$x++;
	$t1.='
	<tr>
	<td>'.$tmpline['product_id'].'</td>
	<td>'.$tmpline['sku'].'</td>
	<td>'.$tmpline['actual_model'].'</td>
	<td>'.$tmpline['name'].'</td>
	</tr>
	';
}

unset($cachetabletmp);

$mail = new PHPMailer;
$email ="richard@startfitness.co.uk";
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
$mail->addAddress($email);  // Add a recipient
$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
$mail->addAddress("geoffrey@startfitness.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness - Unassigned products report';
$mail->Body    = 

'
<h3>Products which currently are not assigned to an active category</h3>
<table>
<tr>
<th>ID</th>
<th>SKU</th>
<th>MODEL #</th>
<th>NAME</th>
</tr>
'.$t1.'
</table>
';

if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';

