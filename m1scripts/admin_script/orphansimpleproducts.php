<?php
ini_set('memory_limit', '3024M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
Mage::app()->setCurrentStore(0);

orphaned_products();
function orphaned_products(){
		$counter = 0;
		$store_id = 6;	
		echo "<table>";
		$collection = Mage::getModel('catalog/product')->getCollection()
				->addAttributeToFilter('type_id', 'simple' )
				->addAttributeToSelect('name','type','sku','short_description','actual_model')
				->addStoreFilter($store_id)
				->addAttributeToFilter('visibility', array('eq' => 1));

		foreach($collection as $item){
				unset($product);
				unset($parentIds);
				$product = Mage::getModel('catalog/product')->load($item->getId());
				$parentIds = Mage::getModel('catalog/product_type_configurable')->getParentIdsByChild($product->getId());
			    if(!$parentIds){
			        echo "<tr><td>" . $product->getSku() . "</td><td>" . $product->getName() . "</td><td>" . $product->getTypeId() . "</td><td>" . $product->getShortDescription() . "</td><td>" . $product->getActualModel() . "</td></tr>";
					$counter++;
			    }			
		}
		echo "</table>";
		echo "<br/>Number of products: " . $counter;
	}

?>