<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../../app/bootstrap.php';
$params = $_SERVER;

$bootstrap = Bootstrap::create(BP, $params);

$objectManager = $bootstrap->getObjectManager();

$state = $objectManager->get('\Magento\Framework\App\State');
$store = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$store->setCurrentStore(0);
$state->setAreaCode('frontend');
$skus = array();
$errors = 0;
$except = array();
$productCollection = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
$products = $productCollection->addAttributeToFilter('type_id', 'configurable')
    ->addAttributeToSelect('*')->setPageSize(100);
$pages = $products->getLastPageNumber();
$stockrep = $objectManager->create('\Magento\CatalogInventory\Api\StockRegistryInterface');
//echo (string)$products->getSelect();
echo "\n<br />found products: {$products->count()}<br />\n";
echo "\n<br />found pages: {$pages}<br />\n";
$count = 0;
$currentPage = 1;
do{
	echo "\n<br />current page: {$currentPage}<br />\n";
	$products->setCurPage($currentPage);
    $products->load();
	foreach($products as $_product){
		$numbersizes= 0;
		$subprods = $_product->getTypeInstance()->getUsedProducts($_product);
		foreach($subprods as $subprod){

			$stock = $stockrep->getStockItem($subprod->getEntityId());
			//var_dump($stock->getData());exit;
			if($stock->getIsInStock()){
				$numbersizes++;
			}
			else{

			}
		}
		$_product->setData("sizes_size",$numbersizes);
    	$_product->getResource()->saveAttribute($_product, 'sizes_size');
		echo "updated ".$_product->getSku()." with ".$numbersizes." sizes.\n";
    //exit;
	}
	$currentPage++;
    //make the collection unload the data in memory so it will pick up the next page when load() is called.
	$products->clear();
}while ( $currentPage <= $pages);

echo "\n<br />Updated products: $count -  ERRORS: $errors\n";
