<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$map = 
array(
"fork" => "Fork",
"shock_absorbers"=>"Rear Shock",
"frame"=>"Frame",
"shifters"=>"Shifters",
"derailleur_front"=>"Front Derailleur",
"derailleur_back"=>"Rear Derailleur",
"brakes"=>"Brakes",
"brakelevers"=>"Brake levers",
"crankset"=>"Crankset",
"cassette"=>"Cassette",
"chain"=>"Chain",
"bb_set"=>"BB-Set",
"hub_front"=>"Hub front",
"hub_rear"=>"Hub rear",
"rims"=>"Rims",
"spokes"=>"Spokes",
"tyres"=>"Tires",
"handlebar"=>"Handlebar",
"h_stem"=>"H'stem",
"headset"=>"Headset",
"seat"=>"Seat",
"pedal"=>"Pedal",
)
;

$f = fopen(dirname(__FILE__)."/bikeattr.csv", "r");
$headers  = fgetcsv($f);

while($row = fgetcsv($f)){
	$data = array();
	foreach($row as $index=>$datav){
		
		$data[trim($headers[$index])] = $datav;
		
	}
	$products = Mage::getModel('catalog/product')
	->getCollection()
	->addAttributeToFilter(
	    array(
	        array('attribute' => 'name', 'like' => 'SCO %'.$data['bike model'].'%')
	    )
	)
	->addAttributeToFilter('type_id', "configurable")
	->load();
	//var_dump(count($products));exit;
	foreach($products as $product){
		try{
			foreach($map as $mag=>$csv){
				if(isset($data[$csv]) && $data[$csv] != ""){
					$product->setData($mag, $data[$csv]);
					
				}
			}
			var_dump("Saving ".$product->getName());
			$product->save();
		}
		
		catch(exception $e){
			
		}
	}
	//exit;
}