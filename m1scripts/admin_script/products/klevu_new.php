<?php

ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$sql = "
UPDATE klevu_product_sync 
JOIN catalog_product_entity_int as new on new.attribute_id = 417 AND new.entity_id = klevu_product_sync.product_id AND new.store_id = 0
set klevu_product_sync.last_synced_at = '0000-00-00 00:00:00'
WHERE new.value = 1;
UPDATE catalog_product_entity_int SET `value` = 0 WHERE attribute_id = 417;
INSERT INTO catalog_product_entity_int (entity_type_id, attribute_id, store_id, entity_id, `value`)
SELECT 4, 417, 0, catalog_product_flat_6.entity_id, 1 FROM `catalog_product_flat_6` WHERE 
(DATE( news_from_date) <= DATE(NOW()) AND DATE_ADD(DATE(news_from_date), INTERVAL 6 MONTH) >= DATE(NOW()))
ON DUPLICATE KEY UPDATE `value`=1; 
UPDATE klevu_product_sync 
JOIN catalog_product_entity_int as new on new.attribute_id = 417 AND new.entity_id = klevu_product_sync.product_id AND new.store_id = 0
set klevu_product_sync.last_synced_at = '0000-00-00 00:00:00'
WHERE new.value = 1;
";
$write->query($sql);
