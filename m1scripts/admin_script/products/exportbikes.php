<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$productCollection = Mage::getModel('catalog/product')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('name', array('like' => 'SCO %'))
				->addFieldToFilter('type_id', array('eq' => 'configurable'))
                ;
	//var_dump(count($productCollection));
	
	
$map = 
array(
"entity_id"=> "",
"actual_model" => "",
"name"=>"",
"short_description"=> "",
"description"=>"",
"fork" => "Fork",
"shock_absorbers"=>"Rear Shock",
"frame"=>"Frame",
"shifters"=>"Shifters",
"derailleur_front"=>"Front Derailleur",
"derailleur_back"=>"Rear Derailleur",
"brakes"=>"Brakes",
"brakelevers"=>"Brake levers",
"crankset"=>"Crankset",
"cassette"=>"Cassette",
"chain"=>"Chain",
"bb_set"=>"BB-Set",
"hub_front"=>"Hub front",
"hub_rear"=>"Hub rear",
"rims"=>"Rims",
"spokes"=>"Spokes",
"tyres"=>"Tires",
"handlebar"=>"Handlebar",
"h_stem"=>"H'stem",
"headset"=>"Headset",
"seat"=>"Seat",
"pedal"=>"Pedal",
)
;
$f = fopen("bikeexport", "w+");
$headers = array();
foreach($map as $header=>$void){
	$headers[] = $header;
}
fputcsv($f, $headers);
foreach($productCollection as $product){
	
	$line = array();
	foreach($map as $header=>$void){
		$line[] = $product->getData($header);
	}
	fputcsv($f, $line);
}
fclose($f);
