<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$f = fopen(dirname(__FILE__)."/ebikeimport.csv", "r");
$headers  = fgetcsv($f);

//$headers = array_flip($headers);

//var_dump($headers);
$productgroups = array();
fgetcsv($f);
while($row = fgetcsv($f)){
	
	$data = array();
	foreach($row as $index=>$datav){
		
		$data[trim($headers[$index])] = $datav;
		
	}
	if(!isset($productgroups[$data['PRODUCT REFERENCE']])){
		$productgroups[$data['PRODUCT REFERENCE']] = array("configurable"=>null, "chidren" => array());
	}
	if($data['SIZE VALUE'] == "SAMPLE"){
		$productgroups[$data['PRODUCT REFERENCE']]['configurable'] = $data;
	}
	else{
		$productgroups[$data['PRODUCT REFERENCE']]['chidren'][] = $data;
	}
	
	
}
fclose($f);
foreach($productgroups as $modelnumber => $products){
	$children = $products['chidren'];
	$parent = $products['configurable'];
	foreach($products['chidren'] as $index => $prod){
		$id = createProduct($prod,$modelnumber."-".$prod['ADDITIONAL BARCODE'],"simple");
		$products['chidren'][$index]["pid"] = $id;
		var_dump($id);
	}
	$cid = createProduct($parent,$modelnumber,"configurable", $products['chidren']);
	//var_dump($cid);exit;
}

function createProduct($data, $model, $type, $links = array()){
	
	$product = Mage::getModel('catalog/product');
	if(!$product->getIdBySku($data['BARCODE / EAN CODE'])){
	 $color = explode("/", $data['COLOUR']);
	 $color = (ucfirst(strtolower(trim($color[0]))));
	 var_dump($color);
	 //exit;
	try{
		$product
		//    ->setStoreId(1) //you can set data in store scope
		    ->setWebsiteIds(array(3,8)) //website ID the product is assigned to, as an array
		    ->setAttributeSetId(9) //ID of a attribute set named 'default'
		    ->setTypeId($type) //product type
		    ->setCreatedAt(strtotime('now')) //product creation time
		//    ->setUpdatedAt(strtotime('now')) //product update time
		 
		    ->setSku($data['BARCODE / EAN CODE']) //SKU
		    ->setActualModel($model) //SKU
		    ->setName($data['PRODUCT & ITEM NAME'] .(($type == "simple")? " - ".$data['SIZE VALUE']: "")) //product name
		    ->setWeight($data['ITEM WEIGHT (KG)'])
		    ->setStatus(0) //product status (1 - enabled, 2 - disabled)
		    ->setTaxClassId(2) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
		    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
		    ->setManufacturer(addAttributeValue("manufacturer", $data['BRAND NAME'])) //manufacturer id
		    ->setBrandId(22)
		    ->setColor(addAttributeValue("color", $data['COLOUR']))
			->setProductColor(addAttributeValue("product_color", $data['COLOUR']))
			->setPrimary(addAttributeValue("primary", $color))
		    //->setNewsFromDate('06/26/2014') //product set as new from
		    //->setNewsToDate('06/30/2014') //product set as new to
		    //->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)
		    ->setData("source", 3661)
		 	->setData("model_year", addAttributeValue("model_year", $data['MODEL YEAR']))
		    ->setPrice($data['RRP']) //price in form 11.22
		    ->setData("cost_price", $data['TRADE PRICE']) //price in form 11.22
		    //->setSpecialPrice(00.44) //special price in form 11.22
		    //->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
		    //->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
		    ->setMsrpEnabled(0) //enable MAP
		    ->setMsrpDisplayActualPriceType(4) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
		    ->setMsrp($data['RRP']) //Manufacturer's Suggested Retail Price
		 
		    ->setMetaTitle($data['PRODUCT & ITEM NAME'])
		    ->setMetaKeyword($data['PRODUCT & ITEM NAME'])
		    ->setMetaDescription($data['LONG DESCRIPTION'])
		 	->setData("package_id", 1553)
		    ->setDescription($data['LONG DESCRIPTION'])
		    ->setShortDescription($data['LONG DESCRIPTION'])
		 
		    //->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
		    //->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
		 
		    ->setStockData(array(
		                       'use_config_manage_stock' => 0, //'Use config settings' checkbox
		                       'manage_stock'=>1, //manage stock
		                       'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
		                       'max_sale_qty'=>2, //Maximum Qty Allowed in Shopping Cart
		                       'is_in_stock' => 1, //Stock Availability
		                       'qty' => 0 //qty
		                   )
		    )
		 	;
		    //->setCategoryIds(array(5)); //assign product to categories
		    if($type == "simple"){
		    	$product
		    	->setData("frame_size", addAttributeValue("frame_size", $data['SIZE VALUE']))
				;
		    }
			
			if($type == "configurable"){
				$product->setSku($model); //SKU
				$product->getTypeInstance()->setUsedProductAttributeIds(array(142)); //attribute ID of attribute 'color' in my store
			    $configurableAttributesData = $product->getTypeInstance()->getConfigurableAttributesAsArray();
			 
			    $product->setCanSaveConfigurableAttributes(true);
			    $product->setConfigurableAttributesData($configurableAttributesData);
			 
			    $configurableProductsData = array();
				foreach($links as $prodl){
					$configurableProductsData[$prodl['pid']] = array( //['920'] = id of a simple product associated with this configurable
				        '0' => array(
				            'label' => $data['SIZE VALUE'], //attribute label
				            'attribute_id' => '142', //attribute ID of attribute 'color' in my store
				            'value_index' => addAttributeValue("frame_size", $data['SIZE VALUE']), //value of 'Green' index of the attribute 'color'
				            'is_percent' => '0', //fixed/percent price for this option
				            'pricing_value' => '' //value for the pricing
				        )
				    );
				}
			    
			    $product->setConfigurableProductsData($configurableProductsData);
			}
			$product->save();
			return $product->getId();
		}catch(Exception $e){
		Mage::log($e->getMessage());
		echo $e->getMessage();
		return false;
		}
	}
	else{
		//var_dump($product->getData());
		return $product->getIdBySku($data['BARCODE / EAN CODE']);
	}
	
}



function addAttributeValue($attributeCode, $attValue) {
	$value = attributeValueExists($attributeCode, $attValue);
    if (!$value) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attributeCode);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['option_name'][0] = $attValue;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }
	return attributeValueExists($attributeCode, $attValue);
	
}

function attributeValueExists($attribute, $value) {
    $attribute_model = Mage::getModel('eav/entity_attribute');
    $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
    $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute);
    $attribute = $attribute_model->load($attribute_code);
    $attribute_options_model->setAttribute($attribute);
    $options = $attribute_options_model->getAllOptions(false);

    foreach ($options as $option) {
        if ($option['label'] == $value) {
            return $option['value'];
        }
    }
    return false;
}