<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$mainfolder  = "bikeimg";
$Directory = new RecursiveDirectoryIterator(dirname(__FILE__).'/'.$mainfolder.'/');
$Iterator = new RecursiveIteratorIterator($Directory);
//$Regex = new RegexIterator($Iterator, '/^.+\.xml$/i', RecursiveRegexIterator::GET_MATCH);
$files2process = array();
foreach($Iterator as $name => $object){
    
	$ext = pathinfo($name, PATHINFO_EXTENSION);
	if($ext == "jpg"){
		$files2process[] = $name;
	}
	
}

//var_dump($files2process);
$productimgs = array();

foreach($files2process as $file){
	$regex = '/([0-9]*)([A-Z-]*)\.jpg/';
	
	preg_match($regex, $file,$matches);
	
	if(isset($matches[1]) && $matches[1] != ""){
		if(!isset($productimgs[$matches[1]])) $productimgs[$matches[1]] = array();
		$productimgs[$matches[1]][] = $file;
	}
}
foreach($productimgs as $index => $data){
	 sort($productimgs[$index], SORT_NATURAL);
}
$i = 0;
foreach($productimgs as $index => $data){
	$product = Mage::getModel('catalog/product');
	$product = Mage::getModel('catalog/product')->loadByAttribute('actual_model', $index);
	//var_dump($product);
	if($product){
		$i++;
		
		$first = true;
		$imgs  = Mage::getModel('catalog/product')->load($product->getId())->getMediaGalleryImages();
		//var_dump(count($imgs));exit;
		if(count($imgs) > 0) continue;
		var_dump($product->getName() ." - ".$i);
		foreach($data as $path){
			$meta = array();
			if($first){
				$first = false;
				$meta = array('image', 'small_image', 'thumbnail');
			}
			//var_dump($path);
			$product->addImageToMediaGallery($path, $meta, false, false);
		}
		try{
			$product->save();
		}
		catch(exception $e){
			var_dump($e->getMessage());
		}
		//exit;
	}
}

