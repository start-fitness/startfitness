<?php
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$skus = array();
$errors = 0;
$except = array();
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$products = Mage::getResourceModel('catalog/product_collection')
		->addAttributeToFilter('type_id', "simple")
		->addAttributeToFilter('status', 1)
	    ->addAttributeToSelect('*');
		$products->getSelect()->limit(100);
$headers = array();
$values = array();
foreach($products as $product){
	$_product = Mage::getModel('catalog/product')->load($product->getEntityId());
	$headers = $headers+array_keys($_product->getData());
	//$values[] = $_product->getData();
}
$f = fopen("exportsimples.csv", "w+");

fputcsv($f, $headers);
foreach($products as $product){
	$_product = Mage::getModel('catalog/product')->load($product->getEntityId());
	$line = array_flip($headers);
	foreach($line as $lk=>$lv) $line[$lk] = "";
	foreach($product->getData() as $key=>$val){
		if($key == "stock_item") continue;
		if($_product->getData($key) !== null)
		{
			if( $text = $_product->getAttributeText($key)){
				//var_dump($text);
			$line[$key] = implode(",", $text);
			}
			else{
				$line[$key] = $val;
			}
		}
	}
	fputcsv($f, $line);
	
}
fclose($f);