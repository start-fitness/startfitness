<?php

ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$sql = "
INSERT INTO catalog_product_entity_int (entity_type_id, attribute_id, store_id, entity_id, value)
    SELECT 4, 343, 0, rel.child_id, source_table.value  FROM `catalog_product_entity` p 
    JOIN catalog_product_relation rel ON rel.parent_id = p.entity_id
    JOIN `catalog_product_entity_int` AS source_table ON source_table.`entity_type_id` = 4 AND source_table.`attribute_id` = 343 AND source_table.`store_id` = 0 AND source_table.entity_id = p.entity_id
    WHERE p.`type_id` = 'configurable'
ON DUPLICATE KEY UPDATE value = source_table.value
";
$write->query($sql);
