<?php
set_time_limit(200000);
ini_set('memory_limit', '1024M');
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento

umask(0);
Mage::app()->setCurrentStore(0);
$f2 = fopen("importcat3.csv", "r");
$ii=0;
$headers = fgetcsv($f2);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
while($line = fgetcsv($f2)){
	$entity_id = $line[0];
	$sportsattr = $line[4];
	$catid = $line[3];
	if(is_numeric($catid) && is_numeric($sportsattr) && is_numeric($entity_id)){
		$catsql = "INSERT IGNORE INTO `catalog_category_product` ( product_id, category_id, position) VALUES($entity_id, $catid, 1);";
		$atrsql = "INSERT INTO `catalog_product_entity_int`(`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) 
		VALUES (4,389,0,$entity_id,$sportsattr)
		ON DUPLICATE KEY UPDATE `value`=$sportsattr;";
		//var_dump($catsql);exit;
		$write->query($catsql);
		$write->query($atrsql);
	}
	else{
		var_dump($line);
	}
	//exit;
}