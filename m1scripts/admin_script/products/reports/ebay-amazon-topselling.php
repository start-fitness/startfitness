<?php
//use PHPMailer\PHPMailer\PHPMailer;
//use PHPMailer\PHPMailer\Exception;

require_once dirname(__FILE__).'/../../../app/Mage.php';
require_once dirname(__FILE__).'/../../phpmailer/class.phpmailer.php';
require_once dirname(__FILE__).'/../../phpmailer/class.pop3.php';
require_once dirname(__FILE__).'/../../phpmailer/class.smtp.php';

umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$lastmonday = new DateTime();
if($lastmonday->format("N") > 1) $lastmonday->modify("last monday");
$lastmonday->modify("last monday");
$lastsunday = new DateTime();
$lastsunday->modify("last sunday");


$lastmondayyd = clone $lastmonday;
$lastmondayyd->modify("- 1 years")->modify('next monday')  ;
$lastsundayyd = clone $lastsunday;
$lastsundayyd->modify("- 1 years")->modify('next sunday')  ;




$sqlamazon = "
SELECT count(sales_flat_order_item.sku), IF(catalog_product_entity.sku IS null,sales_flat_order_item.sku ,catalog_product_entity.sku ) as sku, IF(catalog_product_entity.entity_id is null, sales_flat_order_item.product_id , catalog_product_entity.entity_id) as product_id, name FROM `sales_flat_order_item`
JOIN sales_flat_order ON sales_flat_order.entity_id = sales_flat_order_item.order_id
LEFT JOIN catalog_product_super_link link on link.product_id = sales_flat_order_item.product_id
LEFT JOIN catalog_product_entity ON catalog_product_entity.entity_id = link.parent_id
WHERE sales_flat_order.created_at BETWEEN '".$lastmonday->format("Y-m-d 00:00:00")."' AND '".$lastsunday->format("Y-m-d 23:59:59")."' AND sales_flat_order.store_id = 24
GROUP BY IF(link.parent_id IS NULL,sales_flat_order_item.product_id , link.parent_id)
ORDER BY count(sales_flat_order_item.sku) DESC
LIMIT 20
";
$sqlamazonyd = "
SELECT count(sales_flat_order_item.sku), IF(catalog_product_entity.sku IS null,sales_flat_order_item.sku ,catalog_product_entity.sku ) as sku, IF(catalog_product_entity.entity_id is null, sales_flat_order_item.product_id , catalog_product_entity.entity_id) as product_id, name FROM `sales_flat_order_item`
JOIN sales_flat_order ON sales_flat_order.entity_id = sales_flat_order_item.order_id
LEFT JOIN catalog_product_super_link link on link.product_id = sales_flat_order_item.product_id
LEFT JOIN catalog_product_entity ON catalog_product_entity.entity_id = link.parent_id
WHERE sales_flat_order.created_at BETWEEN '".$lastmondayyd->format("Y-m-d 00:00:00")."' AND '".$lastsundayyd->format("Y-m-d 23:59:59")."' AND sales_flat_order.store_id = 24
GROUP BY IF(link.parent_id IS NULL,sales_flat_order_item.product_id , link.parent_id)
ORDER BY count(sales_flat_order_item.sku) DESC
LIMIT 20
";

$sqlebay = "
SELECT count(sales_flat_order_item.sku), IF(catalog_product_entity.sku IS null,sales_flat_order_item.sku ,catalog_product_entity.sku ) as sku, IF(catalog_product_entity.entity_id is null, sales_flat_order_item.product_id , catalog_product_entity.entity_id) as product_id, name FROM `sales_flat_order_item`
JOIN sales_flat_order ON sales_flat_order.entity_id = sales_flat_order_item.order_id
LEFT JOIN catalog_product_super_link link on link.product_id = sales_flat_order_item.product_id
LEFT JOIN catalog_product_entity ON catalog_product_entity.entity_id = link.parent_id
WHERE sales_flat_order.created_at BETWEEN '".$lastmonday->format("Y-m-d 00:00:00")."' AND '".$lastsunday->format("Y-m-d 23:59:59")."' AND sales_flat_order.store_id = 25
GROUP BY IF(link.parent_id IS NULL,sales_flat_order_item.product_id , link.parent_id)
ORDER BY count(sales_flat_order_item.sku) DESC
LIMIT 20
";
$sqlebayyd = "
SELECT count(sales_flat_order_item.sku), IF(catalog_product_entity.sku IS null,sales_flat_order_item.sku ,catalog_product_entity.sku ) as sku, IF(catalog_product_entity.entity_id is null, sales_flat_order_item.product_id , catalog_product_entity.entity_id) as product_id, name FROM `sales_flat_order_item`
JOIN sales_flat_order ON sales_flat_order.entity_id = sales_flat_order_item.order_id
LEFT JOIN catalog_product_super_link link on link.product_id = sales_flat_order_item.product_id
LEFT JOIN catalog_product_entity ON catalog_product_entity.entity_id = link.parent_id
WHERE sales_flat_order.created_at BETWEEN '".$lastmondayyd->format("Y-m-d 00:00:00")."' AND '".$lastsundayyd->format("Y-m-d 23:59:59")."' AND sales_flat_order.store_id = 25
GROUP BY IF(link.parent_id IS NULL,sales_flat_order_item.product_id , link.parent_id)
ORDER BY count(sales_flat_order_item.sku) DESC
LIMIT 20
";


//var_dump($sqlebayyd);
//var_dump($lastmondayyd->format("Y-m-d"));
//var_dump($lastsunday->format("Y-m-d"));
//var_dump($lastsundayyd->format("Y-m-d"));
//exit;

$lines = $write->fetchAll($sqlamazon);

$f = fopen(dirname(__FILE__)."/amazon-".$lastmonday->format("Y-m-d").".csv", "w+");
foreach($lines as $line){
	fputcsv($f, array_values($line));
}
fclose($f);

$lines = $write->fetchAll($sqlamazonyd);

$f = fopen(dirname(__FILE__)."/amazon-".$lastmondayyd->format("Y-m-d").".csv", "w+");
foreach($lines as $line){
	fputcsv($f, array_values($line));
}
fclose($f);

$lines = $write->fetchAll($sqlebay);

$f = fopen(dirname(__FILE__)."/ebay-".$lastmonday->format("Y-m-d").".csv", "w+");
foreach($lines as $line){
	fputcsv($f, array_values($line));
}
fclose($f);

$lines = $write->fetchAll($sqlebayyd);

$f = fopen(dirname(__FILE__)."/ebay-".$lastmondayyd->format("Y-m-d").".csv", "w+");
foreach($lines as $line){
	fputcsv($f, array_values($line));
}
fclose($f);

$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
try {
    //Server settings
    $mail->SMTPDebug = 0;                                 // Enable verbose debug output
    $mail->isSMTP();                                      // Set mailer to use SMTP
    $mail->Host = 'localhost';  // Specify main and backup SMTP servers
    $mail->SMTPAuth = false;                               // Enable SMTP authentication
    //$mail->Username = 'user@example.com';                 // SMTP username
    //$mail->Password = 'secret';                           // SMTP password
    //$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
    //$mail->Port = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('cron@startfitness.co.uk');
	//$mail->addAddress('mark@startfitness.co.uk');   
    $mail->addAddress('geoffrey@startfitness.co.uk');     // Add a recipient
    $mail->addAddress('richard@startfitness.co.uk');     // Add a recipient
                // Name is optional
    //$mail->addReplyTo('info@example.com', 'Information');
    //$mail->addCC('cc@example.com');
    //$mail->addBCC('bcc@example.com');

    //Attachments
    $mail->addAttachment(dirname(__FILE__)."/amazon-".$lastmonday->format("Y-m-d").".csv");         // Add attachments
    $mail->addAttachment(dirname(__FILE__)."/amazon-".$lastmondayyd->format("Y-m-d").".csv");         // Add attachments
    $mail->addAttachment(dirname(__FILE__)."/ebay-".$lastmonday->format("Y-m-d").".csv");         // Add attachments
    $mail->addAttachment(dirname(__FILE__)."/ebay-".$lastmondayyd->format("Y-m-d").".csv");         // Add attachments
    //$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    //Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Ebay - Amazon top selling (20)';
    $mail->Body    = 'CSV files attached';
    $mail->AltBody = 'CSV files attached';

    $mail->send();
    echo 'Message has been sent';
	file_put_contents(dirname(__FILE__)."/lastsend.txt", $thistime);
} catch (Exception $e) {
    echo 'Message could not be sent.'."\n";
    echo 'Mailer Error: ' . $mail->ErrorInfo;
}
