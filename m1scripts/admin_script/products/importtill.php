<?php
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$sql = "SELECT * FROM tmpproducts2";
$results = $write->fetchAll($sql);

var_dump(count($results));

foreach($results as $row){
	$id = createProduct($row,$row['MODEL'],"simple");
	var_dump($id);
	//exit;
}
exit;


function createProduct($data, $model, $type, $links = array()){
	
	$product = Mage::getModel('catalog/product');
	if(!$product->getIdBySku($data['SKU'])){
	 
	 //exit;
	try{
		$product
		//    ->setStoreId(1) //you can set data in store scope
		    ->setWebsiteIds(array(3,8)) //website ID the product is assigned to, as an array
		    ->setAttributeSetId(4) //ID of a attribute set named 'default'
		    ->setTypeId($type) //product type
		    ->setCreatedAt(strtotime('now')) //product creation time
		//    ->setUpdatedAt(strtotime('now')) //product update time
		 
		    ->setSku($data['SKU']) //SKU
		    ->setActualModel($model) //SKU
		    ->setName($data['NAME'] ) //product name
		    ->setWeight(0)
		    ->setStatus(2) //product status (1 - enabled, 2 - disabled)
		    ->setTaxClassId(2) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
		    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG) //catalog and search visibility
		    //->setManufacturer(addAttributeValue("manufacturer", $data['BRAND NAME'])) //manufacturer id
		    //->setBrandId(22)
		    //->setColor(addAttributeValue("color", $data['COLOUR']))
			//->setProductColor(addAttributeValue("product_color", $data['COLOUR']))
			//->setPrimary(addAttributeValue("primary", $color))
		    //->setNewsFromDate('06/26/2014') //product set as new from
		    //->setNewsToDate('06/30/2014') //product set as new to
		    //->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)
		    //->setData("source", 3661)
		 	//->setData("model_year", addAttributeValue("model_year", $data['MODEL YEAR']))
		    ->setPrice($data['PRICE']) //price in form 11.22
		    ->setData("cost_price", $data['PRICE']) //price in form 11.22
		    //->setSpecialPrice(00.44) //special price in form 11.22
		    //->setSpecialFromDate('06/1/2014') //special price from (MM-DD-YYYY)
		    //->setSpecialToDate('06/30/2014') //special price to (MM-DD-YYYY)
		    ->setMsrpEnabled(0) //enable MAP
		    ->setMsrpDisplayActualPriceType(4) //display actual price (1 - on gesture, 2 - in cart, 3 - before order confirmation, 4 - use config)
		    ->setMsrp($data['RRP']) //Manufacturer's Suggested Retail Price
		 
		    ->setMetaTitle($data['NAME'])
		    //->setMetaKeyword($data['PRODUCT & ITEM NAME'])
		    //->setMetaDescription($data['LONG DESCRIPTION'])
		 	//->setData("package_id", 1553)
		    ->setDescription($data['NAME'])
		    ->setShortDescription($data['NAME'])
		 
		    //->setMediaGallery (array('images'=>array (), 'values'=>array ())) //media gallery initialization
		    //->addImageToMediaGallery('media/catalog/product/1/0/10243-1.png', array('image','thumbnail','small_image'), false, false) //assigning image, thumb and small image to media gallery
		 
		    ->setStockData(array(
		                       'use_config_manage_stock' => 0, //'Use config settings' checkbox
		                       'manage_stock'=>1, //manage stock
		                       'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
		                       'max_sale_qty'=>2, //Maximum Qty Allowed in Shopping Cart
		                       'is_in_stock' => 0, //Stock Availability
		                       'qty' => 0 //qty
		                   )
		    )
		 	
		    ->setCategoryIds(array(3460)); //assign product to categories
		    
			//$product->save();
			//return $product->getId();
		}catch(Exception $e){
		Mage::log($e->getMessage());
		echo $e->getMessage();
		return false;
		}
	}
	else{
		//var_dump($product->getData());
		$product = Mage::getModel('catalog/product')->load($product->getIdBySku($data['SKU']));
		if($product){
			$product->setPrice($data['PRICE']);
			$product->setMsrp($data['RRP']);
			try{
				$product->save();
			}
			catch(exception $e){
				
			}
			return $product->getEntityId();
		}
		else{
			return $product->getIdBySku($data['SKU']);
		}
		
	}
	
}



function addAttributeValue($attributeCode, $attValue) {
	$value = attributeValueExists($attributeCode, $attValue);
    if (!$value) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attributeCode);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['option_name'][0] = $attValue;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }
	return attributeValueExists($attributeCode, $attValue);
	
}

function attributeValueExists($attribute, $value) {
    $attribute_model = Mage::getModel('eav/entity_attribute');
    $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
    $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute);
    $attribute = $attribute_model->load($attribute_code);
    $attribute_options_model->setAttribute($attribute);
    $options = $attribute_options_model->getAllOptions(false);

    foreach ($options as $option) {
        if ($option['label'] == $value) {
            return $option['value'];
        }
    }
    return false;
}