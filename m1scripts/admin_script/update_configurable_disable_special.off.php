<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '81920M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
						
$in_stock_counter = 0;
$out_of_stock_counter = 0;
$product_counter = 0;
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$collectionConfigurable = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToSelect('*')
                	->addAttributeToFilter('type_id', array('eq' => 'configurable'))
			->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
    ->addFieldToFilter('entity_id',array('gteq'=>275000))
    ->addFieldToFilter('entity_id',array('lteq'=>300000));
			//->addAttributeToFilter('SKU', array('eq' => 'T6E1N 9093'));
//$collectionConfigurable->getSelect()->join( array('stock'=> "cataloginventory_stock_item"), 'stock.product_id = main_table.entity_id', array('stock.is_in_stock'));
//Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection( $collectionConfigurable );
$count = 0;

//echo $collectionConfigurable->getSelect()."";exit;
foreach ($collectionConfigurable as $item) {
		$count++;
		$_product = Mage::getModel('catalog/product')->load($item->getId());
		$stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product->getId());
		//
		$childStockQty = 0;
		$children = $_product->getTypeInstance()->getUsedProducts();
                if (count($children) > 0) {
                    foreach ($children as $child) {
                        $childStock = $child->getStockItem();
                        $childStockQty += $childStock->getQty();
                    }
			if($childStockQty >0)
			{
			$stockItem->setData('is_in_stock', 1);
			//NOT SAVING IN STOCK STATUS
			echo "\r\n ".$_product->getId()." - ".$count." Set to in stock<br>";
                        $in_stock_counter++;
			}
			else
			{
			$stockItem->setData('is_in_stock', 0);
			$_product->setStatus(2);
			$stockItem->save();
			$_product->save();
			
			//LOOP THROUGH CHILDREN AGAIN AND DISABLE
			 $children = $_product->getTypeInstance()->getUsedProducts();
        	         if (count($children) > 0) {
                 	 foreach ($children as $child) {
                         	$child->setStatus(2);
                         	$child->save();
				echo "\r\n "."Child set to out of stock<br>";
                         }
			}

			echo "\r\n ".$_product->getId()." - ".$count." Set to out of stock<br>";
                        $out_of_stock_counter++;
			}
		}
		//
		
		//$storeIds = removeAdminStore($_product->getStoreIds());
		//echo "here";
		//print_r($storeIds);
		//echo "here11";
		//if(count($storeIds) > 0){
                        //echo "here2";
			//Mage::app()->setCurrentStore($storeIds[0]);
			//echo "id".$storeIds[0];
			//$stockItem =Mage::getModel('cataloginventory/stock_item')->loadByProduct($_product->getId());
			//if($_product->isSaleable()) {
			//	//$stockItem->setData('is_in_stock', 1);
			//	echo "\r\n Set to in stock<br>";
			//	$in_stock_counter++;
                        //	echo "here3";
			//}else{
			//	//$stockItem->setData('is_in_stock', 0);
			//	echo "\r\n Set to out of stock<br>";
			//	$out_of_stock_counter++;
			//}
			//$stockItem->save();
			//$product_counter++;
			//echo "\r\nprod count: " . $product_counter . ")";
                        //echo "here4";
		//}
		unset($_product);
		unset($stockItem);
	
}

function removeAdminStore($storeIds){
	$data = array();	
	foreach($storeIds as $id){
		if($id != 0){
			$data[] = $id;
		}
	}
	return $data;
}

echo "-----------------";
echo "\r\n Number of products set to in stock: " . $in_stock_counter . "<br>";
echo "\r\n Number of products set to out of stock: " . $out_of_stock_counter . "<br>";
echo "\r\n Total number of products: " . $product_counter;

	mail('richard@startfitness.co.uk', 'Cron Complete - http://www.startfitness.co.uk/script/update_configurable_stock.php', date('Y-M-D H:i:s'));
