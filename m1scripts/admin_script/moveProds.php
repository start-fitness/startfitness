<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
//echo 1717;
require_once(dirname(__FILE__).'/../app/Mage.php'); 
Mage::app()->setCurrentStore(0);
$writeConnection = Mage::getSingleton('core/resource')->getConnection('core_write');
$readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
 
/* Split these into multi array (98)Mens / (13)Ladies / (113)Junior */
$cat2subcats = array(
	/* DEV */
	// "2988" => array(98 => "2998", 13 => "2995"), // CYCLES
	//"2638" => array(98 => "2999", 13 => "2996"), // FOOTBALL
	//"2639" => array(98 => "3000", 13 => "2997") // OUTDOORS
	
	/* LIVE */
	"2995" => array( 98 => "2999", 13 => "2996", 113 => "3002"), // CYCLES
	"2638" => array( 98 => "3000", 13 => "2997", 113 => "3003"), // FOOTBALL
	"2639" => array( 98 => "3001", 13 => "2998") // OUTDOORS
);

/* Start n.b. Gender Attribute is 141 */
$genders = array();
$sql = "SELECT * FROM `eav_attribute_option_value` 
WHERE `option_id` IN 
	(SELECT option_id FROM `eav_attribute_option` WHERE `attribute_id` = 141) 
AND `store_id` = 0 
ORDER BY `option_id` DESC";
$results = $readConnection->fetchAll($sql);

/* Generate an array containing gender options */
foreach($results as $row) {
	$genders[$row['option_id']] = $row['value'];
}
/* End n.b. */

foreach($cat2subcats as $from => $to){

	/* Remove all products from this category */
	$sqlpurge = "
	DELETE FROM catalog_category_product WHERE category_id IN (".implode(',', $to).");
	DELETE FROM catalog_category_product_index WHERE category_id IN (".implode(',', $to).") and store_id = 6;
	";
	
	/* Reinsert products from the desired category ID */
	$sqlinsert = "";
	foreach($to as  $gender => $toid){
		$sqlinsert.= "
		  INSERT INTO catalog_category_product (category_id, product_id, position)
		  SELECT {$toid}, innerj.product_id, innerj.position FROM catalog_category_product_index innerj 
		  INNER JOIN catalog_product_entity_int as cpei ON innerj.product_id = cpei.entity_id
		  WHERE innerj.category_id = {$from} AND innerj.store_id = 6 AND cpei.value = {$gender} AND cpei.attribute_id = 141;
		  ";
		  $sqlinsert.= "
		  INSERT INTO catalog_category_product_index (category_id, product_id, position, is_parent, store_id, visibility)
		  SELECT {$toid}, innerj.product_id, innerj.position, innerj.is_parent, innerj.store_id, innerj.visibility FROM catalog_category_product_index innerj
		  INNER JOIN catalog_product_entity_int as cpei ON innerj.product_id = cpei.entity_id
		  WHERE innerj.category_id = {$from} AND innerj.store_id = 6 AND cpei.value = {$gender} AND cpei.attribute_id = 141;
		  ";
		  $sqlinsert.= "
		  UPDATE catalog_category_flat_store_6 AS toupdate 
		  INNER JOIN catalog_category_flat_store_6 AS jn ON jn.entity_id = {$from}
		  SET toupdate.children_count = jn.children_count, toupdate.is_anchor = jn.is_anchor
		  WHERE toupdate.entity_id = {$toid};
		  ";

   }

   $writeConnection->query($sqlpurge.$sqlinsert);
}

