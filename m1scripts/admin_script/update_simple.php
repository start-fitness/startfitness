<?php
// Memory Limits and Error Reporting	
ini_set('memory_limit','768M');
ini_set('display_errors',1);
error_reporting(E_ALL);

require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
              $fileName = 'update_simple.csv';

              header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
              header('Content-Description: File Transfer');
              header("Content-type: text/csv");
              header("Content-Disposition: attachment; filename={$fileName}");
              header("Expires: 0");
              header("Pragma: public");
              
              $fh = @fopen( 'php://output', 'w' );
              
              $headerDisplayed = true;
              
              $store_id = 5;
              // get all grouped products
              Mage::app()->setCurrentStore($store_id);
              //echo "Current Store ID: " . Mage::app()->getStore()->getId();
              //$categoryID = 189;
              $category = Mage::getModel('catalog/category')->load($categoryID);
              $collection = Mage::getModel('catalog/product')->getCollection()
                     //->addAttributeToFilter('type_id', 'grouped' )
                     ->addAttributeToFilter('type_id', 'configurable' )
                     ->addAttributeToSelect('*')
                     ->addStoreFilter($store_id);
                     //->addAttributeToFilter('sku', array('like' => '%G64653%'));
              $counter = 0;
              $simples = 0;
              $parent_counter = 0;
            //  $categories_type = array(191,192,193,194,195,441,197,198,199,200,201);
            //  $categories_collections = array(399,400,401,404,402,403,210,211,212,213,214,398,216,217,218,219,220,221);
              $products[] = array (
	            'type',
	            'name',
	            'sku',
	            'manufacturer',
	            'gender',
	            'size'
	           );

              foreach($collection as $product){

                           $childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$product);
                          
						   foreach($childProducts as $childp){
                           	
							$size = $childp->getResource()->getAttribute('uk_shoe_size')->getFrontend()->getValue($childp);
							if($size == "No") {
								$size = $childp->getResource()->getAttribute('uk_accessories_size')->getFrontend()->getValue($childp);
							} if($size == "No") {
								$size = $childp->getResource()->getAttribute('uk_clothing_size')->getFrontend()->getValue($childp);
							} 
							$manufacture = $product->getResource()->getAttribute('manufacturer')->getFrontend()->getValue($product);
							$gender = $product->getResource()->getAttribute('gender')->getFrontend()->getValue($product);
							
                                  $childdata = array();      
                                  $childdata[] = array (
                                                $childp->getTypeID(),
                                                $product->getName()." - ". $size,
                                                $childp->getSku(),
  												($manufacture!="No"?$manufacture:""), 	
  												($gender!="No"?$gender:""),	
  												$size			
                                                );                                       
                           $products[] = $childdata[0];
                           }
                           $simples++;
              }

              //print_r($products); die;
              
              foreach ($products as $data) {
                  // Add a header row if it hasn't been added yet
                  if (!$headerDisplayed) {
                      // Use the keys from $data as the titles
                      fputcsv($fh, array_keys($data));
                      $headerDisplayed = true;
                  }
              
                  // Put the data into the stream
                  fputcsv($fh, $data);
              }
              // Close the file
              fclose($fh);
              // Make sure nothing else is sent, our file is done
              exit;
     

