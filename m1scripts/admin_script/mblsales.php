<?php

ini_set('display_startup_errors',1);

ini_set('display_errors',1);

error_reporting(-1);

ini_set('memory_limit', '8096M');

require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento

umask(0);

Mage::app()->setCurrentStore(0);

$observer = Mage::getSingleton('mblconnector/cron');
//var_dump($observer);
$observer->_isCLI = true;

$begin = new DateTime( '2017-08-11' );
$end = new DateTime( '2017-08-14' );

$begin = new DateTime();
$begin->sub(new DateInterval("P1D"));



$from = $begin->format( "Y-m-d" )."T00:00:00Z";
$to = $begin->format( "Y-m-d" )."T23:59:59Z";
echo "PROCESSING ".$begin->format( "Y-m-d" )."\n";
//exit;
$observer->sync("sales", array("from"=>$from, "to"=>$to));

/*$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);

foreach ( $period as $dt ){
	$from = $dt->format( "Y-m-d" )."T00:00:00Z";
	$to = $dt->format( "Y-m-d" )."T23:59:59Z";
	echo "PROCESSING ".$dt->format( "Y-m-d" )."\n";
	$observer->sync("sales", array("from"=>$from, "to"=>$to));
	//exit;
}
*/


