<?php
// More Mile Homepage Builder - To be run via CRON
// Designed and built by James Simpson
// V 1.0.0 BETA

error_reporting(E_ALL);

// First connect up to Mage
require(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento (CRON)
//require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');  //Path to Magento (CRON)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

	$sql_1 = "INSERT IGNORE INTO catalog_product_entity_int (entity_type_id, attribute_id, store_id, entity_id, value)
SELECT 4, 388, 0, product_id, 1 FROM `catalog_product_website` where website_id = 9 and product_id NOT IN
 (SELECT product_id FROM `catalog_product_website` where website_id = 3)";

	 $sql_2 = "INSERT IGNORE INTO catalog_product_entity_int (entity_type_id, attribute_id, store_id, entity_id, value)
SELECT 4, 388, 0, product_id, 0 FROM `catalog_product_website` where website_id = 9 and product_id IN
 (SELECT product_id FROM `catalog_product_website` where website_id = 3)";

	$sql_3 = "UPDATE catalog_product_entity_int SET value = 1 where entity_type_id = 4 AND attribute_id = 388 AND store_id = 0 AND entity_id IN
(SELECT product_id FROM `catalog_product_website` where website_id = 9 and product_id NOT IN
 (SELECT product_id FROM `catalog_product_website` where website_id = 3))";

	$sql_4 = "UPDATE catalog_product_entity_int SET value = 0 where entity_type_id = 4 AND attribute_id = 388 AND store_id = 0 AND entity_id IN
(SELECT product_id FROM `catalog_product_website` where website_id = 9 and product_id IN
 (SELECT product_id FROM `catalog_product_website` where website_id = 3))";

	$write->query($sql_1);
	$write->query($sql_2);
	$write->query($sql_3);
	$write->query($sql_4);


$html = "Successfully updated SS FIT / SS N FIT Attribute";

echo $html;

//Receiver Details
$sendToEmailAddress = array('richard@startfitness.co.uk');

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), Mage::getStoreConfig('trans_email/ident_general/name'));
			$mail->addTo($sendToEmailAddress);
			$mail->setSubject('SS / SS N FIT Attribute Script Ran  '.date("F j, Y, g:i a"));
			$mail->send();

?>
