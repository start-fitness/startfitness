<?php
	ini_set('display_startup_errors',1);
	ini_set('display_errors',1);
	error_reporting(-1);
	ini_set('memory_limit', '10000M');
	require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
	umask(0);
	Mage::app();
	$store_id = 0;
	Mage::app()->setCurrentStore($store_id);

	updateReportTable();
	 function updateReportTable(){
		$past_months = 1;
		if(isset($_GET['past']) && $_GET['past'] != ''){
			$past = $_GET['past'];
		}
		
		for($i=$past_months; $i>=0; $i--){
			$past = $i;	
			//$past = 15; // testing
			/* set start date */
			$set_start_date = date('Y-m-01 00:00:00',strtotime('this month'));
			$set_start_date = new DateTime($set_start_date);
			$set_start_date->modify('-' . $past . ' month');
			//echo $period_start_date = $set_start_date->format('Y-m-d H-i-s');
			/* set end date */
			$set_end_date = date('Y-m-d 23:59:59', strtotime('this month'));
			$set_end_date = new DateTime($set_end_date);
			$set_end_date->modify('-' . $past . ' month');
			$period_end_date = new DateTime(strtotime($set_end_date->format('Y-m-d H-i-s')));
			$period_end_date->format('Y-m-t H-i-s');
			
			$interval = DateInterval::createFromDateString('1 day');
			$period = new DatePeriod($set_start_date, $interval, $period_end_date);
			
			foreach ( $period as $dt ){
			    //echo "\r\n" . $dt->format( "Y-m-d H:i:s\n" );
				if(strtotime($dt->format('Y-m-d H:i:s')) <=  strtotime('now')){
					echo "\r\n" . $dt->format('Y-m-d');
					$start = $dt->format('Y-m-d 08:00:00');
					$date = new DateTime($start);
					$date->modify('+1 day');
					$end = $date->format('Y-m-d 07:59:59');
					
					echo "From: " . $start . " to: " . $end;
					echo "\r\n";
					
					
					deleteReportData($dt->format('Y-m-d'));
					$allStores = Mage::app()->getStores();
					foreach ($allStores as $_eachStoreId => $val){
						//echo "\r\n Processing store: " . $_eachStoreId;
						$data = array();
						$data['start'] = $start;
						$data['end'] = $end;
						$data['store_id'] = $_eachStoreId;
						
						
						$product_skus = array();
						$data['orders'] = get_orders($data);
						if(!empty($data['orders'])){
							$product_skus = getSoldProductsBySku($data);
							//print_r($product_skus); die;
							foreach($product_skus as $key=>$value){
								$result_data['total_price'] = $value['total_price'];
								$result_data['store_id'] = $_eachStoreId;
								$result_data['product_id'] = $value['product_id'];
								$result_data['qty_ordered'] = $value['qty_ordered'];
								//$result_data['attribute_set'] = $value['attribute_set'];
								//$result_data['gender'] = $value['gender'];
								//$result_data['manufacturer'] = $value['manufacturer'];
								$result_data['date'] = $start;
								//$report_data['total_revenue'] = $value['total_price'] * $value['qty_ordered'];
								insertReportData($result_data);
							}
						}
					}					
					
				}
				else{
					break;
				}
				
				if(isset($_GET['stop']) && $_GET['stop'] == 'true'){
					echo "\r\n STOPPED!";	
					die;
				}
			}
					
			//$report_data['date'] = $set_end_date->format('Y-m-01');			
			//echo " FROM " . $period_start_date . " TO " . $period_end_date;
			
			echo "\r\n--- FINISH MONTH \r\n" . $past . "---";
		}
	}

	function insertReportData($binds){
		$resource = Mage::getSingleton('core/resource');
		$write = $resource->getConnection("core_write");
		$query = "INSERT INTO dimasoft_product_sales (
														 product_id,
														 qty_ordered,
														 total_price,
														 store_id,
														 date
														 ) values (
														 :product_id,
														 :qty_ordered,
														 :total_price,
														 :store_id,
														 :date
														 )";
		try {
	        $write->query($query, $binds);
	        //echo "Data successfully inserted. Insert ID: ".$insertId;
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
	}
	
	
	function deleteReportData($date){
		$resource = Mage::getSingleton('core/resource');
		$write = Mage::getSingleton("core/resource")->getConnection("core_write");
		$query = "DELETE FROM dimasoft_product_sales 
				  WHERE date = '" . $date . "'"; 
		$write->query($query);
		try {
	        $write->query($query);
	        //echo "Data deleted successfully";
		    } catch (Exception $e){
		     echo $e->getMessage();  
		}
	}
	
	
	function get_orders($data){
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		$sql = "SELECT entity_id FROM sales_flat_order 
				WHERE status IN('complete','processing','pick_note_printed','pending','packed')";
				if(!isset($data['store_id']) || $data['store_id'] == ''){
											$sql .= " AND store_id IS NOT NULL";
						}
						else{
											$sql .= " AND store_id = " . $data['store_id'];
						} 
		$sql .=	" AND created_at BETWEEN '" . $data['start'] . "' AND '" . $data['end'] . "'";
				  //AND grand_total > 0";
		return $result = $readConnection->fetchAll($sql);
	}


	function getSoldProductsBySku($data){
		 $products_sold = array();
		 //$data['orders'] = array('10636');
		 foreach($data['orders'] as $value){
		 	$order = Mage::getModel('sales/order')->load($value);
            $items = $order->getAllItems();
            foreach ($items as $itemId => $item)
            {
            	unset($product);
            	$qty_ordered = 0;
				$price = 0;
				if($product = $item->getProduct()){
					if($product->getTypeId() != 'configurable'){
						if($product->getTypeId() == 'simple'){
							if(($parent = checkForParent($items, $item->getSku()))){
		            			$qty_ordered = $parent->getQtyOrdered();
								$price = $parent->getData('row_total_incl_tax') - $parent->getDiscountAmount();
		            		}
							else{
								$qty_ordered = $item->getQtyOrdered();
								$price = $item->getData('row_total_incl_tax') - $item->getDiscountAmount();
							}
						}else{
							$qty_ordered = $item->getQtyOrdered();
							$price = $item->getData('row_total_incl_tax') - $item->getDiscountAmount();
						}
						
		            	if(!array_key_exists($item->getSku(), $products_sold)){
		            		$products_sold[$item->getSku()]['qty_ordered'] = $qty_ordered;
		            		$products_sold[$item->getSku()]['total_price'] = $price;
		            	}
						else{
							$products_sold[$item->getSku()]['qty_ordered'] += $qty_ordered;
		            		$products_sold[$item->getSku()]['total_price'] += $price;
						}	
					}
					$products_sold[$item->getSku()]['product_id'] = $item->getProductId();
					//$products_sold[$item->getSku()]['gender'] = $product->getAttributeText('gender');
					//$products_sold[$item->getSku()]['manufacturer'] = $product->getAttributeText('manufacturer');
					//$products_sold[$item->getSku()]['attribute_set'] = Mage::getModel('eav/entity_attribute_set')->load($product->getAttributeSetId())->getAttributeSetName();
				}
            } 
		 }

			//print_r($products_sold); die;
	
			return $products_sold;
	}
	
	function checkForParent($items, $sku){
		foreach ($items as $itemId => $item){
			if($item->getSku() == $sku && $item->getProduct()->getTypeId() != 'simple'){
				return $item;
			}                	
		}
		return false;
	}
	
	