<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../phpmailer/class.phpmailer.php');
umask(0);
$store_id = 0;
Mage::app()->setCurrentStore($store_id);


$resource = Mage::getSingleton('core/resource');
$writeConnection = $resource->getConnection('core_write');
$uploaded = false;


// DO SIMPLES

$sql ="
SELECT restocklinks.product_id, restockrates.*, minr.data as minimums, restock_tillstock.qty as tillqty, stock.qty as mageqty, eav_attribute.attribute_code 
FROM `restocklinks`
JOIN restockrates ON restockrates.id  = restocklinks.restockrates_id AND restockrates.attribute_ids = 0
JOIN restockrates minr ON  minr.tier  = CONCAT(\"R_\", restockrates.tier) AND restockrates.attribute_set_id = minr.attribute_set_id AND restockrates.attribute_ids = minr.attribute_ids AND restockrates.brand_id = minr.brand_id AND restockrates.gender_id = minr.gender_id AND minr.attribute_ids = 0
LEFT JOIN restock_tillstock ON restock_tillstock.magento_id =  restocklinks.product_id
LEFT JOIN `cataloginventory_stock_item` stock ON stock.product_id = restocklinks.product_id
LEFT JOIN eav_attribute ON eav_attribute.attribute_id = restockrates.attribute_ids
WHERE stock.qty > 0";

$results = $writeConnection->fetchAll($sql);

$totalqty = 0;
$productstoorder = array();
$notenough = array();
foreach($results as $row){
	$data = json_decode($row['data'], true);
	$datamin = json_decode($row['minimums'], true);
	$product = Mage::getModel('catalog/product')->load($row['product_id']);
	if($product){
		//$attributeval = $product->getData($row['attribute_code']);
		if(true){
			$tillstock = ($data['unordered'][0]);
			$minstock = ($datamin['unordered'][0]);
			
			if($row['tillqty'] < $tillstock['val'] ){
				$tillstock['val'] = $tillstock['val']  - $row['tillqty'];
				if($minstock['val'] < $row['mageqty']){
					
					$availableqty = $row['mageqty'] - $minstock['val'];
					if($tillstock['val'] <= $availableqty){
						$productstoorder[$product->getId()] = (float)$tillstock['val'];
						$totalqty+= (float)$tillstock['val'];
					}
					else{
						$productstoorder[$product->getId()] = (float)$availableqty;
						$totalqty+= (float)$availableqty;
						$notenough[] = array("sku"=>$product->getSku(), 
						"product_id"=>$product->getId(), 
						"model"=>$product->getActualModel(), 
						"missing_qty"=> $tillstock['val'] - $availableqty);
					}
				}
			}
		}
	}
	
	
	
}
$chunks = array_chunk($productstoorder, 30, true);
if(count($notenough) > 0){
	emailmissing($notenough);
}
//var_dump(($productstoorder), $totalqty);exit;

//exit;
foreach($chunks as $chunk){
	createOrder($chunk);
}


function emailmissing($missing){
	
$list = "";
foreach($missing as $prod){
	$list .='<li>SKU: '.$prod['sku'].' - Model N#: '.$prod['model'].' - missing qty: '.$prod['missing_qty'].'</li>';
}
	
$mail = new PHPMailer;
$email ="geoffrey@startfitness.co.uk";
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'localhost';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startcycles.co.uk';
$mail->FromName = 'Stock Manager';
//$mail->addAddress($email);  // Add a recipient
//$mail->addAddress("geoffrey@startfitness.co.uk");  // Add a recipient
//$mail->addAddress("speedking34@gmail.com");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Start Fitness till Restock - Unallocateable stock';
$mail->Body    = 

'
<h1>Start Fitness till Restock - Unallocateable stock</h1>
<p>
The following products could not be allocated fully, due to missing stock/reserved stock in magento.
</p>
<ul>
'.$list.'
</ul>
';


if(!$mail->send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';
	
}

function createOrder($products){
	$websiteId = 8;
	$store = Mage::app()->getStore();
	// Start New Sales Order Quote
	$quote = Mage::getModel('sales/quote')->setStoreId(33);
	
	// Set Sales Order Quote Currency
	//$quote->setCurrency($order->AdjustmentAmount->currencyID);
	$customer = Mage::getModel('customer/customer')->load(590887);
	        //->setWebsiteId($websiteId)
	        //->loadByEmail("startfitnessshop@startfitness.co.uk");
	
	
	
	// Assign Customer To Sales Order Quote
	$quote->assignCustomer($customer);
	
	// Configure Notification
	$quote->setSendCconfirmation(1);
	foreach ($products as $id => $qty)
	{
	    $product = Mage::getModel('catalog/product')->load($id);
	    $quote->addProduct($product, new Varien_Object(array('qty' => $qty)))
		->setOriginalCustomPrice(0)
	    ->setCustomPrice(0)
	    ->setIsSuperMode(true);
	}
	
	// Set Sales Order Billing Address
	$billingAddress = $quote->getBillingAddress()->addData(array(
	    'customer_address_id' => '',
	    'prefix' => '',
	    'firstname' => 'Start Fitness',
	    'middlename' => '',
	    'lastname' => '(Newcastle)',
	    'suffix' => '',
	    'company' => '',
	    'street' => array(
	        '0' => '108-110 Grainger Street',
	        '1' => ''
	    ),
	    'city' => 'Newcastle Upon Tyne',
	    'country_id' => 'GB',
	    'region' => '',
	    'postcode' => 'NE1 5JQ',
	    'telephone' => '01912610423 ',
	    'fax' => '',
	    'vat_id' => '',
	    'save_in_address_book' => 0
	        ));
	
	// Set Sales Order Shipping Address
	$shippingAddress = $quote->getShippingAddress()->addData(array(
	    'customer_address_id' => '',
	    'prefix' => '',
	    'firstname' => 'Start Fitness',
	    'middlename' => '',
	    'lastname' => '(Newcastle)',
	    'suffix' => '',
	    'company' => '',
	    'street' => array(
	        '0' => '108-110 Grainger Street',
	        '1' => ''
	    ),
	    'city' => 'Newcastle Upon Tyne',
	    'country_id' => 'GB',
	    'region' => '',
	    'postcode' => 'NE1 5JQ',
	    'telephone' => '01912610423 ',
	    'fax' => '',
	    'vat_id' => '',
	    'save_in_address_book' => 0
	        ));
	
	
	$shipmethod = 'productmatrix_Delivered_To_Store';
	$paymentmethod ="purchaseorder";
	
	
	
	$shippingAddress->setCollectShippingRates(true)
		    ->collectShippingRates()
		    ->setShippingMethod($shipmethod) 
		    ->setPaymentMethod($paymentmethod);
	$shippingPrices = array();
	foreach($shippingAddress->getAllShippingRates() as $rate){
	    $shippingPrices[$rate->getCode()] = $rate->getPrice();
	}
	//var_dump($shippingPrices);exit;
	// Collect Rates and Set Shipping & Payment Method
	/*$shippingAddress->setCollectShippingRates(true)
	        ->collectShippingRates()
	        ->setShippingMethod('freeshipping_freeshipping')
	        ->setPaymentMethod('')
			;*/
	// Set Sales Order Payment
	$quote->getPayment()->importData(array('method' => $paymentmethod, "po_number"=>"reorderauto"));
	$quote->setShippingMethod($shipmethod)->save();
	// Collect Totals & Save Quote
	$quote->setCouponCode("BACKEND100OFF")->collectTotals()->save();
	$quote->collectTotals()->save();
	try {
		
		
	    // Create Order From Quote
	    $service = Mage::getModel('sales/service_quote', $quote);
	    $service->submitAll();
		//exit;
	    $increment_id = $service->getOrder()->getRealOrderId();
	}
	catch (Exception $ex) {
	    var_dump($ex->getMessage());
	}
	catch (Mage_Core_Exception $e) {
	    var_dump( $e->getMessage());
	}
	
	
	
	// Resource Clean-Up
	$quote = $customer = $service = null;
}
//var_dump($productstoorder);
