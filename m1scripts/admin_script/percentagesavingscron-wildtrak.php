<?php

ini_set('memory_limit', '10048M');

require_once('../app/Mage.php'); //Path to Magento

umask(0);

Mage::app();

$store_id = 0;

$skip_products = array();

//$skip_products = array();

// get all grouped products

Mage::app()->setCurrentStore($store_id);

$collection = Mage::getModel('catalog/product')->getCollection()

	->addAttributeToSelect('final_price')

	->addAttributeToSelect('special_price')

	->addAttributeToSelect('price')

	->addAttributeToSelect('msrp')

	->addAttributeToSelect('entity_id')

	->addAttributeToSelect('visibility')

	->addAttributeToSelect('percentage_saving')

	->addAttributeToFilter('visibility', array('neq' => 1))

	->addAttributeToFilter('status', 1)

	//->addAttributeToFilter('entity_id', 9675)

	->addStoreFilter(7);

	//->addAttributeToFilter('entity_id', array('in' => $skip_products));

$counter = 0;

$cycle = 0;

// get all visible products

$products_error = array();

foreach($collection as $item){

	$cycle++;

	echo "\r\n Final Price: " . $item->getFinalPrice();

	echo "\r\n Price: " . $item->getPrice();

	echo "\r\n Special Price: " . $item->getSpecialPrice();

	echo "\r\n MRSP: " . $item->getMsrp();

	$savings_percentage = round((($item->getMsrp() - $item->getFinalPrice())/$item->getMsrp())*100, 0 );

	//echo "\r\n" . round($item->getPercentageSaving(),0);	

	if(round($item->getPercentageSaving(), 0) != $savings_percentage && !in_array($item->getId(), $skip_products)){

		unset($product);

		$product = Mage::getModel('catalog/product')->load($item->getId());

		if($product){

			if($savings_percentage < 0){

				$savings_percentage = 0.00;

			}

			$product->setPercentageSaving($savings_percentage);

			try{

				$product->save();

				$counter++;

				echo "\r\n" . $cycle . ") Product Updated: " . $product->getId();	

			}

			catch (Exception $e) {

				$products_error[] = $product->getId();

				echo "\r\n" . $cycle . ") Product Update Error: " . $product->getId();

			}

		}

	}

	else{

		echo "\r\n" . $cycle . ") Product Skipped: " . $item->getId(); 

	}

}

if(!empty($products_error)){

echo "The following products were not updated: " . implode(',', $products_error);

}

echo "\r\nNumber products updated: " . $counter;



$resource = Mage::getSingleton('core/resource');

$writeConnection = $resource->getConnection('core_write');

$query = "DELETE FROM `catalog_product_entity_decimal` 

		  WHERE `attribute_id` = 338

		  AND entity_type_id = 4

		  AND store_id != 0";

$writeConnection->query($query);



?>