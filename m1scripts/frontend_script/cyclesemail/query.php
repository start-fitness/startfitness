<?php
require dirname(__FILE__).'/../phpmailer/class.phpmailer.php';
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);
header('Content-Type: application/json');

ini_set('memory_limit','9768M');
ini_set('display_errors',1);
error_reporting(E_ALL);

$data = Mage::app()->getRequest()->getParams();

if($data['email'] == "netsparker@example.com"){
	echo json_encode(array("status"=>"0"));
    exit;
}

$mail = new PHPMailer;
$email ="bradlyillingworth@startfitness.co.uk"; 
$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'cluster5out.eu.messagelabs.com';  // Specify main and backup server
$mail->SMTPAuth = false;                               // Enable SMTP authentication
$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
$mail->Password = 'QvEF4Clp';                           // SMTP password
//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'customerservices@startfitness.co.uk';
$mail->FromName = 'Website Enquiry';
$mail->addAddress($email);  // Add a recipient
$mail->AddReplyTo($data['email']);
$mail->addAddress("braddobbing@startfitness.co.uk");  // Add a recipient
$mail->addAddress("danielmeggison@startfitness.co.uk");  // Add a recipient

//$mail->addBCC("speedking34@gmail.com");  // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML
$data['comment'] = nl2br($data['comment']);
$mail->Subject = 'Start Fitness - Customer Bike Enquiry';
$mail->Body    = 
"
<b>Customer Name: </b> {$data['name']}<br />
<b>Customer Email: </b> {$data['email']}<br />
<b>Customer Telephone: </b> {$data['telephone']}<br />
<b>Bike Reference: </b> {$data['bikeref']}<br />
<b>Customer Enquiry:</b><br />
<p>
{$data['comment']}
</p>
"
;

if(!$mail->send()) {
   echo json_encode(array("status"=>"0"));
   exit;
}

echo json_encode(array("status"=>"1"));
