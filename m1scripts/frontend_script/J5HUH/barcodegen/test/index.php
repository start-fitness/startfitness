<?php

/*

*/

  error_reporting (E_ALL);  // remove this from Production Environment
  require('pdfb/pdfb.php'); // Must include this

  // Recommended way to use PDFB Library
  // - create your own PDF class
  // - instantiate it wherever necessary
  // - you can create multiple classes extending from PDFB
  //   for each different report

  class PDF extends PDFB
  {
    function Header()
    {
      // Add your code here to generate Headers on every page
    }

    function Footer()
    {
      // Replace this with your code to generate Footers on every page

      // PDFB Library made this dynamic PDF :)
      // Remember to use '$this->' instead of '$pdf->'
      //$this->Text(402, 735, "Dynamic PDF: PDFB Library!");
    }

    // You can create your own methods to make printing easier
    // Here is a method to print information about a product on the packing slip
    function printProduct($p)
    {
      // Product Name
      $this->Write(0, $p["product"]);

      // Size
      $this->SetY(15);
      $this->Write(0, "Size:" . $p["size"]);

      // UPC-A Barcode
	$this->SetY(35);
      $this->BarCode($p["c39"], "C39", 10, 40, 198, 80, 0.6, 0.6, 2, 5, "", "PNG");

      // Draw a separator line
      //$lineY = $this->GetY() + 50;
      //$this->Line(80, $lineY, 100, $lineY);
	
	$this->SetY(115);
      // Move cursor down
      //$this->SetXY(80, $lineY + 24);
    }

  }

  // Create a PDF object and set up the properties
  $pdf = new PDF("L", "mm", array(100,150));
  $pdf->SetAuthor("PDFB Library");
  $pdf->SetTitle("Packing Slip");

  // Add custom font
  $pdf->SetFont("Arial", "", 10);

 // Load the base PDF into template
  $pdf->setSourceFile("demo.pdf");

  // Add new page & use the base PDF as template
  $pdf->AddPage();

  // Load product information
  $products[] = array("product" => "Whisker City - Catnip Mist", "size" => 12, "c39" => "7372573588290");
  $products[] = array("product" => "Pounce - Tuna Flavor Moist", "size" => 18, "c39" => "0791000016060");

  $pdf->SetXY(0, 10);

  // Print product information
  if($products)
    foreach($products as $product){
      $pdf->printProduct($product);
	}


  $pdf->Output();
  $pdf->closeParsers();

?>