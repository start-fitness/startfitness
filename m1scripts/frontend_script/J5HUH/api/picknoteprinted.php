<?php
// Script to count how many processing & picknote printed orders currently on the system
// This is for Dan Robin's packers stats board in the packing office
// Updated script by James Simpson to streamline

error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit','768M');
ini_set('display_errors', 1);

// Get the mage connection 
require('/home/sfadmin/public_html/app/Mage.php');
Mage::app();

// Zero all required figures
$picknoteprinted = 0;
$processing = 0;

// Setup database connection
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');

// SQL Query
$sql_picknote = "SELECT count(*) FROM `sales_flat_order` WHERE status = 'pick_note_printed'";
$sql_process = "SELECT count(*) FROM `sales_flat_order` WHERE status = 'processing'";

// Execute the query
$picknoteprinted = $readConnection->fetchOne($sql_picknote);
$processing = $readConnection->fetchOne($sql_process);

// Setup the reqired fields
$data_pick .= '<root><item><value>';
$data_pick .= $picknoteprinted;
$data_pick .= '</value><text>Pick note printed</text></item></root>';

$data_proc .= '<root><item><value>';
$data_proc .= $processing;
$data_proc .= '</value><text>Processing</text></item></root>';

// Finally write everything to xml files
$file = fopen("cram_pnp.xml","w");
fwrite($file,$data_pick);
fclose($file);

$file = fopen("cram_pro.xml","w");
fwrite($file,$data_proc);
fclose($file);

?>