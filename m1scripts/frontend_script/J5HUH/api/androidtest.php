<?php
$method = $_GET["method"];
$sku = $_GET["sku"];
$stocklevel = $_GET["stocklevel"];
error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit', '10048M');
ini_set('display_errors', 1);
$mageFilename = $_SERVER['DOCUMENT_ROOT'].'/app/Mage.php';

if (!file_exists($mageFilename)) {
    echo $mageFilename." was not found";
    exit;
}
else {
}
require_once $mageFilename;
Mage::app();

if ($method == "getstocklevel") {
	if ($sku) {
		$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));




		if ($productid) {
			$product = Mage::getModel('catalog/product');
			$product->load($productid);

			$attributeSetModel = Mage::getModel("eav/entity_attribute_set");
			$attributeSetModel->load($product->getAttributeSetId());
			$attributeSetName  = $attributeSetModel->getAttributeSetName();
			$size = "Not found";

			if($attributeSetName === "Bras")
			{
				$size = $product->getAttributeText('uk_chest_size') . $product->getAttributeText('uk_cup_size');
			}
			else if($attributeSetName === "Socks")
			{
				$size = $product->getAttributeText('uk_sock_size');
			}			
			else if($attributeSetName === "Footwear")
			{
				$size = $product->getAttributeText('uk_shoe_size');
			}
			else if($attributeSetName === "Clothing")
			{
				$size = $product->getAttributeText('uk_clothing_size');
			}
			else if($attributeSetName === "Nutrition")
			{
				$size = $product->getAttributeText('uk_nutrition_flavour');
			}

			echo round($product->getStockItem()->getQty()) . ',' . $product->getStockItem()->getProductName() . ',' . $product->getData('actual_model'). ',' . $product->getData('bin_location') . ',' . $attributeSetName . ',' . $size . ',' . $product->getId();

			/*echo round($product->getStockItem()->getQty()) . ',' . $product->getStockItem()->getProductName() . ',' . $product->getData('actual_model'). ',' . $product->getData('bin_location');*/
		}
		else {
			echo 'no product with sku '.$sku.' exists!';
		}
	}
	else {
		echo 'URL must contain SKU number';
	}
}
elseif ($method == "addstocklevel"){
       if ($sku) {
                $productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
                if ($productid) {
                        $product = Mage::getModel('catalog/product');
                        $product->load($productid);
                        $currentqty = $product->getStockItem()->getQty();
			$newQty = $currentqty + $stocklevel;	
			$stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);
			$stock_item->setData('qty', $newQty);
			try {
  				$stock_item->save();
			}
			catch (Exception $e) {
  				echo $e->getMessage();
			}
			//echo 'Added qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
			echo 'Added:'.round($stocklevel).','.round($currentqty).','.$sku;
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}
elseif ($method == "removestocklevel"){
       if ($sku) {
                $productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
                if ($productid) {
                        $product = Mage::getModel('catalog/product');
                        $product->load($productid);
                        $currentqty = $product->getStockItem()->getQty();
			$newQty = $currentqty - $stocklevel;	
			$stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);
			$stock_item->setData('qty', $newQty);
			try {
  				$stock_item->save();
			}
			catch (Exception $e) {
  				echo $e->getMessage();
			}
			echo 'Removed qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}
elseif ($method == "setstocklevel") {
 	if ($sku) {
       		$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
        	if ($productid) {
			$newQty = $stocklevel;
			$stock_item = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productid);
                        $stock_item->setData('qty', $newQty);
                        try {
                                $stock_item->save();
                        }       
                        catch (Exception $e) {
                                echo $e->getMessage();
                        }       
			echo 'Inserted qty of '.round($stocklevel).' to product qty of for sku number '.$sku;
                }
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}

