<?php
use Magento\Framework\App\Bootstrap;
 
require __DIR__ . '../../../../../app/bootstrap.php';

$method = $_GET["method"];
$sku = $_GET["sku"];
$stocklevel = isset($_GET["stocklevel"]) ? $_GET["stocklevel"] : "";
error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit', '10048M');
ini_set('display_errors', 1);

 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$imagesize = 350; // G.Hall - Added to contol image size for image function call

// $sku = preg_replace("/[^0-9,.]/", "", $sku);

/* IP check
*
* 82.39.63.63 now 92.238.187.185 - Dan
* 173.55.9.183 - Dan
* 2.223.131.71 - Dan
* 86.148.138.84 - Dan
* 217.39.187.57 - Tri-Shop (Upstairs Office)
* 81.149.242.103 - Cycles (Shop Floor / Basement)
* 212.140.204.146 - Cramlington
* 212.140.204.148 - Cramlington - G.Hall
*/
if( ($_SERVER['REMOTE_ADDR'] != "81.105.192.30") &&
        ($_SERVER['REMOTE_ADDR'] != "94.1.147.11") &&
        ($_SERVER['REMOTE_ADDR'] != "188.222.77.40") &&
	($_SERVER['REMOTE_ADDR'] != "93.0.244.211") &&
	($_SERVER['REMOTE_ADDR'] != "82.35.168.223") &&
	($_SERVER['REMOTE_ADDR'] != "173.55.9.183") &&
	($_SERVER['REMOTE_ADDR'] != "2.223.131.71") &&
	($_SERVER['REMOTE_ADDR'] != "217.39.187.57") &&
	($_SERVER['REMOTE_ADDR'] != "92.40.248.217") &&
	($_SERVER['REMOTE_ADDR'] != "5.62.0.13") &&
	($_SERVER['REMOTE_ADDR'] != "81.149.242.103") &&
	($_SERVER['REMOTE_ADDR'] != "82.35.171.12") &&
	($_SERVER['REMOTE_ADDR'] != "82.246.238.242") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.146") &&
	($_SERVER['REMOTE_ADDR'] != "82.246.238.242") &&
	($_SERVER['REMOTE_ADDR'] != "86.148.138.84") &&
        ($_SERVER['REMOTE_ADDR'] != "213.123.128.29") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.148")&&
	($_SERVER['REMOTE_ADDR'] != "82.65.142.250")&&
	($_SERVER['REMOTE_ADDR'] != "92.40.201.183")&&
        ($_SERVER['REMOTE_ADDR'] != "77.95.33.38")&&
	($_SERVER['REMOTE_ADDR'] != "188.222.77.40"))
{
	die("Invalid IP request from ".$_SERVER['REMOTE_ADDR'] );
}

if (false) {
    echo $mageFilename." was not found";
    exit;
}
else {
}


$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$readConnection = $resource->getConnection();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$productfactory = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');

if ($method == "searchmodelno") {
	if ($sku) {
		// Search all stores
		//Mage::app()->setCurrentStore(0);
		$storeManager->setCurrentStore(0);
		//$magcollection = Mage::getResourceModel('catalog/product_collection')
		$magcollection = $productfactory->create()
			->addAttributeToFilter('type_id', 'simple')
			->addAttributeToSelect('sku')
			->addAttributeToFilter('actual_model',array('like' => '%' . $sku . '%'));
			
			//var_dump($magcollection->getSelect()."");
		
		foreach($magcollection as $magprod) {
			
			$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
			$sku = $magprod->getSku();
			//$productid = Mage::getModel('catalog/product')->getIdBySku(trim($sku));
			$productid = $prodint->get($sku);
			if ($productid) {
				//$product = Mage::getModel('catalog/product');
				//$product->load($productid);	
				$product = $productid;
				
				$attributeSetModel = $objectManager->create('\Magento\Eav\Api\AttributeSetRepositoryInterface');
				$attributeSetRepository = $attributeSetModel->get($product->getAttributeSetId());
				$attributeSetName  = $attributeSetRepository->getAttributeSetName();
				$size = "Not found";

				if($attributeSetName === "Bras")
				{
					$size = $product->getAttributeText('uk_chest_size') . $product->getAttributeText('uk_cup_size');
				}
				else if($attributeSetName === "Socks")
				{
					$size = $product->getAttributeText('uk_sock_size');
				}			
				else if($attributeSetName === "Footwear")
				{
					$size = $product->getAttributeText('uk_shoe_size');
				}
				else if($attributeSetName === "Clothing")
				{
					$size = $product->getAttributeText('uk_clothing_size');
				}
				else if($attributeSetName === "Nutrition")
				{
					$size = $product->getAttributeText('uk_nutrition_flavour');
				}
				else if($attributeSetName === "Accessories")
				{
					$size = $product->getAttributeText('uk_accessories_size');
				}
				else if($attributeSetName === "CycleHelmets")
				{
					$size = $product->getAttributeText('helmet_size');
				}
				else if($attributeSetName === "CycleClothing")
				{
					$size = $product->getAttributeText('clothing_size');
				}

				
				if ($product->getData('image') == 'no_selection' || $product->getData('image') == '') {
					$simpleProductId = $product->getId();
					$parentIds  = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($simpleProductId);
					$product2 = $objectManager->create('Magento\Catalog\Model\Product')->load($parentIds[0]);
					if ($product2->getId()) {
						$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
						$prodimage = $_imageHelper->init($product2, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
					} else {
						$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
						$prodimage = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
					}
				}
				else
				{
					$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
					$prodimage = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
				}

				$binloc = $product->getData('bin_location');

				if($binloc == '')
				{
					$binloc = 'Not Set';	
				}

				// Have split by tilde here as it is easier for me to parse at my end - Dan
				echo round($product->getExtensionAttributes()->getStockItem()->getQty()) . '~' . $sku . '~' . $product->getName() . '~' . $product->getData('actual_model'). '~' . $binloc . '~' . $attributeSetName . '~' . $size . '~' . str_replace(',','',number_format($product->getData('price'),2)) . '~' . $prodimage->getUrl() . ',';

			
			} else {
				echo 'no product with sku '.$sku.' exists!';
			}
		}	
	} else {
		echo 'Please include the sku search string';
	}
}
if ($method == "getstocklevel") {
	if ($sku) {

		$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
		try {
			$productid = $prodint->get($sku);
		}
		catch (Exception $e) {
		}

		if (!$productid) {
			$sku='0'.trim($sku);
			$productid = $prodint->get($sku);
		}

		if ($productid) {
			$product = $productid;

			$attributeSetModel = $objectManager->create('\Magento\Eav\Api\AttributeSetRepositoryInterface');
			$attributeSetRepository = $attributeSetModel->get($product->getAttributeSetId());
			$attributeSetName  = $attributeSetRepository->getAttributeSetName();
			$size = "Not found";

			if($attributeSetName === "Bras")
			{
				$size = $product->getAttributeText('uk_chest_size') . $product->getAttributeText('uk_cup_size');
			}
			else if($attributeSetName === "Socks")
			{
				$size = $product->getAttributeText('uk_sock_size');
			}			
			else if($attributeSetName === "Footwear")
			{
				$size = $product->getAttributeText('uk_shoe_size');
			}
			else if($attributeSetName === "Clothing")
			{
				$size = $product->getAttributeText('uk_clothing_size');
			}
			else if($attributeSetName === "Nutrition")
			{
				$size = $product->getAttributeText('uk_nutrition_flavour');
			}
			else if($attributeSetName === "Accessories")
			{
				$size = $product->getAttributeText('uk_accessories_size');
			}
			else if($attributeSetName === "Migration_CycleHelmets")
			{
				$size = $product->getAttributeText('helmet_size');
			}
			else if($attributeSetName === "CycleClothing")
			{
				$size = $product->getAttributeText('clothing_size');
			}

			if ($product->getData('image') == 'no_selection' || $product->getData('image') == '') {
					$simpleProductId = $product->getId();
					$parentIds  = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($simpleProductId);
					$product2 = $objectManager->create('Magento\Catalog\Model\Product')->load($parentIds[0]);
					if ($product2->getId()) {
						$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
						$prodimage = $_imageHelper->init($product2, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
					} else {
						$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
						$prodimage = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
					}
				}
				else
				{
					$_imageHelper = $objectManager->get('Magento\Catalog\Helper\Image');
					$prodimage = $_imageHelper->init($product, 'small_image', ['type'=>'small_image'])->keepAspectRatio(true)->resize($imagesize);
				}



		$sql = 	"SELECT MAX(status) AS statename, COUNT(status) AS noinstate FROM sales_order_item ".
				"RIGHT JOIN sales_order on sales_order_item.order_id=sales_order.entity_id ".
				"WHERE sales_order_item.sku='" . $sku . "' ".
				"AND (status IN ('preprocessing','processing','pick_note_printed','pending_auth','order_query','bike_order','awaiting_giftcard','held','emailed','wholesale_order','pre_order','fraud','bike_awaiting_shipping','cyclescheme_pending')) ".
				"AND state NOT IN ('complete') ".
				"AND sales_order_item.parent_item_id IS NULL ".
				"GROUP BY status ".
				"ORDER BY sales_order.created_at DESC, sales_order.status  ASC";

		$readresult=$readConnection->query($sql);
		$pnp = 0;
		$processing = 0;

		while ($row = $readresult->fetch() ) 
		{
			if ($row['statename'] == 'pick_note_printed')
			{
				$pnp = $row['noinstate'];
			}

			if ($row['statename'] == 'preprocessing')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'processing')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'bike_order')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'awaiting_giftcard')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'emailed')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'order_query')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'pre_order')
			{
				$processing += $row['noinstate'];
			}
			if ($row['statename'] == 'fraud')
			{
				$processing += $row['noinstate'];
			}

		}

			echo round($product->getExtensionAttributes()->getStockItem()->getQty()) . ',' . str_replace(',',' ',$product->getName()) . ',' . str_replace(',',' ',$product->getData('actual_model')). ',' . $product->getData('bin_location') . '  -  OS: ' . str_replace(',','-',strtoupper($product->getData('bin_overstock_location'))) . ',' . $attributeSetName . ',' . $size . ',' . number_format($product->getData('price'),2) . ',' . $prodimage->getUrl() . ',' . $processing . ',' . $pnp;

			/*echo round($product->getStockItem()->getQty()) . ',' . $product->getStockItem()->getProductName() . ',' . $product->getData('actual_model'). ',' . $product->getData('bin_location');*/
		}
		else {
			echo 'no product with sku '.$sku.' exists!';
		}
	}
	else {
		echo 'URL must contain SKU number';
	}
}
elseif ($method == "setbinforconfigurable")
{
       	if ($sku) 
		{
				// New BIN passed in from Android
				$newbin = $_GET["newbin"];

				$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}

				if ($productid) {
					$product = $productid;
					// Get the simple's configurable parent
					$parentIds  = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
					//var_dump($parentIds);
					// Save the new BIN loc at a configurable level
					$configprod = $objectManager->create('Magento\Catalog\Model\Product')->load($parentIds[0]);
					$configprod->setStoreId(0);
					$configprod->setBinLocation($newbin);
					$configprod->getResource()->saveAttribute($configprod, 'bin_location');
					//$configprod->setUpdatedAt(strtotime('now'));
					//$configprod->setUrlKey(false);
					//$configprod->save();

					try 
					{
						$product =$configprod;
						$childIds =$configprod->getTypeInstance()->getUsedProducts($configprod);
						
						foreach ($childIds as $simpleid)
						{
							//foreach ($simple as $simpleid)
							//{
								$simpleprod = $objectManager->create('Magento\Catalog\Model\Product')->load($simpleid->getEntityId());
								$simpleprod->setStoreId(0);
								$simpleprod->setBinLocation($newbin);
								$simpleprod->getResource()->saveAttribute($simpleprod, 'bin_location');
								//$simpleprod->setUpdatedAt(strtotime('now'));
								//$simpleprod->setUrlKey(false);
								//$simpleprod->save();
							//}
						}

					// JS Fix For Stock Not Showing
					// Seems the issue is somewhere with the simples being saved
					// To look into further, but this is just a quick fix
					//$sql_fix = "UPDATE `cataloginventory_stock_status` SET `stock_status` = '1' WHERE `product_id` = ".$configprod->getId()." ";
					// Get the SQL Read/Write Setup
					//$resource = Mage::getSingleton('core/resource');
					//$writeConnection = $resource->getConnection('core_write');
					//$writeConnection->query($sql_fix);
					// JS FIX END

					}
					catch (Exception $e) 
					{
						///Mage::log($e->getMessage());
  						echo $e->getMessage();
					}
				}

				echo ' New BIN set';
		}
		else 
		{
			echo 'no product with sku '.$sku.' exists!';
		}
}
elseif ($method == "setbin"){
       if ($sku) {
                $prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}
               if ($productid) 
				{
					$newbin = $_GET["newbin"];
					$product = $productid;
					
					$product->setStoreId(0);
					echo 'Old BIN: ' . $product->getData('bin_location');
					$product->setBinLocation($newbin);
					$product->getResource()->saveAttribute($product, 'bin_location');
					echo ' New BIN: ' . $product->getData('bin_location');

			try {
  				//$stock_item->save();
				/*$product->setUrlKey(false);
				$product->setUpdatedAt(strtotime('now'));
				$product->save();

				// JS Fix For Stock Not Showing
				$stockStatus = Mage::getModel('cataloginventory/stock_status'); 
				$stockStatus->assignProduct($product); 
				$stockStatus->saveProductStatus($product->getId(),1);*/
			}
			catch (Exception $e) {
				Mage::log($e->getMessage());
  				echo $e->getMessage();
			}
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }
}
elseif ($method == "setoverstockforconfigurable")
{
       	if ($sku) 
		{
				// New BIN passed in from Android
				$newbin = $_GET["newoverstock"];

				$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}

				if ($productid) {
					$product = $productid;
					// Get the simple's configurable parent
					$parentIds  = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
					//var_dump($parentIds);
					// Save the new BIN loc at a configurable level
					$configprod = $objectManager->create('Magento\Catalog\Model\Product')->load($parentIds[0]);
					$configprod->setStoreId(0);
					$configprod->setBinOverstockLocation($newbin);
					$configprod->getResource()->saveAttribute($configprod, 'bin_overstock_location');
					//$configprod->setUpdatedAt(strtotime('now'));
					//$configprod->setUrlKey(false);
					//$configprod->save();

					try 
					{
						$product =$configprod;
						$childIds =$configprod->getTypeInstance()->getUsedProducts($configprod);
						
						foreach ($childIds as $simpleid)
						{
							//foreach ($simple as $simpleid)
							//{
								$simpleprod = $objectManager->create('Magento\Catalog\Model\Product')->load($simpleid->getEntityId());
								$simpleprod->setStoreId(0);
								$simpleprod->setBinOverstockLocation($newbin);
								$simpleprod->getResource()->saveAttribute($simpleprod, 'bin_overstock_location');
								//$simpleprod->setUpdatedAt(strtotime('now'));
								//$simpleprod->setUrlKey(false);
								//$simpleprod->save();
							//}
						}

					// JS Fix For Stock Not Showing
					// Seems the issue is somewhere with the simples being saved
					// To look into further, but this is just a quick fix
					//$sql_fix = "UPDATE `cataloginventory_stock_status` SET `stock_status` = '1' WHERE `product_id` = ".$configprod->getId()." ";
					// Get the SQL Read/Write Setup
					//$resource = Mage::getSingleton('core/resource');
					//$writeConnection = $resource->getConnection('core_write');
					//$writeConnection->query($sql_fix);
					// JS FIX END

					}
					catch (Exception $e) 
					{
						///Mage::log($e->getMessage());
  						echo $e->getMessage();
					}
				}

				echo ' New BIN set';
		}
		else 
		{
			echo 'no product with sku '.$sku.' exists!';
		}
}
elseif ($method == "setoverstock"){
       if ($sku) {
                $prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}
               if ($productid) 
				{
					$newbin = $_GET["newoverstock"];
					$product = $productid;
					
					$product->setStoreId(0);
					echo 'Old BIN: ' . $product->getData('bin_overstock_location');
					$product->setBinOverstockLocation($newbin);
					$product->getResource()->saveAttribute($product, 'bin_overstock_location');
					echo ' New BIN: ' . $product->getData('bin_overstock_location');

			try {
  				//$stock_item->save();
				/*$product->setUrlKey(false);
				$product->setUpdatedAt(strtotime('now'));
				$product->save();

				// JS Fix For Stock Not Showing
				$stockStatus = Mage::getModel('cataloginventory/stock_status'); 
				$stockStatus->assignProduct($product); 
				$stockStatus->saveProductStatus($product->getId(),1);*/
			}
			catch (Exception $e) {
				Mage::log($e->getMessage());
  				echo $e->getMessage();
			}
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }
}
elseif ($method == "addstocklevel"){
       if ($sku) {
                $prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}
               if ($productid) 
				{
				$product = $productid;
				$currentqty = $product->getExtensionAttributes()->getStockItem()->getQty();
			$newQty = $currentqty + $stocklevel;	
			$stock_item = $product->getExtensionAttributes()->getStockItem();


			if($newQty >= 1)
			{
				$stock_item->setData('is_in_stock', 1);

				// G.Hall - 06/06/2016 - Make sure parent is also in stock if set out of stock
				$parent_ids = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
				if (!empty($parent_ids)) {
					$parent_id=$parent_ids[0];
					$stockItemParent =$objectManager->create('Magento\Catalog\Model\Product')->load($parent_id)->getExtensionAttributes()->getStockItem();
					if ($stockItemParent->getData('is_in_stock')==0) {
						$stockItemParent->setData('is_in_stock', 1);
						$stockItemParent->save();
					}
				}
			}

			$stock_item->setData('qty', $newQty);

			try {
				//$product->setUpdatedAt(strtotime('now'));
  				$stock_item->save();
			}
			catch (Exception $e) {
				//Mage::log($e->getMessage());
  				echo $e->getMessage();
			}
			//echo 'Added qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
			echo 'Added:'.round($stocklevel).','.round($currentqty).','.$sku;
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}
elseif ($method == "batchstocklevel")
{
	$data = $_GET["data"];
	$mode = isset($_GET["mode"])? $_GET["mode"]: null;
	$message = 'Error';

	if ($data) 
	{
		$updates = explode("|",$data);
		$total = 0;


		foreach ($updates as $value)
		{
			$entry = explode(",",$value);
			$skutochange = $entry[0];
			$amount = $entry[1];

			$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($skutochange);
				if (!$productid) {
					$skutochange='0'.trim($skutochange);
					$productid = $prodint->get($skutochange);
				}
               if ($productid) 
				{
				$product = $productid;
				$currentqty = $product->getExtensionAttributes()->getStockItem()->getQty();
				$newQty = $currentqty + $amount;	
				$stock_item = $product->getExtensionAttributes()->getStockItem();


				if($newQty >= 1)
				{
					$stock_item->setData('is_in_stock', 1);

					// G.Hall - 06/06/2016 - Make sure parent is also in stock if set out of stock
					$parent_ids = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
					if (!empty($parent_ids)) {
						$parent_id=$parent_ids[0];
						$stockItemParent =$objectManager->create('Magento\Catalog\Model\Product')->load($parent_id)->getExtensionAttributes()->getStockItem();
						if ($stockItemParent->getData('is_in_stock')==0) {
							$stockItemParent->setData('is_in_stock', 1);
							$stockItemParent->save();
						}
					}
				}

				$stock_item->setData('qty', $newQty);

				try {
					//$product->setUpdatedAt(strtotime('now'));
					$stock_item->save();
				}
				catch (Exception $e) {
					//Mage::log($e->getMessage());
					echo $e->getMessage();
				}
				//echo 'Added qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
				echo 'Added:'.round($amount).','.round($currentqty).','.$sku;
				$total += $amount;
			}
               
		}
	}

	echo $message.$total;

}
elseif ($method == "removestocklevel"){
       if ($sku) {
                $prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}
               if ($productid) 
				{
				$product = $productid;
				$currentqty = $product->getExtensionAttributes()->getStockItem()->getQty();
			$newQty = $currentqty - $stocklevel;	
			$stock_item = $product->getExtensionAttributes()->getStockItem();


			

			$stock_item->setData('qty', $newQty);

			try {
				//$product->setUpdatedAt(strtotime('now'));
  				$stock_item->save();
			}
			catch (Exception $e) {
				//Mage::log($e->getMessage());
  				echo $e->getMessage();
			}
			//echo 'Added qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
			echo 'Added:'.round($stocklevel).','.round($currentqty).','.$sku;
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}
elseif ($method == "setstocklevel") {
 	if ($sku) {
                $prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
				$productid = $prodint->get($sku);
				if (!$productid) {
					$sku='0'.trim($sku);
					$productid = $prodint->get($sku);
				}
               if ($productid) 
				{
				$product = $productid;
				$currentqty = $product->getExtensionAttributes()->getStockItem()->getQty();
			$newQty = $stocklevel;	
			$stock_item = $product->getExtensionAttributes()->getStockItem();


			if($newQty >= 1)
			{
				$stock_item->setData('is_in_stock', 1);

				// G.Hall - 06/06/2016 - Make sure parent is also in stock if set out of stock
				$parent_ids = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
				if (!empty($parent_ids)) {
					$parent_id=$parent_ids[0];
					$stockItemParent =$objectManager->create('Magento\Catalog\Model\Product')->load($parent_id)->getExtensionAttributes()->getStockItem();
					if ($stockItemParent->getData('is_in_stock')==0) {
						$stockItemParent->setData('is_in_stock', 1);
						$stockItemParent->save();
					}
				}
			}

			$stock_item->setData('qty', $newQty);

			try {
				//$product->setUpdatedAt(strtotime('now'));
  				$stock_item->save();
			}
			catch (Exception $e) {
				//Mage::log($e->getMessage());
  				echo $e->getMessage();
			}
			//echo 'Added qty of '.round($stocklevel).' to old product qty of '.round($currentqty).' for sku number '.$sku;
			echo 'Added:'.round($stocklevel).','.round($currentqty).','.$sku;
		}
                else {
                        echo 'no product with sku '.$sku.' exists!';
                }
        }
        else {
                echo 'URL must contain SKU number';
        }

}
elseif ($method == "setsku") 
{
	$newsku = $_GET["newsku"];

 	if ($sku) {
		$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
		$productid = $prodint->get($sku);
		if (!$productid) {
			$sku='0'.trim($sku);
			$productid = $prodint->get($sku);
		}
		if ($productid) 
		{
		$product = $productid;
			try 
			{
				$newbin = 'bintest';
				
				$product->setStoreId(0);
				echo 'Old SKU: ' . $product->getData('sku');
				$product->setSku($newsku);
				echo ' New SKU: ' . $product->getData('sku');

				$product->setUpdatedAt(strtotime('now'));
				$product->setUrlKey(false);
				$product->save();

			}       
			catch (Exception $e) 
			{
				//Mage::log($e->getMessage());
				echo $e->getMessage();
			}       
		}
		else 
		{
			echo 'no product with sku '.$sku.' exists!';
		}
	}
	else 
	{
		echo 'URL must contain SKU';
	}
}
elseif($method == "viewallsizes"){
	header('Content-Type: text/plain; charset=utf-8');
	$return = array();
	$sku = $_GET["sku"];
	$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
	$productid = $prodint->get($sku);
	if (!$productid) {
		$sku='0'.trim($sku);
		$productid = $prodint->get($sku);
	}
	if ($productid) 
	{
		$product = $productid;
		$parent = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
		if(isset($parent[0])){
			$parent = $objectManager->create('Magento\Catalog\Model\Product')->load($parent[0]);
			if($parent){
				$productType = $parent->getTypeId();
				//var_dump($productType);
				$childProducts =$parent->getTypeInstance()->getUsedProducts($parent);

				foreach($childProducts as $child) {
					$child = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
					$stocklevel = (int)$child->getExtensionAttributes()->getStockItem()->getQty();
					
					//var_dump($child->getData());
					/*
					 * ["uk_chest_size"]=>
					  string(4) "2768"
					  ["uk_cup_size"]=>
					 */
					
					$socksize = $child->getAttributeText("uk_sock_size");
					$accessorysize = $child->getAttributeText("uk_accessories_size");
					$clothsize = $child->getAttributeText("uk_clothing_size").$child->getAttributeText("clothing_size"); // G.Hall - Added clothing_size to capture Bike clothing sizes
					$shoesize = $child->getAttributeText("uk_shoe_size").$child->getAttributeText("shoe_size"); // G.Hall - Added shoe_size to capture EU shoe sizes
					$nutrisize = $child->getAttributeText("uk_nutrition_size");
					$nutriflavour = $child->getAttributeText("uk_nutrition_flavour");
					$brasize = $child->getAttributeText("uk_chest_size"). "" .$child->getAttributeText("uk_cup_size");
					$helmetsize = $child->getAttributeText("helmet_size");
					
					if(!empty($brasize) && $brasize != ""){
						$size =$brasize;
					}
					else if(!empty($shoesize)){
						$size = $shoesize;
					}
					else if(!empty($nutrisize)){
						$size = $nutrisize;
					}
					else if(!empty($nutriflavour)){
						$size = $nutriflavour;
					}
					else if(!empty($clothsize)){ // G.Hall - Moved $clothsize inside of if to stop everything defaulting to it
						$size =$clothsize;
					}
					else if(!empty($socksize)){
						$size =$socksize;
					}
					else if(!empty($accessorysize)){
						$size =$accessorysize;
					}
					else if(!empty($helmetsize)){
						$size =$helmetsize;
					}
					else{
						$size = "NONE";
					}
					
				    $string = $size."~".$stocklevel."~".($child->getSku()); 
					$return[] = $string;
				}
			}
		}
	}
	echo implode(",", $return);
	
}
elseif($method == "viewallsizeswithpro"){
	header('Content-Type: text/plain; charset=utf-8');
	$return = array();
	$sku = $_GET["sku"];
	$prodint = $objectManager->create('\Magento\Catalog\Api\ProductRepositoryInterface');
	$productid = $prodint->get($sku);
	if (!$productid) {
		$sku='0'.trim($sku);
		$productid = $prodint->get($sku);
	}
	if ($productid) 
	{
		$product = $productid;
		$parent = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getEntityId());
		if(isset($parent[0])){
			$parent = $objectManager->create('Magento\Catalog\Model\Product')->load($parent[0]);
			if($parent){
				$productType = $parent->getTypeId();
				//var_dump($productType);
				$childProducts =$parent->getTypeInstance()->getUsedProducts($parent);

				foreach($childProducts as $child) {
					$child = $objectManager->create('Magento\Catalog\Model\Product')->load($child->getId());
					$stocklevel = (int)$child->getExtensionAttributes()->getStockItem()->getQty();
					
					$sqlcountprocessing ='SELECT IF(SUM(ot.qty_ordered) IS NULL, 0, SUM(ot.qty_ordered)) as totalordered  FROM `sales_order_item` ot 
JOIN sales_order fo ON fo.entity_id = ot.order_id
WHERE fo.state NOT IN ("complete") AND fo.status IN ("preprocessing","processing","pick_note_printed","pending_auth","order_query","bike_order","awaiting_giftcard","held","emailed","wholesale_order","pre_order","fraud","bike_awaiting_shipping","cyclescheme_pending") AND ot.`product_type` = \'simple\' AND ot.`sku` LIKE \''.$child->getSku().'\'';
    				$inprocessing = $readConnection->fetchOne($sqlcountprocessing);
					
					//var_dump($child->getData());
					/*
					 * ["uk_chest_size"]=>
					  string(4) "2768"
					  ["uk_cup_size"]=>
					 */
					
					$socksize = $child->getAttributeText("uk_sock_size");
					$accessorysize = $child->getAttributeText("uk_accessories_size");
					$clothsize = $child->getAttributeText("uk_clothing_size").$child->getAttributeText("clothing_size"); // G.Hall - Added clothing_size to capture Bike clothing sizes
					$shoesize = $child->getAttributeText("uk_shoe_size").$child->getAttributeText("shoe_size"); // G.Hall - Added shoe_size to capture EU shoe sizes
					$nutrisize = $child->getAttributeText("uk_nutrition_size");
					$nutriflavour = $child->getAttributeText("uk_nutrition_flavour");
					$brasize = $child->getAttributeText("uk_chest_size"). "" .$child->getAttributeText("uk_cup_size");
					$helmetsize = $child->getAttributeText("helmet_size");
					
					if(!empty($brasize) && $brasize != ""){
						$size =$brasize;
					}
					else if(!empty($shoesize)){
						$size = $shoesize;
					}
					else if(!empty($nutrisize)){
						$size = $nutrisize;
					}
					else if(!empty($nutriflavour)){
						$size = $nutriflavour;
					}
					else if(!empty($clothsize)){ // G.Hall - Moved $clothsize inside of if to stop everything defaulting to it
						$size =$clothsize;
					}
					else if(!empty($socksize)){
						$size =$socksize;
					}
					else if(!empty($accessorysize)){
						$size =$accessorysize;
					}
					else if(!empty($helmetsize)){
						$size =$helmetsize;
					}
					else{
						$size = "NONE";
					}
					
				    $string = $size."~".$stocklevel."~".round($inprocessing).'~'.($child->getSku()); 
					$return[] = $string;
				}
			}
		}
	}
	echo implode(",", $return);
	
}
