<?php

/**
 *
 * James Simpson Order API
 *
 */

	$order_number = $_GET['get_order_number'];
	$barcode_num  = $_GET['barcode_scan'];
	
	require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
	Mage::app();
	$read = Mage::getSingleton('core/resource')->getConnection('core_read');

if(isset($order_number)){
	$sql = "SELECT entity_id,increment_id,sku,name,product_id,qty_invoiced,qty_refunded FROM `sales_flat_order` INNER JOIN `sales_flat_order_item` on sales_flat_order.entity_id=sales_flat_order_item.order_id WHERE sales_flat_order.increment_id=".$order_number." AND sales_flat_order_item.product_type = 'simple'";
	
	$readresult=$read->query($sql);

	while ($row = $readresult->fetch() ) {
		$model_number = "SELECT value FROM `catalog_product_entity_varchar` WHERE entity_id = ".$row['product_id']." AND attribute_id = '134' AND store_id = '0' ";
		$model = $read->fetchOne($model_number);	

		// If item has been refunded
		if(isset($row['qty_refunded'])){
			$quantity = round($row['qty_invoiced']) - round($row['qty_refunded']) ;

		}else{		
			// Nothing Refunded - Get the actual count
			$quantity = round($row['qty_invoiced']);
	}
		echo $row['sku']."~".$model."~".$row['name']."~".$quantity.",\r\n";
	}

}

if(isset($barcode_num)){
	$sql = "SELECT ent1.sku,var1.value as model,var2.value as title FROM `catalog_product_entity` AS ent1 INNER JOIN `catalog_product_entity_varchar` AS var1  ON ent1.entity_id=var1.entity_id INNER JOIN `catalog_product_entity_varchar` AS var2 ON ent1.entity_id=var2.entity_id where var1.attribute_id = '134' and ent1.sku = '".$barcode_num."' and var2.attribute_id = '71'";

	$readresult1 = $read->query($sql);
	while ($row = $readresult1->fetch() ) {
	echo $row['sku']."~".$row['model']."~".$row['title'];
	}
}
//header("HTTP/1.0 404 Not Found");


?>