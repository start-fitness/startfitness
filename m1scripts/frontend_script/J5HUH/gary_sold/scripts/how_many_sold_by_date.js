$(document).ready(function(){
 $("#Go").bind("click", function() {
	$("#results").html("<div align='center' style='margin-top: 50px; width: 100%'><div><img src='loading.gif' width='50'></div><div style='margin-top: 25px;'><strong>Please Wait... Getting results for "+$("#model").val()+"</strong></div></div>");
     $.ajax({
         type: "GET", 
         url: "how_many_sold_by_date_results.php",
         data: {model: $("#model").val(), start_date: $("#start_date").val(), end_date:$("#end_date").val(), internal_orders:$("#internal_orders").is(":checked")},
         success: function(html) {
             $("#results").html(html);
         }
     });
 });
});
