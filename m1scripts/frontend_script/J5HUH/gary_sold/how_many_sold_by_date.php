<?php
require_once("../gary_stock/config.php");
require_once("../gary_stock/functions.php");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>How Many Sold By Date</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/mootools/1.5.0/mootools-yui-compressed.js"></script>
<script src="scripts/datepicker.js"></script>
<script src="scripts/how_many_sold_by_date.js"></script>

<script>
	window.addEvent('load', function() {
		new DatePicker('.date', { positionOffset: { x: 0, y: 5 }, inputOutputFormat: 'Y-m-d', yearpicker: true});
	});
</script>

<style type="text/css">
input.date {
	color: #FFF;
	width: 100px;
	background-color: #039;
	border-top-width: thin;
	border-right-width: thin;
	border-bottom-width: thin;
	border-left-width: thin;
	border-top-style: solid;
	border-right-style: solid;
	border-bottom-style: solid;
	border-left-style: solid;
}
#main {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 10px;
	color: #060;
	background-color: #FF9;
	width: 800px;
	border: medium double #900;
}
</style>
<link href="css/datepicker.css" rel="stylesheet" type="text/css" />
<link href="../gary_stock/styles.css" rel="stylesheet" type="text/css">
</head>

<body>
<?php
//print_r($_POST);
if (isset($_POST['model'])) {
	$model=mysql_real_escape_string($_POST['model']);
} else {
	$model='';
}
if (isset($_POST['start_date'])) {
	$start_date=mysql_real_escape_string($_POST['start_date']);
} else {
	$start_date =date('Y-m-d');
}
if (isset($_POST['end_date'])) {
	$end_date=mysql_real_escape_string($_POST['end_date']);
} else {
	$end_date =date('Y-m-d');
}
?>
<div id="main" align="center" style="width: 100%">
<form action="" method="post" name="main">
Model<input id='model' name='model' type='text' value='' style='margin-right: 30px' />
<input id="Go" name="Go" type="button" value="Go" style="margin-left: 30px" /><input name="Clear" type="reset" value="Reset" style="margin-left: 10px" />
Start Date<input id='start_date' name='start_date' type='text' value='<?= $start_date?>' class='date date' />
End Date<input id='end_date' name='end_date' type='text' value='<?= $end_date?>' class='date date' />
Include Internal Orders<input id="internal_orders" name="internal_orders" type="checkbox" />
</form>
</div>
<div id="results" align="center" style="width: 100%"></div>

</body>
</html>
