<?php
require_once("../gary_stock/config.php");
require_once("../gary_stock/functions.php");
$model = mysqli_real_escape_string($dbconnection,$_GET['model']);
$start_date = mysqli_real_escape_string($dbconnection,$_GET['start_date']) . " 00:00:00";
$end_date = mysqli_real_escape_string($dbconnection,$_GET['end_date']) . " 23:59:59";
$exclude_stores = '(23,29,30,31,32,33,34)'; // 23,33-Wholesale, (29-32)-Shop Tills, 34 Gina Shop?
$internal_orders = $_GET['internal_orders'];
$days = (strtotime($end_date)-strtotime($start_date))/(60*60*24);
$where_clause = "WHERE sfoi.`store_id`<>-1 AND sfo.`created_at`>='". $start_date ."' AND sfo.`created_at`<='". $end_date ."' AND sfoi.`product_type`='simple' AND sfo.`store_id` NOT IN ".$exclude_stores;
if ($internal_orders=='false') { $where_clause .= " AND sfo.`customer_group_id` NOT IN (2,6,7,9)";}
if ($model=='') {die("<p style=\"margin-top: 50px;\">Please Enter a Model Number!!</p>");}
if ($start_date>$end_date) {die("<p style=\"margin-top: 50px;\">Start Date cannot be after the end date!!</p>");}

$query = "SELECT tmp.`model`, sum(tmp.`qty`) qty, sum(tmp.`refunded`) refunded FROM (SELECT sfoi.`product_id`, sfoi.`store_id`, (SELECT DISTINCT cpev.`value` FROM `catalog_product_entity_varchar` cpev WHERE cpev.`entity_id` = sfoi.`product_id` AND cpev.`attribute_id`=134 AND cpev.`store_id`=0) model, (sfoi.`qty_invoiced`) `qty`, (sfoi.`qty_refunded`) `refunded` FROM `sales_order_item` sfoi INNER JOIN `sales_order` sfo ON sfo.`entity_id` = sfoi.`order_id` $where_clause) tmp WHERE tmp.`model` LIKE '". $model ."%' GROUP BY tmp.`model` ";
$query .= iif(LIMIT_RESULTS > 0," LIMIT " . LIMIT_RESULTS, "");
//echo $query."<p>";
$result = mysqli_query($dbconnection,$query);

$row_count=0;
$total_qty=0;
$total_refunded=0;
$total_value=0.00;
echo '<table style="margin-top: 50px;">';
while($row = mysqli_fetch_array($result,MYSQLI_ASSOC)) {
	if ($row_count==0) {
		echo '<thead><tr align="left">';
		foreach($row as $col=>$val) {    
			echo "<th class=\"header_$col\">".htmlentities(strtoupper($col))."</th>";
		}
		echo '</tr></thead><tbody>';
	}
	echo '<tr class="'.iif($row_count%2 == 0, "row_colour_light", "row_colour_dark").'" align="left">';
	foreach($row as $col=>$val) {
		echo "<td class=\"data_$col\" align=\"" . iif($col=="qty" || $col=="refunded","right","left") . "\">".htmlentities($val)."</td>";
		if($col=="qty") {
			$total_qty+=(int)$val;
		}

		if($col=="refunded") {
			$total_refunded+=(int)$val;
		}
	}
	echo '</tr>';
	$row_count++;
}
echo '<tr></tr>';
echo "<tr class=\"".iif($row_count%2 == 0, "row_colour_light", "row_colour_dark")."\" height=\"50\"><td align=\"center\" colspan=\"3\" align=\"right\"><span style=\"font-size: 16px; margin-right: 10px;\"><strong>".number_format($total_qty, 0, '.', ',')." Sold</strong></span> in $days Days</td></tr>";
echo "<tr class=\"".iif($row_count%2 == 0, "row_colour_light", "row_colour_dark")."\" height=\"50\"><td align=\"center\" colspan=\"3\" align=\"right\"><span style=\"font-size: 16px; margin-right: 10px;\"><strong>".number_format($total_refunded, 0, '.', ',')." Refunded</strong></span></td></tr>";
echo '</tbody>';
echo '</table>';
?>