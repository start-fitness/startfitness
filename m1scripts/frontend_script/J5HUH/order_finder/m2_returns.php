<?php
//error_reporting(E_ALL & ~E_NOTICE);
set_time_limit(0);

ini_set('display_errors', 0);

use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();

 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();

$directoryList = $objectManager->get('\Magento\Framework\App\Filesystem\DirectoryList');
$dir = $directoryList->getPath('base');
?>
<head>
<title>Product Return Finder - James Simpson</title>
<style type="text/css">
#container {
	width: 980px;
	margin-top: 10px;
	margin-bottom: 10px;
	padding: 10px;
	margin-right: auto;
	margin-left: auto;
	background-color:#FFF;
}
#head {
	height: 100px;	
}
#content {
	padding-top:10px;
	padding-bottom: 10px;	
}
#footer {
	text-align:center;
	font-size:10px;	
	margin-top: 1px solid #CCC;
}
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #999;
}
</style>
</head>

<body>
<div id="container">
<div id="head"><img src="logo.png" /></div>
<div id="content">

  <form id="form1" name="form1" method="get" action="m2_returns.php">
    <label for="store">Barcode</label> 
    <label for="barcode"></label>
    <input type="text" name="barcode" id="barcode">
<input type="submit" name="button" id="button" value="Submit" />
  </form>
<?php
$is_set = false;

/* IP check
*
* 217.39.187.57		-	Tri-Shop (Upstairs Office)
* 81.149.242.103	-	Cycles (Shop Floor / Basement)
* 212.140.204.146	-	Cramlington
* 212.140.204.148	- 	Cramlington - G.Hall
* 77.95.33.38 		-	Newcastle Wildcard Networks WiFi Dish
* 31.121.125.106	-	Newcastle BTNet Leased Line
*/
if( 	($_SERVER['REMOTE_ADDR'] != "217.39.187.57") &&
	($_SERVER['REMOTE_ADDR'] != "81.149.242.103") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.146") &&
	($_SERVER['REMOTE_ADDR'] != "77.95.33.38") &&
	($_SERVER['REMOTE_ADDR'] != "31.121.125.106") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.148"))
{
if( 	($_SERVER['HTTP_CF_CONNECTING_IP'] != "217.39.187.57") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "81.149.242.103") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.146") &&
        ($_SERVER['HTTP_CF_CONNECTING_IP'] != "77.95.33.38") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.148"))
{
	die("Invalid IP request from ".$_SERVER['REMOTE_ADDR'] );
}
}

$barcode = $_GET['barcode'];

if(isset($barcode)){
	echo "<h1>Finding Returns...</h1>";
	$is_set = true;
}else{
	$is_set = false;
}

if($is_set == 'true'){
	echo "<h3>Looking For Barcode: ".$barcode."</h3><br>";
//connection to the database
//$dbhandle = mysqli_connect($hostname, $username, $password, $dbname) or die("Unable to connect to MySQL");
//select a database to work with
//$selected = mysql_select_db("fitness",$dbhandle) or die("Could not select examples");
$sql_query = "SELECT sfo.`entity_id`,sfcmi.`sku`,sfo.`increment_id`,sfo.`state`,sfcm.`created_at` FROM `sales_creditmemo_item` sfcmi
						RIGHT JOIN `sales_creditmemo` sfcm on sfcm.`entity_id`=sfcmi.`parent_id` AND sfcmi.`row_total` IS NOT NULL
						INNER JOIN `sales_order` sfo ON sfo.`entity_id`=sfcm.`order_id` 
						WHERE sfcmi.`sku`= '$barcode'
						ORDER BY sfcm.`created_at` DESC";

$result = $read->fetchAll($sql_query);
if (empty($result)) { echo "Not Found!!"; exit; }
foreach($result as $row){
	echo '<strong>Order Number:</strong> <a target="_blank" href="https://www.startfitness.co.uk/admin/sales/order/view/order_id/'.$row['entity_id'].'/">' . $row['increment_id'] . '</a> - <strong>Status:</strong> ' . $row['state'] . ' - <strong>Order Date:</strong> ' . $row['created_at'] . '<br>';
}

//close the connection
//mysqli_close($dbhandle);
}

?>


</div>
<div id="footer">Designed by <a href="http://www.jamessimpson.co.uk">James Simpson</a></div>
</div>
</body>
</html>
