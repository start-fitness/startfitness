<?php
set_time_limit (1000000); 
ini_set('max_execution_time', 1000000);
?>
<head>
<title>Product Order Finder - James Simpson</title>
<style type="text/css">
#container {
	width: 1200px;
	margin-top: 10px;
	margin-bottom: 10px;
	padding: 10px;
	margin-right: auto;
	margin-left: auto;
	background-color:#FFF;
}
#head {
	height: 100px;	
}
#content {
	padding-top:10px;
	padding-bottom: 10px;	
}
#footer {
	text-align:center;
	font-size:10px;	
	margin-top: 1px solid #CCC;
}
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #999;
}

.name {
	color: green;
}
</style>
</head>

<body>
<div id="container">
<div id="head"><img src="logo.png" /></div>
<div id="content">

  <form id="form1" name="form1" method="get" action="find1.php">
    <label for="store">Barcode</label> 
    <label for="barcode"></label>
    <input type="text" name="barcode" id="barcode">
<input type="submit" name="button" id="button" value="Submit" />
  </form>
<?php
$is_set = false;

/* IP check
*
* 217.39.187.57		-	Tri-Shop (Upstairs Office)
* 81.149.242.103	-	Cycles (Shop Floor / Basement)
* 212.140.204.146	-	Cramlington
* 212.140.204.148	- 	Cramlington - G.Hall
* 77.95.33.38 		-	Newcastle Wildcard Networks WiFi Dish
* 31.121.125.106	-	Newcastle BTNet Leased Line
*/
if( 	($_SERVER['REMOTE_ADDR'] != "217.39.187.57") &&
	($_SERVER['REMOTE_ADDR'] != "81.149.242.103") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.146") &&
	($_SERVER['REMOTE_ADDR'] != "77.95.33.38") &&
	($_SERVER['REMOTE_ADDR'] != "31.121.125.106") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.148"))
{
if( 	($_SERVER['HTTP_CF_CONNECTING_IP'] != "217.39.187.57") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "81.149.242.103") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.146") &&
        ($_SERVER['HTTP_CF_CONNECTING_IP'] != "77.95.33.38") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.148"))
{
	die("Invalid IP request from ".$_SERVER['REMOTE_ADDR'] );
}
}

//$username = "fitness";
//$password = "gvBujgRqvReHh8v8";
//$hostname = "db1.i"; 
//$dbname = "fitness";

$username = "m2";
$password = "FZ1hQgaliOLMWCMQ";
$hostname = "db5.i"; 
$dbname = "m2";

$barcode = $_GET['barcode'];

if(isset($barcode)){
	echo "<h1>Finding Orders...</h1>";
	$is_set = true;
}else{
	$is_set = false;
}

if($is_set == 'true'){
	echo "<h3>Looking For Barcode: ".$barcode."</h3><br>";
//connection to the database
$dbhandle = mysqli_connect($hostname, $username, $password, $dbname) or die("Unable to connect to MySQL");
//select a database to work with
//$selected = mysql_select_db("fitness",$dbhandle) or die("Could not select examples");
/*$result = mysqli_query($dbhandle, "SELECT `order_id`,`sku`,`sfo`.`increment_id`,`sfo`.`state`,`sfo`.`created_at`, `sfoa`.`firstname`, `sfoa`.`lastname`, `sfoa`.`street`, `sfoa`.`city`, `sfoa`.`postcode`, `sfoa`.`country_id`, `sfoa`.`telephone`, `sfoa`.`email` FROM `sales_flat_order_item` RIGHT JOIN `sales_flat_order` sfo on `sales_flat_order_item`.`order_id`=`sfo`.`entity_id`
LEFT JOIN `sales_flat_order_address` sfoa ON `sfoa`.`entity_id`=`sfo`.`shipping_address_id`
WHERE `sales_flat_order_item`.`sku`='$barcode' AND `sales_flat_order_item`.`parent_item_id` IS NULL  ORDER BY `sfo`.`created_at` DESC");  */

$result = mysqli_query($dbhandle, "SELECT `order_id`,`sku`,`sfo`.`increment_id`,`sfo`.`state`,`sfo`.`created_at`, `sfoa`.`firstname`, `sfoa`.`lastname`, `sfoa`.`street`, `sfoa`.`city`, `sfoa`.`postcode`, `sfoa`.`country_id`, `sfoa`.`telephone`, `sfoa`.`email` FROM `sales_order_item` sfoi RIGHT JOIN `sales_order` sfo on `sfoi`.`order_id`=`sfo`.`entity_id`
LEFT JOIN `sales_order_address` sfoa ON `sfoa`.`entity_id`=`sfo`.`shipping_address_id`
WHERE `sfoi`.`sku`='$barcode' AND `sfoi`.`parent_item_id` IS NULL  ORDER BY `sfo`.`created_at` DESC");

while($row = mysqli_fetch_array($result)){
	echo '<strong>Order Number:</strong> <a target="_blank" href="https://www.startfitness.co.uk/admin/sales/order/view/order_id/'.$row[0].'/">' . $row[2] . '</a> - <strong>Status:</strong> ' . $row[3] . ' - <strong>Order Date:</strong> ' . $row[4] . ' <span class="name"><strong>' . $row[5] . ' ' . $row[6] . '</strong></span> - ' . $row[7] . ',' . $row[8] . ',' . $row[9] . ',' . $row[10] . '<br>';
}

//close the connection
mysqli_close($dbhandle);
}

?>


</div>
<div id="footer">Designed by <a href="http://www.jamessimpson.co.uk">James Simpson</a></div>
</div>
</body>
</html>
