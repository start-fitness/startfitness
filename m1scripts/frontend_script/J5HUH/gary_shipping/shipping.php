<?php
require_once('calendar/classes/tc_calendar.php');
?>

<head>
<title>Gary Shipping Report - James Simpson</title>
<style type="text/css">
#container {
	width: 980px;
	margin-top: 10px;
	margin-bottom: 10px;
	padding: 10px;
	margin-right: auto;
	margin-left: auto;
	background-color:#FFF;
}
#head {
	height: 100px;	
}
#content {
	padding-top:10px;
	padding-bottom: 10px;	
}
#footer {
	text-align:center;
	font-size:10px;	
	margin-top: 1px solid #CCC;
}
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 11px;
	color: #000;
	border: 1px solid black;
}
table {
    border-collapse: collapse;
}
table, td, th {
    border: 1px solid green;
	padding: 3px;
}

th {
    background-color: green;
    color: white;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #999;
}
.whiteBackground {background-color: #fff;}
.grayBackground {background-color: #eee;}
</style>
<link href="calendar/calendar.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="calendar/calendar.js"></script>
</head>

<body>
<div id="container">
<div id="head"><img src="logo.png" /></div>
<div id="content">



<?php
$theDate1 = isset($_POST["date3"]) ? $_POST["date3"] : "";
$theDate2 = isset($_POST["date4"]) ? $_POST["date4"] : "";
?>



<form name="form1" method="post" action="">
 <p class="largetxt"><b>Date Pair</b></p>
              <div style="float: left;">
                <div style="float: left; padding-right: 3px; line-height: 18px;">from:</div>
                <div style="float: left;">
                  <?php
						$thisweek = date('W');
						$thisyear = date('Y');

						$dayTimes = getDaysInWeek($thisweek, $thisyear);
						//----------------------------------------
						$datemod = strtotime("-1 week", time());

						if(isset($_POST["date3"])){
							$date1 = $theDate1;
							$date2 = $theDate2;
						}else{
							$date1 = date('Y-m-d', $datemod);
							$date2 = date('Y-m-d');
						}

						function getDaysInWeek ($weekNumber, $year, $dayStart = 1) {
						  // Count from '0104' because January 4th is always in week 1
						  // (according to ISO 8601).
						  $time = strtotime($year . '0104 +' . ($weekNumber - 1).' weeks');
						  // Get the time of the first day of the week
						  $dayTime = strtotime('-' . (date('w', $time) - $dayStart) . ' days', $time);
						  // Get the times of days 0 -> 6
						  $dayTimes = array ();
						  for ($i = 0; $i < 7; ++$i) {
							$dayTimes[] = strtotime('+' . $i . ' days', $dayTime);
						  }
						  // Return timestamps for mon-sun.
						  return $dayTimes;
						}


					  $myCalendar = new tc_calendar("date3", true, false);
					  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
					  $myCalendar->setDate(date('d', strtotime($date1)), date('m', strtotime($date1)), date('Y', strtotime($date1)));
					  $myCalendar->setPath("calendar/");
					  $myCalendar->setYearInterval(1970, 2020);
					  //$myCalendar->dateAllow('2009-02-20', "", false);
					  $myCalendar->setAlignment('left', 'bottom');
					  $myCalendar->setDatePair('date3', 'date4', $date2);
					  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
					  $myCalendar->writeScript();
					  ?>
                </div>
              </div>
              <div style="float: left;">
                <div style="float: left; padding-left: 3px; padding-right: 3px; line-height: 18px;">to</div>
                <div style="float: left;">
                  <?php
					  $myCalendar = new tc_calendar("date4", true, false);
					  $myCalendar->setIcon("calendar/images/iconCalendar.gif");
					  $myCalendar->setDate(date('d', strtotime($date2)), date('m', strtotime($date2)), date('Y', strtotime($date2)));
					  $myCalendar->setPath("calendar/");
					  $myCalendar->setYearInterval(1970, 2020);
					  //$myCalendar->dateAllow("", '2009-11-03', false);
					  $myCalendar->setAlignment('left', 'bottom');
					  $myCalendar->setDatePair('date3', 'date4', $date1);
					  //$myCalendar->setSpecificDate(array("2011-04-01", "2011-04-04", "2011-12-25"), 0, 'year');
					  $myCalendar->writeScript();
					  ?>
                </div>
              </div>
              <p>
		<input type="submit" name="submit" value="Pick Date" style="margin-left:10px;">
              </p>
</form>

<?php

$is_set = false;
$username = "fitness";
$password = "PK4LLCUz";
$hostname = "193.200.192.53"; 



//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) or die("Unable to connect to MySQL");
//select a database to work with
$selected = mysql_select_db("fitness",$dbhandle) or die("Could not select examples");

$getOrders = mysql_query('SELECT DISTINCT (SO.`increment_id`) AS "Order Num",SA.`country_id` AS "Country",SO.`shipping_description` AS "Shipping Method",SO.`weight` AS "Weight",SO.`store_id` AS "Website",`SO`.`base_grand_total` AS "Goods  Price",`SO`.`base_shipping_amount` AS "Shipping Price",SO.`created_at`AS "Order Date",ST.`title` AS "Dispatched Via" FROM `sales_flat_order` SO  RIGHT JOIN `sales_flat_order_address` SA ON  SO.`entity_id`=SA.`parent_id` RIGHT JOIN `sales_flat_shipment_track` ST ON SO.`entity_id`=ST.`order_id` where SO.created_at BETWEEN "'.$theDate1.'" AND "'.$theDate2.'" AND SO.base_grand_total > "0"');
	
?>
<table width="100%" >
  <tr>
    <th align="center"><strong>Order #</strong></th>
    <th align="center"><strong>Country</strong></th>
    <th align="center"><strong>Shipping Method</strong></th>
    <th align="center"><strong>Wegt</strong></th>
    <th align="center"><strong>Web</strong></th>
    <th align="center"><strong>Goods Price</strong></th>
    <th align="center"><strong>Shipping Price</strong></th>
    <th align="center"><strong>Should Price</strong></th>
    <th align="center"><strong>Date</strong></th>
    <th align="center"><strong>Dispatched Via</strong></th>
  </tr>

<?php

function getShipping($dest,$weight,$method) {
	$cost=0;
    	if($dest=='GB'){
		if($method=="Delivery Options - Free Express Promotion"){
			$cost = "6.95";
		}else if($method=="Delivery Options - Free Delivery"){
			$cost = "4.95";
		}else if($method=="Amazon Shipping - Std UK Dom"){
			$cost = "4.95";
		}else if($method=="Delivery Options - Free Delivery"){
			$cost = "4.95";
		}
	}else{
		$cost = $method;
	}
	return $cost;
}
	$replace = array('Delivery Options - ','Amazon Shipping - ','eBay Shipping - ','Royal Mail International ');
	while($row = mysql_fetch_array($getOrders)){
	$class = ($x%2 == 0)? 'whiteBackground': 'graybackground';
	echo "<tr class='$class'>";
	echo "<td>" . $row['0'] . "</td>"; // Order Num
	echo "<td>" . $row['1'] . "</td>"; // Country
	echo "<td>" . str_replace($replace,"",$row['2']) . "</td>"; // Shipping Method
	echo "<td>" . number_format($row['3'],2) . "</td>"; // Weight
	echo "<td>" . $row['4'] . "</td>"; // Website
	echo "<td>�" . number_format($row['5'],2) . "</td>"; // Goods Price
	echo "<td>�" . number_format($row['6'],2) . "</td>"; // Shipping Price
	if($row['6']==0){
	$should_be = 0;
	$should_be = getShipping($row['1'],$row['3'],$row['2']);
	$grand_total += $should_be;
	echo "<td>�" . $should_be .  "</td>"; // Should Cost
	}else{
	echo "<td></td>";
	}
	echo "<td>" . date_format(date_create($row['7']),'d/m/y') . "</td>"; // Order Date
	echo "<td>" . $row['8'] . "</td>"; // Dispatched Via
	echo "</tr>";
	$mag_total += $row['6'];
	$x++ ;
	}
	
	echo "<tr>";
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border-bottom: 1px solid #fff;"></td>';
	echo "<td><strong>&pound;".number_format($mag_total,2)."</strong></td>";
	echo "<td><strong>&pound;".number_format($grand_total,2)."</strong></td>";
	echo '<td style="border: 1px solid #fff;"></td>';
	echo '<td style="border: 1px solid #fff;"></td>';
	echo "</tr>";

mysql_close($connect);
?>
</table>

<?php

     
      




?>
</div>
<div id="footer">Designed by <a href="http://www.jamessimpson.co.uk">James Simpson</a></div>
</div>
</body>
</html>