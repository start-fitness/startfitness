<?php
error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/vhosts/startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
require_once(dirname(__FILE__).'/../../../app/Mage.php');

umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

date_default_timezone_set("GMT");

$fitness_image_url = '6/small_image/180x/9df78eab33525d08d6e5fb8d27136e95';
$fitness_image_smushed_url = '6/small_image/180x/smushed_9df78eab33525d08d6e5fb8d27136e95';
$cycles_image_url = '1/small_image/170x/9df78eab33525d08d6e5fb8d27136e95';
$cycles_image_smushed_url = '1/small_image/170x/smushed_9df78eab33525d08d6e5fb8d27136e95';
$football_image_url = '5/small_image/170x/9df78eab33525d08d6e5fb8d27136e95';
$football_image_smushed_url = '5/small_image/170x/smushed_9df78eab33525d08d6e5fb8d27136e95';

$store_id = 0;
//echo $_POST['cat_id']; exit;

$category_id = (int)$_GET['cat_id'];

if($category_id==0) exit;

$sql_categories = "SELECT cce.`children_count` FROM `catalog_category_entity` cce WHERE `entity_id` = $category_id";
$results_sql_categories = $read->fetchOne($sql_categories);

//print_r($results_sql_categories);

if ($results_sql_categories > 0) {
	$sql_categories = "SELECT GROUP_CONCAT(cce.`entity_id`) entity_id FROM `catalog_category_entity` cce WHERE cce.`parent_id` = $category_id GROUP BY cce.`parent_id` ";
	$results_sql_categories = $read->fetchOne($sql_categories);
	$category_id =$results_sql_categories;

	//exit;
}

// Get Product Rows
$sql_products = "SELECT ccp.`category_id`, cpe.`entity_id`, cpev_model.`value` model, cpev_name.`value` name, cpe.`type_id`, eas.`attribute_set_name`, tmp.`qty`, cpev_gender.`value` `gender` FROM `catalog_product_entity` cpe
				INNER JOIN `catalog_category_product` ccp ON ccp.`product_id`=cpe.`entity_id`
				LEFT JOIN `eav_attribute_set` eas ON eas.`attribute_set_id`=cpe.`attribute_set_id`
				LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id` INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id` ) tmp ON tmp.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = cpe.`entity_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0
				LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = cpe.`entity_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0
				LEFT JOIN `catalog_product_entity_varchar` cpev_gender ON cpev_gender.`entity_id` = cpe.`entity_id` and cpev_gender.`attribute_id`=141 and cpev_gender.`store_id`=0
				WHERE ccp.`category_id` IN ($category_id)
				HAVING tmp.`qty`>0";

//echo '<p>'.$sql_products;

// Get Product Attribute Columns

$sql_attributes = "SELECT DISTINCT cpei.`attribute_id`, ea.`frontend_label` FROM `catalog_product_entity` cpe
				INNER JOIN `catalog_category_product` ccp ON ccp.`product_id`=cpe.`entity_id`
				INNER JOIN `catalog_product_entity_int` cpei ON cpei.`entity_id`=cpe.`entity_id`
				LEFT JOIN `eav_attribute` ea ON ea.`attribute_id` = cpei.`attribute_id`
				LEFT JOIN `eav_attribute_set` eas ON eas.`attribute_set_id`=cpe.`attribute_set_id` LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id` INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id` ) tmp ON tmp.`product_id`=cpe.`entity_id` 
				WHERE ccp.`category_id` IN ($category_id) AND (cpei.`attribute_id` IN (SELECT DISTINCT cea.`attribute_id` FROM `catalog_eav_attribute` cea WHERE `is_filterable` = 1) OR cpei.`attribute_id`=343) AND cpei.`value` is not NULL AND tmp.`qty`>0";

//echo '<p>'.$sql_attributes;

// Draw Table

$results_sql_products = $read->fetchAll($sql_products);
$results_sql_attributes = $read->fetchAll($sql_attributes);

$html .= '<style>';
$html .= 'table { color: #000; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= 'td, th { border: 1px solid #CCC; height: 20px; }';
$html .= 'th { background: #CCCCCC; padding-left: 5px; font-weight: bold; font-size: 14px; }';
$html .= 'td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= '.red { background: #F00; }';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr align="left"><th>Category ID</th><th>Entity ID</th><th>Model</th><th>Name</th><th>Type</th><th>Attribute Set</th><th>Qty</th><th>Gender</th>';
foreach ($results_sql_attributes as $col) {
	$html.='<th>'.$col['frontend_label'].'</th>';
	$total_count[$col['attribute_id']]=0;
	$missing_count[$col['attribute_id']]=0;
	$success_count[$col['attribute_id']]=0;
}
$html.='</tr>';

$num_products = 0;
$gender_missing_count = 0; $gender_total_count = 0; $gender_success_count = 0; 

//Loop Rows
foreach ($results_sql_products as $row) {
	$entity_id = $row['entity_id'];

	$gender=$row['gender'];
	if ($gender=='') {
		$gender_class='red';
		$gender_missing_count+=1;
	} else {
		$gender_class='';
		$gender_success_count+=1;
	}

	$html.='<tr><td>'.$row['category_id'].'</td><td>'.$entity_id.'</td><td>'.$row['model'].'</td><td>'.$row['name'].'</td><td>'.$row['type_id'].'</td><td>'.$row['attribute_set_name'].'</td><td>'.$row['qty'].'</td><td class="'.$gender_class.'">'.$gender.'</td>';

	//echo $row['entity_id'] . ' - ' .$row['type_id'] . ' - ' .$row['attribute_set_name'] . ' - ' .$row['qty'];

	foreach ($results_sql_attributes as $col) {
		$attribute_id = $col['attribute_id'];
		$total_count[$attribute_id]+=1;
		$sql_attribute_values = "SELECT eaov.`value` FROM catalog_product_entity cpe
								INNER JOIN catalog_product_entity_int cpei ON cpei.entity_id = cpe.entity_id
								INNER JOIN eav_attribute ea ON ea.attribute_id = cpei.attribute_id
								INNER JOIN eav_attribute_option eao ON eao.attribute_id = ea.attribute_id AND eao.option_id = cpei.value
								INNER JOIN eav_attribute_option_value eaov ON eaov.option_id = eao.option_id AND eaov.store_id = 0
								WHERE cpe.`entity_id`=$entity_id AND ea.`attribute_id`=$attribute_id";
		$results_sql_attribute_values = $read->fetchAll($sql_attribute_values);
		if ($results_sql_attribute_values[0]['value'] == '') {
			$html.='<td class="red">';
			$missing_count[$attribute_id]+=1;
		} else {
			$html.='<td>';
			$success_count[$attribute_id]+=1;
		}
		$html.=$results_sql_attribute_values[0]['value'];

		//echo ' - ' . $results_sql_attribute_values[0]['value'];
		//echo ' - ' . $col['frontend_label'];
		$num_products += 1;
	}
	$html.='</tr>';
	//echo '<br / >';



	$num_products += 1;
}

$gender_total_count = $gender_missing_count + $gender_success_count; 

$columns=sizeof($total_count)+7;
$html.='<tr><td colspan="'.$columns.'">&nbsp;</td></tr>';

$html.='<tr><td colspan="7" align="right">Missing</td>';
$html.='<td>'.$gender_missing_count.'</td>';
foreach ($missing_count as $attribute => $attribute_total) {
	$html.='<td>'.$attribute_total.'</td>';
}
$html.='</tr>';

$html.='<tr><td colspan="7" align="right">Success</td>';
$html.='<td>'.$gender_success_count.'</td>';
foreach ($success_count as $attribute => $attribute_total) {
	$html.='<td>'.$attribute_total.'</td>';
}
$html.='</tr>';

$html.='<tr><td colspan="7" align="right">Total</td>';
$html.='<td>'.$gender_total_count.'</td>';
foreach ($total_count as $attribute => $attribute_total) {
	$html.='<td>'.$attribute_total.'</td>';
}
$html.='</tr></table>';

echo $html;
?>
