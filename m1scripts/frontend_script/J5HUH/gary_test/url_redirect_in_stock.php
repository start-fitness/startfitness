<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

/* IP check
*
* 217.39.187.57 - Tri-Shop (Upstairs Office)
* 81.149.242.103 - Cycles (Shop Floor / Basement)
* 212.140.204.146 - Cramlington
* 212.140.204.148 - Cramlington - G.Hall
* 77.95.33.38 - Newcastle 
*/
if( 	($_SERVER['REMOTE_ADDR'] != "217.39.187.57") &&
	($_SERVER['REMOTE_ADDR'] != "81.149.242.103") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.146") &&
        ($_SERVER['REMOTE_ADDR'] != "77.95.33.38") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.148"))
{
if( 	($_SERVER['HTTP_CF_CONNECTING_IP'] != "217.39.187.57") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "81.149.242.103") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.146") &&
        ($_SERVER['HTTP_CF_CONNECTING_IP'] != "77.95.33.38") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.148"))
{
	die("Invalid IP request from ".$_SERVER['REMOTE_ADDR'] );
}
}

// First connect up to Mage
//require_once('/var/www/vhosts/startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
require_once dirname(__FILE__).'/../../../app/Mage.php';

umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';

// Get Data
$sql_getsales = "SELECT DISTINCT cs.`name` `store_name`, cpw.`website_id`, eurr.`store_id`, cpev_model.`value` model, tmp.`qty`, cpev_name.`value` name, CONCAT('http://www.startfitness.co.uk/',cpeuk.`value`,'.html') full_url_key, CONCAT(cpeuk.`value`,'.html') url_key, eurr.`target_path`, eurr.`description` FROM `catalog_product_entity` cpe
	INNER JOIN `catalog_product_entity_int` cpei ON cpei.`entity_id`=cpe.`entity_id` AND cpei.`attribute_id` = 102 AND cpei.`value`=4
	LEFT JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = cpe.`entity_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0
	LEFT JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = cpe.`entity_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0
	LEFT JOIN `catalog_product_entity_url_key` cpeuk ON `cpeuk`.`entity_id` = cpe.`entity_id` and `cpeuk`.`attribute_id`=97 and `cpeuk`.`store_id`=0
	LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id` INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp ON tmp.`product_id`=cpe.`entity_id`
	LEFT JOIN `catalog_product_entity_int` cpei_status ON cpei_status.`entity_id`=cpe.`entity_id` AND cpei_status.`attribute_id`=96 AND cpei_status.`value`=1
	LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id`
	INNER JOIN `enterprise_url_rewrite_redirect` eurr ON eurr.`identifier` = CONCAT(cpeuk.`value`,'.html') AND eurr.`store_id` IN (6,28)
	LEFT JOIN `core_store` cs ON cs.`store_id`=eurr.`store_id` AND cs.`website_id`=cpw.`website_id`
	WHERE tmp.`qty` > 0 AND eurr.`options` = 'RP' AND cpei_status.`value`<>2 AND cpw.`website_id` <> 8 AND cs.`name` IS NOT NULL
	ORDER BY name";

//echo $sql_getsales; exit;
// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Website ID</th><th align="left">Store ID</th><th align="left">Model #</th><th align="left">Name</th><th>Qty</th><th>URL</th><th>Target Path</th><th>Description</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['website_id'].'</td><td>'.$home['store_id'].'</td><td>'.$home['model'].'</td><td>'.$home['name'].'</td><td align="center">'.number_format($home['qty'],0).'</td><td>'.$home['url_key'].'</td><td>'.$home['target_path'].'</td><td>'.$home['description'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
/*
$sendToEmailAddress = array('michaelgrimes@startfitness.co.uk','bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','lindsayhedley@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Sales Report '.date("D d-M-Y"));
			$mail->send();

*/
echo $html;
?>
