<?php
error_reporting(E_ALL);

/* IP check
*
* 217.39.187.57 - Tri-Shop (Upstairs Office)
* 81.149.242.103 - Cycles (Shop Floor / Basement)
* 77.95.33.38 - Cycles Shop (Wifi Dish IP?)
* 212.140.204.146 - Cramlington
* 212.140.204.148 - Cramlington - G.Hall
* 46.253.188.182 - Brad on Tour
*/
if( 	($_SERVER['REMOTE_ADDR'] != "217.39.187.57") &&
	($_SERVER['REMOTE_ADDR'] != "81.149.242.103") &&
	($_SERVER['REMOTE_ADDR'] != "77.95.33.38") &&
	($_SERVER['REMOTE_ADDR'] != "46.253.188.182") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.146") &&
	($_SERVER['REMOTE_ADDR'] != "212.140.204.148"))
{
if( 	($_SERVER['HTTP_CF_CONNECTING_IP'] != "217.39.187.57") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "81.149.242.103") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "77.95.33.38") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "46.253.188.182") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.146") &&
	($_SERVER['HTTP_CF_CONNECTING_IP'] != "212.140.204.148"))
{
	die("Invalid IP request from ".$_SERVER['REMOTE_ADDR'] );
}
}


// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento

umask(0);
Mage::app();

$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

date_default_timezone_set("GMT");

// PHP - Build SQL for form

$fitness_image_url = '6/small_image/200x/9df78eab33525d08d6e5fb8d27136e95';
$fitness_image_smushed_url = '6/small_image/200x/smushed_9df78eab33525d08d6e5fb8d27136e95';
$cycles_image_url = '1/small_image/170x/9df78eab33525d08d6e5fb8d27136e95';
$cycles_image_smushed_url = '1/small_image/170x/smushed_9df78eab33525d08d6e5fb8d27136e95';
$football_image_url = '5/small_image/170x/9df78eab33525d08d6e5fb8d27136e95';
$football_image_smushed_url = '5/small_image/170x/smushed_9df78eab33525d08d6e5fb8d27136e95';

$sportssize_image_url = '28/small_image/210x/9df78eab33525d08d6e5fb8d27136e95';
$sportssize_image_smushed_url = '28/small_image/210x/smushed_9df78eab33525d08d6e5fb8d27136e95';

$store_id = 0;


//Create Bundle Price Array

$sql_bundle = "SELECT al.`related_id`, cped_price.`value` price FROM `autobundler_link` al
				LEFT JOIN `catalog_product_entity_decimal` cped_price ON cped_price.`entity_id` = al.`bundle_id` and cped_price.`attribute_id`=75 AND cped_price.`store_id`=0
				WHERE al.`to_delete`=0";
$results_sql_bundle = $read->fetchAll($sql_bundle);

foreach ($results_sql_bundle as $key) {
	$bundle_price_array[$key['related_id']]=$key['price'];
}

$sql_fields = "IF(al.`related_id`>0 AND al.`to_delete` <> 1,'true','false') hasBundle, cpe.`entity_id`, DATE(cpe.`created_at`) created_at, cpe.`attribute_set_id`, cpe.`has_options`, IF(cpe.`type_id`='configurable' OR (cpe.`type_id`='simple' AND cpe.`has_options` = 0 AND cpsl.`parent_id` is NULL),'true','false') isProduct, cpsl.`parent_id`, tmp_qty.`qty`, cped_rrp.`value` rrp, `cped_cost`.`value` cost_price, `cped_wholesale`.`value` wholesale_price, cped_price.`value` price, IF(cped_rrp.`value`<>0,round(((cped_rrp.`value`-cped_price.`value`)/cped_rrp.`value`)*100),0) discount, cpev_bin.`value` bin, cpei_wholesale.`value` isWholesale";
$sql_where_clause = "";
$sql_having_clause = "";
$sql_order_by1 = "";
$sql_order_by2 = "";
$sql_join = "LEFT JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpe.`entity_id`
			LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi
						LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl
						INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id`
						INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp_qty ON tmp_qty.`product_id`=cpe.`entity_id`
			LEFT OUTER JOIN `catalog_product_entity_decimal` cped_rrp ON `cped_rrp`.`entity_id` = `cpe`.`entity_id` and `cped_rrp`.`attribute_id`=120 AND `cped_rrp`.`store_id`=0 
			LEFT OUTER JOIN `catalog_product_entity_decimal` cped_cost ON `cped_cost`.`entity_id` = `cpe`.`entity_id` and `cped_cost`.`attribute_id`=293 AND `cped_cost`.`store_id`=0 
			LEFT OUTER JOIN `catalog_product_entity_decimal` cped_wholesale ON `cped_wholesale`.`entity_id` = `cpe`.`entity_id` and `cped_wholesale`.`attribute_id`=382 AND `cped_wholesale`.`store_id`=23 
			LEFT OUTER JOIN `catalog_product_entity_decimal` cped_price ON `cped_price`.`entity_id` = `cpe`.`entity_id` and `cped_price`.`attribute_id`=75 AND `cped_price`.`store_id`=0
			LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_bin ON cpev_bin.`entity_id` = cpe.`entity_id` and cpev_bin.`attribute_id`=136 and cpev_bin.`store_id`=0
			LEFT OUTER JOIN `autobundler_link` al ON al.`related_id` = cpe.`entity_id`
			LEFT OUTER JOIN `catalog_product_entity_int` cpei_wholesale ON cpei_wholesale.`entity_id` = cpe.`entity_id` and cpei_wholesale.`attribute_id`=381 and cpei_wholesale.`store_id`=0
			LEFT OUTER JOIN `catalog_category_product` ccp ON ccp.`product_id` = cpe.`entity_id` ";

if (isset($_POST)) {
	$max_results=$_POST['max_results'];
	if ($max_results == '') {
		$max_results = 500;
	}
	$limit = "LIMIT ".$max_results;

	$start_created_date=$_POST['start_created_date'];
	if ($start_created_date == '') {
		$start_created_date = '2000/01/01';
	}

	$end_created_date=$_POST['end_created_date'];
	if ($end_created_date == '') {
		$end_created_date = '2039/12/31';
	}
	$sql_where_clause.="AND `created_at` BETWEEN '".$start_created_date."' AND '".$end_created_date."' ";

	$min_qty=$_POST['min_qty'];
	if ($min_qty == '') {
		$min_qty = 1;
	}

	$max_qty=$_POST['max_qty'];
	if ($max_qty == '') {
		$max_qty = 99999;
	}
	$sql_where_clause.="AND tmp_qty.`qty` BETWEEN ".$min_qty." AND ".$max_qty." ";

	$min_price=$_POST['min_price'];
	if ($min_price == '') {
		$min_price = 0.00;
	}

	$max_price=$_POST['max_price'];
	if ($max_price == '') {
		$max_price = 9999.99;
	}
	$sql_where_clause.="AND cped_price.`value` BETWEEN ".$min_price." AND ".$max_price." ";

	$min_discount=$_POST['min_discount'];
	if ($min_discount == '') {
		$min_discount = 0.00;
	}

	$max_discount=$_POST['max_discount'];
	if ($max_discount == '') {
		$max_discount = 100.00;
	}
	$sql_where_clause.="AND IF(cped_rrp.`value`<>0,round(((cped_rrp.`value`-cped_price.`value`)/cped_rrp.`value`)*100),0) BETWEEN ".$min_discount." AND ".$max_discount." ";

	$source=$_POST['source'];
	if ($source != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`attribute_id` = 343 AND cpei_source.`entity_id` = cpe.`entity_id` AND cpei_source.`entity_type_id` = 4 AND cpei_source.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpei_source.`value` = ".$source." ";
	}

	$gender=$_POST['gender'];
	if ($gender != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_varchar` cpei_gender ON cpei_gender.`attribute_id` = 141 AND cpei_gender.`entity_id` = cpe.`entity_id` AND cpei_gender.`entity_type_id` = 4 AND cpei_gender.`store_id` = ".$store_id." ";
		if ($gender=="NULL") {
			$sql_where_clause.="AND cpei_gender.`value` is NULL ";
		} else {
			$sql_where_clause.="AND POSITION(".$gender." IN cpei_gender.`value`) AND (NOT POSITION(1".$gender." IN cpei_gender.`value`)) ";
		}
	}

	$brand=$_POST['brand'];
	if ($brand != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_int` cpei_brand ON cpei_brand.`attribute_id` = 81 AND cpei_brand.`entity_id` = cpe.`entity_id` AND cpei_brand.`entity_type_id` = 4 AND cpei_brand.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpei_brand.`value` IN (".implode(',',$brand).") ";
	}

	$attribute_set=$_POST['attribute_set'];
	if ($attribute_set != '') {
		$sql_where_clause.="AND cpe.`attribute_set_id` IN (".implode(',',$attribute_set).") ";
	}

	$name_inclusive=$_POST['name_inclusive'];

	$bundles_only=$_POST['bundles_only'];
	if ($bundles_only) {
		$sql_having_clause .= "AND hasBundle = 'true' ";
	}

	$wholesale_only=$_POST['wholesale_only'];
	if ($wholesale_only) {
		$sql_having_clause .= "AND isWholesale = 1 ";
	}

	$name_search=$_POST['name_search'];
	if ($name_search != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_varchar` cpev_name ON cpev_name.`entity_id`=cpe.`entity_id` AND cpev_name.`attribute_id` = 71 AND cpev_name.`entity_type_id` = 4 AND cpev_name.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpev_name.`value` ".($name_inclusive == "true" ? "" : "NOT")." LIKE '%".$name_search."%' ";
	}

	$startgroup_only=$_POST['startgroup_only'];
	if ($startgroup_only != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`product_id` NOT IN (SELECT DISTINCT `product_id` FROM `catalog_product_website` WHERE `website_id` IN (1,3,5,7))";
	}

	$sportssize=$_POST['sportssize'];
	if ($sportssize != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`website_id` IN (9)";
	}

	$fitness=$_POST['fitness'];
	if ($fitness != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`website_id` IN (3)";
	}

	$fitnessnotssize=$_POST['fitnessnotssize'];
	if ($fitnessnotssize != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`website_id` IN (3) AND cpw.`product_id` NOT IN (SELECT DISTINCT `product_id` FROM `catalog_product_website` WHERE `website_id` IN (9))";
	}

	$ssizenotfitness=$_POST['ssizenotfitness'];
	if ($ssizenotfitness != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`website_id` IN (9) AND cpw.`product_id` NOT IN (SELECT DISTINCT `product_id` FROM `catalog_product_website` WHERE `website_id` IN (3))";
	}

	$fitnessandssize=$_POST['fitnessandssize'];
	if ($fitnessandssize != '') {
		$sql_join.="LEFT JOIN `catalog_product_website` cpw ON cpw.`product_id`=cpe.`entity_id` ";
		$sql_where_clause.="AND cpw.`website_id` IN (3) AND cpw.`product_id` IN (SELECT DISTINCT `product_id` FROM `catalog_product_website` WHERE `website_id` IN (9))";
	}

	$show_sales=$_POST['show_sales'];
	if ($show_sales) {
		$sales_from_date=date("Y-m-d", strtotime("-30 days")).' 00:00:00';
		$sql_sales = "SELECT sfoi.`product_id`, SUM(`sfoi`.`qty_ordered`) qty FROM `sales_flat_order_item` sfoi
					INNER JOIN `sales_flat_order` sfo ON sfo.`entity_id` = sfoi.`order_id`
					WHERE sfoi.`product_type`='simple' AND sfo.`store_id` NOT IN (23,29,30,31,32,33) AND sfo.`status` NOT IN ('canceled','payment_pending') AND `sfo`.`customer_group_id` NOT IN (2,6,7,9) AND sfo.`created_at`>='".$sales_from_date."'
					GROUP BY sfoi.`product_id`";

		//echo '<p>'.$sql_sales;

		$results_sql_sales = $read->fetchAll($sql_sales);

		foreach ($results_sql_sales as $key) {
			$sales_array[$key['product_id']]=$key['qty'];
		}
	}

	$model_filter=$_POST['model_filter'];
	if ($model_filter != '') {
		$tmp_array=array_map('trim',explode(PHP_EOL,$model_filter));
		//print_r($tmp_array);
		$model_filter = "'".implode("','",$tmp_array)."'";
		$sql_join.="LEFT JOIN `catalog_product_entity_varchar` cpev_model ON cpev_model.`entity_id`=cpe.`entity_id` AND cpev_model.`attribute_id` = 134 AND cpev_model.`entity_type_id` = 4 AND cpev_model.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpev_model.`value` IN (".$model_filter.") ";
		//echo $model_filter;
		//exit;
	}

	$num_in_stock_sizes=$_POST['num_in_stock_sizes'];
	if ($num_in_stock_sizes != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_varchar` cpev_num_in_stock_sizes ON cpev_num_in_stock_sizes.`entity_id`=cpe.`entity_id` AND cpev_num_in_stock_sizes.`attribute_id` = 401 AND cpev_num_in_stock_sizes.`entity_type_id` = 4 AND cpev_num_in_stock_sizes.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpev_num_in_stock_sizes.`value` = ".$num_in_stock_sizes." ";
	}

	$sort_order1=$_POST['sort_order1'];
	if ($sort_order1 != '') {
		switch ($sort_order1) {
			case "Qty_Desc":
				$sql_order_by1 = "ORDER BY tmp_qty.`qty` DESC ";
				break;
			case "Qty_Asc":
				$sql_order_by1 = "ORDER BY tmp_qty.`qty` ASC ";
				break;
			case "Created_Desc":
				$sql_order_by1 = "ORDER BY `created_at` DESC ";
				break;
			case "Created_Asc":
				$sql_order_by1 = "ORDER BY `created_at` ASC ";
				break;
			case "Price_Desc":
				$sql_order_by1 = "ORDER BY `price` DESC ";
				break;
			case "Price_Asc":
				$sql_order_by1 = "ORDER BY `price` ASC ";
				break;
			case "RRP_Desc":
				$sql_order_by1 = "ORDER BY `rrp` DESC ";
				break;
			case "RRP_Asc":
				$sql_order_by1 = "ORDER BY `rrp` ASC ";
				break;
			case "Discount_Desc":
				$sql_order_by1 = "ORDER BY `discount` DESC ";
				break;
			case "Discount_Asc":
				$sql_order_by1 = "ORDER BY `discount` ASC ";
				break;
			case "Bin_Desc":
				$sql_order_by1 = "ORDER BY `bin` DESC ";
				break;
			case "Bin_Asc":
				$sql_order_by1 = "ORDER BY `bin` ASC ";
				break;
		}
	}

	$sort_order2=$_POST['sort_order2'];
	if ($sort_order2 != '') {
		switch ($sort_order2) {
			case "Qty_Desc":
				$sql_order_by2 = ",tmp_qty.`qty` DESC ";
				break;
			case "Qty_Asc":
				$sql_order_by2 = ",tmp_qty.`qty` ASC ";
				break;
			case "Created_Desc":
				$sql_order_by2 = ",`created_at` DESC ";
				break;
			case "Created_Asc":
				$sql_order_by2 = ",`created_at` ASC ";
				break;
			case "Price_Desc":
				$sql_order_by2 = ",`price` DESC ";
				break;
			case "Price_Asc":
				$sql_order_by2 = ",`price` ASC ";
				break;
			case "RRP_Desc":
				$sql_order_by2 = ",`rrp` DESC ";
				break;
			case "RRP_Asc":
				$sql_order_by2 = ",`rrp` ASC ";
				break;
			case "Discount_Desc":
				$sql_order_by2 = ",`discount` DESC ";
				break;
			case "Discount_Asc":
				$sql_order_by2 = ",`discount` ASC ";
				break;
			case "Bin_Desc":
				$sql_order_by2 = "ORDER BY `bin` DESC ";
				break;
			case "Bin_Asc":
				$sql_order_by2 = "ORDER BY `bin` ASC ";
				break;
		}
	}

}

$sql_products = "SELECT DISTINCT ".$sql_fields." FROM `catalog_product_entity` cpe ".$sql_join." WHERE 1=1 AND ccp.`category_id` IN (2489,2530,2564,3082,3112) ".$sql_where_clause." HAVING isProduct = 'true' ".$sql_having_clause.$sql_order_by1.$sql_order_by2.$limit;

//echo $sql_products;

function build_dropdown_list($name, $attribute_id, $selected_id) {
	global $read;
	$list='<select name="'.strtolower($name).'">';
	$list.='<option value="">Select '.$name.'...</option>';
	if ($name=="Gender") {
		$list.='<option value="NULL">No Gender</option>';
	}
	$sql_stmt = "SELECT eao.`attribute_id`,eao.`option_id`,eaov.`value` FROM `eav_attribute_option` eao
				 INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id`=eao.`option_id` AND eaov.`store_id`=0
				 WHERE eao.`attribute_id` = ".$attribute_id."
				 ORDER BY eaov.`value`";
	$results_stmt = $read->fetchAll($sql_stmt);
	foreach ($results_stmt as $row) {
		$list.='<option value="'.$row['option_id'].'"'.($row['option_id']==$selected_id ? ' selected' : '').'>'.$row['value'].'</option>';
	}
	$list.='</select>';
	return $list;
}

function build_attribute_set_dropdown_list($selected_id) {
	global $read;
	$list='<select name="attribute_set[]" size="4" multiple>';
	$list.='<option value="">Select Attribute Set...</option>';
	$sql_stmt = "SELECT attribute_set_id, attribute_set_name  FROM `eav_attribute_set` WHERE `entity_type_id` = 4";
	$results_stmt = $read->fetchAll($sql_stmt);
	foreach ($results_stmt as $row) {
		$list.='<option value="'.$row['attribute_set_id'].'"'.(in_array($row['attribute_set_id'], $selected_id, true) ? ' selected' : '').'>'.$row['attribute_set_name'].'</option>';
	}
	$list.='</select>';
	return $list;
}

function build_brand_dropdown_list($selected_id) {
	global $read;
	$list='<select name="brand[]" size="4" multiple>';
	$list.='<option value="">Select Brand...</option>';
	$sql_stmt = "SELECT eao_brand.`attribute_id`, eao_brand.`option_id`, eaov_brand.`value`, sum(cisi.`qty`) qty FROM `cataloginventory_stock_item` cisi
				LEFT JOIN `catalog_product_entity_int` cpei_brand ON cpei_brand.`entity_id` = cisi.`product_id`
				INNER JOIN `eav_attribute` ea_brand ON ea_brand.`attribute_id` = cpei_brand.`attribute_id` AND ea_brand.`attribute_id` = 81
				INNER JOIN `eav_attribute_option` eao_brand ON eao_brand.`attribute_id` = ea_brand.`attribute_id` AND eao_brand.`option_id` = cpei_brand.`value`
				INNER JOIN `eav_attribute_option_value` eaov_brand ON eaov_brand.`option_id` = eao_brand.`option_id` AND eaov_brand.`store_id` = 0
				GROUP BY eaov_brand.`value`
				HAVING qty >-1";
	$results_stmt = $read->fetchAll($sql_stmt);
	foreach ($results_stmt as $row) {
		$list.='<option value="'.$row['option_id'].'"'.(in_array($row['option_id'], $selected_id, true) ? ' selected' : '').'>'.$row['value'].' ('.number_format($row['qty'],0).')</option>';
	}
	$list.='</select>';
	return $list;
}


function build_num_in_stock_sizes_dropdown_list($selected_id) {
	$list='<select name="num_in_stock_sizes" size="4">';
	$list.='<option value="">Select Number of In Stock Sizes...</option>';
	$list.='<option value="1" '.($selected_id=="1" ? ' selected' : '').'>1</option>';
	$list.='<option value="2" '.($selected_id=="2" ? ' selected' : '').'>2</option>';
	$list.='<option value="3" '.($selected_id=="3" ? ' selected' : '').'>3</option>';
	$list.='<option value="4" '.($selected_id=="4" ? ' selected' : '').'>4</option>';
	$list.='<option value="5" '.($selected_id=="5" ? ' selected' : '').'>5</option>';
	$list.='</select>';
	return $list;
}

?>

<!-- Render HTML -->

<html>
<style type="text/css">
.total_qty {
	font-size: 14px !important;
	font-weight: bold;
	text-align: center;}
.low_bundle_qty {
	font-size: 18px !important;
	font-weight: bold;
	color: #00F;	
}
select {
    border-radius: 15px 5px 5px 5px;
	-webkit-border-radius: 15px 5px 5px 5px;
	-moz-border-radius: 15px 5px 5px 5px;
	background-color: #9C3;
	color: #FFF;
	width: 200px; }
input {
	border-radius: 15px 5px 5px 5px;
	-webkit-border-radius: 15px 5px 5px 5px;
	-moz-border-radius: 15px 5px 5px 5px;
	background-color: #933;
	color: #FFF; }
.button {
	background-color: #ACF;
	color: #FFF;
	width: 100px;
	border-radius: 15px 5px 15px 5px;
	-webkit-border-radius: 15px 5px 15px 5px;
	-moz-border-radius: 15px 5px 5px 15px; }
.hvr-grow {
    display: inline-block;
    vertical-align: middle;
    transform: translateZ(0);
    box-shadow: 0 0 1px rgba(0, 0, 0, 0);
    backface-visibility: hidden;
    -moz-osx-font-smoothing: grayscale;
    transition-duration: 0.3s;
    transition-property: transform;
}

.hvr-grow:hover,
.hvr-grow:focus,
.hvr-grow:active {
    transform: scale(1.1);
}

@-webkit-keyframes hvr-buzz {
  50% {
    -webkit-transform: translateX(3px) rotate(2deg);
    transform: translateX(3px) rotate(2deg);
  }

  100% {
    -webkit-transform: translateX(-3px) rotate(-2deg);
    transform: translateX(-3px) rotate(-2deg);
  }
}

@keyframes hvr-buzz {
  50% {
    -webkit-transform: translateX(3px) rotate(2deg);
    transform: translateX(3px) rotate(2deg);
  }

  100% {
    -webkit-transform: translateX(-3px) rotate(-2deg);
    transform: translateX(-3px) rotate(-2deg);
  }
}

.hvr-buzz {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -moz-osx-font-smoothing: grayscale;
}
.hvr-buzz:hover, .hvr-buzz:focus, .hvr-buzz:active {
	background-color: #FC3;
  -webkit-animation-name: hvr-buzz;
  animation-name: hvr-buzz;
  -webkit-animation-duration: 0.15s;
  animation-duration: 0.15s;
  -webkit-animation-timing-function: linear;
  animation-timing-function: linear;
  -webkit-animation-iteration-count: infinite;
  animation-iteration-count: infinite;
}

.hvr-box-shadow-outset {
  display: inline-block;
  vertical-align: middle;
  -webkit-transform: translateZ(0);
  transform: translateZ(0);
  box-shadow: 0 0 1px rgba(0, 0, 0, 0);
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -moz-osx-font-smoothing: grayscale;
  -webkit-transition-duration: 0.3s;
  transition-duration: 0.3s;
  -webkit-transition-property: box-shadow;
  transition-property: box-shadow;
}
.hvr-box-shadow-outset:hover, .hvr-box-shadow-outset:focus, .hvr-box-shadow-outset:active {
  box-shadow: 2px 2px 2px rgba(0, 0, 0, 0.6);
}

</style>

<body>

<div align="left" style="margin-top: 15px; margin-left: 15px;">
<form id="form1" name="form1" method="post" action="stock_report18c.php">
	<table>
		<tr><td><?php echo build_dropdown_list("Source",343,$source); ?></td><td><?php echo build_dropdown_list("Gender",141,$gender); ?></td>
			<td valign="bottom"><select name="sort_order1" style="background-color: #CC0;">
				<option value="">Choose Sort Order 1...</option>
				<option value="Qty_Desc" <?php echo ($sort_order1=='Qty_Desc' ? ' selected' : ''); ?> />Quantity: Hi-Lo</option>
				<option value="Qty_Asc" <?php echo ($sort_order1=='Qty_Asc' ? ' selected' : ''); ?> />Quantity: Lo-Hi</option>
				<option value="Discount_Desc" <?php echo ($sort_order1=='Discount_Desc' ? ' selected' : ''); ?> />Discount: Best</option>
				<option value="Discount_Asc" <?php echo ($sort_order1=='Discount_Asc' ? ' selected' : ''); ?> />Discount: Worst</option>
				<option value="Price_Desc" <?php echo ($sort_order1=='Price_Desc' ? ' selected' : ''); ?> />Price: Hi-Lo</option>
				<option value="Price_Asc" <?php echo ($sort_order1=='Price_Asc' ? ' selected' : ''); ?> />Price: Lo-Hi</option>
				<option value="RRP_Desc" <?php echo ($sort_order1=='RRP_Desc' ? ' selected' : ''); ?> />RRP: Hi-Lo</option>
				<option value="RRP_Asc" <?php echo ($sort_order1=='RRP_Asc' ? ' selected' : ''); ?> />RRP: Lo-Hi</option>
				<option value="Created_Desc" <?php echo ($sort_order1=='Created_Desc' ? ' selected' : ''); ?> />Created: New-Old</option>
				<option value="Created_Asc" <?php echo ($sort_order1=='Created_Asc' ? ' selected' : ''); ?> />Created: Old-New</option>
				<option value="Bin_Desc" <?php echo ($sort_order1=='Bin_Desc' ? ' selected' : ''); ?> />Bin: Z-A</option>
				<option value="Bin_Asc" <?php echo ($sort_order1=='Bin_Asc' ? ' selected' : ''); ?> />Bin: A-Z</option>
				</select><br/><br/>
				<select name="sort_order2" style="background-color: #C0C;">
				<option value="">Choose Sort Order 2...</option>
				<option value="Qty_Desc" <?php echo ($sort_order2=='Qty_Desc' ? ' selected' : ''); ?> />Quantity: Hi-Lo</option>
				<option value="Qty_Asc" <?php echo ($sort_order2=='Qty_Asc' ? ' selected' : ''); ?> />Quantity: Lo-Hi</option>
				<option value="Discount_Desc" <?php echo ($sort_order2=='Discount_Desc' ? ' selected' : ''); ?> />Discount: Best</option>
				<option value="Discount_Asc" <?php echo ($sort_order2=='Discount_Asc' ? ' selected' : ''); ?> />Discount: Worst</option>
				<option value="Price_Asc" <?php echo ($sort_order2=='Price_Asc' ? ' selected' : ''); ?> />Price: Lo-Hi</option>
				<option value="RRP_Desc" <?php echo ($sort_order2=='RRP_Desc' ? ' selected' : ''); ?> />RRP: Hi-Lo</option>
				<option value="RRP_Asc" <?php echo ($sort_order2=='RRP_Asc' ? ' selected' : ''); ?> />RRP: Lo-Hi</option>
				<option value="Created_Desc" <?php echo ($sort_order2=='Created_Desc' ? ' selected' : ''); ?> />Created: New-Old</option>
				<option value="Created_Asc" <?php echo ($sort_order2=='Created_Asc' ? ' selected' : ''); ?> />Created: Old-New</option>
				<option value="Bin_Desc" <?php echo ($sort_order2=='Bin_Desc' ? ' selected' : ''); ?> />Bin: Z-A</option>
				<option value="Bin_Asc" <?php echo ($sort_order2=='Bin_Asc' ? ' selected' : ''); ?> />Bin: A-Z</option>
				</select>
				</td><td></td></tr>
			<tr height="40" valign="bottom"><td><?php echo build_brand_dropdown_list($brand); ?></td><td><?php echo build_attribute_set_dropdown_list($attribute_set); ?></td>
			<td align="right"><input name="name_search" type="text" maxlength="10" value = "<?php echo $name_search; ?>" /><input name="name_inclusive" type="checkbox" value="true" <?php echo ($name_inclusive=='true' ? 'checked' : ''); ?> />
			<br/>Show Sales Data (30 Days) <input name="show_sales" type="checkbox" value="true" <?php echo ($show_sales=='true' ? 'checked' : ''); ?> />
			<br/>Show Bundles Only <input name="bundles_only" type="checkbox" value="true" <?php echo ($bundles_only=='true' ? 'checked' : ''); ?> />
			<br/>Wholesale Only <input name="wholesale_only" type="checkbox" value="true" <?php echo ($wholesale_only=='true' ? 'checked' : ''); ?> />
			<br/>Start Group Only <input name="startgroup_only" type="checkbox" value="true" <?php echo ($startgroup_only=='true' ? 'checked' : ''); ?> />
			<br/>Fitness <input name="fitness" type="checkbox" value="true" <?php echo ($fitness=='true' ? 'checked' : ''); ?> />
			<br/>Sports Size <input name="sportssize" type="checkbox" value="true" <?php echo ($sportssize=='true' ? 'checked' : ''); ?> />
			<br/>SSize Not Fitness <input name="ssizenotfitness" type="checkbox" value="true" <?php echo ($ssizenotfitness=='true' ? 'checked' : ''); ?> />
			<br/>Fitness Not SSize <input name="fitnessnotssize" type="checkbox" value="true" <?php echo ($fitnessnotssize=='true' ? 'checked' : ''); ?> />
			<br/>Fitness And SSize <input name="fitnessandssize" type="checkbox" value="true" <?php echo ($fitnessandssize=='true' ? 'checked' : ''); ?> />
			<br/><?php echo build_num_in_stock_sizes_dropdown_list($num_in_stock_sizes); ?>
			</td></tr>
			<tr><td>Min Price<br /><input name="min_price" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $min_price; ?>" /></td><td>Max Price<br /><input name="max_price" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $max_price; ?>" /></td><td></td></tr>
			<tr><td>Min Discount<br /><input name="min_discount" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $min_discount; ?>" /></td><td>Max Discount<br /><input name="max_discount" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $max_discount; ?>" /></td><td></td></tr>
			<tr><td>Min Qty<br /><input name="min_qty" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $min_qty; ?>" /></td><td>Max Qty<br /><input name="max_qty" type="text" maxlength="5" style="background-color: #C0C;" value = "<?php echo $max_qty; ?>" /></td><td>Max Results<br /><input name="max_results" type="text" maxlength="5" style="background-color: #0CC;" value = "<?php echo $max_results; ?>" /></td></tr>
			<tr><td>Start Date<br /><input name="start_created_date" type="text" maxlength="10" style="background-color: #C0C;" value = "<?php echo $start_created_date; ?>" /></td><td>End Date<br /><input name="end_created_date" type="text" maxlength="10" style="background-color: #C0C;" value = "<?php echo $end_created_date; ?>" /></td><td align="right"><input class="button" type="submit" name="submit" value="Search" /></td></tr>
			<tr><td><textarea name="model_filter" style="width:250px;height:150px;"></textarea></td></tr>
	</table>
</form>
</div>
<br/><br/>


<!-- End Render HTML -->

<?php
//error_reporting(E_ALL);
//exit;
// First connect up to Mage
//require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento

//require_once('/var/www/html/app/Mage.php'); //Path to Magento
//umask(0);
//Mage::app();

//$product_name=implode('%',explode(' ',$_GET['name']));
// Get the SQL Read/Write Setup
//$resource = Mage::getSingleton('core/resource');
//$read  = $resource->getConnection('core_read');

//date_default_timezone_set("GMT");

$html .= '<style>';
$html .= '.CSSTableGenerator table { font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #d4d4d4; height: 20px; }';
$html .= '.CSSTableGenerator th { color: #FFF; background: #d4d4d4; font-weight: bold; font-size: 11px; }';
$html .= '.CSSTableGenerator td { padding-left: 5px; font-size: 10px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';

$max_option_columns=4;
$store_id = 0;
$entity_type_id = 4;
$build_html='<div class="CSSTableGenerator">';

$product_attribute_names = array("name","price","wholesale_price","msrp","actual_model","image","bin_location");

// Get Parent Product ID's
$_sql_products = "SELECT cpe.`entity_id`, cpe.`attribute_set_id`, cpe.`has_options`, IF(cpe.`type_id`='configurable' OR (cpe.`type_id`='simple' AND cpe.`has_options` = 0 AND cpsl.`parent_id` is NULL),'true','false') isProduct, cpsl.`parent_id`, tmp_qty.`qty` FROM `catalog_product_entity` cpe
				LEFT JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpev_name ON cpev_name.`entity_id`=cpe.`entity_id` AND cpev_name.`attribute_id` = 71 AND cpev_name.`entity_type_id` = ".$entity_type_id." AND cpev_name.`store_id` = ".$store_id."
				LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi
							LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl
							INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id`
							INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp_qty ON tmp_qty.`product_id`=cpe.`entity_id`
				WHERE cpev_name.`value` LIKE '%".$product_name."%'
				HAVING isProduct = 'true' AND tmp_qty.`qty` > 0 LIMIT 200";

$results_sql_products = $read->fetchAll($sql_products);

$num_products = 0;
$overall_qty = 0;
$overall_value = 0;

// Get Child ID's from Parent in $row
foreach ($results_sql_products as $row) {
	$bundle_price=($bundle_price_array[$row['entity_id']]?'<br /><br />Bundle<br />'.number_format($bundle_price_array[$row['entity_id']],2):'');
	$num_products += 1;
	$option_attribute_names = array();

	// Add Option Attribute Names to Array
	if ($row['has_options']) {
		$sql_product_attributes = "SELECT ea.`attribute_id`, ea.`attribute_code` FROM `catalog_product_super_attribute` cpsa 
									INNER JOIN `eav_attribute` ea ON ea.`attribute_id` = cpsa.`attribute_id`
									WHERE cpsa.`product_id` = ".$row['entity_id'];
		$results_sql_product_attributes = $read->fetchAll($sql_product_attributes);
		foreach ($results_sql_product_attributes as $option_attributes) {
			$option_attribute_names[]=array($option_attributes['attribute_id'],$option_attributes['attribute_code']);
		}
		$option_count = count($option_attribute_names);
	}

	$build_html.='<table class="hvr-box-shadow-outset" border="1">';
	$build_html.='<tr><th>Image</th><th>Info</th><th>Name</th><th>RRP</th>';

	if ($wholesale_only) {
		$build_html.='<th>Cost</th><th>Trade</th>';
	} else {
		$build_html.='<th>Price (<span style="color: #ff024e; font-weight: bold; font-size: 12px;">'.number_format($row['discount'],0).'%</span>)</th><th>Cost</th>';
	}

	$build_html.= '<th>Total Qty</th>';

	$a=get_attribute($row['entity_id'], $product_attribute_names);

	$image_url=$a['image'];

	//$image_name='http://www.startfitness.co.uk/media/catalog/product/'.substr($image_url,1,strrpos($image_url,'/')).'resized-100px-100px-'.substr($image_url,strrpos($image_url,'/')-strlen($image_url)+1);

	$image_name='';
	$image_found = false;

//echo '/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$fitness_image_url.$image_url.'<p>';
//echo '/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$cycles_image_url.$image_url.'<p>';
//echo '/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$football_image_url.$image_url.'<p>';


	if (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$fitness_image_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$fitness_image_url.$image_url;
		$image_found = true;
	} elseif (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$fitness_image_smushed_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$fitness_image_smushed_url.$image_url;
		$image_found = true;
	}

	if (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$sportssize_image_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$sportssize_image_url.$image_url;
		$image_found = true;
	} elseif (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$sportssize_image_smushed_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$sportssize_image_smushed_url.$image_url;
		$image_found = true;
	}

	if (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$cycles_image_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$cycles_image_url.$image_url;
		$image_found = true;
	} elseif (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$cycles_image_smushed_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$cycles_image_smushed_url.$image_url;
		$image_found = true;
	}

	if (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$football_image_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$football_image_url.$image_url;
		$image_found = true;
	} elseif (file_exists('/home/scadmin/domains/startcycles.co.uk/public_html/media/catalog/product/cache/'.$football_image_smushed_url.$image_url) && !$image_found) {
		$image_name='http://media.startfitness.co.uk/media/catalog/product/cache/'.$football_image_smushed_url.$image_url;
		$image_found = true;
	}

 /* if (!$image_found) {
	echo $image_url.'<p>';
} */

	//echo $image_name;
	//echo $image_name.'<br />';
//echo date('d/m/Y', strtotime($row['created_at']));
	//echo $row['entity_id']."<br />";
	if ($row['has_options']) {
		if ($option_count == 1) {
			$column_attribute_id = $option_attribute_names[0][0];
			$sql_product_options = "SELECT cpsl.`product_id`, eaov.`value`, cisi.`qty` FROM `catalog_product_super_link` cpsl
									INNER JOIN `catalog_product_entity_int` cpei ON cpei.`entity_id` = cpsl.`product_id` AND cpei.`attribute_id` = ".$column_attribute_id."
									INNER JOIN `eav_attribute_option` eao ON eao.`attribute_id` = cpei.`attribute_id` AND eao.`option_id` = cpei.`value`
									INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = eao.`option_id` AND eaov.`store_id` = ".$store_id."
									LEFT OUTER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id` = cpsl.`product_id`
									WHERE cpsl.`parent_id` = ".$row['entity_id']."
									ORDER BY eao.`attribute_id`, eao.`sort_order`, eaov.`value`";

			$results_sql_product_options = $read->fetchAll($sql_product_options);
			//print_r($results_sql_product_options);
			//exit;
			$total_qty = 0;
			$total_sales = 0;
			$total_sales_html = '';
			$total_value = 0;
			$i=1;
			foreach ($results_sql_product_options as $option_data_row) {
				//echo $i % $max_option_columns;
				$total_qty += $option_data_row['qty'];
				if ($show_sales) {
					$total_sales += $sales_array[$option_data_row['product_id']];
				}
				$b = get_attribute($option_data_row['product_id'], $option_attribute_names);
				$build_html.='<th>'.$option_data_row['value'].'</th>';
				$i++;
			}
			//exit;
			if ($show_sales) {
				$total_sales_html = '<br /><br />'.number_format($total_sales,0);
			}

			$total_value = $total_qty * $a['price'];
			$overall_qty += $total_qty;
			$overall_value += $total_value;
			$build_html.='</tr>';
			$build_html.='<tr><td width="100" align="center"><a href="https://admin.startfitness.co.uk/index.php/startfitnessadmin/catalog_product/edit/id/'.$row['entity_id'].'/" target="_blank"><img src="'.$image_name.'" width="100"></a></td><td width="100">'.$a['actual_model'].'<br /><br />ID: '.$a['entity_id'].'<br /><br />Date: '.date('d/m/Y', strtotime($row['created_at'])).'<br /><br />Value: '.number_format($total_value,2).'</td><td width="200">'.$a['name'].'<br /><br />Bin Location: '.$a['bin_location'].'</td><td width="50">'.number_format($a['msrp'],2).'</td>';

			if ($wholesale_only) {
				$build_html.='<td width="50">'.number_format($row['cost_price'],2).'</td><td width="50">'.number_format($a['wholesale_price'],2).'</td>';
			} else {
				$build_html.='<td width="50">'.number_format($a['price'],2).$bundle_price.'</td><td width="50">'.number_format($row['cost_price'],2).'</td>';
			}
			$build_html.='<td class="total_qty" width="50">'.number_format($total_qty,0).$total_sales_html.'</td>';

			foreach ($results_sql_product_options as $option_data_row) {
				if ($show_sales) {
					$sales_html='<br /><br />'.number_format($sales_array[$option_data_row['product_id']],0);
				}
				if ($option_data_row['qty'] < 10 AND $option_data_row['qty'] <> 0 AND $bundles_only) {
					$class="low_bundle_qty";
				} else {
					$class="";
				}
				$build_html.='<td width="20" class="'.$class.'">'.number_format($option_data_row['qty'],0).$sales_html.'</td>';
			}

		} else { // 2+ options
			$column_attribute_id = $option_attribute_names[0][0];
			$row_attribute_id = $option_attribute_names[1][0];
			//echo $column_attribute_id . ' - ' . $row_attribute_id;

			$sql_product_options = "SELECT cpsl.`product_id`, eaov_col.`value` col, eaov_row.`value` row, cisi.`qty` FROM `catalog_product_super_link` cpsl
									LEFT JOIN `catalog_product_entity_int` cpei_col ON cpei_col.`entity_id` = cpsl.`product_id` AND cpei_col.`attribute_id` = ".$column_attribute_id."
									INNER JOIN `eav_attribute_option` eao_col ON eao_col.`attribute_id` = cpei_col.`attribute_id` AND eao_col.`option_id` = cpei_col.`value`
									INNER JOIN `eav_attribute_option_value` eaov_col ON eaov_col.`option_id` = eao_col.`option_id` AND eaov_col.`store_id` = ".$store_id."
									LEFT JOIN `catalog_product_entity_int` cpei_row ON cpei_row.`entity_id` = cpsl.`product_id` AND cpei_row.`attribute_id` = ".$row_attribute_id."
									INNER JOIN `eav_attribute_option` eao_row ON eao_row.`attribute_id` = cpei_row.`attribute_id` AND eao_row.`option_id` = cpei_row.`value`
									INNER JOIN `eav_attribute_option_value` eaov_row ON eaov_row.`option_id` = eao_row.`option_id` AND eaov_row.`store_id` = ".$store_id."

									LEFT OUTER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id` = cpsl.`product_id`
									WHERE cpsl.`parent_id` = ".$row['entity_id']."
									ORDER BY eao_col.`attribute_id`, eao_col.`sort_order`, eaov_col.`value`";

			$results_sql_product_options = $read->fetchAll($sql_product_options);

			$total_qty = 0;
			$total_sales = 0;
			$total_value = 0;
			$i=1;
			foreach ($results_sql_product_options as $option_data_row) {
				//echo $i % $max_option_columns;
				$total_qty += $option_data_row['qty'];
				if ($show_sales) {
					$total_sales += $sales_array[$option_data_row['product_id']];
				}
				$b = get_attribute($option_data_row['product_id'], $option_attribute_names);
				$build_html.='<th>'.$option_data_row['col'].'<br /><i>'.$option_data_row['row'].'</i></th>';
				$i++;
			}
			if ($show_sales) {
				$total_sales_html = '<br /><br />'.number_format($total_sales,0);
			}
			$total_value = $total_qty * $a['price'];
			$overall_qty += $total_qty;
			$overall_value += $total_value;
			$build_html.='</tr>';
			$build_html.='<tr><td width="100" align="center"><a href="https://admin.startfitness.co.uk/index.php/startfitnessadmin/catalog_product/edit/id/'.$row['entity_id'].'/" target="_blank"><img src="'.$image_name.'" width="100"></a></td><td width="100">'.$a['actual_model'].'<br /><br />ID: '.$a['entity_id'].'<br /><br />Date: '.date('d/m/Y', strtotime($row['created_at'])).'<br /><br />Value: '.number_format($total_value,2).'</td><td width="200">'.$a['name'].'<br /><br />Bin Location: '.$a['bin_location'].'</td><td width="50">'.number_format($a['msrp'],2).'</td>';

			if ($wholesale_only) {
				$build_html.='<td width="50">'.number_format($row['cost_price'],2).'</td><td width="50">'.number_format($a['wholesale_price'],2).'</td>';
			} else {
				$build_html.='<td width="50">'.number_format($a['price'],2).$bundle_price.'</td><td width="50">'.number_format($row['cost_price'],2).'</td>';
			}
			$build_html.='<td class="total_qty" width="50">'.number_format($total_qty,0).$total_sales_html.'</td>';

			foreach ($results_sql_product_options as $option_data_row) {
				if ($show_sales) {
					$sales_html='<br /><br />'.number_format($sales_array[$option_data_row['product_id']],0);
				}
				if ($option_data_row['qty'] < 10 AND $option_data_row['qty'] <> 0 AND $bundles_only) {
					$class="low_bundle_qty";
				} else {
					$class="";
				}
				$build_html.='<td width="20" class="'.$class.'">'.number_format($option_data_row['qty'],0).$sales_html.'</td>';
			}


			//$build_html.='<th>Error</th>';
			//$build_html.='<tr><td width="100" align="center"><img src="'.$image_name.'"></td><td width="100">'.$a['actual_model'].'<br /><br />ID: '.$a['entity_id'].'<br /><br />Date: '.date('d/m/Y', strtotime($row['created_at'])).'</td><td width="200">'.$a['name'].'</td><td width="50">'.number_format($a['msrp'],2).'</td><td width="50">'.number_format($a['price'],2).'</td><td width="50">'.$total_qty.'</td><td>Too many dimensions for product</td>';
			//echo '<pre>'; print_r($results_sql_product_options); echo '</pre>';
			//exit;
		}
			//print_r($b);

		$build_html.='</tr>';
	} else {
		$total_value = $row['qty'] * $a['price'];
		$overall_qty += $row['qty'];
		$overall_value += $total_value;
		if ($show_sales) {
			$sales_html='<br /><br />'.number_format($sales_array[$row['entity_id']],0);
		}

		if ($row['qty'] < 10 AND $row['qty'] <> 0 AND $bundles_only) {
			$class="low_bundle_qty";
		} else {
			$class="total_qty";
		}
		$build_html.='<tr><td width="100" align="center"><a href="https://admin.startfitness.co.uk/index.php/startfitnessadmin/catalog_product/edit/id/'.$row['entity_id'].'/" target="_blank"><img src="'.$image_name.'" width="100"></a></td><td width="100">'.$a['actual_model'].'<br /><br />ID: '.$a['entity_id'].'<br /><br />Date: '.date('d/m/Y', strtotime($row['created_at'])).'<br /><br />Value: '.number_format($total_value,2).'</td><td width="200">'.$a['name'].'<br /><br />Bin Location: '.$a['bin_location'].'</td><td width="50">'.number_format($a['msrp'],2).'</td>';

		if ($wholesale_only) {
			$build_html.='<td width="50">'.number_format($row['cost_price'],2).'</td><td width="50">'.number_format($a['wholesale_price'],2).'</td>';
		} else {
			$build_html.='<td width="50">'.number_format($a['price'],2).$bundle_price.'</td><td width="50">'.number_format($row['cost_price'],2).'</td>';
		}
		$build_html.='<td class="'.$class.'" width="50">'.number_format($row['qty'],0).$sales_html.'</td></tr>';

	}
	$build_html.='</table><br /><br />';
}
$build_html.='</div><p><hr></p>';
$build_html.='<p>Product Count: '.$num_products.'</p>';
$build_html.='<p>Total Quantity: '.$overall_qty.'</p>';
$build_html.='<p>Total Value: '.number_format($overall_value,2).'</p>';
$html.=$build_html;
echo $html;

// Send the email
//Receiver Details
$sendToEmailAddress = array('gary@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');

$sendToName = 'Start Fitness';
// $mail = new Zend_Mail();
			$mail->setBodyText('');
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Product Report '.$product_name);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Product Report '.$product_name.' - '.date("D d-M-Y"));
			$mail->send();

//end
echo '</body></html>';
exit;

foreach ($results_sql_products as $row) {
	$a[$row['entity_id']]=get_attribute($row['entity_id'], $attribute_names);
	$a[$row['entity_id']]['qty']=$row['qty'];
}

$writeTurnaround .= '<div class="CSSTableGenerator" >';
$writeTurnaround .= '<table width="200" border="0" cellspacing="0" cellpadding="0">';
$writeTurnaround .= '<tr><th colspan="2" align="center"><strong>All Products</strong></th></tr>';
$writeTurnaround .= '<tr>';
foreach ($a as $row) {
	foreach (array_keys($row) as $key) {
		$writeTurnaround .= '<th>'.$key.'</th>';
	}
	break;
}
$writeTurnaround .= '</tr>';

foreach ($a as $row) {
	$writeTurnaround .= '<tr>';
	//print_r(array_keys($a));
	foreach ($row as $key => $value) {
		$writeTurnaround .= '<td>'.$value.'</td>';
	}
	$writeTurnaround .= '</tr>';
}
$writeTurnaround .= '</table></div>';

function get_attribute($entity_id, $attribute) {
	global $read, $store_id, $entity_type_id;
	$results_array = array();
	$results_array['entity_id']=$entity_id;
	foreach ($attribute as $attribute_name) {
		$sql_get_attribute = "SELECT `attribute_id`, `backend_type` FROM `eav_attribute` WHERE attribute_code = '".$attribute_name."' AND `entity_type_id` = ".$entity_type_id;
		$results_sql_get_attribute = $read->fetchAll($sql_get_attribute);

		if ($results_sql_get_attribute) {
			$attribute_id = $results_sql_get_attribute[0]['attribute_id'];
			$attribute_type = $results_sql_get_attribute[0]['backend_type'];
			switch ($attribute_type) {
			    case "datetime":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_datetime` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "decimal":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_decimal` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "int":
			    	$sql_attribute = "SELECT eaov.`value` FROM `catalog_product_entity_int` cpei INNER JOIN `eav_attribute_option_value` eaov ON eaov.`option_id` = cpei.`value` WHERE cpei.`attribute_id` = ".$attribute_id." AND cpei.`entity_id` = ".$entity_id." AND cpei.`entity_type_id` = ".$entity_type_id." AND cpei.`store_id` = ".$store_id." AND eaov.`store_id` = ".$store_id;
			        break;
			    case "media_gallery":
			    	$sql_attribute = "SELECT `value` FROM  `catalog_product_entity_media_gallery` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id;
			        break;
			    case "varchar":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_varchar` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			    case "text":
			    	$sql_attribute = "SELECT `value` FROM `catalog_product_entity_text` WHERE `attribute_id` = ".$attribute_id." AND `entity_id` = ".$entity_id." AND `entity_type_id` = ".$entity_type_id." AND `store_id` = ".$store_id;
			        break;
			}
			$results_sql_attribute = $read->fetchAll($sql_attribute);
			$results_array[$attribute_name]=$results_sql_attribute[0]['value'];
		} else {
			$results_array[$attribute_name]="NULL";
		}
	}
	return $results_array;
}



// css styling
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #F33; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #0303F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr>';
$html .= '<td align="center" valign="top">'.$writeTurnaround.'</td>';
$html .= '</tr>';
$html .= '<tr><td colspan="3" align="center"><div class="line"></div><h3>ISSUE ORDERS</h3><p>Orders below need updating/chasing/dispatching ASAP</p></td></tr>';
$html .= '<tr><td colspan="3">'.$writeUpdate.'</td></tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';


// Closing tables
$html .= '</td></tr></table>';


?>
