<html>
<head>
</head>
<!-- <link href="../gary_stock/styles.css" rel="stylesheet" type="text/css"> -->
<body>

<?php
require_once("../gary_stock/config.php");
require_once("../gary_stock/functions.php");



// GET PRODUCTS

$store_id = 0;

$sql_fields = "cpe.`entity_id`, DATE(cpe.`created_at`) created_at, cpe.`attribute_set_id`, cpe.`has_options`, IF(cpe.`type_id`='configurable' OR (cpe.`type_id`='simple' AND cpe.`has_options` = 0 AND cpsl.`parent_id` is NULL),'true','false') isProduct, cpsl.`parent_id`, tmp_qty.`qty`";
$sql_where_clause = "";
$sql_order_by1 = "";
$sql_order_by2 = "";
$sql_join = "LEFT JOIN `catalog_product_super_link` cpsl ON cpsl.`product_id`=cpe.`entity_id`
			LEFT JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty FROM `cataloginventory_stock_item` cisi
						LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty FROM `catalog_product_super_link` cpsl
							INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id`
							INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp_qty ON tmp_qty.`product_id`=cpe.`entity_id`";

if (isset($_GET)) {
	$source=$_GET['source'];
	if ($source != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`attribute_id` = 343 AND cpei_source.`entity_id` = cpe.`entity_id` AND cpei_source.`entity_type_id` = 4 AND cpei_source.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpei_source.`value` = ".$source." ";
	}

	$gender=$_GET['gender'];
	if ($gender != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_int` cpei_gender ON cpei_gender.`attribute_id` = 141 AND cpei_gender.`entity_id` = cpe.`entity_id` AND cpei_gender.`entity_type_id` = 4 AND cpei_gender.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpei_gender.`value` = ".$gender." ";
	}

	$brand=$_GET['brand'];
	if ($brand != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_int` cpei_brand ON cpei_brand.`attribute_id` = 81 AND cpei_brand.`entity_id` = cpe.`entity_id` AND cpei_brand.`entity_type_id` = 4 AND cpei_brand.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpei_brand.`value` = ".$brand." ";
	}

	$attribute_set=$_GET['attribute_set'];
	if ($attribute_set != '') {
		$sql_where_clause.="AND cpe.`attribute_set_id` IN (".implode(',',$attribute_set).") ";
	}

	$name_inclusive=$_GET['name_inclusive'];
	$name_search=$_GET['name_search'];
	if ($name_search != '') {
		$sql_join.="LEFT JOIN `catalog_product_entity_varchar` cpev_name ON cpev_name.`entity_id`=cpe.`entity_id` AND cpev_name.`attribute_id` = 71 AND cpev_name.`entity_type_id` = 4 AND cpev_name.`store_id` = ".$store_id." ";
		$sql_where_clause.="AND cpev_name.`value` ".($name_inclusive == "true" ? "" : "NOT")." LIKE '%".$name_search."%' ";
	}

}

$sql_products = "SELECT ".$sql_fields." FROM `catalog_product_entity` cpe ".$sql_join." WHERE 1=1 ".$sql_where_clause." HAVING isProduct = 'true' AND tmp_qty.`qty` > 0 ".$sql_order_by1.$sql_order_by2."LIMIT 2000";
$result_active_products = mysql_query($sql_products, $dbconnection);









// GET CATEGORIES

//echo '<table>';
$query = "SELECT ccev.`store_id`, ccev.`entity_id`, ccev.`value` FROM `catalog_category_entity_varchar` ccev WHERE ccev.`entity_type_id` = 3 AND ccev.`attribute_id` = 41 ORDER BY ccev.`store_id`, ccev.`entity_id`";
$result = mysql_query($query, $dbconnection);


$row_count=0;
$total_qty=0;
$total_value=0.00;
while($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
	$categories[$row['store_id']][$row['entity_id']]=$row['value'];
/*
	if ($row_count==0) {
		echo '<thead><tr align="left">';
		foreach($row as $col=>$val) {    
			echo "<th class=\"header_$col\">".htmlentities(strtoupper($col))."</th>";
		}
		echo '</tr></thead><tbody>';
	}
	echo '<tr class="'.iif($row_count%2 == 0, "row_colour_light", "row_colour_dark").'" align="left">';
	foreach($row as $col=>$val) {
		echo "<td class=\"data_$col\" align=\"" . iif($col=="qty","right","left") . "\">".htmlentities($val)."</td>";
		if($col=="qty") {
			$total_qty+=(int)$val;
		}
	}
	echo '</tr>';
	$row_count++;
*/
}

/*
echo '<tr></tr>';
echo "<tr><td colspan=\"7\" align=\"right\">Total Quantity: $total_qty</td></tr>";
echo '</tbody>';
echo '</table>';
*/






// LIST CATEGORIES


while($row_active_products = mysql_fetch_array($result_active_products,MYSQL_ASSOC)) {
	$query = "SELECT cpe.`entity_id`, cce.`path` FROM `catalog_product_entity` cpe INNER JOIN `catalog_category_product` ccp ON ccp.`product_id`=cpe.`entity_id` INNER JOIN `catalog_category_entity` cce ON cce.`entity_id`=ccp.`category_id` WHERE cpe.`entity_id`=".$row_active_products['entity_id']." ORDER BY cpe.`entity_id`";
	$result_products = mysql_query($query, $dbconnection);
	$num_in=0;
	$num_not_in=0;
	while($row = mysql_fetch_array($result_products,MYSQL_ASSOC)) {
		if (preg_match('/^1\/4\/75\/1674.*/', $row['path'])) {
			echo '<span style="color: #0F0">Entity = ' . $row['entity_id'] . ' -- Short Path = ' . $row['path'] . ' -- Long Path = ';
			$prod_cat = explode("/",$row['path']);

			foreach($prod_cat as $cat) {
				$path_name[] = '(' . $cat . ') ' . $categories[0][$cat];
			}
			echo $path=implode(" / ", $path_name).'</span><br />';
			unset($path_name);
			$num_in++;
		} else if (preg_match('/^1\/4\/75\/(?!1674).*/', $row['path'])) {
			echo '<span style="color: #F00">Entity = ' . $row['entity_id'] . ' -- Short Path = ' . $row['path'] . ' -- Long Path = ';
			$prod_cat = explode("/",$row['path']);

			foreach($prod_cat as $cat) {
				$path_name[] = '(' . $cat . ') ' . $categories[0][$cat];
			}
			echo $path=implode(" / ", $path_name).'</span><br />';
			unset($path_name);
			$num_not_in++;
		}
	}
	echo $num_in.' - '.$num_not_in.'<br /><br />';
	if ($num_in==0 || $num_not_in==0) {
		echo $row_active_products['entity_id'].' - MISSING FROM CATEGORY';
	}
	echo '<hr>';
}

?>

</body>
</html>

