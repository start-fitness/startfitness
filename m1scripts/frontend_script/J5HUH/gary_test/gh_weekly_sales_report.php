<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


// Get webstie ID's
$fitness = '6,19';
$websites = array("$fitness");
$sources = array("Start Fitness","Start Football","Start Cycles","Wild Trak");

$day = date("N"); // get day number
$today = date("Y-m-d"); // Todays Date mysql format
$last_week = date("Y-m-d", strtotime("-7 days")); // Last Week Date mysql format

// Else every other day of the working week
$date_from = $last_week;
$date_to = $today;

$count = 0;
$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:700px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';
$html .= '<center><p>Weekly Sales ** SOURCE ** Report run from '.$date_from.' 9:00am till '.$date_to.' 8:59am</p></center>';
// The SQL Queries
foreach($sources as $source){

// The website names
if($website == $fitness){ $web_name = "Start Fitness"; }
if($website == $football){$web_name = "Start Football"; }
if($website == $cycles){ $web_name = "Start Cycles"; }
if($website == $wildtrak){ $web_name = "WildTrak"; }
if($website == $moremile){ $web_name = "More Mile"; }
if($website == $amazon){ $web_name = "Amazon"; }
if($website == $ebay){ $web_name = "eBay"; }

// Get Data
$sql_getsales = "SELECT `cpev_model`.`value` as 'Model',`cpev_name`.`value` as 'Name',`tmp`.`Qty_Sold` 
		  FROM (SELECT IF ((SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` ORDER BY `cpsl`.`product_id` DESC LIMIT 1) >1,(
		  SELECT DISTINCT `parent_id` FROM `catalog_product_super_link` cpsl WHERE `cpsl`.`product_id` = `sfoi`.`product_id` 
		  ORDER BY `cpsl`.`product_id` DESC LIMIT 1),`sfoi`.`product_id`) parent_id, `sfoi`.`product_id`, SUM(`sfoi`.`qty_ordered`) as Qty_Sold 
		  FROM `sales_flat_order_item` sfoi LEFT JOIN `sales_flat_order` sfo ON `sfo`.`entity_id`=`sfoi`.`order_id` WHERE `sfo`.`store_id` IN (6,19) 
		  AND `sfoi`.`product_type` = 'simple' AND (`sfoi`.`created_at` >= '".$date_from." 09:00:00' AND `sfoi`.`created_at` <= '".$date_to." 08:59:59') AND `sfo`.`customer_group_id` NOT IN (2,6,7) 
		  GROUP BY `parent_id` ORDER BY Qty_Sold Desc) tmp LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_model ON `cpev_model`.`entity_id` = `tmp`.`parent_id` and `cpev_model`.`attribute_id`=134 and `cpev_model`.`store_id`=0 
		  LEFT OUTER JOIN `catalog_product_entity_varchar` cpev_name ON `cpev_name`.`entity_id` = `tmp`.`parent_id` and `cpev_name`.`attribute_id`=71 and `cpev_name`.`store_id`=0
		  LEFT JOIN `catalog_product_entity_int` cpei_source ON cpei_source.`entity_id` = `tmp`.`parent_id` INNER JOIN `eav_attribute` ea_source ON ea_source.`attribute_id` = cpei_source.`attribute_id` AND ea_source.`attribute_id` = 343 INNER JOIN `eav_attribute_option` eao_source ON eao_source.`attribute_id` = ea_source.`attribute_id` AND eao_source.`option_id` = cpei_source.`value` INNER JOIN `eav_attribute_option_value` eaov_source ON eaov_source.`option_id` = eao_source.`option_id` AND eaov_source.`store_id` = 0
		   WHERE `tmp`.`Qty_Sold` >0 AND eaov_source.`value`='".$source."' LIMIT 25";

 
// Fetch all reqults
$results_home = $read->fetchAll($sql_getsales);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<h1 style="font-family:Arial;font-size:18px;padding:10px;margin-top:20px;margin-bottom:-10px;">'.$source.' Weekly Sales **Source** Report</h1>';
$html .= '<table class="tftable" width="600" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Model #</th><th align="left">Name</th><th>Qty</th></tr>';
foreach($results_home as $home){
	$html .= '<tr><td>'.$home['Model'].'</td><td>'.$home['Name'].'</td><td align="center">'.number_format($home['Qty_Sold'],0).'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';


}

//Receiver Details
$sendToEmailAddress = array('mark@startfitness.co.uk','gary@startfitness.co.uk','gina@startfitness.co.uk');
//$sendToEmailAddress = array('michaelgrimes@startfitness.co.uk','bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','lindsayhedley@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento Weekly Sales **Source** Report');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Weekly Sales **Source** Report '.date("D d-M-Y"));
			$mail->send();

echo $html;
echo "Success";
?>