<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
//require_once('/var/www/html/app/Mage.php'); //Path to Magento
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento

umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

date_default_timezone_set("GMT");

// Set out days
$today = date("Y-m-d", strtotime("-3 years")); // Todays Date mysql format
$today1 = date("Y-m-d", strtotime("-1 weeks"));
$today2 = date("Y-m-d", strtotime("-2 weeks"));
$today3 = date("Y-m-d", strtotime("-3 weeks"));
$today4 = date("Y-m-d", strtotime("-1 months"));
$today5 = date("Y-m-d", strtotime("-3 months"));
$today6 = date("Y-m-d", strtotime("-6 months"));
$today7 = date("Y-m-d", strtotime("-1 years"));

$sql_test = "SELECT LEFT(sfoa.`postcode`,2) part_postcode, COUNT(*) num_orders FROM `sales_flat_order` sfo INNER JOIN `sales_flat_order_address` sfoa ON sfoa.`entity_id`=sfo.`billing_address_id` WHERE sfoa.`country_id` IN ('GB','GY','JE') AND sfo.`customer_group_id` <> '6' GROUP BY LEFT(sfoa.`postcode`,2) ORDER BY part_postcode ASC";

$sql_undispatched = "SELECT(SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today." 23:59:59.999999' ) AS 'Day1',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today1." 23:59:59.999999' ) AS 'Day2',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today2." 23:59:59.999999') AS 'Day3',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today3." 23:59:59.999999') AS 'Day4',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today4." 23:59:59.999999') AS 'Day5',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today5." 23:59:59.999999') AS 'Day6',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today6." 23:59:59.999999') AS 'Day6',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today7." 23:59:59.999999') AS 'Day6',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `created_at` <= '".$today6." 00:00:00.000000') AS 'DayP'";

//$sql_website = "SELECT(SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('6','22','19','15','17') ) AS 'Fitness',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('5','16','20','14') ) AS 'Football',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('1','10','21','11') ) AS 'Cycles',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('8','23') ) AS 'More',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('7') ) AS 'Wild',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('24') ) AS 'Amazon',
//			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND  `store_id` IN ('25') ) AS 'Ebay'";

//$sql_orders = "SELECT fo.entity_id,fo.increment_id,fo.store_id,fo.created_at,fo.customer_firstname,fo.customer_lastname,fo.status,maxdate
//		 FROM `sales_flat_order` fo
//         	 LEFT JOIN (SELECT tmp.entity_id, tmp.parent_id, DATEDIFF(NOW(), MAX(t.created_at)) as maxdate 
//				FROM `sales_flat_order_status_history` tmp 
//				LEFT JOIN `sales_flat_order_status_history` t ON tmp.entity_id = t.entity_id GROUP BY tmp.parent_id) 
//				AS tmp  ON fo.`entity_id`=tmp.`parent_id` 
//	     	 WHERE fo.`state` = 'pick_note_printed' 
//		 AND fo.`status` NOT LIKE 'complete'
//		 AND fo.`created_at` <= '".$today6."'
//		 GROUP BY fo.`entity_id`
//		 ORDER BY `tmp`.`maxdate`  DESC";

//$sql_dispatch = "SELECT  DATEDIFF(fs.created_at,fo.created_at) AS 'diff' , count(*) as 'orders'
//		   FROM `sales_flat_order` fo 
//		   RIGHT JOIN `sales_flat_shipment` fs ON fo.`entity_id`=fs.`order_id`
//		   WHERE fo.`created_at` IS NOT NULL
//		   AND fs.`created_at` IS NOT NULL
//		   AND fs.`created_at` >= '".$twowks."'
//		   AND fo.`customer_group_id`  NOT LIKE '6'
//		   GROUP BY diff
//		   ORDER BY `diff` ASC";


// Fetch all reqults
//$results_undispatched = $read->fetchAll($sql_undispatched);
$results_test = $read->fetchAll($sql_test);
//$results_website = $read->fetchAll($sql_website);
//$results_orders = $read->fetchAll($sql_orders);
//$results_dispatch = $read->fetchAll($sql_dispatch);
//$results_test['part_postcode'] = preg_replace('/^([a-z]{1,2})\d.*/i','$1',$results_test['part_postcode']);

$sum = 0;
foreach($results_test as $row){
	$row['part_postcode'] = strtoupper(preg_replace('/^([a-z]{1,2})\d.*/i','$1',$row['part_postcode']));
	//echo ' - '.$row['num_orders'].'<br />';
	if (preg_match('/^[a-z]{1,2}$/i', $row['part_postcode'])) {
		$new_array[$row['part_postcode']] += $row['num_orders'];
	}
}

//print_r($new_array);


// Turnaround Table
$writeTurnaround .= '<div class="CSSTableGenerator" >';
$writeTurnaround .= '<table width="200" border="0" cellspacing="0" cellpadding="0">';
$writeTurnaround .= '<tr><th colspan="2" align="center"><strong>Postcode Distribution</strong></th></tr>';
$writeTurnaround .= '<tr><th>Area</th><th>No. Orders</th></tr>';
foreach ($new_array as $key => $value) {
	$writeTurnaround .= '<tr><td>'.$key.'</td><td>'.$value.'</td></tr>';
}
$writeTurnaround .= '</table></div>';





// css styling
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #333; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #F3F3F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr>';
$html .= '<td width="200" align="center" valign="top">'.$writeTurnaround.'</td>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';


// Closing tables
$html .= '</td></tr></table>';
echo $html;

// Send the email
//Receiver Details
$sendToEmailAddress = array('hallgary@sky.com','gary@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Daily Customer Services');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Customer Services Report '.date("D d-M-Y"));
			$mail->send();

//end
echo $html;

?>