<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');


$html .= '<style type="text/css">p{font-family:Arial;font-size:12px;}.tftable {font-family:Arial;font-size:12px;color:#333333;width:900px;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}.tftable th {font-size:12px;background-color:#ACC937;border-width: 1px;padding: 8px;border-style: solid;border-color: #72A045;text-align:left;}.tftable tr {background-color:#D4E470;}.tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729F0F;}</style>';

// Get Data
$sql_getproducts = "SELECT cpev_bin.`value` bin_location, cpes.`sku`, cpev_model.`value` actual_model, cisi.`qty` FROM `catalog_product_entity` cpes
				INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id` = cpes.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpev_model ON cpev_model.`entity_id` = cpes.`entity_id` and cpev_model.`attribute_id`=134 and cpev_model.`store_id`=0 
				LEFT JOIN `catalog_product_entity_varchar` cpev_bin ON cpev_bin.`entity_id` = cpes.`entity_id` and cpev_bin.`attribute_id`=136 and cpev_bin.`store_id`=0
				WHERE cpev_bin.`value` LIKE 'AB%'
				ORDER BY cpev_bin.`value`";

// Fetch all reqults
$results = $read->fetchAll($sql_getproducts);

// Output rest of the report
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<table class="tftable" width="900" border="0" cellpadding="2" cellspacing="2">';
$html .= '<tr><th align="left">Bin #</th><th>Aisle</th><th>Column</th><th>Row</th><th>New Bin #</th><th align="left">SKU</th><th>Model</th><th>QTY</th></tr>';
$end_col=131;
$new_bin='';
foreach($results as $products){
	$aisle=substr($products['bin_location'], 0, 2);
	$col=substr($products['bin_location'], 3, 2);
	$row=substr($products['bin_location'], 6, 1);
	if ($col<=53) {
		$new_col=$end_col-($col-1);
		$new_bin=$aisle.'-'.$new_col.'-'.$row;
	}
	$html .= '<tr><td>'.$products['bin_location'].'</td><td>'.$aisle.'</td><td>'.$col.'</td><td>'.$row.'</td><td>'.$new_bin.'</td><td>'.$products['sku'].'</td><td>'.$products['actual_model'].'</td><td>'.$products['qty'].'</td></tr>';
}
$html .= '</table>';
$html .= '</td></tr></table>';

//Receiver Details
//$sendToEmailAddress = array('gary@startfitness.co.uk');
/*
$sendToEmailAddress = array('michaelgrimes@startfitness.co.uk','bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','gary@startfitness.co.uk','tony@startfitness.co.uk','lindsayhedley@startfitness.co.uk','carlsmith@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento '.$title);
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Sales Report '.date("D d-M-Y"));
			$mail->send();

*/
echo $html;
?>