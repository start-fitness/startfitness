<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0

error_reporting(E_ALL);

// First connect up to Mage
require_once('/var/www/vhosts/startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

date_default_timezone_set("GMT");

// Set out days
$today = date("Y-m-d"); // Todays Date mysql format
$today1 = date("Y-m-d", strtotime("-1 days"));
$today2 = date("Y-m-d", strtotime("-2 days"));
$today3 = date("Y-m-d", strtotime("-3 days"));
$today4 = date("Y-m-d", strtotime("-4 days"));
$today5 = date("Y-m-d", strtotime("-5 days"));
$today6 = date("Y-m-d", strtotime("-6 days"));
$todayP = date("Y-m-d", strtotime("-7 days"));
$twowks = date("Y-m-d", strtotime("-2 weeks"));

// Turnaround Stats Query
$sql_dispatch = "SELECT  DATEDIFF(fs.created_at,fo.created_at) AS 'diff' , count(*) as 'orders'
		   FROM `sales_flat_order` fo 
		   RIGHT JOIN `sales_flat_shipment` fs ON fo.`entity_id`=fs.`order_id`
		   WHERE fo.`created_at` IS NOT NULL
		   AND fs.`created_at` IS NOT NULL
		   AND fs.`created_at` >= '".$twowks."'
		   AND fo.`customer_group_id`  NOT LIKE '6'
		   GROUP BY diff
		   ORDER BY `diff` ASC";

// Undispatched Stats Query
$sql_undispatched = "SELECT(SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today." 00:00:00.000000' AND '".$today." 23:59:59.999999' ) AS 'Day1',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today1." 00:00:00.000000' AND '".$today1." 23:59:59.999999' ) AS 'Day2',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today2." 00:00:00.000000' AND '".$today2." 23:59:59.999999') AS 'Day3',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today3." 00:00:00.000000' AND '".$today3." 23:59:59.999999') AS 'Day4',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today4." 00:00:00.000000' AND '".$today4." 23:59:59.999999') AS 'Day5',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` BETWEEN '".$today5." 00:00:00.000000' AND '".$today5." 23:59:59.999999') AS 'Day6',
			      (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id` NOT LIKE '6' AND `created_at` <= '".$today6." 00:00:00.000000') AS 'DayP'";

// Undispatched Orders By Website Table
$sql_website = "SELECT(SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id`  NOT LIKE '6' AND  `store_id` IN ('6','22','19','15','17') ) AS 'Fitness',
			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id`  NOT LIKE '6' AND  `store_id` IN ('28') ) AS 'SportsSize',
			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id`  NOT LIKE '6' AND `store_id` IN ('8','23') ) AS 'More',
			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id`  NOT LIKE '6' AND  `store_id` IN ('24','26') ) AS 'Amazon',
			 (SELECT count(*) FROM `sales_flat_order` WHERE `state` = 'pick_note_printed' AND `status` NOT LIKE 'complete' AND `customer_group_id`  NOT LIKE '6' AND  `store_id` IN ('25') ) AS 'Ebay'";

// Amended query as subtracted days not calculated correctly - G.Hall 04/04/2016
$sql_dispatched = "SELECT (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE(NOW())) as 'today',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 1 DAY) AND `created_at` <= DATE(NOW())) as 'day1',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 2 DAY) AND `created_at` <= DATE_SUB(DATE(NOW()), INTERVAL 1 DAY)) as 'day2',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 3 DAY) AND `created_at` <= DATE_SUB(DATE(NOW()), INTERVAL 2 DAY)) as 'day3',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 4 DAY) AND `created_at` <= DATE_SUB(DATE(NOW()), INTERVAL 3 DAY)) as 'day4',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 5 DAY) AND `created_at` <= DATE_SUB(DATE(NOW()), INTERVAL 4 DAY)) as 'day5',
	   	     (SELECT count(*) AS 'Dispatched Today' FROM `sales_flat_shipment` WHERE `created_at` >= DATE_SUB(DATE(NOW()), INTERVAL 6 DAY) AND `created_at` <= DATE_SUB(DATE(NOW()), INTERVAL 5 DAY)) as 'day6'";


// Bottom Issue Order Table // GH (UPDATE - REPORT AFTER 3 DAYS NOT 6)
$sql_orders = "SELECT fo.entity_id,fo.increment_id,fo.store_id,fo.created_at,fo.customer_firstname,fo.customer_lastname,fo.status,maxdate
		 FROM `sales_flat_order` fo
         	 LEFT JOIN (SELECT tmp.entity_id, tmp.parent_id, DATEDIFF(NOW(), MAX(t.created_at)) as maxdate 
				FROM `sales_flat_order_status_history` tmp 
				LEFT JOIN `sales_flat_order_status_history` t ON tmp.entity_id = t.entity_id GROUP BY tmp.parent_id) 
				AS tmp  ON fo.`entity_id`=tmp.`parent_id` 
	     	 WHERE fo.`state` = 'pick_note_printed' 
		 AND fo.`status` NOT IN ('complete','wholesale_order','pending_auth')
		 AND fo.`store_id` <> 23
		 AND (fo.`customer_firstname` != 'Wholesale -' AND fo.`customer_lastname` != 'Cramlington')
 		 AND fo.`created_at` <= '".$today3."'
		 GROUP BY fo.`entity_id`
		 ORDER BY tmp.`maxdate` DESC";


// Fetch all results
$results_undispatched = $read->fetchAll($sql_undispatched);
$results_website = $read->fetchAll($sql_website);
$results_orders = $read->fetchAll($sql_orders);
$results_dispatch = $read->fetchAll($sql_dispatch);
$results_dispatched = $read->fetchAll($sql_dispatched);

//print_r($results_dispatch);

$sum = 0;
foreach($results_dispatch as $count){
	if($count['diff'] >= '6'){
		$sum+=$count['orders'];
	}else{
		continue;
	}
}

// Dispatched Table
$writeDispatched .= '<div class="CSSTableGenerator" >';
$writeDispatched .= '<table width="160" border="0" cellspacing="0" cellpadding="0">';
$writeDispatched .= '<tr><th colspan="2" align="center"><strong>Dispatched</strong></th></tr>';
$writeDispatched .= '<tr><th>Days</th><th>Orders</th></tr>';
$writeDispatched .= '<tr><td>Today</td><td>'.$results_dispatched[0]['today'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-1 day")).'</td><td>'.$results_dispatched[0]['day1'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-2 day")).'</td><td>'.$results_dispatched[0]['day2'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-3 day")).'</td><td>'.$results_dispatched[0]['day3'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-4 day")).'</td><td>'.$results_dispatched[0]['day4'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-5 day")).'</td><td>'.$results_dispatched[0]['day5'].'</td></tr>';
$writeDispatched .= '<tr><td>'.date("d-m-y", strtotime("-6 day")).'</td><td>'.$results_dispatched[0]['day6'].'</td></tr>';
$writeDispatched .= '</table></div>';

// Turnaround Table
$writeTurnaround .= '<div class="CSSTableGenerator" >';
$writeTurnaround .= '<table width="160" border="0" cellspacing="0" cellpadding="0">';
$writeTurnaround .= '<tr><th colspan="2" align="center"><strong>2w Turnaround Avg</strong></th></tr>';
$writeTurnaround .= '<tr><th>Days</th><th>Orders</th></tr>';
$writeTurnaround .= '<tr><td>0 - 1 Days</td><td>'.$results_dispatch[0]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>1 - 2 Days</td><td>'.$results_dispatch[1]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>2 - 3 Days</td><td>'.$results_dispatch[2]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>3 - 4 Days</td><td>'.$results_dispatch[3]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>4 - 5 Days</td><td>'.$results_dispatch[4]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>5 - 6 Days</td><td>'.$results_dispatch[5]['orders'].'</td></tr>';
$writeTurnaround .= '<tr><td>>6 Days</td><td>'.$sum.'</td></tr>';
$writeTurnaround .= '</table></div>';

//Undispatched Table
// Turnaround Table
$writeUndispatched .= '<div class="CSSTableGenerator" >';
$writeUndispatched .= '<table width="160" border="0" cellspacing="0" cellpadding="0">';
$writeUndispatched .= '<tr><th colspan="2" align="center"><strong>Undispatched Stats</strong></th></tr>';
$writeUndispatched .= '<tr><th>Days</th><th>Orders</th></tr>';
$writeUndispatched .= '<tr><td>0 - 1 Day</td><td>'.$results_undispatched[0]['Day1'].'</td></tr>';
$writeUndispatched .= '<tr><td>1 - 2 Days</td><td>'.$results_undispatched[0]['Day2'].'</td></tr>';
$writeUndispatched .= '<tr><td>2 - 3 Days</td><td>'.$results_undispatched[0]['Day3'].'</td></tr>';
$writeUndispatched .= '<tr><td>3 - 4 Days</td><td>'.$results_undispatched[0]['Day4'].'</td></tr>';
$writeUndispatched .= '<tr><td>4 - 5 Days</td><td>'.$results_undispatched[0]['Day5'].'</td></tr>';
$writeUndispatched .= '<tr><td>5 - 6 Days</td><td>'.$results_undispatched[0]['Day6'].'</td></tr>';
$writeUndispatched .= '<tr><td>>6 Days</td><td class="red">'.$results_undispatched[0]['DayP'].'</td></tr>';
$writeUndispatched .= '</table></div>';

//Undispatched Website
// Turnaround Table
$writeWebsite .= '<div class="CSSTableGenerator" >';
$writeWebsite .= '<table width="160" border="0" cellspacing="0" cellpadding="0">';
$writeWebsite .= '<tr><th colspan="2" align="center"><strong>Unshipped Orders</strong></th></tr>';
$writeWebsite .= '<tr><th>Website</th><th>Orders</th></tr>';
$writeWebsite .= '<tr><td>Fitness</td><td>'.$results_website[0]['Fitness'].'</td></tr>';
$writeWebsite .= '<tr><td>Sports Size</td><td>'.$results_website[0]['SportsSize'].'</td></tr>';
$writeWebsite .= '<tr><td>More Mile</td><td>'.$results_website[0]['More'].'</td></tr>';
$writeWebsite .= '<tr><td>Amazon</td><td>'.$results_website[0]['Amazon'].'</td></tr>';
$writeWebsite .= '<tr><td>eBay</td><td>'.$results_website[0]['Ebay'].'</td></tr>';
$writeWebsite .= '</table></div>';

//No Update Table
// Turnaround Table
$writeUpdate .= '<div class="CSSTableGenerator" >';
$writeUpdate .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
$writeUpdate .= '<tr><th>Order</th><th>Website</th><th>Date</th><th>Name</th><th>Status</th><th> Updated</th></tr>';
foreach($results_orders as $orders){
	$website_id = $orders['store_id'];
	     if($website_id == '6' || $website_id == '19'){$source_id = "Start Fitness"; } // Start Fitness
	else if($website_id == '28'){$source_id = "Sports Size";  } // Sports Size
	else if($website_id == '1' || $website_id == '21'){$source_id = "Start Cycles";  } // Start Cycles
	else if($website_id == '5' || $website_id == '20'){$source_id = "Start Football";} // Start Football
	else if($website_id == '8'){$source_id = "More Mile";     } // More Mile
	else if($website_id == '7'){$source_id = "Wild Trak";     } // Wild Trak
	else if($website_id == '25'){$source_id = "Ebay";     	  } // EBay
	else if($website_id == '24' || $website_id == '26'){$source_id = "Amazon";       } // Amazon

	$writeUpdate .= '<tr><td><a href="https://admin.startfitness.co.uk/index.php/startfitnessadmin/sales_order/view/order_id/'.$orders['entity_id'].'" target="_blank">'.$orders['increment_id'].'</a></td><td>'.$source_id.'</td><td>'.$orders['created_at'].'</td><td>'.$orders['customer_firstname'] .' ' . $orders['customer_lastname'].'</td><td>'.$orders['status'].'</td><td>'.$orders['maxdate'].' Days Ago</td></tr>';
}
$writeUpdate .= '</table>';
$writeUpdate .= '</div>';




// css styling
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #333; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #F3F3F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr>';
$html .= '<td width="200" align="center" valign="top">'.$writeDispatched.'</td>';
$html .= '<td width="200" align="center" valign="top">'.$writeTurnaround.'</td>';
$html .= '<td width="200" align="center" valign="top">'.$writeUndispatched.'</td>';
$html .= '<td width="200" align="center" valign="top">'.$writeWebsite.'</td>';
$html .= '</tr>';
$html .= '<tr><td colspan="4" align="center"><div class="line"></div><h3>ISSUE ORDERS</h3><p>Orders below need updating/chasing/dispatching ASAP</p></td></tr>';
$html .= '<tr><td colspan="4">'.$writeUpdate.'</td></tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';


// Closing tables
$html .= '</td></tr></table>';

// Send the email
//Receiver Details
$sendToEmailAddress = array('bradlyillingworth@startfitness.co.uk','gina@startfitness.co.uk','michaeltaggart@startfitness.co.uk','michaelamillican@startfitness.co.uk','gary@startfitness.co.uk','mark@startfitness.co.uk','davidquinn@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Daily Customer Services');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Customer Services Report '.date("D d-M-Y"));
			$mail->send();

//end
echo $html;

?>