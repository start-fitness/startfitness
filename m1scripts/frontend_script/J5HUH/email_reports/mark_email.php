<?php
// Start Group Script To Email Mark New Email Addresses
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/var/www/vhosts/startfitness.co.uk/htdocs/app/Mage.php'); //Path to Magento
umask(0);
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

$today = date("Y-m-d"); // Todays Date mysql format
$last_week = date("Y-m-d", strtotime("-2 weeks")); // Last Week Date mysql format
$count = 0;

// Get Data
$sql_getnewsletter = "SELECT ns.`subscriber_id` AS 'id' ,ns.`subscriber_email` AS 'email',csg.`name` AS 'website',caev.`value` AS 'firstname',caev1.`value` AS 'lastname' 
			 FROM `newsletter_subscriber` ns
			 LEFT JOIN `core_store` cs ON ns.`store_id`=cs.`store_id`
			 LEFT JOIN `core_store_group` csg ON cs.`group_id`=csg.`group_id`
			 LEFT JOIN `customer_entity_varchar` caev ON ns.`customer_id`=caev.`entity_id` AND caev.`attribute_id` = '5'
			 LEFT JOIN `customer_entity_varchar` caev1 ON ns.`customer_id`=caev1.`entity_id` AND caev1.`attribute_id` = '7'";

$sql_getcustomer = "SELECT DISTINCT(o.customer_email),o.customer_firstname, o.customer_lastname, csg.name, a.country_id 
		      FROM `sales_flat_order` o 
		      LEFT JOIN `core_store` cs ON o.`store_id`=cs.`store_id`
		      LEFT JOIN `core_store_group` csg ON cs.`group_id`=csg.`group_id`
		      RIGHT JOIN `sales_flat_order_address` a on o.`entity_id`=a.`parent_id` WHERE o.`customer_email` NOT LIKE '%marketplace%' AND o.`customer_email` NOT LIKE  '%@startcycles.co.uk' 
		      AND o.`customer_email` NOT LIKE '%@startfitness.co.uk' AND o.`customer_email` NOT LIKE '%@startfootball.co.uk' AND o.`customer_email` NOT LIKE '%@amazon' 
		      AND a.address_type= 'billing' AND o.`created_at` >= '".$last_week." 00:00:00' GROUP BY(customer_email)";

// Fetch all reqults
$results_getnewsletter = $read->fetchAll($sql_getnewsletter);
$results_getcustomer = $read->fetchAll($sql_getcustomer);

// Output newsletter People To CSV
unlink("/var/www/vhosts/startfitness.co.uk/htdocs/script/J5HUH/email_reports/newsletter.csv");
$fp = fopen('/var/www/vhosts/startfitness.co.uk/htdocs/script/J5HUH/email_reports/newsletter.csv', 'w');
$csvHeader = array("id", "email","website","firstname","lastname");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getnewsletter as $newsletter){
    fputcsv($fp, array($newsletter['id'], $newsletter['email'], $newsletter['website'], $newsletter['firstname'], $newsletter['lastname']), ",");
}
fclose($fp);

// Output Customers To CSV
unlink("/var/www/vhosts/startfitness.co.uk/htdocs/script/J5HUH/email_reports/customers.csv");
$fp = fopen('/var/www/vhosts/startfitness.co.uk/htdocs/script/J5HUH/email_reports/customers.csv', 'w');
$csvHeader = array("email", "firstname","lastname","website","country");
fputcsv( $fp, $csvHeader,",");
foreach ($results_getcustomer as $customer){
    fputcsv($fp, array($customer['customer_email'], $customer['customer_firstname'], $customer['customer_lastname'], $customer['name'], $customer['country_id']), ",");
}
fclose($fp);


//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
$sendToEmailAddress = array('gary@startfitness.co.uk','mark@startfitness.co.uk');

$sendToName = 'Start Fitness';
$message = "Bi-Weekly Email Lists";
$html = "Hi Mark, <br><br> This is your latest emails from Magento.<br><br>These are newsletter subscribers, and customers who have placed orders on any of the websites.<br><br>See you again in two weeks time.";
$mail = new Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Magento Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Weekly Email Export '.date("D d-M-Y"));

	$dir = Mage::getBaseDir();
	$path = $dir.DS.'script'.DS.'J5HUH'.DS.'email_reports'.DS.'newsletter.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'newsletter.csv';

	$path = $dir.DS.'script'.DS.'J5HUH'.DS.'email_reports'.DS.'customers.csv';
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'customers.csv';

		try {
    			$mail->send();
    			Mage::getSingleton('core/session')->addSuccess('Your request has been sent');
		}
		catch (Exception $e) {
    			Mage::getSingleton('core/session')->addError('Unable to send.');
		}

echo "Success";
?>