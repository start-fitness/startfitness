<?php
// Start Group Script To Populate Percentage Savings
// Designed and built by James Simpson
// V1.0

error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

$count = 0;
$count1 = 0;

// The SQL Queries
$sql_getprod = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,csi.is_in_stock,cpev.value as 'finance'
			FROM `catalog_product_entity` cpe 
			RIGHT JOIN `cataloginventory_stock_item` csi ON cpe.`entity_id`=csi.`product_id`
			RIGHT JOIN `catalog_product_entity_varchar`cpev ON cpe.`entity_id`=cpev.`entity_id`
			WHERE cpev.`attribute_id` = 365
			AND cpe.`attribute_set_id` = 9 
			AND cpev.`store_id` = 0
			AND csi.`is_in_stock` = 1
			AND cpev.value IS NOT NULL
			ORDER BY `cpe`.`entity_id` ASC";

// Fetch all reqults
$results = $read->fetchAll($sql_getprod);

// Print out all results
//print_r($results);

// Now Insert the data
foreach($results as $value){
	$count++;
	$insert = NULL;
	$finance_options = explode(",",$value['finance']);

	// Insert Percentage Savings
	// IFF6
	// IFF12
	// IFF24
	// CC12w19q5 - Credit 12 19.5%
	// CC24w19q5 - Credit 24 19.5%

	// Get each selected option
	$len = count($finance_options);
	foreach($finance_options as $index => $option){
	//$insert = NULL;
		if($option==='3990'){ $data = " `IFF6` = '1'"; }
		elseif($option=='3989'){ $data = " `IFF12` = '1'"; }
		elseif($option=='3988'){ $data = " `IFF24` = '1'"; }
		elseif($option=='3987'){ $data = " `CC12w19q5` = '1'"; }
		elseif($option=='3986'){ $data = " `CC24w19q5` = '1'"; }
		
		if ($index == $len - 1) { // Dont need a comma on the last element
        		$insert .= $data;
    		}else{
			$insert .= $data .',';
		}
	}

	try{
		$sql_reset = "UPDATE `v12prodtoprod` SET `IFF6` = '0', `IFF12` = '0', `IFF24` = '0', `CC12w19q5` = '0', `CC24w19q5` = '0' WHERE `product_entity_id` = ".$value['entity_id']." ";
		$sql_insert = "UPDATE `v12prodtoprod` SET ".$insert."  WHERE `product_entity_id` = ".$value['entity_id']." ";
		echo $sql_reset . "<br>";
		echo $sql_insert . "<br>";
		$write->query($sql_reset); // write updates to database
		$write->query($sql_insert); // write updates to database
	}
	catch(Exception $e){
		echo "Message:" . $e->getMessage();
	}
	
	
	$count1++;
}

$html .= "Total Finance Products (".$count.") - Updated Percentages (".$count1.")"; 
echo $html;

//Receiver Details
$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gina@startfitness.co.uk','lori@startfitness.co.uk');
$sendToName = 'Magento Server';

/*$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Percentage Savings Cron');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Percentage Savings '.date("F j, Y, g:i a"));
			$mail->send();*/

echo "<br>Completed successfully <br>";
?>