<?php
// Start Group Script To Populate Percentage Savings
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

// Get webstie ID's
//fitness = 3
//football = 2
//cycles = 1
//wildtrak = 4
$websites = array("3","2","1","4"); // Live website

$count = 0;
$count1 = 0;

// The SQL Queries - This is the backend product details
$sql_getprod = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,cped.value as 'price',cped2.value as 'RRP',cped1.value as 'percent',cpei.value as 'visability', csi.is_in_stock 
			FROM `catalog_product_entity` cpe RIGHT JOIN `catalog_product_entity_decimal` cped ON cpe.`entity_id`=cped.`entity_id` 
			LEFT JOIN `catalog_product_entity_decimal` cped1 ON cpe.`entity_id`=cped1.`entity_id` 
			LEFT JOIN `catalog_product_entity_decimal` cped2 ON cpe.`entity_id`=cped2.`entity_id` 
			LEFT JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id` 
			LEFT JOIN `cataloginventory_stock_item` csi ON cpe.`entity_id`=csi.`product_id`
			WHERE cped.`attribute_id` = 75 
			AND cped1.`attribute_id` = 338
			AND cped2.`attribute_id`= 120 
			AND cpei.`attribute_id` = 102 
			AND cpei.`value` = 4 
			AND cped1.`store_id`= 0 
			AND cped2.`store_id`= 0 
			AND cped.`store_id` = 0 
			AND cpei.`store_id` = 0 
			AND csi.`is_in_stock` = 1
			ORDER BY cped1.`value` ASC";

// This is the index table - for some reason products arent being updated?
$sql_getprod1 = "SELECT cpe.entity_id,cpe.type_id,cpe.sku,cpe.price,cpe.msrp as 'RRP',cpe.percentage_saving as 'percent',cpe.visibility as 'visability' 
			FROM `catalog_product_flat_1` cpe
			JOIN `catalog_product_entity_int` cpei ON cpe.`entity_id`=cpei.`entity_id`
			WHERE cpei.`attribute_id` = 102 
			AND cpei.`value` = 4 
			AND cpei.`store_id` = 0 
			ORDER BY cpe.percentage_saving ASC";

// Fetch all reqults
//$results = $read->fetchAll($sql_getprod);
$results = $read->fetchAll($sql_getprod1);

// Print out all results
//print_r($results);

// Defaults
$number = 0;
$rrpset = 0;

// Set the table up to display issue products - if any
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center">';
$html .= '<center><h2>Product Issue Table</h2></center>';
$html .= '<table style="border:1px #000 solid;" width="600" border="1" cellspacing="2" cellpadding="2"><tr><th>ID</th><th>Model #</th><th>Price</th><th>RRP</th></tr>';

// Now Insert the data
foreach($results as $value){
	$count++;
	if($value['RRP'] == NULL){ // No RRP Set
		$html .= '<tr><td>'.$value['entity_id'].'</td><td>'.$value['sku'].'</td><td>�'.number_format($value['price'],2).'</td><td>-</td></tr>';
		$rrpset++;
	}else{ // Else work out the RRP/Percentage Savings
		$savings_percentage = round((($value['RRP'] - $value['price'])/$value['RRP'])*100, 0 );
		$current_percentage = round($value['percent'], 0);
	}

	if(round($savings_percentage,0) == round($current_percentage)){ // If the current percent is fine, skip it
		$count_correct++;
		continue;
	}else if(round($savings_percentage) <= 0){ // Product Price bigger than RRP
		$savings_percentage = 0.00;
		$html .= '<tr><td>'.$value['entity_id'].'</td><td>'.$value['sku'].'</td><td>�'.number_format($value['price'],2).'</td><td>�'.number_format($value['RRP'],2).'</td></tr>';
		$rrpset++;
		$count1--;
	}else if($savings_percentage != $current_percentage){ // These Percentages need updating
		$savings_percentage = $savings_percentage;
	}
	// Insert Percentage Savings
	//$sql_insert = "UPDATE `catalog_product_entity_decimal` SET `value` = '".$savings_percentage."' WHERE `attribute_id` = 338 AND `store_id` = 0 AND `entity_id` = ".$value['entity_id']." ";
	$sql_insert = "UPDATE `catalog_product_flat_1` SET `percentage_saving` = '".$savings_percentage."' WHERE `entity_id` = ".$value['entity_id']." ";
	//echo $sql_insert . "<br>";

	$write->query($sql_insert); // write updates to database
	$count1++ ;
}
// If all RRPs are fine - show this cell
if($rrpset == '0'){ $html .= '<tr><td colspan="4" align="center">No Problem Products</td></tr>'; }

$html .= '</table>';

$html .= "Total Active Products (".$count.") - No Update Required (".$count_correct.") - Updated Percentages (".$count1.")"; 
$html .= "</table>";
echo $html;

//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gina@startfitness.co.uk','lori@startfitness.co.uk');
$sendToName = 'Magento Server';

$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Percentage Savings Cron');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Percentage Savings '.date("F j, Y, g:i a"));
			$mail->send();

echo "Completed successfully <br>";
?>