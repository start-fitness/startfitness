<?php

function iif($condition,$true,$false) {
	return ($condition) ? $true : $false;
}

function build_dropdown1($query,$name,$selected,$dbconnection) {
	if (isset($_POST["website"])) {
		$website=mysql_real_escape_string($_POST["website"]);
	} else {
		$website="";
	}
	$result = mysql_query($query, $dbconnection);	
	
	$options[0] = "<select id=\"$name\" name=\"$name\">";
	$options[0] .= "<option value=\"\">Select $name</option>";
	
	while ($row = mysql_fetch_array($result,MYSQL_BOTH)) {	 
		$options[0] .= '<option value="'.$row[0].'"' . iif($website==$row[0],'selected="selected"','') . '>'.htmlentities($row[1]).'</option>';
	}
	$options[0] .= "</select>";
	
	return $options[0];
}

function build_dropdown($name,$attribute_id,$dbconnection) {
	$query = "SELECT eaov.`option_id`, eaov.`value` FROM `eav_attribute_option` eao INNER JOIN `eav_attribute_option_value` eaov ON eaov.option_id = eao.`option_id` AND eao.`attribute_id` = $attribute_id AND eaov.`store_id` = 0 ORDER BY eaov.`value`";
	$result = mysql_query($query, $dbconnection);	
	
	$options = "<select name=\"$name\">";
	$options .= "<option value=\"\">Select $name</option>";
	
	while ($row = mysql_fetch_array($result,MYSQL_ASSOC)) {	 
		$options .= '<option value="'.$row['value'].'">'.htmlentities($row['value']).'</option>';
	}
	$options .= "</select>";
	
	return $options;
}

function build_attribute_set_dropdown($dbconnection) {
	$query = "SELECT eas.`attribute_set_id`, eas.`attribute_set_name` FROM `eav_attribute_set` eas WHERE eas.`entity_type_id` = 4 ORDER BY eas.`attribute_set_name`";
	$result = mysql_query($query, $dbconnection);	
	$name = 'Attribute Set';
	$options = "<select name=\"$name\">";
	$options .= "<option value=\"\">Select $name</option>";
	
	while ($row = mysql_fetch_array($result,MYSQL_ASSOC)) {	 
		$options .= '<option value="'.$row['attribute_set_name'].'">'.htmlentities($row['attribute_set_name']).'</option>';
	}
	$options .= "</select>";
	
	return $options;
}
?>