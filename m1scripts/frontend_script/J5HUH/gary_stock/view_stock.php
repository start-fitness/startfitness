<html>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="view_stock.js"></script>
</head>
<link href="styles.css" rel="stylesheet" type="text/css">
<body>

<noscript>
  This page needs JavaScript activated to work. 
  <style>div { display:none; }</style>
</noscript>

<?php
require_once("config.php");
require_once("functions.php");

if (isset($_POST["website"])) {
	$website=mysqli_real_escape_string($dbconnection, $_POST["website"]);
} else {
	$website="";
}
$model_search="";
if (isset($_POST["model"])) {
	$model_search=mysqli_real_escape_string($dbconnection, $_POST["model"]);
}
if (isset($_POST["store"])) {
	$store=mysqli_real_escape_string($dbconnection, $_POST["store"]);
}
?>

<form action="" method="post" name="myForm">
    <!--Website :-->
	<?php //echo build_dropdown1("SELECT cw.`website_id`, cw.`name` website_name FROM `core_website` cw WHERE cw.`website_id` <>6 ORDER BY cw.`name`","website",$website,$dbconnection); ?>
    <div id="store1" style="display:none">
    Store :
    <select name="store" id="store">
    </select>
    </div>
    <div id="d1"><br />
      <label for="model">Model</label>
      <input type="text" name="model" id="model">
	  <button name="clear" value="RESET" type="reset">CLEAR</button>
    </div><br />
   	<button id="toggle_entity_id" name="toggle_entity_id" type="button" value ="" style="padding-left: 10px;">Entity OFF</button>
   	<button id="toggle_name" name="toggle_name" type="button" value ="" style="padding-left: 10px;">Name OFF</button>
   	<button id="toggle_bin" name="bin" type="button" value ="" style="padding-left: 10px;">Bin OFF</button>
   	<button id="toggle_rrp" name="rrp" type="button" value ="" style="padding-left: 10px;">RRP OFF</button>
   	<button id="toggle_price" name="price" type="button" value ="" style="padding-left: 10px;">Price OFF</button>
   	<button id="toggle_model" name="toggle_model" type="button" value ="" style="padding-left: 10px;">Model OFF</button>
   	<button id="toggle_qty" name="toggle_qty" type="button" value ="" style="padding-left: 10px;">Qty OFF</button><p>
</form>
<p>
<div id="results"></div>

</body>
</html>

