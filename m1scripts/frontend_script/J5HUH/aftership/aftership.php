<?php
// Start Group Script To Create Aftership CSV
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

// Set out days
$yesterday = date("Y-m-d", strtotime("-1 days")); // Todays Date mysql format

// The SQL query
$sql_dispatched = "SELECT st.track_number,sg.shipping_name,sg.order_increment_id FROM `sales_flat_shipment_track` st
LEFT JOIN `sales_flat_shipment_grid` sg ON st.`parent_id`=sg.`entity_id`
WHERE st.`created_at` > '".$yesterday."' AND st.`track_number` NOT LIKE 'NOT%' ";

// Fetch all reqults
$results_dispatched = $read->fetchAll($sql_dispatched);


//print_r($results_dispatch);

// File headers
$headings = array('tracking_number','customer_name','order_id');

// open the "output" stream
$f = fopen('afterchip.csv', 'w');

// Start output buffering (to capture stream contents)
ob_start();

// print out the headers
fputcsv($f, $headings);

foreach ($results_dispatched as $line) {
	fputcsv($f, $line);
	//echo $line;
}

echo "CSV Built Successfully";

?>