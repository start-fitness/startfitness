<?php
// Mark DHL Report
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
Mage::setIsDeveloperMode();
date_default_timezone_set("GMT");

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

// Set out days
$today = date("Y-m-d"); // Todays Date mysql format
$today1 = date("Y-m-d", strtotime("-1 days"));

// Regions
// EU Member States
$EU  = array("AT","BE","BG","HR","CY","CZ","DK","EE","FI","FR","DE","GI","GR","HU","IE","IT","LV","LT","LU","MT","NL","PL","PT","RO","SK","SI","ES","SE");

// Countries not in EU but in Europe
$EUP = array("AX","BY","BA","IS","MK","MD","MC","NO","CH","TR","TM","UA");

// Rest Of World countries
$ROW = array("AF","AL","DZ","AS","AD","AO","AI","AQ","AG","AR","AM","AW","AU","AZ","BS","BH","BD","BB","BZ","BJ","BM","BT","BO","BW","BV","BR","IO","VG","BN","BF","BI","KH","CM","CA","CV","KY","CF","TD","CL","CN","CX","CC","CO","KM","CD","CG","CK","CR","CI","CU","DJ","DM","DO","EC","EG","SV","GQ","ER","ET","FK","FO","FJ","GF","PF","TF","GA","GM","GE","GH","GL","GD","GP","GU","GT","GN","GW","GY","HT","HM","HN","HK","IN","ID","IR","IQ","IL","JM","JP","JO","KZ","KE","KI","KW","KG","LA","LB","LS","LR","LY","LI","MO","MG","MW","MY","MV","ML","MH","MQ","MR","MU","YT","MX","FM","MN","ME","MS","MA","MZ","MM","NA","NR","NP","AN","NC","NZ","NI","NE","NG","NU","NF","KP","MP","OM","PK","PW","PS","PA","PG","PY","PE","PH","PN","PR","QA","RE","RU","RW","BL","SH","KN","LC","MF","PM","VC","WS","SM","ST","SA","SN","RS","SC","SL","SG","SB","SO","ZA","GS","KR","LK","SD","SR","SJ","SZ","SY","TW","TJ","TZ","TH","TL","TG","TK","TO","TT","TN","TC","TV","UM","VI","UG","AE","US","UY","UZ","VU","VA","VE","VN","WF","EH","YE","ZM","ZW");

$sql_tracking = "SELECT sfoa.region,sfoa.postcode,sfoa.firstname,sfoa.lastname,sfoa.city,sfoa.email,sfoa.country_id,sfst.track_number,sfst.title,sfst.carrier_code,sfst.created_at,sfst.updated_at 
			FROM `sales_flat_order_address` sfoa
			RIGHT JOIN `sales_flat_shipment_track` sfst ON sfoa.`parent_id`=sfst.`parent_id`
			WHERE sfoa.`address_type` = 'shipping'";


// Fetch all reqults
//$results_tracking = $read->fetchAll($sql_tracking);
print_r($results_tracking);




?>

<html>
  <head>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'By Courier'],
          ['DHL',    11],
          ['RM',     2],
          ['DPD',  	2],
          ['CL', 	2],
          ['Other',	7]
        ]);

        var options = {
          title: 'ROW Del',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="donutchart" style="width: 900px; height: 500px;"></div>
  </body>
</html>