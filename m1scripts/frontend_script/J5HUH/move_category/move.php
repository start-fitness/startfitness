<?php
// Turn On Error Reporting
error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit', '10048M');
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);

// Require Mage
require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
Mage::app();
?>

<head>
<title>Category Mover - James Simpson</title>
<style type="text/css">
#container {
	width: 980px;
	margin-top: 10px;
	margin-bottom: 10px;
	padding: 10px;
	margin-right: auto;
	margin-left: auto;
	background-color:#FFF;
}
#head {
	height: 100px;	
}
#content {
	padding-top:10px;
	padding-bottom: 10px;	
}
#footer {
	text-align:center;
	font-size:10px;	
	margin-top: 1px solid #CCC;
}
body,td,th {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 12px;
	color: #000;
	border: 1px solid black;
}
table {
    border-collapse: collapse;
}
table, td, th {
    border: 1px solid green;
	padding: 3px;
}

th {
    background-color: green;
    color: white;
}
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	background-color: #999;
}
.whiteBackground {background-color: #fff;}
.grayBackground {background-color: #eee;}
</style>
</head>

<body>

<?php
	// Setup Database Connection
	$username = "fitness";
	$password = "PK4LLCUz";
	$hostname = "193.200.192.53"; 

	//connection to the database
	$dbhandle = mysql_connect($hostname, $username, $password) or die("Unable to connect to MySQL");
	//select a database to work with
	$selected = mysql_select_db("fitness",$dbhandle) or die("Could not select examples");
?>

<div id="container">
<div id="head"><img src="logo.png" /></div>
<div id="content">

<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
<?php
$result = mysql_query("SELECT entity_id,value FROM `catalog_category_entity_varchar` WHERE `attribute_id` = 41 and `store_id`= 0"); 
?>
<p>Move products from one category to another</p>
  <table width="400" border="1">
    <tr>
      <td width="95">Move From:</td>
      <td width="189"><select name="from" id="from">
	<?php
	while($row = mysql_fetch_array($result)){
		if(isset($_POST['from'])&& $_POST['from']==$row[0]){
			echo '<option value="'.$row[0].'" selected="selected">'.$row[0].' - '.$row[1].'</option>';
		}else{
			echo '<option value="'.$row[0].'">'.$row[0].' - '.$row[1].'</option>';
		}
	}
	?>
      </select></td>
    </tr>
    <tr>
      <td>Move To:</td>
      <td><select name="to" id="to">
	<?php
	mysql_data_seek( $result, 0 );
	while($row = mysql_fetch_array($result)){
		if(isset($_POST['to']) && $_POST['to']==$row[0]){
			echo '<option value="'.$row[0].'" selected="selected">'.$row[0].' - '.$row[1].'</option>';
		}else{
			echo '<option value="'.$row[0].'">'.$row[0].' - '.$row[1].'</option>';
		}
	}
	?>
      </select></td>
    </tr>
    <tr>
      <td>Update Store:</td>
      <td><input type="checkbox" name="update_store" id="update_store" <?php if(isset($_POST['update_store'])){echo 'checked="checked"';} ?> /></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><input type="submit" name="submit" value="Move Products" /></td>
    </tr>
  </table>
</form>

<?php


if (isset($_POST["submit"])){
	$move_from = $_POST['from'];
	$move_to = $_POST['to'];
	$update_website = $_POST['update_store'];
	$store_id = 0;
	$manufact = 1048;

	// First we need to check to see if we are updating websites
	if($update_website){
		// Check to see what the category is under
		echo "You are going to be moving category id(".$move_from.") to category id(".$move_to.") and updating the products website to ";
		
		// Need to find what website to transfer to
		// For this will use regular expression from the category path
		$path = mysql_query("SELECT path FROM `catalog_category_entity` WHERE `entity_id` = '$move_to'");
 		if(preg_match('^1/4/^',mysql_result($path, 0))){
			echo "Start Cycles";
			$store_id = 1;
		}else if(preg_match('^1/162/^',mysql_result($path, 0))){
			echo "Start Football";
			$store_id = 2;
		}else if(preg_match('^1/163/^',mysql_result($path, 0))){
			echo "Start Fitness";
			$store_id = 3;
		}else if(preg_match('^1/164/^',mysql_result($path, 0))){
			echo "Wild Trak";
			$store_id = 4;
		}else if(preg_match('^1/165^',mysql_result($path, 0))){
			echo "More Mile";
			$store_id = 5;
		}
		echo "<br>";

		// Get list of IDs in the from category
		$cat_products = mysql_query("SELECT product_id FROM `catalog_category_product` WHERE `category_id` = $move_from") or die(mysql_error());
		while($row = mysql_fetch_array($cat_products)){
			echo "<strong>" . $row[0] . "</strong>";
			$manu= Mage::getResourceModel('catalog/product')->getAttributeRawValue($row[0], 'manufacturer', 0);
				echo "(".$manu.")";
			$current_website = mysql_query("SELECT website_id FROM `catalog_product_website` WHERE `product_id` = $row[0] AND `website_id` = $store_id");
				if(mysql_num_rows($current_website)==0){
					echo " - Not On";
					//if($manu==$manufact){
						echo " Needs to be put on";
						$add_web = mysql_query("INSERT INTO catalog_product_website (product_id,website_id) VALUES('$row[0]','$store_id') ") or die(mysql_error());
						if($add_web){echo "data inserted";}else{"error";}
					//}
				}else{
					echo " - On";
				}
				echo "<br>";

			// Get child ID's as these need to be updated too
			$product = Mage::getModel('catalog/product')->load($row[0]);
			if ($product->getTypeId() == 'configurable'){
				$childProducts = Mage::getModel('catalog/product_type_configurable')
								->getUsedProducts(null,$product);
				foreach($childProducts as $child) {
					echo "--";
						print_r($child->getId());  // You can use any of the magic get functions on this object to get the value
					$childid = $child->getId();
					$manu= Mage::getResourceModel('catalog/product')->getAttributeRawValue($child->getId(), 'manufacturer', 0);
					echo "(".$manu.")";
					$current_website = mysql_query("SELECT website_id FROM `catalog_product_website` WHERE `product_id` = ".$child->getId()." AND `website_id` = $store_id");
					if(mysql_num_rows($current_website)==0){
						echo " - Not On";
						//if($manu==$manufact){
							echo " Needs to be put on";
							$add_web = mysql_query("INSERT INTO catalog_product_website (product_id,website_id) VALUES('$childid','$store_id') ") or die(mysql_error());
							if($add_web){echo "data inserted";}else{"error";}
						//}
					}else{
						echo " - On";
					}
					echo "<br>";
				}
			}
		}
		// Now move updated products into the category
		echo "You are going to be moving category id(".$move_from.") to category id(".$move_to.")";
		$insert = mysql_query("INSERT IGNORE INTO catalog_category_product SELECT '$move_to' as category_id, tablea.product_id, 0 as position FROM `catalog_category_product`  tablea LEFT OUTER JOIN `catalog_category_product`  tableb ON tablea.product_id = tableb.product_id WHERE tablea.`category_id` = '$move_from' AND tableb.category_id = '$move_from'") or die(mysql_error());
		if($insert){
			echo "<br><strong>Data Inserted</strong>";
		}else{
			echo "<br><strong>Error</strong>";
		}
		
	}else{
		// Just move products
		// See if already exists in category?
		echo "You are going to be moving category id(".$move_from.") to category id(".$move_to.")";
		$insert = mysql_query("INSERT IGNORE INTO catalog_category_product SELECT '$move_to' as category_id, tablea.product_id, 0 as position FROM `catalog_category_product`  tablea LEFT OUTER JOIN `catalog_category_product`  tableb ON tablea.product_id = tableb.product_id WHERE tablea.`category_id` = '$move_from' AND tableb.category_id = '$move_from'") or die(mysql_error());
		if($insert){
			echo "<br><strong>Data Inserted</strong>";
		}else{
			echo "<br><strong>Error</strong>";
		}
	}
} 
//close the connection
mysql_close($dbhandle);
?>

</div>
<div id="footer">Designed by <a href="http://www.jamessimpson.co.uk">James Simpson</a></div>
</div>
</body>
</html>