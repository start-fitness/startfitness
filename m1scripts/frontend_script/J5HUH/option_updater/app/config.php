<?php

error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit','768M');
require_once($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
umask(0);
Mage::app();


// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

?>