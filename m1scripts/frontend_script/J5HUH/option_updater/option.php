<?php
//
// Designed By James Simpson
//

// Include the header
require('app/config.php');
include('skin/header.php');

// The switch for each page
$action = isset( $_GET['action'] ) ? $_GET['action'] : "";

// Set out the page switch
switch ( $action ) {
	case 'edit-attributes':
    		require( "template/edit_attributes.php" );
    	break;
	case 'view-products':
    		require( "template/view_products.php" );
    	break;

  	default:
		// Home Page
    		require( "template/view_attributes.php" );
}

// Include the footer
include('skin/footer.php');

?>