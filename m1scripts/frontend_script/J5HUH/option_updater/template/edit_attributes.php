<?php

$attribute = $_GET['attribute_id'];

$sql_attribute_names = "SELECT frontend_label FROM `eav_attribute` WHERE `frontend_input` = 'select' AND `attribute_id` > 0 AND `source_model` = 'eav/entity_attribute_source_table' AND `attribute_id` = '".$attribute."' ";
$results_attName = $read->fetchOne($sql_attribute_names);

echo "<h2>".$results_attName."</h2>";
$sql_details = "SELECT ao.option_id,ao.attribute_id,ao.sort_order,ov.value_id,ov.store_id,ov.value, count(*) as 'products' FROM `eav_attribute_option` ao LEFT JOIN `eav_attribute_option_value` ov on ao.`option_id`=ov.`option_id` LEFT JOIN `catalog_product_entity_int` ei ON ao.`option_id`=ei.`value` WHERE ov.`store_id` = 0 AND ei.`store_id` = 0 AND ao.`attribute_id` = ".$attribute." GROUP BY ov.`value` ORDER BY `ao`.`sort_order`,`ov`.`value` ASC";
$att_details = $read->fetchAll($sql_details);

$sql_nodetails = "SELECT * FROM `eav_attribute_option` ao LEFT JOIN `eav_attribute_option_value` ov on ao.`option_id`=ov.`option_id` WHERE ov.`store_id` = 0 AND ao.`attribute_id` = ".$attribute." AND ao.`option_id` NOT IN (SELECT ao.option_id FROM `eav_attribute_option` ao LEFT JOIN `eav_attribute_option_value` ov on ao.`option_id`=ov.`option_id` LEFT JOIN `catalog_product_entity_int` ei ON ao.`option_id`=ei.`value`WHERE ov.`store_id` = 0 AND ei.`store_id` = 0 AND ao.`attribute_id` = ".$attribute." GROUP BY ov.`value`)";
$att_nodetails = $read->fetchAll($sql_nodetails);
?>
<table style="border:none;" width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td style="border:none;" align="center" valign="top">

<form action="<?php echo $_SERVER["PHP_SELF"]; ?>?action=edit-attributes&attribute_id=<?php echo $attribute; ?>" method="post" enctype="multipart/form-data">
<table width="400" border="1">
    <tr>
      <td width="95">Move From:</td>
      <td width="189"><select name="from" id="from">
	<?php
	foreach($att_details as $row){
		echo '<option value="'.$row['option_id'].'">' . $row['option_id'] . ' --- ' .$row['value'] . '</option>';
	}
	?>
      </select></td>
    </tr>
    <tr>
      <td>Move To:</td>
      <td><select name="to" id="to">
	<?php
	//mysql_data_seek( $result, 0 );
	foreach($att_details as $row){
		echo '<option value="'.$row['option_id'].'">' . $row['option_id'] . ' --- ' .$row['value'] . '</option>';
	}
	?>
      </select></td>
    </tr>
    <tr>
      <td colspan="2" align="center"><input type="submit" name="submit" value="Merge Products" /></td>
    </tr>
  </table>
</form>

<br><br>

<?php

if (isset($_POST["submit"])){

	$move_from = $_POST['from'];
	$move_to = $_POST['to'];

	$update_sql = "UPDATE `fitness`.`catalog_product_entity_int` SET `value` = '".$move_to."' WHERE `catalog_product_entity_int`.`attribute_id` = ".$attribute." AND `catalog_product_entity_int`.`value` = ".$move_from." ";
	//echo $update_sql;
	$write->query($update_sql);

	echo "Products Moved Successfully<br>";
	echo '<a href="'.$_SERVER["PHP_SELF"]; ?>?action=edit-attributes&attribute_id=<?php echo $attribute.'">Back To Previous Page</a>';
}else{

echo '<h2> Attributes With No Products</h2>';
echo '<table width="600" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    			<th align="center">ID</th>
    			<th align="center">Name</th>
    			<th align="center">Product</th>
    			<th align="center">Delete</th>
  		</tr>';

foreach($att_nodetails as $att1){
	echo '<tr>';
		echo '<td>'. $att1['option_id'] .'</td>';
		echo '<td>'. $att1['value'] .'</td>';
		echo '<td>0</td>';
		echo '<td></td>';
	echo "</tr>";
}

echo '</table>';

echo '<h2> Attributes With Products</h2>';
echo '<table width="600" border="0" cellspacing="0" cellpadding="0">
  		<tr>
    			<th align="center">ID</th>
    			<th align="center">Name</th>
    			<th align="center">Product</th>
    			<th align="center">Edit</th>
  		</tr>';

foreach($att_details as $att){
	echo '<tr>';
		echo '<td>'. $att['option_id'] .'</td>';
		echo '<td>'. $att['value'] .'</td>';
		echo '<td>'. $att['products'] .' (<a href="option.php?action=view-products&attribute_id='.$attribute.'&item_id='.$att['option_id'].' ">view</a>)</td>';
		echo '<td></td>';
	echo "</tr>";
}

echo '</table></td></tr></table>';

}

?>