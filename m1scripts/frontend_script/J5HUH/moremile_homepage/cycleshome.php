<?php
// Start Cycles Homepage Builder - To be run via CRON
// Designed and built by James Simpson
// V 1.0.0 BETA

// If you want manual products on this page - put them to position 1, as this script only runs 
// from position 2

error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
Mage::setIsDeveloperMode();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

// Work out the dates need (from yesterday at 9:00am till 8:59am today)
$datemod = strtotime("-1 day", time());
$today = date('Y-m-d 08:59:59');
$yesterday = date('Y-m-d 09:00:00', $datemod);

// Set the homepage category ID
$cat_id = "907";
// Set the store ID
$store_id = '1,21';

// The SQL query to get the sales 
$sql_getprod = "SELECT SFOI.`product_id`,SFOI.`name`,sum(SFOI.qty_ordered) as 'sold', tmp_qty.`qty`, CE.`value`
    FROM `sales_flat_order` SFO 
    LEFT JOIN `sales_flat_order_item` SFOI on SFO.`entity_id`=SFOI.`order_id` 
    LEFT JOIN (SELECT * FROM `catalog_product_entity_int` WHERE `attribute_id` = '102') CE  ON SFOI.`product_id`=CE.`entity_id`
	INNER JOIN (SELECT cisi.`product_id`, IF(tmp.`parent_id` is NULL, cisi.`qty`, tmp.`qty`) qty 
                FROM `cataloginventory_stock_item` cisi 
                LEFT JOIN (SELECT cpsl.`parent_id`, sum(cisi.`qty`) qty 
                           FROM `catalog_product_super_link` cpsl 
                           INNER JOIN `cataloginventory_stock_item` cisi ON cisi.`product_id`=cpsl.`product_id` 
                           INNER JOIN `catalog_product_entity` cpe ON cpe.`entity_id`=cpsl.`parent_id` 
                           GROUP BY cpsl.`parent_id`) tmp ON tmp.`parent_id`=cisi.`product_id`) tmp_qty ON tmp_qty.`product_id`=SFOI.`product_id`
	WHERE SFO.`store_id` IN ('".$store_id."')
    AND SFOI.`parent_item_id` IS NULL 
    AND SFO.`created_at` BETWEEN '".$yesterday."' AND '".$today."' 
    AND SFO.`customer_group_id` NOT IN (2,6,7)
    AND CE.`value` = '4'
    AND tmp_qty.`qty` >= '4' 
    GROUP BY SFOI.`product_id` ORDER BY `sold` DESC, SFOI.`product_id` DESC LIMIT 28";

// Fetch all reqults
$results = $read->fetchAll($sql_getprod);

// Print out all results
//print_r($results);

// Delete Current Items

	$sql_delete = "DELETE FROM `catalog_category_product` WHERE `category_id` = '".$cat_id."' AND `position` > '2'";
	$write->query($sql_delete);

	// Testing - Write out the query
	//echo $sql_delete . '<br>';

// Add New Items
$count  = 0;
$number = 3;

// Products Table
$writeUpdate = '';
$writeUpdate .= '<div class="CSSTableGenerator" >';
$writeUpdate .= '<table width="100%" border="0" cellspacing="0" cellpadding="0">';
$writeUpdate .= '<tr><th>Product ID</th><th>Product Name</th><th>Qty Sold</th><th>Qty In Stock</th><th>Position</th></tr>';

foreach($results as $value){
	if($count >= 4){
		$count = 0;
		$number++ ;
	}
	
	// More Mile Category 
	$sql_insert = "INSERT IGNORE INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat_id."','".$value['product_id']."','".$number."')";
	$write->query($sql_insert);
	$count++ ;

	// Testing - Write out the insert
	//echo $sql_insert . '<br>';
	
	$writeUpdate .= '<tr><td>'.$value['product_id'].'</td><td>'.$value['name'].'</td><td>'.$value['sold'].'</td><td>'.$value['qty'].'</td><td>'.$number.'</td></tr>';
}
$writeUpdate .= '</table>';
$writeUpdate .= '</div>';

// css styling
$html = '';
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #333; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #F3F3F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr><td colspan="3" align="center"><h3>Start Cycles HomePage</h3><p>This will be your Start Cycles Homepage Tomorrow</p><div class="line"></div></td></tr>';
$html .= '<tr><td colspan="3">'.$writeUpdate.'</td></tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';

// Closing tables
$html .= '</td></tr></table>';

// Send the email
//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gina@startfitness.co.uk','pat@startfitness.co.uk','IanHarding@startfitness.co.uk','michaeltaggart@startfitness.co.uk','camerontaggart@startcycles.co.uk','michaelamillican@startfitness.co.uk','donnabridgwood@startfitness.co.uk','gary@startfitness.co.uk','mark@startfitness.co.uk');
$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gary@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Start Cycles Homepage');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Start Cycles Homepage Update '.date("D d-M-Y"));
			$mail->send();

//end
echo $html;

?>