<?php
require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');
umask(0);
Mage::app();

// Turn On Error Reporting
error_reporting(E_ALL | E_STRICT);
ini_set('memory_limit', '10048M');
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);
Mage::setIsDeveloperMode(true);
Mage::getModel('core/store')->load(0);

echo "Google Cloud Print<br>";

$proxy = new SoapClient('http://startdev.startfitness.co.uk/api/v2_soap/?wsdl'); // TODO : change url
$sessionId = $proxy->login('printtest', 'QWV$2774gw'); 

// Checking if API connected
//$result = $proxy->customerCustomerInfo($sessionId, '229');
//var_dump($result);


// If you don't need the session anymore
//$client->endSession($session);
 
$order_count = 0;
$from = date('Y-m-d H:i:s', strtotime('-5 minutes'));
$to = date('Y-m-d H:i:s');
$orders = Mage::getSingleton('sales/order');
/* @var $orders Mage_Sales_Model_Resource_Order_Collection */
$orders = $orders->getCollection()->addAttributeToSelect('*')->addFieldToFilter('created_at', array('from' => $from, 'to' => $to));

foreach($orders as $order){    
	/* @var $order Mage_Sales_Model_Order */         
	if ($order->canInvoice()){    
		try {    
			$invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();     
			if (!$invoice->getTotalQty()) {        
				Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));   
		 	}     
		 	$invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);    
		 	$invoice->register();        
		 	$transactionSave = Mage::getModel('core/resource_transaction')->addObject($invoice)->addObject($invoice->getOrder());         
		 	$transactionSave->save();    
		 	}    
		 	catch (Mage_Core_Exception $e) {   
				echo 'Caught exception: ',  $e->getMessage(), "\n";     
			}            
	 	}

	         
	 $invoices = $order->getInvoiceCollection();   
	 //$pdfInvoice = Mage::getModel('sales/order_pdf_invoice');
    	 //$pdf = $pdfInvoice->getPdf($invoices);

	 // MOOGENTO CODE 

	 $pdf = Mage::getModel('sales/order_pdf_invoice')->getPickSeparated2($invoices);	 
	 //$this->_prepareDownloadResponse('pick-list-separated'.Mage::getSingleton('core/date')->date('Y-m-d_H').'.pdf', $pdf->render(), 'application/pdf');
	 
	 // MOOGENTO CODE END	       
	 
	 $pdfFile = $pdf->render();    

	 $pdf_filename = "Email_".$order->getIncrementId().".pdf";            
	 $mail = new Zend_Mail();        
	 $at = $mail->createAttachment($pdfFile);        
	 $at->type = 'application/pdf';        
	 $at->disposition = Zend_Mime::DISPOSITION_ATTACHMENT;        
	 $at->encoding = Zend_Mime::ENCODING_BASE64;        
	 $at->filename = $pdf_filename;            
	 $mail->setBodyText("New Order: " . $order->getIncrementId());        
	 $mail->setSubject("New Order: " . $order->getIncrementId());        
	 $mail->setFrom('customerservices@startcycles.co.uk');        
	 $mail->addTo('jamessimpson@startfitness.co.uk');        
	 $mail->send();    
 }
 exit;
 ?>