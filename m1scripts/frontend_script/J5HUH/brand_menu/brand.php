<?php
// Start Group Script To Auto Populate Sale Pages
// Designed and built by James Simpson
// V1.0


error_reporting(E_ALL);

// First connect up to Mage
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');



$sql_brand = "SELECT eaov.`value` as 'brand',sum(csi.`qty`) 'stock',bdb.`visibility_nav`,bdb.`store_id`,cpw.`website_id`,bdb.`visibility`
		FROM `catalog_product_entity_int` cpei
		LEFT JOIN `eav_attribute_option_value` eaov ON cpei.`value`=eaov.`option_id`
		LEFT JOIN `cataloginventory_stock_item` csi ON cpei.`entity_id`=csi.`product_id`
		LEFT JOIN `smashingmagazine_branddirectory_brand` bdb ON eaov.`value`=bdb.`name`
		LEFT JOIN `catalog_product_website` cpw ON cpei.`entity_id`=cpw.`product_id`
		WHERE cpei.`attribute_id` = 81
		AND cpei.`store_id` = 0
		AND eaov.`store_id` = 0
		AND cpw.`website_id` != 8
		AND eaov.`value` NOT IN ('Life Fitness')
		GROUP BY eaov.`value`,cpw.`website_id`
		";

// Fetch all reqults
$results_brand = $read->fetchAll($sql_brand);

//print_r($results_brand);

$sum = 0;

// Store ID's per website
$startcycles = array('1','10','21'); 	// Website 1
$startfootball = array('5','16','20'); 	// Website 2
$startfitness = array('6','22','19'); 	// Website 3
$wildtrak = array('7'); 			// Website 4
$doesntmatter = array('1','5','6','7');


// Turnaround Table
$writeTurnaround = '';
$writeTurnaround .= '<div class="CSSTableGenerator" >';
$writeTurnaround .= '<table width="652" border="0" cellspacing="0" cellpadding="0">';
$writeTurnaround .= '<tr><th colspan="4" align="center"><strong>Brands Navigation Checker</strong></th></tr>';
$writeTurnaround .= '<tr><th>Brand</th><th>Stock</th><th>Website</th><th>Nav Status</th></tr>';

foreach($results_brand as $brand){
$website = explode(",",$brand['store_id']);
	
	// Find active nav brands where no stock
	IF($brand['stock'] == '0' && $brand['visibility_nav'] == '1' && $brand['visibility'] == '1'){
		IF(count(array_intersect($website,$startcycles)) > 0 && $brand['website_id'] == '1'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>Out Of Stock</td><td>Start Cycles</td><td>Active</td></tr>';
		}ELSE IF(count(array_intersect($website,$startfootball)) > 0 && $brand['website_id'] == '2'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>Out Of Stock</td><td>Start Football</td><td>Active</td></tr>';
		}ELSE IF(count(array_intersect($website,$startfitness)) > 0 && $brand['website_id'] == '3'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>Out Of Stock</td><td>Start Fitness</td><td>Active</td></tr>';
		}ELSE IF(count(array_intersect($website,$wildtrak)) > 0 && $brand['website_id'] == '4'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>Out Of Stock</td><td>WildTrak</td><td>Active</td></tr>';
		}

	// Now find stock and not active
	}ELSE IF($brand['stock'] > 0 && $brand['visibility_nav'] == '0'){
		IF($brand['website_id'] == '1'){
			IF(count(array_intersect($website,$startcycles)) > 0){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>In Stock</td><td>Start Cycles</td><td>Disabled</td></tr>';
			}ELSE{
				
			}
		}ELSE IF(count(array_intersect($website,$startfootball)) > 0 && $brand['website_id'] == '2'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>In Stock</td><td>Start Football</td><td>Disabled</td></tr>';
		}ELSE IF(count(array_intersect($website,$startfitness)) > 0 && $brand['website_id'] == '3'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>In Stock</td><td>Start Fitness</td><td>Disabled</td></tr>';
		}ELSE IF(count(array_intersect($website,$wildtrak)) > 0 && $brand['website_id'] == '4'){
			$writeTurnaround .= '<tr><td>'.$brand['brand'].'</td><td>In Stock</td><td>Wildtrak</td><td>Disabled</td></tr>';
		}
	}

}


$writeTurnaround .= '</table></div>';




// css styling
$html .= '<style>';
$html .= '.CSSTableGenerator table { color: #333; font-family: Helvetica, Arial, sans-serif; border-collapse: collapse; border-spacing: 0; }';
$html .= '.CSSTableGenerator td, th { border: 1px solid #CCC; height: 20px; }';
$html .= '.CSSTableGenerator th { background: #F3F3F3; font-weight: bold; font-size: 14px; }';
$html .= '.CSSTableGenerator td { background: #FAFAFA; padding-left: 5px; font-size: 11px; }';
$html .= 'h3 { margin-bottom: 0px; font-family: Helvetica, Arial, sans-serif;}';
$html .= 'p { margin: 0px; font-family: Helvetica, Arial, sans-serif; font-size:12px; }';
$html .= '.line { height:2px; background:#000; }';
$html .= '.red {color: #ff0000;} ';
$html .= '</style>';
$html .= '<table width="100%" border="0" cellspacing="0" cellpadding="0"><tr><td align="center" valign="top">';

// Internal table
$html .= '<table width="652" border="0" cellspacing="5" cellpadding="5">';
$html .= '<tr>';
$html .= '<td width="652" align="center" valign="top">'.$writeTurnaround.'</td>';
$html .= '</tr>';
$html .= '<tr>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '<td>&nbsp;</td>';
$html .= '</tr>';
$html .= '</table>';


// Closing tables
$html .= '</td></tr></table>';

/*
// Send the email
//Receiver Details
//$sendToEmailAddress = array('jamessimpson@startfitness.co.uk','gina@startfitness.co.uk','pat@startfitness.co.uk','IanHarding@startfitness.co.uk','michaeltaggart@startfitness.co.uk','camerontaggart@startcycles.co.uk','michaelamillican@startfitness.co.uk','donnabridgwood@startfitness.co.uk','gary@startfitness.co.uk','mark@startfitness.co.uk');
$sendToEmailAddress = array('jamessimpson@startfitness.co.uk');

$sendToName = 'Start Fitness';
$mail = new Zend_Mail();
			$mail->setBodyText($message);
			$mail->setBodyHtml($html);
			$mail->setFrom(Mage::getStoreConfig('trans_email/ident_general/email'), 'Daily Customer Services');
			$mail->addTo($sendToEmailAddress, $sendToName);
			$mail->setSubject('Daily Customer Services Report '.date("D d-M-Y"));
			$mail->send();

//end
*/
echo $html;

?>