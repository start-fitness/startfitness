<?php
ini_set('memory_limit', '2048M');
$start = microtime(true);
require_once '../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(0);
$resource = Mage::getSingleton('core/resource');
$readConnection = $resource->getConnection('core_read');
$writeConnection = $resource->getConnection('core_write');

$sql = "SELECT * FROM `enterprise_url_rewrite` WHERE `request_path` LIKE '%.html' AND `target_path` LIKE 'catalog/%' AND is_system = 0";
$writeConnection->query($sql);