<?php

ini_set('memory_limit', '16048M');
require_once dirname(__FILE__).'/../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$collection = Mage::getModel('catalog/product')->getCollection();
$collection
        ->addAttributeToSelect('*')
		->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED))
		//->addAttributeToFilter('entity_id', 147970)
		->setOrder("entity_id", "DESC")
        ->addAttributeToFilter('type_id', 'configurable');
Mage::getSingleton('cataloginventory/stock')
    ->addInStockFilterToCollection($collection);
$page = 1;
$processed = 0;
do {
    $collection
        ->setPageSize(100)
        ->setCurPage($page);

    $results = $collection->clear()->load();

    // do stuff ......

    
	echo "Processing page $page of ".$collection->getLastPageNumber()."... Processed {$processed}\n";
	$page++;
	foreach($results as $confProduct ){
		$confProduct = Mage::getModel('catalog/product')->load($confProduct->getId());
		$gallery = $confProduct->getData('media_gallery');
		//var_dump($gallery);exit;
		//get the instance for the media_gallery attribute
		$mediaGalleryAttribute = Mage::getModel('eav/config')->getAttribute('catalog_product', 'media_gallery');
		//get access to a model that handles the image association to products
		$resource = Mage::getResourceSingleton('catalog/product_attribute_backend_media');
		if (isset($gallery['images'])) {
		    //get all simple child products ids 
		    $simpleProductIds = $confProduct->getTypeInstance()->getUsedProductIds();
		    //for each simple product
		    //var_dump($simpleProductIds[0]);
		    foreach ($simpleProductIds as $productId) {
		    	
				$childp = Mage::getModel('catalog/product')->load($productId);
				
				$cgallery = $childp->getData('media_gallery');
				if(isset($cgallery['images']) && count($cgallery['images']) > 0) {
					//var_dump($cgallery['images']);
					//echo $confProduct->getSku()." children have images\n";
					continue;
					
				}
				else{
					//var_dump($confProduct->getId());
				}
				try{
			        //for eahc image from the main product
			        foreach ($gallery['images'] as $image) {
			            //build an array with imag settings
			            $data = array();
			            $data['entity_id']      = $productId;
			            $data['attribute_id']   = $mediaGalleryAttribute->getId();
			            $data['value']          = $image['file'];
			            //save the image and get the id
			            $lastId = $resource->insertGallery($data);
			            //set the label and position for the new images
			            $data = array();
			            $data['value_id'] = $lastId;
			            $data['label']    = $image['label'];
			            $data['position'] = (int) $image['position'];
			            $data['disabled'] = (int) $image['disabled'];
			            $data['store_id'] = (int) 0;
			            //finish the job
			            $resource->insertGalleryValueInStore($data);
			        }
			    
					file_put_contents("idsupdated.txt", $productId, FILE_APPEND);
					$processed++;
					}
					catch(exception $e){
						var_dump($e->getMessage());			
					}
				//exit;
			}
			
		}
	}

} while ($page <= $collection->getLastPageNumber());
		
echo "Processing page $page of ".$collection->getLastPageNumber()."... Processed {$processed}\n";



