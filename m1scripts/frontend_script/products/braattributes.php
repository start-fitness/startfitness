<?php


require_once '../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$collection = Mage::getModel('catalog/product')->getCollection();
$products = $collection
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('type_id', Mage_Catalog_Model_Product_Type::TYPE_SIMPLE)
		->addAttributeToFilter('attribute_set_id', 47);
		
foreach($products as $product){
	var_dump($product->getData("uk_cup_size")."_".$product->getData("uk_chest_size"));
	$store_id = 0;
	addAttributeValue('uk_bra_chest_size', ($product->getData("uk_cup_size")."_".$product->getData("uk_chest_size")));
	$val = attributeValueExists('uk_bra_chest_size', ($product->getData("uk_cup_size")."_".$product->getData("uk_chest_size")));
	var_dump($val);
	$product->setData("uk_bra_chest_size", $val ); 
	$product->getResource()->saveAttribute($product, "uk_bra_chest_size");
}


function addAttributeValue($attributeCode, $attValue) {

    if (!attributeValueExists($attributeCode, $attValue)) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attributeCode);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['option_name'][0] = $attValue;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }
}

function attributeValueExists($attribute, $value) {
    $attribute_model = Mage::getModel('eav/entity_attribute');
    $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
    $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute);
    $attribute = $attribute_model->load($attribute_code);
    $attribute_options_model->setAttribute($attribute);
    $options = $attribute_options_model->getAllOptions(false);

    foreach ($options as $option) {
        if ($option['label'] == $value) {
            return $option['value'];
        }
    }
    return false;
}
