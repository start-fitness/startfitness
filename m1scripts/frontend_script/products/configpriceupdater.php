<?php
ini_set('memory_limit', '2048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$skus = array();
$errors = 0;
$except = array();

$products = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', 'configurable')
    ->addAttributeToSelect('*')->setPageSize(100);
$pages = $products->getLastPageNumber();
//echo (string)$products->getSelect();
echo "\n<br />found products: {$products->count()}<br />\n";
echo "\n<br />found pages: {$pages}<br />\n";
$count = 0;
$currentPage = 1;
do{
	echo "\n<br />current page: {$currentPage}<br />\n";
	$products->setCurPage($currentPage);
    $products->load();
	foreach($products as $_product){
			
		$childrenids = Mage::getResourceSingleton('catalog/product_type_configurable')->getChildrenIds($_product->getId(), true);
		$childrenids[0] = array_values($childrenids[0]);
		//var_dump($childrenids[0][0]);
		if(count($childrenids)>0 && count($childrenids[0]) > 0 && isset($childrenids[0][0]) && is_numeric($childrenids[0][0])){
			$_simple_product = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addIdFilter(array($childrenids[0][0]))
            ;
			$_simple_product = $_simple_product->getFirstItem();
			//var_dump($_product->getData("price"));
			$haschanged = false;
			if($_product->getData("special_price") != $_simple_product->getData("special_price")){$haschanged=true;$_product->setData("special_price", $_simple_product->getData("special_price"));}
			if($_product->getData("price") != $_simple_product->getData("price")){$haschanged=true;$_product->setData("price", $_simple_product->getData("price"));}
			if($_product->getData("special_from_date") != $_simple_product->getData("special_from_date")){$haschanged=true;$_product->setData("special_from_date", $_simple_product->getData("special_from_date"));}
			if($_product->getData("special_to_date") != $_simple_product->getData("special_to_date")){$haschanged=true;$_product->setData("special_to_date", $_simple_product->getData("special_to_date"));}
			//var_dump($_product->getData("price"));
			if($haschanged){
			try{
				
				$_product->save();
				//var_dump($_product->getData("entity_id"));exit;
				$count++;
			}
			catch(exception $e){
				echo "could not save ".$_product->getSku()." !\n";
				$errors++;
			}
			}
		}
	}
	$currentPage++;
    //make the collection unload the data in memory so it will pick up the next page when load() is called.
	$products->clear();
}while ( $currentPage <= $pages);

echo "\n<br />Updated products: $count -  ERRORS: $errors\n";

