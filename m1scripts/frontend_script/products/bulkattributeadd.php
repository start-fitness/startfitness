<?php
/*
* This script adds an attribute to all of the attributes sets
* Use with care
* This file should be located in the Magento-root/shell directory.
*/

//change the [attribute code] to be the code of the attribute you want to add
$attributeCode = 'product_notes';
//change this to be the group name in the attribute set where you want this attribute to go
$group = 'General';
//change this to the order location where you want this attribute to go
$sortOrder = '399';

require_once '../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$attSet = Mage::getModel('eav/entity_type')->getCollection()->addFieldToFilter('entity_type_code','catalog_product')->getFirstItem(); // This is because the you adding the attribute to catalog_products entity ( there is different entities in magento ex : catalog_category, order,invoice... etc ) 
$attSetCollection = Mage::getModel('eav/entity_type')->load($attSet->getId())->getAttributeSetCollection(); // this is the attribute sets associated with this entity 
$attributeInfo = Mage::getResourceModel('eav/entity_attribute_collection')
    ->setCodeFilter($attributeCode)
    ->getFirstItem();
$attCode = $attributeInfo->getAttributeCode();
$attId = $attributeInfo->getId();
foreach ($attSetCollection as $a)
{
	//if(in_array($a->getId(), array(43,44,45))) continue;
    $set = Mage::getModel('eav/entity_attribute_set')->load($a->getId());
    $setId = $set->getId();
    $group = Mage::getModel('eav/entity_attribute_group')->getCollection()->addFieldToFilter('attribute_set_id',$setId)->setOrder('sort_order',ASC)->getFirstItem();
    $groupId = $group->getId();
    $newItem = Mage::getModel('eav/entity_attribute');
    $newItem->setEntityTypeId($attSet->getId()) // catalog_product eav_entity_type id ( usually 10 )
              ->setAttributeSetId($setId) // Attribute Set ID
              ->setAttributeGroupId($groupId) // Attribute Group ID ( usually general or whatever based on the query i automate to get the first attribute group in each attribute set )
              ->setAttributeId($attId) // Attribute ID that need to be added manually
              ->setSortOrder($sortOrder) // Sort Order for the attribute in the tab form edit
              ->save()
    ;
    echo "Attribute ".$attCode." Added to Attribute Set ".$set->getAttributeSetName()." in Attribute Group ".$group->getAttributeGroupName()."\n";
}
?>