<?php
ini_set('memory_limit', '2048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$skus = array();
$errors = 0;
$except = array();

$products = Mage::getResourceModel('catalog/product_collection')->addAttributeToFilter('type_id', 'configurable')
    ->addAttributeToSelect('*')->setPageSize(100);
$pages = $products->getLastPageNumber();
//echo (string)$products->getSelect();
echo "\n<br />found products: {$products->count()}<br />\n";
echo "\n<br />found pages: {$pages}<br />\n";
$count = 0;
$currentPage = 1;
do{
	echo "\n<br />current page: {$currentPage}<br />\n";
	$products->setCurPage($currentPage);
    $products->load();
	foreach($products as $_product){
		$numbersizes= 0;	
		$subprods = $_product->getTypeInstance()->getUsedProducts();
		foreach($subprods as $subprod){
			if($subprod->getStockItem()->getIsInStock()){
				$numbersizes++;
			}
			else{
				
			}
		}
		$_product->setData("sizes_size",$numbersizes);
    	$_product->getResource()->saveAttribute($_product, 'sizes_size');
		echo "updated ".$_product->getSku()." with ".$numbersizes." sizes.\n";
	}
	$currentPage++;
    //make the collection unload the data in memory so it will pick up the next page when load() is called.
	$products->clear();
}while ( $currentPage <= $pages);

echo "\n<br />Updated products: $count -  ERRORS: $errors\n";

