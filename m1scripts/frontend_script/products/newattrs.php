<?php


require_once '../../app/Mage.php';
umask(0);
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


$attr = "jacket_type";

$json = file_get_contents(dirname(__FILE__)."/".$attr.".json");
//var_dump($json);
$data = json_decode($json);
foreach($data as $line){
	
	$_product = Mage::getModel('catalog/product')->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($line->entity_id);
    if($_product){
    	addAttributeValue($attr, $line->$attr);
		$val = attributeValueExists($attr, $line->$attr);
		var_dump($line->entity_id, $val);
		if($val){
			try{
	    	$_product->setData($attr,$val);
	    	$_product->getResource()->saveAttribute($_product, $attr);
			}
			catch(Exception $e){
				var_dump($e->getMessage());
			}
		}
	}
	//exit;
}
		


function addAttributeValue($attributeCode, $attValue) {

    if (!attributeValueExists($attributeCode, $attValue)) {
        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
        $attr = $attr_model->loadByCode('catalog_product', $attributeCode);
        $attr_id = $attr->getAttributeId();
        $option['attribute_id'] = $attr_id;
        $option['value']['option_name'][0] = $attValue;
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
        $setup->addAttributeOption($option);
    }
}

function attributeValueExists($attribute, $value) {
    $attribute_model = Mage::getModel('eav/entity_attribute');
    $attribute_options_model = Mage::getModel('eav/entity_attribute_source_table');
    $attribute_code = $attribute_model->getIdByCode('catalog_product', $attribute);
    $attribute = $attribute_model->load($attribute_code);
    $attribute_options_model->setAttribute($attribute);
    $options = $attribute_options_model->getAllOptions(false);

    foreach ($options as $option) {
        if ($option['label'] == $value) {
            return $option['value'];
        }
    }
    return false;
}
