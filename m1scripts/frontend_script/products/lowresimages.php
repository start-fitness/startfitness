<?php
ini_set('memory_limit', '4048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(0);
$skus = array();
$errors = 0;
$except = array();

$products = Mage::getResourceModel('catalog/product_collection')
->addAttributeToFilter('status', '1')
->addAttributeToFilter('visibility', '4')
    ->addAttributeToSelect('*')->setPageSize(100);
$pages = $products->getLastPageNumber();
//echo (string)$products->getSelect();
echo "\n<br />found products: {$products->count()}<br />\n";
echo "\n<br />found pages: {$pages}<br />\n";
$count = 0;
$currentPage = 1;
$f = fopen(dirname(__FILE__)."/lowresimg.csv", "w+");
do{
	echo "\n<br />current page: {$currentPage}<br />\n";
	$products->setCurPage($currentPage);
    $products->load();
	foreach($products as $_product){
		$qty = 0;
		
		$__product = Mage::getModel('catalog/product')->load($_product->getId());	
		$images = $__product->getMediaGalleryImages();
		foreach($images as $image){
			if(is_file($image->getData("path"))){
				$data = (getimagesize($image->getData("path")));
				if($data[0] < 1000 || $data[1] < 1000){
					if($_product->getTypeId() =="configurable"){
						$subprods = $_product->getTypeInstance()->getUsedProducts();
						foreach($subprods as $subprod){
							if($subprod->getStockItem()->getIsInStock()){
								$qty = $qty + $subprod->getStockItem()->getQty();
							}
							
						}
					}else{
						if($_product->getStockItem()->getIsInStock()){
							$qty = $_product->getStockItem()->getQty();
						}
					}
					//var_dump($_product->getSku()." - ".$image->getData("url") ." - ".$data[0]."x".$data[1]);
					
					fputcsv($f, array($__product->getSku(), $__product->getActualModel(), $__product->getName(), $data[0],$data[1], $qty, $image->getData("url")));
				}
			}
			//var_dump($image);exit;
		}
	}
	$currentPage++;
    //make the collection unload the data in memory so it will pick up the next page when load() is called.
	$products->clear();
}while ( $currentPage <= $pages);
fclose($f);
echo "\n<br />Updated products: $count -  ERRORS: $errors\n";

