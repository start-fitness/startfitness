<?php
// More Mile Homepage Builder - To be run via CRON
// Designed and built by James Simpson
// V 1.0.0 BETA

error_reporting(E_ALL);

// First connect up to Mage
require(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento (CRON)
//require($_SERVER['DOCUMENT_ROOT'].'/app/Mage.php');  //Path to Magento (CRON)
umask(0);
Mage::app();

// Get the SQL Read/Write Setup
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');
$write = $resource->getConnection('core_write');

// Work out the dates need (from yesterday at 9:00 till 8:59 today)
$datemod = strtotime("-24 hours", time());
$datemodweek = strtotime("-24 hours", time());

$today = date('Y-m-d 08:59:59');
$yesterday = date('Y-m-d 09:00:00', $datemod);
$week = date('Y-m-d 09:00:00', $datemodweek);

// Set the homepage category ID
$cat_id = "3139";

// The SQL query to get the sales 
//$sql_getprod = "SELECT * FROM `review`"; // Test
$sql_getprod = "SELECT SFO.store_id,SFOI.`product_id`,SFOI.`name`,sum(SFOI.qty_ordered) as 'count' FROM `sales_flat_order` SFO 
		  left JOIN `sales_flat_order_item` SFOI on SFO.`entity_id`=SFOI.`order_id` 
		  WHERE SFO.`store_id` = 28 
     		  AND SFOI.`parent_item_id` IS NULL 
		  AND SFO.`created_at` BETWEEN '".$yesterday."' AND '".$today."' 
		  AND SFO.`customer_group_id` NOT IN (2,6,7) 
		  GROUP BY SFOI.`product_id` ORDER BY `count` DESC LIMIT 20";

// Fetch all reqults
$results = $read->fetchAll($sql_getprod);

if(count($results) < 20)
{
	echo "here";
	$sql_getprod = "SELECT SFO.store_id,SFOI.`product_id`,SFOI.`name`,sum(SFOI.qty_ordered) as 'count' FROM `sales_flat_order` SFO
                  left JOIN `sales_flat_order_item` SFOI on SFO.`entity_id`=SFOI.`order_id`
                  WHERE SFO.`store_id` = 28
                  AND SFOI.`parent_item_id` IS NULL
                  AND SFO.`created_at` BETWEEN '".$week."' AND '".$today."'
                  AND SFO.`customer_group_id` NOT IN (2,6,7)
                  GROUP BY SFOI.`product_id` ORDER BY `count` DESC LIMIT 20";

$results = $read->fetchAll($sql_getprod);

}

// Print out all results
//print_r($results);
//exit;

// Delete Current Items

	$sql_delete = "DELETE FROM `catalog_category_product` WHERE `category_id` = '".$cat_id."'";
	echo $sql_delete;
	$write->query($sql_delete);
	echo "<br>";

// Add New Items
$count = 0;
$number = 1;
foreach($results as $value){
	if($count >= 4){
		$count = 0;
		$number++ ;
	}
	
	// More Mile Category 
	$sql_insert = "INSERT INTO `catalog_category_product` (`category_id`,`product_id`,`position`) VALUES ('".$cat_id."','".$value['product_id']."','".$number."')";
	echo $sql_insert;
	$write->query($sql_insert);
	$count++ ;
	echo "<br>";
}

$html = "Successfully addded products to More Mile Homepage";

echo $html;

//Receiver Details
$sendToEmailAddress = array('geoffrey@dimasoft.co.uk');

$mail = Mage::getModel('core/email');
$mail->setSubject('MM Homepage Script Ran '.date("F j, Y, g:i a"));
$mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
$mail->setFromName(Mage::getStoreConfig('trans_email/ident_general/name'));
$mail->setType('text');
$mail->setBodyText($message);
$mail->setToName($sendToEmailAddress);
$mail->setToEmail($sendToEmailAddress);
$mail->send();

?>
