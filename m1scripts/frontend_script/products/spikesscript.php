<?php
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');
$idsmap = array(
"715" => array("728", "741"),
"716" => array("729", "742"),
"720" => array("733", "746"),
"1164" => array("740", "1174"),
"1165" => array("739", "1175"),
"1166" => array("738", "1176"),
"1167" => array("737", "750"),
"1168" => array("736", "1177"),
"1169" => array("735", "1178"),
"1170" => array("734", "1179"),
"1171" => array("732", "1180"),
"1172" => array("731", "1181"),
"1173" => array("730", "1182"),
);
$category = new Mage_Catalog_Model_Category();
$category->load(715);

$collection = $category->getProductCollection()->setPageSize(255) ;
$collection->addAttributeToSelect('*');
$exccount = 0;

for ($i = 1; $i <= $collection->getLastPageNumber(); $i++) {
	if ($collection->isLoaded()) {
        $collection->clear();
        $collection->setPage($i);
    }
	foreach($collection as $product){
		//echo "Old: ";
		//var_dump($product->getCategoryIds());
		//echo "<br />";
		$currids = $product->getCategoryIds();
		
		$newarray = array();
		foreach($currids as $catid){
			if(isset($idsmap[$catid])){
				$newarray = array_merge($newarray, $idsmap[$catid]);
			}
		}
		
		//echo "New: ";
		//var_dump($product->getStoreId());
		//echo "<br /><br />";
		//$product = Mage::getModel('catalog/product')->load($product->getId());
		try{
			$pid = $product->getId();
			//var_dump("DELETE FROM catalog_category_product_dup WHERE product_id = ".$pid." AND category_id IN ('".implode(",", $newarray)."')");exit;
			$write->query("DELETE FROM catalog_category_product WHERE product_id = ".$pid." AND category_id IN (".implode(",", $newarray).")");
			$batchinsert = "";
			$catpos = 1;
			$newarray = array_unique($newarray);
			foreach($newarray as $insid){
				//var_dump($insid);
				$batchinsert .= '('.$insid.', '.$pid.', '.$catpos.'),';
				$catpos++;
			}
			//var_dump($batchinsert);exit();
			$batchinsert = rtrim($batchinsert, ",");
			$insertqry = "INSERT INTO catalog_category_product (category_id, product_id, position) VALUES $batchinsert";
			//var_dump($insertqry);exit;
			if($batchinsert != "") $write->query($insertqry);
			//echo $insertqry;exit;
			//$product->setCategoryIds($newarray);
			//$product->save();
			//$product->setStoreId(6);
			//$product->save();
			$exccount++;
		}
		catch(exception $e){
			echo 'Caught exception: ',  $e->getMessage(), "\n";
			exit;
			//var_dump($e);exit;
			$exccount++;
		}
	}
}
echo "finished - exc count : $exccount";
