<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$store_id = 0;
	// get all grouped products
Mage::app()->setCurrentStore($store_id);
// read the csv file, line by line
//echo str_ireplace("index.php/", "", "startdev.startcycles.co.uk/scripts/update-website1.csv"); die;
if (($handle =  fopen("/home/scadmin/domains/startcycles.co.uk/public_html/scripts/update-website2.csv", "r")) !== FALSE) {
	$headers = true;
	echo "<table>";
	$counter = 0;
	$success = 0;	
	while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
			echo "<tr><td><strong>Line Number: " . $counter .  "</strong></td>";
			unset($_product);
			//echo $data[0];
			$_product = Mage::getModel('catalog/product')->loadByAttribute('sku', $data[0]);
		    if($_product){
					// update existing record
					//$_product->setName($_product->getName() . "test");							
					$websiteids = $_product->getWebsiteIds();
					if(!in_array(3, $websiteids)){
						array_push($websiteids,3);
						$_product->setWebsiteIds($websiteids);
					}
					try {
					        $_product->save();
							$success++;
							echo "<td>" . $_product->getSku() . " | Product updated successfully.</td>";
					        
					    } catch (Exception $e){
							echo "<td>" . $_product->getSku() . " | " . $e->getMessage() . "</td>";
					}
				
		    }else{
		    	echo "<td><strong>PRODUCT NOT FOUND</strong></tr></td>";
		    }
			
		$counter++;
		/*if($counter == 2){
			break;
		} */  
	}
	echo "</table>";

	fclose($handle);
}

echo "Number of products successfully updated: " . $success;
