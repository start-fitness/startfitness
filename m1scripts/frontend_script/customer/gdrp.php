<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
Mage::app();
$collection = Mage::getResourceModel('customer/customer_collection');
$collection->addAttributeToFilter('gdrp_newsletter',1);
$collection->addAttributeToFilter('gdrp_emailed', array('or'=> array(
             0 => array( 'neq' => '1'),
             1 => array('is' => new Zend_Db_Expr('null')))
         ), 'left');
$collection->addAttributeToSelect('*');

foreach($collection as $customer){
	$customer = Mage::getModel('customer/customer')->load($customer->getEntityId());
	
	$storeId = $customer->getStoreId();
	$sender  = array(
        'name' => Mage::getStoreConfig('trans_email/ident_support/name', $storeId),
        'email' => Mage::getStoreConfig('trans_email/ident_support/email', $storeId)
    );
     $vars = array(
    'customername' => $customer->getName(),
    'storeurl' => Mage::getUrl('/', array(
	    '_store'=> $storeId,
	    
	)),
	'storeurlgdpr' => Mage::getUrl('/', array(
	    '_store'=> $storeId,
	    
	))."confirm-newsletter?customerid=".$customer->getEntityId(),
	'storename' => Mage::getStoreConfig('general/store_information/name', $storeId),
	'logosrc' =>   Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_SKIN).Mage::getStoreConfig('design/header/logo_src', $storeId)
    );
    $templateId = 59;
	var_dump($templateId, $sender, $customer->getEmail() , '', $vars, $storeId);
    
	//var_dump($customer->getName(), $customer->getData("gdrp_emailed"));
	$customer->setData("gdrp_emailed", "1");
	//var_dump($customer->getName(), $customer->getData("gdrp_emailed"));
	($customer->save());
	Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $customer->getEmail() , '', $vars, $storeId);
}
