<?php
use ourcodeworld\PNGQuant\PNGQuant;
require(dirname(__FILE__)."/vendor/autoload.php");
$rootdir = dirname(__FILE__)."/../../";
$cachedir = $rootdir."var/cache/imagecache/";
if(!is_dir($cachedir)) mkdir($cachedir, 0777, true);
$imgpath  = $_GET['id'];

if(!is_file($rootdir."".$imgpath)){
	echo "file not found";
}
else{
	
	$pathparts = pathinfo($imgpath);
	$ext = $pathparts['extension'];
	$imgcachedpath = $cachedir.$imgpath;
	$originalpath = $rootdir."".$imgpath;
	
	if(is_file($imgcachedpath)){
		
		if($ext == "jpg"){
			header("Content-type: image/jpeg");
		}
		elseif($ext == "png"){
			
			header('Content-Type: image/png');
		
		}
		echo file_get_contents($imgcachedpath);
	}
	else{
		
		//var_dump($pathparts);
		if($ext == "jpg"){
			if(!is_dir(dirname($imgcachedpath))) mkdir(dirname($imgcachedpath), 0777, true);
			$command = "jpegoptim -m85 --force --strip-all --all-progressive --dest='".dirname($imgcachedpath)."/"."' '".$originalpath."'";
			//var_dump($command);exit;
			$out = array();
			$status = null;
			exec ($command,$out,$status);
			//$content = implode("\n", $out);
			
			//file_put_contents($imgcachedpath, $content);
			//var_dump($out,$status);
			header("Content-type: image/jpeg");
			if(is_file($imgcachedpath))
				echo file_get_contents($imgcachedpath);
			else{
				echo file_get_contents($originalpath);
			}
		}
		elseif($ext == "png"){
			if(!is_dir(dirname($imgcachedpath))) mkdir(dirname($imgcachedpath), 0777, true);
			$instance = new PNGQuant();
			//$command = "pngquant - < ".."";
			$exit_code = $instance->setImage($originalpath)
			->setOutputImage($imgcachedpath)
			->setQuality(50,80)
			->execute();
			//echo $command;
			//exit;
			$command = "optipng -i0 -o2 '".$imgcachedpath."'";
			$out = array();
			$status = null;
			exec ($command, $out, $status);
			//var_dump($exit_code, $command, $instance->getErrorTable()[(string) $exit_code] );exit;
			header('Content-Type: image/png');
			if(!$exit_code){
				echo file_get_contents($imgcachedpath);
			}
			else{
				echo file_get_contents($originalpath);
			}
		
		}
	}
}
