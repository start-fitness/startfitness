<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
$idblock = "WMWTAC";
if(isset($_GET['blockid'])) $idblock = $_GET['blockid'];

Mage::app()->loadArea('frontend');

$layout = Mage::getSingleton('core/layout');

//load default xml layout handle and generate blocks
$layout->getUpdate()->load('default');
$layout->generateXml()->generateBlocks();

//get the loaded head and header blocks and output
$block = $layout->createBlock('cms/block');
$block->setBlockId($idblock);
echo ($block->toHtml());
?>