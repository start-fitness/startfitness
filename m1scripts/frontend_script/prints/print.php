<?php

require_once(dirname(__FILE__).'/../../app/Mage.php');
//require_once('/microcloud/domains/fitnes/domains/startfitness.co.uk/http/app/Mage.php');

$app = Mage::app();
$app->setCurrentStore(0);

if(isset($_GET['orderno'])){
	$order = Mage::getModel('sales/order')->load($_GET['orderno'], 'increment_id');
	if($order && isset($_GET['status'])){
		$order->setIsCustomerNotified(false);
		$order->addStatusHistoryComment('AUTO PRINT -- JOB ID: '.$_GET['jobid']." -- ".'STATUS: '.$_GET['status']);
		$order->save();
		echo json_encode(array("status"=>"OK", "messsage"=>"note added to order"));
	}
}
