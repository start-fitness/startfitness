<?php
ini_set('memory_limit', '1048M');
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);

$skus = array();
//$products = Mage::getModel('catalog/product')->getCollection()->addAttributeToSelect("manufacturer")->addAttributeToSelect("sku");

$brands = Mage::getModel('smashingmagazine_branddirectory/brand')->getCollection();
$brandids = array();
$oldbrandids = array();


//exit();
foreach($brands as $brand){
	$brand = $brand->getData();
	$brandids[strtolower($brand['name'])] = $brand['entity_id'];
}

$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'manufacturer');
if ($attribute->usesSource()) {
    $options = $attribute->getSource()->getAllOptions(false);
	foreach($options as $option){
		$oldbrandids[$option['value']] = strtolower($option['label']);
	}
}

/*foreach (Mage::app()->getWebsites() as $website) {
    foreach ($website->getGroups() as $group) {
        $stores = $group->getStores();
        foreach ($stores as $store) {
        $storeId = ($store->getData("store_id"));*/
			//var_dump($storeId);exit();
			//Mage::app()->setCurrentStore($storeId);
			$products = Mage::getResourceModel('catalog/product_collection')
			    ->addAttributeToSelect('*')->setPageSize(10000);
			$pages = $products->getLastPageNumber();
			//echo (string)$products->getSelect();
			echo "\n<br />found products: {$products->count()}<br />\n";
			echo "\n<br />found pages: {$pages}<br />\n";
			$count = 0;
			$currentPage = 1;
			do{
				$products->setCurPage($currentPage);
			    $products->load();
				foreach($products as $product){
						
					### SCRIPT TO RESET ITEM URL KEYS ###
					/*
					$name = $product->getName();
					$name = preg_replace('!\s+!', ' ', $name);
					$name = trim($name, " ");
					
					$model = $product->getData("actual_model");
					$model = preg_replace('!\s+!', ' ', $model);
					$model = trim($model, " ");
					
					$name = $name . "-".$model;
					$name = str_replace("  ", " ", $name);
					$name = preg_replace("/[^A-Za-z0-9]/", '-', $name);
					$name = strtolower($name);
					//var_dump($name);
					$product->setData("url_key", $name);
					try{
						$product->getResource()->saveAttribute($product, 'url_key');
					}
					catch(exception $e){
						
					}
					$count++;
					echo "$count Of Page $currentPage/$pages\n";
					*/
					### SCRIPT TO RESET ITEMS BRANDS ###
					
					//if($count > 10000000000000) break;
					try{
						echo $product->getData("manufacturer")." -- ".$brandids[$oldbrandids[$product->getData("manufacturer")]]." (SKU=".$product->getData("sku").")<br />\n";
						$product->setData('brand_id', $brandids[$oldbrandids[$product->getData("manufacturer")]])->getResource()->saveAttribute($product, 'brand_id');
						$count++;
						$skus[$product->getData("sku")] = $product->getData("sku");
					}
					catch(exception $e){
						echo "could not save manufacturer : ".$product->getData("manufacturer")." \n";
					}
					
				}
				$currentPage++;
			    //make the collection unload the data in memory so it will pick up the next page when load() is called.
				$products->clear();
			}while ( $currentPage <= $pages);
			/*;break;
		}
		;break;
    }
    $products->clear();
}*/
echo "\n<br />Updated products: $count - unique = ".count($skus);