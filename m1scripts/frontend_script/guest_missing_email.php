<?php

ini_set('display_startup_errors',1);

ini_set('display_errors',1);

error_reporting(-1);

ini_set('memory_limit', '1048M');

require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento

umask(0);

Mage::app()->setCurrentStore(0);
$resource = Mage::getSingleton('core/resource');
$read  = $resource->getConnection('core_read');

$sql  = "UPDATE `sales_flat_order` 
JOIN sales_flat_order_address addr on addr.parent_id = sales_flat_order.entity_id AND addr.address_type = \"billing\"
SET sales_flat_order.customer_email = addr.email, sales_flat_order.customer_firstname = addr.firstname, sales_flat_order.customer_lastname = addr.lastname
WHERE `customer_email` IS NULL  AND created_at > DATE_SUB(NOW(), INTERVAL 1 MONTH)
";
$read->query($sql);