<?php
set_time_limit(200000);
ini_set('memory_limit', '4024M');
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
Mage::app()->setCurrentStore(6);
//exit();
$f = fopen("cust-parsed2.csv", "r");
$f2 = fopen("countries.csv", "r");
$countries = array();
while($line = fgetcsv($f2)){
	if($line[0] != "" && $line[1] != ""){
		$countries[strtolower($line[0])] = $line[1];
	}
}
fclose($f2);
$header = true;
$headers = array();
$data = array();
$ii=0;
while($line = fgetcsv($f)){
	if($header){
		$header = false;
	}
	else{
		
		$email = $line[1];
		$name = explode("{||}", $line[4]);
		$firstname = $name[0]; //Customer's firstname
		$lastname = $name[1]; //Customer's lastname
		$pwd = $line[3]; //auto-generated password length
		
		//if($email !="vincenterizzo@gmail.com") continue;
		//var_dump($line);
		
		$customer = Mage::getModel('customer/customer');
		$customer->setWebsiteId(3);
		$customer->loadByEmail($email);
		
		if(!$customer->getId())
		{
			$customer->setEmail($email);
			$customer->setFirstname($firstname);
			$customer->setLastname($lastname);
			$customer->setPassword($pwd);
			echo "customer created sucessfully - $email\n\n";
			    
			
			try
			{
				$customer->save();
			}
			catch(Exception $e)
			{
				print_r($e->__toString());echo "\n\n";
			}
			$address = Mage::getModel("customer/address");
			// you need a customer object here, or simply the ID as a string.
			$address->setCustomerId($customer->getId())
					->setIsDefaultBilling('1')
	           	 	->setIsDefaultShipping('1')
	            	->setSaveInAddressBook('1');
			$address->setFirstname($customer->getFirstname());
			$address->setLastname($customer->getLastname());
			$tmppays = strtolower($line[9]);
			$countrycode = (isset($countries[$tmppays]))?$countries[$tmppays]:"GB";
			$address->setCountryId($countrycode); //Country code here
			$address->setStreet($line[5]);
			$address->setPostcode($line[8]);
			$address->setCity($line[6]);
			$address->setTelephone($line[2]);
			 
			try
			{
				$address->save();
			}
			catch(Exception $e)
			{
				print_r($e->__toString());echo "\n\n";
				
			}
		}else{
			echo "customer already exists - $email\n\n";
			$cdata = ($customer->getData());
			if($cdata['website_id'] == "3"){
				//var_dump($cdata);
				$customer->setFirstname($firstname);
				$customer->setLastname($lastname);
				try
				{
					$customer->save();
				}
				catch(Exception $e)
				{
					print_r($e->__toString());echo "\n\n";
				}
				
				foreach ($customer->getAddresses() as $address)
			     {
			        $address->delete();
			     }
				 $address = Mage::getModel("customer/address");
				// you need a customer object here, or simply the ID as a string.
				$address->setCustomerId($customer->getId())
						->setIsDefaultBilling('1')
		           	 	->setIsDefaultShipping('1')
		            	->setSaveInAddressBook('1');
				$address->setFirstname($customer->getFirstname());
				$address->setLastname($customer->getLastname());
				$tmppays = strtolower($line[9]);
				$countrycode = (isset($countries[$tmppays]))?$countries[$tmppays]:"GB";
				$address->setCountryId($countrycode); //Country code here
				$address->setStreet($line[5]);
				$address->setPostcode($line[8]);
				$address->setCity($line[6]);
				$address->setTelephone($line[2]);
				try
				{
					$address->save();
				}
				catch(Exception $e)
				{
					print_r($e->__toString());echo "\n\n";
					
				}
				 //exit();
			}
			
		}
	}
	$ii++;
	//if($ii > 20)exit();
}