<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require ("geoip.inc");
$allowed_country_ids = array(
									'BE','BG','CZ','DK','DE','EE','IE','GR','ES','FR','HR','IT','CY','LV',
									'LT','LU','HU','MT','NL','AT','PL','PT','RO','SI','SK','FI','SE','GB'
									); 
$ip = $_SERVER['REMOTE_ADDR'];
if(isset($_POST['IP'])) $ip = $_POST['ip'];
$gi = geoip_open("GeoIP.dat",GEOIP_STANDARD);

$resp = array(
"CODE"	=>  geoip_country_code_by_addr($gi, $ip),
"NAME"	=>  geoip_country_name_by_addr($gi, $ip),
"IP"	=>	$ip,
"ALLOW"	=>	in_array(geoip_country_code_by_addr($gi, $ip), $allowed_country_ids)
);

echo json_encode($resp);