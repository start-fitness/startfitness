<?php
ini_set('memory_limit', '20000M');
require_once('/home/scadmin/domains/startcycles.co.uk/public_html/app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$store_id = 6;
$id = 600163438;
// get all grouped products
Mage::app()->setCurrentStore($store_id);
?>

<?php
// GOOGLE ANALYTICS ECOMMERCE JS
$order = Mage::getModel('sales/order')->loadByIncrementId($id);
$total = $order->getBaseGrandTotal();
//var_dump($order->getBaseTaxAmount());exit;
$shipping = $order->getShippingAmount();
$tax = $order->getBaseTaxAmount();
$store = Mage::app()->getStore();
$name = $store->getName();

//RW Debug Code

//$to = "richard@dimasoft.co.uk";
//$subject = "Order: ".$this->getOrderId();
//$body = print_r($order,true);
//$headers = 'From: sales@startfitness.co.uk' . "\r\n" .
//    'errors-to: richard@dimasoft.co.uk' . "\r\n" .
//    'X-Mailer: PHP/' . phpversion();
//
//mail($to, $subject, $body, $headers);

//END RW

$items = $order->getAllVisibleItems();
$item_details = array() ;
foreach($items as $item) {
$det = array() ;
$det['sku'] = $item->getSku() ;
$det['id'] = $item->getProductId() ;
$det['name'] = $item->getName() ;
$det['category'] = get_category_name($item->getProductId()) ;
$det['price'] = $item->getBaseRowTotal() ;
//var_dump($item->getBaseRowTotal());exit;
$det['quantity'] = number_format($item->getQtyOrdered(), 0) ;
array_push($item_details, $det) ;

}

function get_category_name($productId) {
$product = Mage::getModel('catalog/product')->load($productId);
$category_name = "" ;
$cats = $product->getCategoryIds();

$cnt = 0 ;
foreach ($cats as $category_id) {
$_cat = Mage::getModel('catalog/category')->load($category_id) ;
$cnt++ ;
if($cnt == count($cats))
$category_name.=$_cat->getName() ;
else
$category_name.=$_cat->getName()."," ;
}
return $category_name ;
}

function getItemJs(&$transId, &$item, $store_id) {
	/*$cats = explode(",", $item['category']);
	foreach($cats as $key => $iteml) {if($iteml == "") unset($cats[$key]);}
	$cats = array_values($cats);
	$firstcat = str_replace("'", "\\'",$cats[count($cats)-1]);
	$cat = str_replace("'", "\\'", $item['category']);*/
	$pid = ($item['id']);
	$attr = Mage::getResourceModel('catalog/product')->getAttributeRawValue($pid, 'source', $store_id);
	$product = Mage::getModel('catalog/product')
     ->setStoreId($store_id)
     ->setData('source',$attr); 
	$optionLabel = str_replace("'", "\\'",$product->getAttributeText('source'));
	if($optionLabel == "") $optionLabel = "NOT DEFINED";
	//var_dump($optionLabel);exit;

	
return <<<HTML
ga('ecommerce:addItem', {
'id': '$transId',
'name': '{$item['name']}',
'sku': '{$item['sku']}',
'category': '{$optionLabel}',
'price': '{$item['price']}',
'quantity': '{$item['quantity']}'
});
HTML;
}

?>
<script type="text/javascript">
ga('ecommerce:addTransaction', {
'id': '<?php echo $id; ?>', // Transaction ID. Required
'affiliation': '<?php echo $name ?>', // Affiliation or store name
'revenue': '<?php echo $total; ?>', // Grand Total
'shipping': '<?php echo $shipping; ?>', // Shipping
'tax': '<?php echo $tax; ?>' // Tax
});
// to get items
<?php
foreach ($item_details as &$item) {
echo getItemJs($id, $item, $store_id);
}
?>
ga('ecommerce:send');
</script>


