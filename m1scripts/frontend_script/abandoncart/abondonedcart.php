<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '1048M');
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app()->setCurrentStore(6);
$write = Mage::getSingleton('core/resource')->getConnection('core_write');

$date4h = date("Y-m-d H:i:s",strtotime("-6 hours"));
$date8h = date("Y-m-d H:i:s",strtotime("-24 hours"));
$date24h = date("Y-m-d H:i:s",strtotime("-48hours"));
$datecutoff = "2017-01-26 06:00:00";

$orders = (get4horders($write, $date4h, $date8h, $date24h, $datecutoff));
//var_dump($orders);exit;
sendFirstEmail($write, $orders);

$orders = (get8horders($write, $date4h, $date8h, $date24h, $datecutoff));
send2ndEmail($write, $orders);

exit;
$orders = (get24horders($write, $date4h, $date8h, $date24h, $datecutoff));
send3rdEmail($write, $orders);

function sendFirstEmail($write, $orders){
	
	if(count($orders) > 0){
		foreach($orders as $order){
			
			$quote = Mage::getModel('sales/quote')->getCollection()
    		->addFieldToFilter('entity_id', $order['entity_id'])->getFirstItem();
			//var_dump(($quote->getData("items_count")));
			if($quote->getData("items_count") < 1) continue;
			//continue;
			$custemail = $quote->getData("customer_email");
			if(empty($custemail)) continue;
			
			$storeId = Mage::app()->getStore()->getStoreId();
			
			$sender  = array(
		        'name' => Mage::getStoreConfig('trans_email/ident_support/name', 6),
		        'email' => Mage::getStoreConfig('trans_email/ident_support/email', 6)
		    );
		     $vars = array(
		    'quote' => $quote,
		    'cart_items' => quoteProductsString($quote)
		    );
			//exit;
		    $templateId = 50;
			echo "$custemail\n";
		    Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $custemail , '', $vars, $storeId);
			//Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, "geoffrey@dimasoft.co.uk" , '', $vars, $storeId);
			try{
				$sql = "INSERT INTO dimasoft_abcart_state (quote_id, `state`) VALUES(".$quote->getEntityId().", 1)";
				$write->query($sql);
			}
			catch(Exception $e){
				
			}
			//var_dump($quote->getEntityId());exit;
		}
	}
	
}
function send2ndEmail($write, $orders){
	if(count($orders) > 0){
		foreach($orders as $order){
			
			$quote = Mage::getModel('sales/quote')->getCollection()
    		->addFieldToFilter('entity_id', $order['entity_id'])->getFirstItem();
			//var_dump(($quote->getData("items_count")));
			if($quote->getData("items_count") < 1) continue;
			//continue;
			$custemail = $quote->getData("customer_email");
			if(empty($custemail)) continue;
			
			$codes = getCodes($write);
			if(count($codes) < 1) continue;
			//var_dump($codes);
			$storeId = Mage::app()->getStore()->getStoreId();
			$sender  = array(
		        'name' => Mage::getStoreConfig('trans_email/ident_support/name', 6),
		        'email' => Mage::getStoreConfig('trans_email/ident_support/email', 6),
		        
		    );
		     $vars = array(
		    'quote' => $quote,
		    'cart_items' => quoteProductsString($quote),
		    "coupon_code" => $codes[0]
		    );
			//exit;
		    $templateId = 49;
		    Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $custemail , '', $vars, $storeId);
			//Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, "geoffrey@dimasoft.co.uk" , '', $vars, $storeId);
			try{
				$sql = "UPDATE dimasoft_abcart_state SET`state` = 2 WHERE quote_id = ".$quote->getEntityId();
				//var_dump($sql);
				$write->query($sql);
			}
			catch(Exception $e){
				//var_dump($e->getMessage());
			}
			//var_dump($quote->getEntityId());exit;
		}
	}
}
function send3rdEmail($write, $orders){
	
}

function quoteProductsString($quote){
	$str = "";
	foreach ($quote->getAllItems() as $item) {
		//var_dump($item->getData("name"));exit;
	    if ($item->getParentItemId()) {
	        
	    }
		else{
			$str .= '<tr><td style="vertical-align: middle;"><img style="vertical-align: middle; margin-right: 20px;" src="'.Mage::helper('catalog/image')->init($item->getProduct(), 'thumbnail')->resize(75).'" />'.$item->getQty() ." x " .$item->getName()."</td></tr>\n";
		}
		
	}
	return ('<table>'.$str.'</table>');
}

function get4horders($write, $date4h, $date8h, $date24h, $cutoff){
	$sql = "
SELECT quote.* FROM `sales_flat_quote` quote
LEFT JOIN sales_flat_order ordert on ordert.quote_id = quote.entity_id
LEFT JOIN dimasoft_abcart_state abcart ON abcart.quote_id = quote.entity_id 
LEFT JOIN (
	SELECT COUNT(*) as countp, quote_id FROM sales_flat_quote_item qoj 
    JOIN catalog_product_entity pej ON pej.entity_id = qoj.product_id
    JOIN `sales_flat_quote` qj ON qj.entity_id = qoj.quote_id
    WHERE pej.attribute_set_id NOT IN (9,34)
    AND qj.updated_at <= '$date4h' AND qj.updated_at >= '$date8h' 
    GROUP BY quote_id
) as cycleprod ON cycleprod.quote_id = quote.entity_id
WHERE ordert.entity_id IS NULL AND abcart.id IS NULL AND quote.updated_at <= '$date4h' AND quote.updated_at >= '$date8h' 
AND quote.updated_at >= '{$cutoff}' AND (cycleprod.countp IS NOT NULL AND cycleprod.countp > 0) AND quote.store_id IN (6,19)
	";
	//echo $sql;
	$results = $write->fetchAll($sql);
	return $results;
}
function get8horders($write, $date4h, $date8h, $date24h, $cutoff){
	$sql = "
SELECT quote.* FROM `sales_flat_quote` quote
LEFT JOIN sales_flat_order ordert on ordert.quote_id = quote.entity_id
LEFT JOIN dimasoft_abcart_state abcart ON abcart.quote_id = quote.entity_id 
LEFT JOIN (
	SELECT COUNT(*) as countp, quote_id FROM sales_flat_quote_item qoj 
    JOIN catalog_product_entity pej ON pej.entity_id = qoj.product_id
    JOIN `sales_flat_quote` qj ON qj.entity_id = qoj.quote_id
    WHERE pej.attribute_set_id NOT IN (9,34)
    AND qj.updated_at >= '$date24h' AND qj.updated_at <= '$date8h' 
    GROUP BY quote_id
) as cycleprod ON cycleprod.quote_id = quote.entity_id
WHERE ordert.entity_id IS NULL AND abcart.state = 1  AND quote.updated_at >= '$date24h' AND quote.updated_at <= '$date8h' 
AND quote.updated_at >= '{$cutoff}' AND (cycleprod.countp IS NOT NULL AND cycleprod.countp > 0) AND quote.store_id IN (6,19)
	";
	//echo $sql;
	$results = $write->fetchAll($sql);
	return $results;
}
function get24horders($write, $date4h, $date8h, $date24h, $datecutoff){
	$sql = "
SELECT quote.* FROM `sales_flat_quote` quote
LEFT JOIN sales_flat_order ordert on ordert.quote_id = quote.entity_id
LEFT JOIN dimasoft_abcart_state abcart ON abcart.quote_id = quote.entity_id 
WHERE ordert.entity_id IS NULL AND abcart.state = 2 AND quote.updated_at <= '$date24h' AND quote.updated_at >= '$datecutoff'	AND quote.store_id IN (6,19)
	";
	//echo $sql;
	$results = $write->fetchAll($sql);
	return $results;
}


function getCodes($write){
	
	// Get the rule in question
	$rule = Mage::getModel('salesrule/rule')->load(4244); //21 = ID of coupon in question
	
	// Define a coupon code generator model instance
	// Look at Mage_SalesRule_Model_Coupon_Massgenerator for options
	$generator = Mage::getModel('salesrule/coupon_massgenerator');
	
	$parameters = array(
	    'count'=>1,
	    'format'=>'alphanumeric',
	    'dash_every_x_characters'=>0,
	    'prefix'=>'FIVEOFF',
	    'suffix'=>'',
	    'length'=>12
	);
	
	if( !empty($parameters['format']) ){
	  switch( strtolower($parameters['format']) ){
	    case 'alphanumeric':
	    case 'alphanum':
	      $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHANUMERIC );
	      break;
	    case 'alphabetical':
	    case 'alpha':
	      $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_ALPHABETICAL );
	      break;
	    case 'numeric':
	    case 'num':
	      $generator->setFormat( Mage_SalesRule_Helper_Coupon::COUPON_FORMAT_NUMERIC );
	      break;
	  }
	}
	
	$generator->setDash( !empty($parameters['dash_every_x_characters'])? (int) $parameters['dash_every_x_characters'] : 0);
	$generator->setLength( !empty($parameters['length'])? (int) $parameters['length'] : 6);
	$generator->setPrefix( !empty($parameters['prefix'])? $parameters['prefix'] : '');
	$generator->setSuffix( !empty($parameters['suffix'])? $parameters['suffix'] : '');
	
	// Set the generator, and coupon type so it's able to generate
	$rule->setCouponCodeGenerator($generator);
	$rule->setCouponType( Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO );
	//var_dump(Mage_SalesRule_Model_Rule::COUPON_TYPE_AUTO );
	// Get as many coupons as you required
	$count = !empty($parameters['count'])? (int) $parameters['count'] : 1;
	$codes = array();
	for( $i = 0; $i < $count; $i++ ){
	  $coupon = $rule->acquireCoupon();
	  $code = $coupon->getCode();
	  $codes[] = $code;
	  $sql = "UPDATE salesrule_coupon SET type = 1, usage_limit = 1, usage_per_customer = 1 WHERE code = '{$code}'";
	  $write->query($sql);
	}
	return $codes;
}
