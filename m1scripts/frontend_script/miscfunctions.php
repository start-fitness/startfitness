<?php
ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');
require_once('../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();

$store_id = 0;
$passcode = '897df6gh45786yguihfbvg870tydr';
Mage::app()->setCurrentStore($store_id);
$filename = 'sageorders.csv';
if(isset($_GET['filename']) && $_GET['filename'] != ''){
	$filename = $_GET['filename'];
}
$filepath = 'sagepayments/' . $filename;
if(isset($_GET['passcode']) && $_GET['passcode'] == $passcode){
	checkOrdersCSV($filepath);
}
else{
	echo "Access Denied!";
}


function checkOrdersCSV($filepath){
	if (($handle =  fopen($filepath, "r")) !== FALSE) {
		echo "<table>";
		$counter = 0;
		$success = 0;	
		while (($data = fgetcsv($handle, 100000, ",")) !== FALSE) {
				if($counter >= 1){
					$data['vendortxcode'] = trim($data[2]);
					$data['order_number'] = trim($data[3]);
					$success = trim($data[4]);
					$data['amount'] = trim($data[5]);
					$data['currency'] = trim($data[6]);
				    if(checkSageOrders($data) && $success == 'FAILURE'){
							echo "<tr><td>PAYMENT FAILED BUT MAGENTO ORDER CREATED</td></tr>";
				    }
					elseif(!checkSageOrders($data) && $success == 'SUCCESS'){
						if(checkRepeatSageOrder($data)){
							echo "<tr><td>PAYMENT SUCCESS BUT NO MAGENTO ORDER >>>> REPEAT PAYMENT WAS SUCCESSFULL</td></tr>";
						}
						else{
							echo "<tr><td>PAYMENT SUCCESS BUT NO MAGENTO ORDER</td></tr>";
						}
					}
					else{
				    	echo "<tr><td>NORMAL</td></tr>";
			    	}
				}
			$counter++;
		}
		echo "</table>";
		fclose($handle);
	}
	else{
		echo "File not found!";
	}
}

function checkRepeatSageOrder($data){
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$sql = "SELECT entity_id 
			FROM sales_flat_order AS main_table 
			WHERE main_table.increment_id = '" . $data['order_number'] . "' 
			AND main_table.grand_total = " . $data['amount'] . " 
			AND main_table.order_currency_code = '" . $data['currency'] . "'";				  						  							  											  
	$result = $readConnection->fetchAll($sql);
	if(empty($result)){
		return false;
	}
	return true;
}

function checkSageOrders($data){
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$sql = "SELECT entity_id 
			FROM sales_flat_order AS main_table 
			JOIN sagepaysuite_transaction st ON main_table.entity_id = st.order_id 
			WHERE st.vendor_tx_code = '" . $data['vendortxcode'] . "'
			AND main_table.increment_id = '" . $data['order_number'] . "' 
			AND main_table.order_currency_code = '" . $data['currency'] . "'";			  						  							  											  
	$result = $readConnection->fetchAll($sql);
	if(empty($result)){
		return false;
	}
	return true;
}


