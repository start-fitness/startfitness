<?php
if (extension_loaded('newrelic')) { // Ensure PHP agent is available
   newrelic_disable_autorum();
}
require_once(dirname(__FILE__).'/../../app/Mage.php'); //Path to Magento
require(dirname(__FILE__).'/fpdf.php');
Mage::app()->setCurrentStore(0);
$increment = trim(Mage::app()->getRequest()->getParam('ref'), " ");
$printer = trim(Mage::app()->getRequest()->getParam('printer'), " ");
$rlabel = trim(Mage::app()->getRequest()->getParam('returnlabel'), " ");
$order = Mage::getModel('sales/order')->loadByIncrementId($increment);

if(count($order->getData()) == 0){
	?>
	<html>
	<body>
		<script>window.close()</script>
	</body>
	</html>
	<?php
}
?>

<html>
	<body>
		<iframe src="/script/orders/returnlabel.php?printer=<?=$_GET['printer']?>&ref=<?=$_GET['ref']?>&returnlabel=<?=$_GET['returnlabel']?>" width="1000px" height="1000px" type='application/pdf' />
	</body>
</html>
