<?php
set_time_limit(200000);
ini_set('memory_limit', '1024M');
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);
require_once('../../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
Mage::app()->setCurrentStore(6);
$f = fopen("reviews.csv", "r");
$header = true;
//var_dump($f);
$i = 0;
$found = 0;
$notfound = 0;
while($line = fgetcsv($f)){
	$productsarr = array();
	$beenfound = false;
	if($header) $header = false;
	else{
		$collection = Mage::getModel('catalog/product')->getCollection();
	
		$collection->addAttributeToSelect('cactu_pid');	
		$collection->addAttributeToSelect('type_id');	
		$collection->addFieldToFilter(array(
		array('attribute'=>'cactu_pid','eq'=>$line[1]),
		));	
		$collection->addAttributeToSort('type_id', 'DESC');
		if(count($collection) > 0):
			echo "REVIEW #{$line[0]} - PRODUCT FOUND!\n\n";
			$found++;
			$beenfound = true;
		else:
			echo "REVIEW #{$line[0]} - :( PRODUCT NOT FOUND! PID {$line[1]}\n\n";
			
			/*foreach ($collection as $product) {
				$productsarr[] = $product->getData();
			}*/
			// var_dump($productsarr);
		endif;
		if($beenfound){
			$product = array();
			$gotprod = false;
			foreach ($collection as $product2) {
				if(!$gotprod) $product = $product2;
				else break;
				//var_dump($product);
				
				//break;
			}
			$product = $product->getData();
			var_dump($product['type_id']);
			$customer = Mage::getModel('customer/customer');
			$customer->setWebsiteId(3);
			
			$customer->loadByEmail($line[9]);
			if(!$customer->getId()) $uid = null;
			else $uid = $customer->getId();
			
			$review = Mage::getModel('review/review');
			$review->setEntityPkValue($product['entity_id']);//product id
			$review->setStatusId(1);
			$review->setTitle($line[5]);
			$review->setDetail($line[6]);
			//var_dump((int)$line[7]);
			
			$review->setEntityId(1);                                      
			$review->setStoreId(6);                    
			$review->setStatusId(1); //approved
			$review->setCustomerId($uid);//null is for administrator
			$review->setNickname($line[8]);
			$review->setReviewId($review->getId());
			$review->setStores(array(6));                    
			//
			//$review->setUserIp("127.0.0.1");
			//$review->setUserAgent("firefox"); 
			//var_dump($review->getData());exit;
			try
			{
				$review->save();
				$review->aggregate();
				if((int)$line[7] > 0):
					$rating_options = array("Quality_"=>1, "Value_"=>2, "Price_"=>3);
					foreach($rating_options as $index=> $rating_id):					    
				        $_rating = Mage::getModel('rating/rating')
				            ->setRatingId($rating_id)
				            ->setReviewId($review->getId())
				            ->setCustomerId($uid)
				            ->addOptionVote(((int)$line[7]*$rating_id),$product['entity_id']);
					    
					endforeach;
					
					$review->aggregate();
				endif;
			}
			catch(Exception $e)
			{
				print_r($e->__toString());echo "\n\n";
			}
		}
	}
	$i++;
	//if($i > 5) exit();
	flush();
	
}
echo "FOUND $found OUT OF $i";
?> 