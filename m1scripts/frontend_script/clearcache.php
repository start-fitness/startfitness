<?php

ini_set('display_startup_errors',1);

ini_set('display_errors',1);

error_reporting(-1);

ini_set('memory_limit', '1048M');

require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento

umask(0);

Mage::app()->setCurrentStore(0);

if (Extendware::helper('ewpagecache/api')) {
     // this code will only run if the api is available
     (Mage::helper('ewpagecache/api')->flushPagesByGroup(array("catalogsearch-result-index")));
}