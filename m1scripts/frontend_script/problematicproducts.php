<?php
ini_set('memory_limit', '20000M');
require_once(dirname(__FILE__).'/../app/Mage.php'); //Path to Magento
umask(0);
Mage::app();
$store_id = 0;
// get all grouped products
Mage::app()->setCurrentStore($store_id);

	
$corruption_error = checkCorruptProducts();
$no_categories_collection = checkProductsNoCategory();


if(isset($corruption_error)){
	if(!empty($corruption_error)){
		echo "The following products were found to be corrupted: " . implode(',', $corruption_error);
		send_general_email($corruption_error, 'corruption');
	}
	echo "\r\nNumber products scanned: " . $counter;
}

if(!empty($no_categories_collection)){
	echo "The following products are not assigned to any categories: ";
	send_general_email($no_categories_collection, 'no_categories');
}


function checkCorruptProducts(){
	$collection = Mage::getModel('catalog/product')->getCollection()
	->addAttributeToSelect('entity_id')
	//->addAttributeToFilter('visibility', array('neq' => 1))
	->addAttributeToFilter('type_id', 'configurable');
	
	$counter = 0;
	// get all visible products
	$products_error = array();
	foreach($collection as $item){
		$counter++;
		unset($product);
		$product = Mage::getModel('catalog/product')->load($item->getId());
		if($product){
			echo "\r\n" . $counter . ") Product ID: " . $product->getId();
		    $_attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);
		    foreach($_attributes as $_attribute){
		    		$product_attribute = $_attribute->getProductAttribute();
					if(!$product_attribute){
						$products_error[] = $product->getId();
						echo " - Product Corrupted";
					}
	
		    }
		}
	}
	return $products_error;
}

 function checkProductsNoCategory(){
 	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');
	$sql = "SELECT cpe.entity_id, cpe.sku
			FROM catalog_product_entity as cpe
			LEFT JOIN catalog_category_product as ccp ON cpe.entity_id = ccp.product_id 
			JOIN catalog_product_entity_int cpei_visibility ON (cpe.entity_id = cpei_visibility.entity_id AND cpei_visibility.attribute_id = 102 AND value != 1)
			WHERE ccp.category_id IS NULL GROUP BY cpe.entity_id";				  						  							  											  
	$result = $readConnection->fetchAll($sql);
 	return $result;
 } 	

 function send_general_email($message_array, $error_type){

     list($toEmail,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('problemproducts');

				   $headers = "MIME-Version: 1.0" . "\r\n";
				   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				   $headers .= 'From: <magento@startfitness.com>' . "\r\n";
				   $headers .= 'Cc: ' . $ccEmailAddresses . "\r\n";

				   $subject = '';
				   $message = '';

				   switch ($error_type) {

					    case 'corruption':

								$subject = "Magento Corrupted Products Alert";

						        $message .= '

								 <html>

								 <head>

								  <title>

								  </title>

								 </head>

								 <body>

								<table width="600">

								  <tr><td>Corrupted products have been discovered from the last scheduled product check:</td></tr>

								  <tr><td></td></tr>';

								    
							$counter = 1;
							foreach($message_array as $error){

				  			$message .= "<tr><td>" . $counter . ") " . $error . '</td></tr>';
				  			$counter++; 

				  			}	    

							$message .=	'

								<tr><td></td></tr>

								</table>

								 </body>

								</html>';

					        break;
					        
					   case 'no_categories':

								$subject = "Magento Products Not Assigned to Categories";

						        $message .= '

								 <html>

								 <head>

								  <title>

								  </title>

								 </head>

								 <body>

								<table width="600">

								  <tr><td>' . count($message_array) . ' products with no categories have been discovered from the last scheduled product check:</td></tr>

								  <tr><td></td></tr>';

								    
							$counter = 1;
							foreach($message_array as $error){
								//print_r($message_array); die;
							$sku = ($error['sku'] == '' ? 'NO SKU' : $error['sku']);

				  			$message .= '<tr><td>' . $counter . ') ID: ' . $error['entity_id'] . ' | '. $sku . '</td></tr>';
				  			$counter++; 

				  			}
							
							$message .=	'

								<tr><td></td></tr>

								</table>

								 </body>

								</html>';	    

					        break;
						
					}	
					
					//echo $message; die;
					mail($toEmail,$subject,$message,$headers);
			}

?>