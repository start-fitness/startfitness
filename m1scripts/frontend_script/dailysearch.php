<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

ini_set('memory_limit', '3024M');
require_once('../app/Mage.php'); //Path to Magento
require 'phpmailer/class.phpmailer.php';
umask(0);
Mage::app();
/**
 * Set the store id
 */
$store_id = 6;
/**
 * Set the website id
 */
$website_id = 3;


$stores = array(
	array("Start Fitness Desktop", 6, 3),
	array("Start Fitness Mobile", 19, 3),
        array("Sports Size", 28, 9)
);

foreach ($stores as $store) {
	call_user_func_array('generate',$store);
}



function generate($store_code, $store_id, $website_id) {
	/**
	 * Get the resource model
	 */
	$resource = Mage::getSingleton('core/resource');
	
	/**
	 * Retrieve the read connection
	 */
	$readConnection = $resource->getConnection('core_read');
	
	$query = "SELECT query_text, popularity, num_results, updated_at FROM `catalogsearch_query` WHERE `updated_at` > CURRENT_DATE() and store_id = ".$store_id." ORDER BY `updated_at` DESC ";
	
	/**
	 * Execute the query and store the results in $results
	 */
	$results = $readConnection->fetchAll($query);
	
	/**
	 * Print out the results
	 */
	ob_start();
	?>
	<html>
		<head>
			  <!-- Compiled and minified CSS -->
			  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/css/materialize.min.css">
			
			  <!-- Compiled and minified JavaScript -->
			  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.3/js/materialize.min.js"></script>
		</head>
		<body> 
			<?php echo "<pre>Total: ".count($results)."</pre>"; ?>
			<?php if(count($results) > 5000) {
				echo "<pre>Max 5000 rows in preview. ".(count($results) - 500)." items hidden.</pre>";
			} ?>
			
			<br />
			<table>
				<tr>
					<th>Query</th>
					<th>Popularity</th>
					<th>Num Results</th>
					<th>Date / Time</th>
				</tr>
			<?php 
			foreach ($results as $row) {
				?>
				<tr>
					<?php
                    $count = 0;
                    foreach ($row as $key => $value) {
						if($count <= 5000) {
							echo "<td>".$value."</td>";
						} else {
							break;
						}
						$count++;
					} ?>
				</tr>
				<?php
			}
			?>
			</table>
	
		</body>
	</html>
	
	<?php
	$body = ob_get_contents();
	ob_end_clean();
	
	
	$file = fopen($store_code."_search.csv","w");
	
	$result = array("Query", "Popularity", "Num Results", "Date / Time");
	 fputcsv($file, $result);
	foreach ($results as $row) {
	    $result = array();
	    array_walk_recursive($row, function($item) use (&$result) {
	        $result[] = $item;
	    });
	    fputcsv($file, $result);
	}
	fclose($file);
	
	
	try{

        list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('dailysearch',true);

        $mail = new PHPMailer(); // defaults to using php "mail()"
			
		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'dimasoft-development.co.uk';  // Specify main and backup server
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'smtp@charlie.dimasoft-development.co.uk';                            // SMTP username
		$mail->Password = 'ISpNlbTctT';     
		$mail->CharSet = 'utf-8';
		// $mail->AddReplyTo("charlie@dimasoft.co.uk", "Charlie Stelling");
		
		$mail->SetFrom('noreply@startfitness.co.uk', $store_code.'_cron');
		
		// $mail->AddReplyTo("name@yourdomain.com","First Last");

        foreach($toEmailAddresses as $toEmailAddress){
            $mail->AddAddress($toEmailAddress, $toEmailAddress);
        }

        foreach($ccEmailAddresses as $ccEmailAddress){
            $mail->AddCC($ccEmailAddress, $ccEmailAddress);
        }
		
		$mail->Subject    = $store_code." Search Results";
		
		$mail->isHTML(true);
		$mail->MsgHTML($body);
		
		if(file_exists($store_code."_search.csv")) {
			$mail->AddAttachment($store_code."_search.csv");
		}
		
		if(!$mail->Send()) {
		  throw new Exception("Mailer Error: " . $mail->ErrorInfo);
		} else {
		  echo "Message sent !<br/>";
		}
	
	} catch(exception $e) {
		echo $e->getMessage();
	}
}
