define([
    'jquery',
    'izimodal',
    //'autoheight',
    'Magento_Ui/js/modal/modal',
    'mage/loader',
    'Magento_Customer/js/customer-data',
    'mage/validation'
], function ($, modal, loader, customerData) {
    'use strict';

    return function(config, node) {

        var product_id = jQuery(node).data('id');
      //  alert(product_id);
        var product_url = jQuery(node).data('url');

        var options = {
            type: 'popup',
            responsive: true,
            innerScroll: false,
            title: $.mage.__('Quick View'),
            buttons: [{
                text: $.mage.__('Close'),
                class: 'close-modal',
                click: function () {
                    this.closeModal();
                }
            }]
        };

        //var popup = modal(options, $('#quickViewContainer' + product_id));

        var popup = $('#quickViewContainer' + product_id).iziModal(


        );

        $("#quickViewButton" + product_id).on("click", function () {
        //  alert('here');
            openQuickViewModal();
        });

        var openQuickViewModal = function () {
            var modalContainer = $("#quickViewContainer" + product_id);
            modalContainer.find(".iziModal-content").html(createIframe());


            var iframe_selector = "#iFrame" + product_id;
			$('body').trigger('processStart');
            $(iframe_selector).on("load", function () {
            	$('body').trigger('processStop');
            	var that = this;
                //modalContainer.addClass("product-quickview");
                modalContainer.iziModal('open');
                setTimeout(function(){

			    that.style.height =
			    (document.documentElement.clientHeight * 0.8) + 'px';


                }, 1000);

                observeAddToCart(iframe_selector, modalContainer);

                var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
		     	//console.log("test!"+w);

		     	var framecontent = $(iframe_selector).contents();
		     	framecontent.find(".rewardpointsproduct").not(':first').remove();
		     	if(w < 768){
		     		framecontent.find(".rewardpointsproduct").detach().insertAfter(framecontent.find(".box-tocart"));
		     	}
		     	else{
		     		framecontent.find(".rewardpointsproduct").detach().insertAfter(framecontent.find(".product-info-price"));
		     		if(framecontent.find(".product-options-wrapper").length > 0) framecontent.find(".product-info-stock-sku").detach().insertAfter(framecontent.find(".product-options-wrapper"));
		     	}
            });
        };

        var observeAddToCart = function (iframe_selector, modalContainer) {
            var doc = $(iframe_selector)[0].contentWindow.document;
			//console.log(doc);
			var dataForm = $(doc).find("#product-addtocart-button").closest("form");
        	dataForm.mage('validation', {
	        ignore: false ? ':hidden:not(' + ignore + ')' : ':hidden'
		    }).find('input:text').attr('autocomplete', 'off');
            $(doc).find("#product-addtocart-button").unbind("click").on('click', function (e) {


			    console.log(dataForm.validation('isValid'));
                if(!dataForm.validation('isValid'))
                	return;
                e.preventDefault();
                e.stopPropagation();
                console.log("test yey!",modalContainer );

                $('[data-block="minicart"]').trigger('contentLoading');
                $('body').trigger('processStart');
                //$(".close-modal").trigger("click");
              //  modalContainer.iziModal('close');

                $.ajax({
                    data: $(this).closest("form").serialize(),
                    type: $(this).closest("form").attr('method'),
                    url: $(this).closest("form").attr('action'),
                    success: function(response) {
                    //  console.log(response);
$('[data-block="minicart"]').find('[data-role="dropdownDialog"]').dropdownDialog("open");
                    },
                    complete: function(request, status) {
                    //  console.log(request);
                    //  console.log(status);
                        $('body').trigger('processStop');
                        $('.iziModal-overlay').hide();
                        $('.iziModal').hide();
                        //location.reload();

                    }


                });

                //console.log(data);
              //  console.log(success);

            });
        };

        var createIframe = function () {
            return $('<iframe />', {
                id: 'iFrame' + product_id,
                src: product_url + "?iframe=1"
            });
        }
    };
});
