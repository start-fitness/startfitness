<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\ShippingMatrixRates\Model\Carrier;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Framework\Serialize\Serializer\Json;

class Matrixrates extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements
    \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * @var string
     */
    protected $_code = 'matrixrates';

    /**
     * @var string
     */
    protected $_defaultConditionName = 'per_group';

    /**
     * @var array
     */
    protected $_conditionNames = [];

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $_rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $_resultMethodFactory;

    /**
     * @var \Mageside\ShippingMatrixRates\Model\ResourceModel\Carrier\MatrixratesFactory
     */
    protected $_matrixratesFactory;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * Matrixrates constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory
     * @param \Mageside\ShippingMatrixRates\Model\ResourceModel\Carrier\MatrixratesFactory $matrixratesFactory
     * @param Json $serializer
     * @param array $data
     * @throws LocalizedException
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $resultMethodFactory,
        \Mageside\ShippingMatrixRates\Model\ResourceModel\Carrier\MatrixratesFactory $matrixratesFactory,
        Json $serializer,
        array $data = []
    ) {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_resultMethodFactory = $resultMethodFactory;
        $this->_matrixratesFactory = $matrixratesFactory;
        $this->serializer = $serializer;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
        foreach ($this->getCode('condition_name') as $k => $v) {
            $this->_conditionNames[] = $k;
        }
    }

    /**
     * @param RateRequest $request
     * @return \Magento\Shipping\Model\Rate\Result
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function collectRates(RateRequest $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }

        if (!$request->getConditionName()) {
            $conditionName = $this->getConfigData('condition_name');
            $request->setConditionName($conditionName ? $conditionName : $this->_defaultConditionName);
        }

        /** @var \Magento\Shipping\Model\Rate\Result $result */
        $result = $this->_rateResultFactory->create();
        $rates = $this->getRate($request);

        if (!empty($rates)) {
            foreach ($rates as $rate) {
                /** @var \Magento\Quote\Model\Quote\Address\RateResult\Method $method */
                $method = $this->_resultMethodFactory->create();

                $method->setCarrier('matrixrates');
                $method->setCarrierTitle($this->getConfigData('title'));

                $method->setMethod(preg_replace('/[^A-Za-z0-9_]/', '_', strtolower(substr($rate['delivery_method'],0,240))));
                $method->setMethodTitle(__($rate['delivery_method']));

                $shippingPrice = $this->getFinalPriceWithHandlingFee($rate['price']);

                $method->setPrice($shippingPrice);
                $method->setCost($rate['cost']);

                $result->append($method);
            }
        } else {
            /** @var \Magento\Quote\Model\Quote\Address\RateResult\Error $error */
            $error = $this->_rateErrorFactory->create(
                [
                    'data' => [
                        'carrier' => $this->_code,
                        'carrier_title' => $this->getConfigData('title'),
                        'error_message' => $this->getConfigData('specificerrmsg'),
                    ],
                ]
            );
            $result->append($error);
        }

        return $result;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return array|bool
     */
    public function getRate(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        $rates = $this->_matrixratesFactory->create()->getRate($request);
        return $this->getPriorityRates($rates);
    }

    /**
     * @param string $type
     * @param string $code
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCode($type, $code = '')
    {
        $codes = [
            'condition_name' => [
                'per_item' => __('Calculate Price Per Item'),
                'per_row' => __('Calculate Price Per Row'),
                'per_group' => __('Calculate Price Per Shipping Group'),
                'highest_group'  => __('Calculate Highest Group Price'),
            ],
            'condition_name_short' => [
                'per_item' => __('Calculate Price Per Item'),
                'per_row' => __('Calculate Price Per Row'),
                'per_group' => __('Calculate Price Per Shipping Group'),
                'highest_group'  => __('Calculate Highest Group Price'),
            ],
        ];

        if (!isset($codes[$type])) {
            throw new LocalizedException(__('Please correct Matrix Rates code type: %1.', $type));
        }

        if ('' === $code) {
            return $codes[$type];
        }

        if (!isset($codes[$type][$code])) {
            throw new LocalizedException(__('Please correct Matrix Rates code for type %1: %2.', $type, $code));
        }

        return $codes[$type][$code];
    }

    /**
     * @return array
     */
    protected function getShippingMethodsPriority()
    {
        $options = $this->getConfigData('methods_priority');
        $options = empty($options) ? [] : $this->serializer->unserialize($options);

        $priority = [];
        foreach ($options as $option) {
            $code = str_replace(" ", "_", trim($option['method']));
            $priority[$code] = (int) $option['priority'];
        }

        return $priority;
    }

    /**
     * @param $rates
     * @return mixed
     */
    protected function getPriorityRates($rates)
    {
        if (empty($rates)) {
            return [];
        }

        $config = $this->getShippingMethodsPriority();
        if (empty($config)) {
            return $rates;
        }

        $ratesByPriority = [];
        $maxPriority = 0;
        foreach ($rates as $rate) {
            $code = str_replace(" ", "_", trim($rate['delivery_method']));
            $priority = !empty($config[$code]) ? $config[$code] : 0;
            $maxPriority = $priority > $maxPriority ? $priority : $maxPriority;
            if (empty($ratesByPriority[$priority])) {
                $ratesByPriority[$priority] = [];
            }
            $ratesByPriority[$priority][] = $rate;
        }

        return $ratesByPriority[$maxPriority];
    }

    /**
     * Get allowed shipping methods
     *
     * @return array
     */
    public function getAllowedMethods()
    {
        return ['bestway' => $this->getConfigData('name')];
    }
}
