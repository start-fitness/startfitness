<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */

namespace Mageside\ShippingMatrixRates\Helper;

class MergeResultHelper
{
    /**
     * @param $itemRateResult
     * @param $result
     * @param string $method sum_first|sum_last|highest
     * @return array
     */
    public function _mergeResults($itemRateResult, $result, $method = 'sum_first')
    {
        if (empty($result) && empty($itemRateResult)) {
            return [
                'all'       => [],
                'grouped'   => []
            ];
        }

        if (empty($result)) {
            $result = [];
            $result['all'] = $this->prepareFirstResults($itemRateResult, $method);
            $result['grouped'] = $this->prepareFirstResults($itemRateResult, $method, true);
        } elseif (!empty($itemRateResult)) {
            $result['all'] = $this->prepareAndMergeResults($result['all'], $itemRateResult, $method);
            $result['grouped'] = $this->prepareAndMergeResults($result['grouped'], $itemRateResult, $method, true);
        }

        return $result;
    }

    /**
     * @param $itemRateResult
     * @param $method
     * @param boolean $useGroupPriority
     * @return array
     */
    public function prepareFirstResults($itemRateResult, $method, $useGroupPriority = false)
    {
        $result = [];
        foreach ($itemRateResult as $keyRate => $itemRate) {
            /**
             * Skip items with "*" shipping_group, if "Low Priority for Shipping Group "*" enabled
             */
            if ($useGroupPriority && $itemRate['shipping_group'] == '*') {
                continue;
            }
            if (empty($result)) {
                $result[] = $itemRate;
                continue;
            }
            reset($result);
            $found = false;
            foreach ($result as $keyResult => $itemResult) {
                if ($itemResult['delivery_method'] != $itemRate['delivery_method']) {
                    continue;
                }
                $found = true;
                if ($method == 'sum_first') {
                    //First one rate already got
                    continue;
                } elseif ($method == 'sum_last') {
                    //Get last one rate from result
                    $result[$keyResult] = $itemRate;
                } else {
                    //Rewrite rate to highest one if find
                    if ($itemResult['price'] < $itemRate['price']) {
                        $result[$keyResult] = $itemRate;
                    }
                }
            }
            if (!$found) {
                $result[] = $itemRate;
            }
        }

        return $result;
    }

    /**
     * @param $result
     * @param $itemRateResult
     * @param $method
     * @param bool $useGroupPriority
     * @return mixed
     */
    public function prepareAndMergeResults($result, $itemRateResult, $method, $useGroupPriority = false)
    {
        reset($result);
        foreach ($result as $keyResult => $itemResult) {
            /**
             * Skip items with "*" shipping_group, if "Low Priority for Shipping Group "*" enabled
             */
            if ($useGroupPriority && $itemResult['shipping_group'] == '*') {
                continue;
            }
            $price = 0;
            $cost = 0;
            $found = false;
            $firstFound = false;
            reset($itemRateResult);
            foreach ($itemRateResult as $keyRate => $itemRate) {
                if ($itemResult['delivery_method'] != $itemRate['delivery_method']) {
                    continue;
                }
                if ($method == 'sum_first') {
                    //Get first one rate from result
                    if (!$firstFound) {
                        $price = $itemRate['price'];
                        $cost = $itemRate['cost'];
                        $firstFound = true;
                    }
                } elseif ($method == 'sum_last') {
                    //Get last one rate from result
                    $price = $itemRate['price'];
                    $cost = $itemRate['cost'];
                } else {
                    //Rewrite rate to highest one if find
                    if ($itemResult['price'] < $itemRate['price']) {
                        $itemResult = $itemRate;
                        $result[$keyResult] = $itemRate;
                    }
                }
                $found = true;
            }
            if ($found) {
                if ($method == 'sum_last' || $method == 'sum_first') {
                    $result[$keyResult]['price'] = $itemResult['price'] + $price;
                    $result[$keyResult]['cost'] = $itemResult['cost'] + $cost;
                }
                $result[$keyResult]['found'] = true;
            } else {
                $result[$keyResult]['found'] = false;
            }
        }

        $result = $this->cleanResult($result);

        return $result;
    }

    /**
     * @param $result
     * @return mixed
     */
    public function cleanResult($result)
    {
        //Deleting records that were not found in the processing
        reset($result);
        foreach ($result as $key => $item) {
            if (!isset($result[$key]['found'])) {
                unset($result[$key]);
            } elseif (isset($result[$key]['found']) && !$result[$key]['found']) {
                unset($result[$key]);
            }
        }

        return $result;
    }

    /**
     * Multiplication result on quantity
     *
     * @param $itemRateResult
     * @param $qty
     * @return array
     */
    public function _multipleResult($itemRateResult, $qty)
    {
        $result = [];
        foreach ($itemRateResult as $key => $item) {
            $result[] = $item;
            $result[$key]['price'] = $item['price'] * $qty;
            $result[$key]['cost'] = $item['cost'] * $qty;
        }

        return $result;
    }
}
