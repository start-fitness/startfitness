<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\ShippingMatrixRates\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;

class MethodsPriorityList extends AbstractFieldArray
{
    /**
     * Prepare to render
     *
     * @return void
     */
    public function _prepareToRender()
    {
        $this->addColumn('method', ['label' => __('Method'), 'class' => 'required']);
        $this->addColumn('priority', ['label' => __('Priority'), 'class' => 'required']);
        $this->_addAfter = false;
    }
}
