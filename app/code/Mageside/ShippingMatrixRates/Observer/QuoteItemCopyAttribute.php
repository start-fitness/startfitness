<?php
/**
 * Copyright © Mageside. All rights reserved.
 * See MS-LICENSE.txt for license details.
 */
namespace Mageside\ShippingMatrixRates\Observer;

use Magento\Framework\Event\ObserverInterface;

class QuoteItemCopyAttribute implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Catalog\Model\Product $product */
        $product = $observer->getProduct();
        if ($shippingGroup = $product->getShippingGroup()) {
            $observer->getQuoteItem()->setShippingGroup($shippingGroup);
        } elseif ($product->getTypeId() == 'configurable') {
            /** Fix for configurable products */
            if ($simpleProduct = $product->getCustomOption('simple_product')) {
                if ($shippingGroup = $simpleProduct->getProduct()->getShippingGroup()) {
                    $observer->getQuoteItem()->setShippingGroup($shippingGroup);
                }
            }
        }

        return $this;
    }
}
