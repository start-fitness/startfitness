/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
//alert('here2');
define([
    'jquery',
    'Magento_Ui/js/grid/listing'
], function ($, Component) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Cminds_Orderhighlight/grid/listing'
        },

        getBackgroundColor: function (row) {
            return row.back_color;
        },

        getTextColor: function (row) {
            return row.text_color;
        },

        triggerMouseOver: function (row, data, event) {
            row.originalColor = row.back_color;

            var tdElements = $(".data-grid tr:hover td");
            row.columns = tdElements;

            tdElements.css('background-color', row.back_color);
        },

        triggerMouseOut: function (row, data, event) {
            row.columns.css('background-color', row.originalColor);
        }
    });


});
