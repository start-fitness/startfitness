<?php

namespace Cminds\Orderhighlight\Ui\Component\Listing\Columns;

use Cminds\Orderhighlight\Helper\ConfigHelper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Sales\Model\OrderRepository;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class StatusCode extends Column
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * @var ConfigHelper
     */
    private $configHelper;

    /**
     * StatusCode constructor.
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param ConfigHelper $configHelper
     * @param OrderRepository $orderRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        ConfigHelper $configHelper,
        OrderRepository $orderRepository,
        array $components = [],
        array $data = []
    ) {
        $this->configHelper = $configHelper;
        $this->orderRepository = $orderRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if ($this->configHelper->isModuleEnabled() && $this->configHelper->getHighlightTemplate()) {
            $colors = $this->configHelper->getColorsArray() ?: [];
            if (isset($dataSource['data']['items'])) {
                foreach ($dataSource['data']['items'] as &$item) {
                    try {
                        $order = $this->orderRepository->get($item['entity_id']);
                        $shippingMethod = $order->getShippingMethod();
                        //print_r($shippingMethod);
                        $paymentMethod = $item['payment_method'];
                        $status = $item['status'];

                        if (isset($colors[$status])) {
                            $item += $colors[$status];
                        } elseif (isset($colors[$paymentMethod])) {
                            $item += $colors[$paymentMethod];
                        }  elseif (isset($colors[$shippingMethod])) {
                            $item += $colors[$shippingMethod];
                        }

                    } catch (\Exception $e) {
                        //no such order, skip
                    }
                }
            }
        }

        return $dataSource;
    }
}
