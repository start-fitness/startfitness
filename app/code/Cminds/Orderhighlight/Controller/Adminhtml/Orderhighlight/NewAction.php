<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Orderhighlight;

use Magento\Backend\App\Action;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class NewAction extends Action
{
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $this->_forward('edit');
    }
}