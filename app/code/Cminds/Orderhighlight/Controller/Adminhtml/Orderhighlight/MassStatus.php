<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Orderhighlight;

use Cminds\Orderhighlight\Model\OrderhighlightFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class MassStatus extends Action
{
    /**
     * @var Session
     */
    protected $sessionManager;

    /**
     * @var OrderHightlightFactory
     */
    protected $orderhighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @param Context $context
     * @param OrderhighlightFactory $orderhighlightFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Context $context,
        OrderhighlightFactory $orderhighlightFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->orderhighlightFactory = $orderhighlightFactory;
        $this->sessionManager = $sessionManager;
        parent::__construct($context);
    }

    /**
     * @var PageFactory
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('id');
        $status = $this->getRequest()->getParam('status');
        if (!is_array($ids) || empty($ids)) {
            $this->messageManager->addError(__('Please select product(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $orderColor = $this->orderhighlightFactory->create();
                    $orderColor->load($id);
                    $orderColor->setData('status', $status)->save();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($ids))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }
        $this->_redirect('*/*/');
    }
}