<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Orderhighlight;

use Cminds\Orderhighlight\Model\OrderhighlightFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Edit extends Action
{
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @var OrderHightlightFactory
     */
    protected $orderhighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param OrderhighlightFactory $orderhighlightFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Context $context,
        Registry $registry,
        OrderhighlightFactory $orderhighlightFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->orderhighlightFactory = $orderhighlightFactory;
        $this->sessionManager = $sessionManager;
        $this->registry = $registry;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        $orderColor = $this->orderhighlightFactory->create();

        if ($id) {
            $orderColor->load($id);
            if (!$orderColor->getId()) {
                $this->messageManager->addError(__('This row no longer exists.'));
                $this->_redirect('*/*/');

                return;
            }
        }

        $data = $this->sessionManager->getFormData(true);
        if (!empty($data)) {
            $orderColor->setData($data);
        }
        $this->registry->register('cminds_orderhighlight', $orderColor);
        $this->_view->loadLayout();
        if ($id) {
            $title = 'Edit Order Highlight';
        } else {
            $title = 'Add Order Highlight';
        }
        $this->_view->getPage()->getConfig()->getTitle()->set(__($title));
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}