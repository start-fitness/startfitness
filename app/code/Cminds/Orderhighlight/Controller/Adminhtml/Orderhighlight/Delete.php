<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Orderhighlight;

use Cminds\Orderhighlight\Model\OrderhighlightFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Delete extends Action
{
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @var OrderHightlightFactory
     */
    protected $orderhighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @param Context $context
     * @param OrderhighlightFactory $orderhighlightFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Context $context,
        OrderhighlightFactory $orderhighlightFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->orderhighlightFactory = $orderhighlightFactory;
        $this->sessionManager = $sessionManager;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $orderColor = $this->orderhighlightFactory->create();
            $orderColor->load($id);
            $orderColor->delete();
            $this->messageManager->addSuccess(
                __('Delete successfully !')
            );
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
        }

        $this->_redirect('*/*/');
    }
}