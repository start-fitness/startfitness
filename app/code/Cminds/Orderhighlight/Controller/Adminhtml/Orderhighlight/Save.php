<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Orderhighlight;

use Cminds\Orderhighlight\Model\OrderhighlightFactory;
use Magento\Backend\App\Action\Context;

class Save extends \Magento\Backend\App\Action
{
    /**
     * @var array
     */
    protected $multiselectParams = [
        'payment_methods',
        'shipping_methods',
        'order_status'
    ];

    /**
     * @var OrderHightlightFactory
     */
    protected $orderhighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @param Context $context
     * @param OrderhighlightFactory $orderhighlightFactory
     */
    public function __construct(
        Context $context,
        OrderhighlightFactory $orderhighlightFactory
    ) {
        $this->orderhighlightFactory = $orderhighlightFactory;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();

        if ($data) {
            foreach ($data as $key => &$value) {
                if (in_array($key, $this->multiselectParams)) {
                    $value = implode(',', $value);
                }
            }
            $model = $this->orderhighlightFactory->create();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            if (isset($data['select_field'])) {
                switch (intval($data['select_field'])) {
                    case 0:
                        unset($data['select_field']);
                        unset($data["order_status"]);
                        unset($data["payment_methods"]);
                        unset($data["shipping_methods"]);
                        break;
                    case 1:
                        unset($data['select_field']);
                        $data["payment_methods"] = '';
                        $data["shipping_methods"] = '';
                        break;
                    case 2:
                        unset($data['select_field']);
                        $data["order_status"] = '';
                        $data["shipping_methods"] = '';
                        break;
                    case 3:
                        unset($data['select_field']);
                        $data["order_status"] = '';
                        $data["payment_methods"] = '';
                        break;
                }
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('Order color saved successfully.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $this->_redirect('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }

                return $this->_redirect('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e,
                    __('Something went wrong while saving the order color.'));
            }
            $this->_getSession()->setFormData($data);

            return $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $this->_redirect('*/*/');
    }
}