<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Templatehighlight;

use Cminds\Orderhighlight\Model\TemplatehighlightFactory;
use Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight\Collection;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class MassDelete extends Action
{
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @var TemplateHightlightFactory
     */
    protected $templatehighlightFactory;

    /**
     * @var Collection
     */
    protected $orderhighlightFactory;


    /**
     * @param Context $context
     */
    protected $context;

    /**
     * MassDelete constructor.
     *
     * @param Context $context
     * @param TemplatehighlightFactory $templatehighlightFactory
     * @param Collection $orderhighlightFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Context $context,
        TemplatehighlightFactory $templatehighlightFactory,
        Collection $orderhighlightFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->templatehighlightFactory = $templatehighlightFactory;
        $this->orderhighlightFactory = $orderhighlightFactory;
        $this->sessionManager = $sessionManager;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $ids = $this->getRequest()->getParam('id');
        if (!is_array($ids) || empty($ids)) {
            $this->messageManager->addError(__('Please select template(s).'));
        } else {
            try {
                foreach ($ids as $id) {
                    $template = $this->templatehighlightFactory->create();
                    $template->load($id);
                    $colors = $this->orderhighlightFactory
                        ->addFieldToFilter('template_id', $id)
                        ->load();
                    foreach ($colors as $color){
                        $color->delete();
                    }
                    $template->delete();
                }
                $this->messageManager->addSuccess(
                    __('A total of %1 record(s) have been deleted.', count($ids))
                );
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        $this->_redirect('*/*/');
    }
}