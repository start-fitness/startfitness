<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Templatehighlight;

use Cminds\Orderhighlight\Model\TemplatehighlightFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Backend\App\Action;

class Save extends Action
{
    /**
     * @var TemplatehighlightFactory
     */
    protected $templatehighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @param Context $context
     * @param TemplatehighlightFactory $templatehighlightFactory
     */
    public function __construct(
        Context $context,
        TemplatehighlightFactory $templatehighlightFactory
    ) {
        $this->templatehighlightFactory = $templatehighlightFactory;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getParams();

        if ($data) {
            $model = $this->templatehighlightFactory->create();
            $id = $this->getRequest()->getParam('id');
            if ($id) {
                $model->load($id);
            }

            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccess(__('Template saved successfully.'));
                $this->_getSession()->setFormData(false);
                if ($this->getRequest()->getParam('back')) {
                    return $this->_redirect('*/*/edit', ['id' => $model->getId(), '_current' => true]);
                }

                return $this->_redirect('*/*/');
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e,
                    __('Something went wrong while saving the template.'));
            }
            $this->_getSession()->setFormData($data);

            return $this->_redirect('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }

        return $this->_redirect('*/*/');
    }
}