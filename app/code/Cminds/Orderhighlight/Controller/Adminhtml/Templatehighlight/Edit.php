<?php

namespace Cminds\Orderhighlight\Controller\Adminhtml\Templatehighlight;

use Cminds\Orderhighlight\Model\TemplatehighlightFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\ResultInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Edit extends Action
{
    /**
     * @var SessionManagerInterface
     */
    protected $sessionManager;

    /**
     * @var TemplatehighlightFactory
     */
    protected $templatehighlightFactory;

    /**
     * @param Context $context
     */
    protected $context;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * Edit constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param TemplatehighlightFactory $templatehighlightFactory
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Context $context,
        Registry $registry,
        TemplatehighlightFactory $templatehighlightFactory,
        SessionManagerInterface $sessionManager
    ) {
        $this->templatehighlightFactory = $templatehighlightFactory;
        $this->sessionManager = $sessionManager;
        $this->registry = $registry;

        parent::__construct($context);
    }

    /**
     * @return ResponseInterface|ResultInterface|void
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('id');

        $template = $this->templatehighlightFactory->create();

        if ($id) {
            $template->load($id);
            if (!$template->getId()) {
                $this->messageManager->addError(__('This row no longer exists.'));
                $this->_redirect('*/*/');

                return;
            }
        }

        $data = $this->sessionManager->getFormData(true);
        if (!empty($data)) {
            $template->setData($data);
        }

        $this->registry->register('cminds_templatehighlight', $template);
        $this->_view->loadLayout();
        $this->_view->getPage()->getConfig()->getTitle()->set(__('Edit Template Highlight'));
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}