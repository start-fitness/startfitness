<?php

namespace Cminds\Orderhighlight\Model;

use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\Exception\OrderhighlightException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Orderhighlight extends AbstractModel
{
    const ORDER_STATUS_TYPE = 1;
    const PAYMENT_METHOD_TYPE = 2;
    const SHIPPING_METHOD_TYPE = 3;

    /**
     * Orderhighlight constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param AbstractResource|null $resource
     * @param AbstractDb|null $resourceCollection
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        AbstractResource $resource = null,
        AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    public function _construct()
    {
        $this->_init(\Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight::class);
    }
}