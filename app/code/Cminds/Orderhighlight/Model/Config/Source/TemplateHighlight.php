<?php

namespace Cminds\Orderhighlight\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Cminds\Orderhighlight\Model\ResourceModel\Templatehighlight\CollectionFactory;

class TemplateHighlight implements OptionSourceInterface{
    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var array|null
     */
    protected $options;

    /**
     * TemplateHighlight constructor.
     *
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactory $collectionFactory
    ) {
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray() {
        if ($this->options === null) {
            $collection = $this->collectionFactory->create();
            $collection->addFieldToFilter('status', 1)->load();

            $this->options = [['label' => '', 'value' => '']];

            foreach ($collection as $template) {
                $this->options[] = [
                    'label' => __('%1 Template Highlight', $template->getName()),
                    'value' => $template->getId()
                ];
            }
        }

        return $this->options;
    }
}