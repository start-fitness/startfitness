<?php

namespace Cminds\Orderhighlight\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Templatehighlight extends AbstractDb
{
    /**
     * Initialize resource.
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init('cminds_templatehighlight', 'id');
    }
}