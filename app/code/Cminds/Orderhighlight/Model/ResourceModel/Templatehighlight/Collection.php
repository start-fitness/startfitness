<?php

namespace Cminds\Orderhighlight\Model\ResourceModel\Templatehighlight;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Collection extends AbstractCollection
{
    /**
     * Initialize resource collection.
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            'Cminds\Orderhighlight\Model\Templatehighlight',
            'Cminds\Orderhighlight\Model\ResourceModel\Templatehighlight'
        );
    }
}