<?php

namespace Cminds\Orderhighlight\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(
        SchemaSetupInterface $setup,
        ModuleContextInterface $context
    )
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Upgrade table 'cminds_orderhighlight'.
         */

        if (version_compare($context->getVersion(),'1.1.0','<')){
            $installer->getConnection()->addColumn(
                $installer->getTable('cminds_orderhighlight'),
                'template_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => false,
                    'size' => null,
                    'after' => 'title',
                    'comment' => 'Template Id'
                ]
            );

            /**
             * Create table cminds_templatehighlight
             */
            $tableTemplate = $installer->getConnection()->newTable(
                $installer->getTable('cminds_templatehighlight')
            )->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'cminds_templatehighlight'
            )->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '64k',
                [],
                'name'
            )->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['nullable' => false],
                'status'
            );
            $installer->getConnection()->createTable($tableTemplate);

        }
        $installer->endSetup();

    }

}