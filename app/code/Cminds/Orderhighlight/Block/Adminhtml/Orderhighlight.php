<?php

namespace Cminds\Orderhighlight\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Orderhighlight extends Container
{
    /**
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_orderhighlight';
        $this->_blockGroup = 'Cminds_Orderhighlight';
        $this->_headerText = __('Orderhighlight');
        $this->_addButtonLabel = __('Add New');
        parent::_construct();
    }
}