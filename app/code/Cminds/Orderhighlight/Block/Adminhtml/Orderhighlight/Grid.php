<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Orderhighlight;

use Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight\Collection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Grid extends Extended
{
    /**
     * @var Collection
     */
    protected $collectionFactory;

    /**
     * Grid constructor.
     *
     * @param Collection $collectionFactory
     * @param Context $context
     * @param Data $backendHelper
     * @param array $data
     */
    public function __construct(
        Collection $collectionFactory,
        Context $context,
        Data $backendHelper,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('orderhighlight/*/index', ['_current' => true]);
    }

    /**
     * @param Object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'orderhighlight/*/edit',
            [
                'store' => $this->getRequest()->getParam('store'),
                'id' => $row->getId()
            ]
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('productGrid')
            ->setDefaultSort('id')
            ->setDefaultDir('DESC')
            ->setSaveParametersInSession(true)
            ->setUseAjax(false);
    }

    /**
     * @return Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->load();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     *
     * @return Extended
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField(
                    'websites',
                    'catalog_product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left'
                );
            }
        }

        return parent::_addColumnFilterToCollection($column);
    }

    /**
     * @return Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('Id'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'template_id',
            [
                'header' => __('Template Highlight'),
                'type' => 'text',
                'index' => 'template_id',
                'class' => 'template_id',
                'renderer' => 'Cminds\Orderhighlight\Block\Adminhtml\Orderhighlight\Renderer\TemplateName',
                'filter_condition_callback' => array($this, '_templateFilter'),
            ]
        );

        $this->addColumn(
            'title',
            [
                'header' => __('Title'),
                'index' => 'title',
                'class' => 'title'
            ]
        );

        $this->addColumn(
            'multiple_order',
            [
                'header' => __('Multiple Order'),
                'index' => 'multiple_order',
                'class' => 'multiple_order',
                'type' => 'options',
                'options' => [1 => 'Enable', 0 => 'Disable']
            ]
        );

        $this->addColumn(
            'background',
            [
                'header' => __('Background Color'),
                'index' => 'background',
                'class' => 'background'
            ]
        );

        $this->addColumn(
            'foreground',
            [
                'header' => __('Foreground Color'),
                'index' => 'foreground',
                'class' => 'foreground'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'class' => 'status',
                'type' => 'options',
                'options' => [1 => 'Enable', 0 => 'Disable']
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('orderhighlight/*/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        return $this;
    }

    /**
     * @param $collection
     * @param $column
     *
     * @return $this
     */
    protected function _templateFilter($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return $this;
        }

        $this->getCollection()->getSelect()->joinLeft(
            array('ct' => 'cminds_templatehighlight'),
            'main_table.template_id = ct.id',
            array('name')
        );

        $this->getCollection()->getSelect()->where(
            "name like ?" , "%$value%"
        );

        return $this;
    }
}