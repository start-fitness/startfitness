<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Orderhighlight\Renderer;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Cminds\Orderhighlight\Model\TemplatehighlightFactory;
use Magento\Framework\DataObject;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class TemplateName extends AbstractRenderer
{
    protected $templatehighlightFactory;

    /**
     * TemplateName constructor.
     *
     * @param TemplatehighlightFactory $templatehighlightFactory
     */
    public function __construct(
        TemplatehighlightFactory $templatehighlightFactory
    ) {
        $this->templatehighlightFactory = $templatehighlightFactory;
    }

    /**
     * @param DataObject $row
     *
     * @return string
     */
    public function render(DataObject $row)
    {
        $templateId = $row->getTemplateId();
        $templateName = $this->templatehighlightFactory->create()
            ->load($templateId);
        return $templateName->getName();
    }
}