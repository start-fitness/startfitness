<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Orderhighlight\Edit;

use Magento\Backend\Block\Widget\Tabs as Tablist;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Tabs extends Tablist
{
    protected function _construct()
    {
        parent::_construct();
        $this->setId('checkmodule_orderhighlight_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Cmind Orderhighlight'));
    }
}