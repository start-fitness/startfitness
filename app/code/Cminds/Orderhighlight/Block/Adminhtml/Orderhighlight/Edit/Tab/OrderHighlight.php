<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Orderhighlight\Edit\Tab;

use Cminds\Orderhighlight\Model\Config\Source\PaymentMethod;
use Cminds\Orderhighlight\Model\Config\Source\ShippingMethod;
use Cminds\Orderhighlight\Model\Config\Source\TemplateHighlight;
use Cminds\Orderhighlight\Model\Orderhighlight as OrderhighlightModel;
use Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight\Collection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\Data\FormFactory;
use Magento\Framework\Registry;
use Magento\Sales\Model\ResourceModel\Order\Status\CollectionFactory;
use Magento\Store\Model\System\Store;

class OrderHighlight extends Generic implements TabInterface
{
    /**
     * @var Store
     */
    protected $systemStore;

    /**
     * @var CollectionFactory
     */
    protected $statusCollectionFactory;

    /**
     * @var Collection
     */
    protected $collectionFactory;

    /**
     * @var ShippingMethod
     */
    protected $shippingMethodList;

    /**
     * @var PaymentMethod
     */
    protected $paymentMethodList;

    /**
     * @var TemplateHighlight
     */
    protected $templateList;

    /**
     * OrderHighlight constructor.
     *
     * @param Context $context
     * @param Registry $registry
     * @param FormFactory $formFactory
     * @param CollectionFactory $statusCollectionFactory
     * @param Collection $collectionFactory
     * @param PaymentMethod $paymentMethodList
     * @param ShippingMethod $shippingMethodList
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        FormFactory $formFactory,
        CollectionFactory $statusCollectionFactory,
        Collection $collectionFactory,
        PaymentMethod $paymentMethodList,
        ShippingMethod $shippingMethodList,
        TemplateHighlight $templateList,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->statusCollectionFactory = $statusCollectionFactory;
        $this->shippingMethodList = $shippingMethodList;
        $this->paymentMethodList = $paymentMethodList;
        $this->templateList = $templateList;

        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabLabel()
    {
        return __('Order Highlight');
    }

    /**
     * @return \Magento\Framework\Phrase|string
     */
    public function getTabTitle()
    {
        return __('Order Highlight');
    }

    /**
     * @return bool
     */
    public function canShowTab()
    {
        return true;
    }

    /**
     * @return bool
     */
    public function isHidden()
    {
        return false;
    }

    /**
     * @return Generic
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('cminds_orderhighlight');
        $isElementDisabled = false;

        /** @var \Magento\Framework\Data\Form $form */

        $form = $this->_formFactory->create();

        $form->setHtmlIdPrefix('page_');

        $fieldset = $form->addFieldset('base_fieldset', ['legend' => __('Order Highlight')]);

        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }

        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Title'),
                'title' => __('title'),
                'required' => true,
                'style' => "width:50%",
            ]
        );

        $fieldset->addField(
            'template_id',
            'select',
            [
                'name' => 'template_id',
                'label' => __('Template Highlight'),
                'title' => __('Template Highlight'),
                'required' => true,
                'values' => $this->templateList->toOptionArray()
            ]
        );

        $selectField =$fieldset->addField(
            'select_field',
            'select',
            [
                'name' => 'select_field',
                'label' => __('Select Option'),
                'required' => true,
                'options' => [
                    '0' => __('--Please Select--'),
                    '1' => __('Order Status'),
                    '2' => __('Payment Methods'),
                    '3' => __('Shipping Method')],
                'note' => __("Please choose option otherwise your color won't be applied"),
            ]
        );

        $hideOrderStatus = $fieldset->addField(
            'order_status',
            'multiselect',
            [
                'name' => 'order_status',
                'label' => __('Order Status'),
                'title' => __('order status'),
                'required' => true,
                'values' => $this->statusCollectionFactory->create()->toOptionArray(),
                'note' => __('Set flag if order changes to one of selected statuses'),
            ]
        );

        $hidePaymentMethods = $fieldset->addField(
            'payment_methods',
            'multiselect',
            [
                'name' => 'payment_methods',
                'label' => __('Payment Methods'),
                'title' => __('Payment Methods'),
                'required' => true,
                'values' => $this->paymentMethodList->toOptionArray()
            ]
        );

        $hideShippingMethods = $fieldset->addField(
            'shipping_methods',
            'multiselect',
            [
                'name' => 'shipping_methods',
                'label' => __('Shipping Method'),
                'title' => __('Shipping Method'),
                'required' => true,
                'values' => $this->shippingMethodList->toOptionArray()
            ]
        );

        $dependence = $this->getLayout()->createBlock(
            'Magento\Backend\Block\Widget\Form\Element\Dependence')
            ->addFieldMap($selectField->getHtmlId(), $selectField->getName())
            ->addFieldMap($hideOrderStatus->getHtmlId(), $hideOrderStatus->getName())
            ->addFieldMap($hidePaymentMethods->getHtmlId(), $hidePaymentMethods->getName())
            ->addFieldMap($hideShippingMethods->getHtmlId(), $hideShippingMethods->getName())
            ->addFieldDependence(
                $hideOrderStatus->getName(),
                $selectField->getName(),
                '1'
            )
            ->addFieldDependence(
                $hidePaymentMethods->getName(),
                $selectField->getName(),
                '2'
            )
            ->addFieldDependence(
                $hideShippingMethods->getName(),
                $selectField->getName(),
                '3'
            );

        $this->setChild('form_after', $dependence);

        $fieldset->addField(
            'multiple_order',
            'select',
            [
                'name' => 'multiple_order',
                'label' => __('Multiple Order'),
                'title' => __('multiple_order'),
                'class' => 'input-select',
                'options' => ['1' => __('Enable'), '0' => __('Disable')],
                'style' => "width:50%",
                'note' => __('If enabled, it will highlight multiple orders'),
            ]
        );

        $fieldset->addField(
            'background',
            'text',
            [
                'name' => 'background',
                'label' => __('Background Color'),
                'class' => 'jscolor {hash:true,refine:false}',
                'required' => false
            ]
        );

        $fieldset->addField(
            'foreground',
            'text',
            [
                'name' => 'foreground',
                'label' => __('Foreground Color'),
                'class' => 'jscolor {hash:true,refine:false}',
                'required' => false,
                'note' => __("Complementary color to background"),

            ]
        );

        $fieldset->addField(
            'status',
            'select',
            [
                'name' => 'status',
                'label' => __('Status'),
                'title' => __('status'),
                'class' => 'input-select',
                'options' => ['1' => __('Enable'), '0' => __('Disable')]
            ]
        );

        if (!$model->getId()) {
            $model->setData('status', $isElementDisabled ? '2' : '1');
        }

        if ($model->getData('order_status')) {
            $model->setData('select_field', OrderhighlightModel::ORDER_STATUS_TYPE);
        } elseif ($model->getData('payment_methods')) {
            $model->setData('select_field', OrderhighlightModel::PAYMENT_METHOD_TYPE);
        } elseif ($model->getData('shipping_methods')) {
            $model->setData('select_field', OrderhighlightModel::SHIPPING_METHOD_TYPE);
        }

        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

    /**
     * Check permission for passed action.
     *
     * @param string $resourceId
     *
     * @return bool
     */
    protected function _isAllowedAction($resourceId)
    {
        return $this->_authorization->isAllowed($resourceId);
    }
}