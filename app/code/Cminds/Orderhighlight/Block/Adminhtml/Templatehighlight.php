<?php
namespace Cminds\Orderhighlight\Block\Adminhtml;

use Magento\Backend\Block\Widget\Grid\Container;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Templatehighlight extends Container
{
    /**
     * Construct
     * return void
     */
    protected function _construct()
    {
        $this->_controller = 'adminhtml_templatehighlight';
        $this->_blockGroup = 'Cminds_Orderhighlight';
        $this->_headerText = __('Templatehighlight');
        $this->_addButtonLabel = __('Create New Template');

        parent::_construct();
    }
}