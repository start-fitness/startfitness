<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Templatehighlight;

use Cminds\Orderhighlight\Model\ResourceModel\Templatehighlight\Collection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Backend\Block\Widget\Grid\Column;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Grid extends Extended
{
    /**
     * @var Collection
     */
    protected $collectionFactory;

    /**
     * Grid constructor.
     *
     * @param Collection $collectionFactory
     * @param Context $context
     * @param Data $backendHelper
     * @param array $data
     */
    public function __construct(
        Collection $collectionFactory,
        Context $context,
        Data $backendHelper,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;

        parent::__construct(
            $context,
            $backendHelper,
            $data
        );
    }

    /**
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('orderhighlight/templatehighlight/index', ['_current' => true]);
    }

    /**
     * @param Object $row
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl(
            'orderhighlight/templatehighlight/edit',
            [
                'store' => $this->getRequest()->getParam('store'),
                'id' => $row->getId()
            ]
        );
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('productGrid')
            ->setDefaultSort('id')
            ->setDefaultDir('DESC')
            ->setSaveParametersInSession(true)
            ->setUseAjax(false);
    }

    /**
     * @return Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory->load();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @param Column $column
     *
     * @return Extended
     */
    protected function _addColumnFilterToCollection($column)
    {
        if ($this->getCollection()) {
            if ($column->getId() == 'websites') {
                $this->getCollection()->joinField(
                    'websites',
                    'catalog_product_website',
                    'website_id',
                    'product_id=entity_id',
                    null,
                    'left'
                );
            }
        }

        return parent::_addColumnFilterToCollection($column);
    }

    /**
     * @return Extended
     *
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'id',
            [
                'header' => __('Id'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'name',
            [
                'header' => __('Name'),
                'index' => 'name',
                'class' => 'name'
            ]
        );

        $this->addColumn(
            'status',
            [
                'header' => __('Status'),
                'type' => 'options',
                'index' => 'status',
                'class' => 'status',
                'options' => [1 => 'Enable', 0 => 'Disable']
            ]
        );

        $block = $this->getLayout()->getBlock('grid.bottom.links');
        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');

        $this->getMassactionBlock()->addItem(
            'delete',
            [
                'label' => __('Delete'),
                'url' => $this->getUrl('orderhighlight/templatehighlight/massDelete'),
                'confirm' => __('Are you sure?')
            ]
        );

        return $this;
    }
}