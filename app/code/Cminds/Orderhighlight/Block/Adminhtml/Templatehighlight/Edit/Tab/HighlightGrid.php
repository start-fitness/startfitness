<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Templatehighlight\Edit\Tab;

use Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight\Collection;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Block\Widget\Grid\Extended;
use Magento\Backend\Helper\Data;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Framework\App\Request\Http;

class HighlightGrid extends Extended implements TabInterface
{
    /**
     * @var Collection
     */
    protected $collectionFactory;


    /**
     * @var Http
     */
    protected $request;

    /**
     * Grid constructor.
     *
     * @param Collection $collectionFactory
     * @param Context $context
     * @param Data $backendHelper
     * @param array $data
     */
    public function __construct(
        Collection $collectionFactory,
        Context $context,
        Data $backendHelper,
        Http $request,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->request = $request;

        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();

        $this->setId('productGrid')
            ->setDefaultSort('id')
            ->setDefaultDir('DESC')
            ->setSaveParametersInSession(true)
            ->setUseAjax(false);
    }

    /**
     * @return Extended
     */
    protected function _prepareCollection()
    {
        $collection = $this->collectionFactory
            ->addFieldToFilter('template_id',$this->request->getParam('id'))
            ->load();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    /**
     * @return Extended
     * @throws \Exception
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'col_id',
            [
                'header' => __('Id'),
                'type' => 'number',
                'index' => 'id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id'
            ]
        );

        $this->addColumn(
            'col_title',
            [
                'header' => __('Title'),
                'index' => 'title',
                'class' => 'title'
            ]
        );

        $this->addColumn(
            'col_multiple_order',
            [
                'header' => __('Multiple Order'),
                'index' => 'multiple_order',
                'class' => 'multiple_order',
                'type' => 'options',
                'options' => [1 => 'Enable', 0 => 'Disable']
            ]
        );

        $this->addColumn(
            'col_background',
            [
                'header' => __('Background Color'),
                'index' => 'background',
                'class' => 'background'
            ]
        );

        $this->addColumn(
            'col_foreground',
            [
                'header' => __('Foreground Color'),
                'index' => 'foreground',
                'class' => 'foreground'
            ]
        );

        $this->addColumn(
            'col_status',
            [
                'header' => __('Status'),
                'index' => 'status',
                'class' => 'status',
                'type' => 'options',
                'options' => [1 => 'Enable', 0 => 'Disable']
            ]
        );

        return parent::_prepareColumns();
    }

    public function getTabLabel()
    {
        return __('Hightlight Order Color');
    }

    public function getTabTitle()
    {
        return __('Hightlight Order Color');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}