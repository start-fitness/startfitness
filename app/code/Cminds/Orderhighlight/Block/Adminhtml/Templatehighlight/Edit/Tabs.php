<?php

namespace Cminds\Orderhighlight\Block\Adminhtml\Templatehighlight\Edit;

use Magento\Backend\Block\Widget\Tabs as Tablist;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Tabs extends Tablist
{
    protected function _construct()
    {
        parent::_construct();

        $this->setId('checkmodule_templatehighlight_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(__('Cmind Templatehighlight'));
    }

}