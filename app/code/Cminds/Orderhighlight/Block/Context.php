<?php

namespace Cminds\Orderhighlight\Block;

use Cminds\Orderhighlight\Helper\Data;
use Cminds\Orderhighlight\Helper\ConfigHelper;
use Magento\Framework\App\Cache\StateInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\State;
use Magento\Framework\Escaper;
use Magento\Framework\Event\ManagerInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Filter\FilterManager;
use Magento\Framework\ObjectManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Session\SidResolverInterface;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Framework\Translate\Inline\StateInterface as TranslateInlineStateInterface;
use Magento\Framework\UrlFactory;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\ConfigInterface;
use Magento\Framework\View\DesignInterface;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Framework\View\Element\Template\File\Resolver;
use Magento\Framework\View\Element\Template\File\Validator;
use Magento\Framework\View\FileSystem as ViewFilesystem;
use Magento\Framework\View\LayoutInterface;
use Magento\Framework\View\Page\Config as PageConfig;
use Magento\Framework\View\TemplateEnginePool;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class Context extends TemplateContext
{
    /**
     * @var devToolHelper
     */
    protected $devToolHelper;

    /**
     * @var Registry
     */
    protected $registry;

    /**
     * @var ConfigHelper
     */
    protected $config;

    /**
     * @var ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var UrlFactory
     */
    protected $urlFactory;

    /**
     * Context constructor.
     *
     * @param Data $devToolHelper
     * @param ConfigHelper $config
     * @param RequestInterface $request
     * @param LayoutInterface $layout
     * @param ManagerInterface $eventManager
     * @param UrlInterface $urlBuilder
     * @param CacheInterface $cache
     * @param DesignInterface $design
     * @param SessionManagerInterface $session
     * @param SidResolverInterface $sidResolver
     * @param ScopeConfigInterface $scopeConfig
     * @param Repository $assetRepo
     * @param ConfigInterface $viewConfig
     * @param StateInterface $cacheState
     * @param LoggerInterface $logger
     * @param Escaper $escaper
     * @param FilterManager $filterManager
     * @param TimezoneInterface $localeDate
     * @param TranslateInlineStateInterface $inlineTranslation
     * @param Filesystem $filesystem
     * @param ViewFilesystem $viewFileSystem
     * @param TemplateEnginePool $enginePool
     * @param State $appState
     * @param StoreManagerInterface $storeManager
     * @param PageConfig $pageConfig
     * @param Resolver $resolver
     * @param Validator $validator
     * @param Registry $registry
     * @param ObjectManagerInterface $objectManager
     * @param UrlFactory $urlFactory
     */
    public function __construct(
        Data $devToolHelper,
        ConfigHelper $config,
        RequestInterface $request,
        LayoutInterface $layout,
        ManagerInterface $eventManager,
        UrlInterface $urlBuilder,
        CacheInterface $cache,
        DesignInterface $design,
        SessionManagerInterface $session,
        SidResolverInterface $sidResolver,
        ScopeConfigInterface $scopeConfig,
        Repository $assetRepo,
        ConfigInterface $viewConfig,
        StateInterface $cacheState,
        LoggerInterface $logger,
        Escaper $escaper,
        FilterManager $filterManager,
        TimezoneInterface $localeDate,
        TranslateInlineStateInterface $inlineTranslation,
        Filesystem $filesystem,
        ViewFilesystem $viewFileSystem,
        TemplateEnginePool $enginePool,
        State $appState,
        StoreManagerInterface $storeManager,
        PageConfig $pageConfig,
        Resolver $resolver,
        Validator $validator,
        Registry $registry,
        ObjectManagerInterface $objectManager,
        UrlFactory $urlFactory
    ) {
        $this->devToolHelper = $devToolHelper;
        $this->registry = $registry;
        $this->config = $config;
        $this->objectManager = $objectManager;
        $this->urlFactory = $urlFactory;
        parent::__construct(
            $request,
            $layout,
            $eventManager,
            $urlBuilder,
            $cache,
            $design,
            $session,
            $sidResolver,
            $scopeConfig,
            $assetRepo,
            $viewConfig,
            $cacheState,
            $logger,
            $escaper,
            $filterManager,
            $localeDate,
            $inlineTranslation,
            $filesystem,
            $viewFileSystem,
            $enginePool,
            $appState,
            $storeManager,
            $pageConfig,
            $resolver,
            $validator
        );
    }

    /**
     * Function for getting developer helper object.
     *
     * @return Data
     */
    public function getOrderhighlightHelper()
    {
        return $this->devToolHelper;
    }

    /**
     * Function for getting registry object.
     *
     * @return Registry
     */
    public function getRegistry()
    {
        return $this->registry;
    }

    /**
     * Function for getting orderhighlight model config object.
     *
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Function for getting object manager object.
     *
     * @return ObjectManagerInterface
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }

    /**
     * Function for getting UrlFactory object.
     *
     * @return UrlFactory
     */
    public function getUrlFactory()
    {
        return $this->urlFactory;
    }
}