<?php

namespace Cminds\Orderhighlight\Helper;

use Cminds\Orderhighlight\Model\ResourceModel\Orderhighlight\CollectionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Collection;

/**
 * Cminds Orderhighlight.
 *
 * @category Cminds
 * @package  Cminds_Orderhighlight
 */
class ConfigHelper
{
    /**
     * Scope Config.
     *
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * Color Collection Factory.
     *
     * @var CollectionFactory
     */
    protected $colorCollection;

    /**
     * ConfigHelper constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        CollectionFactory $collectionFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->colorCollection = $collectionFactory;
    }

    /**
     * Get value from store scope config.
     *
     * @param string $path
     * @param int|null $storeId
     *
     * @return mixed
     */
    protected function getConfigValue($path, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $path, \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId
        );
    }

    /**
     * Get Module status.
     *
     * @param int|null $storeId
     *
     * @return bool
     */
    public function isModuleEnabled($storeId = null)
    {
        return (bool)$this->getConfigValue(
            'orderhighlight/general/enable', $storeId
        );
    }

    /**
     * @param null $storeId
     *
     * @return mixed
     */
    public function getHighlightTemplate($storeId = null)
    {
        return $this->getConfigValue(
            'orderhighlight/general/template', $storeId
        );
    }

    /**
     * Get orders' status colors.
     *
     * @return array
     */
    public function getColorsArray()
    {
        $ordersColors = [];
        $templateId = $this->getHighlightTemplate();
        if ($templateId) {
            $colors = $this->colorCollection->create()
                ->setOrder('id', Collection::SORT_ORDER_ASC)
                ->addFieldToFilter('template_id', $templateId)
                ->load();

            foreach ($colors as $color) {
                if ($color->getStatus()) {
                    $statuses = explode(',', $color->getOrderStatus());
                    $paymentMethods = explode(',', $color->getPaymentMethods());
                    $shippingMethods = explode(',', $color->getShippingMethods());
                    $colors = [
                        'back_color' => $color->getBackground(),
                        'text_color' => $color->getForeground()
                    ];

                    foreach ($statuses as $status) {
                        if ($status != "") {
                            $ordersColors[$status] = $colors;
                        }
                    }

                    foreach ($paymentMethods as $paymentMethod) {
                        if ($paymentMethod != "") {
                            $ordersColors[$paymentMethod] = $colors;
                        }
                    }

                    foreach ($shippingMethods as $shippingMethod) {
                        if ($shippingMethod != "") {
                            $ordersColors[$shippingMethod] = $colors;
                        }
                    }
                }
            }
        }

        return $ordersColors;
    }
}
