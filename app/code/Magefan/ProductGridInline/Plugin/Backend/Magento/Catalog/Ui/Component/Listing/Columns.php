<?php
/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

declare(strict_types=1);

namespace Magefan\ProductGridInline\Plugin\Backend\Magento\Catalog\Ui\Component\Listing;

use Magefan\ProductGridInline\Model\Config;

class Columns
{
    /**
     * @var Config
     */
    private $config;

    /**
     * Columns constructor.
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * @param \Magento\Catalog\Ui\Component\Listing\Columns $subject
     * @param $result
     * @return mixed
     */
    public function afterPrepare(
        \Magento\Catalog\Ui\Component\Listing\Columns $subject,
        $result
    ) {
        if (!$this->config->isEnabled()) {
            return $result;
        }

        foreach ($subject->getChildComponents() as $attrCode => $column) {

            if (in_array($attrCode, ['websites', 'type_id', 'entity_id', 'ids', 'thumbnail', 'salable_quantity' ])) {
                continue;
            }

            $frontendInput = 'text';
            $config = $column->getConfiguration();
           //print_r($column->getConfiguration());
        //  echo $config['label'];

            if($config['label'] == 'Quantity') {
            //  echo 'here';
              $editorType = $frontendInput;
              $config['editor'] = [
                  'editorType' => ''
              ];
            }
          else   if (!(isset($config['editor']) && isset($config['editor']['editorType']))) {
                if (isset($config['editor']) && is_string($config['editor'])) {
                    $editorType = $config['editor'];
                } elseif (isset($config['dataType'])) {
                    $editorType = $config['dataType'];
                } else {
                    $editorType = $frontendInput;
                }

                $config['editor'] = [
                    'editorType' => $editorType
                ];
            }

            $column->setData('config', $config);
        }

        return $result;
    }
}
