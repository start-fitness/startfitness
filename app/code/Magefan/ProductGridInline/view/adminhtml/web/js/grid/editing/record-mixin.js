/**
 * Copyright © Magefan (support@magefan.com). All rights reserved.
 * Please visit Magefan.com for license details (https://magefan.com/end-user-license-agreement).
 */

define(function () {
    'use strict';

    return function (targetModule) {
        return targetModule.extend({
            defaults: {
                templates: {
                    fields: {
                        multiselect: {
                            component: 'Magento_Ui/js/form/element/multiselect',
                            template: 'ui/form/element/multiselect',
                            options: '${ JSON.stringify($.$data.column.options) }'
                        },
                    }
                }
            }
        });
    };
});
