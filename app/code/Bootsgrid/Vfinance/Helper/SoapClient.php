<?php

namespace Bootsgrid\Vfinance\Helper;

use Zend\Soap\Client;

class SoapClient extends Client {

    protected $_wsdl; // WSDL of SOAP web service
    protected $_location; // URI of web service
    protected $_gateway; // Gateway for function calls

    public function __construct() {
        // Pick up WSDL SOAP URL from config.xml
        $this->_wsdl = 'https://apply.v12finance.com/Services/ApplicationGatewayWebService.svc?wsdl';
        $this->_location = 'https://apply.v12finance.com/Services/ApplicationGatewayWebService.svc';
        $this->_gateway = 'http://v12finance.com/Services/ApplicationGateway/1.1/IApplicationGatewayWebService/';
        
        // Define options for SOAP client
        $options = array('soap_version' => SOAP_1_2);
        
        return parent::__construct($this->_wsdl, $options);
    }

    /**
     * @param $function
     * @param $action
     * @param $dataObj
     * @return mixed
     */
    public function soapCall($function, $action, $dataObj) {
        // Set 'To' and 'Action' headers
        $headers = array(
            new \SoapHeader("http://www.w3.org/2005/08/addressing", "Action", $this->_gateway . $function, true),
            new \SoapHeader("http://www.w3.org/2005/08/addressing", "To", $this->_location, true)
        );
        
        //$this->__setSoapHeaders($headers);
        foreach ($headers as $key => $header) {
            $this->addSoapInputHeader($header);
        }

        // Create wrapper object for the request
        $wrapperObj = (object)array();
        $wrapperObj->$action = $dataObj;
        
        return parent::__call($function, array($wrapperObj));
    }
}

?>