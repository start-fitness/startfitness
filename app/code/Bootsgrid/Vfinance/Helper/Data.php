<?php

namespace Bootsgrid\Vfinance\Helper;

use Magento\Checkout\Model\Session;

use Magento\Sales\Model\OrderFactory;

use \Magento\Framework\App\Config\ScopeConfigInterface;

class Data extends \Magento\Framework\App\Helper\AbstractHelper {

    protected $_checkoutSession;

    protected $_order;

    protected $_scopeConfig;

    public function __construct(Session $checkoutSession, OrderFactory $order, ScopeConfigInterface $scopeConfig){

        $this->_checkoutSession = $checkoutSession;

        $this->_order = $order;

        $this->_scopeConfig = $scopeConfig;

    }

    public function getPendingPaymentStatus() {

        return OrderFactory::STATE_PENDING_PAYMENT;

    }

    public function getActive() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/active',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getProductWidget() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/product_widget',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getCategoryWidget() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/category_widget',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getRetailerId()

    {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/retailer_id',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getRetailerguId()

    {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/retailer_guid',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getAuthKey()

    {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/auth_key',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getMinAmount() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/min_finance_amount',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getV12Percent() {

        $percentage = $this->_scopeConfig->getValue('payment/vfinance/deposit_percent', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($percentage != '') {
            $percentage_exp = explode(',',$percentage);
            $percent_array =array();
            foreach ($percentage_exp as $key => $value) {
                $percent_array[trim($value)]= trim($value);
            }
            return $percent_array;
        } else {
            return array();
        }
    }

    public function getV12Pack() {

        $package = $this->_scopeConfig->getValue('payment/vfinance/packages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        if($package != '') {
            $package_exp = explode(',', $package);
            /*$package_array = array();
            foreach ($package_exp as $key => $value){
                $package_value = explode(':', $value);
                if(count( $package_value) >= 2){
                    $package_array[trim($package_value[0])] = trim($package_value[1]);
                }
            }
            return $package_array;*/
            return $package_exp;
        } else {
            return array();
        }
    }

    public function lowMonthlyCost2($productCost) {
        $minAmount = $this->getMinAmount();
        if($productCost > $minAmount) {
            $productPrice = number_format($productCost, 2, '.', '' );
            $v12_dpercent = 50;
            $dDeposit = ($productPrice / 100) * $v12_dpercent;
            $v12_dloan_amt = $productPrice - $dDeposit;
            $v12_dinterest = 0.03459;
            $v12_dmonthly_payment = $v12_dloan_amt * $v12_dinterest;
            $v12_monthly_cost = number_format($v12_dmonthly_payment, 2, '.', '' );
            $lowMonthlyHtml = '<span class="savingslabel">Finance From £'.$v12_monthly_cost.' P/M</span>';
            return $lowMonthlyHtml;
        } else {
            return '';
        }
    }
	
	
	public function lowMonthlyCost($productCost){
		return "";
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$v12Percent = $this->getV12Percent();
		$v12Pack = $this->getV12Pack();
		$enable = $this->getActive();
		$proWidget = $this->getProductWidget();
		$minAmount = $this->getMinAmount();
		$productPrice = $productCost;
		
		$finace_0 = 1; //$block->getCurrentProduct()->getIs0Intrest();
		if($finace_0 != 1) {
		$max = 36;
		}
		elseif($productPrice >= 2000) {
		$max = 36;
		} elseif($productPrice >= 1000) {
		$max = 24;
		}else {
		$max = 18;
		}
		//not interest free
		if($finace_0 != 1) {
			foreach ($v12Pack as $key => $value) {
			if (strpos($value, 'Free') !== false) {
				  unset($v12Pack[$key]);
		}
		if($max == 18) {
			if (strpos($value, '24 Months') !== false) {
						unset($v12Pack[$key]);
		}	if (strpos($value, '36 Months') !== false) {
			 unset($v12Pack[$key]);
		}
		}
		if($max == 24) {
			if (strpos($value, '36 Months') !== false) {
						unset($v12Pack[$key]);
		}
		}

		}
		}
		//interest free
		if($finace_0 != 0) {
		foreach ($v12Pack as $key => $value) {
				if (strpos($value, 'Classic') !== false) {
						unset($v12Pack[$key]);
		}
		if($max == 18) {
			if (strpos($value, '24 Months') !== false) {
			//	echo 'val' . $value;
						unset($v12Pack[$key]);
		}	if (strpos($value, '36 Months') !== false) {
		//	echo 'val' . $value;
						unset($v12Pack[$key]);
		}
		}
		if($max == 24) {
			if (strpos($value, '36 Months') !== false) {
						unset($v12Pack[$key]);
		}
		}
		}
		}
		if($finace_0 != 1) {
			$min = 12;
			$intrest = 15.9;
			$step = 12;
		}
		else {
		$min = 6;
		$step = 6;
		$intrest = 0;
		}
		$originalpercentage = 50;
		do{
		$percentage = $originalpercentage;
		$originalpercentage = $originalpercentage - 10;
		$deposit = substr(number_format(($percentage / 100) * $productPrice, 3), 0, -1);
		$calc = round($productPrice - $deposit,2);
		}
		while($calc < $minAmount);
		var_dump($calc);

		$interestfractions = [6 => 0.166667, 12 => 0.083333, 18 => 0.055556, 24 => 0.03459];

		$interestfraction = (isset($interestfractions[$max]))?$interestfractions[$max]:0.03459;
		//var_dump($interestfraction);
		if($intrest != 0) {
		$lowprice =  number_format(round($calc * $interestfraction,2),2);
		}
		else {
			$lowprice = number_format(round($calc / $max,2),2);
		}
		
		$lowMonthlyHtml = '<span class="savingslabel">Finance From £'.$lowprice.' P/M</span>';
        return $lowMonthlyHtml;
		
	}

    public function getApproveMsg() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/approve_msg',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getReferMsg() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/refer_msg',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getDeclineMsg() {

        return $this->_scopeConfig->getValue(

            'payment/vfinance/decline_msg',

            \Magento\Store\Model\ScopeInterface::SCOPE_STORE

        );

    }

    public function getRefundAmount() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $creditMemo = $objectManager->get('Magento\Sales\Model\Order\Creditmemo')->getCollection()
                            ->setOrder('entity_id','DESC')
                            ->setPageSize(1);
        $creditMemoData = $creditMemo->getData();
        $refundAmount = number_format($creditMemoData[0]['grand_total'], 2, '.', '' );

        return $refundAmount;

    }

    public function getSalesEmail() {

        $salesEmail = $this->_scopeConfig->getValue('trans_email/ident_sales/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        return $salesEmail;
    }

    public function getTagImageUrl() {

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $tagImage = $objectManager->get('Bootsgrid\Vfinance\Block\Product\View')->getTagImageUrl();
        return $tagImage;

    }

}
