<?php

/**

 * Copyright © 2016 Magento. All rights reserved.

 * See COPYING.txt for license details.

 */

namespace Bootsgrid\Vfinance\Block\Product;

use Magento\Framework\Json\Helper\Data;

class View extends \Magento\Framework\View\Element\Template

{
	protected $_registry;

    protected $_storeManager;

	public function __construct(

		\Magento\Framework\View\Element\Template\Context $context,

		\Magento\Framework\Registry $registry,

        \Magento\Store\Model\StoreManagerInterface $storeManager,

		array $data = [])

	{
		$this->_registry = $registry;

        $this->_storeManager = $storeManager;

		parent::__construct($context, $data);				

	}

    public function getCurrentProduct()
    {        

        return $this->_registry->registry('current_product');

    }

    public function getAccounts() {

        $accounts_config = $this->_scopeConfig->getValue('payment/vfinance/addFinance', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);

        /*$accounts = json_decode($accounts_config, true);*/
        
        /*$this->accounts = [];
        $fields = is_array($accounts) ? array_keys($accounts) : null;
        if (!empty($fields)) {
            foreach ($accounts[$fields[0]] as $i => $k) {
                if ($k) {
                    $account = [];
                    foreach ($fields as $field) {
                        $account[$field] = $accounts[$field][$i];
                    }
                    $this->accounts[] = $account;
                }
            }
        }*/

        return $accounts_config;
    }

    public function getImageUrl()
    {
        
        return $this->getViewFileUrl('Bootsgrid_Vfinance::images/vfinance.svg');
    }

    public function getTagImageUrl()
    {
        
        return $this->getViewFileUrl('Bootsgrid_Vfinance::images/lbanners.png');
    }

}