<?php

namespace Bootsgrid\Vfinance\Block\Adminhtml\System\Config\Form;

class Finance extends \Magento\Config\Block\System\Config\Form\Field
{
    
    /**
     * @param \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $this->setElement($element);

        $html = '<div id="finance_template">';
        $html .= '<input type="button" data-url="'.$this->getAjaxUrl().'" class="getV12finance" value="Get V12Finance Products" />';
        $html .= '</div>';

        $html .= "<script>
                    require(['jquery'], function ($) {
                        $('.getV12finance').click(function() {
                            var ajaxurl = $(this).attr('data-url');                            
                            var finance_req = 1;
                            $.ajax({  
                                showLoader: true,
                                data: {finance_req:finance_req},
                                url: ajaxurl,       
                                type: 'POST',
                                dataType: 'json',
                                success:function(response){
                                    location.reload();
                                }
                            })    
                        })
                    });
                </script>";        

        return $html;
    }

    public function getAjaxUrl() {
        
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $baseUrl = $objectManager->get('Magento\Framework\UrlInterface')->getBaseUrl();
        return $baseUrl.'vfinance/payment/finance';

    }
   
}
