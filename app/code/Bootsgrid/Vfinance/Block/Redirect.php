<?php

namespace Bootsgrid\Vfinance\Block;

class Redirect extends \Magento\Framework\View\Element\Template {

	protected $_scopeConfig;

	protected $_checkoutSession;

    protected $_customerSession;

    protected $_order;

	public function __construct(
		\Magento\Framework\View\Element\Template\Context $context,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, 
		\Magento\Checkout\Model\Session $checkoutSession, 
		\Magento\Customer\Model\Session $customerSession, 
		\Magento\Sales\Model\Order $order)
	{
		parent::__construct($context);
		$this->_scopeConfig = $scopeConfig;
		$this->_checkoutSession = $checkoutSession;
        $this->_customerSession = $customerSession;
        $this->_order = $order;
	}

	public function getConfig() {

		$storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORES;

		$api_key = $this->_scopeConfig->getValue("payment/vfinance/retailer_id", $storeScope);
		$inst_id = $this->_scopeConfig->getValue("payment/vfinance/retailer_guid", $storeScope);
		$mode = $this->_scopeConfig->getValue("payment/vfinance/auth_key", $storeScope);

		return $api_key.','.$inst_id.','.$mode;		

	}

	protected function getCheckout() {

        return $this->_checkoutSession;

    }
    
    protected function getOrder() {

        $lastOrderId = $this->_checkoutSession->getLastRealOrderId();

        /*$lastOrderId = $this->_checkoutSession->setOmniRealOrderId();*/

        $order = $this->_order->loadByIncrementId($lastOrderId);

        return $order;

    }

}