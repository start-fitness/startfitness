<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Bootsgrid\Vfinance\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class ConfigProvider implements ConfigProviderInterface
{ 
	
  protected $_scopeConfig;

  public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig) {
	  $this->_scopeConfig = $scopeConfig;      
  }

	 
  public function getConfig()
  {			         
  	$config = [
               'payment' => [
                    'vfinance' => [
                        'active' => $this->_scopeConfig->getValue('payment/vfinance/active',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'auth_key' => $this->_scopeConfig->getValue('payment/vfinance/auth_key',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'retailer_id' => $this->_scopeConfig->getValue('payment/vfinance/retailer_id',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'retailer_guid' => $this->_scopeConfig->getValue('payment/vfinance/retailer_guid',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'min_amt' => $this->_scopeConfig->getValue('payment/vfinance/min_finance_amount',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),
                        'v12percent' => $this->getV12Percent(),
                        'v12pack' => $this->getV12Pack(),      
                    ]
                ]
            ];          
    return $config;
  }

  public function getV12Pack() {
    $package = $this->_scopeConfig->getValue('payment/vfinance/packages', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    if($package != '') {
      $package_exp = explode(',', $package);
      /*$package_array = array();
      foreach ($package_exp as $key => $value){
        $package_value = explode(':', $value);
        if(count( $package_value) >= 2){
          $package_array[trim($package_value[0])] = trim($package_value[1]);
        }
      }*/
	
	//var_dump($package_exp);
    return $this->filterPackBasedOnCart($package_exp);
    } else {
      return array();
    }
  }
  
  public function filterPackBasedOnCart($package_exp){
	$fiance_type = array();
	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	$cart = $objectManager->get('\Magento\Checkout\Model\Cart');
	$quote = $cart->getQuote();
	$attributeSet = $objectManager->create('Magento\Eav\Api\AttributeSetRepositoryInterface');
	
	$itemsCollection = $quote->getItemsCollection();
	$items = $quote->getAllVisibleItems();

	foreach($items as $item) {
	  $product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getProductId());
	  $attributeSetRepository = $attributeSet->get($product->getAttributeSetId());
	  $attributeSetName = $attributeSetRepository->getAttributeSetName();
	//echo 'Name: '.$item->getName().'<br />';
	//echo 'Finance: ' . $product->getIs0Intrest().'<br />';

	if("Migration_StartBikes" == $attributeSetName || "Migration_CycleFrameset" == $attributeSetName || "Migration_CycleWheels" == $attributeSetName) {
	   $fiance_type = $product->getIs0Intrest();
	}
	//var_dump($fiance_type);
	//echo "<br />";
	}

	$result = ($fiance_type);
	//we have 2 types of finace in the cart
	$remove0percent = true;
	$removeinterest = true;
	$max = 36;
	if($result == 1) {
		$remove0percent = false;
	}
	else{
		$removeinterest = false;
	}
	$grandTotal = $quote->getGrandTotal();
	if($grandTotal >= 1000 && $grandTotal < 2000){
		$max = 24;
	}
	else if($grandTotal < 1000){
		$max = 18;
	}
	foreach($package_exp as $index => $name){
		$name = explode("/", $name);
		if($remove0percent && $name[4] == 0)	unset($package_exp[$index]);
		if($removeinterest && $name[4] != 0)	unset($package_exp[$index]);
		if($name[2] > $max) unset($package_exp[$index]);
	}
	return array_values($package_exp);
  }
     
  public function getV12Percent() {
    $percentage = $this->_scopeConfig->getValue('payment/vfinance/deposit_percent', \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    if($percentage != '') {
      $percentage_exp = explode(',',$percentage);
      $percent_array =array();
      foreach ($percentage_exp as $key => $value) {
        $percent_array[trim($value)]= trim($value.'%');
      }
      return $percent_array;
    } else {
      return array();
    }
  }

}

