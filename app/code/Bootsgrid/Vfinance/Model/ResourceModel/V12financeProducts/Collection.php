<?php

namespace Bootsgrid\Vfinance\Model\ResourceModel\V12financeProducts;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    
	protected $_idFieldName = 'id';

    /**
     * Initialize resource collection
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Bootsgrid\Vfinance\Model\V12financeProducts', 'Bootsgrid\Vfinance\Model\ResourceModel\V12financeProducts');
    }
}