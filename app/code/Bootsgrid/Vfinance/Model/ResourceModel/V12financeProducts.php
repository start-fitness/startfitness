<?php

namespace Bootsgrid\Vfinance\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class V12financeProducts extends AbstractDb
{
    
	public function __construct(
		\Magento\Framework\Model\ResourceModel\Db\Context $context
	)
	{
		parent::__construct($context);
	}
	
    /**
     * Initialize resource
     *
     * @return void
     */
    protected function _construct() {

        $this->_init('v12finance_products', 'id');
    }
}