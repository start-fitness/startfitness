<?php

namespace Bootsgrid\Vfinance\Model;

use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order;
/**
 * Pay In Store payment method model
 */
class PaymentMethod extends \Magento\Payment\Model\Method\AbstractMethod
{

    /**
     * Payment code
     *
     * @var string
     */
    protected $_code = 'vfinance';

    protected $_canAuthorize = true;
    protected $_canCapture = true;

    public function assignData(\Magento\Framework\DataObject $data)
    {
        $this->_eventManager->dispatch(
            'payment_method_assign_data_vfinance',
            [
                AbstractDataAssignObserver::METHOD_CODE => $this,
                AbstractDataAssignObserver::MODEL_CODE => $this->getInfoInstance(),
                AbstractDataAssignObserver::DATA_CODE => $data
            ]
        );

        return $this;
    }

    public function isAvailable(\Magento\Quote\Api\Data\CartInterface $quote = null)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->helper = $_objectManager->get('Bootsgrid\Vfinance\Helper\Data');

		$shippingAddress = $quote->getShippingAddress();
    $discount = $quote->getAppliedRuleIds();

		$countryCode = $shippingAddress->getData('country_id');

		if($countryCode != "GB") return false;
  if($discount > 0) return false;


        if ($quote && $quote->getBaseGrandTotal() < $this->helper->getMinAmount())
        {
            return false;
        }

        if (!$this->helper->getRetailerId() && !$this->helper->getRetailerguId()) {
            return false;
        }

        if ($this->helper->getActive() != 1 && !$this->helper->getAuthKey()) {
            return false;
        }

		$fiance_type = array();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();

		$attributeSet = $objectManager->create('Magento\Eav\Api\AttributeSetRepositoryInterface');

		$itemsCollection = $quote->getItemsCollection();
		$items = $quote->getAllVisibleItems();

		foreach($items as $item) {
		  $product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getProductId());
		  $attributeSetRepository = $attributeSet->get($product->getAttributeSetId());
		  $attributeSetName = $attributeSetRepository->getAttributeSetName();
		//echo 'Name: '.$item->getName().'<br />';
		//echo 'Finance: ' . $product->getIs0Intrest().'<br />';

		if("Migration_StartBikes" == $attributeSetName || "Migration_CycleFrameset" == $attributeSetName || "Migration_CycleWheels" == $attributeSetName) {
		   $fiance_type[] = $product->getIs0Intrest();
		}
		//var_dump($fiance_type);
		//echo "<br />";
		}

		$result = array_unique($fiance_type);
		//we have 2 types of finace in the cart
		if(count($result) > 1) {
			return false;
		}

        return parent::isAvailable($quote);
    }

    public function capture(InfoInterface $payment, $amount)
    {
       return $this;
    }

    public function authorize(InfoInterface $payment, $amount)
    {

        $order = $payment->getOrder();

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->logger = $_objectManager->get('Bootsgrid\Vfinance\Helper\Logger');
        $client = $_objectManager->get('Bootsgrid\Vfinance\Helper\SoapClient');

        $appRequest = $this->_getApplicationRequest($order);
        try {
            $response = $client->soapCall("SubmitApplication", "applicationRequest", $appRequest);
            /*$this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());*/

            // If an error was returned
            if($response->SubmitApplicationResult->Status == "ErrorProcessing") {
                $exceptionMessage = 'There was an error processing your application.  Please try again later.';

                if (isset($response->SubmitApplicationResult->Errors->ApplicationStatusResponseError)) {
                    $error = $response->SubmitApplicationResult->Errors->ApplicationStatusResponseError;
                    $this->logger->log(print_r($error));

                    if (isset($error->Description)) {
                        $exceptionMessage = $error->Description;
                    }
                }
                throw new \Exception(__($exceptionMessage));
            }
            $result = $response->SubmitApplicationResult;

            $payment->setAdditionalInformation("V12AppUrl", $result->ApplicationFormUrl);
            $payment->setAdditionalInformation("V12AuthCode", $result->AuthorisationCode);
            $payment->setAdditionalInformation("V12ApplicationId", $result->ApplicationId);
            $payment->setAdditionalInformation("V12ApplicationStatus", $result->Status);

        }
        catch (Exception $e) {
            $exceptionMessage = 'There was an error processing your application.  Please try again later.';
            // Otherwise the exception is thrown by the SOAP request.
            $this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());

            throw new \Exception(__($exceptionMessage));
        }
        return $this;
    }

    public function _getApplicationRequest(Order $order)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $data = array(
            "WaitForDecision"     => false,
        );
        $retailer = $this->_getRetailer();
        $apiOrder = $this->_getApiOrder($order);

        $appRequest = $_objectManager->get('Bootsgrid\Vfinance\Model\Soap\Api\ApplicationRequest');
        $appRequest->setData($data);

        $appRequest->Retailer = $retailer;
        $appRequest->Order = $apiOrder;

        return $appRequest;

    }

    public function _getRetailer()
    {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->helper = $_objectManager->get('Bootsgrid\Vfinance\Helper\Data');

        $data = array(
            "AuthenticationKey" => $this->helper->getAuthKey(),
            "RetailerGuid"      => $this->helper->getRetailerguId(),
            "RetailerId"        => $this->helper->getRetailerId(),
        );

        $retailer = $_objectManager->get('Bootsgrid\Vfinance\Model\Soap\Api\Retailer');
        $retailer->setData($data);

        return $retailer;

    }

    public function _getApiOrder(Order $order)
    {
        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $grandTotal = $order->getGrandTotal();
        $orderTotal = number_format($grandTotal, 2, '.', '' );

        $payment = $order->getPayment();
        $finPercent = $payment->getPoNumber();
        $products_exp = explode('/', $payment->getCcType());
        $productId = $products_exp[0];
        $productGuid = $products_exp[1];
        $depositamount = $orderTotal * $finPercent / 100;
        $depositamount = number_format($depositamount, 2, '.', '' );

        $data = array(
            "ProductId"         => $productId,
            "ProductGuid"       => $productGuid,
            "SalesReference"    => $order->getIncrementId(),
            "CashPrice"         => $orderTotal,
            "Deposit"           => $depositamount,
            "DuplicateSalesReferenceMethod" => "ShowError"
        );

        $apiOrder = $_objectManager->get('Bootsgrid\Vfinance\Model\Soap\Api\Order');
        $apiOrder->setData($data);

        return $apiOrder;

    }

    public function doUpdateApplicationRequest(Order $order, $status) {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->logger = $_objectManager->get('Bootsgrid\Vfinance\Helper\Logger');
        $client = $_objectManager->get('Bootsgrid\Vfinance\Helper\SoapClient');

        $updateRequest = $this->_getApplicationUpdateRequest($order, $status);

        try {
            $response = $client->soapCall("UpdateApplication", "applicationUpdateRequest", $updateRequest);

            // If an error was returned
            if($response->UpdateApplicationResult->Status == "ErrorProcessing") {
                $error = $response->UpdateApplicationResult->Errors->ApplicationStatusResponseError;
                $this->logger->log($error->Code . ": " . $error->Description . "." . ($error->Reference ? "Ref: " . $error->Reference : ''));
            }

            $this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());

            $responseStatus = $response->UpdateApplicationResult->Status;

            $requestPaymentSuccessfulResponses = array(
                'PaymentRequested',
                'PaymentProcessed'
            );

            // If the status of the application has not been updated by the API call
            if(($status == 'RequestPayment' && (!in_array($responseStatus, $requestPaymentSuccessfulResponses))) || ($status == 'Cancel' && $responseStatus != 'Cancelled')) {
                throw new \Exception(__("Unable to set the finance application status, if this problem persists please contact V12 Finance on 02920 468900, quoting application ID ".$updateRequest->ApplicationId."."));
            }

            $payment = $order->getPayment();
            $payment->setAdditionalInformation("V12ApplicationStatus", $response->UpdateApplicationResult->Status);
            $payment->save();

            $assignStatus = $_objectManager->create('Magento\Sales\Model\Order\Status');
            if($status == 'PartialRefund') {

                $orderIncid = $order->getIncrementId();
                $appId = $payment->getAdditionalInformation("V12ApplicationId");
                $firstname = $order->getBillingAddress()->getFirstName();
                $lastname = $order->getBillingAddress()->getLastName();
                $email = $order->getCustomerEmail();
                $adminEmail = $_objectManager->get('Bootsgrid\Vfinance\Helper\Data')->getSalesEmail();
                $to = $email;
                $subject = "Partial Refund Requested";
                $message = $firstname.' '.$lastname." requested refund amount ".$this->getRefundAmount().' for '.$appId;
                $headers = "From: " . strip_tags($adminEmail) . "\r\n";
                mail($to,$subject,$message,$headers);

                $assignStatus->setData('status', 'v12finance_refund_request')->setData('label', 'V12finance Refund Request')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Refund Requested.');
                $order->setStatus('v12finance_refund_request');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Refund Requested.', true
                );
                $order->setEmailSent(true);
                $order->save();
            }

        } catch (Exception $e) {
            $this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());

            throw new \Exception(__($e->getMessage()));
        }
    }

    protected function _getApplicationUpdateRequest(Order $order, $status) {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $payment = $order->getPayment();
        if($status != 'PartialRefund') {
            $data = array(
                "ApplicationId"     => $payment->getAdditionalInformation("V12ApplicationId"),
                "Update"            => $status,
            );
        } else {
            $data = array(
                "ApplicationId"     => $payment->getAdditionalInformation("V12ApplicationId"),
                "Update"            => $status,
                "RefundAmount"      => $this->getRefundAmount(),
            );
        }

        $retailer = $this->_getRetailer();

        $updateRequest = $_objectManager->get('Bootsgrid\Vfinance\Model\Soap\Api\UpdateRequest');
        $updateRequest->setData($data);

        $updateRequest->Retailer  = $retailer;

        return $updateRequest;

    }

    public function doFinanceProductListRequest() {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->logger = $_objectManager->get('Bootsgrid\Vfinance\Helper\Logger');
        $client = $_objectManager->get('Bootsgrid\Vfinance\Helper\SoapClient');

        $data = $this->_getRetailer();

        $request = $this->_getFinanceProductListReuqest();

        try {
            $response = $client->soapCall("GetRetailerFinanceProducts", "financeProductListRequest", $request);

            // If an error was returned
            if(isset($response->GetRetailerFinanceProductsResult->Errors->ApplicationStatusResponseError)) {
                $error = $response->GetRetailerFinanceProductsResult->Errors->ApplicationStatusResponseError;
                $this->logger->log(($error->Code . ": " . $error->Description . "." . ($error->Reference ? "Ref: " . $error->Reference : '')));
            }

            /*$this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());*/

            return $response;


        } catch (Exception $e) {
            $this->logger->log("*** REQUEST: " . $client->getLastRequest());
            $this->logger->log("*** RESPONSE: " . $client->getLastResponse());

            throw new \Exception($e->getMessage());
        }
    }

    protected function _getFinanceProductListReuqest() {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $request = $_objectManager->get('Bootsgrid\Vfinance\Model\Soap\Api\FinanceProductListRequest');
        $request->Retailer  = $this->_getRetailer();

        return $request;
    }

    protected function getRefundAmount() {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $this->helper = $_objectManager->get('Bootsgrid\Vfinance\Helper\Data');
        $refundAmount = $this->helper->getRefundAmount();

        return $refundAmount;
    }

    /*public function getOrderPlaceRedirectUrl()
    {
        return $this->getUrl('vfinance/payment/redirect', array('_secure' => false));

    }*/

}
