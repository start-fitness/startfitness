<?php

namespace Bootsgrid\Vfinance\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

class V12financeProducts extends AbstractModel implements \Magento\Framework\DataObject\IdentityInterface {
    
    const CACHE_TAG = 'bootsgrid_vfinance_v12financeproducts';
    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $_dateTime;

    /**
     * @return void
     */
    protected function _construct() {
        $this->_init('Bootsgrid\Vfinance\Model\ResourceModel\V12financeProducts');
    }

    public function getIdentities() {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues() {
        $values = [];
        return $values;
    }

}