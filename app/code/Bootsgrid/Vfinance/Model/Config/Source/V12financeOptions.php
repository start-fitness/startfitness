<?php

namespace Bootsgrid\Vfinance\Model\Config\Source;

class V12financeOptions implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * {@inheritdoc}
     */
    public function toOptionArray() {

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $V12financeProducts = $_objectManager->create('Bootsgrid\Vfinance\Model\V12financeProducts');
        $collection = $V12financeProducts->getCollection();

        $options = [];
        foreach ($collection->getData() as $key => $item) {
            
            $options[] = ['value' => $item['product_id'].'/'.$item['product_guid'].'/'.$item['months'].'/'.$item['calc_factor'].'/'.$item['apr'].'/'.$item['name'], 'label' => $item['name']];
            
        }

        return $options;
    }
}
