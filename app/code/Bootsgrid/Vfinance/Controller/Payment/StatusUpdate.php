<?php

namespace Bootsgrid\Vfinance\Controller\Payment;

use Magento\Sales\Model\Order;

class StatusUpdate extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_order;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->_pageFactory = $pageFactory;
        $this->_order = $order;
        $this->_checkoutSession = $checkoutSession;        
        return parent::__construct($context);
    }

    public function execute() {       
        
        $orderIncId = $_GET['SR'];
        $status = $_GET['Status'];
        $appId = $_GET['REF'];
        $authCode = $_GET['Auth'];

        $order = $this->_order->loadByIncrementId($orderIncId);

        $payment = $order->getPayment();

        if(!$order->getId()) {
            $exceptionMessage = 'Order Not Found';
            throw new \Exception(__($exceptionMessage));        
        }

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $assignStatus = $_objectManager->create('Magento\Sales\Model\Order\Status');
        
        $session = $this->_checkoutSession;
        $session->setLastOrderId($order->getId());
        $session->setLastRealOrderId($orderIncId);

        switch ($status) {
            case 'R':

                $assignStatus->setData('status', 'v12finance_referred')->setData('label', 'V12finance Referred Pending Payment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Referred Pending Payment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Referred Pending Payment.');
                $order->setStatus('v12finance_referred');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Referred Pending Payment.', true
                );
                $order->setEmailSent(true);
                $order->save();               

                break;            

            case 'D':
                
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Canceled');
                $order->setState(Order::STATE_CANCELED, true, 'The V12finance was Canceled.');
                $order->setStatus('canceled');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Canceled.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;              

            case 'A':

                $assignStatus->setData('status', 'v12finance_accepted')->setData('label', 'V12finance Accepted Pending Payment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Accepted Pending Payment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Accepted Pending Payment.');
                $order->setStatus('v12finance_accepted');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Accepted Pending Payment.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;                  

            case 'M':

                $assignStatus->setData('status', 'v12finance_amended')->setData('label', 'V12finance Amended')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Amended');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Amended.');
                $order->setStatus('v12finance_amended');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Amended.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;                  

            case 'C':

                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Cancelled');
                $order->setState(Order::STATE_CANCELED, true, 'The V12finance was Canceled.');
                $order->setStatus('canceled');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Cancelled.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;

            case 'S':   

                $assignStatus->setData('status', 'v12finance_awaitingFulfilment')->setData('label', 'V12finance AwaitingFulfilment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance AwaitingFulfilment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was AwaitingFulfilment.');
                $order->setStatus('v12finance_awaitingFulfilment');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was AwaitingFulfilment.', true
                );
                $order->setEmailSent(true);
                $order->save();              

                break;    

        }       

        $this->_redirect('checkout/onepage/success');

    }

}