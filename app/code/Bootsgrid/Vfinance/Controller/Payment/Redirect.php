<?php

namespace Bootsgrid\Vfinance\Controller\Payment;

use Magento\Sales\Model\Order;

class Redirect extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_order;
    protected $_checkoutSession;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->_pageFactory = $pageFactory;
        $this->_order = $order;
        $this->_checkoutSession = $checkoutSession;
        return parent::__construct($context);
    }

    public function execute() {

        $lastOrderId = $this->_checkoutSession->getLastRealOrderId();
        $order = $this->_order->loadByIncrementId($lastOrderId);

        if (!$order->getId()) {
            throw new \Exception('No order for processing found');
        }
        $payment = $order->getPayment();

        $order->setState(Order::STATE_PENDING_PAYMENT, true, 'Customer was redirected to V12finance.');
        $order->setStatus('pending_payment');
        $order->save();

        if ($payment->getAdditionalInformation("V12AppUrl")) {
            $V12AppUrl = $payment->getAdditionalInformation("V12AppUrl");
            echo '<meta http-equiv="refresh" content="0;url='.$V12AppUrl.'" />';exit;
        }

    }
}
