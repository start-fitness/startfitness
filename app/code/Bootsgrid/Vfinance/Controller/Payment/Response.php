<?php

namespace Bootsgrid\Vfinance\Controller\Payment;

use Magento\Sales\Model\Order;

class Response extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;
    protected $_checkoutSession;
    protected $_order;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory,
        \Magento\Sales\Model\Order $order,
        \Magento\Checkout\Model\Session $checkoutSession)
    {
        $this->_pageFactory = $pageFactory;
        $this->_order = $order;
        $this->_checkoutSession = $checkoutSession;        
        return parent::__construct($context);
    }

    public function execute() {
       
        $orderIncId = $this->getRequest()->getPost("SalesReference");
        $appId = $this->getRequest()->getPost("ApplicationId");
        $status = $this->getRequest()->getPost("Status");
        $authCode = '';

        $order = $this->_order->loadByIncrementId($orderIncId);

        $payment = $order->getPayment();

        if(!$order->getId()) {
            $exceptionMessage = 'Order Not Found';
            throw new \Exception(__($exceptionMessage));        
        }

        $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $assignStatus = $_objectManager->create('Magento\Sales\Model\Order\Status');

        switch ($status) {

                case 1:

                $assignStatus->setData('status', 'v12finance_acknowledged')->setData('label', 'V12finance Acknowledged Pending Payment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Acknowledged Pending Payment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Acknowledged Pending Payment.');
                $order->setStatus('v12finance_acknowledged');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Acknowledged Pending Payment.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break; 

            case 2:

                $assignStatus->setData('status', 'v12finance_referred')->setData('label', 'V12finance Referred Pending Payment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Referred Pending Payment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Referred Pending Payment.');
                $order->setStatus('v12finance_referred');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Referred Pending Payment.', true
                );
                $order->setEmailSent(true);
                $order->save();               

                break;            

            case 3:
                
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Canceled');
                $order->setState(Order::STATE_CANCELED, true, 'The V12finance was Canceled.');
                $order->setStatus('canceled');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Canceled.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;              

            case 4:

                $assignStatus->setData('status', 'v12finance_accepted')->setData('label', 'V12finance Accepted Pending Payment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Accepted Pending Payment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was Accepted Pending Payment.');
                $order->setStatus('v12finance_accepted');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Accepted Pending Payment.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;  

            case 5:

                $assignStatus->setData('status', 'v12finance_awaitingFulfilment')->setData('label', 'V12finance AwaitingFulfilment')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance AwaitingFulfilment');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was AwaitingFulfilment.');
                $order->setStatus('v12finance_awaitingFulfilment');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was AwaitingFulfilment.', true
                );
                $order->setEmailSent(true);
                $order->save();              

                break;

            case 6:
                
                $appId = $payment->getAdditionalInformation("V12ApplicationId");
                /*$firstname = $order->getBillingAddress()->getFirstName();
                $lastname = $order->getBillingAddress()->getLastName();*/
                $email = $order->getCustomerEmail();
                $adminEmail = $_objectManager->get('Bootsgrid\Vfinance\Helper\Data')->getSalesEmail();
                $to = $email;
                $subject = "Payment Requested";
                $message = "Payment has been requested for application id: ".$appId;
                $headers = "From: " . strip_tags($adminEmail) . "\r\n";
                mail($to,$subject,$message,$headers);    

                $assignStatus->setData('status', 'v12finance_paymentRequested')->setData('label', 'V12finance PaymentRequested')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance PaymentRequested');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was PaymentRequested.');
                $order->setStatus('v12finance_paymentRequested');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was PaymentRequested.', true
                );
                $order->setEmailSent(true);
                $order->save();              

                break;
            
            case 7:

                $assignStatus->setData('status', 'v12finance_paymentProcessed')->setData('label', 'V12finance PaymentProcessed')->save();
                $assignStatus->assignState(Order::STATE_NEW, true);
                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance PaymentProcessed');
                $order->setState(Order::STATE_NEW, true, 'The V12finance was PaymentProcessed.');
                $order->setStatus('v12finance_paymentProcessed');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was PaymentProcessed.', true
                );
                $order->setEmailSent(true);
                $order->save();              

                break;

            case 100:

                $payment->setAdditionalInformation("V12AuthCode", $authCode);
                $payment->setAdditionalInformation("V12ApplicationId", $appId);
                $payment->setAdditionalInformation("V12ApplicationStatus", 'V12finance Cancelled');
                $order->setState(Order::STATE_CANCELED, true, 'The V12finance was Canceled.');
                $order->setStatus('canceled');
                $order->addStatusToHistory(
                    $order->getStatus(), 'The V12finance was Cancelled.', true
                );
                $order->setEmailSent(true);
                $order->save();

                break;              

        }

    }

}