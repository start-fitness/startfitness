<?php

namespace Bootsgrid\Vfinance\Controller\Payment;

class Finance extends \Magento\Framework\App\Action\Action {
	
	protected $_pageFactory;

	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $pageFactory)
	{
		$this->_pageFactory = $pageFactory;
		return parent::__construct($context);
	}

	public function execute() {
		
		$_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$finance_req = $this->getRequest()->getParam('finance_req');
		if ($finance_req) {
						
			$Getv12financeProducts = $_objectManager->create('Bootsgrid\Vfinance\Model\V12financeProducts');
        	$collection = $Getv12financeProducts->getCollection();

        	$proArray = array();
			foreach ($collection->getData() as $key => $value) {
				$proArray[] = $value['product_id'];
			}

        	//if(empty($collection->getData())) {
    		$financeProductsModel = $_objectManager->get('Bootsgrid\Vfinance\Model\PaymentMethod');
			$financeProductsResponse = $financeProductsModel->doFinanceProductListRequest();			

			$financeProducts = $financeProductsResponse->GetRetailerFinanceProductsResult->FinanceProducts->FinanceProduct;			

			foreach ($financeProducts as $key => $financeProduct) {
				if(!in_array($financeProduct->ProductId, $proArray)) {
					$V12financeProducts = $_objectManager->create('Bootsgrid\Vfinance\Model\V12financeProducts');
					$V12financeProducts->setProduct_id($financeProduct->ProductId);
			        $V12financeProducts->setProduct_guid($financeProduct->ProductGuid);
			        $V12financeProducts->setMonths($financeProduct->Months);
			        $V12financeProducts->setCalc_factor($financeProduct->CalculationFactor);
			        $V12financeProducts->setApr($financeProduct->APR);
			        $V12financeProducts->setName($financeProduct->Name);
			        $V12financeProducts->setTag($financeProduct->Tag);
			        $V12financeProducts->save();
			    }    	
			}
			//}	

			$message = __(
	            'V12finance products are successfully added.'
	        );
	        /*$context = $_objectManager->get('Magento\Framework\App\Action\Context');
	        $context->getMessageManager()->addSuccess($message);*/ 
			
		} else {

			$message = __(
	            'Something Went Wrong in Your Request.'
	        );
	        /*$context = $_objectManager->get('Magento\Framework\App\Action\Context');
	        $context->getMessageManager()->addError($message);*/ 

		}		
		//return $this->_pageFactory->create();
	}

}