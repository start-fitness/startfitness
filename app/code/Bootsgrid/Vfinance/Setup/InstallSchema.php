<?php 

namespace Bootsgrid\Vfinance\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;

use Magento\Framework\Setup\ModuleContextInterface;

use Magento\Framework\Setup\SchemaSetupInterface;

use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface {

    /**

     * Installs DB schema for a module

     *

     * @param SchemaSetupInterface $setup

     * @param ModuleContextInterface $context

     * @return void

     */

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {

        $installer = $setup;

        $installer->startSetup();

        $table = $installer->getConnection()

            ->newTable($installer->getTable('v12finance_products'))

            ->addColumn(

                'id',

                Table::TYPE_INTEGER,

                11,

                ['identity' => true, 'nullable' => false, 'primary' => true],

                'ID'

            )

            ->addColumn('product_id', Table::TYPE_INTEGER, 11, ['nullable' => false], 'Product Id')

            ->addColumn('product_guid', Table::TYPE_TEXT, 100, ['nullable' => false], 'Product GuId')

            ->addColumn('months', Table::TYPE_TEXT, 100, ['nullable' => false], 'Months')

            ->addColumn('calc_factor', Table::TYPE_TEXT, 100, ['nullable' => false], 'Calculation Factor')

            ->addColumn('apr', Table::TYPE_TEXT, 100, ['nullable' => false], 'Apr')

            ->addColumn('name', Table::TYPE_TEXT, 100, ['nullable' => false], 'Product Name')

            ->addColumn('tag', Table::TYPE_TEXT, 100, ['nullable' => false], 'Product tag')

            ->setComment('Bootsgrid V12finance Products');

        $installer->getConnection()->createTable($table);

        $table2 = $installer->getConnection()

            ->newTable($installer->getTable('v12_app'))

            ->addColumn(

                'id',

                Table::TYPE_INTEGER,

                11,

                ['identity' => true, 'nullable' => false, 'primary' => true],

                'ID'

            )

            ->addColumn('appurl', Table::TYPE_TEXT, 255, ['nullable' => false], 'App Url')

            ->setComment('Bootsgrid V12App');

        $installer->getConnection()->createTable($table2);

        $installer->endSetup();

    }

}