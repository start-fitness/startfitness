<?php

namespace Bootsgrid\Vfinance\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class ShipmentOrder implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();

        if($order->getPayment()->getMethod() == "vfinance") {            
            try {
                // Send API Update request
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $method = $_objectManager->get('Bootsgrid\Vfinance\Model\PaymentMethod');
                $method->doUpdateApplicationRequest($order, 'RequestPayment');
                
            } catch (Exception $e) {
                // Clear other messages, as Magento adds success message before saving shipment... (bad Magento)
                throw new \Exception(__("Shipment not created. " . $e->getMessage()));            
            }
        }

    }
}