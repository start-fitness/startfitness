<?php

namespace Bootsgrid\Vfinance\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class RefundOrder implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $creditmemo = $observer->getEvent()->getCreditmemo();
        $order = $creditmemo->getOrder();

        if($order->getPayment()->getMethod() == "vfinance") {            
            try {
                // Send API Update request
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $method = $_objectManager->get('Bootsgrid\Vfinance\Model\PaymentMethod');
                $method->doUpdateApplicationRequest($order, 'PartialRefund');
                
            } catch (Exception $e) {
                // Clear other messages, as Magento adds success message before order cancel... (bad Magento)
                throw new \Exception(__("Order not refunded. " . $e->getMessage()));            
            }
        }

    }
}