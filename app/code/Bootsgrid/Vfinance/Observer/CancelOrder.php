<?php

namespace Bootsgrid\Vfinance\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;

class CancelOrder implements ObserverInterface
{
    public function execute(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();

        if($order->getPayment()->getMethod() == "vfinance") {            
            try {
                // Send API Update request
                $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
                $method = $_objectManager->get('Bootsgrid\Vfinance\Model\PaymentMethod');
                $method->doUpdateApplicationRequest($order, 'Cancel');
                
            } catch (Exception $e) {
                // Clear other messages, as Magento adds success message before order cancel... (bad Magento)
                throw new \Exception(__("Order not Cancelled. " . $e->getMessage()));            
            }
        }

    }
}