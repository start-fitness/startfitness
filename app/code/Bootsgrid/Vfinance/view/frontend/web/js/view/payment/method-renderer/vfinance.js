define(
    [
        'ko',
        'jquery',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/action/select-payment-method',
        'Magento_Checkout/js/checkout-data',
        'Bootsgrid_Vfinance/js/action/set-payment-method-action',
        'Magento_Checkout/js/model/quote'
    ],
    function (ko, $, Component, selectPaymentMethodAction, checkoutData, setPaymentMethodAction, quote) {
        'use strict';


//      var new_amount = $('.grand.incl .price').text();




        return Component.extend({


            v12Percent: '',
            v12Pack: '',
            defaults: {
                redirectAfterPlaceOrder: false,
                template: 'Bootsgrid_Vfinance/payment/vfinance'
            },
            afterPlaceOrder: function () {
                setPaymentMethodAction(this.messageContainer);
                return false;
            },
            selectPaymentMethod: function() {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                return true;
            },
            getCode: function() {
                return 'vfinance';
            },
            isActive: function() {
                return true;
            },
            getV12Percentage: function () {
                return window.checkoutConfig.payment.vfinance.v12percent;
            },
            getVfPercentageValues: function () {
                return _.map(this.getV12Percentage(), function (value, key) {
                    return {
                        'value': key,
                        'month': value
                    };
                });
            },
            getV12Package: function () {
                return window.checkoutConfig.payment.vfinance.v12pack;
            },
            getVfPackageValues: function () {
                return _.map(this.getV12Package(), function (value, key) {
                    var v12_value_split = value.split('/');
                    return {
                        'value': value,
                        'month': v12_value_split[5]
                    };
                });
            },
            selectvfpercentage: function(){
                $('#vfinance_package').prop('selectedIndex',0);
                $('#show_v12_finance').hide();
                $('.v12_error').show();
                $('.v12deperror').hide();
                $('#show_not_available').hide();
            },
            selectvfpackage: function(){
                var v12_pack = $('#vfinance_package option:selected').val();
                var v12_pack_split = v12_pack.split('/');
                var v12_pack_txt = $('#vfinance_package option:selected').text();
                var v12_percent = $('#vfinance_percentage option:selected').val();
                if(v12_pack != '' && v12_percent != '') {
                  var amount = window.checkoutConfig.totalsData.base_grand_total;
                   var new_amount = $('.grand.incl .price').text();
                  // alert(new_amount);
                    var new_amount = new_amount.replace("£","");
                    //alert(new_amount);
                    var new_amount = new_amount.replace(",","");
                    //alert(new_amount);


                //  console.log(quote);

                  //  alert(amount);
                  //  var amount = parseFloat(amount).toFixed(2);
                   var amount = new_amount;

                    //alert(amount);
                    var deposit = (amount / 100) * v12_percent;
                    var v12_loan_amt = amount - deposit;
                    var v12_interest = v12_pack_split[3];
                    var v12_apr = v12_pack_split[4];
                    var v12_months = v12_pack_split[2];
                    var v12_monthly_payment = v12_loan_amt * v12_interest;
                    var v12_total_amount_payable = v12_monthly_payment * v12_months + deposit;
                    var v12_cost_of_loan = Math.abs(v12_total_amount_payable - v12_loan_amt - deposit);

                    if(v12_apr == 0) {
                    v12_cost_of_loan = 0;
                  }
					if(v12_cost_of_loan <= 0.05) v12_cost_of_loan = 0;
                    $('.v12_error').hide();
                    if (true || v12_loan_amt >= 250) {
                        document.getElementById('show_v12_finance').style.display="block";
                        document.getElementById('v12_pack_name').innerHTML = v12_pack_txt;
                        document.getElementById('v12_cart_val').innerHTML = '£'+amount;
                        document.getElementById('v12_deposit_amt').innerHTML = '£'+deposit.toFixed(2);
                        document.getElementById('v12_loan_amt').innerHTML = '£'+v12_loan_amt.toFixed(2);
                        document.getElementById('v12_interest_rate').innerHTML = v12_apr+'%';
                        document.getElementById('v12_montly_pay').innerHTML = '£'+v12_monthly_payment.toFixed(2);
                        document.getElementById('v12_payments').innerHTML = v12_months;
                        document.getElementById('v12_cost_credit').innerHTML = '£'+v12_cost_of_loan.toFixed(2);
                        if(v12_apr == 0){
                          document.getElementById('v12_total_cost').innerHTML = '£'+amount;
                        }
                        else {
                          document.getElementById('v12_total_cost').innerHTML = '£'+v12_total_amount_payable.toFixed(2);
                        }
                    } else {
                        $('#show_not_available').show();
                    }
                } else {
                    $('show_v12_finance').hide();
                    $('.v12deperror').show();
                }
            },
            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {
                        'v12Percent': $('#vfinance_percentage').val(),
                        'v12Pack': $('#vfinance_package').val()
                    }
                };
            },
        });
    }
);
