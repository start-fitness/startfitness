define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'vfinance',
                component: 'Bootsgrid_Vfinance/js/view/payment/method-renderer/vfinance'
            }
        );
        return Component.extend({});
    }
);