<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Model\ResourceModel;

class CurrencyOrder extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     */
    protected function _construct()
    {
        $this->_init('bss_currency_order', 'id');
    }

    /**
     * @throws \Zend_Db_Statement_Exception
     */
    public function import()
    {
        $connection = $this->_resources->getConnection();
        $order = $this->_resources->getTableName('sales_order');
        $bss_currency_order = $this->_resources->getTableName('bss_currency_order');

        // clear before import
        $connection->truncateTable($bss_currency_order);

        $data_imp = [];
        $data = [
            'entity_id',
            'store_to_base_rate',
            'store_to_order_rate',
            'base_currency_code',
            'store_currency_code',
            'order_currency_code',
            'global_currency_code',
            'base_to_global_rate',
            'base_to_order_rate'
        ];

        $sql = $connection->select()->from(['bss_order' => $order], $data);
        $result = $connection->query($sql);
        while ($row = $result->fetch()) {
            if (!empty($row)) {
                $row['order_id'] = $row['entity_id'];
                unset($row['entity_id']);
                $data_imp[] = $row;
            }
        }

        if (!empty($data_imp)) {
            $connection->insertMultiple($bss_currency_order, $data_imp);
        }
    }
}
