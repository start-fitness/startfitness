<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Override\Model;

use Magento\Quote\Model\Quote\Address;
use Magento\SalesRule\Model\Quote\ChildrenValidationLocator;
use Magento\Framework\App\ObjectManager;

class RulesApplier extends \Magento\SalesRule\Model\RulesApplier
{
    /**
     * @param \Magento\SalesRule\Model\Rule\Action\Discount\Data $discountData
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @return $this
     */
    protected function setDiscountData($discountData, $item)
    {
        $helper = ObjectManager::getInstance()->create(\Bss\CheckoutWithDisplayCurrency\Helper\Data::class);

        $item->setDiscountAmount($discountData->getAmount());
        $item->setOriginalDiscountAmount($discountData->getOriginalAmount());
        if ($helper->isEnabled()) {
            $item->setBaseDiscountAmount($discountData->getAmount());
            $item->setBaseOriginalDiscountAmount($discountData->getOriginalAmount());
        } else {
            $item->setBaseDiscountAmount($discountData->getBaseAmount());
            $item->setBaseOriginalDiscountAmount($discountData->getBaseOriginalAmount());
        }
        
        return $this;
    }
}
