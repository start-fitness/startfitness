<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Override\Model\ResourceModel\Reports;

use Magento\Framework\DB\Select;
use Magento\Framework\App\ObjectManager;

class Collection extends \Magento\Reports\Model\ResourceModel\Order\Collection
{

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\DB\Sql\ExpressionFactory
     */
    protected $sql_expression;

    /**
     * construct
     */
    protected function _construct()
    {
        parent::_construct();
        $this->helper = ObjectManager::getInstance()->create(
            \Bss\CheckoutWithDisplayCurrency\Helper\Data::class
        );
        $this->sql_expression = ObjectManager::getInstance()->create(
            \Magento\Framework\DB\Sql\ExpressionFactory::class
        );
    }

    /**
     * Prepare report summary from live data
     *
     * @param string $range
     * @param mixed $customStart
     * @param mixed $customEnd
     * @param int $isFilter
     * @return $this
     */
    protected function _prepareSummaryLive($range, $customStart, $customEnd, $isFilter = 0)
    {
        $this->setMainTable('sales_order');
        $connection = $this->getConnection();

        /**
         * Reset all columns, because result will group only by 'created_at' field
         */
        $this->getSelect()->reset(\Magento\Framework\DB\Select::COLUMNS);

        $expression = $this->_getSalesAmountExpression();
        if ($isFilter == 0) {
            $this->getSelect()->columns(
                [
                    'revenue' => $this->getBsExpression(
                        sprintf(
                            'SUM((%s) * %s)',
                            $expression,
                            $connection->getIfNullSql('main_table.base_to_global_rate', 0)
                        )
                    ),
                ]
            );
        } else {
            if ($this->helper->isEnabled()) {
                $this->getSelect()->columns(
                    [
                        'revenue' => $this->getBsExpression(
                            sprintf(
                                'SUM((%s) * %s)',
                                $expression,
                                $connection->getIfNullSql('main_table.base_to_order_rate', 0)
                            )
                        ),
                    ]
                );
            } else {
                $this->getSelect()->columns(['revenue' => $this->getBsExpression(sprintf('SUM(%s)', $expression))]);
            }
        }

        $dateRange = $this->getDateRange($range, $customStart, $customEnd);

        $tzRangeOffsetExpression = $this->_getTZRangeOffsetExpression(
            $range,
            'created_at',
            $dateRange['from'],
            $dateRange['to']
        );

        // @codingStandardsIgnoreStart
        $this->getSelect()->columns(
            ['quantity' => 'COUNT(main_table.entity_id)', 'range' => $tzRangeOffsetExpression]
        )->where(
            'main_table.state NOT IN (?)',
            [\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, \Magento\Sales\Model\Order::STATE_NEW]
        )->order(
            'range',
            \Magento\Framework\DB\Select::SQL_ASC
        )->group(
            $tzRangeOffsetExpression
        );
        // @codingStandardsIgnoreEnd

        $this->addFieldToFilter('created_at', $dateRange);

        return $this;
    }

    /**
     * Calculate totals live report
     *
     * @param int $isFilter
     * @return $this
     */
    protected function _calculateTotalsLive($isFilter = 0)
    {
        $this->setMainTable('sales_order');
        $this->removeAllFieldsFromSelect();

        $connection = $this->getConnection();

        $baseTaxInvoiced = $connection->getIfNullSql('main_table.base_tax_invoiced', 0);
        $baseTaxRefunded = $connection->getIfNullSql('main_table.base_tax_refunded', 0);
        $baseShippingInvoiced = $connection->getIfNullSql('main_table.base_shipping_invoiced', 0);
        $baseShippingRefunded = $connection->getIfNullSql('main_table.base_shipping_refunded', 0);

        $revenueExp = $this->_getSalesAmountExpression();
        $taxExp = sprintf('%s - %s', $baseTaxInvoiced, $baseTaxRefunded);
        $shippingExp = sprintf('%s - %s', $baseShippingInvoiced, $baseShippingRefunded);

        if ($isFilter == 0) {
            $rateExp = $connection->getIfNullSql('main_table.base_to_global_rate', 0);
            $this->getSelect()->columns(
                [
                    'revenue' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $revenueExp, $rateExp)),
                    'tax' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $taxExp, $rateExp)),
                    'shipping' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $shippingExp, $rateExp)),
                ]
            );
        } else {
            if ($this->helper->isEnabled()) {
                $rateExp = $connection->getIfNullSql('main_table.base_to_order_rate', 0);
                $this->getSelect()->columns(
                    [
                        'revenue' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $revenueExp, $rateExp)),
                        'tax' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $taxExp, $rateExp)),
                        'shipping' => $this->getBsExpression(sprintf('SUM((%s) * %s)', $shippingExp, $rateExp)),
                    ]
                );
            } else {
                $this->getSelect()->columns(
                    [
                        'revenue' => $this->getBsExpression(sprintf('SUM(%s)', $revenueExp)),
                        'tax' => $this->getBsExpression(sprintf('SUM(%s)', $taxExp)),
                        'shipping' => $this->getBsExpression(sprintf('SUM(%s)', $shippingExp)),
                    ]
                );
            }
        }

        $this->getSelect()->columns(
            ['quantity' => 'COUNT(main_table.entity_id)']
        )->where(
            'main_table.state NOT IN (?)',
            [\Magento\Sales\Model\Order::STATE_PENDING_PAYMENT, \Magento\Sales\Model\Order::STATE_NEW]
        );

        return $this;
    }

    /**
     * Calculate lifitime sales
     *
     * @param int $isFilter
     * @return $this
     */
    public function calculateSales($isFilter = 0)
    {
        $statuses = $this->_orderConfig->getStateStatuses(\Magento\Sales\Model\Order::STATE_CANCELED);

        if (empty($statuses)) {
            $statuses = [0];
        }
        $connection = $this->getConnection();

        if ($this->_scopeConfig->getValue(
            'sales/dashboard/use_aggregated_data',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )
        ) {
            $this->setMainTable('sales_order_aggregated_created');
            $this->removeAllFieldsFromSelect();
            $averageExpr = $connection->getCheckSql(
                'SUM(main_table.orders_count) > 0',
                'SUM(main_table.total_revenue_amount)/SUM(main_table.orders_count)',
                0
            );
            $this->getSelect()->columns(
                ['lifetime' => 'SUM(main_table.total_revenue_amount)', 'average' => $averageExpr]
            );

            if (!$isFilter) {
                $this->addFieldToFilter(
                    'store_id',
                    ['eq' => $this->_storeManager->getStore(\Magento\Store\Model\Store::ADMIN_CODE)->getId()]
                );
            }
            $this->getSelect()->where('main_table.order_status NOT IN(?)', $statuses);
        } else {
            $this->setMainTable('sales_order');
            $this->removeAllFieldsFromSelect();

            $expr = $this->_getSalesAmountExpression();

            if ($isFilter == 0) {
                $expr = '(' . $expr . ') * main_table.base_to_global_rate';
            } elseif ($this->helper->isEnabled()) {
                $expr = '(' . $expr . ') * main_table.base_to_order_rate';
            }

            $this->getSelect()->columns(
                ['lifetime' => "SUM({$expr})", 'average' => "AVG({$expr})"]
            )->where(
                'main_table.status NOT IN(?)',
                $statuses
            )->where(
                'main_table.state NOT IN(?)',
                [\Magento\Sales\Model\Order::STATE_NEW, \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT]
            );
        }
        return $this;
    }

    /**
     * Set store filter collection
     *
     * @param int[] $storeIds
     * @return $this
     */
    public function setStoreIds($storeIds)
    {
        $connection = $this->getConnection();
        $baseSubtotalInvoiced = $connection->getIfNullSql('main_table.base_subtotal_invoiced', 0);
        $baseDiscountRefunded = $connection->getIfNullSql('main_table.base_discount_refunded', 0);
        $baseSubtotalRefunded = $connection->getIfNullSql('main_table.base_subtotal_refunded', 0);
        $baseDiscountInvoiced = $connection->getIfNullSql('main_table.base_discount_invoiced', 0);
        $baseTotalInvocedCost = $connection->getIfNullSql('main_table.base_total_invoiced_cost', 0);
        if ($storeIds) {
            if ($this->helper->isEnabled()) {
                $this->getSelect()->columns(
                    [
                        'subtotal' => 'SUM(main_table.base_subtotal * main_table.base_to_order_rate)',
                        'tax' => 'SUM(main_table.base_tax_amount * main_table.base_to_order_rate)',
                        'shipping' => 'SUM(main_table.base_shipping_amount * main_table.base_to_order_rate)',
                        'discount' => 'SUM(main_table.base_discount_amount * main_table.base_to_order_rate)',
                        'total' => 'SUM(main_table.base_grand_total * main_table.base_to_order_rate)',
                        'invoiced' => 'SUM(main_table.base_total_paid * main_table.base_to_order_rate)',
                        'refunded' => 'SUM(main_table.base_total_refunded * main_table.base_to_order_rate)',
                        'profit' => "SUM({$baseSubtotalInvoiced} * main_table.base_to_order_rate) " .
                        "+ SUM({$baseDiscountRefunded} * main_table.base_to_order_rate) " .
                        "- SUM({$baseSubtotalRefunded} * main_table.base_to_order_rate) " .
                        "- SUM({$baseDiscountInvoiced} * main_table.base_to_order_rate) " .
                        "- SUM({$baseTotalInvocedCost} * main_table.base_to_order_rate)",
                    ]
                );
            } else {
                $this->getSelect()->columns(
                    [
                        'subtotal' => 'SUM(main_table.base_subtotal)',
                        'tax' => 'SUM(main_table.base_tax_amount)',
                        'shipping' => 'SUM(main_table.base_shipping_amount)',
                        'discount' => 'SUM(main_table.base_discount_amount)',
                        'total' => 'SUM(main_table.base_grand_total)',
                        'invoiced' => 'SUM(main_table.base_total_paid)',
                        'refunded' => 'SUM(main_table.base_total_refunded)',
                        'profit' => "SUM({$baseSubtotalInvoiced}) " .
                        "+ SUM({$baseDiscountRefunded}) - SUM({$baseSubtotalRefunded}) " .
                        "- SUM({$baseDiscountInvoiced}) - SUM({$baseTotalInvocedCost})",
                    ]
                );
            }
        } else {
            $this->getSelect()->columns(
                [
                    'subtotal' => 'SUM(main_table.base_subtotal * main_table.base_to_global_rate)',
                    'tax' => 'SUM(main_table.base_tax_amount * main_table.base_to_global_rate)',
                    'shipping' => 'SUM(main_table.base_shipping_amount * main_table.base_to_global_rate)',
                    'discount' => 'SUM(main_table.base_discount_amount * main_table.base_to_global_rate)',
                    'total' => 'SUM(main_table.base_grand_total * main_table.base_to_global_rate)',
                    'invoiced' => 'SUM(main_table.base_total_paid * main_table.base_to_global_rate)',
                    'refunded' => 'SUM(main_table.base_total_refunded * main_table.base_to_global_rate)',
                    'profit' => "SUM({$baseSubtotalInvoiced} *  main_table.base_to_global_rate) " .
                    "+ SUM({$baseDiscountRefunded} * main_table.base_to_global_rate) " .
                    "- SUM({$baseSubtotalRefunded} * main_table.base_to_global_rate) " .
                    "- SUM({$baseDiscountInvoiced} * main_table.base_to_global_rate) " .
                    "- SUM({$baseTotalInvocedCost} * main_table.base_to_global_rate)",
                ]
            );
        }

        return $this;
    }

    /**
     * Get SQL expression for totals
     *
     * @param int $storeId
     * @param string $baseSubtotalRefunded
     * @param string $baseSubtotalCanceled
     * @param string $baseDiscountCanceled
     * @return string
     */
    protected function getTotalsExpression(
        $storeId,
        $baseSubtotalRefunded,
        $baseSubtotalCanceled,
        $baseDiscountCanceled
    ) {
        if ($this->helper->isEnabled()) {
            $template = ($storeId != 0)
                ? '((main_table.base_subtotal - %1$s - %2$s - ABS(main_table.base_discount_amount) - %3$s) '
                    . ' * main_table.base_to_order_rate)'
                : '((main_table.base_subtotal - %1$s - %2$s - ABS(main_table.base_discount_amount) - %3$s) '
                    . ' * main_table.base_to_global_rate)';
        } else {
            $template = ($storeId != 0)
            ? '(main_table.base_subtotal - %2$s - %1$s - ABS(main_table.base_discount_amount) - %3$s)'
            : '((main_table.base_subtotal - %1$s - %2$s - ABS(main_table.base_discount_amount) - %3$s) '
                . ' * main_table.base_to_global_rate)';
        }
        return sprintf($template, $baseSubtotalRefunded, $baseSubtotalCanceled, $baseDiscountCanceled);
    }

    private function getBsExpression($expression)
    {
        return $this->sql_expression->create(['expression' => $expression]);
    }
}
