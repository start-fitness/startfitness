<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Override\Model\Cart;

use \Magento\Framework\App\ObjectManager;

/**
 * Quote shipping method data.
 *
 */
class ShippingMethodConverter extends \Magento\Quote\Model\Cart\ShippingMethodConverter
{
    /**
     * Converts a specified rate model to a shipping method data object.
     *
     * @param string $quoteCurrencyCode The quote currency code.
     * @param \Magento\Quote\Model\Quote\Address\Rate $rateModel The rate model.
     * @return \Magento\Quote\Api\Data\ShippingMethodInterface Shipping method data object.
     */
    public function modelToDataObject($rateModel, $quoteCurrencyCode)
    {
        $helper = ObjectManager::getInstance()->create(\Bss\CheckoutWithDisplayCurrency\Helper\Data::class);
        if ($helper->isEnabled()) {
            /** @var \Magento\Directory\Model\Currency $currency */
            $currency = $this->storeManager->getStore()->getBaseCurrency();

            $errorMessage = $rateModel->getErrorMessage();
            return $this->shippingMethodDataFactory->create()
                ->setCarrierCode($rateModel->getCarrier())
                ->setMethodCode($rateModel->getMethod())
                ->setCarrierTitle($rateModel->getCarrierTitle())
                ->setMethodTitle($rateModel->getMethodTitle())
                ->setAmount($currency->convert($rateModel->getPrice(), $quoteCurrencyCode))
                ->setBaseAmount($currency->convert($rateModel->getPrice(), $quoteCurrencyCode))
                ->setAvailable(empty($errorMessage))
                ->setErrorMessage(empty($errorMessage) ? false : $errorMessage)
                ->setPriceExclTax(
                    $currency->convert($this->getShippingPriceWithFlag($rateModel, false), $quoteCurrencyCode)
                )
                ->setPriceInclTax(
                    $currency->convert($this->getShippingPriceWithFlag($rateModel, true), $quoteCurrencyCode)
                );
        } else {
            return parent::modelToDataObject($rateModel, $quoteCurrencyCode);
        }
    }

    /**
     * Get Shipping Price including or excluding tax
     *
     * @param \Magento\Quote\Model\Quote\Address\Rate $rateModel
     * @param bool $flag
     * @return float
     */
    private function getShippingPriceWithFlag($rateModel, $flag)
    {
        return $this->taxHelper->getShippingPrice(
            $rateModel->getPrice(),
            $flag,
            $rateModel->getAddress(),
            $rateModel->getAddress()->getQuote()->getCustomerTaxClassId()
        );
    }
}
