<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Setup;

use Magento\Framework\Setup;

class InstallData implements Setup\InstallDataInterface
{
    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Model\ResourceModel\CurrencyOrder
     */
    protected $bssCurrencyOrder;

    /**
     * InstallData constructor.
     * @param \Bss\CheckoutWithDisplayCurrency\Model\ResourceModel\CurrencyOrder $bssCurrencyOrder
     */
    public function __construct(
        \Bss\CheckoutWithDisplayCurrency\Model\ResourceModel\CurrencyOrder $bssCurrencyOrder
    ) {
        $this->bssCurrencyOrder = $bssCurrencyOrder;
    }

    /**
     * {@inheritdoc}
     */
    public function install(Setup\ModuleDataSetupInterface $setup, Setup\ModuleContextInterface $moduleContext)
    {
        $this->bssCurrencyOrder->import();
    }
}
