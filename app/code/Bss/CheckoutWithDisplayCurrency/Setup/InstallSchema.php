<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Create table 'bss_currency_order'
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('bss_currency_order')
        )->addColumn(
            'id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'Entity Id'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'default' => '0'],
            'Order Id'
        )->addColumn(
            'store_to_base_rate',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['default' => '0.0000'],
            'Store To Base Rate'
        )->addColumn(
            'store_to_order_rate',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['default' => '0.0000'],
            'Store To Order Rate'
        )->addColumn(
            'base_currency_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Base Currency Code'
        )->addColumn(
            'store_currency_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Store Currency Code'
        )->addColumn(
            'order_currency_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Order Currency Code'
        )->addColumn(
            'global_currency_code',
            \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            255,
            [],
            'Global Currency Code'
        )->addColumn(
            'base_to_global_rate',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '24,12',
            [],
            'Base To Global Rate'
        )->addColumn(
            'base_to_order_rate',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '24,12',
            [],
            'Base To Order Rate'
        )->addForeignKey(
            $installer->getFkName('bss_checkout_currency', 'order_id', 'sales_order', 'entity_id'),
            'order_id',
            $installer->getTable('sales_order'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        )->addIndex(
            $installer->getIdxName('bss_checkout_currency', ['id']),
            ['id']
        )->setComment(
            'Bss Checkout Currency'
        );
        
        $setup->getConnection()->changeColumn(
            $setup->getTable('sales_order'),
            'base_to_global_rate',
            'base_to_global_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '24,12',
                'comment' => 'Base To Global Rate'
            ]
        );

        $setup->getConnection()->changeColumn(
            $setup->getTable('sales_order'),
            'base_to_order_rate',
            'base_to_order_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '24,12',
                'comment' => 'Base To Order Rate'
            ]
        );

        $setup->getConnection()->changeColumn(
            $setup->getTable('sales_invoice'),
            'base_to_global_rate',
            'base_to_global_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '24,12',
                'comment' => 'Base To Global Rate'
            ]
        );

        $setup->getConnection()->changeColumn(
            $setup->getTable('sales_creditmemo'),
            'base_to_global_rate',
            'base_to_global_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '24,12',
                'comment' => 'Base To Global Rate'
            ]
        );

        $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}
