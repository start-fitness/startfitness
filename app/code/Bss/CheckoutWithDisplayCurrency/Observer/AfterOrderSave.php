<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\RequestInterface;

class AfterOrderSave implements ObserverInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $config;

    /**
     * @var \Magento\Directory\Model\Currency
     */
    protected $currency;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Model\CurrencyOrder
     */
    protected $currencyorder;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * SalesOrderSaveAfter constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $config
     * @param \Magento\Directory\Model\Currency $currency
     * @param \Bss\CheckoutWithDisplayCurrency\Model\CurrencyOrder $currencyorder
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $config,
        \Magento\Directory\Model\Currency $currency,
        \Bss\CheckoutWithDisplayCurrency\Model\CurrencyOrderFactory $currencyorder,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->storeManager = $storeManager;
        $this->currency = $currency;
        $this->currencyorder = $currencyorder;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->helper->isEnabled()) {
            $order = $observer->getEvent()->getOrder();
            $storeId = $order->getStoreId();
            $store = $this->storeManager->getStore($storeId);

            $globalCurrencyCode = $this->config->getValue(
                \Magento\Directory\Model\Currency::XML_PATH_CURRENCY_BASE,
                'default'
            );
            $baseCurrencyCode = $store->getBaseCurrency()->getCode();
            $currentCurrencyCode = $store->getCurrentCurrency()->getCode();

            $bss_model = $this->currencyorder->create();
            $collection = $bss_model->getCollection()->addFieldToFilter('order_id', $order->getId());
            if ($collection->getSize() < 1) {
                $rateGlobalToStore = $this->currency->load($globalCurrencyCode)->getAnyRate($currentCurrencyCode);
                $rateBaseToStore = $this->currency->load($baseCurrencyCode)->getAnyRate($currentCurrencyCode);
                $rateToGlobal = round(1/$rateGlobalToStore, 12);
                $rateToBase = round(1/$rateBaseToStore, 12);
           
                $this->saveOrderBaseToGlobalRate($order, $rateToGlobal, $rateToBase);
                $data = [
                            'order_id'             => $order->getId(),
                            'global_currency_code' => $globalCurrencyCode,
                            'base_currency_code'   => $baseCurrencyCode,
                            'store_currency_code'  => $currentCurrencyCode,
                            'order_currency_code'  => $currentCurrencyCode,
                            'base_to_global_rate'  => $rateToGlobal,
                            'base_to_order_rate'   => $rateToBase
                        ];

                try {
                    $bss_model->setData($data)->save();
                } catch (\Exception $e) {
                    $this->logger->critical($e->getMessage());
                }
            }
        }
    }

    /**
     * @param $order
     * @param $rateToGlobal
     */
    private function saveOrderBaseToGlobalRate($order, $rateToGlobal, $rateToBase)
    {
        try {
            $order->setBaseToGlobalRate($rateToGlobal);
            $order->setBaseToOrderRate($rateToBase);
            $order->save();
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
