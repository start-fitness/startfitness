<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin;

class Currency
{
    /**
     * @var \Magento\Framework\App\Request\Http
     */
    protected $request;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * Currency constructor.
     * @param \Magento\Framework\App\Request\Http $request
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\App\Request\Http $request,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->request = $request;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Directory\Model\Currency $subject
     * @param callable $proceed
     * @param float|null $price
     * @param null $toCurrency
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundConvert(
        \Magento\Directory\Model\Currency $subject,
        callable $proceed,
        $price,
        $toCurrency = null
    ) {
        if ($this->helper->isEnabled()) {
            $moduleName = $this->request->getModuleName();
            $controller = $this->request->getControllerName();
            $route      = $this->request->getRouteName();
            $action     = $this->request->getActionName();
            $section    = $this->request->getParam('sections')? $this->request->getParam('sections') : '';
            $full_path = $moduleName.'_'.$controller.'_'.$route.'_'.$action;
            if (strpos($section, 'cart') !== false && $full_path == 'customer_section_customer_load') {
                return $price;
            } else {
                return $proceed($price, $toCurrency);
            }
        } else {
            return $proceed($price, $toCurrency);
        }
    }
}
