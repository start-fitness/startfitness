<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin\Block\Item\Price;

class Renderer
{
    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * Renderer constructor.
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Tax\Block\Item\Price\Renderer $subject
     * @param float|null $result
     * @return float|null
     */
    public function afterGetItemDisplayPriceExclTax(\Magento\Tax\Block\Item\Price\Renderer $subject, $result)
    {
        $item = $subject->getItem();
        if ($this->helper->isEnabled()) {
            return $item->getPrice();
        }
        return $result;
    }
}
