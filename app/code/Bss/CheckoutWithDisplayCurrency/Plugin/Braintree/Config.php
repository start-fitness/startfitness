<?php
namespace Bss\CheckoutWithDisplayCurrency\Plugin\Braintree;

class Config
{

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $StoreManagerInterface
    ) {
        $this->StoreManagerInterface = $StoreManagerInterface;
    }

    public function afterGetMerchantAccountId($subject, $result)
    {	$merchantAccountId = $result;
	
        $merchantaccounts = [
			"GBP" => "startfitnessGBP",
			"EUR" => "startfitnessEUR",
			"USD" => "startfitnessUSD"
		];
	
		$currency = $this->StoreManagerInterface->getStore()->getCurrentCurrencyCode();
		
		if(isset($merchantaccounts[$currency])){
			$merchantAccountId = $merchantaccounts[$currency];
		}
        return $merchantAccountId;
    }
}
