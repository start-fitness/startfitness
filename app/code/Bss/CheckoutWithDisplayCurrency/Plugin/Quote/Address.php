<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin\Quote;

class Address
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $pricecurrency;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * Address constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->pricecurrency = $pricecurrency;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Address $subject
     * @param callable $proceed
     * @return bool
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundValidateMinimumAmount(\Magento\Quote\Model\Quote\Address $subject, callable $proceed)
    {
        $storeId = $subject->getQuote()->getStoreId();
        $validateEnabled = $this->scopeConfig->isSetFlag(
            'sales/minimum_order/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        if (!$validateEnabled) {
            return true;
        }

        if (!$subject->getQuote()->getIsVirtual() xor
            $subject->getAddressType() == \Magento\Quote\Model\Quote\Address::TYPE_SHIPPING
        ) {
            return true;
        }

        $amount = $this->scopeConfig->getValue(
            'sales/minimum_order/amount',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $amount = $this->convertMinAmount($amount);

        $taxInclude = $this->scopeConfig->getValue(
            'sales/minimum_order/tax_including',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $taxes = $taxInclude ? $subject->getBaseTaxAmount() : 0;

        return ($subject->getBaseSubtotalWithDiscount() + $taxes >= $amount);
    }

    /**
     * @param $minAmount
     * @return mixed
     */
    private function convertMinAmount($amount)
    {
        if ($this->helper->isEnabled()) {
            $store = $this->storeManager->getStore();
            $quoteCurrencyCode = $store->getCurrentCurrency()->getCode();
            $amount = $this->pricecurrency->convert($amount, $store, $quoteCurrencyCode);
        }
        return $amount;
    }
}
