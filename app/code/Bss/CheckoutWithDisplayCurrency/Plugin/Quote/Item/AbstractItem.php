<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin\Quote\Item;

class AbstractItem
{
    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * AbstractItem constructor.
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $subject
     */
    public function beforeGetBaseCalculationPrice(\Magento\Quote\Model\Quote\Item\AbstractItem $subject)
    {
        if (!$subject->hasBaseCalculationPrice()) {
            if ($subject->hasCustomPrice()) {
                $price = (double)$subject->getCustomPrice();
                if ($price) {
                    $rate = $this->priceCurrency->convert($price, $subject->getStore()) / $price;
                    $price = $price / $rate;
                }
            } else {
                if ($this->helper->isEnabled()) {
                    $price = $subject->getConvertedPrice();
                } else {
                    $price = $subject->getPrice();
                }
            }
            $subject->setBaseCalculationPrice($price);
        }
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $subject
     */
    public function beforeGetBaseCalculationPriceOriginal(\Magento\Quote\Model\Quote\Item\AbstractItem $subject)
    {
        if (!$subject->hasBaseCalculationPrice()) {
            if ($subject->hasCustomPrice()) {
                $price = (double)$subject->getCustomPrice();
                if ($price) {
                    $rate = $this->priceCurrency->convert($price, $subject->getStore()) / $price;
                    $price = $price / $rate;
                }
            } else {
                if ($this->helper->isEnabled()) {
                    $price = $subject->getConvertedPrice();
                } else {
                    $price = $subject->getPrice();
                }
            }
            $subject->setBaseCalculationPrice($price);
        }
    }
}
