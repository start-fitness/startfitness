<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin\Shipping;

class Carrier
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $pricecurrency;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * Carrier constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->storeManager = $storeManager;
        $this->pricecurrency = $pricecurrency;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Shipping\Model\Carrier\AbstractCarrier $subject
     * @param callable $proceed
     * @param $field
     * @return mixed
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundGetConfigData(
        \Magento\Shipping\Model\Carrier\AbstractCarrier $subject,
        callable $proceed,
        $field
    ) {
        $value = $proceed($field);
        if ($field == 'free_shipping_subtotal' && $this->helper->isEnabled()) {
            $store = $this->storeManager->getStore();
            $quoteCurrencyCode = $store->getCurrentCurrency()->getCode();
            $value = $this->pricecurrency->convert($value, $store, $quoteCurrencyCode);
        }
        return $value;
    }
}
