<?php
/**
 * BSS Commerce Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://bsscommerce.com/Bss-Commerce-License.txt
 *
 * @category   BSS
 * @package    Bss_CheckoutWithDisplayCurrency
 * @author     Extension Team
 * @copyright  Copyright (c) 2019-2020 BSS Commerce Co. ( http://bsscommerce.com )
 * @license    http://bsscommerce.com/Bss-Commerce-License.txt
 */
namespace Bss\CheckoutWithDisplayCurrency\Plugin;

class Quote
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $pricecurrency;

    /**
     * @var \Bss\CheckoutWithDisplayCurrency\Helper\Data
     */
    protected $helper;

    /**
     * Quote constructor.
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency
     * @param \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
     */
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Pricing\PriceCurrencyInterface $pricecurrency,
        \Bss\CheckoutWithDisplayCurrency\Helper\Data $helper
    ) {
        $this->storeManager = $storeManager;
        $this->scopeConfig = $scopeConfig;
        $this->pricecurrency = $pricecurrency;
        $this->helper = $helper;
    }

    /**
     * @param \Magento\Quote\Model\Quote $subject
     * @param callable $proceed
     * @param bool $multishipping
     * @return bool
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.UnusedLocalVariable)
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundValidateMinimumAmount(
        \Magento\Quote\Model\Quote $subject,
        callable $proceed,
        $multishipping = false
    ) {
        $storeId = $subject->getStoreId();
        $minOrderActive = $this->scopeConfig->isSetFlag(
            'sales/minimum_order/active',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        
        $minOrderMulti = $this->scopeConfig->isSetFlag(
            'sales/minimum_order/multi_address',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $minAmount = $this->scopeConfig->getValue(
            'sales/minimum_order/amount',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );
        $minAmount = $this->convertMinAmount($minAmount);
        
        $taxInclude = $this->scopeConfig->getValue(
            'sales/minimum_order/tax_including',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $storeId
        );

        $addresses = $subject->getAllAddresses();

        if (!$multishipping) {
            foreach ($addresses as $address) {
                /* @var $address Address */
                if (!$address->validateMinimumAmount()) {
                    return false;
                }
            }
            return true;
        }

        if (!$minOrderMulti) {
            foreach ($addresses as $address) {
                $taxes = ($taxInclude) ? $address->getBaseTaxAmount() : 0;
                foreach ($address->getQuote()->getItemsCollection() as $item) {
                    /** @var \Magento\Quote\Model\Quote\Item $item */
                    $amount = $item->getBaseRowTotal() - $item->getBaseDiscountAmount() + $taxes;
                    if ($amount < $minAmount) {
                        return false;
                    }
                }
            }
        } else {
            $baseTotal = 0;
            foreach ($addresses as $address) {
                $taxes = ($taxInclude) ? $address->getBaseTaxAmount() : 0;
                $baseTotal += $address->getBaseSubtotalWithDiscount() + $taxes;
            }
            if ($baseTotal < $minAmount) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param $minAmount
     * @return mixed
     */
    private function convertMinAmount($minAmount)
    {
        if ($this->helper->isEnabled()) {
            $store = $this->storeManager->getStore();
            $quoteCurrencyCode = $store->getCurrentCurrency()->getCode();
            $minAmount = $this->pricecurrency->convert($minAmount, $store, $quoteCurrencyCode);
        }
        return $minAmount;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param null $result
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterBeforeSave(\Magento\Quote\Model\Quote $quote, $result)
    {
        if ($this->helper->isEnabled()) {
            $currency = $quote->getQuoteCurrencyCode();
            $quote->setGlobalCurrencyCode($currency);
            $quote->setBaseCurrencyCode($currency);
            $quote->setStoreCurrencyCode($currency);

            $quote->setBaseToGlobalRate(1);
            $quote->setBaseToQuoteRate(1);
        }
    }
}
