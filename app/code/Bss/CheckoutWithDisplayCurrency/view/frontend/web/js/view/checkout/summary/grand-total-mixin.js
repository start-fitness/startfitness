define([
    'jquery',
    'Magento_Checkout/js/view/summary/abstract-total',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'Magento_Checkout/js/model/totals'
], function ($, Component, quote, priceUtils, totals) {
    return function (target) {
        return target.extend({
            /**
             * @return {*}
             */
            isBaseGrandTotalDisplayNeeded: function () {
                var total = this.totals();
                var bssShowMessage = window.checkoutConfig.bssShowMessage;
                if (!total) {
                    return false;
                }

                if (bssShowMessage) {
                    $('tr.totals.charge').show();
                    return true;
                }

                return total['base_currency_code'] != total['quote_currency_code'];
            }
        });
    }
});