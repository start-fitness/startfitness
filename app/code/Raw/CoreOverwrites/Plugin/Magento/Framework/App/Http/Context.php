<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\CoreOverwrites\Plugin\Magento\Framework\App\Http;

class Context
{

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
		\Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->customerSession = $customerSession;
		$this->_storeManager = $storeManager;
    }
    /**
     * \Magento\Framework\App\Http\Context::getVaryString is used by Magento to retrieve unique identifier for selected context,
     * so this is a best place to declare custom context variables
     */
    function beforeGetVaryString(\Magento\Framework\App\Http\Context $subject)
    {
        $currency = (isset($_GET['currency'])) ? $_GET['currency'] : '';
		
        $subject->setValue('qs_currency', $currency, '');
    }
}