<?php

namespace Raw\CoreOverwrites\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Currency implements ObserverInterface
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->storeManager = $storeManager;
    }

    /**
     * @inheritDoc
     */
    public function execute(Observer $observer)
    {
        // this can be coming from geoip PHP function
        // php.net/manual/en/ref.geoip.php
        $currency = (isset($_GET['currency'])) ? $_GET['currency'] : 'GBP';
		if(in_array($currency, ['GBP','USD','EUR'])){	

			if (isset($_GET['currency']) && $this->getStoreCurrency() !== $currency) {
				$this->storeManager->getStore()->setCurrentCurrencyCode($currency);
			}
		}
    }

    private function getStoreCurrency()
    {
        return $this->storeManager->getStore()->getCurrentCurrencyCode();
    }
}