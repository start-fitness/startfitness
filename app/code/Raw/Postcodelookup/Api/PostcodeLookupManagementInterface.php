<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Postcodelookup\Api;

interface PostcodeLookupManagementInterface
{

    /**
     * GET for PostcodeLookup api
     * @param string $param
     * @return string
     */
    public function getPostcodeLookup($param);
}

