<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Postcodelookup\Model;

class PostcodeLookupManagement implements \Raw\Postcodelookup\Api\PostcodeLookupManagementInterface
{
	
	/**
     * @param string $id
	 * @return string
     */
    
    public function getPostcodeLookup($param)
    {
		
		$postcode = str_replace(" ", "", strtolower($param));
            
		$feed_url = "https://api.getAddress.io/v2/uk/" . $postcode . "?format=true&api-key=RZmkJ0igZUKTSBLC0D-_mw15810";
		$ch       = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $feed_url);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$curl_response = curl_exec($ch);
		$error_msg = false;
		if (curl_error($ch)) {
			$error_msg = curl_error($ch);
		}
		curl_close($ch);
		
		if ($error_msg) {
			return null;
		} else {
			$result = json_decode($curl_response);
			if ($addresses = $result->Addresses) {
				$response['status']    = 'ok';
				$response['addresses'] = $addresses;
				usort($result->Addresses, array(
					$this,
					"cmp"
				));
			}
			return json_encode($result);
		}
            
    }
		
    private function cmp($a, $b) {
        $addressA = explode(" ", $a[0]);
        $addressB = explode(" ", $b[0]);
        return strnatcmp($addressA[0], $addressB[0]);
    }
    
}
class AddressResponse
{
    public function getLatitude() { 
         return $this->Latitude ;
    }
    public function setLatitude($Latitude) { 
         $this->Latitude = $Latitude ;
    }
    public $Latitude; //double

    public function getLongitude() { 
         return $this->Longitude ;
    }
    public function setLongitude($Longitude) { 
         $this->Longitude = $Longitude ;
    }
    public $Longitude; //double

    public function getAddresses() { 
         return $this->Addresses ;
    }
    public function setAddresses($Addresses) { 
         $this->Addresses = $Addresses ;
    }
    public $Addresses; //array(array(String))
}


