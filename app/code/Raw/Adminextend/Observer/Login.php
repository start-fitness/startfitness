<?php
namespace Raw\Adminextend\Observer;
class Login implements \Magento\Framework\Event\ObserverInterface
{
    const DEFAULT_SESSION_NAME_OF_FRONTEND = 'PHPSESSID';

    /**
     * (non-PHPdoc)
     * @see \Magento\Framework\Event\ObserverInterface::execute()
     */
    public function execute(\Magento\Framework\Event\Observer $observer) {
        if (! isset($_COOKIE[self::DEFAULT_SESSION_NAME_OF_FRONTEND])) return;
        $backSessionId = session_id();
        $frontendSessionId = $_COOKIE[self::DEFAULT_SESSION_NAME_OF_FRONTEND];
        session_write_close();
        session_id($frontendSessionId);
        session_start();
        $_SESSION['admin'] = [$backSessionId];
        session_write_close();
        session_id($backSessionId);
        session_start();
        return;
    }
}