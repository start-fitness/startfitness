<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\StoreGiftcard\Controller\Process;

class Action extends \Magento\Framework\App\Action\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Checkout\Model\Session $checkoutSession ,
		\Amasty\StoreCredit\Model\StoreCredit\ManageCustomerStoreCredit $amastyCredit,
		\Amasty\StoreCredit\Model\StoreCredit\ApplyStoreCreditToQuote $amastyCreditAdd,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
		$this->request = $request;
		$this->checkoutSession = $checkoutSession;
		$this->amastyCredit = $amastyCredit;
		$this->amastyCreditAdd = $amastyCreditAdd;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
			
			$number = $this->request->getParam('number');
			$full = ($this->request->getParam('full'))? true : false;
			$pin = str_pad($this->request->getParam('pin'), 4, "0", STR_PAD_LEFT );
			$deduct = $this->request->getParam('deduct');
			$balance = ($this->request->getParam('usebal') && $this->request->getParam('usebal') == "true")? true: false;
			$balanceamt = (is_numeric($this->request->getParam("usebalamt")) && $this->request->getParam("usebalamt") > 0 )? $this->request->getParam("usebalamt") : 0;
			if($deduct == "1"){
			$quote = $this->checkoutSession->getQuote();
			$quoteData= $quote->getData();
			//$grandTotal = $quote->getGrandTotal() + $creditusedorder;
			$creditusedorder = 0;
			if(false && $quote->getData("amstorecredit_use") == 1 && $quote->getData("amstorecredit_amount") != null && $quote->getData("amstorecredit_amount") > 0)
				$creditusedorder = $quote->getData("amstorecredit_amount") *-1;
			if($full){
				$grandTotal = 0;
			}		
			else{
				//var_dump(5);
				$grandTotal = $quote->getGrandTotal();
			}
			$jsondata = $this->getQuery("http://start.fashionmaster.net:8051/FMWebAPI/service.asmx/GiftCardCheck?CardNumber=".$number."&Pin=".$pin);
			$xml = @simplexml_load_string($jsondata);
			$jsondata = ($xml);
			if($full){
				$grandTotal = $jsondata->Balance;
			}
			//$jsondata =  json_decode('{"GiftCardCheckResult":{"CardNumber":"","Status":"OK","Balance":"100.00"}}');
			if($grandTotal <= 0){
				//$this->getResponse()->setHeader('Content-type', 'application/json');
				return $this->jsonResponse(array(
				"GiftCardRedeemResult"=>"You cannot redeem your giftcard as your current store credit balance already covers the order total."));
			}
			else if($jsondata && $jsondata->Balance > 0){
				$deducttotal = 0;
				
				if((float)$grandTotal >= (float)$jsondata->Balance){
					$deducttotal = (float)$jsondata->Balance;
				}
				else{
					$deducttotal = (float)$grandTotal;
				}
				
				
				/*$jsondata = (($client->__soapCall("GiftCardRedeem", array(array(
				"CardNumber"=>$number, 
				"Pin"=>$pin,
				"WebOrderNumber"=>$quoteData['increment_id'],
				"RedeemAmount"=>$deducttotal
				)))));*/
				$jsondata = $this->getQuery("http://start.fashionmaster.net:8051/FMWebAPI/service.asmx/GiftCardRedeem?CardNumber="
				.$number."&Pin=".$pin."&WebOrderNumber=".$quote->getEntityId()."&RedeemAmount=".$deducttotal);
				
				$xml = @simplexml_load_string($jsondata);
				
				$jsondata = ($xml);
				$jsondata = (json_decode(json_encode($jsondata),true));
				
				
				//var_dump($jsondata);
				//exit;
				//$jsondata->GiftCardRedeemResult = "OK"; // TEST
				if(isset($jsondata[0]) && strpos($jsondata[0], "OK") !== false){
					$creditusedorder = 0;
					if($quote->getData("amstorecredit_use") == 1 && $quote->getData("amstorecredit_amount") != null && $quote->getData("amstorecredit_amount") > 0)
						$creditusedorder = $quote->getData("amstorecredit_amount");
					try{
						$this->amastyCredit->addOrSubtractStoreCredit($quote->getCustomerId(), $deducttotal, 1, [], $quote->getStoreId(), "Store giftcard conversion: ".$number );
					}
					catch(\Exception $e){
						return $this->jsonResponse(((array("GiftCardRedeemResult"=>"There was an error adding the credit to your website account. Please get in touch with customer support, quoting your giftcard number."))));
					}
					try{
						$this->amastyCreditAdd->apply($quote->getEntityId(), $deducttotal + $creditusedorder);
					}
					catch(\Exception $e){
						
					}
					/*$balance = Mage::getModel('enterprise_customerbalance/balance')
						->setCustomer(Mage::getModel('customer/customer')->load($quote->getCustomerId()))
						->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
						->setAmountDelta($deducttotal)
						->setAdditionalInfo("Redeemed store giftcard num. {$number}");
					//var_dump($quoteData['customer_id'],Mage::app()->getStore()->getWebsiteId(), $deducttotal );exit;
					$balance->save();*/
					//$this->getResponse()->setHeader('Content-type', 'application/json');
					//$payments = $this->getLayout()->createBlock('checkout/onepage_payment', "checkout.onepage.payment")->setTemplate('checkout/onepage/payment.phtml');
					//$blockChild1 = $this->getLayout()->createBlock('checkout/onepage_payment_methods', 'methods')
					//->setTemplate('checkout/onepage/payment/methods.phtml');
					//$blockChild2 = $this->getLayout()->createBlock('enterprise_customerbalance/checkout_onepage_payment_additional', 'customerbalance')
					//->setTemplate('customerbalance/checkout/onepage/payment/additional.phtml');
					//$payments->setChild('methods', $blockChild1);
					//$payments->setChild('customerbalance', $blockChild2);
					return $this->jsonResponse((array("GiftCardRedeemResult"=>"OK", "payments"=> true)));
				}
				else{
					//this->getResponse()->setHeader('Content-type', 'application/json');
					return $this->jsonResponse(((array("GiftCardRedeemResult"=>implode(",",$jsondata)))));
				}
				//var_dump($jsondata);
				
				//exit;
			}
			else{
				//$this->getResponse()->setHeader('Content-type', 'application/json');
				return $this->jsonResponse(((array("GiftCardRedeemResult"=>"Gift Card Not Found"))));
			}

			}
			else{
			//echo "test";
			//exit;
			  $quote = $this->checkoutSession->getQuote();
			  
			  $creditusedorder = 0;
			  if($quote->getData("amstorecredit_use") == 1 && $quote->getData("amstorecredit_amount") != null && $quote->getData("amstorecredit_amount") > 0)
				$creditusedorder = $quote->getData("amstorecredit_amount") *-1;
			  //var_dump($creditusedorder);exit;
			  //$client = new SoapClient("http://start.fashionmaster.net:8051/FMWebAPI/service.asmx?WSDL");
			  //$jsondata = json_encode(($client->__soapCall("GiftCardCheck", array(array("CardNumber"=>$number, "Pin"=>$pin)))));
			  
			  $jsondata = $this->getQuery("http://start.fashionmaster.net:8051/FMWebAPI/service.asmx/GiftCardCheck?CardNumber=".$number."&Pin=".$pin);
			  $xml = @simplexml_load_string($jsondata);
			  if(isset($_GET['test'])) {var_dump($jsondata);exit;}
			  if(!$jsondata){
				  //$this->getResponse()->setHeader('Content-type', 'application/json');
				  
				  $demo = '{"CardNumber":"","Status":"Temporary issue with the Gift Card system, please try again later.","Balance":"0.00","Grandtotal":"'.'0'.'"}';
				  $demo = json_decode($demo);
				  $demo->details = $jsondata;
				  //$demo = json_encode($demo);
				  return $this->jsonResponse($demo);
			  }
			  if($full){
				$xml->Grandtotal = $xml->Balance;
			  }
			  else{
				$xml->Grandtotal = round($quote->getGrandTotal(), 2) + $creditusedorder;
			  }
			  if(true){
				if($xml->Balance <= 0 && $xml->Status== "OK"){
				  $this->getResponse()->setHeader('Content-type', 'application/json');
				  
				  $demo = '{"CardNumber":"","Status":"Your balance is currently 0.00","Balance":"0.00","Grandtotal":"'.$quote->getGrandTotal().'"}';
				  $this->getResponse()->setBody($demo);
				  return;
				}
				
					//$xml->Grandtotal = $quote->getGrandTotal() - $creditusedorder;
					if($full){
						$xml->Grandtotal = $xml->Balance;
					}
					else if(!$balance){
						//var_dump($creditusedorder);
						$xml->Grandtotal = $quote->getGrandTotal() ;
					}
					else{
						//var_dump(5);
						$xml->Grandtotal = $quote->getGrandTotal();
					}
					//var_dump($quote->getGrandTotal());exit;
			}
			  //var_dump($quote->getData());
			  $jsondata = ($xml);
			  
			  return $this->jsonResponse($jsondata);
			  //$demo = '{"GiftCardCheckResult":{"CardNumber":"","Status":"OK","Balance":"100.00","Grandtotal":"'.$quote->getGrandTotal().'"}}';
			  //$this->getResponse()->setBody($demo);
			}
			
            //return $this->jsonResponse('your response');
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
	
	function getQuery($url){
		
		        // create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 5); 
		//curl_setopt($ch, CURLOPT_INTERFACE, "185.147.133.68");
        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);
		
		return $output;
		
	}
}

