<?php declare(strict_types=1);


namespace Raw\QuickPriceUpdater\Controller\Adminhtml\Quickpriceupdater;


class Search extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;
    protected $jsonHelper;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Psr\Log\LoggerInterface $logger,
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Catalog\Model\ProductFactory $productRepository,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->jsonHelper = $jsonHelper;
        $this->logger = $logger;
		$this->resource = $resource;
		$this->request = $request;
		$this->productRepository = $productRepository;
		$this->_stockItemRepository = $stockItemRepository;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
			$connection = $this->resource->getConnection();
			$post = $this->request->getPost();
			if(isset($post['term'])){
				$model = $post['term'];
				$sql = "SELECT prod.entity_id, sku, model.value as label, model.value  FROM  catalog_product_entity prod 
JOIN `catalog_product_entity_varchar` model on prod.entity_id = model.entity_id AND model.`attribute_id` = 134 AND model.store_id = 0
JOIN catalog_product_entity_int vis on vis.attribute_id = 102 AND vis.store_id = 0 AND vis.entity_id = prod.entity_id
WHERE model.`value` LIKE '%".$model."%' AND type_id IN ('configurable', 'simple') AND vis.value <> 1 LIMIT 40";
				$result = $connection->fetchAll($sql);
				return $this->jsonResponse($result);
			}
			else if(isset($post['search'])){
				$product = $this->productRepository->create();
				$product->setStoreId(0)->load($post['search']);
				if($product->getTypeId() == "configurable"){
					$children = $product
					->getTypeInstance()
					->getUsedProductCollection($product);
					$stock = 0;
					foreach($children as $child){
						$_productStock = $this->_stockItemRepository->getStockItem($child->getEntityId());
						$stock += $_productStock->getQty();
					}
					$data = $product->getData();
					$data['qty'] = $stock;

				}
				else{
					$_productStock = $this->_stockItemRepository->getStockItem($product->getEntityId());
					$data = $product->getData();
					$data['qty'] = $_productStock->getQty();
				}
				return $this->jsonResponse([$data]);
			}

        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            return $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e);
            return $this->jsonResponse($e->getMessage());
        }
    }

    /**
     * Create json response
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function jsonResponse($response = '')
    {
        return $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($response)
        );
    }
}
