<?php declare(strict_types=1);


namespace Raw\QuickPriceUpdater\Controller\Adminhtml\Quickpriceupdater;


class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Catalog\Model\ProductFactory $productRepository,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Framework\Message\ManagerInterface $messageManager

    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->productRepository = $productRepository;
		$this->_messageManager = $messageManager;
		$this->request = $request;
        parent::__construct($context);
    }

    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
		$post = $this->request->getPost();
		foreach($post['newprice'] as $sku => $price){
			$product = $this->productRepository->create();
			$product = $product->loadByAttribute('sku', $sku);
			if($product){
				if($product->getTypeId() == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE){
					$_children = $product->getTypeInstance()->getUsedProducts($product);
					foreach($_children as $child){
						//$child->setPrice($price);
						//$child->getResource()->saveAttribute($child, 'price');
						$child2 = $this->productRepository->create();
						$child = $child2->loadByAttribute('sku', $child->getSku());
						$child->setStoreId(0);
						$child->setPrice($price);
						$child->getResource()->saveAttribute($child, 'price');
					}
				}
				
				//$product->setPrice($price);
				//$product->getResource()->saveAttribute($product, 'price');
				$product->setStoreId(0);
				$product->setPrice($price);
				$product->getResource()->saveAttribute($product, 'price');
				
			}

		}
        $resultRedirect = $this->resultRedirectFactory->create();
		$resultRedirect->setPath('raw/quickpriceupdater/index');
		$this->_messageManager->addSuccess(__("Prices updated successfully!"));
		return $resultRedirect;
    }
}

