<?php declare(strict_types=1);


namespace Raw\QuickPriceUpdater\Block\Adminhtml\Quickpriceupdater;


class Index extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Backend\Model\UrlInterface $urlBuilder,
        array $data = []
    ) {
		$this->urlBuilder = $urlBuilder;
        parent::__construct($context, $data);
    }

	public function getSearchUrl()
	{
		return $this->urlBuilder->getRouteUrl('raw/quickpriceupdater/search',[ 'key'=>$this->urlBuilder->getSecretKey('raw','quickpriceupdater','search')]);
	}
	public function getSaveUrl()
	{
		return $this->urlBuilder->getRouteUrl('raw/quickpriceupdater/save',[ 'key'=>$this->urlBuilder->getSecretKey('raw','quickpriceupdater','save')]);
	}
}

