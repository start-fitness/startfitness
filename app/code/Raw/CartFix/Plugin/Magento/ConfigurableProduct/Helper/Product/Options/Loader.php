<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\CartFix\Plugin\Magento\ConfigurableProduct\Helper\Product\Options;

class Loader
{
    
     public function aroundLoad(\Magento\ConfigurableProduct\Helper\Product\Options\Loader $subject, \Closure $proceed, \Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $typeInstance = $product->getTypeInstance();
		
        if (get_class($typeInstance) == 'Magento\Catalog\Model\Product\Type\Simple' || get_class($typeInstance) == 'Magento\Bundle\Model\Product\Type')
		{
			return null;
		}
		return $proceed($product);
    }
}
