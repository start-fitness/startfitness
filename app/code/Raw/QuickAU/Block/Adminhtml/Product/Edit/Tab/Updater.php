<?php

namespace Raw\QuickAU\Block\Adminhtml\Product\Edit\Tab;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use \Magento\Catalog\Model\Product\Attribute\Repository;
use \Magento\ConfigurableProduct\Model\ConfigurableAttributeData;

class Updater extends \Magento\Framework\View\Element\Template
{
    protected $_template = 'catalog/product/edit/updater.phtml';

    protected $_coreRegistry = null;
	protected $productAttributeRepository;
	protected $ConfigurableAttributeData;
	protected $stockItemRepository;
    public function __construct(
        Context $context,
        Registry $registry,
		Repository $productAttributeRepository,
		ConfigurableAttributeData $ConfigurableAttributeData,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
        array $data = []
    )
    {
        $this->_coreRegistry = $registry;
		$this->productAttributeRepository = $productAttributeRepository;
		$this->ConfigurableAttributeData = $ConfigurableAttributeData;
		$this->stockItemRepository = $stockItemRepository;
        parent::__construct($context, $data);
    }

    public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }
	public function attributeToLoad(){
		return ['price','cost_price','msrp_nomap','weight','name','tax_class_id','bin_location','bin_overstock_location', 'source'];
	}
	public function loadAttribute($code){
		
		return $this->productAttributeRepository->get($code);
		
	}
	public function getConfigurableAttributes(){
		return $productAttributeOptions = $this->getProduct()->getTypeInstance(true)->getConfigurableAttributesAsArray($this->getProduct());
		//var_dump($productAttributeOptions);
	}
	public function getStockItem($productId)
    {
        return $this->stockItemRepository->getStockItem($productId);
    }
	
	public function getNumberNotDispatcheds($product_ids){
		if(empty($product_ids)) return array();
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance(); // Instance of object manager
		$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
		$connection = $resource->getConnection();
		
		
//		$sql = "SELECT SUM(sfoi.qty_ordered) AS PRODSUM 
//				FROM sales_flat_order sfo
//				JOIN sales_flat_order_item sfoi ON sfo.entity_id = sfoi.order_id
//				WHERE sfo.created_at > '2017-01-01 00:00:00' and sfo.status IN('preprocessing','processing','pick_note_printed','pending','order_query','bike_order','awaiting_giftcard','held','emailed','wholesale_order','pre_order','fraud') 
//				AND sfoi.product_id = " . $product_id .
//			  " GROUP BY sku";
		
		$sql = "SELECT product_id, SUM(sfoi.qty_ordered) AS PRODSUM
				FROM sales_order_item sfoi 
				WHERE sfoi.order_id IN 
				(SELECT entity_id
				    FROM  sales_order sfo 
				    WHERE sfo.created_at > '2017-01-01 00:00:00' AND sfo.state NOT IN ('complete') AND sfo.status IN('preprocessing','processing','pick_note_printed','pending_auth','order_query','bike_order','awaiting_giftcard','held','emailed','wholesale_order','pre_order','fraud','bike_awaiting_shipping','cyclescheme_pending'))
				AND  sfoi.product_id IN (" . implode(",", $product_ids).") GROUP BY product_id";
		 //echo $sql;exit;
//		if($store == 0){
//			$sql .= " AND sfo.store_id IS NOT NULL";
//		}
//		else{
//			$sql .= " AND sfo.store_id = " . $store;
//		}
		
		//echo $sql;			  							  											  
		$result = $connection->fetchAll($sql);
		
		$arrayres = array();
		foreach($product_ids as $pid){
			$arrayres[$pid] = 0;
		}
		foreach($result as $row){
			$arrayres[$row['product_id']] = $row['PRODSUM'];
		}
		return ($arrayres);
	}
	
	
}