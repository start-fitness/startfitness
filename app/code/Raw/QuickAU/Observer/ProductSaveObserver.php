<?php
namespace Raw\QuickAU\Observer;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Catalog\Model\Product\Attribute\Repository;
use \Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use \Magento\Catalog\Model\Product\Action;
class ProductSaveObserver implements ObserverInterface
{
	protected $_coreRegistry = null;
	protected $productAttributeRepository;
	protected $ConfigurableAttributeData;
	protected $stockItemRepository;
	protected $request;
	protected $action;
	public function __construct(
        Context $context,
        Registry $registry,
		Repository $productAttributeRepository,
		ConfigurableAttributeData $ConfigurableAttributeData,
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
		\Magento\Framework\App\RequestInterface $request,

		Action $action
    )
    {
        $this->_coreRegistry = $registry;
		$this->productAttributeRepository = $productAttributeRepository;
		$this->ConfigurableAttributeData = $ConfigurableAttributeData;
		$this->stockItemRepository = $stockItemRepository;
		$this->request = $request;
		$this->action = $action;
		$this->_resource = $resource;
        //parent::__construct($context, $data);
    }

  public function execute(\Magento\Framework\Event\Observer $observer)
  {
    // Observer execution code...
	$product = $observer->getEvent()->getProduct();
	$postval = $this->request->getPostValue();
//	print_r($postval); exit;
	if(strtotime($product->getCreatedAt()) >= strtotime("- 5 minutes") && $product->getTypeId() == "configurable"){

		$data = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);

		$this->action->updateAttributes([$product->getEntityId()], array("ebay_name" => $product->getName()), 0);
		$this->action->updateAttributes([$product->getEntityId()], array("amazon_name" => $product->getName()), 0);

		$options = [];
		foreach ($data as $attr) {

			$options[] = $attr['attribute_code'];

		}
		$model = $product->getActualModel();
		$name = $product->getName();
		$children = $product->getTypeInstance()->getUsedProducts($product);
		foreach($children as $child){
			$actualmodel = $model."-";
			$actualname = $name."-";
			foreach($options as $code){
				$attrval = $child->getResource()
                                      ->getAttribute($code)
                                      ->getFrontend()
                                      ->getValue($child);
				$actualmodel .= $attrval."-";
				$actualname .= $attrval."-";
			}
			$actualmodel = rtrim($actualmodel, "-");
			$actualname = rtrim($actualname, "-");

			//var_dump($actualmodel);
			$this->action->updateAttributes([$child->getEntityId()], array("actual_model" => $actualmodel), 0);
			$this->action->updateAttributes([$child->getEntityId()], array("name" => $actualname), 0);
			$this->action->updateAttributes([$child->getEntityId()], array("ebay_name" => $actualname), 0);
			$this->action->updateAttributes([$child->getEntityId()], array("amazon_name" => $actualname), 0);
		}
		//exit;
	}
	if(isset($postval['massupdate']) && $product->getTypeId() == "configurable"){

		$children = $product->getTypeInstance()->getUsedProducts($product);
		$ids = [$product->getEntityId()];
		$idsprod = [];
		$childids = [];
		$productAttributeOptions = $product->getTypeInstance(true)->getConfigurableAttributesAsArray($product);
		foreach($children as $child){
			$ids[] = $child->getEntityId();
			$idsprod[$child->getEntityId()] = $child;
			$childids[] = $child->getEntityId();
		}
		$massupdate = $postval['massupdate'];

		if(isset($massupdate['bin_location1']) && $massupdate['bin_location1'] != "" && isset($massupdate['bin_location2']) && isset($massupdate['bin_location3'])){
			$massupdate['bin_location'] = strtoupper($massupdate['bin_location1']."-".$massupdate['bin_location2']."-".$massupdate['bin_location3']);

		}
		//if($massupdate['bin_location'] == "--") $massupdate['bin_location'] = "";
		$delbin = false;
		$delover = false;

		if(isset($massupdate['bin_location_del']) && $massupdate['bin_location_del'] == "1"){
			$massupdate['bin_location'] = null;
			$delbin = true;
		}

		if(isset($massupdate['bin_overstock_location_del']) && $massupdate['bin_overstock_location_del'] == "1"){
			$massupdate['bin_overstock_location'] = null;
			$delover = true;
		}
		unset($massupdate['bin_location1']);
		unset($massupdate['bin_location2']);
		unset($massupdate['bin_location3']);
		unset($massupdate['bin_location_del']);
		unset($massupdate['bin_overstock_location_del']);
		$massupdatestock = $postval['massupdatestock'];
		$massupdate['updated_at'] = (new \DateTime())->format("Y-m-d H:i:s");
		//var_dump($massupdate);exit;
		foreach($massupdate as $key=>$val){
			if($val === null || ($val)!= ""){

				if($val === null) $val = "";
				if($key == "name"){
					foreach($idsprod as $child){
						$size= "";
						foreach($productAttributeOptions as $attr){
							$size .= $child->getResource()->getAttribute($attr['attribute_code'])->getFrontend()->getValue($child)."/";
						}
						$size = rtrim($size, "/");
						$this->action->updateAttributes([$child->getEntityId()], array($key => $val."-".$size), 0);
					}
				}
				else{

					$this->action->updateAttributes($ids, array($key => $val), 0);
				}
			}
		}
		foreach($massupdatestock as $key=>$val){
			if(!empty($val)){
				$stockItem = $this->stockItemRepository->getStockItem($key);
				$stock = $stockItem->getData('qty') + $val;
				$stockItem->setData('qty', $stock);
				$stockItem->setData('is_in_stock', ($stock > 0 ) ? 1 : 0);
				$stockItem->save();
			}
		}
		//var_dump($massupdate);exit;
	}

  }
}
