<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Newin\Controller\Index;
use Raw\Newin\Block\Index\Index as Blockindex;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends \Magento\Framework\App\Action\Action
{

    /** @var PageFactory */
    protected $pageFactory;

    /** @var  \Magento\Catalog\Model\ResourceModel\Product\Collection */
    protected $productCollection;

    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $collectionFactory
    )
    {
        $this->pageFactory = $pageFactory;
        $this->productCollection = $collectionFactory->create();

        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->pageFactory->create();
		$result->getConfig()->getTitle()->set("New-in Products");
		$date = new \Zend_Date();
        // obtain product collection.
        //$this->productCollection->addIdFilter(5); // do some filtering
        $this->productCollection->addFieldToSelect('*');
		$this->productCollection->addAttributeToFilter(
			'news_from_date',
			[
				'and' => [
					0 => [
						'date' => true,
						'to' => $date->get('YYYY-MM-dd') . ' 23:59:59'
					]
				]
			],
			'left'
		);

		$this->productCollection->addAttributeToFilter(
			'news_to_date',
			[
				'and' => [
					0 => [
						'date' => true,
						'from' => $date->get('YYYY-MM-dd') . ' 00:00:00'
					]
				]
			],
			'left'
		);
        // get the custom list block and add our collection to it
        /** @var CustomList $list */
        $list = $result->getLayout()->getBlock('custom.products.list');
        $list->setProductCollection($this->productCollection);
		echo $this->productCollection->getSelect()."";exit;

        return $result;
    }
}

