define(
    [
        'Magento_Checkout/js/view/payment/default',
		'jquery',
		'mage/validation'
    ],
    function (Component) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Raw_SotPay/payment/sotpay',
				purchaseOrderNumber: ''
            },
			/** @inheritdoc */
			initObservable: function () {
				this._super()
					.observe('purchaseOrderNumber');
				return this;
			},
			/**
			 * @return {Object}
			 */
			getData: function () {
				return {
					method: this.item.method,
					'po_number': this.purchaseOrderNumber(),
					'additional_data': null
				};
			},
			/**
			 * @return {jQuery}
			 */
			validate: function () {
				var form = 'form[data-role=fortoxfinance-form]';
				return $(form).validation() && $(form).validation('isValid');
			},
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);
