define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'sotpay',
                component: 'Raw_SotPay/js/view/payment/method-renderer/sotpay-method'
            }
        );
        return Component.extend({});
    }
);