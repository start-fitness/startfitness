<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\SotPay\Model\Payment;

class Sotpay extends \Magento\Payment\Model\Method\AbstractMethod
{

    protected $_code = "sotpay";
    protected $_isOffline = true;
	protected $_infoBlockType = \Magento\OfflinePayments\Block\Info\Purchaseorder::class;
	protected $_canAuthorize = true;
    protected $_canCapture = true;
	/**
     * @var string
     */
    protected $_formBlockType = \Raw\SotPay\Block\Form\SotPay::class;

	
    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {
		if(!$this->isadmin()) return false;
        return parent::isAvailable($quote);
    }
	
	/**
     * Assign data to info model instance
     *
     * @param \Magento\Framework\DataObject|mixed $data
     * @return $this
     * @throws LocalizedException
     */
    public function assignData(\Magento\Framework\DataObject $data)
    {
        $this->getInfoInstance()->setPoNumber($data->getPoNumber());
        return $this;
    }
	
	/**
     * Validate payment method information object
     *
     * @return $this
     * @throws LocalizedException
     * @api
     */
    public function validate()
    {
        parent::validate();

        if (empty($this->getInfoInstance()->getPoNumber())) {
            throw new LocalizedException(__('SOTPay order number is a required field.'));
        }

        return $this;
    }
	
	
	function isadmin($store = null) {
		/** @var \Magento\Framework\ObjectManagerInterface $om */
		$om = \Magento\Framework\App\ObjectManager::getInstance();
		/** @var \Magento\Framework\App\State $state */
		$state =  $om->get('Magento\Framework\App\State');
		return 'adminhtml' === $state->getAreaCode();
	}
}

