<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface;
use Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterfaceFactory;

class ShippingGroupCourriers extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'raw_courriermanager_shipping_group_courriers';
    protected $shipping_group_courriersDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ShippingGroupCourriersInterfaceFactory $shipping_group_courriersDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ShippingGroupCourriersInterfaceFactory $shipping_group_courriersDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers $resource,
        \Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers\Collection $resourceCollection,
        array $data = []
    ) {
        $this->shipping_group_courriersDataFactory = $shipping_group_courriersDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve shipping_group_courriers model with shipping_group_courriers data
     * @return ShippingGroupCourriersInterface
     */
    public function getDataModel()
    {
        $shipping_group_courriersData = $this->getData();
        
        $shipping_group_courriersDataObject = $this->shipping_group_courriersDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $shipping_group_courriersDataObject,
            $shipping_group_courriersData,
            ShippingGroupCourriersInterface::class
        );
        
        return $shipping_group_courriersDataObject;
    }
}

