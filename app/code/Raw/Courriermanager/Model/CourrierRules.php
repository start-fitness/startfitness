<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\CourrierRulesInterface;
use Raw\Courriermanager\Api\Data\CourrierRulesInterfaceFactory;

class CourrierRules extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'raw_courriermanager_courrier_rules';
    protected $courrier_rulesDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CourrierRulesInterfaceFactory $courrier_rulesDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\CourrierRules $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\CourrierRules\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CourrierRulesInterfaceFactory $courrier_rulesDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\CourrierRules $resource,
        \Raw\Courriermanager\Model\ResourceModel\CourrierRules\Collection $resourceCollection,
        array $data = []
    ) {
        $this->courrier_rulesDataFactory = $courrier_rulesDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve courrier_rules model with courrier_rules data
     * @return CourrierRulesInterface
     */
    public function getDataModel()
    {
        $courrier_rulesData = $this->getData();
        
        $courrier_rulesDataObject = $this->courrier_rulesDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $courrier_rulesDataObject,
            $courrier_rulesData,
            CourrierRulesInterface::class
        );
        
        return $courrier_rulesDataObject;
    }
}

