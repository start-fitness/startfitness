<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterfaceFactory;
use Raw\Courriermanager\Api\Data\ShippingGroupCourriersSearchResultsInterfaceFactory;
use Raw\Courriermanager\Api\ShippingGroupCourriersRepositoryInterface;
use Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers as ResourceShippingGroupCourriers;
use Raw\Courriermanager\Model\ResourceModel\ShippingGroupCourriers\CollectionFactory as ShippingGroupCourriersCollectionFactory;

class ShippingGroupCourriersRepository implements ShippingGroupCourriersRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $shippingGroupCourriersFactory;

    protected $shippingGroupCourriersCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $dataShippingGroupCourriersFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceShippingGroupCourriers $resource
     * @param ShippingGroupCourriersFactory $shippingGroupCourriersFactory
     * @param ShippingGroupCourriersInterfaceFactory $dataShippingGroupCourriersFactory
     * @param ShippingGroupCourriersCollectionFactory $shippingGroupCourriersCollectionFactory
     * @param ShippingGroupCourriersSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceShippingGroupCourriers $resource,
        ShippingGroupCourriersFactory $shippingGroupCourriersFactory,
        ShippingGroupCourriersInterfaceFactory $dataShippingGroupCourriersFactory,
        ShippingGroupCourriersCollectionFactory $shippingGroupCourriersCollectionFactory,
        ShippingGroupCourriersSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->shippingGroupCourriersFactory = $shippingGroupCourriersFactory;
        $this->shippingGroupCourriersCollectionFactory = $shippingGroupCourriersCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataShippingGroupCourriersFactory = $dataShippingGroupCourriersFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
    ) {
        /* if (empty($shippingGroupCourriers->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $shippingGroupCourriers->setStoreId($storeId);
        } */
        
        $shippingGroupCourriersData = $this->extensibleDataObjectConverter->toNestedArray(
            $shippingGroupCourriers,
            [],
            \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface::class
        );
        
        $shippingGroupCourriersModel = $this->shippingGroupCourriersFactory->create()->setData($shippingGroupCourriersData);
        
        try {
            $this->resource->save($shippingGroupCourriersModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the shippingGroupCourriers: %1',
                $exception->getMessage()
            ));
        }
        return $shippingGroupCourriersModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($shippingGroupCourriersId)
    {
        $shippingGroupCourriers = $this->shippingGroupCourriersFactory->create();
        $this->resource->load($shippingGroupCourriers, $shippingGroupCourriersId);
        if (!$shippingGroupCourriers->getId()) {
            throw new NoSuchEntityException(__('shipping_group_courriers with id "%1" does not exist.', $shippingGroupCourriersId));
        }
        return $shippingGroupCourriers->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->shippingGroupCourriersCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
    ) {
        try {
            $shippingGroupCourriersModel = $this->shippingGroupCourriersFactory->create();
            $this->resource->load($shippingGroupCourriersModel, $shippingGroupCourriers->getShippingGroupCourriersId());
            $this->resource->delete($shippingGroupCourriersModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the shipping_group_courriers: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($shippingGroupCourriersId)
    {
        return $this->delete($this->get($shippingGroupCourriersId));
    }
}

