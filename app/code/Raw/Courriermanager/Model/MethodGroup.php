<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\MethodGroupInterface;
use Raw\Courriermanager\Api\Data\MethodGroupInterfaceFactory;

class MethodGroup extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'raw_courriermanager_method_group';
    protected $method_groupDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param MethodGroupInterfaceFactory $method_groupDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\MethodGroup $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\MethodGroup\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        MethodGroupInterfaceFactory $method_groupDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\MethodGroup $resource,
        \Raw\Courriermanager\Model\ResourceModel\MethodGroup\Collection $resourceCollection,
        array $data = []
    ) {
        $this->method_groupDataFactory = $method_groupDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve method_group model with method_group data
     * @return MethodGroupInterface
     */
    public function getDataModel()
    {
        $method_groupData = $this->getData();
        
        $method_groupDataObject = $this->method_groupDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $method_groupDataObject,
            $method_groupData,
            MethodGroupInterface::class
        );
        
        return $method_groupDataObject;
    }
}

