<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\AllocationsRepositoryInterface;
use Raw\Courriermanager\Api\Data\AllocationsInterfaceFactory;
use Raw\Courriermanager\Api\Data\AllocationsSearchResultsInterfaceFactory;
use Raw\Courriermanager\Model\ResourceModel\Allocations as ResourceAllocations;
use Raw\Courriermanager\Model\ResourceModel\Allocations\CollectionFactory as AllocationsCollectionFactory;

class AllocationsRepository implements AllocationsRepositoryInterface
{

    protected $resource;

    protected $allocationsFactory;

    protected $dataObjectHelper;

    protected $extensibleDataObjectConverter;
    private $storeManager;

    protected $dataObjectProcessor;

    protected $allocationsCollectionFactory;

    protected $searchResultsFactory;

    protected $dataAllocationsFactory;

    private $collectionProcessor;

    protected $extensionAttributesJoinProcessor;


    /**
     * @param ResourceAllocations $resource
     * @param AllocationsFactory $allocationsFactory
     * @param AllocationsInterfaceFactory $dataAllocationsFactory
     * @param AllocationsCollectionFactory $allocationsCollectionFactory
     * @param AllocationsSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceAllocations $resource,
        AllocationsFactory $allocationsFactory,
        AllocationsInterfaceFactory $dataAllocationsFactory,
        AllocationsCollectionFactory $allocationsCollectionFactory,
        AllocationsSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->allocationsFactory = $allocationsFactory;
        $this->allocationsCollectionFactory = $allocationsCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataAllocationsFactory = $dataAllocationsFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
    ) {
        /* if (empty($allocations->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $allocations->setStoreId($storeId);
        } */
        
        $allocationsData = $this->extensibleDataObjectConverter->toNestedArray(
            $allocations,
            [],
            \Raw\Courriermanager\Api\Data\AllocationsInterface::class
        );
        
        $allocationsModel = $this->allocationsFactory->create()->setData($allocationsData);
        
        try {
            $this->resource->save($allocationsModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the allocations: %1',
                $exception->getMessage()
            ));
        }
        return $allocationsModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($allocationsId)
    {
        $allocations = $this->allocationsFactory->create();
        $this->resource->load($allocations, $allocationsId);
        if (!$allocations->getId()) {
            throw new NoSuchEntityException(__('allocations with id "%1" does not exist.', $allocationsId));
        }
        return $allocations->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->allocationsCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\AllocationsInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
    ) {
        try {
            $allocationsModel = $this->allocationsFactory->create();
            $this->resource->load($allocationsModel, $allocations->getAllocationsId());
            $this->resource->delete($allocationsModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the allocations: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($allocationsId)
    {
        return $this->delete($this->get($allocationsId));
    }
}

