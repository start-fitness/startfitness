<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\AllocationsInterface;
use Raw\Courriermanager\Api\Data\AllocationsInterfaceFactory;

class Allocations extends \Magento\Framework\Model\AbstractModel
{

    protected $allocationsDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'raw_courriermanager_allocations';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param AllocationsInterfaceFactory $allocationsDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\Allocations $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\Allocations\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        AllocationsInterfaceFactory $allocationsDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\Allocations $resource,
        \Raw\Courriermanager\Model\ResourceModel\Allocations\Collection $resourceCollection,
        array $data = []
    ) {
        $this->allocationsDataFactory = $allocationsDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve allocations model with allocations data
     * @return AllocationsInterface
     */
    public function getDataModel()
    {
        $allocationsData = $this->getData();
        
        $allocationsDataObject = $this->allocationsDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $allocationsDataObject,
            $allocationsData,
            AllocationsInterface::class
        );
        
        return $allocationsDataObject;
    }
}

