<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\Data\MethodGroupInterfaceFactory;
use Raw\Courriermanager\Api\Data\MethodGroupSearchResultsInterfaceFactory;
use Raw\Courriermanager\Api\MethodGroupRepositoryInterface;
use Raw\Courriermanager\Model\ResourceModel\MethodGroup as ResourceMethodGroup;
use Raw\Courriermanager\Model\ResourceModel\MethodGroup\CollectionFactory as MethodGroupCollectionFactory;

class MethodGroupRepository implements MethodGroupRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $methodGroupCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $dataMethodGroupFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $methodGroupFactory;


    /**
     * @param ResourceMethodGroup $resource
     * @param MethodGroupFactory $methodGroupFactory
     * @param MethodGroupInterfaceFactory $dataMethodGroupFactory
     * @param MethodGroupCollectionFactory $methodGroupCollectionFactory
     * @param MethodGroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMethodGroup $resource,
        MethodGroupFactory $methodGroupFactory,
        MethodGroupInterfaceFactory $dataMethodGroupFactory,
        MethodGroupCollectionFactory $methodGroupCollectionFactory,
        MethodGroupSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->methodGroupFactory = $methodGroupFactory;
        $this->methodGroupCollectionFactory = $methodGroupCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMethodGroupFactory = $dataMethodGroupFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
    ) {
        /* if (empty($methodGroup->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $methodGroup->setStoreId($storeId);
        } */
        
        $methodGroupData = $this->extensibleDataObjectConverter->toNestedArray(
            $methodGroup,
            [],
            \Raw\Courriermanager\Api\Data\MethodGroupInterface::class
        );
        
        $methodGroupModel = $this->methodGroupFactory->create()->setData($methodGroupData);
        
        try {
            $this->resource->save($methodGroupModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the methodGroup: %1',
                $exception->getMessage()
            ));
        }
        return $methodGroupModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($methodGroupId)
    {
        $methodGroup = $this->methodGroupFactory->create();
        $this->resource->load($methodGroup, $methodGroupId);
        if (!$methodGroup->getId()) {
            throw new NoSuchEntityException(__('method_group with id "%1" does not exist.', $methodGroupId));
        }
        return $methodGroup->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->methodGroupCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\MethodGroupInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
    ) {
        try {
            $methodGroupModel = $this->methodGroupFactory->create();
            $this->resource->load($methodGroupModel, $methodGroup->getMethodGroupId());
            $this->resource->delete($methodGroupModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the method_group: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($methodGroupId)
    {
        return $this->delete($this->get($methodGroupId));
    }
}

