<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\Data\Method2typeInterfaceFactory;
use Raw\Courriermanager\Api\Data\Method2typeSearchResultsInterfaceFactory;
use Raw\Courriermanager\Api\Method2typeRepositoryInterface;
use Raw\Courriermanager\Model\ResourceModel\Method2type as ResourceMethod2type;
use Raw\Courriermanager\Model\ResourceModel\Method2type\CollectionFactory as Method2typeCollectionFactory;

class Method2typeRepository implements Method2typeRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $method2typeFactory;

    protected $searchResultsFactory;

    protected $dataMethod2typeFactory;

    protected $dataObjectProcessor;

    protected $method2typeCollectionFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceMethod2type $resource
     * @param Method2typeFactory $method2typeFactory
     * @param Method2typeInterfaceFactory $dataMethod2typeFactory
     * @param Method2typeCollectionFactory $method2typeCollectionFactory
     * @param Method2typeSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceMethod2type $resource,
        Method2typeFactory $method2typeFactory,
        Method2typeInterfaceFactory $dataMethod2typeFactory,
        Method2typeCollectionFactory $method2typeCollectionFactory,
        Method2typeSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->method2typeFactory = $method2typeFactory;
        $this->method2typeCollectionFactory = $method2typeCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataMethod2typeFactory = $dataMethod2typeFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
    ) {
        /* if (empty($method2type->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $method2type->setStoreId($storeId);
        } */
        
        $method2typeData = $this->extensibleDataObjectConverter->toNestedArray(
            $method2type,
            [],
            \Raw\Courriermanager\Api\Data\Method2typeInterface::class
        );
        
        $method2typeModel = $this->method2typeFactory->create()->setData($method2typeData);
        
        try {
            $this->resource->save($method2typeModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the method2type: %1',
                $exception->getMessage()
            ));
        }
        return $method2typeModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($method2typeId)
    {
        $method2type = $this->method2typeFactory->create();
        $this->resource->load($method2type, $method2typeId);
        if (!$method2type->getId()) {
            throw new NoSuchEntityException(__('method2type with id "%1" does not exist.', $method2typeId));
        }
        return $method2type->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->method2typeCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\Method2typeInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
    ) {
        try {
            $method2typeModel = $this->method2typeFactory->create();
            $this->resource->load($method2typeModel, $method2type->getMethod2typeId());
            $this->resource->delete($method2typeModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the method2type: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($method2typeId)
    {
        return $this->delete($this->get($method2typeId));
    }
}

