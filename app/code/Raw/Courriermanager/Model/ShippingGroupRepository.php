<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\Data\ShippingGroupInterfaceFactory;
use Raw\Courriermanager\Api\Data\ShippingGroupSearchResultsInterfaceFactory;
use Raw\Courriermanager\Api\ShippingGroupRepositoryInterface;
use Raw\Courriermanager\Model\ResourceModel\ShippingGroup as ResourceShippingGroup;
use Raw\Courriermanager\Model\ResourceModel\ShippingGroup\CollectionFactory as ShippingGroupCollectionFactory;

class ShippingGroupRepository implements ShippingGroupRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $shippingGroupCollectionFactory;

    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $dataShippingGroupFactory;

    protected $shippingGroupFactory;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceShippingGroup $resource
     * @param ShippingGroupFactory $shippingGroupFactory
     * @param ShippingGroupInterfaceFactory $dataShippingGroupFactory
     * @param ShippingGroupCollectionFactory $shippingGroupCollectionFactory
     * @param ShippingGroupSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceShippingGroup $resource,
        ShippingGroupFactory $shippingGroupFactory,
        ShippingGroupInterfaceFactory $dataShippingGroupFactory,
        ShippingGroupCollectionFactory $shippingGroupCollectionFactory,
        ShippingGroupSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->shippingGroupFactory = $shippingGroupFactory;
        $this->shippingGroupCollectionFactory = $shippingGroupCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataShippingGroupFactory = $dataShippingGroupFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
    ) {
        /* if (empty($shippingGroup->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $shippingGroup->setStoreId($storeId);
        } */
        
        $shippingGroupData = $this->extensibleDataObjectConverter->toNestedArray(
            $shippingGroup,
            [],
            \Raw\Courriermanager\Api\Data\ShippingGroupInterface::class
        );
        
        $shippingGroupModel = $this->shippingGroupFactory->create()->setData($shippingGroupData);
        
        try {
            $this->resource->save($shippingGroupModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the shippingGroup: %1',
                $exception->getMessage()
            ));
        }
        return $shippingGroupModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($shippingGroupId)
    {
        $shippingGroup = $this->shippingGroupFactory->create();
        $this->resource->load($shippingGroup, $shippingGroupId);
        if (!$shippingGroup->getId()) {
            throw new NoSuchEntityException(__('shipping_group with id "%1" does not exist.', $shippingGroupId));
        }
        return $shippingGroup->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->shippingGroupCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\ShippingGroupInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
    ) {
        try {
            $shippingGroupModel = $this->shippingGroupFactory->create();
            $this->resource->load($shippingGroupModel, $shippingGroup->getShippingGroupId());
            $this->resource->delete($shippingGroupModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the shipping_group: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($shippingGroupId)
    {
        return $this->delete($this->get($shippingGroupId));
    }
}

