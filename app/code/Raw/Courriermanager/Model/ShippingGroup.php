<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\ShippingGroupInterface;
use Raw\Courriermanager\Api\Data\ShippingGroupInterfaceFactory;

class ShippingGroup extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'raw_courriermanager_shipping_group';
    protected $shipping_groupDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ShippingGroupInterfaceFactory $shipping_groupDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\ShippingGroup $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\ShippingGroup\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ShippingGroupInterfaceFactory $shipping_groupDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\ShippingGroup $resource,
        \Raw\Courriermanager\Model\ResourceModel\ShippingGroup\Collection $resourceCollection,
        array $data = []
    ) {
        $this->shipping_groupDataFactory = $shipping_groupDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve shipping_group model with shipping_group data
     * @return ShippingGroupInterface
     */
    public function getDataModel()
    {
        $shipping_groupData = $this->getData();
        
        $shipping_groupDataObject = $this->shipping_groupDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $shipping_groupDataObject,
            $shipping_groupData,
            ShippingGroupInterface::class
        );
        
        return $shipping_groupDataObject;
    }
}

