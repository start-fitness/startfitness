<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\ShippingGroupInterface;

class ShippingGroup extends \Magento\Framework\Api\AbstractExtensibleObject implements ShippingGroupInterface
{

    /**
     * Get shipping_group_id
     * @return string|null
     */
    public function getShippingGroupId()
    {
        return $this->_get(self::SHIPPING_GROUP_ID);
    }

    /**
     * Set shipping_group_id
     * @param string $shippingGroupId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setShippingGroupId($shippingGroupId)
    {
        return $this->setData(self::SHIPPING_GROUP_ID, $shippingGroupId);
    }

    /**
     * Get code
     * @return string|null
     */
    public function getCode()
    {
        return $this->_get(self::CODE);
    }

    /**
     * Set code
     * @param string $code
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setCode($code)
    {
        return $this->setData(self::CODE, $code);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority()
    {
        return $this->_get(self::PRIORITY);
    }

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setPriority($priority)
    {
        return $this->setData(self::PRIORITY, $priority);
    }
}

