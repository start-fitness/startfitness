<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\MethodGroupInterface;

class MethodGroup extends \Magento\Framework\Api\AbstractExtensibleObject implements MethodGroupInterface
{

    /**
     * Get method_group_id
     * @return string|null
     */
    public function getMethodGroupId()
    {
        return $this->_get(self::METHOD_GROUP_ID);
    }

    /**
     * Set method_group_id
     * @param string $methodGroupId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setMethodGroupId($methodGroupId)
    {
        return $this->setData(self::METHOD_GROUP_ID, $methodGroupId);
    }

    /**
     * Get method_id
     * @return string|null
     */
    public function getMethodId()
    {
        return $this->_get(self::METHOD_ID);
    }

    /**
     * Set method_id
     * @param string $methodId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setMethodId($methodId)
    {
        return $this->setData(self::METHOD_ID, $methodId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId()
    {
        return $this->_get(self::GROUP_ID);
    }

    /**
     * Set group_id
     * @param string $groupId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setGroupId($groupId)
    {
        return $this->setData(self::GROUP_ID, $groupId);
    }
}

