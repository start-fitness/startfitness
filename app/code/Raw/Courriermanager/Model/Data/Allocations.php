<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\AllocationsInterface;

class Allocations extends \Magento\Framework\Api\AbstractExtensibleObject implements AllocationsInterface
{

    /**
     * Get allocations_id
     * @return string|null
     */
    public function getAllocationsId()
    {
        return $this->_get(self::ALLOCATIONS_ID);
    }

    /**
     * Set allocations_id
     * @param string $allocationsId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setAllocationsId($allocationsId)
    {
        return $this->setData(self::ALLOCATIONS_ID, $allocationsId);
    }

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId()
    {
        return $this->_get(self::ORDER_ID);
    }

    /**
     * Set order_id
     * @param string $orderId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId()
    {
        return $this->_get(self::COURRIER_ID);
    }

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setCourrierId($courrierId)
    {
        return $this->setData(self::COURRIER_ID, $courrierId);
    }

    /**
     * Get number
     * @return string|null
     */
    public function getNumber()
    {
        return $this->_get(self::NUMBER);
    }

    /**
     * Set number
     * @param string $number
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setNumber($number)
    {
        return $this->setData(self::NUMBER, $number);
    }

    /**
     * Get data
     * @return string|null
     */
    public function getDatafield()
    {
        return $this->_get(self::DATA);
    }

    /**
     * Set data
     * @param string $data
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setDatafield($data)
    {
        return $this->setData(self::DATA, $data);
    }
}

