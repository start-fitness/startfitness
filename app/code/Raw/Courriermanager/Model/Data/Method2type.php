<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\Method2typeInterface;

class Method2type extends \Magento\Framework\Api\AbstractExtensibleObject implements Method2typeInterface
{

    /**
     * Get method2type_id
     * @return string|null
     */
    public function getMethod2typeId()
    {
        return $this->_get(self::METHOD2TYPE_ID);
    }

    /**
     * Set method2type_id
     * @param string $method2typeId
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setMethod2typeId($method2typeId)
    {
        return $this->setData(self::METHOD2TYPE_ID, $method2typeId);
    }

    /**
     * Get shipping_method
     * @return string|null
     */
    public function getShippingMethod()
    {
        return $this->_get(self::SHIPPING_METHOD);
    }

    /**
     * Set shipping_method
     * @param string $shippingMethod
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setShippingMethod($shippingMethod)
    {
        return $this->setData(self::SHIPPING_METHOD, $shippingMethod);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get courrier_type
     * @return string|null
     */
    public function getCourrierType()
    {
        return $this->_get(self::COURRIER_TYPE);
    }

    /**
     * Set courrier_type
     * @param string $courrierType
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setCourrierType($courrierType)
    {
        return $this->setData(self::COURRIER_TYPE, $courrierType);
    }

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority()
    {
        return $this->_get(self::PRIORITY);
    }

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setPriority($priority)
    {
        return $this->setData(self::PRIORITY, $priority);
    }
}

