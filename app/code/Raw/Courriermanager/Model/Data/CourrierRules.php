<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\CourrierRulesInterface;

class CourrierRules extends \Magento\Framework\Api\AbstractExtensibleObject implements CourrierRulesInterface
{

    /**
     * Get courrier_rules_id
     * @return string|null
     */
    public function getCourrierRulesId()
    {
        return $this->_get(self::COURRIER_RULES_ID);
    }

    /**
     * Set courrier_rules_id
     * @param string $courrierRulesId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCourrierRulesId($courrierRulesId)
    {
        return $this->setData(self::COURRIER_RULES_ID, $courrierRulesId);
    }

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId()
    {
        return $this->_get(self::COURRIER_ID);
    }

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCourrierId($courrierId)
    {
        return $this->setData(self::COURRIER_ID, $courrierId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get rule_type
     * @return string|null
     */
    public function getRuleType()
    {
        return $this->_get(self::RULE_TYPE);
    }

    /**
     * Set rule_type
     * @param string $ruleType
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setRuleType($ruleType)
    {
        return $this->setData(self::RULE_TYPE, $ruleType);
    }

    /**
     * Get from
     * @return string|null
     */
    public function getFrom()
    {
        return $this->_get(self::FROM);
    }

    /**
     * Set from
     * @param string $from
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setFrom($from)
    {
        return $this->setData(self::FROM, $from);
    }

    /**
     * Get to
     * @return string|null
     */
    public function getTo()
    {
        return $this->_get(self::TO);
    }

    /**
     * Set to
     * @param string $to
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setTo($to)
    {
        return $this->setData(self::TO, $to);
    }

    /**
     * Get cost
     * @return string|null
     */
    public function getCost()
    {
        return $this->_get(self::COST);
    }

    /**
     * Set cost
     * @param string $cost
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCost($cost)
    {
        return $this->setData(self::COST, $cost);
    }

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority()
    {
        return $this->_get(self::PRIORITY);
    }

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setPriority($priority)
    {
        return $this->setData(self::PRIORITY, $priority);
    }

    /**
     * Get country_id
     * @return string|null
     */
    public function getCountryId()
    {
        return $this->_get(self::COUNTRY_ID);
    }

    /**
     * Set country_id
     * @param string $countryId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCountryId($countryId)
    {
        return $this->setData(self::COUNTRY_ID, $countryId);
    }
}

