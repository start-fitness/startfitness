<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\CourriersInterface;

class Courriers extends \Magento\Framework\Api\AbstractExtensibleObject implements CourriersInterface
{

    /**
     * Get courriers_id
     * @return string|null
     */
    public function getCourriersId()
    {
        return $this->_get(self::COURRIERS_ID);
    }

    /**
     * Set courriers_id
     * @param string $courriersId
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setCourriersId($courriersId)
    {
        return $this->setData(self::COURRIERS_ID, $courriersId);
    }

    /**
     * Get name
     * @return string|null
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * Set name
     * @param string $name
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\CourriersExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\CourriersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\CourriersExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get method
     * @return string|null
     */
    public function getMethod()
    {
        return $this->_get(self::METHOD);
    }

    /**
     * Set method
     * @param string $method
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setMethod($method)
    {
        return $this->setData(self::METHOD, $method);
    }

    /**
     * Get type
     * @return string|null
     */
    public function getType()
    {
        return $this->_get(self::TYPE);
    }

    /**
     * Set type
     * @param string $type
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setType($type)
    {
        return $this->setData(self::TYPE, $type);
    }
}

