<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\Data;

use Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface;

class ShippingGroupCourriers extends \Magento\Framework\Api\AbstractExtensibleObject implements ShippingGroupCourriersInterface
{

    /**
     * Get shipping_group_courriers_id
     * @return string|null
     */
    public function getShippingGroupCourriersId()
    {
        return $this->_get(self::SHIPPING_GROUP_COURRIERS_ID);
    }

    /**
     * Set shipping_group_courriers_id
     * @param string $shippingGroupCourriersId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setShippingGroupCourriersId($shippingGroupCourriersId)
    {
        return $this->setData(self::SHIPPING_GROUP_COURRIERS_ID, $shippingGroupCourriersId);
    }

    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId()
    {
        return $this->_get(self::GROUP_ID);
    }

    /**
     * Set group_id
     * @param string $groupId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setGroupId($groupId)
    {
        return $this->setData(self::GROUP_ID, $groupId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId()
    {
        return $this->_get(self::COURRIER_ID);
    }

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setCourrierId($courrierId)
    {
        return $this->setData(self::COURRIER_ID, $courrierId);
    }
}

