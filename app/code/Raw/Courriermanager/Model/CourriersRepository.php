<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\CourriersRepositoryInterface;
use Raw\Courriermanager\Api\Data\CourriersInterfaceFactory;
use Raw\Courriermanager\Api\Data\CourriersSearchResultsInterfaceFactory;
use Raw\Courriermanager\Model\ResourceModel\Courriers as ResourceCourriers;
use Raw\Courriermanager\Model\ResourceModel\Courriers\CollectionFactory as CourriersCollectionFactory;

class CourriersRepository implements CourriersRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $dataObjectHelper;

    protected $resource;

    protected $courriersFactory;

    protected $courriersCollectionFactory;

    private $storeManager;

    protected $dataCourriersFactory;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceCourriers $resource
     * @param CourriersFactory $courriersFactory
     * @param CourriersInterfaceFactory $dataCourriersFactory
     * @param CourriersCollectionFactory $courriersCollectionFactory
     * @param CourriersSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCourriers $resource,
        CourriersFactory $courriersFactory,
        CourriersInterfaceFactory $dataCourriersFactory,
        CourriersCollectionFactory $courriersCollectionFactory,
        CourriersSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->courriersFactory = $courriersFactory;
        $this->courriersCollectionFactory = $courriersCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCourriersFactory = $dataCourriersFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
    ) {
        /* if (empty($courriers->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $courriers->setStoreId($storeId);
        } */
        
        $courriersData = $this->extensibleDataObjectConverter->toNestedArray(
            $courriers,
            [],
            \Raw\Courriermanager\Api\Data\CourriersInterface::class
        );
        
        $courriersModel = $this->courriersFactory->create()->setData($courriersData);
        
        try {
            $this->resource->save($courriersModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the courriers: %1',
                $exception->getMessage()
            ));
        }
        return $courriersModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($courriersId)
    {
        $courriers = $this->courriersFactory->create();
        $this->resource->load($courriers, $courriersId);
        if (!$courriers->getId()) {
            throw new NoSuchEntityException(__('courriers with id "%1" does not exist.', $courriersId));
        }
        return $courriers->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->courriersCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\CourriersInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
    ) {
        try {
            $courriersModel = $this->courriersFactory->create();
            $this->resource->load($courriersModel, $courriers->getCourriersId());
            $this->resource->delete($courriersModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the courriers: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($courriersId)
    {
        return $this->delete($this->get($courriersId));
    }
}

