<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\ResourceModel\CourrierRules;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'courrier_rules_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Raw\Courriermanager\Model\CourrierRules::class,
            \Raw\Courriermanager\Model\ResourceModel\CourrierRules::class
        );
    }
	
	protected function _initSelect()
	{
    parent::_initSelect();

		$this->getSelect()->joinLeft(
				['courrier' => $this->getTable('raw_courriermanager_courriers')],
				'main_table.courrier_id = courrier.courriers_id',
				['courriername'=>'courrier.name','courriermethod' => 'courrier.method']
			);
		$this->addFilterToMap('courriername', 'courrier.name');
		$this->addFilterToMap('courriermethod', 'courrier.method');
		
		//var_dump($this->getSelect()."");exit;
		return $this;
	}
}

