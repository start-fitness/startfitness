<?php

namespace Raw\Courriermanager\Model\ResourceModel\Order\Grid;

use Magento\Framework\Data\Collection\Db\FetchStrategyInterface as FetchStrategy;

use Magento\Framework\Data\Collection\EntityFactoryInterface as EntityFactory;

use Magento\Framework\Event\ManagerInterface as EventManager;

use Magento\Sales\Model\ResourceModel\Order;

use Magento\Sales\Model\ResourceModel\Order\Grid\Collection as OrderGridCollection;

use Psr\Log\LoggerInterface as Logger;



class Collection extends OrderGridCollection

{

    public function __construct(

        EntityFactory $entityFactory,

        Logger $logger,

        FetchStrategy $fetchStrategy,

        EventManager $eventManager,

        $mainTable = 'sales_order_grid',

        $resourceModel = Order::class

    ) {

        parent::__construct($entityFactory, $logger, $fetchStrategy, $eventManager, $mainTable, $resourceModel);

    }

    protected function _renderFiltersBefore() {

        $joinTable = $this->getTable('raw_courriermanager_allocations');

        $this->getSelect()->joinLeft(

            ['allocinner' => new \Zend_Db_Expr('(SELECT MAX(allocations_id) as allocations_id, order_id FROM raw_courriermanager_allocations GROUP BY order_id)')],

            'main_table.entity_id = allocinner.order_id',

            [] // you can add other attributes here

        );
		$this->getSelect()->joinLeft(

            ['alloc' => $joinTable],

            'allocinner.allocations_id = alloc.allocations_id',

            ['number'] // you can add other attributes here

        );

        parent::_renderFiltersBefore();

    }

}
