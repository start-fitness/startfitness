<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model\ResourceModel\MethodGroup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    /**
     * @var string
     */
    protected $_idFieldName = 'method_group_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Raw\Courriermanager\Model\MethodGroup::class,
            \Raw\Courriermanager\Model\ResourceModel\MethodGroup::class
        );
    }
}

