<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\Method2typeInterface;
use Raw\Courriermanager\Api\Data\Method2typeInterfaceFactory;

class Method2type extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $method2typeDataFactory;

    protected $_eventPrefix = 'raw_courriermanager_method2type';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param Method2typeInterfaceFactory $method2typeDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\Method2type $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\Method2type\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        Method2typeInterfaceFactory $method2typeDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\Method2type $resource,
        \Raw\Courriermanager\Model\ResourceModel\Method2type\Collection $resourceCollection,
        array $data = []
    ) {
        $this->method2typeDataFactory = $method2typeDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve method2type model with method2type data
     * @return Method2typeInterface
     */
    public function getDataModel()
    {
        $method2typeData = $this->getData();
        
        $method2typeDataObject = $this->method2typeDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $method2typeDataObject,
            $method2typeData,
            Method2typeInterface::class
        );
        
        return $method2typeDataObject;
    }
}

