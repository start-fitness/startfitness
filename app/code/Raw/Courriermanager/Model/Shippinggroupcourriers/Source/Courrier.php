<?php
namespace Raw\Courriermanager\Model\Shippinggroupcourriers\Source;

class Courrier implements \Magento\Framework\Option\ArrayInterface
{
	
	public function __construct(
	\Raw\Courriermanager\Model\ResourceModel\Courriers\CollectionFactory $courriersfactory
	)
    {
		
		$this->_courriersFactory = $courriersfactory;
    }
	
    /**
     * Retrieve options array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
		
        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        $collection = $this->_courriersFactory->create();
		$courriers = [];
		foreach($collection as $courrier){
			$courriers[$courrier->getCourriersId()] = $courrier->getName() . " - " .$courrier->getMethod();
		}
		return $courriers;
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = $this->getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}