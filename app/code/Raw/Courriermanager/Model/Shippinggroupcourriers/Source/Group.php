<?php
namespace Raw\Courriermanager\Model\Shippinggroupcourriers\Source;

class Group implements \Magento\Framework\Option\ArrayInterface
{
	
	public function __construct(
	\Raw\Courriermanager\Model\ResourceModel\ShippingGroup\CollectionFactory $groupfactory
	)
    {
		
		$this->_groupfactory = $groupfactory;
    }
	
    /**
     * Retrieve options array.
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = [];
		
        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option array
     *
     * @return string[]
     */
    public function getOptionArray()
    {
        $collection = $this->_groupfactory->create();
		$courriers = [];
		foreach($collection as $courrier){
			$courriers[$courrier->getShippingGroupId()] = $courrier->getCode();
		}
		return $courriers;
    }

    /**
     * Retrieve option array with empty value
     *
     * @return string[]
     */
    public function getAllOptions()
    {
        $result = [];

        foreach ($this->getOptionArray() as $index => $value) {
            $result[] = ['value' => $index, 'label' => $value];
        }

        return $result;
    }

    /**
     * Retrieve option text by option value
     *
     * @param string $optionId
     * @return string
     */
    public function getOptionText($optionId)
    {
        $options = $this->getOptionArray();

        return isset($options[$optionId]) ? $options[$optionId] : null;
    }
}