<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Raw\Courriermanager\Api\Data\CourriersInterface;
use Raw\Courriermanager\Api\Data\CourriersInterfaceFactory;

class Courriers extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $courriersDataFactory;

    protected $_eventPrefix = 'raw_courriermanager_courriers';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param CourriersInterfaceFactory $courriersDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Raw\Courriermanager\Model\ResourceModel\Courriers $resource
     * @param \Raw\Courriermanager\Model\ResourceModel\Courriers\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        CourriersInterfaceFactory $courriersDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Raw\Courriermanager\Model\ResourceModel\Courriers $resource,
        \Raw\Courriermanager\Model\ResourceModel\Courriers\Collection $resourceCollection,
        array $data = []
    ) {
        $this->courriersDataFactory = $courriersDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve courriers model with courriers data
     * @return CourriersInterface
     */
    public function getDataModel()
    {
        $courriersData = $this->getData();
        
        $courriersDataObject = $this->courriersDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $courriersDataObject,
            $courriersData,
            CourriersInterface::class
        );
        
        return $courriersDataObject;
    }
}

