<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Raw\Courriermanager\Api\CourrierRulesRepositoryInterface;
use Raw\Courriermanager\Api\Data\CourrierRulesInterfaceFactory;
use Raw\Courriermanager\Api\Data\CourrierRulesSearchResultsInterfaceFactory;
use Raw\Courriermanager\Model\ResourceModel\CourrierRules as ResourceCourrierRules;
use Raw\Courriermanager\Model\ResourceModel\CourrierRules\CollectionFactory as CourrierRulesCollectionFactory;

class CourrierRulesRepository implements CourrierRulesRepositoryInterface
{

    protected $extensibleDataObjectConverter;
    protected $courrierRulesFactory;

    protected $courrierRulesCollectionFactory;

    protected $dataCourrierRulesFactory;

    protected $dataObjectHelper;

    protected $resource;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;


    /**
     * @param ResourceCourrierRules $resource
     * @param CourrierRulesFactory $courrierRulesFactory
     * @param CourrierRulesInterfaceFactory $dataCourrierRulesFactory
     * @param CourrierRulesCollectionFactory $courrierRulesCollectionFactory
     * @param CourrierRulesSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceCourrierRules $resource,
        CourrierRulesFactory $courrierRulesFactory,
        CourrierRulesInterfaceFactory $dataCourrierRulesFactory,
        CourrierRulesCollectionFactory $courrierRulesCollectionFactory,
        CourrierRulesSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->courrierRulesFactory = $courrierRulesFactory;
        $this->courrierRulesCollectionFactory = $courrierRulesCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataCourrierRulesFactory = $dataCourrierRulesFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
    ) {
        /* if (empty($courrierRules->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $courrierRules->setStoreId($storeId);
        } */
        
        $courrierRulesData = $this->extensibleDataObjectConverter->toNestedArray(
            $courrierRules,
            [],
            \Raw\Courriermanager\Api\Data\CourrierRulesInterface::class
        );
        
        $courrierRulesModel = $this->courrierRulesFactory->create()->setData($courrierRulesData);
        
        try {
            $this->resource->save($courrierRulesModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the courrierRules: %1',
                $exception->getMessage()
            ));
        }
        return $courrierRulesModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($courrierRulesId)
    {
        $courrierRules = $this->courrierRulesFactory->create();
        $this->resource->load($courrierRules, $courrierRulesId);
        if (!$courrierRules->getId()) {
            throw new NoSuchEntityException(__('courrier_rules with id "%1" does not exist.', $courrierRulesId));
        }
        return $courrierRules->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->courrierRulesCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Raw\Courriermanager\Api\Data\CourrierRulesInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
    ) {
        try {
            $courrierRulesModel = $this->courrierRulesFactory->create();
            $this->resource->load($courrierRulesModel, $courrierRules->getCourrierRulesId());
            $this->resource->delete($courrierRulesModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the courrier_rules: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($courrierRulesId)
    {
        return $this->delete($this->get($courrierRulesId));
    }
}

