<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface MethodGroupRepositoryInterface
{

    /**
     * Save method_group
     * @param \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
    );

    /**
     * Retrieve method_group
     * @param string $methodGroupId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($methodGroupId);

    /**
     * Retrieve method_group matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\MethodGroupSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete method_group
     * @param \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\MethodGroupInterface $methodGroup
    );

    /**
     * Delete method_group by ID
     * @param string $methodGroupId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($methodGroupId);
}

