<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CourrierRulesRepositoryInterface
{

    /**
     * Save courrier_rules
     * @param \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
    );

    /**
     * Retrieve courrier_rules
     * @param string $courrierRulesId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($courrierRulesId);

    /**
     * Retrieve courrier_rules matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete courrier_rules
     * @param \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\CourrierRulesInterface $courrierRules
    );

    /**
     * Delete courrier_rules by ID
     * @param string $courrierRulesId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($courrierRulesId);
}

