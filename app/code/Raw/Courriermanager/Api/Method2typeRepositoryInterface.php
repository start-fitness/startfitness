<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface Method2typeRepositoryInterface
{

    /**
     * Save method2type
     * @param \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
    );

    /**
     * Retrieve method2type
     * @param string $method2typeId
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($method2typeId);

    /**
     * Retrieve method2type matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\Method2typeSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete method2type
     * @param \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\Method2typeInterface $method2type
    );

    /**
     * Delete method2type by ID
     * @param string $method2typeId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($method2typeId);
}

