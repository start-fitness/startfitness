<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ShippingGroupCourriersRepositoryInterface
{

    /**
     * Save shipping_group_courriers
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
    );

    /**
     * Retrieve shipping_group_courriers
     * @param string $shippingGroupCourriersId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($shippingGroupCourriersId);

    /**
     * Retrieve shipping_group_courriers matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete shipping_group_courriers
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface $shippingGroupCourriers
    );

    /**
     * Delete shipping_group_courriers by ID
     * @param string $shippingGroupCourriersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shippingGroupCourriersId);
}

