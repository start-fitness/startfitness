<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface ShippingGroupRepositoryInterface
{

    /**
     * Save shipping_group
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
    );

    /**
     * Retrieve shipping_group
     * @param string $shippingGroupId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($shippingGroupId);

    /**
     * Retrieve shipping_group matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete shipping_group
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\ShippingGroupInterface $shippingGroup
    );

    /**
     * Delete shipping_group by ID
     * @param string $shippingGroupId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($shippingGroupId);
}

