<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface AllocationsInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const ORDER_ID = 'order_id';
    const NUMBER = 'number';
    const ALLOCATIONS_ID = 'allocations_id';
    const DATA = 'data';
    const COURRIER_ID = 'courrier_id';

    /**
     * Get allocations_id
     * @return string|null
     */
    public function getAllocationsId();

    /**
     * Set allocations_id
     * @param string $allocationsId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setAllocationsId($allocationsId);

    /**
     * Get order_id
     * @return string|null
     */
    public function getOrderId();

    /**
     * Set order_id
     * @param string $orderId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setOrderId($orderId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\AllocationsExtensionInterface $extensionAttributes
    );

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId();

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setCourrierId($courrierId);

    /**
     * Get number
     * @return string|null
     */
    public function getNumber();

    /**
     * Set number
     * @param string $number
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setNumber($number);

    /**
     * Get data
     * @return string|null
     */
    public function getDatafield();

    /**
     * Set data
     * @param string $data
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     */
    public function setDatafield($data);
}

