<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface ShippingGroupCourriersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get shipping_group_courriers list.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface[]
     */
    public function getItems();

    /**
     * Set group_id list.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

