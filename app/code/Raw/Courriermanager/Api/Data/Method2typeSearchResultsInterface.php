<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface Method2typeSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get method2type list.
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface[]
     */
    public function getItems();

    /**
     * Set shipping_method list.
     * @param \Raw\Courriermanager\Api\Data\Method2typeInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

