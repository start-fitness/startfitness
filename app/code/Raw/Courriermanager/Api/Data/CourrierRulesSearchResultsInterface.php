<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface CourrierRulesSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get courrier_rules list.
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface[]
     */
    public function getItems();

    /**
     * Set courrier_id list.
     * @param \Raw\Courriermanager\Api\Data\CourrierRulesInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

