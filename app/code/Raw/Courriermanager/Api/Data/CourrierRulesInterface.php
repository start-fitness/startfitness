<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface CourrierRulesInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const PRIORITY = 'priority';
    const COURRIER_RULES_ID = 'courrier_rules_id';
    const COST = 'cost';
    const TO = 'to';
    const RULE_TYPE = 'rule_type';
    const FROM = 'from';
    const COURRIER_ID = 'courrier_id';
    const COUNTRY_ID = 'country_id';

    /**
     * Get courrier_rules_id
     * @return string|null
     */
    public function getCourrierRulesId();

    /**
     * Set courrier_rules_id
     * @param string $courrierRulesId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCourrierRulesId($courrierRulesId);

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId();

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCourrierId($courrierId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\CourrierRulesExtensionInterface $extensionAttributes
    );

    /**
     * Get rule_type
     * @return string|null
     */
    public function getRuleType();

    /**
     * Set rule_type
     * @param string $ruleType
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setRuleType($ruleType);

    /**
     * Get from
     * @return string|null
     */
    public function getFrom();

    /**
     * Set from
     * @param string $from
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setFrom($from);

    /**
     * Get to
     * @return string|null
     */
    public function getTo();

    /**
     * Set to
     * @param string $to
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setTo($to);

    /**
     * Get cost
     * @return string|null
     */
    public function getCost();

    /**
     * Set cost
     * @param string $cost
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCost($cost);

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority();

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setPriority($priority);

    /**
     * Get country_id
     * @return string|null
     */
    public function getCountryId();

    /**
     * Set country_id
     * @param string $countryId
     * @return \Raw\Courriermanager\Api\Data\CourrierRulesInterface
     */
    public function setCountryId($countryId);
}

