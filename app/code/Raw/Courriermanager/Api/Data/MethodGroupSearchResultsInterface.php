<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface MethodGroupSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get method_group list.
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface[]
     */
    public function getItems();

    /**
     * Set method_id list.
     * @param \Raw\Courriermanager\Api\Data\MethodGroupInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

