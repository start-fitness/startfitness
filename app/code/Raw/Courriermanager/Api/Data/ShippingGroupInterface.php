<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface ShippingGroupInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CODE = 'code';
    const SHIPPING_GROUP_ID = 'shipping_group_id';
    const PRIORITY = 'priority';

    /**
     * Get shipping_group_id
     * @return string|null
     */
    public function getShippingGroupId();

    /**
     * Set shipping_group_id
     * @param string $shippingGroupId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setShippingGroupId($shippingGroupId);

    /**
     * Get code
     * @return string|null
     */
    public function getCode();

    /**
     * Set code
     * @param string $code
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setCode($code);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\ShippingGroupExtensionInterface $extensionAttributes
    );

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority();

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface
     */
    public function setPriority($priority);
}

