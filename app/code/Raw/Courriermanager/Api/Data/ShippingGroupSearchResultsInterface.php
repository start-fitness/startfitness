<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface ShippingGroupSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get shipping_group list.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupInterface[]
     */
    public function getItems();

    /**
     * Set code list.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

