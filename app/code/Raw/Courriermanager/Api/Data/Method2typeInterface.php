<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface Method2typeInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const SHIPPING_METHOD = 'shipping_method';
    const COURRIER_TYPE = 'courrier_type';
    const METHOD2TYPE_ID = 'method2type_id';
    const PRIORITY = 'priority';

    /**
     * Get method2type_id
     * @return string|null
     */
    public function getMethod2typeId();

    /**
     * Set method2type_id
     * @param string $method2typeId
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setMethod2typeId($method2typeId);

    /**
     * Get shipping_method
     * @return string|null
     */
    public function getShippingMethod();

    /**
     * Set shipping_method
     * @param string $shippingMethod
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setShippingMethod($shippingMethod);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\Method2typeExtensionInterface $extensionAttributes
    );

    /**
     * Get courrier_type
     * @return string|null
     */
    public function getCourrierType();

    /**
     * Set courrier_type
     * @param string $courrierType
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setCourrierType($courrierType);

    /**
     * Get priority
     * @return string|null
     */
    public function getPriority();

    /**
     * Set priority
     * @param string $priority
     * @return \Raw\Courriermanager\Api\Data\Method2typeInterface
     */
    public function setPriority($priority);
}

