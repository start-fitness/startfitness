<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface CourriersInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const NAME = 'name';
    const METHOD = 'method';
    const COURRIERS_ID = 'courriers_id';
    const TYPE = 'type';

    /**
     * Get courriers_id
     * @return string|null
     */
    public function getCourriersId();

    /**
     * Set courriers_id
     * @param string $courriersId
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setCourriersId($courriersId);

    /**
     * Get name
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     * @param string $name
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setName($name);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\CourriersExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\CourriersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\CourriersExtensionInterface $extensionAttributes
    );

    /**
     * Get method
     * @return string|null
     */
    public function getMethod();

    /**
     * Set method
     * @param string $method
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setMethod($method);

    /**
     * Get type
     * @return string|null
     */
    public function getType();

    /**
     * Set type
     * @param string $type
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     */
    public function setType($type);
}

