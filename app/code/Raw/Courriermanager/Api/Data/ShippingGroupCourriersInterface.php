<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface ShippingGroupCourriersInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const GROUP_ID = 'group_id';
    const COURRIER_ID = 'courrier_id';
    const SHIPPING_GROUP_COURRIERS_ID = 'shipping_group_courriers_id';

    /**
     * Get shipping_group_courriers_id
     * @return string|null
     */
    public function getShippingGroupCourriersId();

    /**
     * Set shipping_group_courriers_id
     * @param string $shippingGroupCourriersId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setShippingGroupCourriersId($shippingGroupCourriersId);

    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId();

    /**
     * Set group_id
     * @param string $groupId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setGroupId($groupId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\ShippingGroupCourriersExtensionInterface $extensionAttributes
    );

    /**
     * Get courrier_id
     * @return string|null
     */
    public function getCourrierId();

    /**
     * Set courrier_id
     * @param string $courrierId
     * @return \Raw\Courriermanager\Api\Data\ShippingGroupCourriersInterface
     */
    public function setCourrierId($courrierId);
}

