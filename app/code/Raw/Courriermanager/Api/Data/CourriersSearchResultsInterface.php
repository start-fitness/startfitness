<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface CourriersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get courriers list.
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface[]
     */
    public function getItems();

    /**
     * Set name list.
     * @param \Raw\Courriermanager\Api\Data\CourriersInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

