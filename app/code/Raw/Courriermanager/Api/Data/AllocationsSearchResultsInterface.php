<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface AllocationsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get allocations list.
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface[]
     */
    public function getItems();

    /**
     * Set order_id list.
     * @param \Raw\Courriermanager\Api\Data\AllocationsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

