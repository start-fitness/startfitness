<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api\Data;

interface MethodGroupInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const GROUP_ID = 'group_id';
    const METHOD_GROUP_ID = 'method_group_id';
    const METHOD_ID = 'method_id';

    /**
     * Get method_group_id
     * @return string|null
     */
    public function getMethodGroupId();

    /**
     * Set method_group_id
     * @param string $methodGroupId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setMethodGroupId($methodGroupId);

    /**
     * Get method_id
     * @return string|null
     */
    public function getMethodId();

    /**
     * Set method_id
     * @param string $methodId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setMethodId($methodId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Raw\Courriermanager\Api\Data\MethodGroupExtensionInterface $extensionAttributes
    );

    /**
     * Get group_id
     * @return string|null
     */
    public function getGroupId();

    /**
     * Set group_id
     * @param string $groupId
     * @return \Raw\Courriermanager\Api\Data\MethodGroupInterface
     */
    public function setGroupId($groupId);
}

