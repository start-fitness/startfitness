<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface AllocationsRepositoryInterface
{

    /**
     * Save allocations
     * @param \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
    );

    /**
     * Retrieve allocations
     * @param string $allocationsId
     * @return \Raw\Courriermanager\Api\Data\AllocationsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($allocationsId);

    /**
     * Retrieve allocations matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\AllocationsSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete allocations
     * @param \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\AllocationsInterface $allocations
    );

    /**
     * Delete allocations by ID
     * @param string $allocationsId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($allocationsId);
}

