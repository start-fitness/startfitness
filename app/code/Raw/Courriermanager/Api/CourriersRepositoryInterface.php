<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface CourriersRepositoryInterface
{

    /**
     * Save courriers
     * @param \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
    );

    /**
     * Retrieve courriers
     * @param string $courriersId
     * @return \Raw\Courriermanager\Api\Data\CourriersInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($courriersId);

    /**
     * Retrieve courriers matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Raw\Courriermanager\Api\Data\CourriersSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete courriers
     * @param \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Raw\Courriermanager\Api\Data\CourriersInterface $courriers
    );

    /**
     * Delete courriers by ID
     * @param string $courriersId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($courriersId);
}

