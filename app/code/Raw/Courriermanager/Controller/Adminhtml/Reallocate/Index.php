<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Controller\Adminhtml\Reallocate;

class Index extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Raw\Courriermanager\Helper\Allocate $allocate,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Framework\Controller\ResultFactory $resultFactory,
		\Magento\Framework\Message\ManagerInterface $message
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->_allocate = $allocate;
		$this->resultFactory = $resultFactory;
		$this->request = $request;
		$this->message = $message;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       $id = $this->request->getParam('id');
	   if(!$this->_allocate->checkOrderInMetapack($id)){
			$allocated = $this->_allocate->allocateOrder($id);
			if(!$allocated){
				$this->messageManager->addError('Could not re-allocate, please see logs for more details.');
			}
			else{
				$this->messageManager->addSuccess("Reallocation completed !");
			}
	   }
	   else{
			$this->messageManager->addError('A metapack consignement already exists for this order.');
	   }
	   $resultRedirect = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_REDIRECT);

        // Your code

        $resultRedirect->setUrl($this->_redirect->getRefererUrl());
        return $resultRedirect;
    }
}

