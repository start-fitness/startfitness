<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Plugin;

class Orderview
{

    public function beforeSetLayout(\Magento\Sales\Block\Adminhtml\Order\View $view)
	{
		$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$_urlBuilder= $objectManager->create('\Magento\Framework\UrlInterface');
		$message ='Are you sure you want to do this?';
		$url = '/admin/courriermanager/reallocate/index/id/' . $view->getOrderId();
		
		$url = $_urlBuilder->getUrl(
            'courriermanager/reallocate/index',
            ['id' => $view->getOrderId()]
        );

		$view->addButton(
			'order_myaction',
			[
				'label' => __('Re-allocate'),
				'class' => 'myclass',
				'onclick' => "confirmSetLocation('{$message}', '{$url}')"
			]
		);


	}
}
