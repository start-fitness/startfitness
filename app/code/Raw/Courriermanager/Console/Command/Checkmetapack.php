<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Courriermanager\Console\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Checkmetapack extends Command
{


	public function __construct(\Raw\Courriermanager\Helper\Allocate $allocate, string $name = null)
    {
		$this->allocate = $allocate;
        parent::__construct($name);
    }
	
    const NAME_ARGUMENT = "name";
    const NAME_OPTION = "option";

    /**
     * {@inheritdoc}
     */
    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ) {
        $this->allocate->checkOrders();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName("raw_courriermanager:checkmetapack");
        $this->setDescription("check metapack for updates");
        $this->setDefinition([
            new InputArgument(self::NAME_ARGUMENT, InputArgument::OPTIONAL, "Name"),
            new InputOption(self::NAME_OPTION, "-a", InputOption::VALUE_NONE, "Option functionality")
        ]);
        parent::configure();
    }
}

