# Mage2 Module Raw Courriermanager

    ``raw/module-courriermanager``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Raw`
 - Enable the module by running `php bin/magento module:enable Raw_Courriermanager`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require raw/module-courriermanager`
 - enable the module by running `php bin/magento module:enable Raw_Courriermanager`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - Label creation (courrier/settings/enable)


## Specifications

 - Model
	- courriers

 - Model
	- courrier_rules

 - Console Command
	- allocatemethod

 - Console Command
	- generatelabel


## Attributes



