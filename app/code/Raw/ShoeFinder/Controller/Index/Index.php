<?php

namespace Raw\ShoeFinder\Controller\Index;

use Magento\Framework\App\Action\Context;


class Index extends \Magento\Framework\App\Action\Action
{
    protected $_resultPageFactory;
    public function __construct(Context $context, \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->_resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
      if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && ( $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest' ) )
  		{
  			$this->filterAction();
  		}
      else {
        $resultPage = $this->_resultPageFactory->create();
        return $resultPage;
      }
    }

  public function filterAction() {
		if(empty($_GET["gender"])) {
			return;
		}

		$spikes = false;
		if(!empty($_GET["spikes"])) {
			$spikes = true;
		}

		/* If atribute is mens show mens category */
		$id = $_GET["gender"] == 98 ? 2528 : 2488;
		//if($spikes)
		//$id = $_GET["gender"] == 98 ? 2805 : 2805;
		//if($spikes){
		//	$id = $_GET['footwear_type'];
		//	unset($_GET['footwear_type']);
		//}

    $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $category = $_objectManager->create('Magento\Catalog\Model\Category')->load($id);
		$url = $category->getUrl($category)."?ajax=1";

    $connection = $this->_objectManager->create('\Magento\Framework\App\ResourceConnection');
    $conn = $connection->getConnection();

    $productCollection = $_objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection');
    $products = $productCollection->addAttributeToSelect('*')
    ->addFieldToFilter('type_id', 'configurable')
    //->addAttributeToFilter('visibility', \Magento\Catalog\Model\Product\Visibility::VISIBILITY_BOTH)
    ->addAttributeToFilter('status',\Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED)
    ->addCategoryFilter($category);

		// Add a filter for each parameter
		$x = 0;
    $redirect = 0;

		foreach ( $_GET as $attribute=>$value )
		{
			if(!empty($value)) {
				if($attribute == "price") {
					$price_range = explode("-", $value);
					$products->addAttributeToFilter('price',
					array('from' => $price_range[0], 'to' => $price_range[1]) );
				}

				else if($attribute == "gender"){
					$products->addAttributeToFilter($attribute, array('finset'=>array($value)));
				}

        else if($attribute == "footwear_type") {
         $products->addAttributeToFilter($attribute, array('finset'=>array($value)));
        }

				$url .= "&".$attribute."=".$value;
				$x++;
			}
			if(count($_GET) == $x){
				$redirect = 1;
			 }
		 } 

    if(!empty($_GET["uk_shoe_size"])) {
     $size = $_GET["uk_shoe_size"];
    }

    $count_prods = '';
    $products->addAttributeToSort('update_at', 'ASC');
    $products->load();
		$productList = array();

    foreach($products as $product) {
     if(!empty($_GET["uk_shoe_size"])) {
      $size = $_GET["uk_shoe_size"];
      $resource = $_objectManager->get('Magento\Framework\App\ResourceConnection');
 			$connection = $resource->getConnection();
 			$tableName = $resource->getTableName('catalog_product_index_eav'); //gives table name with prefix
      $query = "Select entity_id
      FROM catalog_product_index_eav
      join cataloginventory_stock_item on " . $tableName . ".source_id = cataloginventory_stock_item.product_id  where catalog_product_index_eav.store_id = 6 and catalog_product_index_eav.attribute_id = 266 and catalog_product_index_eav.entity_id =" . $product->getId() . " and value = ". $size ." and cataloginventory_stock_item.qty > 0";


    	$result = $connection->fetchAll($query);
      if(!empty($result)) {
        $productList[] =  $result[0]['entity_id'];
      }
    }
  }

  $count_prods = count($productList);

     $result = $this->_resultPageFactory->create();
     $template = $result->getLayout()->createBlock('Raw\SpikeFinder\Block\SpikeFinder')->setTemplate('Raw_SpikeFinder::products.phtml')->setData("productList",$productList)->toHtml();
     echo json_encode(array("products" => $count_prods,'data'=>$productList, 'template' => $template)); die;
  }
}
