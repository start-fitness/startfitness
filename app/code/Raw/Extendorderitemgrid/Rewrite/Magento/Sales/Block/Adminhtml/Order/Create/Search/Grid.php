<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Extendorderitemgrid\Rewrite\Magento\Sales\Block\Adminhtml\Order\Create\Search;
use Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\DataProvider\ProductCollection;
use Magento\Framework\App\ObjectManager;
use Magento\Backend\Block\Widget\Grid\Extended;

class Grid extends \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid
{

	/**
     * Sales config
     *
     * @var \Magento\Sales\Model\Config
     */
    protected $_salesConfig;

    /**
     * Session quote
     *
     * @var \Magento\Backend\Model\Session\Quote
     */
    protected $_sessionQuote;

    /**
     * Catalog config
     *
     * @var \Magento\Catalog\Model\Config
     */
    protected $_catalogConfig;

    /**
     * Product factory
     *
     * @var \Magento\Catalog\Model\ProductFactory
     */
    protected $_productFactory;

    /**
     * @var ProductCollection $productCollectionProvider
     */
    private $productCollectionProvider;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data $backendHelper
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Catalog\Model\Config $catalogConfig
     * @param \Magento\Backend\Model\Session\Quote $sessionQuote
     * @param \Magento\Sales\Model\Config $salesConfig
     * @param array $data
     * @param ProductCollection|null $productCollectionProvider
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Catalog\Model\Config $catalogConfig,
        \Magento\Backend\Model\Session\Quote $sessionQuote,
        \Magento\Sales\Model\Config $salesConfig,
        array $data = [],
        ProductCollection $productCollectionProvider = null
    ) {
        $this->_productFactory = $productFactory;
        $this->_catalogConfig = $catalogConfig;
        $this->_sessionQuote = $sessionQuote;
        $this->_salesConfig = $salesConfig;
        $this->productCollectionProvider = $productCollectionProvider
            ?: ObjectManager::getInstance()->get(ProductCollection::class);
        parent::__construct($context, $backendHelper,$productFactory, $catalogConfig, $sessionQuote, $salesConfig, $data, $productCollectionProvider);
    }

	/**
     * Add column filter to collection
     *
     * @param \Magento\Backend\Block\Widget\Grid\Column $column
     * @return $this
     */
    protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {
            $productIds = $this->_getSelectedProducts();
            if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', ['in' => $productIds]);
            } else {
                if ($productIds) {
                    $this->getCollection()->addFieldToFilter('entity_id', ['nin' => $productIds]);
                }
            }
        }
				else if($column->getId() == 'aqty'){
			$condition = $column->getFilter()->getCondition();
			$count_new = array_count_values($condition);
			$count_2 = count($count_new);
			if($count_2 == 2) {
							$this->getCollection()->getSelect()->where("stock.qty >= ? AND stock.qty <= ?", $condition['from'], $condition['to']);
					}
					else {
						if(array_key_exists('from', $condition) == true) {
							$this->getCollection()->getSelect()->where("stock.qty >= ?", $condition['from']);

						}
						else if(array_key_exists('to', $condition) == true) {
							$this->getCollection()->getSelect()->where("stock.qty <= ?", $condition['to']);

						}
					}

				}

		else if($column->getId() == 'actual_model'){
			$condition = $column->getFilter()->getCondition();
			$this->getCollection()->getSelect()->where("eavvarchar.value LIKE ".$condition['like']->__toString());
			//var_dump($condition['like'],$this->getCollection()->getSelect()."");
			//exit;
		}
		else {
            parent::_addColumnFilterToCollection($column);
        }
        return $this;
    }

	/**
     * Prepare collection to be displayed in the grid
     *
     * @return $this
     */
    protected function _prepareCollection()
    {



        $attributes = $this->_catalogConfig->getProductAttributes();
        $store = $this->getStore();

        /* @var $collection \Magento\Catalog\Model\ResourceModel\Product\Collection */
        $collection = $this->productCollectionProvider->getCollectionForStore($store);
        $collection->addAttributeToSelect(
            $attributes
        );
        $collection->addAttributeToFilter(
            'type_id',
            $this->_salesConfig->getAvailableProductTypes()
        );


		//$collection = $this->getCollection();
		$collection->getSelect()->joinLeft(
             ['stock'=>'cataloginventory_stock_status'],
             'e.entity_id = stock.product_id AND stock.website_id = 0',
             ['aqty' => 'stock.qty']
            );

		$collection->getSelect()->joinLeft(
             ['eavtable'=>'eav_attribute'],
             'eavtable.attribute_code = "actual_model"',
             []
            );
		$collection->getSelect()->joinLeft(
             ['eavvarchar'=>'catalog_product_entity_varchar'],
             'eavvarchar.attribute_id = eavtable.attribute_id AND eavvarchar.entity_id = e.entity_id AND eavvarchar.store_id = 0',
             ["actual_model"=> "eavvarchar.value"]
            );
		//echo $collection->getSelect()."";exit;
		//$collection->addFieldToSelect("actual_model");
		$collection->addFilterToMap('stock_qty', 'stock.qty');
		$collection->addFilterToMap('actual_model_f', 'eavvarchar.value');
		$collection->addFilterToMap('stock_qty', 'stock.qty', "attributes");
		$collection->addFilterToMap('actual_model_f', 'eavvarchar.value', "attributes");
        $this->setCollection($collection);
		Extended::_prepareCollection();
        return $this;
    }

    /**
     * Prepare columns
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
		$this->addColumn(
            'entity_id',
            [
                'header' => __('ID'),
                'sortable' => true,
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
                'index' => 'entity_id'
            ]
        );
        $this->addColumn(
            'name',
            [
                'header' => __('Product'),
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Product::class,
                'index' => 'name'
            ]
        );
		$this->addColumn(
            'actual_model',
            [
                'header' => __('Model'),
                //'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Product::class,
                'index' => 'actual_model',
				'type'=>'text',
				'filter_index' => 'actual_model_f'
            ]
        );
        $this->addColumn('sku', ['header' => __('SKU'), 'index' => 'sku']);
        $this->addColumn(
            'price',
            [
                'header' => __('Price'),
                'column_css_class' => 'price',
                'type' => 'currency',
                'currency_code' => $this->getStore()->getCurrentCurrencyCode(),
                'rate' => $this->getStore()->getBaseCurrency()->getRate($this->getStore()->getCurrentCurrencyCode()),
                'index' => 'price',
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Price::class
            ]
        );

        $this->addColumn(
            'in_products',
            [
                'header' => __('Select'),
                'type' => 'checkbox',
                'name' => 'in_products',
                'values' => $this->_getSelectedProducts(),
                'index' => 'entity_id',
                'sortable' => false,
                'header_css_class' => 'col-select',
                'column_css_class' => 'col-select'
            ]
        );
		$this->addColumn(
            'aqty',
            [
                'header' => __('Stock'),
                //'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Product::class,
                'index' => 'aqty',
				'type'=>'number',
				'filter_index' => 'stock_qty'
            ]
        );
        $this->addColumn(
            'qty',
            [
                'filter' => false,
                'sortable' => false,
                'header' => __('Quantity'),
                'renderer' => \Magento\Sales\Block\Adminhtml\Order\Create\Search\Grid\Renderer\Qty::class,
                'name' => 'qty',
                'inline_css' => 'qty',
                'type' => 'input',
                'validate_class' => 'validate-number',
                'index' => 'qty'
            ]
        );
        Extended::_prepareColumns();






        return $this;
    }
}
