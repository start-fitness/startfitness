<?php


namespace Raw\CycleScheme\Model\Payment;

/**
 * Class CycleScheme
 *
 * @package Raw\CycleScheme\Model\Payment
 */
class CycleScheme extends \Magento\Payment\Model\Method\AbstractMethod
{

	protected $_formBlockType = \Raw\CycleScheme\Block\Adminhtml\Form\C2w::class;
    protected $_code = "cyclescheme";
    protected $_isOffline = true;

    public function isAvailable(
        \Magento\Quote\Api\Data\CartInterface $quote = null
    ) {

			$bike_sale = array();
			$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
			$attributeSet = $objectManager->create('Magento\Eav\Api\AttributeSetRepositoryInterface');
			$itemsCollection = $quote->getItemsCollection();
			$items = $quote->getAllVisibleItems();
			$billingAddress = $quote->getBillingAddress();
			$shippingAddress = $quote->getShippingAddress();
			$discount = $quote->getAppliedRuleIds();

			$countryCode = $shippingAddress->getData('country_id');

			if($countryCode != "GB") return false;

			if($discount > 0) return false;



			foreach($items as $item) {
				$product = $objectManager->get('Magento\Catalog\Model\Product')->load($item->getProductId());
				$attributeSetRepository = $attributeSet->get($product->getAttributeSetId());
				$attributeSetName = $attributeSetRepository->getAttributeSetName();
				if($product->getIs0Intrest() == 0) {
					return false;
				}
			 if("Migration_StartBikes" == $attributeSetName || "Migration_CycleFrameset" == $attributeSetName || "Migration_CycleWheels" == $attributeSetName) {
				 $bike_sale[] = $product->getName();
			}
			}

			$result = array_unique($bike_sale);
			if(count($result) < 1) {
				return false;
			}
        return parent::isAvailable($quote);
    }
}
