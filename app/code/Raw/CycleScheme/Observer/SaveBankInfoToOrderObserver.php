<?php
namespace Raw\CycleScheme\Observer;
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\OfflinePayments\Model\Banktransfer;
class SaveBankInfoToOrderObserver implements ObserverInterface {
 protected $_inputParamsResolver;
 protected $_quoteRepository;
 protected $logger;
 protected $_state;
 public function __construct(\Magento\Webapi\Controller\Rest\InputParamsResolver $inputParamsResolver, \Magento\Quote\Model\QuoteRepository $quoteRepository, \Psr\Log\LoggerInterface $logger,\Magento\Framework\App\State $state) {
     $this->_inputParamsResolver = $inputParamsResolver;
     $this->_quoteRepository = $quoteRepository;
     $this->logger = $logger;
     $this->_state = $state;
 }
 public function execute(EventObserver $observer) {
     try{
     if($this->_state->getAreaCode() != \Magento\Framework\App\Area::AREA_ADMINHTML){
	 $inputParams = $this->_inputParamsResolver->resolve();
     foreach ($inputParams as $inputParam) {
         if ($inputParam instanceof \Magento\Quote\Model\Quote\Payment) {
             $paymentData = $inputParam->getData('additional_data');
             $paymentOrder = $observer->getEvent()->getPayment();
             $order = $paymentOrder->getOrder();
             $quote = $this->_quoteRepository->get($order->getQuoteId());
             $paymentQuote = $quote->getPayment();
             $method = $paymentQuote->getMethodInstance()->getCode();

          //  echo  $method; exit;
             //if ($method == Banktransfer::PAYMENT_METHOD_BANKTRANSFER_CODE) {
                 if(isset($paymentData['redemption'])){
                 $paymentQuote->setData('redemption', $paymentData['redemption']);
                 $paymentOrder->setData('redemption', $paymentData['redemption']);
                 }
                 if(isset($paymentData['certificate'])){
                 $paymentQuote->setData('certificate', $paymentData['certificate']);
                 $paymentOrder->setData('certificate', $paymentData['certificate']);
                 }

          //   }
         }
     }
    }
	else{
		if(isset($_POST['payment']) && $_POST['payment']['method'] == "cyclescheme") {
			
			$paymentOrder = $observer->getEvent()->getPayment();
            $order = $paymentOrder->getOrder();
            $quote = $this->_quoteRepository->get($order->getQuoteId());
            $paymentQuote = $quote->getPayment();
			if(isset($_POST['payment']['redemption'])){
            $paymentQuote->setData('redemption', $_POST['payment']['redemption']);
			$paymentOrder->setData('redemption', $_POST['payment']['redemption']);
			}
			if(isset($_POST['payment']['certificate'])){
			$paymentQuote->setData('certificate', $_POST['payment']['certificate']);
			$paymentOrder->setData('certificate', $_POST['payment']['certificate']);
			}
			//var_dump($_POST['payment'], $paymentOrder->getData('certificate'));
			//exit;
			
		}
		
		
	}
   }catch(\Exception $e){}
 }
}

?>
