<?php
namespace Raw\CycleScheme\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'certificate',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Certificate',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'certificate',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Certificate',
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('quote_payment'),
            'redemption',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Redemption',
            ]
        );
        $setup->getConnection()->addColumn(
            $setup->getTable('sales_order_payment'),
            'redemption',
            [
                'type' => 'text',
                'nullable' => true  ,
                'comment' => 'Redemption',
            ]
        );
        $setup->endSetup();
  }
}

?>
