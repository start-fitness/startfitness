define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'cyclescheme',
                component: 'Raw_CycleScheme/js/view/payment/method-renderer/cyclescheme-method'
            }
        );
        return Component.extend({});
    }
);