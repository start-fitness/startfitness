define(
    [
        'Magento_Checkout/js/view/payment/default',
        'jquery'
    ],
    function (Component,$) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Raw_CycleScheme/payment/cyclescheme'
            },
            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                        'redemption': $('#cyclescheme_redemption').val(),
                        'certificate': $('#cyclescheme_certificate').val()
                    }
                };
            },
            getMailingAddress: function () {
                return window.checkoutConfig.payment.checkmo.mailingAddress;
            },
            getInstructions: function () {
                return window.checkoutConfig.payment.instructions[this.item.method];
            },
        });
    }
);
