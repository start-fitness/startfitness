<?php declare(strict_types=1);


namespace Raw\OrderReturns\Api;


interface ScanreturnsManagementInterface
{

    /**
     * GET for scanreturns api
     * @param string $param
     * @return string
     */
    public function getScanreturns($param);
}

