<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * Created By : Rohan Hapani
 */
namespace Raw\OrderReturns\Block\Adminhtml\Expressorders;

class Grid extends \Magento\Backend\Block\Widget\Grid\Extended
{

    /**
     * @var \Rh\Blog\Model\Status
     */
    protected $_status;

    /**
     * @var \Rh\Blog\Model\BlogFactory
     */
    protected $_blogFactory;

    /**
     * @var \Magento\Framework\Module\Manager
     */
    protected $moduleManager;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Backend\Helper\Data            $backendHelper
     * @param \Rh\Blog\Model\BlogFactory              $blogFactory
     * @param \Rh\Blog\Model\Status                   $status
     * @param \Magento\Framework\Module\Manager       $moduleManager
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Backend\Helper\Data $backendHelper,
        \Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
        \Magento\Framework\Module\Manager $moduleManager,
        array $data = []
    ) {
		
        $this->_orderCollectionFactory = $orderCollectionFactory;
        $this->moduleManager = $moduleManager;
        parent::__construct($context, $backendHelper, $data);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('gridGrid');
        $this->setDefaultSort('entity_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(false);
        $this->setVarNameFilter('grid_record');
    }

    /**
     * @return $this
     */
    protected function _prepareCollection()
    {
		
        $collection = $this->_orderCollectionFactory->create();
		$collection->join(array('a' => 'sales_order_address'), 'main_table.entity_id = a.parent_id AND a.address_type != \'billing\'', array(
                'city'       => 'city',
                'country_id' => 'country_id'
            ))
            ->join(array('c' => 'customer_group'), 'main_table.customer_group_id = c.customer_group_id', array(
                'customer_group_code' => 'customer_group_code'
            ))
            ->addExpressionFieldToSelect(
                'fullname',
                'CONCAT({{customer_firstname}}, \' \', {{customer_lastname}})',
                array('customer_firstname' => 'main_table.customer_firstname', 'customer_lastname' => 'main_table.customer_lastname'))
            ->addExpressionFieldToSelect(
                'products',
                '(SELECT GROUP_CONCAT(\' \', x.name)
                    FROM sales_order_item x
                    WHERE {{entity_id}} = x.order_id
                        AND x.product_type != \'configurable\')',
                array('entity_id' => 'main_table.entity_id')
            )
        ;
		
		$collection->addAttributeToFilter("status",  ['in' => ['processing', 'pick_note_printed']]);
		
	    $collection->addFilterToMap('store_id', 'main_table.store_id');
		
		$collection->getSelect()->where("
                        main_table.shipping_description LIKE 'Delivery Options - Next Working Day (See T&C)' OR
                        main_table.shipping_description LIKE '%Amazon Shipping - Exp%' OR
                        main_table.shipping_description LIKE '%saturday%' OR
                        main_table.shipping_description LIKE '%sunday%' OR
                        main_table.shipping_description LIKE '%Delivery Options - Express Delivery%' OR
                        main_table.shipping_description LIKE '%Special Delivery%' OR
                        (main_table.shipping_description LIKE 'SHIPPING PROMOTION - Free' AND main_table.customer_group_id <> '6' AND main_table.grand_total > 0) OR
                        main_table.shipping_description LIKE '%express%' OR
                        main_table.shipping_description LIKE '%dhl%' OR
                        main_table.shipping_description LIKE '%second%' OR
                        main_table.shipping_description LIKE '%next%' OR
                        main_table.shipping_description LIKE '%dpd next day%' OR
                        main_table.shipping_description LIKE '%expedited%' OR
                        main_table.shipping_description LIKE '%Other 24%'"


            );
	    $select = $collection->getSelect();
		
		//echo $select."";exit;
		//if(strpos(($select.""), "histjn") === false){
	    
		
        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    /**
     * @return $this
     */
    protected function _prepareColumns()
    {

        $this->addColumn(
            'increment_id',
            [
                'header' => __('Order #'),
                'index' => 'increment_id',
                'header_css_class' => 'col-id',
                'column_css_class' => 'col-id',
				'type' => 'text',
				//'filter' => true,
            ]
        );

        $this->addColumn('purchased_on', array(
            'header' => __('Purchased On'),
            'type'   => 'datetime',
			'filter' => false,
            'index'  => 'created_at'
        ));
 
        $this->addColumn('products', array(
            'header'       => __('Products Purchased'),
            'index'        => 'products',
			'filter' => false,
            'filter_index' => '(SELECT GROUP_CONCAT(\' \', x.name) FROM sales_flat_order_item x WHERE main_table.entity_id = x.order_id AND x.product_type != \'configurable\')'
        ));
 
        $this->addColumn('fullname', array(
            'header'       => __('Name'),
            'index'        => 'fullname',
			//'filter' => false,
            'filter_index' => 'CONCAT(customer_firstname, \' \', customer_lastname)'
        ));
		
		$this->addColumn('shipping_description', array(
            'header'       => __('Shipping Desc'),
            'index'        => 'shipping_description',
			'type' => "text"
			//'filter' => false,
            
        ));
 
        /*$this->addColumn('city', array(
            'header' => __('City'),
            'index'  => 'city'
        ));*/
 
        $this->addColumn('country', array(
            'header'   => __('Country'),
            'index'    => 'country_id',
			//'filter' => false,
            //'renderer' => 'adminhtml/widget_grid_column_renderer_country'
        ));
 
        /*$this->addColumn('customer_group', array(
            'header' => __('Customer Group'),
            'index'  => 'customer_group_code'
        ));*/
 
        $this->addColumn('grand_total', array(
            'header'        => __('Grand Total'),
            'index'         => 'base_grand_total',
            'type'          => 'currency',
			//'filter' => false,
            'currency_code' => "GBP"
        ));
 
        /*$this->addColumn('shipping_method', array(
            'header' => __('Shipping Method'),
            'index'  => 'shipping_description'
        ));*/
 
        $this->addColumn('order_status', array(
            'header'  => __('Status'),
            'index'   => 'status',
			//'filter' => false,
            'type'    => 'text',
            //'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn(
            'view',
            [
                'header' => __('View order'),
                'type' => 'action',
                'getter' => 'getId',
                'actions' => [
                    [
                        'caption' => __('View Order'),
                        'url' => [
                            'base' => 'sales/order/view',
                        ],
                        'field' => 'order_id',
                    ],
                ],
                'filter' => false,
                'sortable' => false,
                'index' => 'stores',
                'header_css_class' => 'col-action',
                'column_css_class' => 'col-action',
            ]
        );

        

        $block = $this->getLayout()->getBlock('grid.bottom.links');

        if ($block) {
            $this->setChild('grid.bottom.links', $block);
        }

        return parent::_prepareColumns();
    }

    /**
     * @return $this
     */
    protected function _prepareMassaction()
    {
       

        return $this;
    }

    /**
     * @return string
     */
    /*public function getGridUrl()
    {
        return $this->getUrl('raw/expressorders/index', ['_current' => true]);
    }
	*/
    /**
     * @return string
     */
	 /*
    public function getRowUrl($row)
    {
        return $this->getUrl('raw/expressorders/index', ['id' => $row->getId()]);
    }
	*/
}