<?php declare(strict_types=1);


namespace Raw\OrderReturns\Block\Adminhtml\Orderreturns;


class Index extends \Magento\Backend\Block\Widget\Container
{
	/**
     * @var string
     */
    protected $_template = 'grid/view.phtml';
    
	    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
	
	    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        
        $this->setChild('grid', $this->getLayout()->createBlock('Raw\OrderReturns\Block\Adminhtml\Orderreturns\Grid', 'grid.view.grid'));
		
        return parent::_prepareLayout();
    }
    /**
     * @return array
     */
    protected function _getAddButtonOptions()
    {
        
    }
    /**
     * @return string
     */
    protected function _getCreateUrl()
    {
        return $this->getUrl('blog/*/new');
    }
    /**
     * @return string
     */
    public function getGridHtml()
    {
        return $this->getChildHtml('grid');
    }

}

