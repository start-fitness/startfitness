# Mage2 Module Raw OrderReturns

    ``raw/module-orderreturns``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities
Quick attribute updated on configurable + batch price updater

## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Raw`
 - Enable the module by running `php bin/magento module:enable Raw_OrderReturns`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require raw/module-orderreturns`
 - enable the module by running `php bin/magento module:enable Raw_OrderReturns`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - API Endpoint
	- POST - Raw\OrderReturns\Api\ScanreturnsManagementInterface > Raw\OrderReturns\Model\ScanreturnsManagement

 - Controller
	- adminhtml > raw/orderreturns/index


## Attributes



