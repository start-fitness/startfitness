<?php
namespace Raw\Custompayment\Observer;
use Magento\Framework\Event\ObserverInterface;

class PaymentMethodAvailable implements ObserverInterface
{

public function execute(\Magento\Framework\Event\Observer $observer)
{
    if($this->isadmin()){
        return;
    }
    // you can replace "purchaseorder" with your required payment method code
    if($observer->getEvent()->getMethodInstance()->getCode()=="purchaseorder"){
        $checkResult = $observer->getEvent()->getResult();
        $checkResult->setData('is_available', false); //this is disabling the payment method at checkout page
    }
}

function isadmin($store = null) {
    /** @var \Magento\Framework\ObjectManagerInterface $om */
    $om = \Magento\Framework\App\ObjectManager::getInstance();
    /** @var \Magento\Framework\App\State $state */
    $state =  $om->get('Magento\Framework\App\State');
    return 'adminhtml' === $state->getAreaCode();
}
}
