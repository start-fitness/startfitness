define([
    'jquery',
    'Magento_Checkout/js/action/get-totals',
    'Magento_Customer/js/customer-data'
], function ($, getTotalsAction, customerData) {


/*function tick_option() {
    //  alert('ohhh');
      var first_op = $('.choice:first input').attr('id');
      $('.choice:first input').trigger('click');


}

$(document).ready(function(){
  //alert('here');
  var work=setInterval(tick_option,100);

  if(!$("#block-shipping .radio:checked").val()){
  //  alert('test2');
      tick_option();
  }
    else {
      clearInterval(work);
    }

 });
 */


        $(document).on('change', 'input[name$="[qty]"]', function(){
          //alert('here2');
            var form = $('form#form-validate');
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                showLoader: true,
                success: function (res) {
                    var parsedResponse = $.parseHTML(res);
                    var result = $(parsedResponse).find("#form-validate");
                    var sections = ['cart'];

                    $("#form-validate").replaceWith(result);

                    /* This is for reloading the minicart */
                    customerData.reload(sections, true);

                    /* This is for reloading the totals summary  */
                    var deferred = $.Deferred();
                    getTotalsAction([], deferred);
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        });

        $(document).on('click', '.qty-change', function(){


        //  alert('here3');


          var amount = $(this).data("value");


          if(typeof amount != "undefined" && amount != "") {
            //var current = parseInt($("#qty").val());
            var current = parseInt($(this).siblings('.qty').val());


            var new_amount = current + amount;
            if(new_amount >= 0) {
              $(this).siblings('.qty').val(new_amount);
            if(new_amount == 0) {
              location.reload();
            }
            }
          }

            var form = $('form#form-validate');
            $.ajax({
                url: form.attr('action'),
                data: form.serialize(),
                showLoader: true,
                success: function (res) {
                    var parsedResponse = $.parseHTML(res);
                    var result = $(parsedResponse).find("#form-validate");
                    var sections = ['cart'];

                    $("#form-validate").replaceWith(result);

                    /* This is for reloading the minicart */
                    customerData.reload(sections, true);

                    /* This is for reloading the totals summary  */
                    var deferred = $.Deferred();
                    getTotalsAction([], deferred);
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    console.log(err.Message);
                }
            });
        });
    });
