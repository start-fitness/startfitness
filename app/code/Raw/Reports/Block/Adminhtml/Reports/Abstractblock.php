<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Abstractblock extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		
        array $data = []
    ) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_productInterface = $product;
        parent::__construct($context, $data);
    }
	protected function getConnection()
	{
		//if (!$this->connection) {
			$this->connection = $this->_resource->getConnection('core_write');
		//}

		return $this->connection;
	}

	public function getStoreData(){
		$stores = $this->_storeManager->getStores(true, true);
		return $stores;
	}
	
	public function getAllData(){
		return $this->_request->getParams();
	}
	public function getPostValue($value){
		
		$data = $this->getAllData();
		return (isset($data[$value])) ? $data[$value] : null;
		
	}
	
	public function getBySku($sku){
		
		return $this->_productInterface->get($sku);
		
	}
	
	public function getCss(){
		ob_start();
		?>
		<style>
		.content-header {
			margin-bottom: 18px;
			border-bottom: 4px solid #dfdfdf;
			padding-bottom: .25em;
		}
		.messages li {
			min-height: 23px !important;
			margin-bottom: 11px !important;
			padding: 8px 8px 2px 32px !important;
			font-size: .95em !important;
			font-weight: bold !important;
		}
		.notice-msg {
			border: 1px solid #ffd967 !important;
			background: #fffbf0 url(images/note_msg_icon.gif) no-repeat 10px 10px !important;
			color: #3d6611 !important;
		}
		.switcher {
			margin-bottom: 10px;
			border: 1px solid #cddddd;
			background: #e7efef;
			padding: 10px;
		}
		table.actions {
			width: 100%;
			margin: .5em 0;
		}
		div.grid {
			width: 100% !important;
		}
		.grid table {
			width: 100%;
			border: 1px solid #cbd3d4;
				border-bottom-color: rgb(203, 211, 212);
				border-bottom-style: solid;
				border-bottom-width: 1px;
			border-bottom: none;
		}
		.grid tr.headings th.no-link {
			padding-top: 2px;
			padding-bottom: 1px;
			color: #67767e;
		}
		.grid tr.headings th {
			border-width: 1px;
			border-color: #f9f9f9 #d1cfcf #f9f9f9 #f9f9f9;
			border-style: solid;
			padding-top: 1px;
			padding-bottom: 0;
			font-size: .9em;
		}
		.grid th {
			white-space: nowrap;
		}
		.grid th, .grid td {
			padding: 2px 4px 2px 4px;
				padding-top: 2px;
				padding-bottom: 2px;
		}
		.grid tbody {
			background: #fff;
		}
		.grid tr.even, .grid tr.even tr {
			background: #f6f6f6;
		}
		.grid table td {
			border-width: 0 1px 1px 0;
			border-color: #dadfe0;
			border-style: solid;
		}
		.grid th, .grid td {
			padding: 2px 4px 2px 4px;
		}
		.grid table td {
			border-width: 0 1px 1px 0;
			border-color: #dadfe0;
			border-style: solid;
		}
		.grid th, .grid td {
			padding: 2px 4px 2px 4px;
		}
		.grid tr.headings th.no-link {
			padding-top: 2px;
			padding-bottom: 1px;
			color: #67767e;
		}
		.grid tr.headings th {
			border-width: 1px;
			border-color: #f9f9f9 #d1cfcf #f9f9f9 #f9f9f9;
			border-style: solid;
			padding-top: 1px;
			padding-bottom: 0;
			font-size: .9em;
		}
		.grid th {
			white-space: nowrap;
		}
		.grid th, .grid td {
			padding: 2px 4px 2px 4px;
				padding-top: 2px;
				padding-bottom: 2px;
		}
		.grid tr.headings {
			background: #cddddd;
		}
		.customm1grid button:hover {
			background: #f77c16 url(images/btn_over_bg.gif) repeat-x 0 0;
		}
		.customm1grid button, .form-button {
			border-width: 1px;
			border-style: solid;
			border-color: #ed6502 #a04300 #a04300 #ed6502;
			padding: 1px 7px 2px 7px;
			background: #ffac47 url(images/btn_bg.gif) repeat-x 0 100%;
			color: #fff;
			font: bold 12px arial, helvetica, sans-serif;
			cursor: pointer;
			text-align: center !important;
			white-space: nowrap;
		}
		.f-left, .left {
			float: left;
		}
		table.actions td {
			vertical-align: top;
		}
		.a-left {
			text-align: left !important;
		}
		</style>
		<?php
		$css = ob_get_clean();
		return $css;
	}
	
	
	
}

