<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Returneditemsreport extends Abstractblock
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		
        array $data = []
    ) {
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_productInterface = $product;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	
	public function getReportData($store_id= null){
		$postData = $this->getAllData();
		if(empty($postData)){
			return array();
		}
		elseif(!isset($postData['report_from']) || !isset($postData['report_to'])){
			return array();
		}
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		} 
		if (isset($postData['report_from'])){
			$data['report_from'] = date("Y-m-d", strtotime($postData['report_from']));
		}	
		if (isset($postData['report_to'])){
			$data['report_to'] = date("Y-m-d", strtotime($postData['report_to']));
		} 		
		return $this->getDatas($data);
	}
	
	public function getDatas($data)
		{
			if($data['report_from'] == $data['report_to']){
				$date = new \DateTime($data['report_from']);
				$date->modify('-1 day');
				$data['report_from'] = $date->format('Y-m-d');
			}
			$data['report_from'] = $data['report_from'] . " 08:00:00";
			$data['report_to'] = $data['report_to'] . " 07:59:59";
			
			$creditmemo_collection = $this->getCreditMemos($data);
			$results_collection = array();
			foreach($creditmemo_collection as $key=>$value){
				$creditmemoitems_collection = $this->getCreditMemoItems($value['entity_id']);
				$value['creditmemo_items'] = $creditmemoitems_collection;
				$results_collection[] = $value;
			}

			return $results_collection;
		}

	function getCreditMemoItems($id){
		$readConnection = $this->getConnection();
		$sql = "SELECT DISTINCT `main_table`.name,`main_table`.sku,`main_table`.qty,`main_table`.price,`main_table`.row_total_incl_tax 
				FROM `sales_creditmemo_item` AS `main_table` 
				WHERE `main_table`.parent_id = " . $id .
			  " AND `main_table`.row_total_incl_tax IS NOT NULL";
			  
		$result = $readConnection->fetchAll($sql);
		return $result;
	}

	function getCreditMemos($data){
		$readConnection = $this->getConnection();
		$sql = "SELECT  `main_table`.`entity_id`, `main_table`.`increment_id` AS `order_increment_id`, `main_table`.`created_at` AS `returned_on`, 
						`main_table`.`subtotal_incl_tax`,`main_table`.`discount_amount`, `main_table`.`tax_amount`, `main_table`.`shipping_incl_tax`, `main_table`.`grand_total`, 
						`main_table`.`subtotal_incl_tax`, (main_table.subtotal_incl_tax - main_table.subtotal) AS `item_tax` , `a`.`store_name`, `a`.`created_at` AS `purchased_on`, CONCAT(COALESCE(b.firstname, ''), ' ', COALESCE(b.lastname, '')) AS `shipto_name`, 
						`b`.`city` AS `shipto_city`, `b`.`region` AS `shipto_state`, `b`.`country_id`, `b`.`country_id` AS `region_code`, CONCAT(COALESCE(c.firstname, ''), ' ', 
						COALESCE(c.lastname, '')) AS `billto_name` 
				FROM `sales_creditmemo` AS `main_table` 
				JOIN `sales_order` AS `a` ON main_table.order_id = a.entity_id 
				JOIN `sales_order_address` AS `b` ON main_table.shipping_address_id = b.entity_id 
				JOIN `sales_order_address` AS `c` ON main_table.billing_address_id = c.entity_id ";
				
		$sql .= " WHERE main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '" . $data['report_to'] . "'";  
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}
			
		$sql .= " AND main_table.grand_total > 0";
			

		
		$result = $readConnection->fetchAll($sql);
		return $result;
		
	}
}

