<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Takingsreport extends Abstractblock
{
	
	
	var $currencies = array(
	"GBP" => "pounds",
	"EUR" => "euros",
	"USD" => "dollars"
	
	);
	var $isDate = false;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		\Magento\Store\Model\StoreManagerInterface $storeInterface,
		\Magento\Directory\Model\Currency $currencyModel,
		\Magento\Directory\Model\CurrencyFactory $currencyFactory,
		\Magento\Eav\Model\Config $eavConfig,
		
        array $data = []
    ) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_storeInterface = $storeInterface;
		$this->_productInterface = $product;
		$this->eavConfig = $eavConfig;
		$this->currenciesModel = $currencyModel;
		$this->currencyFactory = $currencyFactory;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	public function getAllowedCurrency(){
		return $this->currenciesModel->getConfigAllowCurrencies()  ;
	}
	public function getCurrecnyRates($rate){
		return $this->currenciesModel->getCurrencyRates($rate, $this->getAllowedCurrency());
	}
	
	public function getCurrencySymbol($currencyCode){
		return $this->currencyFactory->create()->load($currencyCode)->getCurrencySymbol();
	}
	protected function getConnection()
	{
		//if (!$this->connection) {
			$this->connection = $this->_resource->getConnection('core_write');
		//}

		return $this->connection;
	}

	public function getReportData($store_id= null, $midnight = false){
		$postData = $this->getAllData();
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		}
		if (isset($postData['website'])){
			$data['website'] = $postData['website'];
		}  	
		if ($this->_request->getParam('from')){
			$data['from'] = $this->_request->getParam('from');
		}
		if ($this->_request->getParam('to')){
			$data['to'] = $this->_request->getParam('to');
		}
		if($this->_request->getParam('midnight') == 1){
			
			$data['midnight'] = true;
			
		}
		if(isset($data['from']) && isset($data['to']))
		{
			$this->isDate = true;
			return $this->getDataFromTo($data, $midnight);
			
		}
		else{
			return $this->getDatas($data, $midnight);
		}
	}
	
	public function getReportDataSource($store_id= null, $midnight = false){
		$postData = $this->getAllData();
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		}
		if (isset($postData['website'])){
			$data['website'] = $postData['website'];
		}  	
		if ($this->_request->getParam('from')){
			$data['from'] = $this->_request->getParam('from');
		}
		if ($this->_request->getParam('to')){
			$data['to'] = $this->_request->getParam('to');
		}
		if($this->_request->getParam('midnight') == 1){
			
			$data['midnight'] = true;
			
		}
		
		return $this->getDataSource($data, $midnight);
		
	}
	
	public function getReportDataBikes($store_id= null, $midnight = false){
		$postData = $this->getAllData();
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		}
		if (isset($postData['website'])){
			$data['website'] = $postData['website'];
		}  	
		if ($this->_request->getParam('from')){
			$data['from'] = $this->_request->getParam('from');
		}
		if ($this->_request->getParam('to')){
			$data['to'] = $this->_request->getParam('to');
		}
		if($this->_request->getParam('midnight') == 1){
			
			$data['midnight'] = true;
			
		}
		
		return $this->getDatacycles($data, $midnight, false);
		
	}
	
	public function getCurrencies(){
		return $this->currencies;
	}
	
	
	
	
	########################################### REPORT LOGIC ####################################################
	
	
	public function getDatas($data){
		//$time = ' 16:30:00';
		$time = ' 15:30:00'; // daylight savings
		if(isset($data['midnight']) && $data['midnight'] ===true){
			$time = ' 22:59:59';
		}			
		
		$timenowfix = $date = new \DateTime(date("Y-m-d", time())." {$time}",new \DateTimeZone('Europe/London'));
		//echo $timenowfix->format(' H:i:s');
		$timenowfix->setTimezone(new \DateTimeZone('UTC'));
		$time = $timenowfix->format(' H:i:s');
		$week_start = date('Y-m-d' . $time, strtotime("last Saturday"));	
		$cut_off_date = date('Y-m-d' . $time, time()); // today
		$numberofweeks = 8;
		$weeks_end = new \DateTime(date('Y-m-d', strtotime("next Friday")));
		$weeks_end->format('Y-m-d H:i:s');
		$today = new \DateTime();
		$difference = $today->diff($weeks_end);
		$difference = $difference->days;
		$days_total = $numberofweeks * 7;
		$days = $days_total - $difference;
				
		if(strtotime("now") > strtotime($cut_off_date)){
			$data['report_from'] = date('Y-m-d', time()) . $time;
			$report_to = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_to);
			$date->modify('+1 day');
			$data['report_to'] = $date->format('Y-m-d H:i:s');
		}
		else{
			$data['report_to'] = date('Y-m-d', time()) . $time;
			$report_from = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_from);
			$date->modify('-1 day');
			$data['report_from'] = $date->format('Y-m-d H:i:s');
			$days--;
		}	
		$result = array();
		$results_collection = array();
		$week = 0;
		for($i=0; $i<$days;$i++){
			//echo "<br/> week: " . $week;
			//echo "<br/>" . $i;
			$dayoftheweek = new \DateTime($data['report_to']);
			$dayoftheweek->modify('-' . $i . 'day');
			//echo "<br/> dayoftheweek: "; 
			$dayoftheweek = $dayoftheweek->format('Y-m-d H:i:s');
			$day = date('l', strtotime($dayoftheweek));
			if($i == 0){
				$day = 'Today';
			}
			elseif($i == 1){
				$day = 'Yesterday';
			}
			
			$sales_results_curr = array();
			$creditmemo_results_curr = array();
			$days_totals_curr = array();
			foreach($this->currencies as $code=>$label){
				$sales_results_curr[$code] = $this->getSalesOrdersData($data , $i, false, $code);
				$creditmemo_results_curr[$code] = $this->getCreditMemoData($data, $i, false, $code);
				$days_totals_curr[$code] = $sales_results_curr[$code]['sum_grand_total'] - $creditmemo_results_curr[$code]['sum_grand_total'];
			}
			
			
			$resulttmp = array();
			foreach($this->currencies as $code=>$label){
				$resulttmp['total_sales_'.$label] = $sales_results_curr[$code]['sum_grand_total'];
				$resulttmp['orders_'.$label] = $sales_results_curr[$code]['total'];
				$resulttmp['averages_'.$label] = $sales_results_curr[$code]['average_grand_total'];
				$resulttmp['returns_'.$label] = $creditmemo_results_curr[$code]['sum_grand_total'];
				$resulttmp['no-returns_'.$label] = $creditmemo_results_curr[$code]['total'];
				$resulttmp['total_'.$label] = $days_totals_curr[$code];
				$resulttmp['raw_total_'.$label] =  $sales_results_curr[$code]['sum_grand_total'];
				$resulttmp[''.$label] = $code;
				//$resulttmp['averages_'.$label] = $sales_results_curr[$code]['sum_grand_total'];
			}
			//var_dump($resulttmp);exit;
			$resulttmp['day'] = $day;
			$result[] = $resulttmp;				   
			//$result[]['day'] = $day;
			
			// if the day equals week start - create a new week. 		   
			if($dayoftheweek == $week_start){
				$results_collection[$week] = $result;
				$week++;
				$date = new \DateTime($week_start);
				$date->modify('-7 day');
				$week_start = $date->format('Y-m-d H:i:s');
				$result = array();
			}
			elseif(strtotime($dayoftheweek) > strtotime("now") && strtotime($dayoftheweek) > strtotime($cut_off_date) && date('l', strtotime($dayoftheweek)) == 'Saturday'){
				$results_collection[$week] = $result;
				$week++;
				$result = array();
			}
		}
						   
		//print_r($results_collection);
		//die;
						   
		return $results_collection;
	}


	public function getDatacycles($data, $midnight= false, $lastyear = false){
		//$time = ' 16:30:00';
		$time = ' 15:30:00'; // daylight savings
		if(isset($data['midnight']) && $data['midnight'] ===true){
			$time = ' 22:59:59';
		}	
		//var_dump($time);
		$endday = "Sunday";
		$startday = "Saturday";
		if($midnight){
			$endday = "Sunday";
		$startday = "Monday";
		}
		$timenowfix = $date = new \DateTime(date("Y-m-d", time())." {$time}",new \DateTimeZone('Europe/London'));
		//echo $timenowfix->format(' H:i:s');
		$timenowfix->setTimezone(new \DateTimeZone('UTC'));
		$datenow = $timenowfix->format("Y-m-d H:i:s");
		$time = $timenowfix->format(' H:i:s');
		if(date('l') == $startday){
			$week_start = date('Y-m-d' . $time);
		}
		else $week_start = date('Y-m-d' . $time, strtotime("last $startday"));	
		$cut_off_date = date('Y-m-d' . $time, time()); // today
		$numberofweeks = 8;
		$weeks_end = new \DateTime(date('Y-m-d', strtotime("next $endday")));
		$weeks_end->format('Y-m-d H:i:s');
		$today = new \DateTime();
		$difference = $today->diff($weeks_end);
		$difference = $difference->days;
		$days_total = $numberofweeks * 7;
		$days = $days_total - $difference;
		
		
				
		if(strtotime("now") > strtotime($cut_off_date)){
			$data['report_from'] = date('Y-m-d', time()) . $time;
			$report_to = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_to);
			$date->modify('+1 day');
			$data['report_to'] = $date->format('Y-m-d H:i:s');
		}
		else{
			$data['report_to'] = date('Y-m-d', time()) . $time;
			$report_from = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_from);
			$date->modify('-1 day');
			$data['report_from'] = $date->format('Y-m-d H:i:s');
			$days--;
		}
		if($lastyear){
			$nowdatetime = new \DateTime();
			$day = $nowdatetime->format('l');           // get the name of the day we want
			
			$nowdatetime->sub(new \DateInterval('P1Y')); // go back a year
			$nowdatetime->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$datenow = $nowdatetime->format('Y-m-d H:i:s');
			
			$day = $date->format('l');           // get the name of the day we want
			
			$date->sub(new \DateInterval('P1Y')); // go back a year
			$date->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$data['report_from'] = $date->format('Y-m-d H:i:s');
			
			
			$dateto = new \DateTime($data['report_to']);
			
			$day = $dateto->format('l');           // get the name of the day we want
			
			$dateto->sub(new \DateInterval('P1Y')); // go back a year
			$dateto->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$data['report_to'] = $dateto->format('Y-m-d H:i:s');
			
			$dateweek_start = new \DateTime($week_start);
			$day = $dateweek_start->format('l');           // get the name of the day we want
			
			$dateweek_start->sub(new \DateInterval('P1Y')); // go back a year
			$dateweek_start->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$week_start = $dateweek_start->format('Y-m-d H:i:s');
				
			$datecut_off_date = new \DateTime($cut_off_date);
			$day = $datecut_off_date->format('l');           // get the name of the day we want
			
			$datecut_off_date->sub(new \DateInterval('P1Y')); // go back a year
			$datecut_off_date->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$cut_off_date = $datecut_off_date->format('Y-m-d H:i:s');
			
			/*$dateweeks_end = new \DateTime($weeks_end);
			$day = $dateweeks_end->format('l');           // get the name of the day we want
			
			$dateweeks_end->sub(new \DateInterval('P1Y')); // go back a year
			$dateweeks_end->modify('next ' . $day);       // from this point, go to the next $day
			//echo $date->format('Ymd'), "\n";     // ouput the date
			$weeks_end = $dateweeks_end->format('Y-m-d');
			*/
			//var_dump($days);
				
		}
		
		$result = array();
		$results_collection = array();
		$week = 0;
		for($i=0; $i<$days;$i++){
			//echo "<br/> week: " . $week;
			//echo "<br/>" . $i;
			$dayoftheweek = new \DateTime($data['report_to']);
			$dayoftheweek->modify('-' . $i . 'day');
			//echo "<br/> dayoftheweek: "; 
			$dayoftheweek = $dayoftheweek->format('Y-m-d H:i:s');
			$day = date('l', strtotime($dayoftheweek));
			if($i == 0){
				$day = 'Today';
			}
			elseif($i == 1){
				$day = 'Yesterday';
			}
			
			$sales_results_curr = array();
			$creditmemo_results_curr = array();
			$days_totals_curr = array();
			foreach($this->currencies as $code=>$label){
				$sales_results_curr[$code] = $this->getSalesOrdersDatacycles($data , $i, false, $code);
				$creditmemo_results_curr[$code] = $this->getCreditMemoDatacycles($data, $i, false, $code);
				$days_totals_curr[$code] = $sales_results_curr[$code]['sum_grand_total'] - $creditmemo_results_curr[$code]['sum_grand_total'];
			}
			
			
			$resulttmp = array();
			foreach($this->currencies as $code=>$label){
				$resulttmp['total_sales_'.$label] = $sales_results_curr[$code]['sum_grand_total'];
				$resulttmp['orders_'.$label] = $sales_results_curr[$code]['total'];
				$resulttmp['averages_'.$label] = $sales_results_curr[$code]['average_grand_total'];
				$resulttmp['returns_'.$label] = $creditmemo_results_curr[$code]['sum_grand_total'];
				$resulttmp['no-returns_'.$label] = $creditmemo_results_curr[$code]['total'];
				$resulttmp['total_'.$label] = $days_totals_curr[$code];
				$resulttmp['raw_total_'.$label] =  $sales_results_curr[$code]['sum_grand_total'];
				$resulttmp[''.$label] = $code;
				//$resulttmp['averages_'.$label] = $sales_results_curr[$code]['sum_grand_total'];
			}
			//var_dump($resulttmp);exit;
			$resulttmp['day'] = $day;
			$result[] = $resulttmp;				   
			//$result[]['day'] = $day;
			
			// if the day equals week start - create a new week. 		   
			if($dayoftheweek == $week_start){
				$results_collection[$week] = $result;
				$week++;
				$date = new \DateTime($week_start);
				$date->modify('-7 day');
				$week_start = $date->format('Y-m-d H:i:s');
				$result = array();
			}
			elseif(strtotime($dayoftheweek) > strtotime($datenow) && strtotime($dayoftheweek) > strtotime($cut_off_date) && date('l', strtotime($dayoftheweek)) == 'Monday'){
				$results_collection[$week] = $result;
				$week++;
				$result = array();
			}
		}
						   
		//print_r($results_collection);
		//die;
						   
		return $results_collection;
	}

	public function getDataFromTo($data, $midnight= false){
		//$time = ' 16:30:00';
		$time = ' 15:30:00'; // daylight savings
		if(isset($data['midnight']) && $data['midnight'] ===true){
			$time = ' 22:59:59';
		}	
		$from = $data['from'];
		$to = $data['to'];
		$timenowfix = $date = new \DateTime($to." {$time}",new \DateTimeZone('Europe/London'));
		//echo $timenowfix->format(' H:i:s');
		$timenowfix->setTimezone(new \DateTimeZone('UTC'));
		$time = $timenowfix->format(' H:i:s');
		//$week_start = date('Y-m-d' . $time, strtotime("last Saturday"));	
		$cut_off_date = date($from . $time, time()); // today
		$numberofweeks = 8;
		//$weeks_end = new \DateTime(date('Y-m-d', strtotime("next Friday")));
		//$weeks_end->format('Y-m-d H:i:s');
		$today = new \DateTime();
		$difference = $timenowfix->diff($cut_off_date);
		$difference = $difference->days;
		$days_total = $numberofweeks * 7;
		$days = $difference ; //$days_total - $difference;
				
		if(strtotime("now") > strtotime($cut_off_date)){
			$data['report_from'] = $timenowfix->format("Y-m-d H:i:s");
			$report_to = $cut_off_date->format("Y-m-d H:i:s");
			$date = new \DateTime($report_to);
			$date->modify('+1 day');
			$data['report_to'] = $date->format('Y-m-d H:i:s');
		}
		else{
			$data['report_to'] = $date->format('Y-m-d H:i:s');
			$report_from = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_from);
			$date->modify('-1 day');
			$data['report_from'] = $timenowfix->format("Y-m-d H:i:s");
			$days--;
		}	
		$result = array();
		$results_collection = array();
		$week = 0;
		for($i=0; $i<$days;$i++){
			//echo "<br/> week: " . $week;
			//echo "<br/>" . $i;
			$dayoftheweek = new \DateTime($data['report_to']);
			$dayoftheweek->modify('-' . $i . 'day');
			//echo "<br/> dayoftheweek: "; 
			$dayoftheweek = $dayoftheweek->format('Y-m-d H:i:s');
			$day = date('l', strtotime($dayoftheweek));
			if($i == 0){
				$day = 'Today';
			}
			elseif($i == 1){
				$day = 'Yesterday';
			}
						
			$sales_results_pounds = $this->getSalesOrdersData($data , $i, false, 'GBP');
			$sales_results_euros = $this->getSalesOrdersData($data , $i, false, 'EUR');
			$sales_results_dollars = $this->getSalesOrdersData($data , $i, false, 'USD');
			
			$creditmemo_results_pounds = $this->getCreditMemoData($data, $i, false, 'GBP');
			$creditmemo_results_euros = $this->getCreditMemoData($data, $i, false, 'EUR');
			$creditmemo_results_dollars = $this->getCreditMemoData($data, $i, false, 'USD');
			
			$days_totals_pounds = $sales_results_pounds['sum_grand_total'] - $creditmemo_results_pounds['sum_grand_total'];
			$days_totals_euros = $sales_results_euros['sum_grand_total'] - $creditmemo_results_euros['sum_grand_total'];
			$days_totals_dollars = $sales_results_dollars['sum_grand_total'] - $creditmemo_results_dollars['sum_grand_total'];			   
							   
			$result[] = array(
							   'total_sales_pounds' => $sales_results_pounds['sum_grand_total'],
							   'total_sales_euros' => $sales_results_euros['sum_grand_total'],
							   'total_sales_dollars' => $sales_results_dollars['sum_grand_total'],
							   'orders_pounds' => $sales_results_pounds['total'],
							   'orders_euros' => $sales_results_euros['total'],
							   'orders_dollars' => $sales_results_dollars['total'],
							   'averages_pounds' => $sales_results_pounds['average_grand_total'],
							   'averages_euros' => $sales_results_euros['average_grand_total'],
							   'averages_dollars' => $sales_results_dollars['average_grand_total'],
							   'returns_pounds' => $creditmemo_results_pounds['sum_grand_total'],
							   'returns_euros' => $creditmemo_results_euros['sum_grand_total'],
							   'returns_dollars' => $creditmemo_results_dollars['sum_grand_total'],
							   'no-returns_pounds' => $creditmemo_results_pounds['total'],
							   'no-returns_euros' => $creditmemo_results_euros['total'],
							   'no-returns_dollars' => $creditmemo_results_dollars['total'],
							   'total_pounds' => $days_totals_pounds,
							   'total_euros' => $days_totals_euros,
							   'total_dollars' => $days_totals_dollars,
							   'raw_total_pounds' => $sales_results_pounds['sum_grand_total'],
							   'raw_total_euros' => $sales_results_euros['sum_grand_total'],
							   'raw_total_dollars' => $sales_results_dollars['sum_grand_total'],
							   'pounds' => 'GBP',
							   'euros' => 'EUR',
							   'dollars' => 'USD',
							   'day' => $day,
							   );
							   
			//$result[]['day'] = $day;
			
			// if the day equals week start - create a new week. 		   
			if($dayoftheweek == $week_start){
				$results_collection[$week] = $result;
				$week++;
				$date = new \DateTime($week_start);
				$date->modify('-7 day');
				$week_start = $date->format('Y-m-d H:i:s');
				$result = array();
			}
			elseif(strtotime($dayoftheweek) > strtotime("now") && strtotime($dayoftheweek) > strtotime($cut_off_date) && date('l', strtotime($dayoftheweek)) == 'Saturday'){
				$results_collection[$week] = $result;
				$week++;
				$result = array();
			}
		}
						   
		//print_r($results_collection);
		//die;
						   
		return $results_collection;
	}


	public function getSalesOrdersData($data, $days = 0, $today = false, $currency = 'GBP'){

		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT SUM(main_table.grand_total) AS sum_grand_total, AVG(main_table.grand_total) AS average_grand_total, COUNT(*) AS total
											  FROM sales_order AS main_table";
											  
		if(isset($_GET['amazon'])){
			$sql.= "
			JOIN m2epro_order ON m2epro_order.magento_order_id = main_table.entity_id
			JOIN m2epro_marketplace ON m2epro_marketplace.id = m2epro_order.marketplace_id
			JOIN m2epro_amazon_order ON m2epro_amazon_order.order_id = m2epro_order.id
			";
		}
											  
		$date = new \DateTime($data['report_to']);
		$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
		if(isset($_GET['gina']) &&$_GET['gina'] == "1"){
			$sql .= " AND main_table.store_id IN (28,6,19,8,27,24,26,25)"; 
		}	
		elseif(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}								  
		
		if (isset($_GET['prime']) &&$_GET['prime'] == "1"){
			$sql .= " AND main_table.`shipping_description` like '%Is Prime%'";
		}

		$sql .= " AND main_table.status IN('order_query', 'awaiting_giftcard', 'bike_awaiting_shipping', 'cyclescheme_pending', 'processing_return', 'closed','holded','complete','processing','pick_note_printed','fraud','pending_auth','packed','pre_order','start_id','held','bike_order','emailed','wholesale_order')
				  AND main_table.order_currency_code = '" . $currency . "' 
				  AND main_table.grand_total > 0";
				  
		if(isset($_GET['amazon']) && $_GET['amazon'] != "All"){
			$sql .= " AND m2epro_marketplace.code = '".$_GET['amazon']."'";
		}
										  			
																  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//print_r($result);
		return $result[0];
	}
	
	public function getSalesOrdersDatacycles($data, $days = 0, $today = false, $currency = 'GBP'){
		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT SUM(sales_order_item.row_total_incl_tax - sales_order_item.discount_invoiced) AS sum_grand_total, 
		AVG(sales_order_item.row_total_incl_tax - sales_order_item.discount_invoiced) AS average_grand_total, 
		SUM(sales_order_item.qty_invoiced) AS total,
		SUM(((sales_order_item.row_total_incl_tax - sales_order_item.discount_invoiced)) - catalog_product_entity_decimal.value) as margin
		
FROM sales_order_item 
JOIN sales_order AS main_table ON main_table.entity_id = sales_order_item.order_id
JOIN catalog_product_entity ON catalog_product_entity.entity_id = sales_order_item.product_id
JOIN catalog_product_entity_decimal ON catalog_product_entity.entity_id = catalog_product_entity_decimal.entity_id 
AND catalog_product_entity_decimal.attribute_id = 293 and catalog_product_entity_decimal.store_id = 0
											  ";
											  
		$date = new \DateTime($data['report_to']);
		$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE sales_order_item.price > 0 AND (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
		if(isset($_GET['gina']) &&$_GET['gina'] == "1"){
			$sql .= " AND main_table.store_id IN (28,6,19,8,27,24,26,25)"; 
		}	
		elseif(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}								  
		
		$sql .= " AND main_table.status IN('order_query', 'awaiting_giftcard', 'bike_awaiting_shipping', 'cyclescheme_pending', 'processing_return', 'closed','holded','complete','processing','pick_note_printed','fraud','pending_auth','packed','pre_order','start_id','held','bike_order','emailed','wholesale_order')
				  AND main_table.order_currency_code = '" . $currency . "' 
				  AND main_table.grand_total > 0 AND catalog_product_entity.attribute_set_id IN (9, 34)";
										  			
																  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;die;
		//print_r($result);
		return $result[0];
	}
	
	public function getWebsiteStoreIds($id){
		$website = $this->_storeInterface->getWebsite($id);
		$store_ids = array();
		foreach($website->getGroups() as $group){
			foreach($group->getStores() as $store){
				$store_ids[] = $store->getId();	
			}
		}
		return $store_ids;
	}
	
	public function getCreditMemoData($data, $days = 0, $today = false, $currency = 'GBP'){
		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT SUM(main_table.grand_total) AS sum_grand_total, COUNT(*) AS total
											  FROM sales_creditmemo AS main_table";
											  
		if(isset($_GET['amazon']) && $_GET['amazon'] != "-"){
			$sql.= "
			JOIN m2epro_order ON m2epro_order.magento_order_id = main_table.order_id
			JOIN m2epro_marketplace ON m2epro_marketplace.id = m2epro_order.marketplace_id
			JOIN m2epro_amazon_order ON m2epro_amazon_order.order_id = m2epro_order.id
			";
		}
											  
		$date = new \DateTime($data['report_to']);
		$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}								  
		
		//$sql .= " AND main_table.status IN('Complete','Processing','Pick Note Printed','Pending') ";
		$sql .= " AND main_table.order_currency_code = '" . $currency . "' 
				  AND main_table.grand_total > 0";
				  
		if(isset($_GET['amazon']) && $_GET['amazon'] != "All"){
			$sql .= " AND m2epro_marketplace.code = '".$_GET['amazon']."'";
		}
										  						  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//print_r($result);
		return $result[0];
	}

	public function getCreditMemoDatacycles($data, $days = 0, $today = false, $currency = 'GBP'){
		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT 
		SUM(sales_creditmemo_item.base_row_total_incl_tax-(IF(sales_creditmemo_item.base_discount_amount IS NULL, 0,sales_creditmemo_item.base_discount_amount))) AS sum_grand_total, 
		AVG(sales_creditmemo_item.base_row_total_incl_tax) AS average_grand_total, SUM(sales_creditmemo_item.qty) AS total
FROM sales_creditmemo_item 
JOIN sales_creditmemo AS main_table ON main_table.entity_id = sales_creditmemo_item.parent_id
JOIN catalog_product_entity ON catalog_product_entity.entity_id = sales_creditmemo_item.product_id";
											  
		$date = new \DateTime($data['report_to']);
		$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE sales_creditmemo_item.price > 0 AND (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}								  
		
		//$sql .= " AND main_table.status IN('Complete','Processing','Pick Note Printed','Pending') ";
		$sql .= " AND main_table.order_currency_code = '" . $currency . "' 
				  AND main_table.grand_total > 0 AND catalog_product_entity.attribute_set_id IN (9, 34)";
										  						  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//print_r($result);
		return $result[0];
	}



	public function getDataSource($data){
		//$time = ' 16:30:00';
		$time = ' 15:30:00'; // daylight savings
		if(isset($data['midnight']) && $data['midnight'] ===true){
			$time = ' 22:59:59';
		}	
		$timenowfix = $date = new \DateTime(date("Y-m-d", time())." {$time}",new \DateTimeZone('Europe/London'));
		//echo $timenowfix->format(' H:i:s');
		$timenowfix->setTimezone(new \DateTimeZone('UTC'));
		$time = $timenowfix->format(' H:i:s');
		$week_start =  new \DateTime(date('Y-m-d', strtotime("last Friday")));
		$week_start->format('Y-m-d H:i:s');
		$cut_off_date = date('Y-m-d' . $time, time()); // today
		$numberofweeks = 1;
		$weeks_end = new \DateTime(date('Y-m-d', strtotime("next Friday")));
		$weeks_end->format('Y-m-d H:i:s');
		$today = new \DateTime();
		$difference = $today->diff($weeks_end);
		$differencestart = $today->diff($week_start);
		$difference = $difference->days;
		$differencestart = $differencestart->days;
		$days_total = $numberofweeks * 7;
		$days = $days_total - $difference;
				
		if(strtotime("now") > strtotime($cut_off_date)){
			$data['report_from'] = date('Y-m-d', time()) . $time;
			$datefromobj = new \DateTime($data['report_from']);
			$report_to = date('Y-m-d', time()) . $time;
			$date = new \DateTime($report_to);
			$date->modify('+1 day');
			$data['report_to'] = $date->format('Y-m-d H:i:s');
			$data['report_to_obj'] = $date;
			$data['report_from_obj'] = $datefromobj;
		}
		else{
			$data['report_to'] = date('Y-m-d', time()) . $time;
			
			$report_from = date('Y-m-d', time()) . $time;
			$datetoobj = new \DateTime(date('Y-m-d', time()) . $time);
			$data['report_to_obj'] = $datetoobj;
			$date = new \DateTime($report_from);
			$date->modify('-1 day');
			$data['report_from'] = $date->format('Y-m-d H:i:s');
			$data['report_from_obj'] = $date;
			$days--;
		}	
		$result = array();
		$results_collection = array();
		$week = 0;
		
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'source');
		$options = array();
		if ($attribute->usesSource()) {
		    $options = $attribute->getSource()->getAllOptions(false);
		}
		$attributeid = $attribute->getAttributeId();
		//
		
		$itemsdata = array();
		for($i=0; $i<(7);$i++){

                        //echo "<br/> week: " . $week;
                        //echo "<br/>" . $i;
                        $dayoftheweek = new \DateTime($data['report_to']);
                        $dayoftheweek->modify('-' . $i . 'day');
                        //echo "<br/> dayoftheweek: ";
                        $dayoftheweek = $dayoftheweek->format('Y-m-d H:i:s');
                        $day = date('l', strtotime($dayoftheweek));
                        if($i == 0){
                                $day = 'Today';
                        }
                        elseif($i == 1){
                                $day = 'Yesterday';
                        }



			//var_dump($data['report_from'], $data['report_to']);echo "<br />\n";
			$currencies = array("GBP", "EUR", "USD");
			
			
			$itemsdata["Week ".$i] = array();
			
			$tmpdata = $this->getSalesOrdersDataSource($data , $i, false, $currency, $attributeid, $option['value']);
			$tmpdatareturn = $this->getSalesOrdersDataSourceReturns($data , $i, false, $currency, $attributeid, $option['value']);
			$idtooption = array();
			foreach($options as  $option){
				$idtooption[$option['value']] = $option['label'];
				$itemsdata["Week ".$i][$option['label']] = array();
				foreach($currencies as $currency){
					//var_dump('GBP', $attributeid, $option['label'], $option['value']);
					$itemsdata["Week ".$i][$option['label']][$currency] = array("sum_grand_total"=>"0", "total"=>"0", "average_grand_total"=>"0", 
					"sum_grand_total_return"=>"0", "total_return"=>"0", "average_grand_total_return"=>"0");
				}
			}
			foreach($tmpdata as $tmprow){
				
				$grouped = explode("-",$tmprow['grouped']);
				$itemsdata["Week ".$i][$idtooption[$grouped[0]]][$grouped[1]] = $tmprow;
				
			}
			foreach($tmpdatareturn as $tmprow){
				
				$grouped = explode("-",$tmprow['grouped']);
				$itemsdata["Week ".$i][$idtooption[$grouped[0]]][$grouped[1]] = array_replace($itemsdata["Week ".$i][$idtooption[$grouped[0]]][$grouped[1]], $tmprow);
				
			}
			
			
			if($i == 0){
				$data['report_to_obj']->modify("-1 days");
				$data['report_to'] = $data['report_to_obj']->format('Y-m-d H:i:s');
			}
			else{
				$data['report_to_obj']->modify("-1 days");
				$data['report_to'] = $data['report_to_obj']->format('Y-m-d H:i:s');
			}
			$data['report_from_obj']->modify("-1 days");
			$data['report_from'] = $data['report_from_obj']->format('Y-m-d H:i:s');
			
			
			
			
			
						   
		
		}
		//echo "<pre>";print_r($itemsdata);echo "</pre>";ob_flush();
		
		return $itemsdata;
	}
	public function getSalesOrdersDataSource($data, $days = 0, $today = false, $currency = 'GBP', $attributeid, $optionid = null){
		
		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT 
		CONCAT(IF(source_table.value IS NULL, \"NONE\", source_table.value), \"-\", order_table.order_currency_code) AS grouped,
		SUM(main_table.row_total_incl_tax) AS sum_grand_total, 
		SUM(main_table.base_row_total_incl_tax) AS base_sum_grand_total, AVG(main_table.row_total_incl_tax) AS average_grand_total, COUNT(*) AS total
		FROM sales_order_item AS main_table 
		JOIN sales_order AS order_table ON order_table.entity_id = main_table.order_id
		LEFT JOIN `catalog_product_entity_int` AS source_table ON source_table.`attribute_id` = {$attributeid} AND source_table.`store_id` = 0 AND source_table.entity_id = main_table.product_id 
		";
											  
		$date = new \DateTime($data['report_to']);
		//$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		//$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}								  
		
		$sql .= " AND order_table.status IN('order_query', 'awaiting_giftcard', 'bike_awaiting_shipping', 'cyclescheme_pending', 'processing_return', 'closed','holded','complete','processing','pick_note_printed','fraud','pending_auth','packed','pre_order','start_id','held','bike_order','emailed','wholesale_order')
				  /*AND order_table.order_currency_code = '" . $currency . "' */
				  AND order_table.base_grand_total > 0
				  AND order_table.store_id <> 1
				  AND main_table.row_total_incl_tax > 0
				  AND main_table.product_type <> 'bundle'
				  ";
				  
		/*if($optionid == null){
			$sql .= " AND  source_table.value IS NULL";
		}
		else{
			$sql .= " AND  source_table.value = {$optionid}";
		}*/
		$sql .= " GROUP BY CONCAT(IF(source_table.value IS NULL, \"NONE\", source_table.value), \"-\", order_table.order_currency_code)";
										  			
		//var_dump($sql);														  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;exit;
		//print_r($result);exit;
		return $result;
	}

	public function getSalesOrdersDataSourceReturns($data, $days = 0, $today = false, $currency = 'GBP', $attributeid, $optionid = null){
		
		$readConnection = $this->getConnection();
		$modify_date = "-" . $days . " day";
		$sql = "SELECT 
		CONCAT(IF(source_table.value IS NULL, \"NONE\", source_table.value), \"-\", order_table.order_currency_code) AS grouped,
		SUM(main_table.row_total_incl_tax) AS sum_grand_total_return,
		SUM(main_table.base_row_total_incl_tax) AS base_sum_grand_total_return, AVG(main_table.row_total_incl_tax) AS average_grand_total_return, COUNT(*) AS total_return
		FROM sales_creditmemo_item AS main_table 
		JOIN sales_creditmemo AS order_table ON order_table.entity_id = main_table.parent_id
        JOIN sales_order AS order_table2 ON order_table.order_id = order_table2.entity_id
		LEFT JOIN `catalog_product_entity_int` AS source_table ON source_table.`attribute_id` = {$attributeid} AND source_table.`store_id` = 0 AND source_table.entity_id = main_table.product_id 
		";
											  
		$date = new \DateTime($data['report_to']);
		//$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new \DateTime($data['report_from']);
		//$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (order_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND order_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND order_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND order_table.store_id = " . $data['store_id'];
		}								  
		
		$sql .= " AND order_table2.status IN('order_query', 'awaiting_giftcard', 'bike_awaiting_shipping', 'cyclescheme_pending', 'processing_return', 'closed','holded','complete','processing','pick_note_printed','fraud','pending_auth','packed','pre_order','start_id','held','bike_order','emailed','wholesale_order')
				  /*AND order_table.order_currency_code = '" . $currency . "' */
				  AND main_table.row_total_incl_tax > 0
				  
				  ";
				  
		/*if($optionid == null){
			$sql .= " AND  source_table.value IS NULL";
		}
		else{
			$sql .= " AND  source_table.value = {$optionid}";
		}*/
		$sql .= " GROUP BY CONCAT(IF(source_table.value IS NULL, \"NONE\", source_table.value), \"-\", order_table.order_currency_code)";
										  			
		//var_dump($sql);														  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//print_r($result);
		return $result;
	}
	
	
	
	
	
}

