<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

use \DateTime as Datetime;
class Dailytakingsreport extends Abstractblock
{
	
	
	var $currencies = array(
	"GBP" => "pounds",
	"EUR" => "euros",
	"USD" => "dollars"
	
	);
	var $isDate = false;

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		\Magento\Store\Model\StoreManagerInterface $storeInterface,
		\Magento\Directory\Model\Currency $currencyModel,
		\Magento\Directory\Model\CurrencyFactory $currencyFactory,
		\Magento\Eav\Model\Config $eavConfig,
		
        array $data = []
    ) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_storeInterface = $storeInterface;
		$this->_productInterface = $product;
		$this->eavConfig = $eavConfig;
		$this->currenciesModel = $currencyModel;
		$this->currencyFactory = $currencyFactory;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	public function getAllowedCurrency(){
		return $this->currenciesModel->getConfigAllowCurrencies()  ;
	}
	public function getCurrecnyRates($rate){
		return $this->currenciesModel->getCurrencyRates($rate, $this->getAllowedCurrency());
	}
	
	public function getCurrencySymbol($currencyCode){
		return $this->currencyFactory->create()->load($currencyCode)->getCurrencySymbol();
	}
	protected function getConnection()
	{
		//if (!$this->connection) {
			$this->connection = $this->_resource->getConnection('core_write');
		//}

		return $this->connection;
	}

	
	public function getReportData($store_id = null){
		$postData =$this->getAllData();
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		}
		if (isset($postData['website'])){
			$data['website'] = $postData['website'];
		}  	
		return $this->getDatas($data);
	}
	
	
	########################################### REPORT LOGIC ####################################################
	
	
	public function getDatas($data){

                $past_months = 36;
                $search_term = 'BAD';


                $results_collection = array();
                $week = 0;
                $result = array();
                for($i=$past_months; $i>=0; $i--){

                        $past = $i;
                        //$past = 15; // testing
			$m=date('m');
			if ($i > 24)
			{
				$x = $i - 24;
			}
			else if ($i > 12)
			{
				$x = $i - 12;
			}
			else
			{
				$x = $i;
			}
			$m=$m-($x);
			if ($m <= 0)
			{
				$m = $m +12;
			}
                        $set_start_date = date('Y-m-01 00:00:00',strtotime('this month'));
                        $set_start_date = new DateTime($set_start_date);
                        $set_start_date->modify('-' . $i . ' month');
                        $period_start_date = $set_start_date->format('Y-m-d H:i:s');
			//$set_start_date = date('Y-m-01 00:00:00', strtotime($period_start_date));
			//$set_start_date = new DateTime($set_start_date);
			//$period_start_date = $set_start_date->format('Y-m-d H:i:s');
			if ($i == 0)
			{
                        $set_end_date = date('Y-m-d 23:59:59', strtotime('this month'));
                        $set_end_date = new DateTime($set_end_date);
                        $set_end_date->modify('-' . $i . ' month');
                        $period_end_date = $set_end_date->format('Y-m-d H:i:s');
			}else{
			//echo "m - ".$m;
                        $set_end_date = date('Y-m-14 23:59:59', strtotime('this month'));//mid month so now over roll
                        $set_end_date = new DateTime($set_end_date);
                        $set_end_date->modify('-' . $i . ' month');
                        $period_end_date = $set_end_date->format('Y-m-d H:i:s');
			$set_end_date = date('Y-'.$m.'-t 23:59:59', strtotime($period_end_date));
                        $set_end_date = new DateTime($set_end_date);
			$period_end_date = $set_end_date->format('Y-m-d H:i:s');			
			}
                        //$report_data_date = $set_end_date->format('Y-m');

                        $data['report_from'] = $period_start_date;
                        $data['report_to'] = $period_end_date;

                        $sales_results_pounds = $this->getSalesOrdersData($data, $i, false);
                        $creditmemo_results_pounds = $this->getCreditMemoData($data, $i, false);

                        $days_totals_pounds = $sales_results_pounds['sum_grand_total'] - $creditmemo_results_pounds['sum_grand_total'];

                        $result[] = array(
                                                           'total_sales' => $sales_results_pounds['sum_grand_total'],
                                                           'orders' => $sales_results_pounds['total'],
                                                           'averages' => $sales_results_pounds['average_grand_total'],
                                                           'returns' => $creditmemo_results_pounds['sum_grand_total'],
                                                           'no-returns' => $creditmemo_results_pounds['total'],
                                                           'total' => $days_totals_pounds,
                                                           'raw_total' => $sales_results_pounds['sum_grand_total'],
                                                           //'shipping_amount' => $sales_results_pounds['shipping_amount'],
                                                           //'euros' => 'EUR',
                                                           'day' => $set_start_date->format('Y-m'),
                                                           );

                        //$results_collection[$report_data_date] = $result;
                }

                //print_r($results_collection);
                //die;

                return $result;


	}

	public function getSalesOrdersData($data, $days = 0, $today = false){
		$readConnection = $this->getConnection();
		$modify_date = "-" . 0 . " day";
		$sql = "SELECT SUM(main_table.base_grand_total - main_table.shipping_amount - main_table.tax_amount) AS sum_grand_total, AVG(main_table.base_grand_total) AS average_grand_total, COUNT(*) AS total
											  FROM sales_order AS main_table";
											  
		$date = new DateTime($data['report_to']);
		//$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new DateTime($data['report_from']);
		//$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}							  

		$sql .= " AND main_table.status IN('bike_awaiting_shipping','cyclescheme_pending', 'processing_return','closed','holded','complete','processing','pick_note_printed','fraud','pending_auth','packed','pre_order','start_id','held','bike_order','emailed','wholesale_order')
				  AND main_table.grand_total > 0";
										  						  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//exit;
		//print_r($result);
		return $result[0];
	}

	public function getWebsiteStoreIds($id){
		$website = $this->_storeInterface->getWebsite($id);
		$store_ids = array();
		foreach($website->getGroups() as $group){
			foreach($group->getStores() as $store){
				$store_ids[] = $store->getId();	
			}
		}
		return $store_ids;
	}
	
	public function getCreditMemoData($data, $days = 0, $today = false){
		$readConnection = $this->getConnection();
		$modify_date = "-" . 0 . " day";
		$sql = "SELECT SUM(main_table.grand_total - main_table.shipping_amount - main_table.tax_amount) AS sum_grand_total, COUNT(*) AS total
											  FROM sales_creditmemo AS main_table";
											  
		$date = new DateTime($data['report_to']);
		//$date->modify($modify_date);
		$data['report_to'] = $date->format('Y-m-d H:i:s');
			
		$date = new DateTime($data['report_from']);
		$date->modify($modify_date);
		$data['report_from'] = $date->format('Y-m-d H:i:s');
		
		$sql .= " WHERE (main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '". $data['report_to'] ."')";
			
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		elseif($data['website'] == 'true'){
			$store_ids = $this->getWebsiteStoreIds($data['store_id']);
			$store_ids_sql = implode(',', $store_ids);
			$sql .= " AND main_table.store_id IN (" . $store_ids_sql . ")"; 
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}							  
		
		//$sql .= " AND main_table.status IN('Complete','Processing','Pick Note Printed','Pending') ";
		
		$sql .= " AND main_table.grand_total > 0";
										  						  							  											  
		$result = $readConnection->fetchAll($sql);
		
		//echo "<br/>" . $sql;
		//print_r($result);
		return $result[0];
	}
	
	
	
	
	
}

