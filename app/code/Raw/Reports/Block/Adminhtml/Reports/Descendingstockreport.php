<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Descendingstockreport extends Abstractblock
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributesets,
		\Magento\Eav\Model\Config $eavConfig,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
        array $data = []
    ) {
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_productInterface = $product;
		$this->_attributesets = $attributesets;
		$this->eavConfig = $eavConfig;
		$this->_customerRepositoryInterface = $customerRepositoryInterface;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	
	
	public function getAttributeSetsCollection(){
		return $this->_attributesets->create()
            ->setEntityTypeFilter(4)
            ->load()
            ->toOptionHash();
	}
	
	public function getManufacturerCollection(){
		$manufacturerArray = [];
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'manufacturer');
            
            foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) { 
                $manufacturerArray[$instance['value']] = $instance['label'];
            }
            asort($manufacturerArray);
		return $manufacturerArray;
	}
	
	public function getGenderCollection(){
		$manufacturerArray = [];
           $attribute = $this->eavConfig->getAttribute('catalog_product', 'gender');
            foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) { 
                $manufacturerArray[$instance['value']] = $instance['label'];
            }
            asort($manufacturerArray);
		return $manufacturerArray;
	}
		
	public function getCountryCollection(){
		return array(
							  "AT" => "Austria",
							  "BE" => "Belgium",
							  "BG" => "Bulgaria",
							  "CY" => "Cyprus",
							  "CZ" => "Czech Republic",
							  "DK" => "Denmark",
							  "EE" => "Estonia",
							  "FI" => "Finland",
							  "FR" => "France",
							  "DE" => "Germany",
							  "GR" => "Greece",
							  "HU" => "Hungary",
							  "IE" => "Ireland",
							  "IT" => "Italy",
							  "LV" => "Latvia",
							  "LT" => "Lithuania",
							  "LU" => "Luxembourg",
							  "MT" => "Malta",
							  "NL" => "Netherlands",
							  "PL" => "Poland",
							  "PT" => "Portugal",
							  "RO" => "Romania",
							  "SK" => "Slovakia (Slovak Republic)",
							  "SI" => "Slovenia",
							  "ES" => "Spain",
							  "SE" => "Sweden",
							  "GB" => "United Kingdom"
							);
	}
	
	
	public function getReportMonths($type = 'start'){
		$months_collection = $this->getReportMonths($type);
		if(empty($months_collection)){
			return array();
		}
		$months = array();
		foreach($months_collection as $result){
			$id = $result['date'];
			$date = new DateTime($result['date']);
			$months[$id] = $date->format('Y') . '-' . $date->format('F');
		}
		return $months;
	}
	
	public function getProductCategories(){
		$manufacturerArray = array();
            $attribute = $this->eavConfig->getAttribute('catalog_product', 'product_category');
			
            foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) { 
                $manufacturerArray[$instance['value']] = $instance['label'];
            }
            asort($manufacturerArray);
		return $manufacturerArray;
	}
	
	public function getStoreId(){
		$postData = $this->getAllData();
		if(!isset($postData['store_id']) || $postData['store_id'] == ''){
			return 0;
		}
		return $postData['store_id'];
		
	}
	
	public function renderColumn($value, $render){
		$result = '';	
		if($render == 'customer_vat'){
			$this->_customerRepositoryInterface->getById($$value);
			$result = $customer->getData('taxvat');
		}
		return $result;
	}
	
	public function getPeriodId(){
		$postData = $this->getAllData();
		if(!isset($postData['period']) || $postData['period'] == ''){
			return 1;
		}
		return $postData['period'];
	}
	
	
	
	public function getReportData(){
		
		$postData = $this->getAllData();
		if(empty($postData)){
			return array();
		}
		$data = array();
		if (isset($postData['limit_products']) && $postData['limit_products'] != ''){
			$data['limit_products'] = $postData['limit_products'];
		}
		else{
			return array();
		}
		if (isset($postData['store_ids']) && !empty($postData['store_ids'])){
			$data['store_ids'] = $postData['store_ids'];
		}
		else{
			return array();
		}
		$data['attributeset_ids'] = $postData['attributeset_ids'];
		if (isset($postData['attributeset_ids']) && $postData['attributeset_ids'] != ''){
			$data['attributeset_ids'] = $postData['attributeset_ids'];
		}
		if(isset($postData['manufacturer']) && $postData['manufacturer'] != ''){
			$data['manufacturer'] = $postData['manufacturer'];
		}
		if(isset($postData['gender']) && $postData['gender'] != ''){
			$data['gender'] = $postData['gender'];
		}
		return $this->getResults($data);
	}
	
	
	########################################### REPORT LOGIC ####################################################
	
	public function getResults($data){
		$parent_collection = $this->getParentData($data);
		$data_results = array();
		foreach($parent_collection as $parent){
			$data_results[$parent['id']]['parent'] = $parent;
			if($parent['type_id'] == 'configurable'){
				$child_collection = $this->getChildDatas($parent);
				foreach($child_collection as $child){
					$data_results[$parent['id']]['child'][] = $child;
				}
			}
		}
		
		$select_columns = array(
								  array('column'=>'name', 'header'=>'Name', 'type'=>'text'),
								  array('column'=>'sku', 'header'=>'SKU', 'type'=>'text'),
								  array('column'=>'actual_model', 'header'=>'Model Number', 'type'=>'text'),
								  //array('column'=>'manufacturer', 'header'=>'Manufacturer', 'type'=>'text'),
								  //array('column'=>'gender', 'header'=>'Gender', 'type'=>'text'),
								  array('column'=>'qty', 'header'=>'Qty', 'type'=>'number'),
								  array('column'=>'price', 'header'=>'Price', 'type'=>'price'),
								  array('column'=>'rrp', 'header'=>'RRP', 'type'=>'number', 'price' => true),
								  array('column'=>'type_id', 'header'=>'Type', 'type'=>'text'),
								  array('column'=>'percentage_saving', 'header'=>'Percentage Saving', 'type'=>'number')
								  );
								  
		//print_r($data_results); die;
		return array('data'=> $data_results, 'columns'=>$select_columns);
	}
	
	public function getParentData($data){
		$readConnection = $this->getConnection();
		$sql = "SELECT DISTINCT *        
				FROM dimasoft_decended_stock 
				WHERE parent_id = 0 ";
				if(isset($data['store_ids']) && !empty($data['store_ids'])){
					$store_ids = implode(',', $data['store_ids']);
					$sql .= " AND store_id IN (" . $store_ids . ")";
				}
				if(isset($data['attributeset_ids']) && !empty($data['attributeset_ids'])){
					$attributeset_ids = implode(',', $data['attributeset_ids']);
					$sql .= " AND attribute_set IN (" . $attributeset_ids . ")";
				}
				if(isset($data['manufacturer']) && $data['manufacturer'] != ''){
					$sql .= " AND manufacturer = '" . $data['manufacturer'] . "' ";
				}
				if(isset($data['gender']) && $data['gender'] != ''){
					$sql .= " AND gender = '" . $data['gender'] . "' ";
				}
				$sql .= "GROUP BY product_id ORDER BY qty DESC ";
				if(isset($data['limit_products']) && !empty($data['limit_products'])){
					$sql .= " LIMIT 0, " . $data['limit_products'];
				} 
				  //ORDER BY SUM(total_revenue) DESC ";
				  //echo $sql; die;	  							  											  
		$result = $readConnection->fetchAll($sql);
		return $result;
	}

	public function getChildDatas($data){
		$readConnection = $this->getConnection();
		$sql = "SELECT DISTINCT *        
				FROM dimasoft_decended_stock 
				WHERE type_id = 'simple' 
				AND parent_id = " . $data['product_id'];
				if(isset($data['store_ids']) && !empty($data['store_ids'])){
					$store_ids = implode(',', $data['store_ids']);
					$sql .= " AND store_id IN (" . $store_ids . ")";
				}   
				$sql .= " GROUP BY product_id ORDER BY qty DESC ";
				  //ORDER BY SUM(total_revenue) DESC ";
				  //echo $sql; die;	  							  											  
		$result = $readConnection->fetchAll($sql);
		return $result;
	}
	
}

