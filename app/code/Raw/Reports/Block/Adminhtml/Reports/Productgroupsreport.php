<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Productgroupsreport extends Abstractblock
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributesets,
		\Magento\Eav\Model\Config $eavConfig,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		\Magento\Catalog\Model\CategoryRepository $categoryRepository,
		\Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $collectionFactory,
		\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        array $data = []
    ) {
		error_reporting(E_ALL & ~E_NOTICE & ~E_DEPRECATED);
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_productInterface = $product;
		$this->_attributesets = $attributesets;
		$this->eavConfig = $eavConfig;
		$this->_customerRepositoryInterface = $customerRepositoryInterface;
		$this->categoryRepository = $categoryRepository;
		$this->categoryFactory = $collectionFactory;
		$this->productCollectionFactory = $productCollectionFactory;
		$this->_stockRegistry = $stockRegistry;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	
	public function getStockItem($productId)
    {
        return $this->_stockRegistry->getStockItem($productId);
    }
	
	public function getAttributeSetsCollection(){
		return $this->_attributesets->create()
            ->setEntityTypeFilter(4)
            ->load()
            ->toOptionHash();
	}
	
	public function getManufacturerCollection(){
		$manufacturerArray = [];
		$attribute = $this->eavConfig->getAttribute('catalog_product', 'manufacturer');
            
            foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) { 
                $manufacturerArray[$instance['value']] = $instance['label'];
            }
            asort($manufacturerArray);
		return $manufacturerArray;
	}
	
	public function getGenderCollection(){
		$manufacturerArray = [];
           $attribute = $this->eavConfig->getAttribute('catalog_product', 'gender');
            foreach ($attribute->getSource()->getAllOptions(true, true) as $instance) { 
                $manufacturerArray[$instance['value']] = $instance['label'];
            }
            asort($manufacturerArray);
		return $manufacturerArray;
	}
	public function getAttribute($attr){
		$manufacturerArray = [];
        $attribute = $this->eavConfig->getAttribute('catalog_product', 'attr');
            
		return $attribute;
	}
	
	
	
	public function getParentCategory($category, $route)
    {
        if($category->getLevel() <= 2){
            return $route;
        } else {
            $parentCategory = $this->categoryRepository->get($category->getParentId());
            $route = $parentCategory['name']." - ".$route;
            return $this->getParentCategory($parentCategory, $route);
        }

    }
	
	public function getProductCollection($type_id = "configurable"){
		$postData = $this->getAllData();
				
		if(empty($postData)){
			return array();
		}
		elseif($postData['report_from'] == '' || $postData['report_to'] == ''){
			return array();
		}
		elseif(count($postData['attributeset_ids']) == 1){
			if($postData['attributeset_ids'][0] == 0){
				return array();
			}
		}
		$data = array();
		$data['attributeset_ids'] = $postData['attributeset_ids'];
		if (isset($postData['store_id']) && $postData['store_id'] != ''){
			$data['store_id'] = $postData['store_id'];
		}
		if(isset($postData['manufacturer']) && $postData['manufacturer'] != ''){
			$data['manufacturer'] = $postData['manufacturer'];
		}
		if(isset($postData['gender']) && $postData['gender'] != ''){
			$data['gender'] = $postData['gender'];
		}
		return $this->getProductCollectionData($data, $type_id);
	}  
		
	public function getQtyOrdered($product_id){
		$postData = $this->getAllData();
		$data = array();
		if (isset($postData['store_id']) && $postData['store_id'] != ''){
			$data['store_id'] = $postData['store_id'];
		} 
		if (isset($postData['report_from'])){
			$data['report_from'] = date("Y-m-d", strtotime($postData['report_from']));
		}	
		if (isset($postData['report_to'])){
			$data['report_to'] = date("Y-m-d", strtotime($postData['report_to']));
		}
		
		return $this->getQtyOrderedData($product_id, $data);	
		
	}
	
	
	public function getCategoriesCollection($store_id){
		$categoryArray = array();
		if($store_id != ""){
			Mage::app()->setCurrentStore($store_id);
			$categories = $categories = $this->categoryFactory->create()
                ->addAttributeToSelect('*')
                ->addIsActiveFilter();
                
            $categoryArray[0] = '';

            foreach ($categories as $_category) {
                if ($_category->getIsActive()) {
                    $categoryRoute = $this->getParentCategory($_category, '');
                    $categoryArray[$_category['entity_id']] = $categoryRoute." ".$_category['name'];
                }
            }
		}
			//asort($categoryArray);
        return $categoryArray;
	}
	
	protected function getConnection()
	{
		//if (!$this->connection) {
			$this->connection = $this->_resource->getConnection('core_write');
		//}

		return $this->connection;
	}
	
	########################################### REPORT LOGIC ####################################################
	
	public function getQtyOrderedData($product_id, $data){
		/*if($data['report_from'] == $data['report_to']){
			$date = new DateTime($data['report_from']);
			$date->modify('-1 day');
			$data['report_from'] = $date->format('Y-m-d');
		}*/
		$report_from = $data['report_from'];
		$report_to = $data['report_to'];
		$readConnection = $this->getConnection();
	
		$sql = "SELECT DISTINCT product_id, SUM(qty_ordered) AS qty_ordered     
				FROM dimasoft_product_sales 
				WHERE date BETWEEN '" . $report_from . "' AND '" . $report_to . "'";
		if(!isset($data['store_id']) || $data['store_id'] == '' || $data['store_id'] == '0'){
			$sql .= " AND store_id IS NOT NULL";
		}
		else{
			$sql .= " AND store_id = " . $data['store_id'];
		} 
		
		$sql .= " AND product_id = " . $product_id;
		/*if($product_id == 36982){
			echo $sql; die;  
		}*/
				   							  											  
		$result = $readConnection->fetchAll($sql);
		return $result[0]['qty_ordered'];
	}


	public function getProductCollectionData($data, $type_id){
		$readConnection = $this->getConnection();
		$collection_array = array();
		$categories = '';
				
		foreach($data['attributeset_ids'] ?? [] as $attributeset_id){
			$collection = $this->productCollectionFactory->create()
			->addAttributeToFilter('status', array('eq' =>1))
			->addAttributeToFilter('type_id', $type_id);
			
			if($type_id == 'simple'){
				$collection->addAttributeToFilter('visibility', array('eq' => 4));
			}
			
			if(isset($data['manufacturer'])){
				 $collection->addAttributeToFilter('manufacturer', $data['manufacturer']);
			}
			
			if(isset($data['gender'])){
				 $collection->addAttributeToFilter('gender', $data['gender']);
			}
			
			if(isset($data['store_id']) && $data['store_id'] != ''){
				$collection->addStoreFilter($data['store_id']);
			}
			
            $collection->addAttributeToFilter('attribute_set_id', $attributeset_id)
			           ->addAttributeToSelect('entity_id')
				       ->addAttributeToSelect('name')
				       ->addAttributeToSelect('msrp')
					   ->addAttributeToSelect('price')
					   ->addAttributeToSelect('actual_model');
					   
			$collection_array[$attributeset_id] = $collection;
		}
		  							  											  
		return $collection_array;
	}
	
	protected function _getDesendentCategories($category_id)
    {
        $children = '';
        $parentCat = Mage::getModel('catalog/category')->load($category_id);
        $childCategories = $parentCat->getChildren();
        if ($childCategories) {
            $this->children = $this->children.','.$childCategories;
            $childCategories = explode(',',$childCategories);
            foreach ($childCategories as $child) {
                $this->_getDesendentCategories($child);
            }
        }
    }
}

