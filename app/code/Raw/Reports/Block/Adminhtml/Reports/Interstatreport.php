<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Reports\Block\Adminhtml\Reports;

class Interstatreport extends Abstractblock
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Store\Model\StoreManagerInterface $storeManager,
		\Magento\Framework\App\ResourceConnection  $resource,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
		
        array $data = []
    ) {
		$this->_storeManager = $storeManager;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_productInterface = $product;
        parent::__construct($context, $request, $storeManager, $resource, $product, $data);
    }
	protected function getConnection()
	{
		//if (!$this->connection) {
			$this->connection = $this->_resource->getConnection('core_write');
		//}

		return $this->connection;
	}

	public function getStoreData(){
		$stores = $this->_storeManager->getStores(true, true);
		return $stores;
	}
	
	public function getAllData(){
		return $this->_request->getParams();
	}
	
	public function getBySku($sku){
		
		return $this->_productInterface->get($sku);
		
	}
	
	public function getReportData(){
		$postData = $this->getAllData();
		if(empty($postData)){
			return array();
		}
		elseif(!isset($postData['report_from']) || !isset($postData['report_to'])){
			return array();
		}
		$data = array();
		if (isset($postData['store_id'])){
			$data['store_id'] = $postData['store_id'];
		} 
		if (isset($postData['report_from'])){
			$data['report_from'] = date("Y-m-d", strtotime($postData['report_from']));
		}	
		if (isset($postData['report_to'])){
			$data['report_to'] = date("Y-m-d", strtotime($postData['report_to']));
		}
		return $this->getDatas($data);
	}
	
	
	########################################### REPORT LOGIC ####################################################
	
	
	public function getDatas($data){
			if($data['report_from'] == $data['report_to']){
				$date = new DateTime($data['report_from']);
				$date->modify('-1 day');
				$data['report_from'] = $date->format('Y-m-d');
			}
			$data['report_from'] = $data['report_from'] . " 09:00:00";
			$data['report_to'] = $data['report_to'] . " 08:59:59";
			
			$creditmemo_collection = $this->getInvoices($data);
			$results_collection = array();
			foreach($creditmemo_collection as $key=>$value){
				$creditmemoitems_collection = $this->getInvoiceItems($value['entity_id']);
				$value['creditmemo_items'] = $creditmemoitems_collection;
				$results_collection[] = $value;
			}
			return $results_collection;
	}

	public function getInvoiceItems($id){
		//$resource = Mage::getSingleton('core/resource');
		$readConnection = $this->getConnection();
		$sql = "SELECT DISTINCT `main_table`.name,`main_table`.sku,`main_table`.qty,`main_table`.base_price,`main_table`.base_row_total_incl_tax 
				FROM `sales_invoice_item` AS `main_table` 
				WHERE `main_table`.parent_id = " . $id .
			  " AND `main_table`.base_row_total_incl_tax IS NOT NULL
			    AND `main_table`.base_row_total >= 160 ";
		$result = $readConnection->fetchAll($sql);
		return $result;
	}

	public function getInvoices($data){
		$eu_countries = implode(",",array(
								"'BE'","'BG'","'CZ'","'DK'","'DE'","'EE'","'IE'","'GR'","'ES'","'FR'","'HR'","'IT'","'CY'","'LV'","'LT'","'LU'","'HU'","'MT'","'NL'","'AT'","'PL'",
								"'PT'","'RO'","'SI'","'SK'","'FI'","'SE'"
								));
		$readConnection = $this->getConnection();
		$sql = "SELECT  `main_table`.`entity_id`, `main_table`.`increment_id` AS `order_increment_id`, `main_table`.`created_at`, 
						`main_table`.`base_subtotal_incl_tax`,`main_table`.`base_discount_amount`, `main_table`.`base_tax_amount`, `main_table`.`base_shipping_incl_tax`, `main_table`.`base_grand_total`, 
						`main_table`.`base_subtotal_incl_tax`, (main_table.base_subtotal_incl_tax - main_table.base_subtotal) AS `item_tax` , `a`.`store_name`, `a`.`created_at` AS `purchased_on`, CONCAT(COALESCE(b.firstname, ''), ' ', COALESCE(b.lastname, '')) AS `shipto_name`, 
						`b`.`city` AS `shipto_city`, `b`.`region` AS `shipto_state`, `b`.`country_id`, `b`.`country_id` AS `region_code`, CONCAT(COALESCE(c.firstname, ''), ' ', 
						COALESCE(c.lastname, '')) AS `billto_name` 
				FROM `sales_invoice` AS `main_table` 
				JOIN `sales_order` AS `a` ON main_table.order_id = a.entity_id 
				JOIN `sales_order_address` AS `b` ON main_table.shipping_address_id = b.entity_id 
				JOIN `sales_order_address` AS `c` ON main_table.billing_address_id = c.entity_id ";
				
		$sql .= " WHERE main_table.created_at BETWEEN '" . $data['report_from'] . "' AND '" . $data['report_to'] . "'";  
		if(!isset($data['store_id']) || $data['store_id'] == ''){
			$sql .= " AND main_table.store_id IS NOT NULL";
		}
		else{
			$sql .= " AND main_table.store_id = " . $data['store_id'];
		}
			
		$sql .= " AND main_table.base_subtotal >= 160
				  AND `b`.`country_id` IN (" . $eu_countries . ")";
					
		$result = $readConnection->fetchAll($sql);
		return $result;
		
	}
	
	
	
}

