<?php

namespace Raw\CheckoutOrder\Plugin\Checkout\Block;

class LayoutProcessor {

  /**
   * Position the postcode field after address fields
   *
   * @param \Magento\Checkout\Block\Checkout\LayoutProcessor $subject
   * @param array $jsLayout
   *
   * @return array
   */
   public function afterProcess(
      \Magento\Checkout\Block\Checkout\LayoutProcessor $subject,
      array  $jsLayout
    ) {

       //Shipping Address
       $jsLayout['components']['checkout']['children']['steps']['children']['shipping-step']
       ['children']['shippingAddress']['children']['shipping-address-fieldset']
       ['children']['postcode']['sortOrder'] = 50;

      //Billing Address on payment method
       if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
           ['payment']['children']['payments-list']['children']
       )) {
           $paymentList = $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
           ['payment']['children']['payments-list']['children'];

           foreach ($paymentList as $key => $payment) {

               /* postcode */
               $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
                   ['payment']['children']['payments-list']['children'][$key]['children']['form-fields']['children']
                   ['postcode']['sortOrder'] = 50;
               }
       }

       //Billing Address on payment page
       if (isset($jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
           ['payment']['children']['afterMethods']['children']
       )) {

           /* postcode */
           $jsLayout['components']['checkout']['children']['steps']['children']['billing-step']['children']
           ['payment']['children']['afterMethods']['children']['billing-address-form']['children']['form-fields']
           ['children']['postcode']['sortOrder'] = 50;

       }
    return $jsLayout;
  }
}
?>
