<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\PaypalOverwrite\Model\Config\Source;

class Authentication implements \Magento\Framework\Option\ArrayInterface
{

    public function toOptionArray()
    {
        return [['value' => '0', 'label' => __('signature')],['value' => '1', 'label' => __('certificate')]];
    }

    public function toArray()
    {
        return ['0' => __('signature'),'1' => __('certificate')];
    }
}

