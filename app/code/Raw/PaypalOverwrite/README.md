# Mage2 Module Raw PaypalOverwrite

    ``raw/module-paypaloverwrite``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Raw`
 - Enable the module by running `php bin/magento module:enable Raw_PaypalOverwrite`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require raw/module-paypaloverwrite`
 - enable the module by running `php bin/magento module:enable Raw_PaypalOverwrite`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration

 - API Authentication Method (paypal_credit/settings/authentication)

 - API Username (paypal_credit/settings/api_username)

 - api_password (paypal_credit/settings/api_password)

 - API Username (paypal_credit/settings/api_signature)


## Specifications

 - Plugin
	- aroundGetValue - Magento\Paypal\Model\abstract > Raw\PaypalOverwrite\Plugin\Magento\Paypal\Model\Abstract


## Attributes



