<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Raw\PaypalOverwrite\Plugin\Magento\Paypal\Model\Api;
use Magento\Store\Model\ScopeInterface;

class AbstractApi
{
	public $checkoutSession;
	public function __construct(
	\Magento\Backend\Block\Template\Context $context,
	\Magento\Checkout\Model\Session $checkoutSession,
	\Magento\Framework\Registry $registry,
	\Magento\Sales\Api\Data\OrderInterface $order,
	\Magento\Framework\App\RequestInterface $request,
	\Magento\Store\Model\StoreManagerInterface $storeManager,
	\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig){
		$this->checkoutSession = $checkoutSession;
		$this->registry = $registry;
		$this->order = $order;
		$this->request = $request;
		$this->storeManager = $storeManager;
		$this->context = $context;
		$this->scopeConfig = $scopeConfig;
	}
	
    /*public function aroundGetValue(
        \Magento\Paypal\Model\AbstractConfig $subject,
        \Closure $proceed, $key, $storeId = null
    ) {
		$keytosubstitute = [
		'api_username' => 'paypal_credit/settings/api_username',
		'api_password' => 'paypal_credit/settings/api_password',
		'api_signature' => 'paypal_credit/settings/api_signature',
		'api_authentication' => 'paypal_credit/settings/authentication',
		];
		
		$default = true;
        //Your plugin code
		//var_dump($key);
		if(isset($keytosubstitute[$key])){
			
			$default = false;
			
		}
		
        if($default)	$result = $proceed($key, $storeId);
		else{
			//$resultold = $proceed($key, $storeId);
			$result = $this->scopeConfig->getValue(
				$keytosubstitute[$key],
				ScopeInterface::SCOPE_STORE,
				$storeId
			);
			//var_dump($result ." >>> ".$resultold);
		}
        return $result;
    }
	*/
	
	/*public function aroundGetApiUsername(
		\Magento\Paypal\Model\Api\AbstractApi $subject,
        \Closure $proceed){
		$default = false;
		$storeId = $this->storeManager->getStore()->getId();
		if($default)	$result = $proceed();
		else{
			//$resultold = $proceed($key, $storeId);
			$result = $this->scopeConfig->getValue(
				'paypal_credit/settings/api_username',
				ScopeInterface::SCOPE_STORE,
				$storeId
			);
			//var_dump($result ." >>> ".$resultold);
		}
        return $result;
	}
	*/
	
	public function selectPaypalAccount($storeId){
		/*$quote = Mage::getSingleton('checkout/session')->getQuote();
		$payment = Mage::registry("paypal_payment");
		if($payment){
			$order = Mage::getModel('sales/order')->load($payment->getParentId());
			if($order){
				$type = ($order->getPaypalAccount());
				if($type == "0pecent"){
					return true;
				}
				else{
					return false;
				}
			}
		}
		
		if(Mage::app()->getRequest()->getControllerName() == "product" &&  Mage::app()->getRequest()->getActionName() == "view"){
			return true;
		
		*/
		
		//exit;
		$quote = $this->checkoutSession->getQuote();
		$haszero =false;
		$zerocredattrset = array(9,26,34);
		if($quote){
            //$storeId = $this->storeManager->getStore()->getId();
			$cartItems = $quote->getAllVisibleItems();
			foreach ($cartItems as $item) {
				$product = $item->getProduct();
			    $productId = $item->getProductId();     
				$is_0_intrest = $product->getResource()->getAttributeRawValue($productId, 'is_0_intrest',$storeId);
				if(in_array($product->getAttributeSetId(), $zerocredattrset) && $is_0_intrest == "1"){
					$this->setPaypalAccount("0pecent");
					return "0pecent";
				}
				
			    // Do something more
			}
			/*if($product->getData("paypal_0for4") == "1"){
				if(strpos($type, "rest_client_id") !== false){
					return $this->CREDIT_CLIENT_ID_0FOR4;
				}
				else{
					return $this->CREDIT_SECRET_0FOR4;
				}
			}*/
		}
		$this->setPaypalAccount("0over4");
		return "0over4";
	}
	
	public function setPaypalAccount($type){
		$quote = $this->checkoutSession->getQuote();
		if($quote && $quote->getEntityId()){
			
			$quote->setPaypalAccount($type)->save();
		}
	}
}

