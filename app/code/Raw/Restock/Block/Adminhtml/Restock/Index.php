<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Block\Adminhtml\Restock;

class Index extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory $attributeFactory,
        array $data = []
    ) {
		$this->_attributeFactory = $attributeFactory;
        parent::__construct($context, $data);
    }
	
	public function getAttributesets(){
		$attributeSetCollection = $this->_attributeFactory->create();
		$attributeSets = $attributeSetCollection->getItems();
		return $attributeSets;
	}
}

