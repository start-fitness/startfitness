<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Block\Adminhtml\Restock;

class Manage extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeset,
		\Magento\Eav\Model\Attribute $eavAttribute,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $optcollection,
		\Magento\Catalog\Model\Product\Attribute\Repository $attrRep,
		\Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
		$this->_attributeSet = $attributeset;
		$this->_request = $request;
		$this->_eavAttribute = $eavAttribute;
		$this->_resource = $resource;
		$this->_optcollection = $optcollection;
		$this->_attrRep = $attrRep;
        parent::__construct($context, $data);
    }
	
	public function getAttributeset(){
		$id = $this->_request->getParam("id");
		//var_dump($id);exit;
		$attributeSet = $this->_attributeSet->get($id);
		return $attributeSet;
	}
	
	public function getAttributeCollection($id){
		$attributeCollection = $this->_eavAttribute->getCollection();
		return $attributeCollection->setAttributeSetFilter($id)
		->addFieldToFilter("frontend_input", "select")
		//->addFieldToFilter("is_configurable", "1")
		//->addFieldToFilter("is_global", "1")
		->getItems();
	}
	
	public function getRestockEntries($id){
		$this->connection = $this->_resource->getConnection('core_write');
		$sql = "SELECT * FROM `restockrates` WHERE attribute_set_id = {$id} ORDER BY tier ASC";
		return $this->connection->fetchAll($sql);
		
	}
	
	public function getAttributeOptions($id){
		$optionData = $this->_optcollection->create()
                ->setPositionOrder('asc')
                ->setAttributeFilter($id)
                ->setStoreFilter(0)
                ->load();
		return $optionData;
	}
	
	public function getBrands(){
		$attribute = $this->_attrRep->get('manufacturer');
		
		$options = $this->getAttributeOptions($attribute->getId());
		//var_dump($options->count()."");exit;
		return $options;
	}
}

