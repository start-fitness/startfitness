<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Block\Adminhtml\Product;

class Tab extends \Magento\Backend\Block\Template
{

    /**
     * Constructor
     *
     * @param \Magento\Backend\Block\Template\Context  $context
     * @param array $data
     */
    public function __construct(
		\Magento\Framework\Registry $registry,
        \Magento\Backend\Block\Template\Context $context,
		\Magento\Framework\App\Request\Http $request,
		\Magento\Catalog\Api\AttributeSetRepositoryInterface $attributeset,
		\Magento\Eav\Model\Attribute $eavAttribute,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $optcollection,
		\Magento\Catalog\Model\Product\Attribute\Repository $attrRep,
		\Magento\Framework\App\ResourceConnection $resource,
        array $data = []
    ) {
		$this->_coreRegistry = $registry;
		$this->_attributeSet = $attributeset;
		$this->_request = $request;
		$this->_eavAttribute = $eavAttribute;
		$this->_resource = $resource;
		$this->_optcollection = $optcollection;
		$this->_attrRep = $attrRep;
        parent::__construct($context, $data);
    }
	
	protected $_template = 'product/tab.phtml';
	
	public function getProduct()
    {
        return $this->_coreRegistry->registry('current_product');
    }
    /**
     * @return string
     */
    public function getData1()
    {
        //Your block code
        return __('Hello Developer! This how to get the storename: %1 and this is the way to build a url: %2', $this->_storeManager->getStore()->getName(), $this->getUrl('contacts'));
    }
	
	public function getProductTier(){
		
		//$resource = Mage::getSingleton('core/resource');
		//$readConnection = $resource->getConnection('core_read');
		$product = $this->getProduct();
		if(!$product){
			
		}
		$this->connection = $this->_resource->getConnection('core_write');
		$sql = "SELECT restockrates_id FROM restocklinks WHERE product_id = ".$product->getId();
		$tierselected = $this->connection->fetchCol($sql);
		return $tierselected;
	}
	public function isSimple(){
		$product = $this->getProduct();
		return ( $product->getTypeId() == 'simple' );
	}
	public function getAllowAttributes()
	{
		if($this->getProduct()->getTypeId() == "configurable")
			return $this->getProduct()->getTypeInstance()->getConfigurableAttributes($this->getProduct());
		else return [];
	}
}

