<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Observer\Catalog;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Event\ObserverInterface;
use \Magento\Catalog\Model\Product\Attribute\Repository;
use \Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use \Magento\Catalog\Model\Product\Action;

class ProductSaveAfter implements \Magento\Framework\Event\ObserverInterface
{
	
	
	public function __construct(
        Context $context,
        Registry $registry,
		Repository $productAttributeRepository,
		ConfigurableAttributeData $ConfigurableAttributeData,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
		\Magento\Framework\App\RequestInterface $request,
		\Magento\Framework\App\ResourceConnection $resource,
		Action $action
    )
    {
        $this->_coreRegistry = $registry;
		$this->productAttributeRepository = $productAttributeRepository;
		$this->ConfigurableAttributeData = $ConfigurableAttributeData;
		$this->stockItemRepository = $stockItemRepository;
		$this->request = $request;
		$this->_resource = $resource;
		$this->action = $action;
        //parent::__construct($context, $data);
    }

    /**
     * Execute observer
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(
        \Magento\Framework\Event\Observer $observer
    ) {
        //Your observer code
		
		try {
				$product = $observer->getEvent()->getProduct();
				$writeConnection = $this->connection = $this->_resource->getConnection('core_write');
				/**
				 * Perform any actions you want here
				 *
				 */
				$tier =   $this->request->getParam("restock_tier");
				if(is_numeric($tier) && $tier != 0){
					$sql = "
INSERT INTO restocklinks (product_id,restockrates_id) VALUES (".$product->getId().",".$tier.")
ON DUPLICATE KEY UPDATE restockrates_id=".$tier.";";
  				$writeConnection->query($sql);
				}
				elseif(is_numeric($tier) && $tier == -1){
					$sql = "
DELETE FROM restocklinks WHERE product_id = ".$product->getId()."";
  				$writeConnection->query($sql);
				}

				/**
				 * Uncomment the line below to save the product
				 *
				 */
				//$product->save();
			}
			catch (Exception $e) {
				//Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		
    }
}

