<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Controller\Adminhtml\Restock;

class Save extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
		\Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\App\ResourceConnection $resource
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->_request = $request;
		$this->_resource = $resource;
        parent::__construct($context);
    }


    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        //var_dump($this->getRequest()->getParams());
       $attr_set = $this->_request->getParam("id");
	   $data = $this->_request->getParam("data");
	   $compiled = array($attr_set => array());
	   foreach($data as $row){
	   		$key = (key($row));
		    $val = $row[$key];
			$ids = explode("-", $key);
			$attr = $ids[0];
			$brand = $ids[1];
			$gender = $ids[2];
			$tier = $ids[3];
			$attrval = $ids[4];
			
			if(!isset($compiled[$attr_set][$attr])) $compiled[$attr_set][$attr] = array();
			if(!isset($compiled[$attr_set][$attr][$brand])) $compiled[$attr_set][$attr][$brand] = array();
			if(!isset($compiled[$attr_set][$attr][$brand][$gender])) $compiled[$attr_set][$attr][$brand][$gender] = array();
			if(!isset($compiled[$attr_set][$attr][$brand][$gender][$tier])) $compiled[$attr_set][$attr][$brand][$gender][$tier] = array();
			
			$compiled[$attr_set][$attr][$brand][$gender][$tier][$attrval] = $val;
	   }
	   foreach($compiled[$attr_set] as $attr => $brands){
	   		foreach($brands as $brand => $genders){
	   			foreach($genders as $gender => $tiers){
	   				foreach($tiers as $tier => $attrvals){
	   					$ordered = array();
						$unordered = array();
	   					foreach($attrvals as $key => $val){
	   						$ordered[] = array("id"=>$key,"val"=> $val);
							$unordered[$key] = array("id"=>$key,"val"=> $val);
	   					}
	   					$data = addslashes(json_encode(array("ordered"=>$ordered, "unordered" => $unordered)));
						
						$sql = 'INSERT INTO restockrates (attribute_set_id, attribute_ids, brand_id, gender_id, tier, data)
								VALUES ("'.$attr_set.'", "'.$attr.'", "'.$brand.'", "'.$gender.'", "'.$tier.'", "'.$data.'")
								ON DUPLICATE KEY UPDATE data = "'.$data.'"';
						
						$this->connection = $this->_resource->getConnection('core_write');
						$this->connection->query($sql);
					}
				}
			}
			
	   }
    }
}

