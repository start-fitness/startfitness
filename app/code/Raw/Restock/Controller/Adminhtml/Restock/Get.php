<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\Restock\Controller\Adminhtml\Restock;

class Get extends \Magento\Backend\App\Action
{

    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Backend\App\Action\Context  $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
		\Magento\Framework\App\Request\Http $request,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
		\Magento\Framework\App\ResourceConnection $resource,
		\Magento\Eav\Model\ResourceModel\Entity\Attribute\Option\CollectionFactory $optcollection
    ) {
        $this->resultPageFactory = $resultPageFactory;
		$this->_request = $request;
		$this->_resource = $resource;
		$this->_optcollection = $optcollection;
        parent::__construct($context);
    }


    /**
     * Execute view action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        //var_dump($this->getRequest()->getParams());

	    $attr_set = $this->_request->getParam("id");
		$attr = $this->_request->getParam("attr");
		$brand = $this->_request->getParam("brand");
		$gender = $this->_request->getParam("gender");
	   
		
		
		$sql = "SELECT * FROM restockrates WHERE attribute_set_id = {$attr_set} AND attribute_ids = {$attr} AND gender_id = {$gender} AND brand_id = {$brand} ORDER BY tier ASC";
		//var_dump($sql);
		$this->connection = $this->_resource->getConnection('core_write');
		$restocks = $this->connection->fetchAll($sql);
		$attributesvalarr = array();
		$attributesvalarrordered = array();

		$valuesCollection = $this->getAttributeOptions($attr);
		$attributesvalarr[$attr] = array();
		foreach($valuesCollection as $rowtmp){
			$attributesvalarr[$rowtmp->getId()] = array("id"=> $rowtmp->getId(), "value"=>$rowtmp->getValue());
			$attributesvalarrordered[] = array("id"=> $rowtmp->getId(), "value"=>$rowtmp->getValue());
				
			
		}
		
		$return = array();
		foreach($restocks as $restock){
			$return[] = $restock;
		}
		header('Content-Type: application/json');
		echo json_encode(array("data" => $return, "attrs"=> $attributesvalarr));
    }
	
	public function getAttributeOptions($id){
		$optionData = $this->_optcollection->create()
                ->setPositionOrder('asc')
                ->setAttributeFilter($id)
                ->setStoreFilter(0)
                ->load();
		return $optionData;
	}
}

