<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\SageTradeExport\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class ExportTradeOrders extends AbstractHelper
{

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
		\Magento\Sales\Model\ResourceModel\Order\CollectionFactory $orderCollectionFactory,
		\Magento\Framework\Filesystem\DirectoryList $dir,
		\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
		\Magento\Customer\Api\CustomerRepositoryInterface $customerRepositoryInterface,
		\Magento\Framework\App\ResourceConnection $resourceConnection,
		\Magento\Directory\Model\CountryFactory $countryFactory
    ) {
		$this->resourceConnection = $resourceConnection;
		$this->_orderCollectionFactory = $orderCollectionFactory;
		$this->scopeConfig = $scopeConfig;
		$this->_dir = $dir;
		$this->_customerRepositoryInterface = $customerRepositoryInterface;
		$this->countryFactory = $countryFactory;
        parent::__construct($context);
    }
	
	public function runExport(){
		$shouldrun = 1; 
		if($shouldrun != 1){
			return false;
		}
		
		$connection = $this->resourceConnection->getConnection();
		
		
		
		$cutoff = "2020-03-01 00:00:00";//Mage::getStoreConfig('dmsettings/dmsettingsgeneral/dmexportcutoff', $store_id);
		//$lastran = date("Y-m-d H:i:s", strtotime("- 14 days",strtotime($exportlast->last_processed)));
		//var_dump($lastran);
		//if($lastran == null) $lastran = $cutoff;
		
		
		
		$currpage = 1;
		$lastorders = $this->_orderCollectionFactory->create()
		 //->addAttributeToFilter("created_at", array("gt"=> $cutoff))
		 ->addAttributeToFilter("updated_at", array("gt"=> $cutoff))
		 ->addAttributeToFilter("status", array("eq"=> "complete"))
		 ->addAttributeToFilter("main_table.store_id", array("in"=> "33"))
	     ->setOrder('entity_id','ASC')
	     ->setPageSize(100);
		 $lastorders->getSelect()->joinLeft(array("track"=>"raw_sage_export_track"), "`main_table`.entity_id=track.order_id", null);
		 $lastorders->getSelect()->join(array("sageid"=>"customer_entity_varchar"), "`main_table`.customer_id=sageid.entity_id", ['sageref'=>'sageid.value']);
		 $lastorders->getSelect()->where(("track.store_id IS NULL"));
		 //var_dump((string)$lastorders->getSelect()."");exit;
		 $lastpage = $lastorders->getLastPageNumber();
		 $lastnotprocess = false;
		 while($currpage <= $lastpage){
		 	
		 	$lastorders->clear();
			$lastorders->setCurPage($currpage);
			
			foreach($lastorders as $order){
				$lastnotprocess = false;
				//echo "{$order->getIncrementId()}\n";
				
				$xml = $this->generateXml($order->getStoreId(), $order);
				//echo "{$xml}\n";
				if($xml === false) {
					$currpage++;	
					$lastnotprocess = true;
					continue;
				}
				$dom = dom_import_simplexml($xml)->ownerDocument;
				$dom->formatOutput = true;
				//var_dump($dom->saveXML());exit;
				try{
				$this->saveToFile($dom, $order->getStoreId(), $order->getIncrementId());
				}
				catch(exception $e){
					var_dump($e->getMessage());
				}
				$writeConnection = $connection;
				$writeConnection->query("INSERT INTO raw_sage_export_track (order_id, store_id) VALUES (".$order->getEntityId().", {$order->getStoreId()})");
			}
			
			$currpage++;
		 }
		 //var_dump(date("Y-m-d H:i:s"));
		 /*if(!$lastnotprocess){
			 $writeConnection = $connection;
			 $writeConnection->query("UPDATE `datamorph_export_tracker` SET last_processed = NOW() WHERE id = ".$exportlast->getId());
		 }*/
		
		
	}
	
	public function saveToFile($dom, $store_id, $filename){
		$path = $this->_dir->getPath('var')."/sageexport/";
		//$path = Mage::getStoreConfig('dmsettings/dmsettingsgeneral/dmexport', $store_id);
		//$path = Mage::getStoreConfig('dmsettings/dmsettingsgeneral/dmexportpath');
        //var_dump($store_id, $path);exit;
		if($path && $path != ""){
			
			$path = rtrim($path, "/");
			
			if(!is_dir($path)) (mkdir($path, 0777, true));
			var_dump($path."/".$filename.".xml");
			$dom->save($path."/".$filename.".xml");
			
			//exit;
			
			
		}
	}
	
	public function generateXml($store_id, $order){
		//var_dump(($order->getCreatedAtStoreDate()->toString(Zend_Date::DAY."/".Zend_Date::MONTH."/".Zend_Date::YEAR)));exit;
		$billingAddress = $order->getBillingAddress();
		$shippingAddress = $order->getShippingAddress();
		//$address = $order->getBillingAddress()->getData();
		$eu_countries = $this
		->scopeConfig
		->getValue(
			'general/country/eu_countries'
		);
		$eu_countries_array = explode(',',$eu_countries);
		if(in_array("GB", $eu_countries_array)){
			unset($eu_countries_array[array_search("GB", $eu_countries_array)]);
		}
		//var_dump($eu_countries_array);
		//exit;
		$iseu = 2;
		if($shippingAddress['country_id'] != "GB" && in_array($shippingAddress['country_id'], $eu_countries_array) && ($store_id == "33" || $store_id == "23")){
			$iseu = 4;
		}
		
		
		
		//var_dump($billingAddress->getData());exit;
		
		
		$orderdata = array();
		$orderdata['ORDER_TYPE'] = 1;
		$orderdata['ORDER_NUMBER'] = $order->getData('increment_id');
		$payment = $order->getPayment();
		$customer_data = $this->_customerRepositoryInterface->getById($order->getData("customer_id"));
		
		var_dump($order->getSageref());
		if($customer_data){
			if($order->getSageref() && $order->getSageref() != "")
				$orderdata['CUST_ACC_REF'] =$order->getSageref();
			else
				return false;//$orderdata['CUST_ACC_REF'] = (substr("MM".$customer_data->getID(), 0, 8));
		}
		else
			$orderdata['CUST_ACC_REF'] = "WEBTRADE";
		
		$orderdata['ORDER_DATE'] = date("Y/m/d", strtotime($order->getCreatedAt()));
		$orderdata['ORDER_PROMISED_DATE'] = $orderdata['ORDER_DATE'];
		$orderdata['CUSTOMER_ORDER_NUMBER'] = $order->getData('increment_id')."/".$payment->getPoNumber();
		
		$orderdata['INV_POSTAL_NAME'] = $billingAddress->getData('firstname'). " " . $billingAddress->getData('lastname');
		$orderdata['INV_ADDRESS_LINE_1'] = $billingAddress->getData('street');
		$orderdata['INV_ADDRESS_LINE_2'] = "";//$billingAddress->getData('');
		$orderdata['INV_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
		$orderdata['INV_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
		$orderdata['INV_POST_CODE'] = $billingAddress->getData('postcode');
		$orderdata['INV_CITY'] = $billingAddress->getData('city');
		$orderdata['INV_COUNTY'] = $billingAddress->getData('region');
		$orderdata['INV_CONTACT'] = $billingAddress->getData($billingAddress->getData('firstname'). " " . $billingAddress->getData('lastname'));
		$orderdata['INV_TELEPHONE'] = $billingAddress->getData('telephone');
		$orderdata['INV_FAX'] = "";//$billingAddress->getData('');
		$orderdata['INV_EMAIL'] = $billingAddress->getData('email');
		$orderdata['INV_TAX_NUMBER'] = ($billingAddress->getData('vat_id') != null) ? $billingAddress->getData('vat_id') : "";
		$orderdata['INV_TAX_CODE'] = "";//$billingAddress->getData('');
		$orderdata['INV_COUNTRY_CODE'] = $billingAddress->getData('country_id');
		$orderdata['INV_COUNTRY'] = $this->countryFactory->create()->loadByCode($billingAddress->getData('country_id'))->getName();
		
		if($shippingAddress){
			$orderdata['DEL_POSTAL_NAME'] = $shippingAddress->getData('firstname'). " " . $shippingAddress->getData('lastname');
			$orderdata['DEL_ADDRESS_LINE_1'] = $shippingAddress->getData('street');
			$orderdata['DEL_ADDRESS_LINE_2'] = "";//$billingAddress->getData('');
			$orderdata['DEL_ADDRESS_LINE_3'] = "";//$billingAddress->getData('');
			$orderdata['DEL_ADDRESS_LINE_4'] = "";//$billingAddress->getData('');
			$orderdata['DEL_POST_CODE'] = $shippingAddress->getData('postcode');
			$orderdata['DEL_CITY'] = $shippingAddress->getData('city');
			$orderdata['DEL_COUNTY'] = $shippingAddress->getData('region');
			$orderdata['DEL_CONTACT'] = $shippingAddress->getData($shippingAddress->getData('firstname'). " " . $shippingAddress->getData('lastname'));
			$orderdata['DEL_TELEPHONE'] = $shippingAddress->getData('telephone');
			$orderdata['DEL_FAX'] = "";//$billingAddress->getData('');
			$orderdata['DEL_EMAIL'] = $shippingAddress->getData('email');
			$orderdata['DEL_TAX_NUMBER'] = ($shippingAddress->getData('vat_id') != null) ? $shippingAddress->getData('vat_id') : "";
			$orderdata['DEL_TAX_CODE'] = "";//$billingAddress->getData('');
			$orderdata['DEL_COUNTRY_CODE'] = $shippingAddress->getData('country_id');
			$orderdata['DEL_COUNTRY'] =$this->countryFactory->create()->loadByCode($shippingAddress->getData('country_id'))->getName();
		}
		
		$orderdata['PAYMENT_IN_FULL'] = "NO";
		//$orderdata['PAYMENT_VALUE'] = $order->getData('');
		//$orderdata['PAYMENT_REF'] = $order->getData('');
		//$orderdata['PAYMENT_METHOD'] = $order->getData('');
		
		$orderdata['ARE_PRICES_TAX_INCLUSIVE'] = "YES";
		
		$items = $order->getAllVisibleItems();
		$i = 1;
		foreach($items as $item){
			if($item->getProduct() == null) continue;
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = "S1"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = $item->getProduct()->getData('actual_model') . "-" .$item->getData('name');
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = (int)$item->getData('qty_ordered');
			$unitprice = (float)$item->getData('row_total') / (int)$item->getData('qty_ordered');
			if($unitprice - floor($unitprice) == 0.33)
				$unitprice = floor($unitprice) + 0.33333;
			elseif($unitprice - floor($unitprice) == 0.66)
				$unitprice = floor($unitprice) + 0.66666;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $unitprice;
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = $item->getData('tax_amount');
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = ($item->getData('base_cost') !=  null) ? $item->getData('base_cost') : "0";
			$orderdata['ITEM_'.$i.'_TAXCODE'] = ((float)$item->getData('tax_amount') == 0)? $iseu : 1;
			//var_dump($orderdata);exit;
			$i++;
		}
		if($order->getBaseShippingInclTax() > 0){
			$orderdata['ITEM_'.$i.'_LINE_TYPE'] = 1;
			$orderdata['ITEM_'.$i.'_STOCK_CODE'] = "CARRIAGE"/*$item->getData('sku')*/;
			$orderdata['ITEM_'.$i.'_DESC'] = "Carriage";
			$orderdata['ITEM_'.$i.'_QTY_ORDER'] = 1;
			$orderdata['ITEM_'.$i.'_UNITPRICE'] = $order->getBaseShippingInclTax() - $order->getBaseShippingTaxAmount();
			$orderdata['ITEM_'.$i.'_TAXAMOUNT'] = 0;
			$orderdata['ITEM_'.$i.'_COSTPRICE'] = $order->getBaseShippingInclTax()- $order->getBaseShippingTaxAmount();
			$orderdata['ITEM_'.$i.'_TAXCODE'] = ($order->getBaseShippingTaxAmount() > 0) ? 1 : $iseu;
			$i++;
		}
		$orderdata['ITEMS_COUNT'] = $i;
		$orderdata['DO_INVOICE'] = "0";
		$orderdata['DO_SHIPPED'] = "0";
		$orderdata['DO_POSTED'] = "0";
		$orderdata['TAKEN_BY'] = "Magento";
		
		//var_dump($orderdata);
		
		$xml = array("data"=> $orderdata);
		$xml_data = new \SimpleXMLElement('<?xml version="1.0"?><form></form>');
		$this->arrayToXml($xml,$xml_data);
		
		return $xml_data;
		
	}
	
	public function setLastOrder($exportobj, $store_id){
		//var_dump($store_id);exit;
		
	}
	
	public function arrayToXml( $data, &$xml_data ) {
	    foreach( $data as $key => $value ) {
	        if( is_array($value) ) {
	            if( is_numeric($key) ){
	                $key = 'item'.$key; //dealing with <0/>..<n/> issues
	            }
	            $subnode = $xml_data->addChild($key);
	            $this->arrayToXml($value, $subnode);
	        } else {
	            $xml_data->addChild("$key",htmlspecialchars("$value"));
	        }
	     }
	}
}

