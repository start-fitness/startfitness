# Mage2 Module Raw SageTradeExport

    ``raw/module-sagetradeexport``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Raw`
 - Enable the module by running `php bin/magento module:enable Raw_SageTradeExport`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require raw/module-sagetradeexport`
 - enable the module by running `php bin/magento module:enable Raw_SageTradeExport`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Cronjob
	- raw_sagetradeexport_exporttradeorders

 - Helper
	- Raw\SageTradeExport\Helper\ExportTradeOrders

 - Plugin
	- afterDisplayBothPrices - Magento\Tax\Helper\Data > Raw\SageTradeExport\Plugin\Magento\Tax\Helper\Data


## Attributes



