<?php

namespace Raw\VatPopup\Plugin;

use Magento\Checkout\Model\Session;
use Magento\Customer\Model\Address;

class RecollectTotalsPlugin
{
    /**
     * @var \Magento\Checkout\Model\Session
     */
    private $checkoutSession;

    public function __construct(Session $checkoutSession)
    {
        $this->checkoutSession = $checkoutSession;
    }

    public function afterSave(Address $subject, $result)
    {
		//var_dump($subject->getData());exit;
		$quote = $this->checkoutSession->getQuote();
		if($subject->getData("default_billing") === true){
			$address = $subject->getData();
 
			//now setting the address as the quote billing address
			$quote->getBillingAddress()->setFirstname($address['firstname']);
			$quote->getBillingAddress()->setLastname($address['lastname']);
			$quote->getBillingAddress()->setStreet($address['street']);
			$quote->getBillingAddress()->setCity($address['city']);
			$quote->getBillingAddress()->setTelephone($address['telephone']);
			$quote->getBillingAddress()->setPostcode($address['postcode']);
			$quote->getBillingAddress()->setCountryId($address['country_id']);
		}
		if($subject->getData("default_shipping") === true){
			$address = $subject->getData();
 
			//now setting the address as the quote billing address
			$quote->getShippingAddress()->setFirstname($address['firstname']);
			$quote->getShippingAddress()->setLastname($address['lastname']);
			$quote->getShippingAddress()->setStreet($address['street']);
			$quote->getShippingAddress()->setCity($address['city']);
			$quote->getShippingAddress()->setTelephone($address['telephone']);
			$quote->getShippingAddress()->setPostcode($address['postcode']);
			$quote->getShippingAddress()->setCountryId($address['country_id']);
		}
        
        $this->checkoutSession->getQuote()->collectTotals()->save();
        return $result;
    }
}