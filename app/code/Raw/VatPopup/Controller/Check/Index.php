<?php


namespace Raw\VatPopup\Controller\Check;

use \Magento\Checkout\Model\Session as CheckoutSession;
use \Magento\Tax\Model\Calculation\Rate;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;
class Index extends \Magento\Framework\App\Action\Action
{

	protected $resultPageFactory;
	protected $jsonHelper;
	protected $checkoutSession;
	protected $_taxModelConfig;
	protected $_coreSession;
	protected $request;

	/**
	 * Constructor
	 *
	 * @param \Magento\Framework\App\Action\Context  $context
	 * @param \Magento\Framework\Json\Helper\Data $jsonHelper
	 */

	public function __construct(
	\Magento\Framework\App\Action\Context $context,
	\Magento\Framework\View\Result\PageFactory $resultPageFactory,
	\Magento\Framework\Json\Helper\Data $jsonHelper,
	CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
	CheckoutSession $checkoutSession,
	Rate $taxModelConfig,
	\Magento\Framework\App\Request\Http $request)
	{
		$this->resultPageFactory = $resultPageFactory;
		$this->jsonHelper        = $jsonHelper;
		$this->checkoutSession   = $checkoutSession;
		$this->_taxModelConfig = $taxModelConfig;
		$this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
		$this->request = $request;
		parent::__construct($context);
	}

	/**
	 * Execute view action
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function execute()
	{
		try {
			$countryidnow = ($this->request->getParam('countryId'));
			$countryidPrev = ($this->request->getParam('countryIdPrev'));
			
			if($countryidPrev){
				$res = true;
				$beforerate = $this->getRateByCountry($countryidPrev);
				$nowrate = $this->getRateByCountry($countryidnow);
				if($beforerate['rate']== $nowrate['rate'])
					$res = false;
				return $this->jsonResponse(["show"=>$res, "debug"=>$countryidPrev. " ".$countryidnow ." ".$beforerate['rate']." ".$nowrate['rate']]);
				
			}
			$countryidbefore = 	$this->getCurrentTax();

			$this->setCurrentTax($countryidnow);
			$res = ($countryidbefore != $countryidnow)? true : false;
			if($countryidbefore == null) $res =false;

			$beforerate = $this->getRateByCountry($countryidbefore);
			$nowrate = $this->getRateByCountry($countryidnow);
			if($beforerate['rate']== $nowrate['rate'])
				$res = false;
			/*$quote = $this->checkoutSession->getQuote();

			$current = ($quote->getShippingAddress() && $quote->getShippingAddress()->getAppliedTaxes())?$quote->getShippingAddress()->getAppliedTaxes():null;
			$previous = $this->getCurrentTax();
			$this->setCurrentTax($current);*/

			return $this->jsonResponse(["show"=>$res, "debug"=>$countryidbefore. " ".$countryidnow ." ".$beforerate['rate']." ".$nowrate['rate']]);
		}
		catch (\Magento\Framework\Exception\LocalizedException $e) {
			return $this->jsonResponse($e->getMessage());
		}
		catch (\Exception $e) {
			//$this->logger->critical($e);
			return $this->jsonResponse($e->getMessage());
		}
	}

	public function taxestoArray()
    {
        $taxRates = $this->_taxModelConfig->getCollection()->getData();
        $taxArray = array();
        foreach ($taxRates as $tax) {
        	if($tax["rate"] < 1) continue;
            $taxRateId = $tax['tax_calculation_rate_id'];
            $taxCode = $tax["code"];
            $taxRate = $tax["rate"];
            $taxName = $taxCode.'('.$taxRate.'%)';
            $taxArray[$taxRateId] = $tax;
        }
		//var_dump($taxArray);
        return $taxArray;
    }

	public function getRateByCountry($country){
		$rates = $this->taxestoArray();
		foreach($rates as $rate){
			if($rate['tax_country_id'] == $country)
				return $rate;
		}
		return ["rate"=>0];
	}

	public function getCurrentTax(){
		//$this->_coreSession->start();
		//var_dump($this->_coreSession->getCurrentVatRates());
    	return $this->cookieManager->getCookie("current_vat_rate");
	}

	public function setCurrentTax($data){
	    //$this->_coreSession->start();
		$metadata = $this->cookieMetadataFactory
            ->createPublicCookieMetadata()
            ->setDuration(90000)
            ->setPath($this->sessionManager->getCookiePath())
            ->setDomain($this->sessionManager->getCookieDomain());

        $this->cookieManager->setPublicCookie(
            "current_vat_rate",
            $data,
            $metadata
        );
		//var_dump($data);

	}

	/**
	 * Create json response
	 *
	 * @return \Magento\Framework\Controller\ResultInterface
	 */
	public function jsonResponse($response = '')
	{
		return $this->getResponse()->representJson($this->jsonHelper->jsonEncode($response));
	}
}
