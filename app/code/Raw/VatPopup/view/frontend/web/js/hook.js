define([
    'ko',
    'jquery',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-service',
    'Magento_Checkout/js/model/shipping-rate-processor/new-address',
	'Magento_Checkout/js/model/cart/totals-processor/default',
    'Magento_Checkout/js/model/cart/cache',
	'Magento_Customer/js/customer-data',
    'mage/url'
], function (ko, $, Component, quote, shippingService, shippingProcessor, defaultTotal, cartCache, customerData, urlBuilder) {
    'use strict';

    return Component.extend({
		modal : null,
        initialize: function () {
            //this.insertPolyfills();
            var that = this;
            this._super();
            var prevBillAddress,
                prevShippingAddress,
                self = this;
			
			
			
			if($("body").hasClass("checkout-cart-index") || $("body").hasClass("customer-address-form") ) return this;
			
			console.log($("body").hasClass("checkout-cart-index") , $("body").hasClass("customer-address-form"), $("#vatpopup"));
			
            quote.shippingAddress.subscribe(function (newShippingAddress) {
               
                var url = urlBuilder.build("raw_vatpopup/Check/Index");
                var objsend = {};
                var keys = Object.keys(newShippingAddress);
                var vals = Object.values(newShippingAddress)
                for(var i =0; i < vals.length; i++){
                	if(!that.isAnObject(vals[i])) objsend[keys[i]] = (vals[i] == undefined)? null : vals[i].toString();
                }
                console.log(url,newShippingAddress,objsend,Object.keys(newShippingAddress));
                 $.ajax({
	                showLoader: false,
	                url: url,
	                data: objsend,
	                type: "GET",
	                dataType: 'json'
	            }).done(function (data) {
	                console.log(data);
	                if(data.show){
						cartCache.set('totals',null);
						defaultTotal.estimateTotals();
						var sections = ['cart'];
						setTimeout(function(){customerData.invalidate(sections);},1000);
						//customerData.reload(sections, true);
	                	$("#vatpopup").modal('openModal');
	                }
	                
	            });
            });

           

            return this;
        },
		isAnObject : function (obj)
		{
		    if(obj==null) return false;
		    return obj.constructor.name.toLowerCase() === "object"
		}
        
    });
});
