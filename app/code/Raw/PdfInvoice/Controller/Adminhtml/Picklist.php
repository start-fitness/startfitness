<?php
 
namespace Raw\PdfInvoice\Controller\Adminhtml;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
 
class Picklist extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    
    
    public function __construct(Context $context, Filter $filter, CollectionFactory $collectionFactory)
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
    }
    
    protected function massAction(AbstractCollection $collection)
    {
       $orderIds = $collection->getAllIds(); // Get the selected orders
       var_dump($orderIds);
       die("ok");
    }
    
    protected function _isAllowed() 
    {
        return $this->_authorization->isAllowed('Raw_PdfInvoice::picklist');
    }
}
 