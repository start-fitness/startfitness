<?php
 
namespace Raw\PdfInvoice\Controller\Adminhtml\Picklist;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
 
class Mass extends \Magento\Sales\Controller\Adminhtml\Order\AbstractMassAction
{
    
    
    public function __construct(
	Context $context, 
	Filter $filter,
	CollectionFactory $collectionFactory,
	\Magento\Sales\Model\Order\Pdf\Invoice $invoice,
	\Magento\Framework\Stdlib\DateTime\DateTime $datetime,
	\Magento\Sales\Api\Data\OrderInterface $order,
	\Raw\PdfInvoice\Model\Picklist $picklist,
	\Magento\Framework\App\Response\Http\FileFactory $fileFactory)
    {
        parent::__construct($context, $filter);
        $this->collectionFactory = $collectionFactory;
		$this->datetime =  $datetime;
		$this->invoice =  $invoice;
		$this->order =  $order;
		$this->picklist =  $picklist;
		$this->_fileFactory = $fileFactory;
    }
    
    protected function massAction(AbstractCollection $collection)
    {
       $orderIds = $collection->getAllIds(); // Get the selected orders
       $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
	   $invoices = [];
	   foreach($orderIds as $orderid){
		   $orderfact = $objectManager->create('\Magento\Sales\Model\OrderFactory');
		   $orderdetails = $orderfact->create()->load($orderid);
		    
		   foreach ($orderdetails->getInvoiceCollection() as $invoice)
			{
				//var_dump($orderdetails->getEntityId() . "-".$invoice->getEntityId());
				$invoices[] = $invoice->getEntityId();
			}
	   }
	   if (count($invoices) > 0) {
                $pdf = $this->picklist->getPdf($invoices);
                $date = $this->datetime->date('Y-m-d_H-i-s');
                $fileContent = ['type' => 'string', 'value' => $pdf->render(), 'rm' => true];

                return $this->_fileFactory->create(
                    'invoice' . $date . '.pdf',
                    $fileContent,
                    DirectoryList::VAR_DIR,
                    'application/pdf'
                );
            }
       //die("ok");
    }
    
    protected function _isAllowed() 
    {
        return $this->_authorization->isAllowed('Raw_PdfInvoice::picklist');
    }
}
