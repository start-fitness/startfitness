# Mage2 Module Raw PdfInvoice

    ``raw/module-pdfinvoice``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Configuration](#markdown-header-configuration)
 - [Specifications](#markdown-header-specifications)
 - [Attributes](#markdown-header-attributes)


## Main Functionalities


## Installation
\* = in production please use the `--keep-generated` option

### Type 1: Zip file

 - Unzip the zip file in `app/code/Raw`
 - Enable the module by running `php bin/magento module:enable Raw_PdfInvoice`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 2: Composer

 - Make the module available in a composer repository for example:
    - private repository `repo.magento.com`
    - public repository `packagist.org`
    - public github repository as vcs
 - Add the composer repository to the configuration by running `composer config repositories.repo.magento.com composer https://repo.magento.com/`
 - Install the module composer by running `composer require raw/module-pdfinvoice`
 - enable the module by running `php bin/magento module:enable Raw_PdfInvoice`
 - apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`


## Configuration




## Specifications

 - Plugin
	- aroundTest - Magento\Sales\Model\Order\Pdf\Invoice > Raw\PdfInvoice\Plugin\Magento\Sales\Model\Order\Pdf\Invoice

 - Plugin
	- aroundTest - Magento\Sales\Model\Order\Pdf\AbstratcPdf > Raw\PdfInvoice\Plugin\Magento\Sales\Model\Order\Pdf\AbstratcPdf

 - Plugin
	- aroundTest - Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice > Raw\PdfInvoice\Plugin\Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice


## Attributes



