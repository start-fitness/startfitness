<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Raw\PdfInvoice\Model;

/**
 * Sales Order Invoice Pdf default items renderer
 */
class DefaultInvoice extends \Magento\Sales\Model\Order\Pdf\Items\Invoice\DefaultInvoice
{
   
	/**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
		\Magento\Catalog\Api\ProductRepositoryInterface $product,
        \Magento\Framework\Registry $registry,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Framework\Stdlib\StringUtils $string,
		\Magento\CatalogInventory\Api\StockRegistryInterface $stockItemRepository,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->string = $string;
		$this->stockItemRepository = $stockItemRepository;
		$this->productRepositoryInterface = $product;
        parent::__construct(
            $context,
            $registry,
            $taxData,
            $filesystem,
            $filterManager,
			$string,
            $resource,
            $resourceCollection,
            $data
        );
    }
	
    /**
     * Draw item line
     *
     * @return void
     */
    public function draw()
    {
        $order = $this->getOrder();
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
		$product = $this->productRepositoryInterface->get($this->getSku($item));
        $lines = [];

        // draw Product name
        $lines[0] = [
            [
                // phpcs:ignore Magento2.Functions.DiscouragedFunction
                'text' => $this->string->split(($product->getBinLocation()), 35, true, true),
                'feed' => 35
            ]
        ];
		
		// draw Product name
        $lines[0][] = 
            [
                // phpcs:ignore Magento2.Functions.DiscouragedFunction
                'text' => $this->string->split(($item->getName()), 60, true, true),
                'feed' => 190
            ]
        ;

        // draw SKU
        $lines[0][] = [
            // phpcs:ignore Magento2.Functions.DiscouragedFunction
            'text' => $this->string->split(html_entity_decode($product->getActualModel()), 25),
            'feed' =>90,
            'align' => 'left',
        ];

        // draw QTY
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 480, 'align' => 'right', 'font' => ($item->getQty() > 1)?'bold':'normal', 'font_size' => ($item->getQty() > 1)?'14':'10'];

        // draw item Prices
        $i = 0;
        $prices = $this->getItemPricesForDisplay();
        $feedPrice = 235;
        $feedSubtotal = 230;
        foreach ($prices as $priceData) {
            if (isset($priceData['label'])) {
                // draw Price label
                //$lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedPrice, 'align' => 'right'];
                // draw Subtotal label
                $lines[$i][] = ['text' => $priceData['label'], 'feed' => $feedSubtotal, 'align' => 'right'];
                $i++;
            }
            /*// draw Price
            $lines[$i][] = [
                'text' => $priceData['price'],
                'feed' => $feedPrice,
                'font' => 'bold',
                'align' => 'right',
            ];
			*/
			$feedSubtotal = 530;
            // draw Subtotal
            $lines[$i][] = [
                'text' => $priceData['subtotal'],
                'feed' => $feedSubtotal,
                'font' => 'bold',
                'align' => 'right',
            ];
            $i++;
        }

        // draw Tax
        /*$lines[0][] = [
            'text' => $order->formatPriceTxt($item->getTaxAmount()),
            'feed' => 495,
            'font' => 'bold',
            'align' => 'right',
        ];*/

        // custom options
        $options = $this->getItemOptions();
        if ($options) {
            foreach ($options as $option) {
                // draw options label
                /*$lines[][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']), 40, true, true),
                    'font' => 'italic',
                    'feed' => 35,
                ];*/

                // Checking whether option value is not null
                if ($option['value'] !== null) {
                    if (isset($option['print_value'])) {
                        $printValue = $option['print_value'];
                    } else {
                        $printValue = $this->filterManager->stripTags($option['value']);
                    }
                    $values = explode(', ', $printValue);
                    foreach ($values as $value) {
						$value = $this->filterManager->stripTags($option['label']).": ".$value;
                        $lines[][] = ['text' => $this->string->split($value, 50, true, true), 'feed' => 190, 'font'=>'bold'];
                    }
                }
            }
        }
		if($product->getData("bin_overstock_location") !== null && !empty($product->getData("bin_overstock_location"))){
			$lines[][] = ['text' => $this->string->split("Overstock: ".$product->getData("bin_overstock_location"), 50, true, true), 'feed' => 190];
		}
		
		$lineBlock = ['lines' => $lines, 'height' => 10];
		$tickbox_y = $pdf->y;
        $page = $pdf->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);
		
		if($tickbox_y < $pdf->y) $tickbox_y = $pdf->y + 20;
		
		$page->setFillColor(new \Zend_Pdf_Color_Rgb(1, 1, 1));
		$page->setLineColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));
		$page->setLineWidth(0.5);
		$page->drawRectangle((555), ($tickbox_y+10), (565), ($tickbox_y));
		$page->setFillColor(new \Zend_Pdf_Color_Rgb(0, 0, 0));
		
        
		$stock = $this->stockItemRepository->getStockItem($product->getEntityId());
		if(($stock->getQty() < 1) )
		{
			//$this->_setFont($page, $font_style_body, ($font_size_body-2), $font_family_body, $non_standard_characters, $font_color_body);

			$pdf->y -= 12;
			$page->setFillColor(new \Zend_Pdf_Color_Html('lightCoral'));
			$page->setLineColor(new \Zend_Pdf_Color_Rgb(1,1,1));
			$page->drawRectangle(30, ($pdf->y-4), 565, ($pdf->y+10));
			$page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
			$warning = 'Stock Warning      SKU: '.$product->getSku().'    Net Stock After All Picks : '.$stock->getQty();
			$page->drawText($warning, 60, $pdf->y , 'UTF-8');
			$pdf->y-= 4;
		}
		
		if(false)
		{
			//$this->_setFont($page, $font_style_body, ($font_size_body-2), $font_family_body, $non_standard_characters, $font_color_body);

			$pdf->y -= 0;
			// BARCODE
			$generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
			$barcodeimage = new \Zend_Pdf_Resource_Image_Png('data:image/png;base64,'.base64_encode($generator->getBarcode(''.$product->getSku(), $generator::TYPE_CODE_128_B, 2, 20)));
			$page->drawImage($barcodeimage, 30, ($pdf->y+10), 130, ($pdf->y) - 10);
			$pdf->y -= 20;
		}
		
			//$pdf->y -= 10;
			$page->setLineColor(new \Zend_Pdf_Color_GrayScale(0.5));
			$page->drawLine(25, $pdf->y, 570, $pdf->y -1);
			
			$pdf->y -= 20;
		
		
		
        $this->setPage($page);
    }
}
