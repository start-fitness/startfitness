<?php
namespace Raw\Generatenewcustomer\Observer\Controller;
//load models
use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class ActionPredispatch implements ObserverInterface
{
//Declare session variables
private $customerSession;
private $_checkoutSession;
public $offer;

  public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $helperData,
         CheckoutSession $checkoutSession

       )
         {
     $this->customerSession = $customerSession;
     $this->ruleFactory = $ruleFactory;
     $this->helperData = $helperData;
     $this->_checkoutSession = $checkoutSession;

     }
    public function execute(
    \Magento\Framework\Event\Observer $observer
    ) {
    // unset offer
    $this->customerSession->unsTickerTape();
    //Check if the customer is logged in
    // Get Active Offers
    $active_offer  =  $this->getEnabledOffers();

    /*$ip = $_SERVER['REMOTE_ADDR'];
    $allowed_ip = array('150.143.34.199');
    if(in_array($ip,$allowed_ip)) {
print_r($active_offer);
}*/

    $runcode = false;
    foreach($active_offer as $active){

      if($active == 1) {
        $runcode = true;
        break;
      }
      }

    if($runcode == true) {
    //Get the offer types and priority
    $offeractive =  $this->getOfferType($active_offer);
    //echo $offeractive;
    //sort the Array by priority
    $offer_sorted = $this->subval_sort($offeractive,'priority');
    //loop through the active offers and break when one is successfully applied
    foreach($offer_sorted as $offer) {
    //  print_r($offer);
    //$applicable_offers = $this->generateOffer($offer);
    $applicable_offers = $this->generateOffer($offer);
    if($applicable_offers == 'true') {
          break;
      }
    }

  }
}

// Function to sort array
public function subval_sort($a,$subkey) {
        foreach($a as $k=>$v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        arsort($b);
        foreach($b as $key=>$val) {
            $c[] = $a[$key];
        }
        return $c;
    }

//fuction to generate a random string for the new customer type
public function generateRandomString($length = 8) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

//Checking if each of the rules enabled
public function getEnabledOffers() {
$offer['ticker_one_enable'] =  $this->helperData->getValue('raw/ticker_tape_one/ticker_one_enable',"store");
$offer['ticker_two_enable'] =  $this->helperData->getValue('raw/ticker_tape_two/ticker_two_enable',"store");
$offer['ticker_three_enable'] =  $this->helperData->getValue('raw/ticker_tape_three/ticker_three_enable',"store");
return $offer;
}

//getting the offer types long with priority
public function getOfferType($active_offer) {
  $offer = '';
    if(isset($active_offer['ticker_one_enable']) && $active_offer['ticker_one_enable'] == 1 ) {
        $offer[1]['type'] =  $this->helperData->getValue('raw/ticker_tape_one/ticker_one_default_tab',"store");
        $offer[1]['priority'] =  $this->helperData->getValue('raw/ticker_tape_one/discount_one_priority',"store");
        $offer[1]['ID'] =  1;
     }
     if(isset($active_offer['ticker_two_enable']) && $active_offer['ticker_two_enable'] == 1 ) {
       $offer[2]['type'] =  $this->helperData->getValue('raw/ticker_tape_two/ticker_two_default_tab',"store");
       $offer[2]['priority'] =  $this->helperData->getValue('raw/ticker_tape_two/discount_two_priority',"store");
       $offer[2]['ID'] =  2;
     }
     if(isset($active_offer['ticker_three_enable']) && $active_offer['ticker_three_enable'] == 1 ) {
       $offer[3]['type'] =  $this->helperData->getValue('raw/ticker_tape_three/ticker_three_default_tab',"store");
       $offer[3]['priority'] =  $this->helperData->getValue('raw/ticker_tape_three/discount_three_priority',"store");
       $offer[3]['ID'] =  3;
     }
 return $offer;
}

//generating the offers, type passsed in
public function generateOffer($offer_gen) {
  $rule = '';
  $rule_number = $this->numToString($offer_gen['ID']);
    if($offer_gen['type'] == 1) {
      //type one is a new customer rules
       $result = $this->newCustRule($rule_number);
       if($result == 1) {
       $rule = 'true';
      }
      }
    if($offer_gen['type'] == 2) {
      // type two is a new prod rule
       $result = $this->newProdRule($rule_number);
       if($result == 1) {
       $rule = 'true';
      }
      }
    if($offer_gen['type'] == 3) {
      // type three is a new cart rule
       $result = $this->newCartRule($rule_number);
       if($result == 1) {
       $rule = 'true';
      }
      }
return $rule;
}

public function newCustRule($number) {
//generate new customer rule
//get values for rule
$offer['display_text'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'',"store");
$offer['discount_amount'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_amount',"store");
$offer['discount_min_spend'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_min_spend',"store");
$offer['discount_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority',"store");
// check if the url has the PPC param set and the customer isnt logged in
$loggedIn =  $this->customerSession->isLoggedIn();
$customerppc = $this->customerSession->getPPC();
if((isset($_GET['PPC']) || ($customerppc == 'TRUE')))  {
if($loggedIn == 1) {
$this->customerSession->unsPPCCode();
$this->customerSession->unsPPC();
}
else {
// Set the PPC session
if($customerppc == '') {
$couponcode = $this->newCustomerPPC($offer);
}
else {
$couponcode = $this->customerSession->getPPCCode();
}
//set the ticker tape
$this->customerSession->setTickerTape($offer['display_text'] .' '. $couponcode);
return true;
}
}
}


public function newProdRule($number) {
//get offer parameters
$offer['display_text'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'',"store");
$offer['discount_static_code'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_static_code',"store");
$offer['discount_product_skus'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_product_skus',"store");
$offer['discount_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority',"store");
$skus = $offer['discount_product_skus'];
//check if the sku have been passed
if(isset($skus)) {
   //check if the items are in the cart
      $cart_items = $this->isInCart($skus);
      if($cart_items  == 'true') {
      $this->customerSession->setTickerTape($offer['display_text']);
      return true;
      }
}
}

public function newCartRule($number) {
$offer['display_text'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'',"store");
$offer['discount_static_code'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_static_code',"store");
$offer['discount_cart_total'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_cart_total',"store");
$offer['discount_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority',"store");
$total = $offer['discount_cart_total'];
//check the total has been set
if(isset($total)) {
  //get the current cart total
      $cart_total = $this->cartTotal($total);
      if($cart_total  == 'true') {
      $this->customerSession->setTickerTape($offer['display_text'] . ' ' .  $offer['discount_static_code']);
      return true;
      }
    }

}
//fuction to check if a customer is logged in
public function loggedIn() {
$loggedIn =  $this->customerSession->isLoggedIn();
	 if($loggedIn == 1) {
        $this->customerSession->unsPPC();
        $this->customerSession->unsPPCCode();

 }
}

public function numToString($number) {
$numbermappings = array("zero","one", "two","three");
return  $numbermappings[$number];
}
//fuction to check id a specific item is in the cart
public function isInCart($product)
{
  //load cart items
	$items = $this->_checkoutSession->getQuote()->getAllVisibleItems();
  //loop through items
  if($items) {
    foreach($items as $item) {
      //get each items sku and load into array
    $skus[] = $item->getSku();
  }
  $products[] = $product;
  //check if an item is both in the cart and definded in the array of applied products
  $result = array_intersect($products, $skus);
  //if results arent empty return true
  if(!empty($result)) {
	return true;
}
}
}
//fuction to check cart total
public function cartTotal($rule_total)
{
  //load total from checkout session
  $total = $this->_checkoutSession->getQuote()->getGrandTotal();
  //check if the total is higher than the rule total
  if($total >= $rule_total || $rule_total == -1) {
  return true;
}
}
//generate new customer rule
public function newCustomerPPC($offer) {
//print_r($offer); exit;
//echo 'in the rule';
$this->customerSession->setPPC('TRUE');


$objectManager =  \Magento\Framework\App\ObjectManager::getInstance();
$storeManager = $objectManager->create("\Magento\Store\Model\StoreManagerInterface");
$websiteId = $storeManager->getStore()->getWebsiteId();


$con_format = '{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Combine",
"attribute":null,"operator":null,"value":"1","is_value_processed":null,"aggregator":"all",
"conditions":[{"type":"Magento\\\\SalesRule\\\\Model\\\\Rule\\\\Condition\\\\Address",
"attribute":"base_subtotal",
"operator":">",
"value":"'.$offer['discount_min_spend'].'",
"is_value_processed":false}]}';

$date_time = time();
$code = $this->generateRandomString();
    $ruleData = [
            "name" => "PPC rule Offer One ash",
            "description" => 'New Customer Offer',
            "from_date" => date('Y-m-d'),
            "to_date" => null,
            "uses_per_customer" => "1",
            "is_active" => "1",
            "stop_rules_processing" => "0",
            "is_advanced" => "1",
            "product_ids" => null,
            "sort_order" => "0",
            "simple_action" => "by_percent",
            "discount_amount" => $offer['discount_amount'],
            "discount_qty" => null,
            "discount_step" => "1",
            "apply_to_shipping" => "0",
            "times_used" => "0",
            "is_rss" => "2",
            "coupon_type" => "2",
            "use_auto_generation" => "0",
            "uses_per_coupon" => "1",
            "simple_free_shipping" => "0",
            "customer_group_ids" => [0],
            "website_ids" => [$websiteId],
            "coupon_code" => $code,
            "store_labels" => [],
            "conditions_serialized" => $con_format,
            "actions_serialized" => ''
  ];
        $ruleModel = $this->ruleFactory->create();
        $ruleModel->setData($ruleData);
        $ruleModel->save();
        $this->customerSession->setPPCCode($code);

return $code;
}
}
