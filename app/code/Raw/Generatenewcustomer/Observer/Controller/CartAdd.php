<?php
namespace Raw\Generatenewcustomer\Observer\Controller;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;


class CartAdd implements ObserverInterface
{


    private $_checkoutSession;
    public function __construct(
        CheckoutSession $checkoutSession
    )
    {
        $this->_checkoutSession = $checkoutSession;
    }

    public function execute(EventObserver $observer)
    {
    $skus[] = '';

   $items = $this->_checkoutSession->getQuote()->getAllVisibleItems();   
    foreach($items as $item) {
        
       $skus[]  = $item->getSku();
           
  }     
  return $skus;
    }   
}
?>