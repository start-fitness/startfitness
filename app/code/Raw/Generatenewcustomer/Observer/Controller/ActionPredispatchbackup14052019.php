<?php
namespace Raw\Generatenewcustomer\Observer\Controller;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Checkout\Model\Session as CheckoutSession;

class ActionPredispatch implements ObserverInterface
{
private $customerSession;
private $_checkoutSession;

  public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $helperData,
         CheckoutSession $checkoutSession

       )
         {
     $this->customerSession = $customerSession;
     $this->ruleFactory = $ruleFactory;
     $this->helperData = $helperData;
     $this->_checkoutSession = $checkoutSession;

     }

    public function execute(
    \Magento\Framework\Event\Observer $observer
    ) {
    if($_SERVER['REMOTE_ADDR']=="77.98.191.91") {
    	$active_offer  =  $this->getEnabledOffers();
        if(isset($active_offer)) {
        print_r($active_offer);
        $offeractive =  $this->getOfferType($active_offer);
       $ticker = $this->generateOffer($offeractive);
       $loggedIn =  $this->customerSession->isLoggedIn();

       foreach($ticker as $offer) {
				if(isset($offer['one'])) {
				$number = 'One';
				$type = $offer['one']['type'];
        switch ($type) {
    		case "Product":
    		$skus = $offer['one']['product_skus'];
    		if(isset($skus)) {
                $cart_items = $this->isInCart($skus);
                if($cart_items  == 'true') {
                $this->customerSession->setTickerTape($offer['one']['text']);
                }
                else {
                $this->customerSession->unsTickerTape();
                }
    			}
        	break;
    			case "Cart":
          $total = $offer['one']['cart_total'];
          if(isset($total)) {
                $cart_total = $this->cartTotal($total);
                if($cart_total  == 'true') {
                $this->customerSession->setTickerTape($offer['one']['text']);
                }
                else {
                $this->customerSession->unsTickerTape();
                }
              }
            break;
        	case "Customer":
            if(isset($_GET['PPC']) && ($loggedIn !== 1) && ($this->customerSession->getPPCCode() == ''))  {
            $couponcode = $this->newCustomerPPC();
            $this->customerSession->setTickerTape($offer['one']['text']);
            }
            break;
}
				}
				if(isset($offer['two'])) {
				$number = 'Two';
				$type = $offer['two']['type'];
                switch ($type) {
    			case "Product":
    			$skus = $offer['two']['product_skus'];
    			if(isset($skus)) {
                $cart_items = $this->isInCart($skus);
                  if($cart_items  == 'true') {
                $this->customerSession->setTickerTape($offer['two']['text']);
                }
                else {
                $this->customerSession->unsTickerTape();
                }
    			}
        		break;
    			case "Cart":
          $total = $offer['two']['cart_total'];
          if(isset($total)) {
                $cart_total = $this->cartTotal($total);
                if($cart_total  == 'true') {
                $this->customerSession->setTickerTape($offer['two']['text']);
                }
                else {
                $this->customerSession->unsTickerTape();
                }
              }
            break;
        		case "Customer":
            if(isset($_GET['PPC']) && ($loggedIn !== 1) && ($this->customerSession->getPPCCode() == ''))  {
            $couponcode = $this->newCustomerPPC();
            $this->customerSession->setTickerTape($offer['two']['text']);
            }
            break;
				}
				}
				if(isset($offer['three'])) {
				$type = $offer['three']['type'];
                switch ($type) {
    			case "Product":
    			$skus = $offer['three']['product_skus'];
    			if(isset($skus)) {
                $cart_items = $this->isInCart($skus);
                  if($cart_items  == 'true') {
                $this->customerSession->setTickerTape($offer['three']['text']);
                }
                else {

                $this->customerSession->unsTickerTape();
                }
    			}
    			break;
    		  case "Cart":
          $total = $offer['three']['cart_total'];
          if(isset($total)) {
                $cart_total = $this->cartTotal($total);
                if($cart_total  == 'true') {
                $this->customerSession->setTickerTape($offer['three']['text']);
                }
                else {
                $this->customerSession->unsTickerTape();
                }
              }
            break;
        		case "Customer":
            if(isset($_GET['PPC']) && ($loggedIn !== 1) && ($this->customerSession->getPPCCode() == ''))  {
            $couponcode = $this->newCustomerPPC();
            $this->customerSession->setTickerTape($offer['three']['text']);
            }
         		break;
				}
			}

}
      }

	}
}


public function subval_sort($a,$subkey) {
        foreach($a as $k=>$v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach($b as $key=>$val) {
            $c[] = $a[$key];
        }
        return $c;
    }

public function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

public function getEnabledOffers() {
$offer['ticker_one_enable'] =  $this->helperData->getValue('raw/ticker_tape_one/ticker_one_enable');
$offer['ticker_two_enable'] =  $this->helperData->getValue('raw/ticker_tape_two/ticker_two_enable');
$offer['ticker_three_enable'] =  $this->helperData->getValue('raw/ticker_tape_three/ticker_three_enable');
return $offer;
}

public function getOfferType($active_offer) {
    //print_r($active_offer);
    if(isset($active_offer['ticker_one_enable']) && $active_offer['ticker_one_enable'] == 1 ) {
        $active_offer['ticker_one'] =  $this->helperData->getValue('raw/ticker_tape_one/ticker_one_default_tab');

     }
     if(isset($active_offer['ticker_two_enable']) && $active_offer['ticker_two_enable'] == 1 ) {
        $active_offer['ticker_two_type'] =  $this->helperData->getValue('raw/ticker_tape_two/ticker_two_default_tab');

     }
     if(isset($active_offer['ticker_three_enable']) && $active_offer['ticker_three_enable'] == 1 ) {
        $active_offer['ticker_three_type'] =  $this->helperData->getValue('raw/ticker_tape_three/ticker_three_default_tab');

     }
 return $active_offer;
}



public function generateOffer($offeractive) {
    if(isset($offeractive['ticker_one_type']) && $offeractive['ticker_one_type'] == 1) {
       $ticker[] = $this->newCustRule('one');
        }
      if(isset($offeractive['ticker_one_type']) && $offeractive['ticker_one_type'] == 2) {
       $ticker[] = $this->newProdRule('one');
       }
      if(isset($offeractive['ticker_one_type']) &&  $offeractive['ticker_one_type'] == 3) {
       $ticker[] = $this->newCartRule('one');
       }
        if(isset($offeractive['ticker_two_type']) && $offeractive['ticker_two_type'] == 1) {
       $ticker[] = $this->newCustRule('two');
        }
      if(isset($offeractive['ticker_two_type']) && $offeractive['ticker_two_type'] == 2) {
      $ticker[] =  $this->newProdRule('two');
       }
      if(isset($offeractive['ticker_two_type']) &&  $offeractive['ticker_two_type'] == 3) {
      $ticker[] =  $this->newCartRule('two');
       }
        if(isset($offeractive['ticker_three_type']) && $offeractive['ticker_three_type'] == 1) {
      $ticker[] =  $this->newCustRule('three');
        }
      if(isset($offeractive['ticker_three_type']) && $offeractive['ticker_three_type'] == 2) {
      $ticker[] =  $this->newProdRule('three');
       }
      if(isset($offeractive['ticker_three_type']) &&  $offeractive['ticker_three_type'] == 3) {
       $ticker[] = $this->newCartRule('three');
       }
  return $ticker;
}

public function newCustRule($number) {
$offer['display_text_'.$number.''] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'');
$offer['discount_'.$number.'_amount'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_amount');
$offer['discount_'.$number.'_min_spend'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_min_spend');
$offer['discount_'.$number.'_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority');

$ticker[$number]['text'] = $offer['display_text_'.$number.''];
$ticker[$number]['priority'] = $offer['discount_'.$number.'_priority'];
$ticker[$number]['type'] = 'Customer';
return $ticker;



}


public function newProdRule($number) {
$offer['display_text_'.$number.''] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'');
$offer['discount_'.$number.'_static_code'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_static_code');
$offer['discount_'.$number.'_product_skus'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_product_skus');
$offer['discount_'.$number.'_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority');

$ticker[$number]['text'] = $offer['display_text_'.$number.''];
$ticker[$number]['coupon_code'] = $offer['discount_'.$number.'_static_code'];
$ticker[$number]['product_skus'] = $offer['discount_'.$number.'_product_skus'];
$ticker[$number]['priority'] = $offer['discount_'.$number.'_priority'];
$ticker[$number]['type'] = 'Product';
return $ticker;

}

public function newCartRule($number) {
$offer['display_text_'.$number.''] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/display_text_'.$number.'');
$offer['discount_'.$number.'_static_code'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_static_code');
$offer['discount_'.$number.'_cart_total'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_cart_total');
$offer['discount_'.$number.'_priority'] =  $this->helperData->getValue('raw/ticker_tape_'.$number.'/discount_'.$number.'_priority');
$ticker[$number]['text'] = $offer['display_text_'.$number.''];
$ticker[$number]['coupon_code'] = $offer['discount_'.$number.'_static_code'];
$ticker[$number]['cart_total'] = $offer['discount_'.$number.'_cart_total'];
$ticker[$number]['priority'] = $offer['discount_'.$number.'_priority'];
$ticker[$number]['type'] = 'Cart';
return $ticker;
}

public function loggedIn() {
$loggedIn =  $this->customerSession->isLoggedIn();
	 if($loggedIn == 1) {
        $this->customerSession->unsPPC();
        $this->customerSession->unsPPCCode();

 }
}

public function isInCart($product)
{
	$items = $this->_checkoutSession->getQuote()->getAllVisibleItems();
	if($items) {
    foreach($items as $item) {
    $skus[] = $item->getSku();
  }
  $products[] = $product;
  $result = array_intersect($products, $skus);

  if(!empty($result)) {
	return true;
}
}
}

public function cartTotal($rule_total)
{
  $total = $this->_checkoutSession->getQuote()->getGrandTotal();
  if($total >= $rule_total) {
  return true;
}
}

public function newCustomerPPC() {
//echo 'in the rule';
$this->customerSession->setPPC('TRUE');
$date_time = time();
$code = 'FIRST' . $this->generateRandomString();
    $ruleData = [
            "name" => "PPC rule Offer One",
            "description" => 'New Customer Offer',
            "from_date" => date('Y-m-d'),
            "to_date" => null,
            "uses_per_customer" => "1",
            "is_active" => "1",
            "stop_rules_processing" => "0",
            "is_advanced" => "1",
            "product_ids" => null,
            "sort_order" => "0",
            "simple_action" => "by_percent",
            "discount_amount" => '10',
            "discount_qty" => null,
            "discount_step" => "1",
            "apply_to_shipping" => "0",
            "times_used" => "0",
            "is_rss" => "2",
            "coupon_type" => "2",
            "use_auto_generation" => "0",
            "uses_per_coupon" => "1",
            "simple_free_shipping" => "0",
            "customer_group_ids" => [0],
            "website_ids" => [1],
            "coupon_code" => $code,
            "store_labels" => [],
            "conditions_serialized" => '',
            "actions_serialized" => ''
        ];
return $code;
}
}
