<?php


namespace Raw\Generatenewcustomer\Observer;


use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class Couponcode implements ObserverInterface
{

	private $customerSession;
    private $ruleFactory;

    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        \Magento\SalesRule\Model\RuleFactory $ruleFactory
    ) {
        $this->customerSession = $customerSession;
        $this->ruleFactory = $ruleFactory; 
    }

    public function execute(EventObserver $observer)
    {
     
       $PPC = $this->customerSession->getPPC();
       $PPCCode = $this->customerSession->getPPCCode();
       if(isset($_GET['PPC'])) {
        //echo 'here';
        $this->Generatecouponcode();
       }
      
    }

    public function Generatecouponcode() {
    $date_time = time();
    $coupon['code'] = $date_time;
       // echo 'lets create a discount code';
    $ruleData = [
            "name" => "PPC rule",
            "description" => "PPC rule",
            "from_date" => null,
            "to_date" => null,
            "uses_per_customer" => "1",
            "is_active" => "1",
            "stop_rules_processing" => "0",
            "is_advanced" => "1",
            "product_ids" => null,
            "sort_order" => "0",
            "simple_action" => "by_percent",
            "discount_amount" => "10",
            "discount_qty" => null,
            "discount_step" => "3",
            "apply_to_shipping" => "0",
            "times_used" => "0",
            "is_rss" => "2",
            "coupon_type" => "1",
            "use_auto_generation" => "0",
            "uses_per_coupon" => "0",
            "simple_free_shipping" => "0",
            "customer_group_ids" => [0],
            "website_ids" => [1],
            "coupon_code" => $coupon['code'],
            "store_labels" => [],
            "conditions_serialized" => '',
            "actions_serialized" => ''
        ];

$ruleModel = $this->ruleFactory->create();
$ruleModel->setData($ruleData);
$ruleModel->save();
 $this->customerSession->setPPCCode($coupon['code']);
}


} ?>