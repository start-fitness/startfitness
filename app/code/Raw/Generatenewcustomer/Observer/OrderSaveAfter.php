<?php


namespace Raw\Generatenewcustomer\Observer;


use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class OrderSaveAfter implements ObserverInterface
{
	protected $_cookieManager;
	private $cookieMetadataFactory;


    public function __construct(
		\Magento\Framework\Stdlib\CookieManagerInterface $_cookieManager,
		\Magento\Framework\Stdlib\Cookie\CookieMetadataFactory $cookieMetadataFactory
    ) {
				$this->_cookieManager = $_cookieManager;
				$this->cookieMetadataFactory = $cookieMetadataFactory;

    }
    public function execute(EventObserver $observer)
    {
			if ($this->_cookieManager->getCookie('ticker_tape')) {
			$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
			$metadata->setPath('/');
			 $this->_cookieManager->deleteCookie('ticker_tape',$metadata);
			}
			if ($this->_cookieManager->getCookie('ticker_tape_type')) {
			$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
			$metadata->setPath('/');
			 $this->_cookieManager->deleteCookie('ticker_tape_type',$metadata);
			}
			if ($this->_cookieManager->getCookie('ppc_code')) {
			$metadata = $this->cookieMetadataFactory->createPublicCookieMetadata();
			$metadata->setPath('/');
			 $this->_cookieManager->deleteCookie('ppc_code',$metadata);
			}


       /* $order    = $observer->getEvent()->getOrder();
        $quoteId  = $order->getQuoteId();
        $quoteObj = $this->_quoteloader->create()->load($quoteId);

         echo 'here' . $order->getId();
         exit; */

    }


} ?>
