<?php

namespace Raw\Generatenewcustomer\Model\Config\Source;

class Ruletype extends \Magento\Eav\Model\Entity\Attribute\Source\AbstractSource implements \Magento\Framework\Option\ArrayInterface
{

    /**
     * Retrieve all options array
     * @return array
     */
    public function getAllOptions()
    {

        if ($this->_options === null) {
            $this->_options = [
                [
                    'label' => __('New Customer'),
                    'value' => 1
                ],
                [
                    'label' => __('Product'),
                    'value' => 2
                ],
                [
                    'label' => __('Cart'),
                    'value' => 3
                ]
            ];
        }

        return $this->_options;
    }

    /**
     * Options getter
     * @return array
     */
    public function toOptionArray()
    {

        return [
            [
                'value' => 1,
                'label' => __('New Customer')
            ],
            [
                'value' => 2,
                'label' => __('Product')
            ],
            [
                'value' => 3,
                'label' => __('Cart')
            ]
        ];
    }

    /**
     * Get options in 'key=>value' format
     * @return array
     */
    public function toArray()
    {

        return [
            1 => __('New Customer'),
            2 => __('Product'),
            3 => __('Cart')
        ];
    }
}
