<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Deepblue\Salesreports\Model\ResourceModel\Report\Order;

/**
 * Report order collection
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class CollectionTrade extends \Deepblue\Salesreports\Model\ResourceModel\Report\Order\Collection
{

    protected $_tradeonly = true;
	

    
}
