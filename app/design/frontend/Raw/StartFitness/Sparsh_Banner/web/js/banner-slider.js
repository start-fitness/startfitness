define([
    'jquery',
    'slick'
], function ($) {
    'use strict';
    return function (config) {
        $('.sparsh_banner_slider').slick(
            {
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: false,
                autoplay: parseInt(config.autoplay),
                autoplaySpeed: parseInt(config.autoplay_speed),
                pauseOnHover:false,
                arrows: false
            }
        );
        $('.sparsh_banner_slider').on('init', function(event, slick){
alert('heree');
});
    }
});
