var config = {

  // When load 'requirejs' always load the following files also

  paths: {
      slick:        'js/slick'

  },
  shim: {
      slick: {
          deps: ['jquery']
      }
  },
  deps: [
    "js/simplebar.min",
	"js/custommenu",
  "js/modeal"

  ]

};
