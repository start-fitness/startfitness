<?php 
require dirname(__FILE__)."/vendor/autoload.php";

$mappingcourier = [
152 => '42', // DHL 2 DAYS
159 => '40', // DPD INTL PARCEL
170 => '44', // DPD INTL EXPRESSPACK
629 => '35', // DHL RW EXPRESS
630 => '35', // DHL RW REMOTE (NO EQUIVALENT?)
633 => '34', // DHL EU EXPRESS
634 => '34', // DHL EU REMOTE (NO EQUIVALENT?)
22008 => '15' // RM INT TRACKED SIGNED
];

$dir = new DirectoryIterator(dirname(__FILE__)."/rawfiles");
$courierids = [];
$countriesmap = [];
$lines = [];
$fc = fopen(dirname(__FILE__)."/countries.csv", "r");
while($country = fgetcsv($fc)){
	$countriesmap[$country[1]] = $country[0];
}
fclose($fc);

foreach ($dir as $fileinfo) {
    if (!$fileinfo->isDot()) {
        $f = fopen(($fileinfo->getPathName()), "r");
		$header = [
		fgetcsv($f),
		fgetcsv($f)
		];
		while($line = fgetcsv($f)){
			if(!in_array($line[0],$courierids)) $courierids[] = $line[0];
			$courier = $mappingcourier[$line[0]];
			if(!isset($countriesmap[$line[1]])){
				echo $line[1] ." NOT MAPPED COUNTRY {$fileinfo->getFileName()}\n";continue;
			}
			$country = $countriesmap[$line[1]];
			$data = (new League\ISO3166\ISO3166)->name($country);
			$type = ($line[3] == "2")? "weight" : "parcel";
			$from = $line[4];
			$to = $line[5];
			$cost = $line[7];
			
			$linetmp = [NULL, $courier, $type, $from, $to, $cost,1,$data['alpha2']];
			$lines[] = $linetmp;
		}
		fclose($f);
    }
}
$csv = fopen(dirname(__FILE__)."/shipping_formatted.csv","w+");
foreach($lines as $line){
	
	fputcsv($csv, $line);
	
}