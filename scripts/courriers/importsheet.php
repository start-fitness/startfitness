<?php 
require dirname(__FILE__)."/vendor/autoload.php";


$csv = fopen(dirname(__FILE__)."/shipping.csv","r");
$count = 0;
$headers = [];
$lines = [];
$startheader = 6;
$methodcount = 20;
$methodsmap = [
'Royal Mail Airmail'=>'19',
'Royal Mail Signed For'=>'15',
'DPD Expresspak'=>'44',
'DPD Classic'=>'39',
'DHL Air Express'=>'34',
'DHL Road' => '37'
];

while($line = fgetcsv($csv)){
	if($count < 2){
		$header[] = $line;
		$count++;
		continue;
	}
	
	if($line[0] == "") {$count ++;continue;}
	$currheader = 6;
	$index = 0;
	do{
		
		$currheader += ($index == 0)? 0 : 1;
		
		$previous = ($index == 0)? "0" : $header[1][$currheader-1];
		$previouscomp = ($index == 0)? $header[0][$currheader] : $header[0][$currheader-1];
		$price = str_replace("£","",$line[$currheader]);
		$country = $line[0];
		
		$data = (new League\ISO3166\ISO3166)->name($country);
		$method =$header[0][$currheader];
		$weight =$header[1][$currheader];
		
		$from = str_replace("kg", "", str_replace("Upto ", "", $previous));
		$to = str_replace("kg", "", str_replace("Upto ", "", $weight));
		if(substr($previouscomp, 0, 3) != substr($method, 0, 3) ) $from = 0;
		$linetmp = [NULL, $methodsmap[$method], "weight", $from, $to, $price,1,$data['alpha2']];
		$lines[] = $linetmp;
		$index++;
	}
	while($currheader <= $startheader + $methodcount );
	
	$count++;
	
}

$csv = fopen(dirname(__FILE__)."/shipping_formatted.csv","w+");
foreach($lines as $line){
	
	fputcsv($csv, $line);
	
}