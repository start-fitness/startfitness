<?php
use Magento\Framework\App\Bootstrap;


require dirname(__FILE__). '/../../app/bootstrap.php';

$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
$objectManager = $bootstrap->getObjectManager();
$dispatcherObject = $objectManager->get('Ess\M2ePro\Model\M2ePro\Connector\Dispatcher');
$connectorObj = $dispatcherObject->getConnector('tables', 'get', 'diff'); 

$dispatcherObject->process($connectorObj);
$responseData = $connectorObj->getResponseData();

var_dump($responseData);
