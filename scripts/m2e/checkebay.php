<?php 
use Magento\Framework\App\Bootstrap;
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require dirname(__FILE__).'/ebay-sdk-php.phar';
use \DTS\eBaySDK\OAuth\Services;
use \DTS\eBaySDK\OAuth\Types;
//use \DTS\eBaySDK\Fulfillment\Types;

ini_set('display_startup_errors',1);
ini_set('display_errors',1);
error_reporting(-1);
ini_set('memory_limit', '10048M');


ini_set('memory_limit', '16048M');
require __DIR__ . '/../../app/bootstrap.php';
stream_wrapper_restore('phar');
require dirname(__FILE__).'/../../m1scripts/admin_script/phpmailer/class.phpmailer.php';
umask(0);
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();
// Services will use the 903 version of the api and UK site (Site ID 3).
$credentials = [
        'appId'  => 'WMWholes-WampMMag-PRD-f8bba84e8-c4891c86',
        'certId' => 'PRD-8bba84e84f8e-7b05-4693-97f9-9680',
        'devId'  => '05e6313c-a67f-406e-8eaa-7b376e7f5443'
    ];
$config = [
    'apiVersion' => '903',
    'siteId'     => '3',
    'credentials' => $credentials,
    "OAuth" => array(
		"ruName" => "W_M_Wholesale-WMWholes-WampMM-fezgllfzh"
	)
];

$scopes = [
		        'https://api.ebay.com/oauth/api_scope/sell.account',
		        'https://api.ebay.com/oauth/api_scope/sell.inventory',
		        'https://api.ebay.com/oauth/api_scope/sell.fulfillment'
		    ];

if(isset($_GET['token'])&&$_GET['token']=="request"){
	// Create an SDK class used to share configuration across services.
	$sdk = new DTS\eBaySDK\Sdk($config);
	
	// Create two services that share the same configuration.
	$service = new Services\OAuthService([
	    'credentials' => $credentials,
	    'ruName'      => 'W_M_Wholesale-WMWholes-WampMM-fezgllfzh'
	]);
	$apptoken = null;
	try{
		$response = $service->getAppToken();
		
		if ($response->getStatusCode() !== 200) {
		    printf(
		        "%s: %s\n\n",
		        $response->error,
		        $response->error_description
		    );
			die();
		} else {
		    /*printf(
		        "%s\n%s\n%s\n\n",
		        $response->access_token,
		        $response->token_type,
		        $response->expires_in
		    );*/
			$apptoken = [
				$response->access_token,
			    $response->token_type,
			    $response->expires_in
			];
		}
		
		$url =  $service->redirectUrlForUser([
		    'state' => 'bar',
		    'scope' => $scopes
		]);
		
		//var_dump($url);
		header('Location: '.$url);
	}
	catch(\Exception $e){
		var_dump($e->getMessage(), $e->getTrace());
	}
}
elseif(isset($_GET['return'])&&$_GET['return']=="success"){
		
		
	$sdk = new DTS\eBaySDK\Sdk($config);
	
	// Create two services that share the same configuration.
	$service = new Services\OAuthService([
	    'credentials' => $credentials,
	    'ruName'      => 'W_M_Wholesale-WMWholes-WampMM-fezgllfzh'
	]);
	$apptoken = null;
	try{
		$response = $service->getAppToken();
		
		if ($response->getStatusCode() !== 200) {
		    printf(
		        "%s: %s\n\n",
		        $response->error,
		        $response->error_description
		    );
			die();
		} else {
		    /*printf(
		        "%s\n%s\n%s\n\n",
		        $response->access_token,
		        $response->token_type,
		        $response->expires_in
		    );*/
			$apptoken = [
				$response->access_token,
			    $response->token_type,
			    $response->expires_in
			];
		}
		
		$response = $service->getUserToken(new Types\GetUserTokenRestRequest([
		    'code' => $_GET['code']
		]));
		if ($response->getStatusCode() !== 200) {
		    printf(
		        "%s: %s\n\n",
		        $response->error,
		        $response->error_description
		    );
			die();
		} else {
		    
			$tokendata = [
			"access_token"=>	$response->access_token,
		    "token_type"=>    $response->token_type,
		    "expires_in"=>    $response->expires_in,
		    "refresh_token"=>    $response->refresh_token
			];
			
			file_put_contents(dirname(__FILE__)."/ebay-token.json", json_encode($tokendata));
		}
		
	}
	catch(\Exception $e){
		var_dump($e->getMessage(), $e->getTrace());
	}
	
}
else{
	
	$tokens = json_decode(file_get_contents(dirname(__FILE__)."/ebay-token.json"));
	
	
	$sdk = new DTS\eBaySDK\Sdk($config);
	
	// Create two services that share the same configuration.
	$service = new Services\OAuthService([
	    'credentials' => $credentials,
	    'ruName'      => 'W_M_Wholesale-WMWholes-WampMM-fezgllfzh'
	]);
	try{
		
		
		
		if(false){
			$response = $service->refreshUserToken(new Types\RefreshUserTokenRestRequest([
			    'refresh_token' => $tokens->refresh_token,
			    'scope' => $scopes
			]));
			
			
			if ($response->getStatusCode() !== 200) {
			    printf(
			        "%s: %s\n\n",
			        $response->error,
			        $response->error_description
			    );
			} else {
			    $tokendata = [
				"access_token"=>	$response->access_token,
			    "token_type"=>    $response->token_type,
			    "expires_in"=>    $response->expires_in,
			    "refresh_token"=>    $tokens->refresh_token
				];
				
				file_put_contents(dirname(__FILE__)."/ebay-token.json", json_encode($tokendata));
				$tokens = json_decode(file_get_contents(dirname(__FILE__)."/ebay-token.json"));
				var_dump($tokens);
			}
			$options = [
			    'credentials' => $credentials,
			    'apiVersion' => 'v1',
	    		'siteId'     => '3',
	    		'authorization' => $tokens->access_token,
			];
			$fservice = new \DTS\eBaySDK\Fulfillment\Services\FulfillmentService($options);
			
			$request = new \DTS\eBaySDK\Fulfillment\Types\GetOrdersRestRequest();
			$request->filter = "lastmodifieddate:[2020-04-01T08:25:43.511Z..2020-04-23T08:25:43.511Z]";
			$request->offset = "0";
			$request->limit = "10000";
			$response = $fservice->GetOrders($request);
			
			var_dump($response->orders);
		}
		else{
			/*$options = [
			    'credentials' => $credentials,
			    'apiVersion' => '1067',
	    		'siteId'     => '3',
	    		'authToken' => "AgAAAA**AQAAAA**aAAAAA**Z81VWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wGkoOgC5SLogmdj6x9nY+seQ**M3EEAA**AAMAAA**zabSnOr8z9093k+UQxTyeBCHbw3uypaFJfHSCcEwTIHTwpxPHLWW/ZVKQUuQUkmjzeed42kV/uhOduqfpzMod7ymYlNTdb3YmLorjt54x6fyzeR8jNAEnY7LvouCYCXPhVk2jfNluzTOkF7nyBD3ebbcYp53Ojmk6EoNh+F1SzVRLahwWesdLecCxn8klk7As5iWluDbCqQuaiSs1I+ars5SKctI7cgBkMkNPO2WubAkM3WMFwm+llod3l7lZR+IeGc6kWe+CCMq+RhEtwfNiT5q9KxMzI59nJc2BBtJzfdaD1/t0oUlB4aXwNpd0UQdUFoWUbePMy6PBS5oZM39686EzChgRpIaUeFuhzKB8d7/yD8kIjwZPFXcT4POm78Lw3jH9aW54zqHjdBPla86SYAlkNjdiRibtuRzJSdMItSlcR/3Vrp9LCbcHxmjST6Cv4tMI1aE2wuDFMGo2/XNh7DRBReam/LROJJAvXg5vhvZJOWUtl5+g0ZhIWM6OeR1PCB3iDp6VXrPtHlEnNgufmhAbf043yUcEhVqIEQ9AHE246ftGj+I0QWAWGnoQO7Pu3cZEgn6swQ7qqIJyw5ibzcDqI3NnQV0TTicPHuC4xKpmC/MVxrenlYoetlynwP6txdDAeY7lAWzBLbGSe5tjnbhM0dD9KiNHvWUooDZvT4/wz9KDuGSqLZh6ophdeSzmQiAbM43qitg7/6yb9fd8K3nHnB6ttVboiA65A+DYqpU58gg4Clmv3ENisHmybrg",
			];*/
			
			$response = $service->refreshUserToken(new Types\RefreshUserTokenRestRequest([
			    'refresh_token' => $tokens->refresh_token,
			    'scope' => $scopes
			]));
			
			
			if ($response->getStatusCode() !== 200) {
			    printf(
			        "%s: %s\n\n",
			        $response->error,
			        $response->error_description
			    );
			} else {
			    $tokendata = [
				"access_token"=>	$response->access_token,
			    "token_type"=>    $response->token_type,
			    "expires_in"=>    $response->expires_in,
			    "refresh_token"=>    $tokens->refresh_token
				];
				
				file_put_contents(dirname(__FILE__)."/ebay-token.json", json_encode($tokendata));
				$tokens = json_decode(file_get_contents(dirname(__FILE__)."/ebay-token.json"));
				var_dump($tokens);
			}
			$options = [
			    'credentials' => $credentials,
			    'apiVersion' => '1067',
	    		'siteId'     => '3',
	    		'authorization' => $tokens->access_token,
			];
			
			$fservice = new \DTS\eBaySDK\Trading\Services\TradingService($options);
			$hasnextpage = false;
			$count = 0;
			$missingorders = array();
			$missingorderstxt = array();
			$offset = 1;
			$perpage = 100;
			do{
				if($offset > 10000) exit;
				$request = new \DTS\eBaySDK\Trading\Types\GetOrdersRequestType();
				$request->NumberOfDays = 7;
				$request->Pagination = New DTS\eBaySDK\Trading\Types\PaginationType(array("EntriesPerPage" => $perpage, "PageNumber"=> $offset));
				//$request->limit = "1000";
				$response = $fservice->GetOrders($request);
				var_dump($response);
				$hasnextpage = false;
				if($response->HasMoreOrders){
					$hasnextpage = true;
				}
				$offset += 1;
				//var_dump($offset, $response->PaginationResult);
				foreach($response->OrderArray->Order as $index => $order){
					var_dump($order->CreatedTime);
					$nowminus = new \DateTime();
					$nowminus->modify("- 2 hours");
					if((($order->CreatedTime))>= $nowminus) continue;

					$checkdb = checkInMagento($read, $order->ShippingDetails->SellingManagerSalesRecordNumber);
				 	if(!$checkdb){
				 		//echo "ORDER {$order->AmazonOrderId} IS NOT IN MAGENTO!\n";
						$missingorderstxt [] = "[$index][".$order->OrderStatus."] ORDER {$order->ShippingDetails->SellingManagerSalesRecordNumber} IS NOT IN MAGENTO!\n";
						$missingorders[] = $order->ShippingDetails->SellingManagerSalesRecordNumber;
						//var_dump($order);
				 	}
					else{
						/*if($checkdb['magento_order_id'] == NULL && $checkdb['status'] == 0 && ($order->OrderStatus != "Pending" && $order->OrderStatus != "PendingAvailability") ){
							//echo "ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderStatus}\n";
							$missingorders[] = $order->OrderID;
							$missingorderstxt[] = "[$index][".$order->OrderStatus."] ORDER {$order->AmazonOrderId} IS IN MAGENTO BUT HAS NO MAGNETO ORDER ENTITY! {$order->OrderID}\n";
							
						}*/
					}
					$count++;
					//var_dump($checkdb, $missingorderstxt);
				//exit;
				}
			}
			while ($hasnextpage);
			
			
			if(count($missingorders) == 0){
				echo "all good, no missing ebay orders;\n";
				exit;
			}
				
			echo $missingorderstxt;
			$mail = new PHPMailer;
			$email ="richard@startfitness.co.uk";
			/*$mail->isSMTP();                                      // Set mailer to use SMTP
			$mail->Host = 'localhost';  // Specify main and backup server
			$mail->SMTPAuth = false;                               // Enable SMTP authentication
			$mail->Username = 'stockmanager@startcycles.co.uk';                            // SMTP username
			$mail->Password = 'QvEF4Clp';                           // SMTP password
			//$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted
			*/
			$mail->From = 'customerservices@startcycles.co.uk';
			$mail->FromName = 'Stock Manager';
			$mail->addAddress($email);  // Add a recipient
			//$mail->addAddress("gina@startfitness.co.uk");  // Add a recipient
			$mail->addAddress("geoffrey@startfitness.co.uk");  // Add a recipient
			$mail->addAddress("gary@startfitness.co.uk");  // Add a recipient
			
			//$mail->addBCC("speedking34@gmail.com");  // Add a recipient
			
			$mail->isHTML(true);                                  // Set email format to HTML
			
			$mail->Subject = 'Start Fitness - Missing Ebay Orders';
			$mail->Body    = 
			
			'
			<p>The following amazon orders are currently missing from Magento: (Script checked '.$count.' orders)</p>
			<p>'.implode("<br />", $missingorders).'<p>
			<p>'.implode("<br />", $missingorderstxt).'<p>
			';
			
			if(!$mail->send()) {
			   echo 'Message could not be sent.';
			   echo 'Mailer Error: ' . $mail->ErrorInfo;
			   exit;
			}
			
			echo 'Message has been sent';
			 var_dump("Number of orders found: ".$count);
			
			 
		}
		
	}
	catch(\Exception $e){
		var_dump($e->getMessage(), $e->getTraceAsString());
	}
}

function checkInMagento($read, $id){
 	
	
	$readConnection = $read;
	
	$sql = "SELECT m2epro_ebay_order.ebay_order_id, \"\" as status, m2epro_ebay_order.paid_amount, m2epro_order.magento_order_id FROM `m2epro_ebay_order` 
	JOIN `m2epro_order` ON m2epro_order.id = m2epro_ebay_order.order_id
	WHERE m2epro_ebay_order.selling_manager_id = '{$id}'";
	
	$results = $readConnection->fetchAll($sql);
	
	if(count($results)> 0)
		return $results[0];
	else {
		return false;
	}
 }
