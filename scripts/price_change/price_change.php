<?php
ini_set('memory_limit', '20000048M');
error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
try{
require __DIR__ . '/../../app/bootstrap.php';
$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);

ini_set('memory_limit', '20000048M');
ini_set('max_execution_time','-1');

$objectManager = $bootstrap->getObjectManager();
$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');
$row = 1;
$top_stock_cycle = array();
$top_price_cycle = array();
$top_stock_other = array();
$top_price_other = array();

$file2 = fopen('stock_and_prices.csv', 'r');
$file = fopen("stock_and_prices_report.csv","w");
$data['sku'] = 'SKU';
$data['previous_price'] = 'Previous Price';
$data['price'] = 'Price';
$data['price_diff'] = 'Price Difference';
$data['previous_stock'] = 'Previous Stock';
$data['stock'] = 'Stock';
$data['stock_diff'] = 'Stock Difference';
$data['last_ran_date'] = 'Last Ran Date';
$storeid = 1;
fputcsv($file,$data);
$attributeSet = $objectManager->create('Magento\Eav\Api\AttributeSetRepositoryInterface');
while (($result = fgetcsv($file2)) !== false)
{
  $sku = $result[0];
      $productObject = $objectManager->get('Magento\Catalog\Model\Product');
      $productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
      $product = $productRepository->get($sku);
      $attributeSetRepository = $attributeSet->get($product->getAttributeSetId());
		  $attributeSetName = $attributeSetRepository->getAttributeSetName();
      $data['sku'] = $result[0];
      $data['previous_price'] = $result[1];
      $data['price'] = $product->getPrice();
      $data['price_diff'] = abs((int)$result[1] - (int)$product->getPrice());
      $data['previous_stock'] = $result[2];
      $StockState = $objectManager->get('\Magento\CatalogInventory\Api\StockStateInterface');
      $stockQty = $StockState->getStockQty($product->getId());
      $data['stock'] = $stockQty;
      $data['stock_diff'] = abs((int)$result[2] - (int)$stockQty);
      $data['last_ran_date'] = $result[3];

      //get top changes
      if((strpos($attributeSetName, 'Cycle') !== false) || (strpos($attributeSetName, 'Bike') !== false)){
        if($data['stock_diff'] != 0) {
          $top_stock_cycle[$data['stock_diff']] = $result[0];
        }
        if($data['price_diff'] != 0) {
          $top_price_cycle[$data['price_diff']] = $result[0];
        }
      }
      else {
        if($data['stock_diff'] != 0) {
          $top_stock_all[$data['stock_diff']] = $result[0];
        }
        if($data['price_diff'] != 0) {
          $top_price_all[$data['price_diff']] = $result[0];
        }
      }
       fputcsv($file,$data);

  }

//close connections
fclose($file);
fclose($file2);

//send email
$params = $_SERVER;
$sendToEmailAddress = array('ashleigh@rawtechnologies.co.uk','gary@startfitness.co.uk');
//$sendToEmailAddress = array('carlsmith@startfitness.co.uk','gary@startfitness.co.uk','daniel@startfitness.co.uk');
$sendToName = 'Start Fitness';
$message = "Stock/Price Report";
$html = "Hi,<br><br> This is your latest price/stock conparison report.";
$html .= "<br><br>These are the top 10 stock changes across all other groups<br>";
$i = 0;
ksort($top_stock_other);
foreach ($top_stock_other as $key => $val) {
$i ++;
$html .=  "$val\n";
if ($i == 10) { break; }
}
$html .= "<br><br>These are the top 10 price changes across all product groups<br>";
$r = 0;
ksort($top_price_other);
foreach ($top_price_other as $key => $val) {
$r ++;
$html .=  "$val\n";
if ($r == 10) { break; }
}
$html .= "<br><br>These are the top 10 stock changes across cycles<br>";
$t = 0;
ksort($top_stock_cycle);
foreach ($top_stock_cycle as $key => $val) {
$t ++;
$html .=  "$val\n";
if ($t == 10) { break; }
}
$html .= "<br><br>These are the top 10 price changes across cycles<br>";
$x = 0;
ksort($top_price_cycle);
foreach ($top_price_cycle as $key => $val) {
$x ++;
$html .=  "$val\n";
if ($x == 10) { break; }
}




$mail = new \Zend_Mail();
	$mail->setBodyText($message);
	$mail->setBodyHtml($html);
	$mail->setFrom("info@startfitness.co.uk", 'Magento Auto Emailer');
	$mail->addTo($sendToEmailAddress, $sendToName);
	$mail->setSubject('Stock/Price Report '.date("D d-M-Y"));
	$dir =  __DIR__ . '/../../../';
	$path = dirname(__FILE__)."/stock_and_prices_report.csv";
	$file = $mail->createAttachment(file_get_contents($path));
	$file ->type        = 'text/csv';
	$file ->disposition = \Zend_Mime::DISPOSITION_INLINE;
	$file ->encoding    = \Zend_Mime::ENCODING_BASE64;
	$file ->filename    = 'stock_and_prices_report.csv';
		try {
    			$mail->send();
		}
		catch (Exception $e) {
		}
//re run refresh of prices after ran
include 'generate_inital_file.php';
}

catch (Exception $e) {
    echo $e->getMessage()."\n\n";
}



?>
