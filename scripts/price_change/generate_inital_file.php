<?php
ini_set('memory_limit', '20000048M');
ini_set('max_execution_time','-1');

error_reporting(E_ALL);
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;
try{
require __DIR__ . '/../../app/bootstrap.php';
$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
$objectManager = $bootstrap->getObjectManager();

ini_set('memory_limit', '20000048M');
ini_set('max_execution_time','-1');


$storeManager = $objectManager->get('\Magento\Store\Model\StoreManagerInterface');
$productRepository = $objectManager->get('\Magento\Catalog\Model\ProductRepository');
$appState = $objectManager->get('\Magento\Framework\App\State');
$appState->setAreaCode('frontend');
$row = 1;

$fh = fopen('stock_and_prices.csv', 'w');
fclose($fh);

$file = fopen("stock_and_prices.csv","w");
$storeid = 1;
$products = $objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')
->addFieldToFilter('type_id', array('eq' => 'simple'))->addAttributeToSelect('*')->setPageSize(1000);
$pages = $products->getLastPageNumber();
$count = 0;
$currentPage = 1;
do{
	echo "\n<br />current page: {$currentPage}<br />\n";
	$products->setCurPage($currentPage);
  $products->load();
  //echo 'here'; exit;
  foreach($products as $prod) {
//echo $prod->getSku(); exit;
    $product = $objectManager->create('\Magento\Catalog\Model\Product')->load($prod->getId());
    $data2['sku'] = $product->getSku();
    $data2['price'] = $product->getPrice();
    $data2['stock'] = $product['quantity_and_stock_status']['qty'];
    $data2['date'] = date("Y-m-d");
         if(!empty($data2)) {
         fputcsv($file,$data2);
  }
}
	$currentPage++;
    //make the collection unload the data in memory so it will pick up the next page when load() is called.
	$products->clear();
}while ( $currentPage <= $pages);

fclose($file);
}
catch (Exception $e) {
    echo $e->getMessage()."\n\n";
}



?>
