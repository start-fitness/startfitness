<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../app/bootstrap.php';
 
$params = $_SERVER;
$rootpath = "/microcloud/domains/fitnes/domains/startfitness.co.uk/http/pub/script/";
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$read = $resource->getConnection();
// Login as Admin user
//Mage::getSingleton('admin/session')->login("autoprint","F^r!34zy%@*sWn.e");

// Set Timeouts and reporting
//Mage::setIsDeveloperMode(true); // Turn This off for it to work
ini_set('memory_limit','768M');
ini_set('display_errors',1);
ini_set('max_execution_time', 0);
error_reporting(E_ALL);

// Function to send email upon error
function sendErrorEmail($message){

	/*list($toEmailAddresses,$toName,$ccEmailAddresses) = Mage::helper('dimasoftgen')->getCustomEmailAddressData('autoprint',true);

	$mail = Mage::getModel('core/email');
	$mail->setToName($toName);
	$mail->setSubject('Auto Print Error '.date("D d-M-Y"));
	$mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/email'));
	$mail->setFromName('Auto Print Error');
	$mail->setType('text');
	$mail->setBodyText($message);

	//Easiest method to send multiple emails, instead of setting up a transactional
	try {
		foreach($toEmailAddresses as $toEmail) {
			$mail->setToEmail($toEmail);
			$mail->send();
		}
	} catch (Exception $e) {
		Mage::log('Autoprint Script: Could not send error report email(s).');
	}*/
	// @TODO

}
// Need to check if we can print yet - here we use date and time
// We will only print between Monday-Sat 
$today = date('N'); // Number Format Days
$time = date('Gi'); // 24 Hour
//echo $time;exit;
// If its Mon-Saturday
if($today == 1 || $today == 2 || $today == 3 || $today == 4 || $today == 5 || $today == 6){
	// Remember the server time is a hour out
	if($time >= 0400 && $time <= 2345){
		$canprint = true;
	}else{
		$canprint = false; // change this to false
	}
}else{
			// Remember the server time is a hour out
			if(($time >= 0400 && $time <= 2300) || ($time >= 1900 && $time <= 2330)){
					$canprint = true;
			}else{
					$canprint = false; // change this to false
			}

}

//test override
//$canprint = true;
if($canprint == true){

	// Need to find all the orders this script has marked as printed
	// and make sure that Google actually has this printed
	// To Do here - limit the output from the sql to only say the last hours orders to check
	// better yet, what ever was printed in the last batch to check against - save time

	// First get the picknote directory
	/*$filename = "orders/";
	$sql_checkprinted = "SELECT distinct(sfo.`entity_id`),sfo.`increment_id` FROM `sales_flat_order` sfo
				RIGHT JOIN `sales_flat_order_status_history` sfosh ON sfo.`entity_id`=sfosh.`parent_id`
				WHERE sfosh.`comment` like '%autoprint'
				AND sfo.`status` = 'pick_note_printed'";
	$checkorders = $read->fetchAll($sql_checkprinted);
	foreach($checkorders as $checked){

		$filename = "PrintFolder/Express" . $checked['increment_id'].".pdf"; // this will be increment_id.pdf

		if (file_exists($filename)) {
				echo "The file $filename exists<br>"; // do nothing - success print
		} else {
			echo "The file $filename does not exist<br>"; // needs to be reprinted
		}
	}*/

	// First we priorities the Express shipments
	// These will always be printed out no matter what

	
	/*$sql_getexpress = "SELECT sfo.`entity_id`,LOWER(sfo.`shipping_description`)
				FROM `sales_order` sfo 
				JOIN raw_courriermanager_allocations mc on mc.order_id = sfo.entity_id
				LEFT JOIN sales_order_status_history hist on hist.parent_id = sfo.entity_id
                LEFT JOIN amasty_giftcard_order gco ON gco.order_id  = sfo.entity_id
				WHERE sfo.`status` = 'processing' 
				AND sfo.`customer_group_id` NOT IN ('6') 
				AND sfo.`customer_group_id` NOT IN ('6') AND
				(
                    	 sfo.`base_total_invoiced` > 0.00 OR sfo.amstorecredit_invoiced_amount > 0 OR gco.invoice_gift_amount > 0
                    ) 
					AND (
						(mc.number = 'ALLOCATED'
						
						AND (sfo.`shipping_description` REGEXP 'next|exp|special|second|dhl' OR sfo.`shipping_description` = 'SHIPPING PROMOTION - Free' OR sfo.`shipping_description` = 'ebay shipping - royal mail tracked 24'))
						OR 
						(( sfo.`shipping_description` REGEXP 'Is Prime' AND sfo.`store_id` IN ('24'))
						AND sfo.`created_at` < date_sub(UTC_TIMESTAMP(), interval 25 minute))
						OR 
						(( sfo.`shipping_description` REGEXP 'Is Prime' AND sfo.`store_id` IN ('24'))
						AND sfo.`created_at` < date_sub(UTC_TIMESTAMP(), interval 25 minute))
				) 
				AND sfo.`store_id` NOT IN ('23','24','25','26','35','28','33')
				AND sfo.entity_id > 2685273
				GROUP BY sfo.entity_id
				HAVING MAX(hist.created_at) < NOW() - INTERVAL 1 MINUTE
				LIMIT 40";

	// cycles exclude code - AND sfo.`store_id` NOT IN ('21','10','1')
	// limits set to 100 to protect against double printing from script overlap...  Please DO NOT CHANGE limits unless very sure of reasons.

	$expressorders = $read->fetchAll($sql_getexpress);

	foreach($expressorders as $express){
		$orderrep = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface');
		echo '<p>'.$express['entity_id'];
		//$loadorder = Mage::getModel('sales/order')->load(378506);
		$loadorder = $orderrep->get($express['entity_id']);
		// Try and get the invoice collection
		
		// Check if order has bike or bikeframe items
		$hasbike = false;
		$hasbike_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_order_item` sfoi
				LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
				WHERE sfoi.`order_id` = ".$loadorder->getID()." 
				AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` IN (9,34))
				GROUP BY cpe.`attribute_set_id`";
		$hasbikeres = $read->fetchCol($hasbike_getitems);
			if(count($hasbikeres) > 0) $hasbike = true;
		
		// Check if order has bike helmet or shoes items - UPDATE only items not in bins starting with "A"
		$hasbikeitems = false;
		$hasbikeitems_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_order_item` sfoi
				LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
				LEFT JOIN `catalog_product_entity_varchar` cpebin ON cpe.`entity_id`=cpebin.`entity_id` AND cpebin.attribute_id = 136 AND cpebin.store_id = 0
				WHERE sfoi.`order_id` = ".$loadorder->getID()." 
				AND sfoi.`parent_item_id` IS NULL AND cpebin.value LIKE 'T%'
				GROUP BY cpe.`attribute_set_id`";
		$hasbikeitemsres = $read->fetchCol($hasbikeitems_getitems);
		if(count($hasbikeitemsres) > 0) $hasbikeitems = true;
		
		$prefix = "";
		if ($loadorder->hasInvoices()) {
			$shipping_type = "Express";
			$product_type = "All";
			
			
			$orderedItems = $loadorder->getAllItems();
			$orderedProductIds = array();
			foreach ($orderedItems as $item) {
				if ($item->getProductType() == "simple")
				{
						$orderedProductIds[] = $item->getData('product_id');
						
				}
			}
			
			if(count($orderedProductIds) > 0){
				$shelves = array();
								
				
				foreach($orderedProductIds as $product){
					$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product);
					$bin = $product->getData('bin_location');
					
					
					if(!empty($bin))$shelves[] = $bin;
				}
				if(count($shelves) > 0){
					asort($shelves);
					//var_dump($shelves);
					$prefix1 = str_replace('-', '', $shelves[0]);
					$prefix = str_replace(' ', '', $prefix1);
					if(count($shelves) > 1)
					$prefix = "Z".$prefix;
				}
				
			}
			
			// A little bodge to get the invoices to print - as moogento expects more than one id in an array
			// We pass the single id in an array for it to print out without an error
			
			$read->query("UPDATE sales_order SET status = 'pick_note_printed' WHERE entity_id = ".$loadorder->getId());
			$read->query("UPDATE sales_order_grid SET status = 'pick_note_printed' WHERE entity_id = ".$loadorder->getId());
			//var_dump($prefix);exit;
			if(true){
				try {
					$picklist = $objectManager->create('\Raw\PdfInvoice\Model\Picklist');
					$filefactory = $objectManager->create('\Magento\Framework\App\Response\Http\FileFactory');
					$pdf = $picklist->getPdf($loadorder->getInvoiceCollection());
					//$date = $this->datetime->date('Y-m-d_H-i-s');
					$content = $pdf->render();
					$pdf_filename = $prefix."-".$loadorder->getIncrementId().".pdf";
					if($hasbike || $hasbikeitems) {
						$dir = $rootpath."NCPrintFolder/bikesexp/";
						if(!is_dir($dir)) mkdir($dir, 0777, true);
						$pdf->save($dir.$pdf_filename); // dont really want to save?
					} else {
						$dir = $rootpath."PrintFolder/Express/";
						if(!is_dir($dir)) mkdir($dir, 0777, true);
						$pdf->save($dir.$pdf_filename); // dont really want to save?
					//UPLOAD TO EXPRESS QUEUE & LOG
					}
					//exit;
					// Get MOOGENTO INVOUICE CODE
				}
				catch(\Exception $e){
				echo "did not print {$loadorder->getIncrementId()} {$e->getMessage()}";
				}
			}
		}
	}
	*/
	///////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Get all the processing orders
	// Have to do it by sql as mage function is protected
	// We will use this process to select oldest first
	// Cameron asked to skip Cycles store view
	$sql_getorders = "SELECT sfo.`entity_id`,LOWER(sfo.`shipping_description`) as shipping_description
					FROM `sales_order` sfo 
					LEFT JOIN raw_courriermanager_allocations mc on mc.order_id = sfo.entity_id
					WHERE sfo.`status` = 'processing' 
					AND sfo.`customer_group_id` NOT IN ('6') 
					AND sfo.`base_total_invoiced` > 0.00
					
					AND mc.number = 'ALLOCATED'
					
					AND sfo.`store_id` IN (24,25,26,35)
					
					AND sfo.entity_id > 2685273
					ORDER BY sfo.`created_at` 
					ASC LIMIT 1000";
	$orders = $read->fetchAll($sql_getorders);
	// limits set to 100 to protect against double printing from script overlap...  Please DO NOT CHANGE limits unless very sure of reasons.
	// cycles exclude code AND sfo.`store_id` NOT IN ('23')

	// Loop thorough each to make sure an invoice has been created
	$orderIDs = array();
	foreach ($orders as $order){
		$orderrep = $objectManager->create('\Magento\Sales\Api\OrderRepositoryInterface');
		echo '<p>aa'.$order['entity_id'];
		//$loadorder = Mage::getModel('sales/order')->load(474143);
		$loadorder = $orderrep->get($order['entity_id']);
		
		$hasbike = false;
		$hasbike_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_order_item` sfoi
				LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
				WHERE sfoi.`order_id` = ".$loadorder->getID()." 
				AND sfoi.`parent_item_id` IS NULL AND (cpe.`attribute_set_id` IN (9,34))
				GROUP BY cpe.`attribute_set_id`";
	
		// Get the output of the above SQL query
		$hasbikeres = $read->fetchCol($hasbike_getitems);
		if(count($hasbikeres) > 0) $hasbike = true;
		
		
		// Check if order has bike helmet or shoes items - UPDATE only items not in bins starting with "A"
		$hasbikeitems = false;
		$hasbikeitems_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_order_item` sfoi
				LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
				LEFT JOIN `catalog_product_entity_varchar` cpebrand ON cpe.`entity_id`=cpebrand.`entity_id` AND cpebrand.attribute_id = 337 AND cpebrand.store_id = 0
				LEFT JOIN `catalog_product_entity_varchar` cpebin ON cpe.`entity_id`=cpebin.`entity_id` AND cpebin.attribute_id = 136 AND cpebin.store_id = 0
				WHERE sfoi.`order_id` = ".$loadorder->getID()." 
				AND sfoi.`parent_item_id` IS NULL AND cpebin.value LIKE 'T%'
				GROUP BY cpe.`attribute_set_id`";
		$hasbikeitemsres = $read->fetchCol($hasbikeitems_getitems);
		if(count($hasbikeitemsres) > 0) $hasbikeitems = true;
		
		// Try and get the invoice collection
		if ($loadorder->hasInvoices()) {
			echo "here";
			// First we will need to see what the delivery type is - Express just goes straight to the printer
			// Include the EXPRESS SHIPPING terms in an array
			// $express_terms = array('Next Working','Express','Exp','Next Day');
			if(preg_match('/exp/', strtolower($order['shipping_description'])) || preg_match('/dhl/', strtolower($order['shipping_description'])) || preg_match('/next/', strtolower($order['shipping_description'])) || preg_match('/second/', strtolower($order['shipping_description'])) || 'ebay shipping - other 24 hour courier' == strtolower($order['shipping_description']) || 'ebay shipping - royal mail tracked 24' == strtolower($order['shipping_description']) || preg_match('/special/', strtolower($order['shipping_description'])) || preg_match('/prime/', strtolower($order['shipping_description'])) || preg_match('/saturday/', strtolower($order['shipping_description']))){
					$shipping_type = "Express";
					$product_type = "Express";
			}else if (preg_match('/saturday/', strtolower($order['shipping_description']))){
				$shipping_type = "SatDel";
				$product_type = "SatDel";
							}else if (preg_match('/sunday/', strtolower($order['shipping_description']))){
									$shipping_type = "SatDel";
									$product_type = "SatDel";
			}else{
				$shipping_type = "Standard";
				// Select the printer
			
				// We will need to see what products are on the order to decide what printer to send this to
				// We will select the order items table for this part
				$sql_getitems = "SELECT cpe.`attribute_set_id` FROM `sales_order_item` sfoi
						LEFT JOIN `catalog_product_entity` cpe ON sfoi.`product_id`=cpe.`entity_id`
						WHERE sfoi.`order_id` = ".$loadorder->getID()." 
						AND sfoi.`parent_item_id` IS NULL
						GROUP BY cpe.`attribute_set_id`";
			
				// Get the output of the above SQL query
				$attributes = $read->fetchCol($sql_getitems);

				//print_r($attributes);
			
				// We now need to know what the attribute set is
				// All footwear to one printer - Clothing and everything else to another printer - And mixed 
				
				if (count($attributes) > 1 && (in_array("45", $attributes, true) || in_array("10", $attributes, true))){
					// As the array contains more than one item, and includes footwear, this is a mixed order
					$product_type = "Mixed";
					//$product_type = "temp";
				}else if($attributes[0]== 45 || $attributes[0]== 10){
					// This is footwear (fitness footwear)
					$product_type = "Footwear";
					//$product_type = "temp";
				}else{
					// This is clothing or other
					$product_type = "Other";
				}
			}
			
			$prefix = "";
			$orderedItems = $loadorder->getAllItems();
			$orderedProductIds = array();
			foreach ($orderedItems as $item) {
				if ($item->getProductType() == "simple")
				{
						$orderedProductIds[] = $item->getData('product_id');
						
				}
			}
			
			if(count($orderedProductIds) > 0){
				$shelves = array();
								
				
				foreach($orderedProductIds as $product){
					$product = $objectManager->create('Magento\Catalog\Model\Product')->load($product);
					$bin = $product->getData('bin_location');
					
					
					if(!empty($bin))$shelves[] = $bin;
				}
				if(count($shelves) > 0){
					asort($shelves);
					//var_dump($shelves);
					$prefix1 = str_replace('-', '', $shelves[0]);
					$prefix = str_replace(' ', '', $prefix1);
					if(count($shelves) > 1)
					$prefix = "Z".$prefix;
				}
				
			}

			// A little bodge to get the invoices to print - as moogento expects more than one id in an array
			// We pass the single id in an array for it to print out without an error
			
			$read->query("UPDATE sales_order SET status = 'pick_note_printed' WHERE entity_id = ".$loadorder->getId());
			$read->query("UPDATE sales_order_grid SET status = 'pick_note_printed' WHERE entity_id = ".$loadorder->getId());
			//var_dump($prefix);exit;
			if(true){
				try {
					$picklist = $objectManager->create('\Raw\PdfInvoice\Model\Picklist');
					$filefactory = $objectManager->create('\Magento\Framework\App\Response\Http\FileFactory');
					$pdf = $picklist->getPdf($loadorder->getInvoiceCollection());
					//$date = $this->datetime->date('Y-m-d_H-i-s');
					$content = $pdf->render();
					$pdf_filename = $prefix."-".$loadorder->getIncrementId().".pdf";
					$pdfFile = $pdf->render();
					if($hasbikeitems && ($shipping_type=='Express' || $shipping_type=='SatDel')) {
						$pdf->save($rootpath."NCPrintFolder/bikesexp/".$pdf_filename); // dont really want to save?
					} else if ($hasbike || $hasbikeitems) {
						$pdf->save($rootpath."NCPrintFolder/bikes/".$pdf_filename); // dont really want to save?
					} else {
						$pdf->save($rootpath."PrintFolder/".$product_type."/".$pdf_filename); // dont really want to save?
					//UPLOAD TO CORRECT QUEUE & LOG
					}
					//exit;
					// Get MOOGENTO INVOUICE CODE
				}
				catch(\Exception $e){
				echo "did not print {$loadorder->getIncrementId()} {$e->getMessage()}";
				}
			}
		}
	}
}else{
	echo "Currently Out Of Hours - Nothing to print till later - just refreshing (" . date('D jS F - G:s').")";
}

// Finally, destroy the session.
//session_destroy();

echo "<br>This script has completed successfully";

?>
