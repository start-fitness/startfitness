<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../app/bootstrap.php';
 
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->emulateAreaCode('adminhtml', function() use ($objectManager){
    //code that does something `adminhtml` specific here
	
	if(isset($_GET['orderno'])){
	$incrId = $_GET['orderno'];
	$collection = $objectManager->create('Magento\Sales\Model\Order'); 
	$order = $collection->loadByIncrementId($incrId);
	
		if($order && isset($_GET['status'])){
			
			
			$history = $order->addStatusHistoryComment('AUTO PRINT -- JOB ID: '.$_GET['jobid']." -- ".'STATUS: '.$_GET['status']);
			$history->setIsVisibleOnFront(false);
			$history->setIsCustomerNotified(false);
			$history->save();
			echo json_encode(array("status"=>"OK", "messsage"=>"note added to order"));
		}
	}
});


