<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../app/bootstrap.php';
 
$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$storeManager = $objectManager->get(\Magento\Store\Model\StoreManagerInterface::class);
$storeManager->setCurrentStore(0);
$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();

$productscollection = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\CollectionFactory');
$stock = $objectManager->create('\Magento\CatalogInventory\Helper\Stock');
$attrs = $objectManager->create('\Magento\Eav\Model\Config');
$collection = $productscollection->create();
$collection
        ->addAttributeToSelect('*')
		->addAttributeToFilter('status', array('eq' => 1))
		//->addAttributeToFilter('entity_id', 147970)
		->addAttributeToFilter('created_at', array('gt' => (new DateTime())->modify("- 10 days")->format("Y-m-d H:i:s")))
		->setOrder("entity_id", "DESC")
        ->addAttributeToFilter('type_id', 'configurable');
$stock->addInStockFilterToCollection($collection);

$page = 1;
$processed = 0;
do {
    $collection
        ->setPageSize(200)
        ->setCurPage($page);

    $results = $collection->clear()->load();

    // do stuff ......

    
	echo "Processing page $page of ".$collection->getLastPageNumber()."... Processed {$processed}\n";
	$page++;
	foreach($results as $confProduct ){
		$confProduct = $objectManager->create('Magento\Catalog\Model\Product')->load($confProduct->getId());
		//$confProduct = Mage::getModel('catalog/product')->load($confProduct->getId());
		$gallery = $confProduct->getData('media_gallery');
		//var_dump($gallery);exit;
		//get the instance for the media_gallery attribute
		$mediaGalleryAttribute = $attrs->getAttribute('catalog_product', 'media_gallery');
		//get access to a model that handles the image association to products
		$resource = $objectManager->create('\Magento\Catalog\Model\ResourceModel\Product\Gallery');
		
		
		if (isset($gallery['images'])) {
		    //get all simple child products ids 
		    $simpleProductIds = $confProduct->getTypeInstance()->getUsedProductIds($confProduct);
		    //for each simple product
		    var_dump($simpleProductIds[0]);
		    foreach ($simpleProductIds as $productId) {
		    	
				$childp = $objectManager->create('Magento\Catalog\Model\Product')->load($productId);
				
				$cgallery = $childp->getData('media_gallery');
				if(isset($cgallery['images']) && count($cgallery['images']) > 0) {
					//var_dump($cgallery['images']);
					//echo $confProduct->getSku()." children have images\n";
					continue;
					
				}
				else{
					//var_dump($confProduct->getId());
				}
				try{
			        //for eahc image from the main product
			        foreach ($gallery['images'] as $image) {
			            //build an array with imag settings
			            $data = array();
			            
			            $data['attribute_id']   = $mediaGalleryAttribute->getId();
			            $data['value']          = $image['file'];
						
			            //save the image and get the id
			            $lastId = $resource->insertGallery($data);
			            //set the label and position for the new images
			            $data = array();
						$data['entity_id']      = $productId;
			            $data['value_id'] = $lastId;
			            $data['label']    = $image['label'];
			            $data['position'] = (int) $image['position'];
			            $data['disabled'] = (int) $image['disabled'];
			            $data['store_id'] = (int) 0;
						
			            //finish the job
			            $resource->bindValueToEntity($lastId, $productId);
						$resource->insertGalleryValueInStore($data);
			        }
			    
					file_put_contents("idsupdated.txt", $productId, FILE_APPEND);
					$processed++;
					}
					catch(exception $e){
						var_dump($e->getMessage());			
					}
					try{
						$resourcecp = $childp->getResource();

						$childp->setData("image", $confProduct->getData("image"));
						$resourcecp->saveAttribute($childp, "image");
						
						$childp->setData("small_image", $confProduct->getData("small_image"));
						$resourcecp->saveAttribute($childp, "small_image");
						
						$childp->setData("thumbnail", $confProduct->getData("thumbnail"));
						$resourcecp->saveAttribute($childp, "thumbnail");
					
					}
					catch(exception $e){
						var_dump($e->getMessage());			
					}
					//var_dump($data);exit;
					//
				
			}
			
			
			
		}
	}

} while ($page <= $collection->getLastPageNumber());
		
echo "Processing page $page of ".$collection->getLastPageNumber()."... Processed {$processed}\n";



