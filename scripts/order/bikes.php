<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$transportBuilder = $objectManager->create('\Magento\Framework\Mail\Template\TransportBuilder');
////$read = Mage::getSingleton('core/resource')->getConnection('core_read');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();
$date = new DateTime();
$date->add(DateInterval::createFromDateString('yesterday'));
$config = $objectManager->get('\Magento\Framework\App\Config\ScopeConfigInterface');
$datetd = new DateTime();
//echo $date->format('F j, Y') . "\n";
// Get the SQL Read/Write Setup
$OrderFactory = $objectManager->create('Magento\Sales\Model\ResourceModel\Order\CollectionFactory');
$orderCollection = $OrderFactory->create();
$_orders = $orderCollection
->addFieldToFilter('status',array('eq' => 'bike_awaiting_shipping'));


foreach($_orders as $order){
	$sql = "SELECT * FROM bike_awaiting_shipping WHERE order_id = ".$order->getEntityId()."";
	$results = $read->fetchAll($sql);
	if(count($results) == 0){
		
		$storeId = 6;
		 $sender  = array(
	        'name' => $config->getValue('trans_email/ident_custom2/name', \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$storeId),
	        'email' => $config->getValue('trans_email/ident_custom2/email', \Magento\Store\Model\ScopeInterface::SCOPE_STORE,$storeId)
	    );
	     $vars = array(
	    'ordernumber' => $order->getIncrementId(),
	    );
	    $templateId = 58;
		
	    /*try{
	    	$sql ="INSERT INTO bike_awaiting_shipping (order_id) VALUE(".$order->getEntityId().")";
			$read->query($sql);
	    	Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $order->getCustomerEmail() , '', $vars, $storeId);
		$order->addStatusToHistory('bike_awaiting_shipping', 'Bike Awaiting Dispatch Email Sent To Customer', false);
		$order->save();
		}
		catch(exception $e){
			
		}*/
		
		try {
			$sql ="INSERT INTO bike_awaiting_shipping (order_id) VALUE(".$order->getEntityId().")";
			$read->query($sql);
            // template variables pass here
            $templateVars = [
                'ordernumber' => $order->getIncrementId(),
            ];
 
            $storeId = $storeId;
 
            $from = [
				'name' => $config->getValue('trans_email/ident_custom2/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId),
				'email' => $config->getValue('trans_email/ident_custom2/email',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId)
			];
 
            $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
            $templateOptions = [
                'area' => \Magento\Framework\App\Area::AREA_FRONTEND,
                'store' => $storeId
            ];
            $transport = $transportBuilder->setTemplateIdentifier($templateId, $storeScope)
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($order->getCustomerEmail())
                ->getTransport();
            $transport->sendMessage();
			$order->addStatusToHistory('bike_awaiting_shipping', 'Bike Awaiting Dispatch Email Sent To Customer', false);
			$order->save();
        } catch (\Exception $e) {
			var_dump($e->getMessage());
            $this->_logger->info($e->getMessage());
        }
		//exit;
		
//echo $order->getCustomerEmail();
//echo $order->getEntityId();
		
	}
}
