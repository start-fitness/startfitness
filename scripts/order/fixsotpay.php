<?php
use Magento\Framework\App\Bootstrap;
ini_set('display_startup_errors',1);
ini_set('display_errors',1);

ini_set('memory_limit', '16048M');
require __DIR__ . '/../../app/bootstrap.php';
$params = $_SERVER;
 
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('frontend');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$writeConnection = $read = $resource->getConnection();
$sql = "
UPDATE  sales_order
join sales_invoice inv on inv.order_id =  sales_order.entity_id
JOIn sales_order_payment pay on pay.parent_id =  sales_order.entity_id
JOIN sales_invoice_item invi ON invi.parent_id = inv.entity_id
SET inv.base_grand_total = inv.grand_total, inv.base_tax_amount = inv.tax_amount, inv.base_shipping_tax_amount = inv.shipping_tax_amount,
inv.base_discount_amount = inv.discount_amount, inv.base_subtotal_incl_tax = inv.subtotal_incl_tax, inv.base_shipping_amount = inv.shipping_amount,
inv.base_subtotal = inv.subtotal, inv.base_shipping_incl_tax = inv.shipping_incl_tax,
invi.base_price = invi.price, invi.base_row_total = invi.row_total, invi.base_discount_amount = invi.discount_amount, invi.base_tax_amount = invi.tax_amount,
invi.base_price_incl_tax = invi.price_incl_tax, invi.base_row_total_incl_tax = invi.row_total_incl_tax
WHERE pay.method = \"sotpay\" AND sales_order.base_currency_code = sales_order.order_currency_code
";

$writeConnection->query($sql);