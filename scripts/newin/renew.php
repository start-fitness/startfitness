<?php
use Magento\Framework\App\Bootstrap;
use Magento\Framework\App\Filesystem\DirectoryList;

require __DIR__ . '/../../app/bootstrap.php';
 
$params = $_SERVER;
$bootstrap = Bootstrap::create(BP, $params);
 
$objectManager = $bootstrap->getObjectManager();
 
$state = $objectManager->get('Magento\Framework\App\State');
$state->setAreaCode('adminhtml');

$resource = $objectManager->get('Magento\Framework\App\ResourceConnection');
$write = $read = $resource->getConnection();

$sql = ["

DELETE
FROM catalog_category_product
WHERE catalog_category_product.category_id = 3576;
","

INSERT INTO catalog_category_product
SELECT NULL,
       3576,
       e.entity_id,
       0
FROM `catalog_product_entity` AS `e`
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_from_date_default` ON (`at_news_from_date_default`.`entity_id` = `e`.`entity_id`)
AND (`at_news_from_date_default`.`attribute_id` = '93')
AND `at_news_from_date_default`.`store_id` = 0
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_from_date` ON (`at_news_from_date`.`entity_id` = `e`.`entity_id`)
AND (`at_news_from_date`.`attribute_id` = '93')
AND (`at_news_from_date`.`store_id` = 6)
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_to_date_default` ON (`at_news_to_date_default`.`entity_id` = `e`.`entity_id`)
AND (`at_news_to_date_default`.`attribute_id` = '94')
AND `at_news_to_date_default`.`store_id` = 0
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_to_date` ON (`at_news_to_date`.`entity_id` = `e`.`entity_id`)
AND (`at_news_to_date`.`attribute_id` = '94')
AND (`at_news_to_date`.`store_id` = 6)
WHERE (((((DATE(IF(at_news_from_date.value_id > 0, at_news_from_date.value, at_news_from_date_default.value)) <= DATE(NOW()))))))
  AND (((((DATE(IF(at_news_to_date.value_id > 0, at_news_to_date.value, at_news_to_date_default.value)) >= DATE(NOW()))))));
","

DELETE
FROM catalog_category_product_index
WHERE catalog_category_product_index.category_id = 3576;

","

INSERT INTO catalog_category_product_index
SELECT 3576,
       e.entity_id,
       0,
       0,
       6,
       vis.value
FROM `catalog_product_entity` AS `e`
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_from_date_default` ON (`at_news_from_date_default`.`entity_id` = `e`.`entity_id`)
AND (`at_news_from_date_default`.`attribute_id` = '93')
AND `at_news_from_date_default`.`store_id` = 0
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_from_date` ON (`at_news_from_date`.`entity_id` = `e`.`entity_id`)
AND (`at_news_from_date`.`attribute_id` = '93')
AND (`at_news_from_date`.`store_id` = 6)
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_to_date_default` ON (`at_news_to_date_default`.`entity_id` = `e`.`entity_id`)
AND (`at_news_to_date_default`.`attribute_id` = '94')
AND `at_news_to_date_default`.`store_id` = 0
LEFT JOIN `catalog_product_entity_datetime` AS `at_news_to_date` ON (`at_news_to_date`.`entity_id` = `e`.`entity_id`)
AND (`at_news_to_date`.`attribute_id` = '94')
AND (`at_news_to_date`.`store_id` = 6)
LEFT JOIN `catalog_product_entity_int` AS `vis` ON (`vis`.`entity_id` = `e`.`entity_id`)
AND (`vis`.`attribute_id` = '102')
AND (`vis`.`store_id` = 0)
WHERE (((((DATE(IF(at_news_from_date.value_id > 0, at_news_from_date.value, at_news_from_date_default.value)) <= DATE(NOW()))))))
  AND (((((DATE(IF(at_news_to_date.value_id > 0, at_news_to_date.value, at_news_to_date_default.value)) >= DATE(NOW()))))))
  AND vis.value <> 1;
"];
foreach( $sql as $query){
	$write->query($query);
}

$cacheManager = $objectManager->get('\Magento\Framework\App\CacheInterface');
$cacheManager->clean('catalog_category_3576');

$write->query("
INSERT INTO brands_products_count
SELECT * FROM (
SELECT at_manufacturer.value, count(*) as count1 FROM `catalog_product_entity` AS `e`
 INNER JOIN `catalog_category_product_index_store6` AS `cat_index` ON cat_index.product_id=e.entity_id AND cat_index.store_id=6 AND cat_index.visibility IN(2, 4) AND cat_index.category_id=2473
 INNER JOIN `catalog_product_entity_int` AS `at_status_default` ON (`at_status_default`.`entity_id` = `e`.`entity_id`) AND (`at_status_default`.`attribute_id` = '96') AND `at_status_default`.`store_id` = 0
 LEFT JOIN `catalog_product_entity_int` AS `at_status` ON (`at_status`.`entity_id` = `e`.`entity_id`) AND (`at_status`.`attribute_id` = '96') AND (`at_status`.`store_id` = 6)
 INNER JOIN `catalog_product_entity_int` AS `at_manufacturer` ON (`at_manufacturer`.`entity_id` = `e`.`entity_id`) AND (`at_manufacturer`.`attribute_id` = '81') AND (`at_manufacturer`.`store_id` = 0)
 INNER JOIN `cataloginventory_stock_item` AS `at_stock_item` ON (at_stock_item.`product_id`=e.entity_id) AND (is_in_stock=1) WHERE (IF(at_status.value_id > 0, at_status.value, at_status_default.value) = '1') 
 AND  at_manufacturer.value IS NOT NULL
 GROUP BY at_manufacturer.value) as derived
 ON DUPLICATE KEY UPDATE `count` = count1

");
