<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;

class MPSearchClassmap
{

    public static function getCollection() : \Phpro\SoapClient\Soap\ClassMap\ClassMapCollection
    {
        return new ClassMapCollection([
            new ClassMap('DateRange', Type\DateRange::class),
            new ClassMap('Product', Type\Product::class),
            new ClassMap('Parcel', Type\Parcel::class),
            new ClassMap('Property', Type\Property::class),
            new ClassMap('Address', Type\Address::class),
            new ClassMap('VerifiedAddress', Type\VerifiedAddress::class),
            new ClassMap('Consignment', Type\Consignment::class),
            new ClassMap('ConsignmentSearchParams', Type\ConsignmentSearchParams::class),
            new ClassMap('ConsignmentSearchResult', Type\ConsignmentSearchResult::class),
            new ClassMap('SearchCondition', Type\SearchCondition::class),
            new ClassMap('SearchOrderBy', Type\SearchOrderBy::class),
            new ClassMap('SearchParams', Type\SearchParams::class),
            new ClassMap('SearchRecord', Type\SearchRecord::class),
            new ClassMap('SearchResult', Type\SearchResult::class),
        ]);
    }


}

