<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Type\RequestInterface;
use Phpro\SoapClient\Exception\SoapException;
use Phpro\SoapClient\Type\ResultInterface;

class MPConsClient extends \Phpro\SoapClient\Client
{

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $numberOfLabels
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createNextLabelsAsPdf(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createNextLabelsAsPdf', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\StringType
     * @throws SoapException
     */
    public function createLabelsAsPdf(string $consignmentCode) : \Metapack\Type\StringType
    {
        return $this->call('createLabelsAsPdf', $consignmentCode);
    }

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\StringType
     * @throws SoapException
     */
    public function createDocumentationAsPdf(string $consignmentCode) : \Metapack\Type\StringType
    {
        return $this->call('createDocumentationAsPdf', $consignmentCode);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * Metapack\Type\Integer $parcelNumber
     * string $printerType
     * string $documentType
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createDocumentationPdf(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createDocumentationPdf', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\Labels
     * @throws SoapException
     */
    public function createPdfsForConsignments(string $consignmentCodes) : \Metapack\Type\Labels
    {
        return $this->call('createPdfsForConsignments', $consignmentCodes);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\StringType
     * @throws SoapException
     */
    public function createBulkLabelsAsPdf(string $consignmentCodes) : \Metapack\Type\StringType
    {
        return $this->call('createBulkLabelsAsPdf', $consignmentCodes);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $parcelNumber
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createLabelAsPdf(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createLabelAsPdf', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\StringType
     * @throws SoapException
     */
    public function createBulkDocumentationAsPdf(string $consignmentCodes) : \Metapack\Type\StringType
    {
        return $this->call('createBulkDocumentationAsPdf', $consignmentCodes);
    }

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\TaxAndDuty
     * @throws SoapException
     */
    public function calculateTaxAndDuty(string $consignmentCode) : \Metapack\Type\TaxAndDuty
    {
        return $this->call('calculateTaxAndDuty', $consignmentCode);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $innerConsignmentCode
     * Metapack\Type\Integer $innerParcelNumber
     * string $outerConsignmentCode
     * Metapack\Type\Integer $outerParcelNumber
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function addInnerToOuter(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('addInnerToOuter', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * string $reasonCode
     * string $reason
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\ConsignmentActionResult
     */
    public function voidConsignments(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\ConsignmentActionResult
    {
        return $this->call('voidConsignments', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $parcelNo
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function deleteParcelFromConsignment(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('deleteParcelFromConsignment', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $cartonId
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function deleteParcelFromConsignmentWithCartonId(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('deleteParcelFromConsignmentWithCartonId', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\Boolean
     * @throws SoapException
     */
    public function markConsignmentsAsReadyToManifest(string $consignmentCodes) : \Metapack\Type\Boolean
    {
        return $this->call('markConsignmentsAsReadyToManifest', $consignmentCodes);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\Boolean
     * @throws SoapException
     */
    public function markConsignmentsAsPrinted(string $consignmentCodes) : \Metapack\Type\Boolean
    {
        return $this->call('markConsignmentsAsPrinted', $consignmentCodes);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Parcel $parcels
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Parcel
     */
    public function appendParcelsToConsignment(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Parcel
    {
        return $this->call('appendParcelsToConsignment', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $parcelNumber
     * Metapack\Type\Product $products
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function packProductsToParcel(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('packProductsToParcel', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $parcelNumber
     * Metapack\Type\Product $products
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function unpackProductsFromParcel(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('unpackProductsFromParcel', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $innerConsignmentCode
     * Metapack\Type\Integer $innerParcelNumber
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function removeInnerFromOuter(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('removeInnerFromOuter', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * string $manifestGroupCode
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Boolean
     */
    public function addConsignmentsToGroup(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Boolean
    {
        return $this->call('addConsignmentsToGroup', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\Boolean
     * @throws SoapException
     */
    public function removeConsignmentsFromGroup(string $consignmentCodes) : \Metapack\Type\Boolean
    {
        return $this->call('removeConsignmentsFromGroup', $consignmentCodes);
    }

    /**
     * @param RequestInterface|Type\Consignment $consignments
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function updateConsignments(\Metapack\Type\Consignment $consignments) : \Metapack\Type\Consignment
    {
        return $this->call('updateConsignments', $consignments);
    }

    /**
     * @param RequestInterface|Type\Consignment $consignments
     * @return ResultInterface|Type\Boolean
     * @throws SoapException
     */
    public function validateConsignments(\Metapack\Type\Consignment $consignments) : \Metapack\Type\Boolean
    {
        return $this->call('validateConsignments', $consignments);
    }

    /**
     * @param RequestInterface|Type\Consignment $consignments
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function createConsignments(\Metapack\Type\Consignment $consignments) : \Metapack\Type\Consignment
    {
        return $this->call('createConsignments', $consignments);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\Integer $parcelNumber
     * float $dpi
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createLabelAsZPL(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createLabelAsZPL', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $cartonId
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createDocumentationForCartonIdAsPdf(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createDocumentationForCartonIdAsPdf', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $cartonId
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createLabelForCartonIdAsPdf(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createLabelForCartonIdAsPdf', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * float $dpi
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createLabelsAsZPL(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createLabelsAsZPL', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $cartonId
     * float $dpi
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createLabelForCartonIdAsZPL(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createLabelForCartonIdAsZPL', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * float $dpi
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\StringType
     */
    public function createBulkLabelsAsZPL(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\StringType
    {
        return $this->call('createBulkLabelsAsZPL', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\AuditRecord
     * @throws SoapException
     */
    public function findConsignmentAuditRecords(string $consignmentCode) : \Metapack\Type\AuditRecord
    {
        return $this->call('findConsignmentAuditRecords', $consignmentCode);
    }

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\Boolean
     * @throws SoapException
     */
    public function deleteConsignment(string $consignmentCode) : \Metapack\Type\Boolean
    {
        return $this->call('deleteConsignment', $consignmentCode);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\UpdateField $updateFields
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function update(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('update', $multiArgumentRequest);
    }


}

