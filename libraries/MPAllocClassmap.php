<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;

class MPAllocClassmap
{

    public static function getCollection() : \Phpro\SoapClient\Soap\ClassMap\ClassMapCollection
    {
        return new ClassMapCollection([
            new ClassMap('WorkingDays', Type\WorkingDays::class),
            new ClassMap('DateRange', Type\DateRange::class),
            new ClassMap('AllocationFilter', Type\AllocationFilter::class),
            new ClassMap('Product', Type\Product::class),
            new ClassMap('Parcel', Type\Parcel::class),
            new ClassMap('Property', Type\Property::class),
            new ClassMap('Address', Type\Address::class),
            new ClassMap('VerifiedAddress', Type\VerifiedAddress::class),
            new ClassMap('Consignment', Type\Consignment::class),
            new ClassMap('DeliveryOption', Type\DeliveryOption::class),
            new ClassMap('Labels', Type\Labels::class),
            new ClassMap('DespatchedConsignment', Type\DespatchedConsignment::class),
        ]);
    }


}

