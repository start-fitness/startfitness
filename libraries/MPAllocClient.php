<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Type\RequestInterface;
use Phpro\SoapClient\Type\ResultInterface;
use Phpro\SoapClient\Exception\SoapException;

class MPAllocClient extends \Phpro\SoapClient\Client
{

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignment
     * Metapack\Type\AllocationFilter $filter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DeliveryOption
     */
    public function findDeliveryOptions(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DeliveryOption
    {
        return $this->call('findDeliveryOptions', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * string $bookingCode
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function allocateConsignmentsWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('allocateConsignmentsWithBookingCode', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\AllocationFilter $filter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function verifyAllocation(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('verifyAllocation', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignment
     * string $bookingCode
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DeliveryOption
     */
    public function findDeliveryOptionsWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DeliveryOption
    {
        return $this->call('findDeliveryOptionsWithBookingCode', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * Metapack\Type\AllocationFilter $filter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DeliveryOption
     */
    public function findDeliveryOptionsForConsignment(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DeliveryOption
    {
        return $this->call('findDeliveryOptionsForConsignment', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $bookingCode
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DeliveryOption
     */
    public function findDeliveryOptionsForConsignmentWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DeliveryOption
    {
        return $this->call('findDeliveryOptionsForConsignmentWithBookingCode', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignments
     * Metapack\Type\AllocationFilter $filter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function createAndAllocateConsignments(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('createAndAllocateConsignments', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignments
     * string $bookingCode
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function createAndAllocateConsignmentsWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Phpro\SoapClient\Type\MixedResult
    {
        return $this->call('createAndAllocateConsignmentsWithBookingCode', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCode
     * string $bookingCode
     * Metapack\Type\Boolean $recalculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function verifyAllocationWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('verifyAllocationWithBookingCode', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * string $consignmentCodes
     * Metapack\Type\AllocationFilter $filter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\Consignment
     */
    public function allocateConsignments(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\Consignment
    {
        return $this->call('allocateConsignments', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignment
     * Metapack\Type\AllocationFilter $allocationFilter
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DespatchedConsignment
     */
    public function despatchConsignment(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DespatchedConsignment
    {
        return $this->call('despatchConsignment', $multiArgumentRequest);
    }

    /**
     * MultiArgumentRequest with following params:
     *
     * Metapack\Type\Consignment $consignment
     * string $bookingCode
     * Metapack\Type\Boolean $calculateTaxAndDuty
     *
     * @param Phpro\SoapClient\Type\MultiArgumentRequest
     * @return ResultInterface|Type\DespatchedConsignment
     */
    public function despatchConsignmentWithBookingCode(\Phpro\SoapClient\Type\MultiArgumentRequest $multiArgumentRequest) : \Metapack\Type\DespatchedConsignment
    {
        return $this->call('despatchConsignmentWithBookingCode', $multiArgumentRequest);
    }

    /**
     * @param RequestInterface|\string $consignmentCodes
     * @return ResultInterface|Type\StringType
     * @throws SoapException
     */
    public function deallocate(string $consignmentCodes) : \Metapack\Type\StringType
    {
        return $this->call('deallocate', $consignmentCodes);
    }


}

