<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\ResultInterface;

class DespatchedConsignment implements ResultInterface
{

    /**
     * @var \Metapack\Type\Consignment
     */
    private $consignment;

    /**
     * @var \Metapack\Type\Labels
     */
    private $labels;

    /**
     * @return \Metapack\Type\Consignment
     */
    public function getConsignment()
    {
        return $this->consignment;
    }

    /**
     * @param \Metapack\Type\Consignment $consignment
     * @return DespatchedConsignment
     */
    public function withConsignment($consignment)
    {
        $new = clone $this;
        $new->consignment = $consignment;

        return $new;
    }

    /**
     * @return \Metapack\Type\Labels
     */
    public function getLabels()
    {
        return $this->labels;
    }

    /**
     * @param \Metapack\Type\Labels $labels
     * @return DespatchedConsignment
     */
    public function withLabels($labels)
    {
        $new = clone $this;
        $new->labels = $labels;

        return $new;
    }


}

