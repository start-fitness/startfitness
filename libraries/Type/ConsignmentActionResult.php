<?php

namespace Metapack\Type;

class ConsignmentActionResult
{

    /**
     * @var string
     */
    private $consignmentCode;

    /**
     * @var string
     */
    private $errorCode;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $text;

    /**
     * @return string
     */
    public function getConsignmentCode()
    {
        return $this->consignmentCode;
    }

    /**
     * @param string $consignmentCode
     * @return ConsignmentActionResult
     */
    public function withConsignmentCode($consignmentCode)
    {
        $new = clone $this;
        $new->consignmentCode = $consignmentCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param string $errorCode
     * @return ConsignmentActionResult
     */
    public function withErrorCode($errorCode)
    {
        $new = clone $this;
        $new->errorCode = $errorCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return ConsignmentActionResult
     */
    public function withReference($reference)
    {
        $new = clone $this;
        $new->reference = $reference;

        return $new;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return ConsignmentActionResult
     */
    public function withText($text)
    {
        $new = clone $this;
        $new->text = $text;

        return $new;
    }


}

