<?php

namespace Metapack\Type;

class SearchRecord
{

    /**
     * @var string
     */
    private $result;

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param string $result
     * @return SearchRecord
     */
    public function withResult($result)
    {
        $new = clone $this;
        $new->result = $result;

        return $new;
    }


}

