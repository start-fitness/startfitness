<?php

namespace Metapack\Type;

class WorkingDays
{

    /**
     * @var bool
     */
    private $friday;

    /**
     * @var bool
     */
    private $monday;

    /**
     * @var bool
     */
    private $saturday;

    /**
     * @var bool
     */
    private $sunday;

    /**
     * @var bool
     */
    private $thursday;

    /**
     * @var bool
     */
    private $tuesday;

    /**
     * @var bool
     */
    private $wednesday;

    /**
     * @return bool
     */
    public function getFriday()
    {
        return $this->friday;
    }

    /**
     * @param bool $friday
     * @return WorkingDays
     */
    public function withFriday($friday)
    {
        $new = clone $this;
        $new->friday = $friday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getMonday()
    {
        return $this->monday;
    }

    /**
     * @param bool $monday
     * @return WorkingDays
     */
    public function withMonday($monday)
    {
        $new = clone $this;
        $new->monday = $monday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getSaturday()
    {
        return $this->saturday;
    }

    /**
     * @param bool $saturday
     * @return WorkingDays
     */
    public function withSaturday($saturday)
    {
        $new = clone $this;
        $new->saturday = $saturday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getSunday()
    {
        return $this->sunday;
    }

    /**
     * @param bool $sunday
     * @return WorkingDays
     */
    public function withSunday($sunday)
    {
        $new = clone $this;
        $new->sunday = $sunday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getThursday()
    {
        return $this->thursday;
    }

    /**
     * @param bool $thursday
     * @return WorkingDays
     */
    public function withThursday($thursday)
    {
        $new = clone $this;
        $new->thursday = $thursday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getTuesday()
    {
        return $this->tuesday;
    }

    /**
     * @param bool $tuesday
     * @return WorkingDays
     */
    public function withTuesday($tuesday)
    {
        $new = clone $this;
        $new->tuesday = $tuesday;

        return $new;
    }

    /**
     * @return bool
     */
    public function getWednesday()
    {
        return $this->wednesday;
    }

    /**
     * @param bool $wednesday
     * @return WorkingDays
     */
    public function withWednesday($wednesday)
    {
        $new = clone $this;
        $new->wednesday = $wednesday;

        return $new;
    }


}

