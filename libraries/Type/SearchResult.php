<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\ResultInterface;

class SearchResult implements ResultInterface
{

    /**
     * @var int
     */
    private $pageCount;

    /**
     * @var \Metapack\Type\SearchRecord
     */
    private $records;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * @return int
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }

    /**
     * @param int $pageCount
     * @return SearchResult
     */
    public function withPageCount($pageCount)
    {
        $new = clone $this;
        $new->pageCount = $pageCount;

        return $new;
    }

    /**
     * @return \Metapack\Type\SearchRecord
     */
    public function getRecords()
    {
        return $this->records;
    }

    /**
     * @param \Metapack\Type\SearchRecord $records
     * @return SearchResult
     */
    public function withRecords($records)
    {
        $new = clone $this;
        $new->records = $records;

        return $new;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return SearchResult
     */
    public function withTotalCount($totalCount)
    {
        $new = clone $this;
        $new->totalCount = $totalCount;

        return $new;
    }


}

