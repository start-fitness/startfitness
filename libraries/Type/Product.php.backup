<?php

namespace Metapack\Type;

class Product
{

    /**
     * @var string
     */
    private $countryOfOrigin = null;

    /**
     * @var string
     */
    private $fabricContent = null;

    /**
     * @var string
     */
    private $harmonisedProductCode = null;

    /**
     * @var string
     */
    private $miscellaneousInfo = null;

    /**
     * @var string
     */
    private $productCode = null;

    /**
     * @var string
     */
    private $productDescription = null;

    /**
     * @var int
     */
    private $productQuantity = null;

    /**
     * @var string
     */
    private $productTypeDescription = null;

    /**
     * @var float
     */
    private $totalProductValue = null;

    /**
     * @var float
     */
    private $unitProductWeight = null;

    /**
     * @return string
     */
    public function getCountryOfOrigin()
    {
        return $this->countryOfOrigin;
    }

    /**
     * @param string $countryOfOrigin
     * @return Product
     */
    public function withCountryOfOrigin($countryOfOrigin)
    {
        $new = clone $this;
        $new->countryOfOrigin = $countryOfOrigin;

        return $new;
    }

    /**
     * @return string
     */
    public function getFabricContent()
    {
        return $this->fabricContent;
    }

    /**
     * @param string $fabricContent
     * @return Product
     */
    public function withFabricContent($fabricContent)
    {
        $new = clone $this;
        $new->fabricContent = $fabricContent;

        return $new;
    }

    /**
     * @return string
     */
    public function getHarmonisedProductCode()
    {
        return $this->harmonisedProductCode;
    }

    /**
     * @param string $harmonisedProductCode
     * @return Product
     */
    public function withHarmonisedProductCode($harmonisedProductCode)
    {
        $new = clone $this;
        $new->harmonisedProductCode = $harmonisedProductCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getMiscellaneousInfo()
    {
        return $this->miscellaneousInfo;
    }

    /**
     * @param string $miscellaneousInfo
     * @return Product
     */
    public function withMiscellaneousInfo($miscellaneousInfo)
    {
        $new = clone $this;
        $new->miscellaneousInfo = $miscellaneousInfo;

        return $new;
    }

    /**
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * @param string $productCode
     * @return Product
     */
    public function withProductCode($productCode)
    {
        $new = clone $this;
        $new->productCode = $productCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getProductDescription()
    {
        return $this->productDescription;
    }

    /**
     * @param string $productDescription
     * @return Product
     */
    public function withProductDescription($productDescription)
    {
        $new = clone $this;
        $new->productDescription = $productDescription;

        return $new;
    }

    /**
     * @return int
     */
    public function getProductQuantity()
    {
        return $this->productQuantity;
    }

    /**
     * @param int $productQuantity
     * @return Product
     */
    public function withProductQuantity($productQuantity)
    {
        $new = clone $this;
        $new->productQuantity = $productQuantity;

        return $new;
    }

    /**
     * @return string
     */
    public function getProductTypeDescription()
    {
        return $this->productTypeDescription;
    }

    /**
     * @param string $productTypeDescription
     * @return Product
     */
    public function withProductTypeDescription($productTypeDescription)
    {
        $new = clone $this;
        $new->productTypeDescription = $productTypeDescription;

        return $new;
    }

    /**
     * @return float
     */
    public function getTotalProductValue()
    {
        return $this->totalProductValue;
    }

    /**
     * @param float $totalProductValue
     * @return Product
     */
    public function withTotalProductValue($totalProductValue)
    {
        $new = clone $this;
        $new->totalProductValue = $totalProductValue;

        return $new;
    }

    /**
     * @return float
     */
    public function getUnitProductWeight()
    {
        return $this->unitProductWeight;
    }

    /**
     * @param float $unitProductWeight
     * @return Product
     */
    public function withUnitProductWeight($unitProductWeight)
    {
        $new = clone $this;
        $new->unitProductWeight = $unitProductWeight;

        return $new;
    }


}

