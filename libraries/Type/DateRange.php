<?php

namespace Metapack\Type;

class DateRange
{

    /**
     * @var \DateTimeInterface
     */
    private $from = null;

    /**
     * @var \DateTimeInterface
     */
    private $to = null;

    /**
     * @return \DateTimeInterface
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param \DateTimeInterface $from
     * @return DateRange
     */
    public function withFrom($from)
    {
        $new = clone $this;
        $new->from = $from;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param \DateTimeInterface $to
     * @return DateRange
     */
    public function withTo($to)
    {
        $new = clone $this;
        $new->to = $to;

        return $new;
    }


}

