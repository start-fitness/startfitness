<?php

namespace Metapack\Type;

class Parcel
{

    /**
     * @var string
     */
    private $cartonId = null;

    /**
     * @var string
     */
    private $code = null;

    /**
     * @var string
     */
    private $destinationReference = null;

    /**
     * @var float
     */
    private $dutyPaid = null;

    /**
     * @var int
     */
    private $number = null;

    /**
     * @var string
     */
    private $originReference = null;

    /**
     * @var string
     */
    private $outerConsignmentCode = null;

    /**
     * @var int
     */
    private $outerParcelNumber = null;

    /**
     * @var float
     */
    private $parcelDepth = null;

    /**
     * @var float
     */
    private $parcelHeight = null;

    /**
     * @var float
     */
    private $parcelValue = null;

    /**
     * @var float
     */
    private $parcelWeight = null;

    /**
     * @var float
     */
    private $parcelWidth = null;

    /**
     * @var \Metapack\Type\Product
     */
    private $products = null;

    /**
     * @var string
     */
    private $recipientReference = null;

    /**
     * @var string
     */
    private $reference = null;

    /**
     * @var string
     */
    private $senderReference = null;

    /**
     * @var string
     */
    private $trackingCode = null;

    /**
     * @var string
     */
    private $trackingUrl = null;

    /**
     * @return string
     */
    public function getCartonId()
    {
        return $this->cartonId;
    }

    /**
     * @param string $cartonId
     * @return Parcel
     */
    public function withCartonId($cartonId)
    {
        $new = clone $this;
        $new->cartonId = $cartonId;

        return $new;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Parcel
     */
    public function withCode($code)
    {
        $new = clone $this;
        $new->code = $code;

        return $new;
    }

    /**
     * @return string
     */
    public function getDestinationReference()
    {
        return $this->destinationReference;
    }

    /**
     * @param string $destinationReference
     * @return Parcel
     */
    public function withDestinationReference($destinationReference)
    {
        $new = clone $this;
        $new->destinationReference = $destinationReference;

        return $new;
    }

    /**
     * @return float
     */
    public function getDutyPaid()
    {
        return $this->dutyPaid;
    }

    /**
     * @param float $dutyPaid
     * @return Parcel
     */
    public function withDutyPaid($dutyPaid)
    {
        $new = clone $this;
        $new->dutyPaid = $dutyPaid;

        return $new;
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param int $number
     * @return Parcel
     */
    public function withNumber($number)
    {
        $new = clone $this;
        $new->number = $number;

        return $new;
    }

    /**
     * @return string
     */
    public function getOriginReference()
    {
        return $this->originReference;
    }

    /**
     * @param string $originReference
     * @return Parcel
     */
    public function withOriginReference($originReference)
    {
        $new = clone $this;
        $new->originReference = $originReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getOuterConsignmentCode()
    {
        return $this->outerConsignmentCode;
    }

    /**
     * @param string $outerConsignmentCode
     * @return Parcel
     */
    public function withOuterConsignmentCode($outerConsignmentCode)
    {
        $new = clone $this;
        $new->outerConsignmentCode = $outerConsignmentCode;

        return $new;
    }

    /**
     * @return int
     */
    public function getOuterParcelNumber()
    {
        return $this->outerParcelNumber;
    }

    /**
     * @param int $outerParcelNumber
     * @return Parcel
     */
    public function withOuterParcelNumber($outerParcelNumber)
    {
        $new = clone $this;
        $new->outerParcelNumber = $outerParcelNumber;

        return $new;
    }

    /**
     * @return float
     */
    public function getParcelDepth()
    {
        return $this->parcelDepth;
    }

    /**
     * @param float $parcelDepth
     * @return Parcel
     */
    public function withParcelDepth($parcelDepth)
    {
        $new = clone $this;
        $new->parcelDepth = $parcelDepth;

        return $new;
    }

    /**
     * @return float
     */
    public function getParcelHeight()
    {
        return $this->parcelHeight;
    }

    /**
     * @param float $parcelHeight
     * @return Parcel
     */
    public function withParcelHeight($parcelHeight)
    {
        $new = clone $this;
        $new->parcelHeight = $parcelHeight;

        return $new;
    }

    /**
     * @return float
     */
    public function getParcelValue()
    {
        return $this->parcelValue;
    }

    /**
     * @param float $parcelValue
     * @return Parcel
     */
    public function withParcelValue($parcelValue)
    {
        $new = clone $this;
        $new->parcelValue = $parcelValue;

        return $new;
    }

    /**
     * @return float
     */
    public function getParcelWeight()
    {
        return $this->parcelWeight;
    }

    /**
     * @param float $parcelWeight
     * @return Parcel
     */
    public function withParcelWeight($parcelWeight)
    {
        $new = clone $this;
        $new->parcelWeight = $parcelWeight;

        return $new;
    }

    /**
     * @return float
     */
    public function getParcelWidth()
    {
        return $this->parcelWidth;
    }

    /**
     * @param float $parcelWidth
     * @return Parcel
     */
    public function withParcelWidth($parcelWidth)
    {
        $new = clone $this;
        $new->parcelWidth = $parcelWidth;

        return $new;
    }

    /**
     * @return \Metapack\Type\Product
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param \Metapack\Type\Product $products
     * @return Parcel
     */
    public function withProducts($products)
    {
        $new = clone $this;
        $new->products = $products;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientReference()
    {
        return $this->recipientReference;
    }

    /**
     * @param string $recipientReference
     * @return Parcel
     */
    public function withRecipientReference($recipientReference)
    {
        $new = clone $this;
        $new->recipientReference = $recipientReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     * @return Parcel
     */
    public function withReference($reference)
    {
        $new = clone $this;
        $new->reference = $reference;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderReference()
    {
        return $this->senderReference;
    }

    /**
     * @param string $senderReference
     * @return Parcel
     */
    public function withSenderReference($senderReference)
    {
        $new = clone $this;
        $new->senderReference = $senderReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getTrackingCode()
    {
        return $this->trackingCode;
    }

    /**
     * @param string $trackingCode
     * @return Parcel
     */
    public function withTrackingCode($trackingCode)
    {
        $new = clone $this;
        $new->trackingCode = $trackingCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return $this->trackingUrl;
    }

    /**
     * @param string $trackingUrl
     * @return Parcel
     */
    public function withTrackingUrl($trackingUrl)
    {
        $new = clone $this;
        $new->trackingUrl = $trackingUrl;

        return $new;
    }


}

