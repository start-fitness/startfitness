<?php

namespace Metapack\Type;

class Property
{

    /**
     * @var string
     */
    private $propertyName = null;

    /**
     * @var string
     */
    private $propertyValue = null;

    /**
     * @return string
     */
    public function getPropertyName()
    {
        return $this->propertyName;
    }

    /**
     * @param string $propertyName
     * @return Property
     */
    public function withPropertyName($propertyName)
    {
        $new = clone $this;
        $new->propertyName = $propertyName;

        return $new;
    }

    /**
     * @return string
     */
    public function getPropertyValue()
    {
        return $this->propertyValue;
    }

    /**
     * @param string $propertyValue
     * @return Property
     */
    public function withPropertyValue($propertyValue)
    {
        $new = clone $this;
        $new->propertyValue = $propertyValue;

        return $new;
    }


}

