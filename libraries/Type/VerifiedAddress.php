<?php

namespace Metapack\Type;

class VerifiedAddress
{

    /**
     * @var bool
     */
    private $changedCompanyName = null;

    /**
     * @var bool
     */
    private $changedCountryCode = null;

    /**
     * @var bool
     */
    private $changedLine1 = null;

    /**
     * @var bool
     */
    private $changedLine2 = null;

    /**
     * @var bool
     */
    private $changedLine3 = null;

    /**
     * @var bool
     */
    private $changedLine4 = null;

    /**
     * @var bool
     */
    private $changedPostCode = null;

    /**
     * @var string
     */
    private $status = null;

    /**
     * @return bool
     */
    public function getChangedCompanyName()
    {
        return $this->changedCompanyName;
    }

    /**
     * @param bool $changedCompanyName
     * @return VerifiedAddress
     */
    public function withChangedCompanyName($changedCompanyName)
    {
        $new = clone $this;
        $new->changedCompanyName = $changedCompanyName;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedCountryCode()
    {
        return $this->changedCountryCode;
    }

    /**
     * @param bool $changedCountryCode
     * @return VerifiedAddress
     */
    public function withChangedCountryCode($changedCountryCode)
    {
        $new = clone $this;
        $new->changedCountryCode = $changedCountryCode;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedLine1()
    {
        return $this->changedLine1;
    }

    /**
     * @param bool $changedLine1
     * @return VerifiedAddress
     */
    public function withChangedLine1($changedLine1)
    {
        $new = clone $this;
        $new->changedLine1 = $changedLine1;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedLine2()
    {
        return $this->changedLine2;
    }

    /**
     * @param bool $changedLine2
     * @return VerifiedAddress
     */
    public function withChangedLine2($changedLine2)
    {
        $new = clone $this;
        $new->changedLine2 = $changedLine2;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedLine3()
    {
        return $this->changedLine3;
    }

    /**
     * @param bool $changedLine3
     * @return VerifiedAddress
     */
    public function withChangedLine3($changedLine3)
    {
        $new = clone $this;
        $new->changedLine3 = $changedLine3;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedLine4()
    {
        return $this->changedLine4;
    }

    /**
     * @param bool $changedLine4
     * @return VerifiedAddress
     */
    public function withChangedLine4($changedLine4)
    {
        $new = clone $this;
        $new->changedLine4 = $changedLine4;

        return $new;
    }

    /**
     * @return bool
     */
    public function getChangedPostCode()
    {
        return $this->changedPostCode;
    }

    /**
     * @param bool $changedPostCode
     * @return VerifiedAddress
     */
    public function withChangedPostCode($changedPostCode)
    {
        $new = clone $this;
        $new->changedPostCode = $changedPostCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return VerifiedAddress
     */
    public function withStatus($status)
    {
        $new = clone $this;
        $new->status = $status;

        return $new;
    }


}

