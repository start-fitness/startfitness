<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\ResultInterface;

class Labels implements ResultInterface
{

    /**
     * @var string
     */
    private $documentsPdf = null;

    /**
     * @var string
     */
    private $labelsPdf = null;

    /**
     * @return string
     */
    public function getDocumentsPdf()
    {
        return $this->documentsPdf;
    }

    /**
     * @param string $documentsPdf
     * @return Labels
     */
    public function withDocumentsPdf($documentsPdf)
    {
        $new = clone $this;
        $new->documentsPdf = $documentsPdf;

        return $new;
    }

    /**
     * @return string
     */
    public function getLabelsPdf()
    {
        return $this->labelsPdf;
    }

    /**
     * @param string $labelsPdf
     * @return Labels
     */
    public function withLabelsPdf($labelsPdf)
    {
        $new = clone $this;
        $new->labelsPdf = $labelsPdf;

        return $new;
    }


}

