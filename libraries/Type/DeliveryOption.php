<?php

namespace Metapack\Type;

class DeliveryOption
{

    /**
     * @var string
     */
    private $bookingCode;

    /**
     * @var string
     */
    private $carrierCode;

    /**
     * @var string
     */
    private $carrierCustom1;

    /**
     * @var string
     */
    private $carrierCustom2;

    /**
     * @var string
     */
    private $carrierCustom3;

    /**
     * @var string
     */
    private $carrierServiceCode;

    /**
     * @var string
     */
    private $carrierServiceTypeCode;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $collectionSlots;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $collectionWindow;

    /**
     * @var \DateTimeInterface
     */
    private $cutOffDateTime;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $deliverySlots;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $deliveryWindow;

    /**
     * @var string
     */
    private $groupCodes;

    /**
     * @var string
     */
    private $name;

    /**
     * @var bool
     */
    private $nominatableCollectionSlot;

    /**
     * @var bool
     */
    private $nominatableDeliverySlot;

    /**
     * @var string
     */
    private $recipientTimeZone;

    /**
     * @var float
     */
    private $score;

    /**
     * @var string
     */
    private $senderTimeZone;

    /**
     * @var float
     */
    private $shippingCharge;

    /**
     * @var float
     */
    private $shippingCost;

    /**
     * @var float
     */
    private $taxAndDuty;

    /**
     * @var string
     */
    private $taxAndDutyStatusText;

    /**
     * @var float
     */
    private $vatRate;

    /**
     * @return string
     */
    public function getBookingCode()
    {
        return $this->bookingCode;
    }

    /**
     * @param string $bookingCode
     * @return DeliveryOption
     */
    public function withBookingCode($bookingCode)
    {
        $new = clone $this;
        $new->bookingCode = $bookingCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierCode()
    {
        return $this->carrierCode;
    }

    /**
     * @param string $carrierCode
     * @return DeliveryOption
     */
    public function withCarrierCode($carrierCode)
    {
        $new = clone $this;
        $new->carrierCode = $carrierCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierCustom1()
    {
        return $this->carrierCustom1;
    }

    /**
     * @param string $carrierCustom1
     * @return DeliveryOption
     */
    public function withCarrierCustom1($carrierCustom1)
    {
        $new = clone $this;
        $new->carrierCustom1 = $carrierCustom1;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierCustom2()
    {
        return $this->carrierCustom2;
    }

    /**
     * @param string $carrierCustom2
     * @return DeliveryOption
     */
    public function withCarrierCustom2($carrierCustom2)
    {
        $new = clone $this;
        $new->carrierCustom2 = $carrierCustom2;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierCustom3()
    {
        return $this->carrierCustom3;
    }

    /**
     * @param string $carrierCustom3
     * @return DeliveryOption
     */
    public function withCarrierCustom3($carrierCustom3)
    {
        $new = clone $this;
        $new->carrierCustom3 = $carrierCustom3;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierServiceCode()
    {
        return $this->carrierServiceCode;
    }

    /**
     * @param string $carrierServiceCode
     * @return DeliveryOption
     */
    public function withCarrierServiceCode($carrierServiceCode)
    {
        $new = clone $this;
        $new->carrierServiceCode = $carrierServiceCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierServiceTypeCode()
    {
        return $this->carrierServiceTypeCode;
    }

    /**
     * @param string $carrierServiceTypeCode
     * @return DeliveryOption
     */
    public function withCarrierServiceTypeCode($carrierServiceTypeCode)
    {
        $new = clone $this;
        $new->carrierServiceTypeCode = $carrierServiceTypeCode;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getCollectionSlots()
    {
        return $this->collectionSlots;
    }

    /**
     * @param \Metapack\Type\DateRange $collectionSlots
     * @return DeliveryOption
     */
    public function withCollectionSlots($collectionSlots)
    {
        $new = clone $this;
        $new->collectionSlots = $collectionSlots;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getCollectionWindow()
    {
        return $this->collectionWindow;
    }

    /**
     * @param \Metapack\Type\DateRange $collectionWindow
     * @return DeliveryOption
     */
    public function withCollectionWindow($collectionWindow)
    {
        $new = clone $this;
        $new->collectionWindow = $collectionWindow;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCutOffDateTime()
    {
        return $this->cutOffDateTime;
    }

    /**
     * @param \DateTimeInterface $cutOffDateTime
     * @return DeliveryOption
     */
    public function withCutOffDateTime($cutOffDateTime)
    {
        $new = clone $this;
        $new->cutOffDateTime = $cutOffDateTime;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getDeliverySlots()
    {
        return $this->deliverySlots;
    }

    /**
     * @param \Metapack\Type\DateRange $deliverySlots
     * @return DeliveryOption
     */
    public function withDeliverySlots($deliverySlots)
    {
        $new = clone $this;
        $new->deliverySlots = $deliverySlots;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getDeliveryWindow()
    {
        return $this->deliveryWindow;
    }

    /**
     * @param \Metapack\Type\DateRange $deliveryWindow
     * @return DeliveryOption
     */
    public function withDeliveryWindow($deliveryWindow)
    {
        $new = clone $this;
        $new->deliveryWindow = $deliveryWindow;

        return $new;
    }

    /**
     * @return string
     */
    public function getGroupCodes()
    {
        return $this->groupCodes;
    }

    /**
     * @param string $groupCodes
     * @return DeliveryOption
     */
    public function withGroupCodes($groupCodes)
    {
        $new = clone $this;
        $new->groupCodes = $groupCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return DeliveryOption
     */
    public function withName($name)
    {
        $new = clone $this;
        $new->name = $name;

        return $new;
    }

    /**
     * @return bool
     */
    public function getNominatableCollectionSlot()
    {
        return $this->nominatableCollectionSlot;
    }

    /**
     * @param bool $nominatableCollectionSlot
     * @return DeliveryOption
     */
    public function withNominatableCollectionSlot($nominatableCollectionSlot)
    {
        $new = clone $this;
        $new->nominatableCollectionSlot = $nominatableCollectionSlot;

        return $new;
    }

    /**
     * @return bool
     */
    public function getNominatableDeliverySlot()
    {
        return $this->nominatableDeliverySlot;
    }

    /**
     * @param bool $nominatableDeliverySlot
     * @return DeliveryOption
     */
    public function withNominatableDeliverySlot($nominatableDeliverySlot)
    {
        $new = clone $this;
        $new->nominatableDeliverySlot = $nominatableDeliverySlot;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientTimeZone()
    {
        return $this->recipientTimeZone;
    }

    /**
     * @param string $recipientTimeZone
     * @return DeliveryOption
     */
    public function withRecipientTimeZone($recipientTimeZone)
    {
        $new = clone $this;
        $new->recipientTimeZone = $recipientTimeZone;

        return $new;
    }

    /**
     * @return float
     */
    public function getScore()
    {
        return $this->score;
    }

    /**
     * @param float $score
     * @return DeliveryOption
     */
    public function withScore($score)
    {
        $new = clone $this;
        $new->score = $score;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderTimeZone()
    {
        return $this->senderTimeZone;
    }

    /**
     * @param string $senderTimeZone
     * @return DeliveryOption
     */
    public function withSenderTimeZone($senderTimeZone)
    {
        $new = clone $this;
        $new->senderTimeZone = $senderTimeZone;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }

    /**
     * @param float $shippingCharge
     * @return DeliveryOption
     */
    public function withShippingCharge($shippingCharge)
    {
        $new = clone $this;
        $new->shippingCharge = $shippingCharge;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param float $shippingCost
     * @return DeliveryOption
     */
    public function withShippingCost($shippingCost)
    {
        $new = clone $this;
        $new->shippingCost = $shippingCost;

        return $new;
    }

    /**
     * @return float
     */
    public function getTaxAndDuty()
    {
        return $this->taxAndDuty;
    }

    /**
     * @param float $taxAndDuty
     * @return DeliveryOption
     */
    public function withTaxAndDuty($taxAndDuty)
    {
        $new = clone $this;
        $new->taxAndDuty = $taxAndDuty;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxAndDutyStatusText()
    {
        return $this->taxAndDutyStatusText;
    }

    /**
     * @param string $taxAndDutyStatusText
     * @return DeliveryOption
     */
    public function withTaxAndDutyStatusText($taxAndDutyStatusText)
    {
        $new = clone $this;
        $new->taxAndDutyStatusText = $taxAndDutyStatusText;

        return $new;
    }

    /**
     * @return float
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @param float $vatRate
     * @return DeliveryOption
     */
    public function withVatRate($vatRate)
    {
        $new = clone $this;
        $new->vatRate = $vatRate;

        return $new;
    }


}

