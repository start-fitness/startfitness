<?php

namespace Metapack\Type;


use Phpro\SoapClient\Type\RequestInterface;

class Address implements RequestInterface
{

    /**
     * @var string
     */
    private $companyName = null;

    /**
     * @var string
     */
    private $countryCode = null;

    /**
     * @var string
     */
    private $line1 = null;

    /**
     * @var string
     */
    private $line2 = null;

    /**
     * @var string
     */
    private $line3 = null;

    /**
     * @var string
     */
    private $line4 = null;

    /**
     * @var string
     */
    private $postCode = null;

    /**
     * @var string
     */
    private $type = null;

    /**
     * Constructor
     *
     * @var string $companyName
     * @var string $countryCode
     * @var string $line1
     * @var string $line2
     * @var string $line3
     * @var string $line4
     * @var string $postCode
     * @var string $type
     */
    public function __construct($companyName = null, $countryCode = null, $line1 = null, $line2 = null, $line3 = null, $line4 = null, $postCode = null, $type = null)
    {
        $this->companyName = $companyName;
        $this->countryCode = $countryCode;
        $this->line1 = $line1;
        $this->line2 = $line2;
        $this->line3 = $line3;
        $this->line4 = $line4;
        $this->postCode = $postCode;
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param string $companyName
     * @return Address
     */
    public function withCompanyName($companyName)
    {
        $new = clone $this;
        $new->companyName = $companyName;

        return $new;
    }

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * @param string $countryCode
     * @return Address
     */
    public function withCountryCode($countryCode)
    {
        $new = clone $this;
        $new->countryCode = $countryCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getLine1()
    {
        return $this->line1;
    }

    /**
     * @param string $line1
     * @return Address
     */
    public function withLine1($line1)
    {
        $new = clone $this;
        $new->line1 = $line1;

        return $new;
    }

    /**
     * @return string
     */
    public function getLine2()
    {
        return $this->line2;
    }

    /**
     * @param string $line2
     * @return Address
     */
    public function withLine2($line2)
    {
        $new = clone $this;
        $new->line2 = $line2;

        return $new;
    }

    /**
     * @return string
     */
    public function getLine3()
    {
        return $this->line3;
    }

    /**
     * @param string $line3
     * @return Address
     */
    public function withLine3($line3)
    {
        $new = clone $this;
        $new->line3 = $line3;

        return $new;
    }

    /**
     * @return string
     */
    public function getLine4()
    {
        return $this->line4;
    }

    /**
     * @param string $line4
     * @return Address
     */
    public function withLine4($line4)
    {
        $new = clone $this;
        $new->line4 = $line4;

        return $new;
    }

    /**
     * @return string
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param string $postCode
     * @return Address
     */
    public function withPostCode($postCode)
    {
        $new = clone $this;
        $new->postCode = $postCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Address
     */
    public function withType($type)
    {
        $new = clone $this;
        $new->type = $type;

        return $new;
    }


}

