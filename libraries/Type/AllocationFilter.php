<?php

namespace Metapack\Type;

class AllocationFilter
{

    /**
     * @var string
     */
    private $acceptableCarrierCodes;

    /**
     * @var string
     */
    private $acceptableCarrierServiceCodes;

    /**
     * @var string
     */
    private $acceptableCarrierServiceGroupCodes;

    /**
     * @var string
     */
    private $acceptableCarrierServiceTypeCodes;

    /**
     * @var \Metapack\Type\WorkingDays
     */
    private $acceptableCollectionDays;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $acceptableCollectionSlots;

    /**
     * @var \Metapack\Type\WorkingDays
     */
    private $acceptableDeliveryDays;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $acceptableDeliverySlots;

    /**
     * @var string
     */
    private $allocationSchemeCode;

    /**
     * @var bool
     */
    private $expandGroups;

    /**
     * @var int
     */
    private $filterGroup1;

    /**
     * @var int
     */
    private $filterGroup2;

    /**
     * @var int
     */
    private $filterGroup3;

    /**
     * @var bool
     */
    private $firstCollectionOnly;

    /**
     * @var int
     */
    private $maxAnalysisDayCount;

    /**
     * @var float
     */
    private $maxCost;

    /**
     * @var int
     */
    private $maxDatesPerService;

    /**
     * @var float
     */
    private $maxScore;

    /**
     * @var float
     */
    private $minScore;

    /**
     * @var int
     */
    private $preFilterSortOrder1;

    /**
     * @var int
     */
    private $preFilterSortOrder2;

    /**
     * @var int
     */
    private $preFilterSortOrder3;

    /**
     * @var int
     */
    private $sortOrder1;

    /**
     * @var int
     */
    private $sortOrder2;

    /**
     * @var int
     */
    private $sortOrder3;

    /**
     * @var string
     */
    private $unacceptableCarrierCodes;

    /**
     * @var string
     */
    private $unacceptableCarrierServiceCodes;

    /**
     * @var string
     */
    private $unacceptableCarrierServiceGroupCodes;

    /**
     * @var string
     */
    private $unacceptableCarrierServiceTypeCodes;

    /**
     * @var \Metapack\Type\WorkingDays
     */
    private $unacceptableCollectionDays;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $unacceptableCollectionSlots;

    /**
     * @var \Metapack\Type\WorkingDays
     */
    private $unacceptableDeliveryDays;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $unacceptableDeliverySlots;

    /**
     * @return string
     */
    public function getAcceptableCarrierCodes()
    {
        return $this->acceptableCarrierCodes;
    }

    /**
     * @param string $acceptableCarrierCodes
     * @return AllocationFilter
     */
    public function withAcceptableCarrierCodes($acceptableCarrierCodes)
    {
        $new = clone $this;
        $new->acceptableCarrierCodes = $acceptableCarrierCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getAcceptableCarrierServiceCodes()
    {
        return $this->acceptableCarrierServiceCodes;
    }

    /**
     * @param string $acceptableCarrierServiceCodes
     * @return AllocationFilter
     */
    public function withAcceptableCarrierServiceCodes($acceptableCarrierServiceCodes)
    {
        $new = clone $this;
        $new->acceptableCarrierServiceCodes = $acceptableCarrierServiceCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getAcceptableCarrierServiceGroupCodes()
    {
        return $this->acceptableCarrierServiceGroupCodes;
    }

    /**
     * @param string $acceptableCarrierServiceGroupCodes
     * @return AllocationFilter
     */
    public function withAcceptableCarrierServiceGroupCodes($acceptableCarrierServiceGroupCodes)
    {
        $new = clone $this;
        $new->acceptableCarrierServiceGroupCodes = $acceptableCarrierServiceGroupCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getAcceptableCarrierServiceTypeCodes()
    {
        return $this->acceptableCarrierServiceTypeCodes;
    }

    /**
     * @param string $acceptableCarrierServiceTypeCodes
     * @return AllocationFilter
     */
    public function withAcceptableCarrierServiceTypeCodes($acceptableCarrierServiceTypeCodes)
    {
        $new = clone $this;
        $new->acceptableCarrierServiceTypeCodes = $acceptableCarrierServiceTypeCodes;

        return $new;
    }

    /**
     * @return \Metapack\Type\WorkingDays
     */
    public function getAcceptableCollectionDays()
    {
        return $this->acceptableCollectionDays;
    }

    /**
     * @param \Metapack\Type\WorkingDays $acceptableCollectionDays
     * @return AllocationFilter
     */
    public function withAcceptableCollectionDays($acceptableCollectionDays)
    {
        $new = clone $this;
        $new->acceptableCollectionDays = $acceptableCollectionDays;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getAcceptableCollectionSlots()
    {
        return $this->acceptableCollectionSlots;
    }

    /**
     * @param \Metapack\Type\DateRange $acceptableCollectionSlots
     * @return AllocationFilter
     */
    public function withAcceptableCollectionSlots($acceptableCollectionSlots)
    {
        $new = clone $this;
        $new->acceptableCollectionSlots = $acceptableCollectionSlots;

        return $new;
    }

    /**
     * @return \Metapack\Type\WorkingDays
     */
    public function getAcceptableDeliveryDays()
    {
        return $this->acceptableDeliveryDays;
    }

    /**
     * @param \Metapack\Type\WorkingDays $acceptableDeliveryDays
     * @return AllocationFilter
     */
    public function withAcceptableDeliveryDays($acceptableDeliveryDays)
    {
        $new = clone $this;
        $new->acceptableDeliveryDays = $acceptableDeliveryDays;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getAcceptableDeliverySlots()
    {
        return $this->acceptableDeliverySlots;
    }

    /**
     * @param \Metapack\Type\DateRange $acceptableDeliverySlots
     * @return AllocationFilter
     */
    public function withAcceptableDeliverySlots($acceptableDeliverySlots)
    {
        $new = clone $this;
        $new->acceptableDeliverySlots = $acceptableDeliverySlots;

        return $new;
    }

    /**
     * @return string
     */
    public function getAllocationSchemeCode()
    {
        return $this->allocationSchemeCode;
    }

    /**
     * @param string $allocationSchemeCode
     * @return AllocationFilter
     */
    public function withAllocationSchemeCode($allocationSchemeCode)
    {
        $new = clone $this;
        $new->allocationSchemeCode = $allocationSchemeCode;

        return $new;
    }

    /**
     * @return bool
     */
    public function getExpandGroups()
    {
        return $this->expandGroups;
    }

    /**
     * @param bool $expandGroups
     * @return AllocationFilter
     */
    public function withExpandGroups($expandGroups)
    {
        $new = clone $this;
        $new->expandGroups = $expandGroups;

        return $new;
    }

    /**
     * @return int
     */
    public function getFilterGroup1()
    {
        return $this->filterGroup1;
    }

    /**
     * @param int $filterGroup1
     * @return AllocationFilter
     */
    public function withFilterGroup1($filterGroup1)
    {
        $new = clone $this;
        $new->filterGroup1 = $filterGroup1;

        return $new;
    }

    /**
     * @return int
     */
    public function getFilterGroup2()
    {
        return $this->filterGroup2;
    }

    /**
     * @param int $filterGroup2
     * @return AllocationFilter
     */
    public function withFilterGroup2($filterGroup2)
    {
        $new = clone $this;
        $new->filterGroup2 = $filterGroup2;

        return $new;
    }

    /**
     * @return int
     */
    public function getFilterGroup3()
    {
        return $this->filterGroup3;
    }

    /**
     * @param int $filterGroup3
     * @return AllocationFilter
     */
    public function withFilterGroup3($filterGroup3)
    {
        $new = clone $this;
        $new->filterGroup3 = $filterGroup3;

        return $new;
    }

    /**
     * @return bool
     */
    public function getFirstCollectionOnly()
    {
        return $this->firstCollectionOnly;
    }

    /**
     * @param bool $firstCollectionOnly
     * @return AllocationFilter
     */
    public function withFirstCollectionOnly($firstCollectionOnly)
    {
        $new = clone $this;
        $new->firstCollectionOnly = $firstCollectionOnly;

        return $new;
    }

    /**
     * @return int
     */
    public function getMaxAnalysisDayCount()
    {
        return $this->maxAnalysisDayCount;
    }

    /**
     * @param int $maxAnalysisDayCount
     * @return AllocationFilter
     */
    public function withMaxAnalysisDayCount($maxAnalysisDayCount)
    {
        $new = clone $this;
        $new->maxAnalysisDayCount = $maxAnalysisDayCount;

        return $new;
    }

    /**
     * @return float
     */
    public function getMaxCost()
    {
        return $this->maxCost;
    }

    /**
     * @param float $maxCost
     * @return AllocationFilter
     */
    public function withMaxCost($maxCost)
    {
        $new = clone $this;
        $new->maxCost = $maxCost;

        return $new;
    }

    /**
     * @return int
     */
    public function getMaxDatesPerService()
    {
        return $this->maxDatesPerService;
    }

    /**
     * @param int $maxDatesPerService
     * @return AllocationFilter
     */
    public function withMaxDatesPerService($maxDatesPerService)
    {
        $new = clone $this;
        $new->maxDatesPerService = $maxDatesPerService;

        return $new;
    }

    /**
     * @return float
     */
    public function getMaxScore()
    {
        return $this->maxScore;
    }

    /**
     * @param float $maxScore
     * @return AllocationFilter
     */
    public function withMaxScore($maxScore)
    {
        $new = clone $this;
        $new->maxScore = $maxScore;

        return $new;
    }

    /**
     * @return float
     */
    public function getMinScore()
    {
        return $this->minScore;
    }

    /**
     * @param float $minScore
     * @return AllocationFilter
     */
    public function withMinScore($minScore)
    {
        $new = clone $this;
        $new->minScore = $minScore;

        return $new;
    }

    /**
     * @return int
     */
    public function getPreFilterSortOrder1()
    {
        return $this->preFilterSortOrder1;
    }

    /**
     * @param int $preFilterSortOrder1
     * @return AllocationFilter
     */
    public function withPreFilterSortOrder1($preFilterSortOrder1)
    {
        $new = clone $this;
        $new->preFilterSortOrder1 = $preFilterSortOrder1;

        return $new;
    }

    /**
     * @return int
     */
    public function getPreFilterSortOrder2()
    {
        return $this->preFilterSortOrder2;
    }

    /**
     * @param int $preFilterSortOrder2
     * @return AllocationFilter
     */
    public function withPreFilterSortOrder2($preFilterSortOrder2)
    {
        $new = clone $this;
        $new->preFilterSortOrder2 = $preFilterSortOrder2;

        return $new;
    }

    /**
     * @return int
     */
    public function getPreFilterSortOrder3()
    {
        return $this->preFilterSortOrder3;
    }

    /**
     * @param int $preFilterSortOrder3
     * @return AllocationFilter
     */
    public function withPreFilterSortOrder3($preFilterSortOrder3)
    {
        $new = clone $this;
        $new->preFilterSortOrder3 = $preFilterSortOrder3;

        return $new;
    }

    /**
     * @return int
     */
    public function getSortOrder1()
    {
        return $this->sortOrder1;
    }

    /**
     * @param int $sortOrder1
     * @return AllocationFilter
     */
    public function withSortOrder1($sortOrder1)
    {
        $new = clone $this;
        $new->sortOrder1 = $sortOrder1;

        return $new;
    }

    /**
     * @return int
     */
    public function getSortOrder2()
    {
        return $this->sortOrder2;
    }

    /**
     * @param int $sortOrder2
     * @return AllocationFilter
     */
    public function withSortOrder2($sortOrder2)
    {
        $new = clone $this;
        $new->sortOrder2 = $sortOrder2;

        return $new;
    }

    /**
     * @return int
     */
    public function getSortOrder3()
    {
        return $this->sortOrder3;
    }

    /**
     * @param int $sortOrder3
     * @return AllocationFilter
     */
    public function withSortOrder3($sortOrder3)
    {
        $new = clone $this;
        $new->sortOrder3 = $sortOrder3;

        return $new;
    }

    /**
     * @return string
     */
    public function getUnacceptableCarrierCodes()
    {
        return $this->unacceptableCarrierCodes;
    }

    /**
     * @param string $unacceptableCarrierCodes
     * @return AllocationFilter
     */
    public function withUnacceptableCarrierCodes($unacceptableCarrierCodes)
    {
        $new = clone $this;
        $new->unacceptableCarrierCodes = $unacceptableCarrierCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getUnacceptableCarrierServiceCodes()
    {
        return $this->unacceptableCarrierServiceCodes;
    }

    /**
     * @param string $unacceptableCarrierServiceCodes
     * @return AllocationFilter
     */
    public function withUnacceptableCarrierServiceCodes($unacceptableCarrierServiceCodes)
    {
        $new = clone $this;
        $new->unacceptableCarrierServiceCodes = $unacceptableCarrierServiceCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getUnacceptableCarrierServiceGroupCodes()
    {
        return $this->unacceptableCarrierServiceGroupCodes;
    }

    /**
     * @param string $unacceptableCarrierServiceGroupCodes
     * @return AllocationFilter
     */
    public function withUnacceptableCarrierServiceGroupCodes($unacceptableCarrierServiceGroupCodes)
    {
        $new = clone $this;
        $new->unacceptableCarrierServiceGroupCodes = $unacceptableCarrierServiceGroupCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getUnacceptableCarrierServiceTypeCodes()
    {
        return $this->unacceptableCarrierServiceTypeCodes;
    }

    /**
     * @param string $unacceptableCarrierServiceTypeCodes
     * @return AllocationFilter
     */
    public function withUnacceptableCarrierServiceTypeCodes($unacceptableCarrierServiceTypeCodes)
    {
        $new = clone $this;
        $new->unacceptableCarrierServiceTypeCodes = $unacceptableCarrierServiceTypeCodes;

        return $new;
    }

    /**
     * @return \Metapack\Type\WorkingDays
     */
    public function getUnacceptableCollectionDays()
    {
        return $this->unacceptableCollectionDays;
    }

    /**
     * @param \Metapack\Type\WorkingDays $unacceptableCollectionDays
     * @return AllocationFilter
     */
    public function withUnacceptableCollectionDays($unacceptableCollectionDays)
    {
        $new = clone $this;
        $new->unacceptableCollectionDays = $unacceptableCollectionDays;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getUnacceptableCollectionSlots()
    {
        return $this->unacceptableCollectionSlots;
    }

    /**
     * @param \Metapack\Type\DateRange $unacceptableCollectionSlots
     * @return AllocationFilter
     */
    public function withUnacceptableCollectionSlots($unacceptableCollectionSlots)
    {
        $new = clone $this;
        $new->unacceptableCollectionSlots = $unacceptableCollectionSlots;

        return $new;
    }

    /**
     * @return \Metapack\Type\WorkingDays
     */
    public function getUnacceptableDeliveryDays()
    {
        return $this->unacceptableDeliveryDays;
    }

    /**
     * @param \Metapack\Type\WorkingDays $unacceptableDeliveryDays
     * @return AllocationFilter
     */
    public function withUnacceptableDeliveryDays($unacceptableDeliveryDays)
    {
        $new = clone $this;
        $new->unacceptableDeliveryDays = $unacceptableDeliveryDays;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getUnacceptableDeliverySlots()
    {
        return $this->unacceptableDeliverySlots;
    }

    /**
     * @param \Metapack\Type\DateRange $unacceptableDeliverySlots
     * @return AllocationFilter
     */
    public function withUnacceptableDeliverySlots($unacceptableDeliverySlots)
    {
        $new = clone $this;
        $new->unacceptableDeliverySlots = $unacceptableDeliverySlots;

        return $new;
    }


}

