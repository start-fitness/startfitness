<?php

namespace Metapack\Type;

class AuditRecord
{

    /**
     * @var \DateTimeInterface
     */
    private $date;

    /**
     * @var string
     */
    private $text;

    /**
     * @var string
     */
    private $userId;

    /**
     * @return \DateTimeInterface
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTimeInterface $date
     * @return AuditRecord
     */
    public function withDate($date)
    {
        $new = clone $this;
        $new->date = $date;

        return $new;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return AuditRecord
     */
    public function withText($text)
    {
        $new = clone $this;
        $new->text = $text;

        return $new;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return AuditRecord
     */
    public function withUserId($userId)
    {
        $new = clone $this;
        $new->userId = $userId;

        return $new;
    }


}

