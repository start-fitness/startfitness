<?php

namespace Metapack\Type;

class SearchOrderBy
{

    /**
     * @var bool
     */
    private $descending;

    /**
     * @var string
     */
    private $field;

    /**
     * @return bool
     */
    public function getDescending()
    {
        return $this->descending;
    }

    /**
     * @param bool $descending
     * @return SearchOrderBy
     */
    public function withDescending($descending)
    {
        $new = clone $this;
        $new->descending = $descending;

        return $new;
    }

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return SearchOrderBy
     */
    public function withField($field)
    {
        $new = clone $this;
        $new->field = $field;

        return $new;
    }


}

