<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\RequestInterface;

class SearchParams implements RequestInterface
{

    /**
     * @var \Metapack\Type\SearchCondition
     */
    private $conditions;

    /**
     * @var bool
     */
    private $distinct;

    /**
     * @var \Metapack\Type\SearchOrderBy
     */
    private $orderByFields;

    /**
     * @var int
     */
    private $pageNumber;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var string
     */
    private $selectFields;

    /**
     * Constructor
     *
     * @var \Metapack\Type\SearchCondition $conditions
     * @var bool $distinct
     * @var \Metapack\Type\SearchOrderBy $orderByFields
     * @var int $pageNumber
     * @var int $pageSize
     * @var string $selectFields
     */
    public function __construct($conditions, $distinct, $orderByFields, $pageNumber, $pageSize, $selectFields)
    {
        $this->conditions = $conditions;
        $this->distinct = $distinct;
        $this->orderByFields = $orderByFields;
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->selectFields = $selectFields;
    }

    /**
     * @return \Metapack\Type\SearchCondition
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * @param \Metapack\Type\SearchCondition $conditions
     * @return SearchParams
     */
    public function withConditions($conditions)
    {
        $new = clone $this;
        $new->conditions = $conditions;

        return $new;
    }

    /**
     * @return bool
     */
    public function getDistinct()
    {
        return $this->distinct;
    }

    /**
     * @param bool $distinct
     * @return SearchParams
     */
    public function withDistinct($distinct)
    {
        $new = clone $this;
        $new->distinct = $distinct;

        return $new;
    }

    /**
     * @return \Metapack\Type\SearchOrderBy
     */
    public function getOrderByFields()
    {
        return $this->orderByFields;
    }

    /**
     * @param \Metapack\Type\SearchOrderBy $orderByFields
     * @return SearchParams
     */
    public function withOrderByFields($orderByFields)
    {
        $new = clone $this;
        $new->orderByFields = $orderByFields;

        return $new;
    }

    /**
     * @return int
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     * @return SearchParams
     */
    public function withPageNumber($pageNumber)
    {
        $new = clone $this;
        $new->pageNumber = $pageNumber;

        return $new;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return SearchParams
     */
    public function withPageSize($pageSize)
    {
        $new = clone $this;
        $new->pageSize = $pageSize;

        return $new;
    }

    /**
     * @return string
     */
    public function getSelectFields()
    {
        return $this->selectFields;
    }

    /**
     * @param string $selectFields
     * @return SearchParams
     */
    public function withSelectFields($selectFields)
    {
        $new = clone $this;
        $new->selectFields = $selectFields;

        return $new;
    }


}

