<?php

namespace Metapack\Type;

class SearchCondition
{

    /**
     * @var string
     */
    private $field;

    /**
     * @var string
     */
    private $operand;

    /**
     * @var string
     */
    private $operator;

    /**
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param string $field
     * @return SearchCondition
     */
    public function withField($field)
    {
        $new = clone $this;
        $new->field = $field;

        return $new;
    }

    /**
     * @return string
     */
    public function getOperand()
    {
        return $this->operand;
    }

    /**
     * @param string $operand
     * @return SearchCondition
     */
    public function withOperand($operand)
    {
        $new = clone $this;
        $new->operand = $operand;

        return $new;
    }

    /**
     * @return string
     */
    public function getOperator()
    {
        return $this->operator;
    }

    /**
     * @param string $operator
     * @return SearchCondition
     */
    public function withOperator($operator)
    {
        $new = clone $this;
        $new->operator = $operator;

        return $new;
    }


}

