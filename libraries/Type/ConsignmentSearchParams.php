<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\RequestInterface;

class ConsignmentSearchParams implements RequestInterface
{

    /**
     * @var string
     */
    private $carrierCodes;

    /**
     * @var string
     */
    private $consignmentStatuses;

    /**
     * @var \DateTimeInterface
     */
    private $dateAllocatedAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateAllocatedBefore;

    /**
     * @var \DateTimeInterface
     */
    private $dateCompletedAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateCompletedBefore;

    /**
     * @var \DateTimeInterface
     */
    private $dateCreatedAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateCreatedBefore;

    /**
     * @var \DateTimeInterface
     */
    private $dateDeliveryAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateDeliveryBefore;

    /**
     * @var \DateTimeInterface
     */
    private $dateDespatchAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateDespatchBefore;

    /**
     * @var \DateTimeInterface
     */
    private $dateModifiedAfter;

    /**
     * @var \DateTimeInterface
     */
    private $dateModifiedBefore;

    /**
     * @var int
     */
    private $pageNumber;

    /**
     * @var int
     */
    private $pageSize;

    /**
     * @var string
     */
    private $transactionType;

    /**
     * Constructor
     *
     * @var string $carrierCodes
     * @var string $consignmentStatuses
     * @var \DateTimeInterface $dateAllocatedAfter
     * @var \DateTimeInterface $dateAllocatedBefore
     * @var \DateTimeInterface $dateCompletedAfter
     * @var \DateTimeInterface $dateCompletedBefore
     * @var \DateTimeInterface $dateCreatedAfter
     * @var \DateTimeInterface $dateCreatedBefore
     * @var \DateTimeInterface $dateDeliveryAfter
     * @var \DateTimeInterface $dateDeliveryBefore
     * @var \DateTimeInterface $dateDespatchAfter
     * @var \DateTimeInterface $dateDespatchBefore
     * @var \DateTimeInterface $dateModifiedAfter
     * @var \DateTimeInterface $dateModifiedBefore
     * @var int $pageNumber
     * @var int $pageSize
     * @var string $transactionType
     */
    public function __construct($carrierCodes, $consignmentStatuses, $dateAllocatedAfter, $dateAllocatedBefore, $dateCompletedAfter, $dateCompletedBefore, $dateCreatedAfter, $dateCreatedBefore, $dateDeliveryAfter, $dateDeliveryBefore, $dateDespatchAfter, $dateDespatchBefore, $dateModifiedAfter, $dateModifiedBefore, $pageNumber, $pageSize, $transactionType)
    {
        $this->carrierCodes = $carrierCodes;
        $this->consignmentStatuses = $consignmentStatuses;
        $this->dateAllocatedAfter = $dateAllocatedAfter;
        $this->dateAllocatedBefore = $dateAllocatedBefore;
        $this->dateCompletedAfter = $dateCompletedAfter;
        $this->dateCompletedBefore = $dateCompletedBefore;
        $this->dateCreatedAfter = $dateCreatedAfter;
        $this->dateCreatedBefore = $dateCreatedBefore;
        $this->dateDeliveryAfter = $dateDeliveryAfter;
        $this->dateDeliveryBefore = $dateDeliveryBefore;
        $this->dateDespatchAfter = $dateDespatchAfter;
        $this->dateDespatchBefore = $dateDespatchBefore;
        $this->dateModifiedAfter = $dateModifiedAfter;
        $this->dateModifiedBefore = $dateModifiedBefore;
        $this->pageNumber = $pageNumber;
        $this->pageSize = $pageSize;
        $this->transactionType = $transactionType;
    }

    /**
     * @return string
     */
    public function getCarrierCodes()
    {
        return $this->carrierCodes;
    }

    /**
     * @param string $carrierCodes
     * @return ConsignmentSearchParams
     */
    public function withCarrierCodes($carrierCodes)
    {
        $new = clone $this;
        $new->carrierCodes = $carrierCodes;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsignmentStatuses()
    {
        return $this->consignmentStatuses;
    }

    /**
     * @param string $consignmentStatuses
     * @return ConsignmentSearchParams
     */
    public function withConsignmentStatuses($consignmentStatuses)
    {
        $new = clone $this;
        $new->consignmentStatuses = $consignmentStatuses;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateAllocatedAfter()
    {
        return $this->dateAllocatedAfter;
    }

    /**
     * @param \DateTimeInterface $dateAllocatedAfter
     * @return ConsignmentSearchParams
     */
    public function withDateAllocatedAfter($dateAllocatedAfter)
    {
        $new = clone $this;
        $new->dateAllocatedAfter = $dateAllocatedAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateAllocatedBefore()
    {
        return $this->dateAllocatedBefore;
    }

    /**
     * @param \DateTimeInterface $dateAllocatedBefore
     * @return ConsignmentSearchParams
     */
    public function withDateAllocatedBefore($dateAllocatedBefore)
    {
        $new = clone $this;
        $new->dateAllocatedBefore = $dateAllocatedBefore;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCompletedAfter()
    {
        return $this->dateCompletedAfter;
    }

    /**
     * @param \DateTimeInterface $dateCompletedAfter
     * @return ConsignmentSearchParams
     */
    public function withDateCompletedAfter($dateCompletedAfter)
    {
        $new = clone $this;
        $new->dateCompletedAfter = $dateCompletedAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCompletedBefore()
    {
        return $this->dateCompletedBefore;
    }

    /**
     * @param \DateTimeInterface $dateCompletedBefore
     * @return ConsignmentSearchParams
     */
    public function withDateCompletedBefore($dateCompletedBefore)
    {
        $new = clone $this;
        $new->dateCompletedBefore = $dateCompletedBefore;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCreatedAfter()
    {
        return $this->dateCreatedAfter;
    }

    /**
     * @param \DateTimeInterface $dateCreatedAfter
     * @return ConsignmentSearchParams
     */
    public function withDateCreatedAfter($dateCreatedAfter)
    {
        $new = clone $this;
        $new->dateCreatedAfter = $dateCreatedAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateCreatedBefore()
    {
        return $this->dateCreatedBefore;
    }

    /**
     * @param \DateTimeInterface $dateCreatedBefore
     * @return ConsignmentSearchParams
     */
    public function withDateCreatedBefore($dateCreatedBefore)
    {
        $new = clone $this;
        $new->dateCreatedBefore = $dateCreatedBefore;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateDeliveryAfter()
    {
        return $this->dateDeliveryAfter;
    }

    /**
     * @param \DateTimeInterface $dateDeliveryAfter
     * @return ConsignmentSearchParams
     */
    public function withDateDeliveryAfter($dateDeliveryAfter)
    {
        $new = clone $this;
        $new->dateDeliveryAfter = $dateDeliveryAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateDeliveryBefore()
    {
        return $this->dateDeliveryBefore;
    }

    /**
     * @param \DateTimeInterface $dateDeliveryBefore
     * @return ConsignmentSearchParams
     */
    public function withDateDeliveryBefore($dateDeliveryBefore)
    {
        $new = clone $this;
        $new->dateDeliveryBefore = $dateDeliveryBefore;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateDespatchAfter()
    {
        return $this->dateDespatchAfter;
    }

    /**
     * @param \DateTimeInterface $dateDespatchAfter
     * @return ConsignmentSearchParams
     */
    public function withDateDespatchAfter($dateDespatchAfter)
    {
        $new = clone $this;
        $new->dateDespatchAfter = $dateDespatchAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateDespatchBefore()
    {
        return $this->dateDespatchBefore;
    }

    /**
     * @param \DateTimeInterface $dateDespatchBefore
     * @return ConsignmentSearchParams
     */
    public function withDateDespatchBefore($dateDespatchBefore)
    {
        $new = clone $this;
        $new->dateDespatchBefore = $dateDespatchBefore;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateModifiedAfter()
    {
        return $this->dateModifiedAfter;
    }

    /**
     * @param \DateTimeInterface $dateModifiedAfter
     * @return ConsignmentSearchParams
     */
    public function withDateModifiedAfter($dateModifiedAfter)
    {
        $new = clone $this;
        $new->dateModifiedAfter = $dateModifiedAfter;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDateModifiedBefore()
    {
        return $this->dateModifiedBefore;
    }

    /**
     * @param \DateTimeInterface $dateModifiedBefore
     * @return ConsignmentSearchParams
     */
    public function withDateModifiedBefore($dateModifiedBefore)
    {
        $new = clone $this;
        $new->dateModifiedBefore = $dateModifiedBefore;

        return $new;
    }

    /**
     * @return int
     */
    public function getPageNumber()
    {
        return $this->pageNumber;
    }

    /**
     * @param int $pageNumber
     * @return ConsignmentSearchParams
     */
    public function withPageNumber($pageNumber)
    {
        $new = clone $this;
        $new->pageNumber = $pageNumber;

        return $new;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     * @return ConsignmentSearchParams
     */
    public function withPageSize($pageSize)
    {
        $new = clone $this;
        $new->pageSize = $pageSize;

        return $new;
    }

    /**
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return ConsignmentSearchParams
     */
    public function withTransactionType($transactionType)
    {
        $new = clone $this;
        $new->transactionType = $transactionType;

        return $new;
    }


}

