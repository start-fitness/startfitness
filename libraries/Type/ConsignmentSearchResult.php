<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\ResultInterface;

class ConsignmentSearchResult implements ResultInterface
{

    /**
     * @var \Metapack\Type\Consignment
     */
    private $consignments;

    /**
     * @var int
     */
    private $pageCount;

    /**
     * @var int
     */
    private $totalCount;

    /**
     * @return \Metapack\Type\Consignment
     */
    public function getConsignments()
    {
        return $this->consignments;
    }

    /**
     * @param \Metapack\Type\Consignment $consignments
     * @return ConsignmentSearchResult
     */
    public function withConsignments($consignments)
    {
        $new = clone $this;
        $new->consignments = $consignments;

        return $new;
    }

    /**
     * @return int
     */
    public function getPageCount()
    {
        return $this->pageCount;
    }

    /**
     * @param int $pageCount
     * @return ConsignmentSearchResult
     */
    public function withPageCount($pageCount)
    {
        $new = clone $this;
        $new->pageCount = $pageCount;

        return $new;
    }

    /**
     * @return int
     */
    public function getTotalCount()
    {
        return $this->totalCount;
    }

    /**
     * @param int $totalCount
     * @return ConsignmentSearchResult
     */
    public function withTotalCount($totalCount)
    {
        $new = clone $this;
        $new->totalCount = $totalCount;

        return $new;
    }


}

