<?php

namespace Metapack\Type;

use Phpro\SoapClient\Type\ResultInterface;

class TaxAndDuty implements ResultInterface
{

    /**
     * @var \Metapack\Type\Property
     */
    private $properties;

    /**
     * @var float
     */
    private $taxAndDuty;

    /**
     * @var string
     */
    private $taxAndDutyCurrencyCode;

    /**
     * @var float
     */
    private $taxAndDutyCurrencyRate;

    /**
     * @var string
     */
    private $taxAndDutyStatusText;

    /**
     * @return \Metapack\Type\Property
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param \Metapack\Type\Property $properties
     * @return TaxAndDuty
     */
    public function withProperties($properties)
    {
        $new = clone $this;
        $new->properties = $properties;

        return $new;
    }

    /**
     * @return float
     */
    public function getTaxAndDuty()
    {
        return $this->taxAndDuty;
    }

    /**
     * @param float $taxAndDuty
     * @return TaxAndDuty
     */
    public function withTaxAndDuty($taxAndDuty)
    {
        $new = clone $this;
        $new->taxAndDuty = $taxAndDuty;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxAndDutyCurrencyCode()
    {
        return $this->taxAndDutyCurrencyCode;
    }

    /**
     * @param string $taxAndDutyCurrencyCode
     * @return TaxAndDuty
     */
    public function withTaxAndDutyCurrencyCode($taxAndDutyCurrencyCode)
    {
        $new = clone $this;
        $new->taxAndDutyCurrencyCode = $taxAndDutyCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getTaxAndDutyCurrencyRate()
    {
        return $this->taxAndDutyCurrencyRate;
    }

    /**
     * @param float $taxAndDutyCurrencyRate
     * @return TaxAndDuty
     */
    public function withTaxAndDutyCurrencyRate($taxAndDutyCurrencyRate)
    {
        $new = clone $this;
        $new->taxAndDutyCurrencyRate = $taxAndDutyCurrencyRate;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxAndDutyStatusText()
    {
        return $this->taxAndDutyStatusText;
    }

    /**
     * @param string $taxAndDutyStatusText
     * @return TaxAndDuty
     */
    public function withTaxAndDutyStatusText($taxAndDutyStatusText)
    {
        $new = clone $this;
        $new->taxAndDutyStatusText = $taxAndDutyStatusText;

        return $new;
    }


}

