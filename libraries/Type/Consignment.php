<?php

namespace Metapack\Type;


use Phpro\SoapClient\Type\ResultInterface;

class Consignment implements ResultInterface
{

    /**
     * @var bool
     */
    private $alreadyPalletisedGoodsFlag = null;

    /**
     * @var string
     */
    private $cardNumber = null;

    /**
     * @var string
     */
    private $carrierCode = null;

    /**
     * @var string
     */
    private $carrierConsignmentCode = null;

    /**
     * @var string
     */
    private $carrierName = null;

    /**
     * @var string
     */
    private $carrierServiceCode = null;

    /**
     * @var string
     */
    private $carrierServiceName = null;

    /**
     * @var float
     */
    private $carrierServiceVATRate = null;

    /**
     * @var string
     */
    private $cartonNumber = null;

    /**
     * @var string
     */
    private $cashOnDeliveryCurrencyCode = null;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $committedCollectionWindow = null;

    /**
     * @var \Metapack\Type\DateRange
     */
    private $committedDeliveryWindow = null;

    /**
     * @var string
     */
    private $consDestinationReference = null;

    /**
     * @var string
     */
    private $consOriginReference = null;

    /**
     * @var string
     */
    private $consRecipientReference = null;

    /**
     * @var string
     */
    private $consReference = null;

    /**
     * @var string
     */
    private $consSenderReference = null;

    /**
     * @var string
     */
    private $consignmentCode = null;

    /**
     * @var bool
     */
    private $consignmentLevelDetailsFlag = null;

    /**
     * @var float
     */
    private $consignmentValue = null;

    /**
     * @var string
     */
    private $consignmentValueCurrencyCode = null;

    /**
     * @var float
     */
    private $consignmentValueCurrencyRate = null;

    /**
     * @var float
     */
    private $consignmentWeight = null;

    /**
     * @var string
     */
    private $custom1 = null;

    /**
     * @var string
     */
    private $custom10 = null;

    /**
     * @var string
     */
    private $custom2 = null;

    /**
     * @var string
     */
    private $custom3 = null;

    /**
     * @var string
     */
    private $custom4 = null;

    /**
     * @var string
     */
    private $custom5 = null;

    /**
     * @var string
     */
    private $custom6 = null;

    /**
     * @var string
     */
    private $custom7 = null;

    /**
     * @var string
     */
    private $custom8 = null;

    /**
     * @var string
     */
    private $custom9 = null;

    /**
     * @var bool
     */
    private $customsDocumentationRequired = null;

    /**
     * @var \DateTimeInterface
     */
    private $cutOffDate = null;

    /**
     * @var \DateTimeInterface
     */
    private $despatchDate = null;

    /**
     * @var \DateTimeInterface
     */
    private $earliestDeliveryDate = null;

    /**
     * @var string
     */
    private $endVatNumber = null;

    /**
     * @var bool
     */
    private $fragileGoodsFlag = null;

    /**
     * @var \DateTimeInterface
     */
    private $guaranteedDeliveryDate = null;

    /**
     * @var bool
     */
    private $hazardousGoodsFlag = null;

    /**
     * @var float
     */
    private $insuranceValue = null;

    /**
     * @var string
     */
    private $insuranceValueCurrencyCode = null;

    /**
     * @var float
     */
    private $insuranceValueCurrencyRate = null;

    /**
     * @var string
     */
    private $languageCode = null;

    /**
     * @var bool
     */
    private $liquidGoodsFlag = null;

    /**
     * @var string
     */
    private $manifestGroupCode = null;

    /**
     * @var float
     */
    private $maxDimension = null;

    /**
     * @var string
     */
    private $metaCampaignKey = null;

    /**
     * @var string
     */
    private $metaCustomerKey = null;

    /**
     * @var bool
     */
    private $moreThanOneMetreGoodsFlag = null;

    /**
     * @var bool
     */
    private $moreThanTwentyFiveKgGoodsFlag = null;

    /**
     * @var \DateTimeInterface
     */
    private $orderDate = null;

    /**
     * @var string
     */
    private $orderNumber = null;

    /**
     * @var float
     */
    private $orderValue = null;

    /**
     * @var int
     */
    private $parcelCount = null;

    /**
     * @var \Metapack\Type\Parcel
     */
    private $parcels = null;

    /**
     * @var string
     */
    private $pickTicketNumber = null;

    /**
     * @var string
     */
    private $pickupPoint = null;

    /**
     * @var string
     */
    private $podRequired = null;

    /**
     * @var \Metapack\Type\Property
     */
    private $properties = null;

    /**
     * @var \Metapack\Type\Address
     */
    private $recipientAddress = null;

    /**
     * @var string
     */
    private $recipientCode = null;

    /**
     * @var string
     */
    private $recipientContactPhone = null;

    /**
     * @var string
     */
    private $recipientEmail = null;

    /**
     * @var string
     */
    private $recipientMobilePhone = null;

    /**
     * @var string
     */
    private $recipientName = null;

    /**
     * @var string
     */
    private $recipientNotificationType = null;

    /**
     * @var string
     */
    private $recipientPhone = null;

    /**
     * @var string
     */
    private $recipientTimeZone = null;

    /**
     * @var string
     */
    private $recipientVatNumber = null;

    /**
     * @var \Metapack\Type\Address
     */
    private $returnAddress = null;

    /**
     * @var \Metapack\Type\Address
     */
    private $senderAddress = null;

    /**
     * @var string
     */
    private $senderCode = null;

    /**
     * @var string
     */
    private $senderContactPhone = null;

    /**
     * @var string
     */
    private $senderEmail = null;

    /**
     * @var string
     */
    private $senderMobilePhone = null;

    /**
     * @var string
     */
    private $senderName = null;

    /**
     * @var string
     */
    private $senderNotificationType = null;

    /**
     * @var string
     */
    private $senderPhone = null;

    /**
     * @var string
     */
    private $senderTimeZone = null;

    /**
     * @var string
     */
    private $senderVatNumber = null;

    /**
     * @var string
     */
    private $shipmentTypeCode = null;

    /**
     * @var string
     */
    private $shippingAccount = null;

    /**
     * @var float
     */
    private $shippingCharge = null;

    /**
     * @var string
     */
    private $shippingChargeCurrencyCode = null;

    /**
     * @var float
     */
    private $shippingChargeCurrencyRate = null;

    /**
     * @var float
     */
    private $shippingCost = null;

    /**
     * @var string
     */
    private $shippingCostCurrencyCode = null;

    /**
     * @var float
     */
    private $shippingCostCurrencyRate = null;

    /**
     * @var string
     */
    private $signatoryOnCustoms = null;

    /**
     * @var string
     */
    private $specialInstructions1 = null;

    /**
     * @var string
     */
    private $specialInstructions2 = null;

    /**
     * @var string
     */
    private $startVatNumber = null;

    /**
     * @var string
     */
    private $status = null;

    /**
     * @var float
     */
    private $taxAndDuty = null;

    /**
     * @var string
     */
    private $taxAndDutyCurrencyCode = null;

    /**
     * @var float
     */
    private $taxAndDutyCurrencyRate = null;

    /**
     * @var string
     */
    private $taxAndDutyStatusText = null;

    /**
     * @var string
     */
    private $taxDutyDeclarationCurrencyCode = null;

    /**
     * @var string
     */
    private $termsOfTradeCode = null;

    /**
     * @var string
     */
    private $transactionType = null;

    /**
     * @var bool
     */
    private $twoManLiftFlag = null;

    /**
     * @return bool
     */
    public function getAlreadyPalletisedGoodsFlag()
    {
        return $this->alreadyPalletisedGoodsFlag;
    }

    /**
     * @param bool $alreadyPalletisedGoodsFlag
     * @return Consignment
     */
    public function withAlreadyPalletisedGoodsFlag($alreadyPalletisedGoodsFlag)
    {
        $new = clone $this;
        $new->alreadyPalletisedGoodsFlag = $alreadyPalletisedGoodsFlag;

        return $new;
    }

    /**
     * @return string
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param string $cardNumber
     * @return Consignment
     */
    public function withCardNumber($cardNumber)
    {
        $new = clone $this;
        $new->cardNumber = $cardNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierCode()
    {
        return $this->carrierCode;
    }

    /**
     * @param string $carrierCode
     * @return Consignment
     */
    public function withCarrierCode($carrierCode)
    {
        $new = clone $this;
        $new->carrierCode = $carrierCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierConsignmentCode()
    {
        return $this->carrierConsignmentCode;
    }

    /**
     * @param string $carrierConsignmentCode
     * @return Consignment
     */
    public function withCarrierConsignmentCode($carrierConsignmentCode)
    {
        $new = clone $this;
        $new->carrierConsignmentCode = $carrierConsignmentCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierName()
    {
        return $this->carrierName;
    }

    /**
     * @param string $carrierName
     * @return Consignment
     */
    public function withCarrierName($carrierName)
    {
        $new = clone $this;
        $new->carrierName = $carrierName;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierServiceCode()
    {
        return $this->carrierServiceCode;
    }

    /**
     * @param string $carrierServiceCode
     * @return Consignment
     */
    public function withCarrierServiceCode($carrierServiceCode)
    {
        $new = clone $this;
        $new->carrierServiceCode = $carrierServiceCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getCarrierServiceName()
    {
        return $this->carrierServiceName;
    }

    /**
     * @param string $carrierServiceName
     * @return Consignment
     */
    public function withCarrierServiceName($carrierServiceName)
    {
        $new = clone $this;
        $new->carrierServiceName = $carrierServiceName;

        return $new;
    }

    /**
     * @return float
     */
    public function getCarrierServiceVATRate()
    {
        return $this->carrierServiceVATRate;
    }

    /**
     * @param float $carrierServiceVATRate
     * @return Consignment
     */
    public function withCarrierServiceVATRate($carrierServiceVATRate)
    {
        $new = clone $this;
        $new->carrierServiceVATRate = $carrierServiceVATRate;

        return $new;
    }

    /**
     * @return string
     */
    public function getCartonNumber()
    {
        return $this->cartonNumber;
    }

    /**
     * @param string $cartonNumber
     * @return Consignment
     */
    public function withCartonNumber($cartonNumber)
    {
        $new = clone $this;
        $new->cartonNumber = $cartonNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getCashOnDeliveryCurrencyCode()
    {
        return $this->cashOnDeliveryCurrencyCode;
    }

    /**
     * @param string $cashOnDeliveryCurrencyCode
     * @return Consignment
     */
    public function withCashOnDeliveryCurrencyCode($cashOnDeliveryCurrencyCode)
    {
        $new = clone $this;
        $new->cashOnDeliveryCurrencyCode = $cashOnDeliveryCurrencyCode;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getCommittedCollectionWindow()
    {
        return $this->committedCollectionWindow;
    }

    /**
     * @param \Metapack\Type\DateRange $committedCollectionWindow
     * @return Consignment
     */
    public function withCommittedCollectionWindow($committedCollectionWindow)
    {
        $new = clone $this;
        $new->committedCollectionWindow = $committedCollectionWindow;

        return $new;
    }

    /**
     * @return \Metapack\Type\DateRange
     */
    public function getCommittedDeliveryWindow()
    {
        return $this->committedDeliveryWindow;
    }

    /**
     * @param \Metapack\Type\DateRange $committedDeliveryWindow
     * @return Consignment
     */
    public function withCommittedDeliveryWindow($committedDeliveryWindow)
    {
        $new = clone $this;
        $new->committedDeliveryWindow = $committedDeliveryWindow;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsDestinationReference()
    {
        return $this->consDestinationReference;
    }

    /**
     * @param string $consDestinationReference
     * @return Consignment
     */
    public function withConsDestinationReference($consDestinationReference)
    {
        $new = clone $this;
        $new->consDestinationReference = $consDestinationReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsOriginReference()
    {
        return $this->consOriginReference;
    }

    /**
     * @param string $consOriginReference
     * @return Consignment
     */
    public function withConsOriginReference($consOriginReference)
    {
        $new = clone $this;
        $new->consOriginReference = $consOriginReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsRecipientReference()
    {
        return $this->consRecipientReference;
    }

    /**
     * @param string $consRecipientReference
     * @return Consignment
     */
    public function withConsRecipientReference($consRecipientReference)
    {
        $new = clone $this;
        $new->consRecipientReference = $consRecipientReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsReference()
    {
        return $this->consReference;
    }

    /**
     * @param string $consReference
     * @return Consignment
     */
    public function withConsReference($consReference)
    {
        $new = clone $this;
        $new->consReference = $consReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsSenderReference()
    {
        return $this->consSenderReference;
    }

    /**
     * @param string $consSenderReference
     * @return Consignment
     */
    public function withConsSenderReference($consSenderReference)
    {
        $new = clone $this;
        $new->consSenderReference = $consSenderReference;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsignmentCode()
    {
        return $this->consignmentCode;
    }

    /**
     * @param string $consignmentCode
     * @return Consignment
     */
    public function withConsignmentCode($consignmentCode)
    {
        $new = clone $this;
        $new->consignmentCode = $consignmentCode;

        return $new;
    }

    /**
     * @return bool
     */
    public function getConsignmentLevelDetailsFlag()
    {
        return $this->consignmentLevelDetailsFlag;
    }

    /**
     * @param bool $consignmentLevelDetailsFlag
     * @return Consignment
     */
    public function withConsignmentLevelDetailsFlag($consignmentLevelDetailsFlag)
    {
        $new = clone $this;
        $new->consignmentLevelDetailsFlag = $consignmentLevelDetailsFlag;

        return $new;
    }

    /**
     * @return float
     */
    public function getConsignmentValue()
    {
        return $this->consignmentValue;
    }

    /**
     * @param float $consignmentValue
     * @return Consignment
     */
    public function withConsignmentValue($consignmentValue)
    {
        $new = clone $this;
        $new->consignmentValue = $consignmentValue;

        return $new;
    }

    /**
     * @return string
     */
    public function getConsignmentValueCurrencyCode()
    {
        return $this->consignmentValueCurrencyCode;
    }

    /**
     * @param string $consignmentValueCurrencyCode
     * @return Consignment
     */
    public function withConsignmentValueCurrencyCode($consignmentValueCurrencyCode)
    {
        $new = clone $this;
        $new->consignmentValueCurrencyCode = $consignmentValueCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getConsignmentValueCurrencyRate()
    {
        return $this->consignmentValueCurrencyRate;
    }

    /**
     * @param float $consignmentValueCurrencyRate
     * @return Consignment
     */
    public function withConsignmentValueCurrencyRate($consignmentValueCurrencyRate)
    {
        $new = clone $this;
        $new->consignmentValueCurrencyRate = $consignmentValueCurrencyRate;

        return $new;
    }

    /**
     * @return float
     */
    public function getConsignmentWeight()
    {
        return $this->consignmentWeight;
    }

    /**
     * @param float $consignmentWeight
     * @return Consignment
     */
    public function withConsignmentWeight($consignmentWeight)
    {
        $new = clone $this;
        $new->consignmentWeight = $consignmentWeight;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * @param string $custom1
     * @return Consignment
     */
    public function withCustom1($custom1)
    {
        $new = clone $this;
        $new->custom1 = $custom1;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * @param string $custom10
     * @return Consignment
     */
    public function withCustom10($custom10)
    {
        $new = clone $this;
        $new->custom10 = $custom10;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * @param string $custom2
     * @return Consignment
     */
    public function withCustom2($custom2)
    {
        $new = clone $this;
        $new->custom2 = $custom2;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * @param string $custom3
     * @return Consignment
     */
    public function withCustom3($custom3)
    {
        $new = clone $this;
        $new->custom3 = $custom3;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * @param string $custom4
     * @return Consignment
     */
    public function withCustom4($custom4)
    {
        $new = clone $this;
        $new->custom4 = $custom4;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * @param string $custom5
     * @return Consignment
     */
    public function withCustom5($custom5)
    {
        $new = clone $this;
        $new->custom5 = $custom5;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * @param string $custom6
     * @return Consignment
     */
    public function withCustom6($custom6)
    {
        $new = clone $this;
        $new->custom6 = $custom6;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * @param string $custom7
     * @return Consignment
     */
    public function withCustom7($custom7)
    {
        $new = clone $this;
        $new->custom7 = $custom7;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * @param string $custom8
     * @return Consignment
     */
    public function withCustom8($custom8)
    {
        $new = clone $this;
        $new->custom8 = $custom8;

        return $new;
    }

    /**
     * @return string
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * @param string $custom9
     * @return Consignment
     */
    public function withCustom9($custom9)
    {
        $new = clone $this;
        $new->custom9 = $custom9;

        return $new;
    }

    /**
     * @return bool
     */
    public function getCustomsDocumentationRequired()
    {
        return $this->customsDocumentationRequired;
    }

    /**
     * @param bool $customsDocumentationRequired
     * @return Consignment
     */
    public function withCustomsDocumentationRequired($customsDocumentationRequired)
    {
        $new = clone $this;
        $new->customsDocumentationRequired = $customsDocumentationRequired;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCutOffDate()
    {
        return $this->cutOffDate;
    }

    /**
     * @param \DateTimeInterface $cutOffDate
     * @return Consignment
     */
    public function withCutOffDate($cutOffDate)
    {
        $new = clone $this;
        $new->cutOffDate = $cutOffDate;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getDespatchDate()
    {
        return $this->despatchDate;
    }

    /**
     * @param \DateTimeInterface $despatchDate
     * @return Consignment
     */
    public function withDespatchDate($despatchDate)
    {
        $new = clone $this;
        $new->despatchDate = $despatchDate;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getEarliestDeliveryDate()
    {
        return $this->earliestDeliveryDate;
    }

    /**
     * @param \DateTimeInterface $earliestDeliveryDate
     * @return Consignment
     */
    public function withEarliestDeliveryDate($earliestDeliveryDate)
    {
        $new = clone $this;
        $new->earliestDeliveryDate = $earliestDeliveryDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getEndVatNumber()
    {
        return $this->endVatNumber;
    }

    /**
     * @param string $endVatNumber
     * @return Consignment
     */
    public function withEndVatNumber($endVatNumber)
    {
        $new = clone $this;
        $new->endVatNumber = $endVatNumber;

        return $new;
    }

    /**
     * @return bool
     */
    public function getFragileGoodsFlag()
    {
        return $this->fragileGoodsFlag;
    }

    /**
     * @param bool $fragileGoodsFlag
     * @return Consignment
     */
    public function withFragileGoodsFlag($fragileGoodsFlag)
    {
        $new = clone $this;
        $new->fragileGoodsFlag = $fragileGoodsFlag;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getGuaranteedDeliveryDate()
    {
        return $this->guaranteedDeliveryDate;
    }

    /**
     * @param \DateTimeInterface $guaranteedDeliveryDate
     * @return Consignment
     */
    public function withGuaranteedDeliveryDate($guaranteedDeliveryDate)
    {
        $new = clone $this;
        $new->guaranteedDeliveryDate = $guaranteedDeliveryDate;

        return $new;
    }

    /**
     * @return bool
     */
    public function getHazardousGoodsFlag()
    {
        return $this->hazardousGoodsFlag;
    }

    /**
     * @param bool $hazardousGoodsFlag
     * @return Consignment
     */
    public function withHazardousGoodsFlag($hazardousGoodsFlag)
    {
        $new = clone $this;
        $new->hazardousGoodsFlag = $hazardousGoodsFlag;

        return $new;
    }

    /**
     * @return float
     */
    public function getInsuranceValue()
    {
        return $this->insuranceValue;
    }

    /**
     * @param float $insuranceValue
     * @return Consignment
     */
    public function withInsuranceValue($insuranceValue)
    {
        $new = clone $this;
        $new->insuranceValue = $insuranceValue;

        return $new;
    }

    /**
     * @return string
     */
    public function getInsuranceValueCurrencyCode()
    {
        return $this->insuranceValueCurrencyCode;
    }

    /**
     * @param string $insuranceValueCurrencyCode
     * @return Consignment
     */
    public function withInsuranceValueCurrencyCode($insuranceValueCurrencyCode)
    {
        $new = clone $this;
        $new->insuranceValueCurrencyCode = $insuranceValueCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getInsuranceValueCurrencyRate()
    {
        return $this->insuranceValueCurrencyRate;
    }

    /**
     * @param float $insuranceValueCurrencyRate
     * @return Consignment
     */
    public function withInsuranceValueCurrencyRate($insuranceValueCurrencyRate)
    {
        $new = clone $this;
        $new->insuranceValueCurrencyRate = $insuranceValueCurrencyRate;

        return $new;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @return Consignment
     */
    public function withLanguageCode($languageCode)
    {
        $new = clone $this;
        $new->languageCode = $languageCode;

        return $new;
    }

    /**
     * @return bool
     */
    public function getLiquidGoodsFlag()
    {
        return $this->liquidGoodsFlag;
    }

    /**
     * @param bool $liquidGoodsFlag
     * @return Consignment
     */
    public function withLiquidGoodsFlag($liquidGoodsFlag)
    {
        $new = clone $this;
        $new->liquidGoodsFlag = $liquidGoodsFlag;

        return $new;
    }

    /**
     * @return string
     */
    public function getManifestGroupCode()
    {
        return $this->manifestGroupCode;
    }

    /**
     * @param string $manifestGroupCode
     * @return Consignment
     */
    public function withManifestGroupCode($manifestGroupCode)
    {
        $new = clone $this;
        $new->manifestGroupCode = $manifestGroupCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getMaxDimension()
    {
        return $this->maxDimension;
    }

    /**
     * @param float $maxDimension
     * @return Consignment
     */
    public function withMaxDimension($maxDimension)
    {
        $new = clone $this;
        $new->maxDimension = $maxDimension;

        return $new;
    }

    /**
     * @return string
     */
    public function getMetaCampaignKey()
    {
        return $this->metaCampaignKey;
    }

    /**
     * @param string $metaCampaignKey
     * @return Consignment
     */
    public function withMetaCampaignKey($metaCampaignKey)
    {
        $new = clone $this;
        $new->metaCampaignKey = $metaCampaignKey;

        return $new;
    }

    /**
     * @return string
     */
    public function getMetaCustomerKey()
    {
        return $this->metaCustomerKey;
    }

    /**
     * @param string $metaCustomerKey
     * @return Consignment
     */
    public function withMetaCustomerKey($metaCustomerKey)
    {
        $new = clone $this;
        $new->metaCustomerKey = $metaCustomerKey;

        return $new;
    }

    /**
     * @return bool
     */
    public function getMoreThanOneMetreGoodsFlag()
    {
        return $this->moreThanOneMetreGoodsFlag;
    }

    /**
     * @param bool $moreThanOneMetreGoodsFlag
     * @return Consignment
     */
    public function withMoreThanOneMetreGoodsFlag($moreThanOneMetreGoodsFlag)
    {
        $new = clone $this;
        $new->moreThanOneMetreGoodsFlag = $moreThanOneMetreGoodsFlag;

        return $new;
    }

    /**
     * @return bool
     */
    public function getMoreThanTwentyFiveKgGoodsFlag()
    {
        return $this->moreThanTwentyFiveKgGoodsFlag;
    }

    /**
     * @param bool $moreThanTwentyFiveKgGoodsFlag
     * @return Consignment
     */
    public function withMoreThanTwentyFiveKgGoodsFlag($moreThanTwentyFiveKgGoodsFlag)
    {
        $new = clone $this;
        $new->moreThanTwentyFiveKgGoodsFlag = $moreThanTwentyFiveKgGoodsFlag;

        return $new;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getOrderDate()
    {
        return $this->orderDate;
    }

    /**
     * @param \DateTimeInterface $orderDate
     * @return Consignment
     */
    public function withOrderDate($orderDate)
    {
        $new = clone $this;
        $new->orderDate = $orderDate;

        return $new;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @param string $orderNumber
     * @return Consignment
     */
    public function withOrderNumber($orderNumber)
    {
        $new = clone $this;
        $new->orderNumber = $orderNumber;

        return $new;
    }

    /**
     * @return float
     */
    public function getOrderValue()
    {
        return $this->orderValue;
    }

    /**
     * @param float $orderValue
     * @return Consignment
     */
    public function withOrderValue($orderValue)
    {
        $new = clone $this;
        $new->orderValue = $orderValue;

        return $new;
    }

    /**
     * @return int
     */
    public function getParcelCount()
    {
        return $this->parcelCount;
    }

    /**
     * @param int $parcelCount
     * @return Consignment
     */
    public function withParcelCount($parcelCount)
    {
        $new = clone $this;
        $new->parcelCount = $parcelCount;

        return $new;
    }

    /**
     * @return \Metapack\Type\Parcel
     */
    public function getParcels()
    {
        return $this->parcels;
    }

    /**
     * @param \Metapack\Type\Parcel $parcels
     * @return Consignment
     */
    public function withParcels($parcels)
    {
        $new = clone $this;
        $new->parcels = $parcels;

        return $new;
    }

    /**
     * @return string
     */
    public function getPickTicketNumber()
    {
        return $this->pickTicketNumber;
    }

    /**
     * @param string $pickTicketNumber
     * @return Consignment
     */
    public function withPickTicketNumber($pickTicketNumber)
    {
        $new = clone $this;
        $new->pickTicketNumber = $pickTicketNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getPickupPoint()
    {
        return $this->pickupPoint;
    }

    /**
     * @param string $pickupPoint
     * @return Consignment
     */
    public function withPickupPoint($pickupPoint)
    {
        $new = clone $this;
        $new->pickupPoint = $pickupPoint;

        return $new;
    }

    /**
     * @return string
     */
    public function getPodRequired()
    {
        return $this->podRequired;
    }

    /**
     * @param string $podRequired
     * @return Consignment
     */
    public function withPodRequired($podRequired)
    {
        $new = clone $this;
        $new->podRequired = $podRequired;

        return $new;
    }

    /**
     * @return \Metapack\Type\Property
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * @param \Metapack\Type\Property $properties
     * @return Consignment
     */
    public function withProperties($properties)
    {
        $new = clone $this;
        $new->properties = $properties;

        return $new;
    }

    /**
     * @return \Metapack\Type\Address
     */
    public function getRecipientAddress()
    {
        return $this->recipientAddress;
    }

    /**
     * @param \Metapack\Type\Address $recipientAddress
     * @return Consignment
     */
    public function withRecipientAddress($recipientAddress)
    {
        $new = clone $this;
        $new->recipientAddress = $recipientAddress;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientCode()
    {
        return $this->recipientCode;
    }

    /**
     * @param string $recipientCode
     * @return Consignment
     */
    public function withRecipientCode($recipientCode)
    {
        $new = clone $this;
        $new->recipientCode = $recipientCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientContactPhone()
    {
        return $this->recipientContactPhone;
    }

    /**
     * @param string $recipientContactPhone
     * @return Consignment
     */
    public function withRecipientContactPhone($recipientContactPhone)
    {
        $new = clone $this;
        $new->recipientContactPhone = $recipientContactPhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientEmail()
    {
        return $this->recipientEmail;
    }

    /**
     * @param string $recipientEmail
     * @return Consignment
     */
    public function withRecipientEmail($recipientEmail)
    {
        $new = clone $this;
        $new->recipientEmail = $recipientEmail;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientMobilePhone()
    {
        return $this->recipientMobilePhone;
    }

    /**
     * @param string $recipientMobilePhone
     * @return Consignment
     */
    public function withRecipientMobilePhone($recipientMobilePhone)
    {
        $new = clone $this;
        $new->recipientMobilePhone = $recipientMobilePhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }

    /**
     * @param string $recipientName
     * @return Consignment
     */
    public function withRecipientName($recipientName)
    {
        $new = clone $this;
        $new->recipientName = $recipientName;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientNotificationType()
    {
        return $this->recipientNotificationType;
    }

    /**
     * @param string $recipientNotificationType
     * @return Consignment
     */
    public function withRecipientNotificationType($recipientNotificationType)
    {
        $new = clone $this;
        $new->recipientNotificationType = $recipientNotificationType;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientPhone()
    {
        return $this->recipientPhone;
    }

    /**
     * @param string $recipientPhone
     * @return Consignment
     */
    public function withRecipientPhone($recipientPhone)
    {
        $new = clone $this;
        $new->recipientPhone = $recipientPhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientTimeZone()
    {
        return $this->recipientTimeZone;
    }

    /**
     * @param string $recipientTimeZone
     * @return Consignment
     */
    public function withRecipientTimeZone($recipientTimeZone)
    {
        $new = clone $this;
        $new->recipientTimeZone = $recipientTimeZone;

        return $new;
    }

    /**
     * @return string
     */
    public function getRecipientVatNumber()
    {
        return $this->recipientVatNumber;
    }

    /**
     * @param string $recipientVatNumber
     * @return Consignment
     */
    public function withRecipientVatNumber($recipientVatNumber)
    {
        $new = clone $this;
        $new->recipientVatNumber = $recipientVatNumber;

        return $new;
    }

    /**
     * @return \Metapack\Type\Address
     */
    public function getReturnAddress()
    {
        return $this->returnAddress;
    }

    /**
     * @param \Metapack\Type\Address $returnAddress
     * @return Consignment
     */
    public function withReturnAddress($returnAddress)
    {
        $new = clone $this;
        $new->returnAddress = $returnAddress;

        return $new;
    }

    /**
     * @return \Metapack\Type\Address
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
     * @param \Metapack\Type\Address $senderAddress
     * @return Consignment
     */
    public function withSenderAddress($senderAddress)
    {
        $new = clone $this;
        $new->senderAddress = $senderAddress;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderCode()
    {
        return $this->senderCode;
    }

    /**
     * @param string $senderCode
     * @return Consignment
     */
    public function withSenderCode($senderCode)
    {
        $new = clone $this;
        $new->senderCode = $senderCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderContactPhone()
    {
        return $this->senderContactPhone;
    }

    /**
     * @param string $senderContactPhone
     * @return Consignment
     */
    public function withSenderContactPhone($senderContactPhone)
    {
        $new = clone $this;
        $new->senderContactPhone = $senderContactPhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param string $senderEmail
     * @return Consignment
     */
    public function withSenderEmail($senderEmail)
    {
        $new = clone $this;
        $new->senderEmail = $senderEmail;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderMobilePhone()
    {
        return $this->senderMobilePhone;
    }

    /**
     * @param string $senderMobilePhone
     * @return Consignment
     */
    public function withSenderMobilePhone($senderMobilePhone)
    {
        $new = clone $this;
        $new->senderMobilePhone = $senderMobilePhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }

    /**
     * @param string $senderName
     * @return Consignment
     */
    public function withSenderName($senderName)
    {
        $new = clone $this;
        $new->senderName = $senderName;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderNotificationType()
    {
        return $this->senderNotificationType;
    }

    /**
     * @param string $senderNotificationType
     * @return Consignment
     */
    public function withSenderNotificationType($senderNotificationType)
    {
        $new = clone $this;
        $new->senderNotificationType = $senderNotificationType;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderPhone()
    {
        return $this->senderPhone;
    }

    /**
     * @param string $senderPhone
     * @return Consignment
     */
    public function withSenderPhone($senderPhone)
    {
        $new = clone $this;
        $new->senderPhone = $senderPhone;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderTimeZone()
    {
        return $this->senderTimeZone;
    }

    /**
     * @param string $senderTimeZone
     * @return Consignment
     */
    public function withSenderTimeZone($senderTimeZone)
    {
        $new = clone $this;
        $new->senderTimeZone = $senderTimeZone;

        return $new;
    }

    /**
     * @return string
     */
    public function getSenderVatNumber()
    {
        return $this->senderVatNumber;
    }

    /**
     * @param string $senderVatNumber
     * @return Consignment
     */
    public function withSenderVatNumber($senderVatNumber)
    {
        $new = clone $this;
        $new->senderVatNumber = $senderVatNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getShipmentTypeCode()
    {
        return $this->shipmentTypeCode;
    }

    /**
     * @param string $shipmentTypeCode
     * @return Consignment
     */
    public function withShipmentTypeCode($shipmentTypeCode)
    {
        $new = clone $this;
        $new->shipmentTypeCode = $shipmentTypeCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getShippingAccount()
    {
        return $this->shippingAccount;
    }

    /**
     * @param string $shippingAccount
     * @return Consignment
     */
    public function withShippingAccount($shippingAccount)
    {
        $new = clone $this;
        $new->shippingAccount = $shippingAccount;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingCharge()
    {
        return $this->shippingCharge;
    }

    /**
     * @param float $shippingCharge
     * @return Consignment
     */
    public function withShippingCharge($shippingCharge)
    {
        $new = clone $this;
        $new->shippingCharge = $shippingCharge;

        return $new;
    }

    /**
     * @return string
     */
    public function getShippingChargeCurrencyCode()
    {
        return $this->shippingChargeCurrencyCode;
    }

    /**
     * @param string $shippingChargeCurrencyCode
     * @return Consignment
     */
    public function withShippingChargeCurrencyCode($shippingChargeCurrencyCode)
    {
        $new = clone $this;
        $new->shippingChargeCurrencyCode = $shippingChargeCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingChargeCurrencyRate()
    {
        return $this->shippingChargeCurrencyRate;
    }

    /**
     * @param float $shippingChargeCurrencyRate
     * @return Consignment
     */
    public function withShippingChargeCurrencyRate($shippingChargeCurrencyRate)
    {
        $new = clone $this;
        $new->shippingChargeCurrencyRate = $shippingChargeCurrencyRate;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @param float $shippingCost
     * @return Consignment
     */
    public function withShippingCost($shippingCost)
    {
        $new = clone $this;
        $new->shippingCost = $shippingCost;

        return $new;
    }

    /**
     * @return string
     */
    public function getShippingCostCurrencyCode()
    {
        return $this->shippingCostCurrencyCode;
    }

    /**
     * @param string $shippingCostCurrencyCode
     * @return Consignment
     */
    public function withShippingCostCurrencyCode($shippingCostCurrencyCode)
    {
        $new = clone $this;
        $new->shippingCostCurrencyCode = $shippingCostCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getShippingCostCurrencyRate()
    {
        return $this->shippingCostCurrencyRate;
    }

    /**
     * @param float $shippingCostCurrencyRate
     * @return Consignment
     */
    public function withShippingCostCurrencyRate($shippingCostCurrencyRate)
    {
        $new = clone $this;
        $new->shippingCostCurrencyRate = $shippingCostCurrencyRate;

        return $new;
    }

    /**
     * @return string
     */
    public function getSignatoryOnCustoms()
    {
        return $this->signatoryOnCustoms;
    }

    /**
     * @param string $signatoryOnCustoms
     * @return Consignment
     */
    public function withSignatoryOnCustoms($signatoryOnCustoms)
    {
        $new = clone $this;
        $new->signatoryOnCustoms = $signatoryOnCustoms;

        return $new;
    }

    /**
     * @return string
     */
    public function getSpecialInstructions1()
    {
        return $this->specialInstructions1;
    }

    /**
     * @param string $specialInstructions1
     * @return Consignment
     */
    public function withSpecialInstructions1($specialInstructions1)
    {
        $new = clone $this;
        $new->specialInstructions1 = $specialInstructions1;

        return $new;
    }

    /**
     * @return string
     */
    public function getSpecialInstructions2()
    {
        return $this->specialInstructions2;
    }

    /**
     * @param string $specialInstructions2
     * @return Consignment
     */
    public function withSpecialInstructions2($specialInstructions2)
    {
        $new = clone $this;
        $new->specialInstructions2 = $specialInstructions2;

        return $new;
    }

    /**
     * @return string
     */
    public function getStartVatNumber()
    {
        return $this->startVatNumber;
    }

    /**
     * @param string $startVatNumber
     * @return Consignment
     */
    public function withStartVatNumber($startVatNumber)
    {
        $new = clone $this;
        $new->startVatNumber = $startVatNumber;

        return $new;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Consignment
     */
    public function withStatus($status)
    {
        $new = clone $this;
        $new->status = $status;

        return $new;
    }

    /**
     * @return float
     */
    public function getTaxAndDuty()
    {
        return $this->taxAndDuty;
    }

    /**
     * @param float $taxAndDuty
     * @return Consignment
     */
    public function withTaxAndDuty($taxAndDuty)
    {
        $new = clone $this;
        $new->taxAndDuty = $taxAndDuty;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxAndDutyCurrencyCode()
    {
        return $this->taxAndDutyCurrencyCode;
    }

    /**
     * @param string $taxAndDutyCurrencyCode
     * @return Consignment
     */
    public function withTaxAndDutyCurrencyCode($taxAndDutyCurrencyCode)
    {
        $new = clone $this;
        $new->taxAndDutyCurrencyCode = $taxAndDutyCurrencyCode;

        return $new;
    }

    /**
     * @return float
     */
    public function getTaxAndDutyCurrencyRate()
    {
        return $this->taxAndDutyCurrencyRate;
    }

    /**
     * @param float $taxAndDutyCurrencyRate
     * @return Consignment
     */
    public function withTaxAndDutyCurrencyRate($taxAndDutyCurrencyRate)
    {
        $new = clone $this;
        $new->taxAndDutyCurrencyRate = $taxAndDutyCurrencyRate;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxAndDutyStatusText()
    {
        return $this->taxAndDutyStatusText;
    }

    /**
     * @param string $taxAndDutyStatusText
     * @return Consignment
     */
    public function withTaxAndDutyStatusText($taxAndDutyStatusText)
    {
        $new = clone $this;
        $new->taxAndDutyStatusText = $taxAndDutyStatusText;

        return $new;
    }

    /**
     * @return string
     */
    public function getTaxDutyDeclarationCurrencyCode()
    {
        return $this->taxDutyDeclarationCurrencyCode;
    }

    /**
     * @param string $taxDutyDeclarationCurrencyCode
     * @return Consignment
     */
    public function withTaxDutyDeclarationCurrencyCode($taxDutyDeclarationCurrencyCode)
    {
        $new = clone $this;
        $new->taxDutyDeclarationCurrencyCode = $taxDutyDeclarationCurrencyCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getTermsOfTradeCode()
    {
        return $this->termsOfTradeCode;
    }

    /**
     * @param string $termsOfTradeCode
     * @return Consignment
     */
    public function withTermsOfTradeCode($termsOfTradeCode)
    {
        $new = clone $this;
        $new->termsOfTradeCode = $termsOfTradeCode;

        return $new;
    }

    /**
     * @return string
     */
    public function getTransactionType()
    {
        return $this->transactionType;
    }

    /**
     * @param string $transactionType
     * @return Consignment
     */
    public function withTransactionType($transactionType)
    {
        $new = clone $this;
        $new->transactionType = $transactionType;

        return $new;
    }

    /**
     * @return bool
     */
    public function getTwoManLiftFlag()
    {
        return $this->twoManLiftFlag;
    }

    /**
     * @param bool $twoManLiftFlag
     * @return Consignment
     */
    public function withTwoManLiftFlag($twoManLiftFlag)
    {
        $new = clone $this;
        $new->twoManLiftFlag = $twoManLiftFlag;

        return $new;
    }


}

