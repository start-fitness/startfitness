<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;

class MPConsClassmap
{

    public static function getCollection() : \Phpro\SoapClient\Soap\ClassMap\ClassMapCollection
    {
        return new ClassMapCollection([
            new ClassMap('Labels', Type\Labels::class),
            new ClassMap('AuditRecord', Type\AuditRecord::class),
            new ClassMap('Property', Type\Property::class),
            new ClassMap('TaxAndDuty', Type\TaxAndDuty::class),
            new ClassMap('Product', Type\Product::class),
            new ClassMap('ConsignmentActionResult', Type\ConsignmentActionResult::class),
            new ClassMap('DateRange', Type\DateRange::class),
            new ClassMap('Parcel', Type\Parcel::class),
            new ClassMap('Address', Type\Address::class),
            new ClassMap('VerifiedAddress', Type\VerifiedAddress::class),
            new ClassMap('Consignment', Type\Consignment::class),
            new ClassMap('UpdateField', Type\UpdateField::class),
        ]);
    }


}

