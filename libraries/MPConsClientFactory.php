<?php

namespace Metapack;

use Metapack\MPConsClient;
use Metapack\MPConsClassmap;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapEngineFactory;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapOptions;

class MPConsClientFactory
{

    public static function factory(string $wsdl) : \Metapack\MPConsClient
    {
        $engine = ExtSoapEngineFactory::fromOptions(
            ExtSoapOptions::defaults($wsdl, [])
                ->withClassMap(MPConsClassmap::getCollection())
        );
        $eventDispatcher = new EventDispatcher();

        return new MPConsClient($engine, $eventDispatcher);
    }


}

