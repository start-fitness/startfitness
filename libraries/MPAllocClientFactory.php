<?php

namespace Metapack;

use Metapack\MPAllocClient;
use Metapack\MPAllocClassmap;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapEngineFactory;
use Phpro\SoapClient\Soap\Driver\ExtSoap\ExtSoapOptions;
use Http\Adapter\Guzzle6\Client;
use Phpro\SoapClient\Middleware\BasicAuthMiddleware;
use Phpro\SoapClient\Middleware\RemoveEmptyNodesMiddleware;
use Phpro\SoapClient\Soap\Handler\HttPlugHandle;

class MPAllocClientFactory
{

    public static function factory(string $wsdl) : \Metapack\MPAllocClient
    {
		$handler = HttPlugHandle::createForClient(
		   Client::createWithConfig(['headers' => ['User-Agent' => 'StartFitnessM2/1.0']])
		);
		$handler->addMiddleware(new BasicAuthMiddleware('startmagento', 'QWV$2774gw'));
		$handler->addMiddleware(new RemoveEmptyNodesMiddleware());
		$engine = ExtSoapEngineFactory::fromOptionsWithHandler(
			ExtSoapOptions::defaults($wsdl, []),
			$handler
		);
       
        $eventDispatcher = new EventDispatcher();

        return new MPAllocClient($engine, $eventDispatcher);
    }


}

