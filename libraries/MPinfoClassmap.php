<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Soap\ClassMap\ClassMapCollection;
use Phpro\SoapClient\Soap\ClassMap\ClassMap;

class MPinfoClassmap
{

    public static function getCollection() : \Phpro\SoapClient\Soap\ClassMap\ClassMapCollection
    {
        return new ClassMapCollection([
            new ClassMap('CodeName', Type\CodeName::class),
            new ClassMap('CarrierService', Type\CarrierService::class),
            new ClassMap('Address', Type\Address::class),
            new ClassMap('VerifiedAddress', Type\VerifiedAddress::class),
        ]);
    }


}

