<?php

namespace Metapack;

use Metapack\Type;
use Phpro\SoapClient\Type\RequestInterface;
use Phpro\SoapClient\Type\ResultInterface;
use Phpro\SoapClient\Exception\SoapException;

class MPSearchClient extends \Phpro\SoapClient\Client
{

    /**
     * @param RequestInterface|\string $consignmentCode
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function findConsignmentByConsignmentCode( $consignmentCode) /*: \Metapack\Type\Consignment*/
    {
        return $this->call('findConsignmentByConsignmentCode', $consignmentCode);
    }

    /**
     * @param RequestInterface|\string $orderReference
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function findConsignmentsByOrderReference(string $orderReference) : \Metapack\Type\Consignment
    {
        return $this->call('findConsignmentsByOrderReference', $orderReference);
    }

    /**
     * @param RequestInterface|\string $parcelCode
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function findConsignmentsByParcelCode(string $parcelCode) : \Metapack\Type\Consignment
    {
        return $this->call('findConsignmentsByParcelCode', $parcelCode);
    }

    /**
     * @param RequestInterface|Type\ConsignmentSearchParams $parameters
     * @return ResultInterface|Type\ConsignmentSearchResult
     * @throws SoapException
     */
    public function searchForConsignments(\Metapack\Type\ConsignmentSearchParams $parameters) : \Metapack\Type\ConsignmentSearchResult
    {
        return $this->call('searchForConsignments', $parameters);
    }

    /**
     * @param RequestInterface|\string $cartonId
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function findConsignmentsByCartonId(string $cartonId) : \Metapack\Type\Consignment
    {
        return $this->call('findConsignmentsByCartonId', $cartonId);
    }

    /**
     * @param RequestInterface|\string $carrierConsignmentCode
     * @return ResultInterface|Type\Consignment
     * @throws SoapException
     */
    public function findConsignmentsByCarrierConsignmentCode(string $carrierConsignmentCode) : \Metapack\Type\Consignment
    {
        return $this->call('findConsignmentsByCarrierConsignmentCode', $carrierConsignmentCode);
    }

    /**
     * @param RequestInterface|Type\SearchParams $parameters
     * @return ResultInterface|Type\SearchResult
     * @throws SoapException
     */
    public function search(\Metapack\Type\SearchParams $parameters) : \Metapack\Type\SearchResult
    {
        return $this->call('search', $parameters);
    }


}

